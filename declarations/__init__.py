"""
Declaration of special classes that are used throughout the model.
"""

__all__ = ["Process", "Product", "Emissions"]


import pandas as pd

class Product(pd.DataFrame):
    """
    |  The Product class is used to define all inputs and outputs in the model. The created objects are slightly modified pandas DataFrames, so that all pandas functions can still be applied.
    |  For Products the name of the item, the quantity (Q) and the unit (U) should always be defined.
    |  The easiest way to create Product objects it via the prod function from tools:
    
    |  **Examples:**
    |  from tools import prod
    |  prod("maize", Q=1, U="kg")
    |  prod(["maize","wheat"], Q=[1,2], U=["kg","kg"])
    |  prod(["maize","wheat"], [1,2], ["kg","kg"], N=[10,20])

    |  A = prod([("poultry","manure"), ("poultry","broiler")], [1,5])
    |  A.selitem("manure")
    """
    @property
    def _constructor(self):
        return Product

    # for unspecified single items introduce new __repr__ method
    def __repr__(self):
        if ((len(self.index) == 1) and (len(self.columns) == 2)):
            if all(self.columns == ['Q', 'U']):
                Q = self.loc[:,"Q"].item()
                U = self.loc[:,"U"].item()
                item = self.index[0]
                return f"Product('{item}', {Q}, {U})"
            else:
                return super().__repr__()
        else:
            return super().__repr__()

    def selitem(self, key):
        axis = 0
        labels = self._get_axis(axis)

        def make_list(x):
            if isinstance(x, list):
                return x
            elif isinstance(x, str):
                return [x]

        # create the tuple of the indexer
        _indexer = [slice(None)] * self.ndim

        item_position = labels.names.index("item") # which of the dimensions is item?
        item_list = [lab[item_position] for lab in labels]
        loc = [item in make_list(key) for item in item_list]
        _indexer[axis] = loc

        new_ax = labels[loc]

        indexer = tuple(_indexer)

        result = self.iloc[indexer]
        setattr(result, result._get_axis_name(axis), new_ax)
        return result


class Emissions(Product):
    """
    |  The Emissions class is used to define all process emissions in the model. The created objects are slightly modified pandas DataFrames, so that all pandas functions can still be applied.
    """
    @property
    def _constructor(self):
        return Emissions

    def __repr__(self):
        return super().__repr__()


class Costs(Product):
    """
    |  The Cost class is used to define all costs in the model. The created objects are slightly modified pandas DataFrames, so that all pandas functions can still be applied.
    """
    @property
    def _constructor(self):
        return Costs

    def __repr__(self):
        return super().__repr__()


class Process:
    """
    |  Process is the standard class of systems with input and output. It is mainly used to be inherited.
    |  Currently is assures that the emissions() and output() methods are defined with a return value of None, and that input() is the sum of I and additional_input().
    """
    def __init__(self, I, O):
        self.I = I
        self.O = O

    def additional_input(self):
        return None

    def input(self):
        if self.additional_input() is None:
            try:
                return self.I
            except:
                return None
        else:
            return pd.concat([self.I, self.additional_input()])

    def emissions(self):
        return None

    def output(self):
        return None

    def cashflow(self):
        return None


class IOProcess:
    """
    |  IOProcess is a variation of Process.
    |  It can be used for processes where bother either I or O can be specified, but no change is done, meaning that input = output
    """
    def __init__(self, I, O):
        self.I = I
        self.O = O

    def additional_input(self):
        return None

    def input(self):
        if self.additional_input() is None:
            try:
                return self.I
            except:
                return None
        else:
            return pd.concat([self.I, self.additional_input()])

    def emissions(self):
        return None

    def output(self):
        return None


class Device:
    """
    |  This is a the standard class for machine operations which only have a input and a emissions method, but no output.
    |  Therefore processes of this type cannot be linked directly in the analysis, but only be used in existing Processes.
    |  Examples: Tractor usage, WindrowCompostingMachine
    |  **NOTE:**
    |  - Not yet in use
    """
    def __init__(self, I):
        self.I = I

    def input(self):
        return None

    def emissions(self):
        return None



class Fraction(float):
    """
    Fraction between 0 and 1. Used to determine which share of a certain requested input is coming from which system
    """
    def __new__(self, value):
        if value >= 0 and value <= 1:
            return float.__new__(self, value)
        else:
            raise ValueError("Fraction must be between 0 and 1")


class Percent(float):
    """
    Percentage value between 0 and 100.
    """
    def __new__(self, value):
        if value >= 0 and value <= 100:
            return float.__new__(self, value/100)
        else:
            raise ValueError("Percentage value must be between 0 and 100")

    def __repr__(self):
        return str(self*100) + " %" 