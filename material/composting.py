from operator import neg
from tkinter.tix import Tree
import pandas as pd
import numpy as np
import os
import warnings
import math

from parameters import GasDensity
from read import Chen_2018, IPCC, EEA, Tiquia_Tam, Pardo_2015, VDLUFA, Ecoinvent, Andersen_2010, Boldrin_and_Christensen, Vergara_2019
from read.Ecoinvent import Ecoinvent34
from read.Prices import InputPrice, LandPrice
from tools import convert_molmasses, get_content, convert_unit, prod, O_from_massloss
from material import ConcreteBasePlate
from declarations import Process, Emissions

class WindrowCompostingMachine:
    """
    |  Describes a turning machine that is pulled by a tractor

    :type turning_time_h: int
    :param turning_time_h: The amount of time the machine is used (in hours)

    """
    LIFETIME = 1000 # 1000 hours, assumption made by ecoinvent
    WEIGHT = 2000 # kg rough assumption

    def __init__(self, turning_time_h = None):
        self.TURNING_TIME = turning_time_h

    def emissions(self):
        WEAR = self.TURNING_TIME/self.LIFETIME
        SPECIFIC_WEIGHT_KG = WEAR * self.WEIGHT

        Operation_Emis = Ecoinvent34('machine operation, diesel, >= 18.64 kW and < 74.57 kW, high load factor', unit="h", location="GLO").emissions() * self.TURNING_TIME
        Operation_Emis.index = ["turning machine, diesel consumption"]

        Construction_Emis = Ecoinvent34('agricultural machinery production, unspecified', 'agricultural machinery, unspecified', unit="kg", location="RoW").emissions() * SPECIFIC_WEIGHT_KG
        Construction_Emis.index = ["turning machine, construction"]

        return pd.concat([Operation_Emis, Construction_Emis], sort=False)
    
    def costs(self):

        DIESEL_CONSUMP=4*3.78 #liter per hour #NOTE:Still have to be verified, this consumption is is gallon (4 gallons) per hour for a 40 kW generator fully loaded. 
        COST_DIESEL=InputPrice().diesel()["Q"].item()

        costs=pd.DataFrame(index=[], columns=["EUR"])#EUR/liter
        
        #cost diesel use
        costs.loc["turning machine, diesel consumption", "EUR"]=self.TURNING_TIME*DIESEL_CONSUMP*COST_DIESEL

        #cost personnel
        return costs

class CompostPlant(Process):
    
    """     
    | *The manure_storage class requires a substrate I with the main information Q (mass) and U (unit) and DM- + nutrient content.*
    | *The ``emissions`` as well as the ``output`` method are based on the available literature information - see* ``output`` and/or ``emissions``
    | *Most of the LCA emissions are calculated based upon ecoinvent database entries*
    
    :type type: string  
    :param type: choose between 'windrow' and 'in-vessel'-composting. Yet it mostly affects the LCA emissions for processing/machinery
    :type avg_temp: string  
    :param avg_temp: Currently not much in use...
    :type H2O_content: numeric  
    :param H2O_content: Required H2O content to which water is added to the substrate in the beginning of the process 
    :type time_period: numeric  
    :param time_period: Time period of composting: 0, 35, 91, 133 or 168 days
    :param method: method of how the emissions should be calculated. "default", "Pardo2015", "IPCC2019", "Chen2018&Tiquia2002" or "Chen2018"

    **Note** 

    1. ``self.type == 'windrow'``:

    - ground sealing construction or rather wear emissions are considered (ecoinvent) of a conceivable substructure for the composting type ``windrow``. Therefore also the function ``silo_construction`` from ``tools`` is called.
    - to consider the construction or rather wear emissions from a specific turning machine (assumed to be attached to a wheel loader/tractor), the function ``turning_machine_construction`` is called.
    - since a wheel loader/tractor emissions are considered as external input it is considered in ``compost_plant.input``

    2. ``self.type == 'in-vessel'``:

    - an industrial scale in-vessel composting-plant is considered for this composting type (ecoinvent). Again the emissions concern ground sealing construction or rather wear emissions.
    - further emissions stemming from energy usage within the plant are considerd in ``compost_plant.input``. These are external energy input and wheel loader/tractor (construction/wear and diesel compbustion).
        
    |
    """        

    TiquiaTamComposition = Tiquia_Tam.all_E_manure_managements()
    TURNING_FREQUENCY = 3 # days (for windrow composting only)
    COMPOSTING_SPEED = 350 # 300-400 m/h (for windrow composting only)

    def __init__(self, I = prod("broiler manure", 1, "ton"), type="windrow", H2O_content=0.55, time_period = 35, method ="default", climate="Cool Temperate Moist", id=None):
        # replace this with a generic function, where you can specify how the input has to look like, e.g. units in ton
        
        if any(I["U"] != "ton"):
            warnings.warn("Input to CompostPlant has to be defined in 'ton'")

        self.I = get_content(I, required=["N", "NH4-N", "DM", "oDM", "orgC", "B0", "density"])

        if self.I.index.get_level_values("item").tolist() != ['broiler manure']:
            warnings.warn("currently only 'broiler manure' supported as input")

        if type in ["windrow", "in-vessel"]:
            self.type = type
        else:
            ValueError(f"type is {type}, has to be either 'windrow' or 'in-vessel'")
        
        if time_period in [0, 35, 91, 133, 168]:
            self.time_period = time_period # 0, 35, 91, 133, 168 (days)
        else:
            raise ValueError("time_period has to be one of the following values: 0, 35, 91, 133, 168 days")
        
        self.H2O_content = H2O_content
               
        if method in ["default", "Pardo2015", "IPCC2019", "Chen2018&Tiquia2002", "Chen2018"]:
            self.method = method
        else:
            raise ValueError (f"method is {method}, has to be either 'default', 'Pardo2015', 'IPCC2019' or 'Chen2018&Tiquia2002' or 'Chen2018'")
        
        self.climate = climate
        self.id = id

    def base_plate_size(self):
        """
        |  Base plate size for windrow composting (Values in meters)
        """
        HEAP_HEIGHT = 1.5 # meters
        HEAP_WIDTH = 2.5 # meters
        COMPOST_VOLUME = (self.I["Q"]/self.I["density"]).sum()
        TRIANGLE = 0.5 * HEAP_HEIGHT * HEAP_WIDTH
        PLATE_LENGTH = COMPOST_VOLUME / TRIANGLE # meters
        PLATE_WIDTH = HEAP_WIDTH + 2 # assuming plate to be 2 meters wider than heap
        PLATE_THICKNESS = 0.2 # meters
        return dict(width = PLATE_WIDTH, length = PLATE_LENGTH, thickness = PLATE_THICKNESS)

    def windrow_turning_time(self):
        """
        |  Total operation time in hours to turn the compost (in case of windrow composting) 
        """
        return self.base_plate_size()["length"] * (self.time_period/self.TURNING_FREQUENCY) / self.COMPOSTING_SPEED

    def water_addition(self):
        INITIAL_WATER_TON = (self.I["Q"] * (1-self.I["DM"])).sum()
        INITIAL_DM_TON = (self.I["Q"] * self.I["DM"]).sum()
    
        REQUIRED_WATER_CONT = self.H2O_content
        TOTAL_WATER_TON = INITIAL_DM_TON / (1-REQUIRED_WATER_CONT) * REQUIRED_WATER_CONT
        WATER_ADDITION_TON = TOTAL_WATER_TON - INITIAL_WATER_TON
        
        if WATER_ADDITION_TON < 0:
            warnings.warn("Calculated water addition negative!")
        return WATER_ADDITION_TON

    def water_vaporation(self):
        """
        | Water loss as in Tiquia and Tam 2000
        """
        START_Q_TON = self.I["Q"].sum() + self.water_addition()
        START_WATER_TON = START_Q_TON * self.TiquiaTamComposition.loc["Water","0"].mean()
        END_Q_TON = START_Q_TON * self.TiquiaTamComposition.loc["mass loss",str(self.time_period)].mean()
        END_WATER_TON = END_Q_TON * self.TiquiaTamComposition.loc["Water",str(self.time_period)].mean()
        WATER_VAPORATION_TON = START_WATER_TON - END_WATER_TON
        return WATER_VAPORATION_TON

    def additional_input(self):
        """      
        - Currently this implies external machinery (ecoinvent) as a wheel loader (``self.type == 'windrow'``) or external energy input and wheel loader/tractor for the composting plant (``self.type == 'in-vessel'``)      
        - Also independentl of the LCA flag water is considered as an additional input for composting

        |
        """        
        if self.type == "in-vessel":
            ELECTRICITY_REQUIREMENT = 11.8 # per t, ecoinvent, item: 'treatment of biowaste, industrial composting', 'RoW'                
            LOADING_TIME = 0.352 # in hour, ecoinvent, item: 'treatment of biowaste, industrial composting', 'RoW'

            ACT_LOADING_TIME = LOADING_TIME * self.I['Q'].item()
            ACT_ELECTRICITY = ELECTRICITY_REQUIREMENT * self.I['Q'].item()
            
            # electricity input for compost plant
            Electricity = prod('electricity, low voltage', Q=ACT_ELECTRICITY, U="kWh", location="DE", activity='market for electricity, low voltage', source='external.ExternInput')

            # machinery and diesel input in terms of wheel loader or tractor beeing used in the compost_plant
            Machinery = prod('machine operation, diesel, >= 74.57 kW, low load factor', Q=ACT_LOADING_TIME, U="h", location="GLO", source='external.ExternInput')

            AddInput = pd.concat([Electricity, Machinery])

            return AddInput
        else:
            return None

    def emissions(self):
        """
        | *The ``emissions`` method comprises the calculation of emissions for CH4, CO2, NH3, N2O, NO and N2, providing a method based on Chen et al 2018 and a method based on the IPCC 2006.*
        | *All LCA emissions are described and explained below the direct emissions*.
        |
        """

        Emis = Emissions(index = self.I.index)
        I = self.I.copy()

        # default method largely based on IPCC methodology, but including CO2 emission estimates from Pardo 2015
        # NOTE: Here the default is the combined reporting of NH3&NOx emissions, while it is not in the case of storage
        if self.method == "default":
            CH4_method = "IPCC2019"
            CO2_method = "Pardo2015"
            N2O_method = "IPCC2019"
            NH3_method = 0
            NO_method = 0
            NH3_NOx_method = "IPCC2019"
            N2_method = "IPCC2019"
            H2O_method = "Tiquia2000"
        if self.method == "IPCC2019": # currently same as default!!!
            CH4_method = "IPCC2019"
            CO2_method = "Pardo2015"
            N2O_method = "IPCC2019"
            NH3_method = 0
            NO_method = 0
            NH3_NOx_method = "IPCC2019"
            N2_method = "IPCC2019"
            H2O_method = "Tiquia2000"
        elif self.method == "Pardo2015":
            CH4_method = "Pardo2015"
            CO2_method = "Pardo2015"
            NH3_method = "Pardo2015"
            N2O_method = "Pardo2015"
            NO_method = None
            NH3_NOx_method = 0
            N2_method = "IPCC2019"
            H2O_method = "Tiquia2000"
        elif self.method == "Chen2018&Tiquia2002":
            CH4_method = "Chen2018"
            CO2_method = "Chen2018"
            NH3_method = "Chen2018"
            N2O_method = "Chen2018"
            NO_method = None
            NH3_NOx_method = None
            N2_method = None
            H2O_method = "Tiquia2000"
        elif self.method == "Chen2018":
            CH4_method = "Chen2018"
            CO2_method = "Chen2018"
            NH3_method = "Chen2018"
            N2O_method = "Chen2018"
            NO_method = None
            NH3_NOx_method = None
            N2_method = None
            H2O_method = "Tiquia2000"


        ## CH4 emissions 
        if CH4_method == 0:
            Emis['CH4-C'] = 0
        elif CH4_method == None:
            Emis['CH4-C'] = np.nan
        elif CH4_method == "IPCC2019":
            MCF_tab = IPCC.mcf_manure_managements()/100 # MCF value from  IPCC 2019 Table 10.17 (devision by 100 because values are in %)
            if self.type == "windrow":
                MCF = MCF_tab.loc["Composting, intensive windrow", self.climate]
            elif self.type == "in-vessel":
                MCF = MCF_tab.loc["Composting, in-vessel", self.climate]
            else:
                raise ValueError(f"Currently no existing for method {self.method} and {self.type}.")
            Emis['CH4-C'] = I['mass']*1000*I['DM']*I['oDM']*I['B0']*MCF*GasDensity['CH4']/16*12
        elif CH4_method == "Pardo2015":
            EF_CH4 = Pardo_2015.gas_emissions_solid_waste().loc["Turned",("CH4-C", "Mean (%)")]/100
            Emis['CH4-C'] = I['mass']*1000*I['DM']*I['oDM']*I['orgC']*EF_CH4
        elif CH4_method == "Chen2018":
            EF_CH4 = Chen_2018.all_abs_manure_managements().loc["CK", "CH4-C"]/100
            Emis['CH4-C'] = I['mass']*1000*I['DM']*I['oDM']*I['orgC']*EF_CH4
        elif CH4_method == None:
            Emis['CH4-C'] = 0
        else:
            raise ValueError(f"CH4_method {CH4_method} not defined")

        ## CO2 emissions
        if CO2_method == 0:
            Emis['CO2-C'] = 0
        elif CO2_method == None:
            Emis['CO2-C'] = np.nan
        elif CO2_method == "Pardo2015":
            EF_CO2 = Pardo_2015.gas_emissions_solid_waste().loc["Turned",("CO2-C", "Mean (%)")]/100
            Emis['CO2-C'] = I['mass']*1000*I['DM']*I['oDM']*I['orgC']*EF_CO2
        elif CO2_method == "Chen2018":
            CH4_LOSS = Chen_2018.all_abs_manure_managements().loc["CK", "CH4-C"]/100
            TOTAL_C_LOSS = Chen_2018.all_abs_manure_managements().loc["CK", "Total C losses"]/100
            # CO2-C emissions assumed as residual
            EF_CO2 = TOTAL_C_LOSS - CH4_LOSS
            Emis['CO2-C'] = I['mass']*1000*I['DM']*I['oDM']*I['orgC']*EF_CO2
        else:
            raise ValueError(f"CO2_method {CO2_method} not defined")

        ## N2O emissions
        if N2O_method == 0:
            Emis['N2O-N'] = 0
        elif N2O_method == None:
            Emis['N2O-N'] = np.nan
        elif N2O_method == "IPCC2019":
            N2O_N_tab = IPCC.N2O_EF_manure_management() # Proportion of N
            if self.type == "windrow":
                EF_N2O = N2O_N_tab.loc["Composting, intensive windrow (frequent turning)","EF"]
            elif self.type == "in-vessel":
                EF_N2O = N2O_N_tab.loc["Composting, in-vessel","EF"]
            Emis['N2O-N'] = I['mass']*I['N']*EF_N2O
        elif N2O_method == "Pardo2015":
            EF_N2ON = Pardo_2015.gas_emissions_solid_waste().loc["Turned", ("N2O-N", "Mean (%)")]/100
            Emis['N2O-N'] = I['mass']*I['N']*EF_N2ON
        elif N2O_method == "Chen2018":
            EF_N2ON = Chen_2018.all_abs_manure_managements().loc["CK", "N2O-N"]/100
            Emis['N2O-N'] = I['mass']*I['N']*EF_N2ON
        else:
            raise ValueError(f"N2O_method {N2O_method} not defined")

        ## NH3 emissions
        if NH3_method == 0:
            Emis['NH3-N'] = 0
        elif NH3_method == None:
            Emis['NH3-N'] = np.nan
        elif NH3_method == "Pardo2015":
            EF_NH3N = Pardo_2015.gas_emissions_solid_waste().loc["Turned", ("NH3-N", "Mean (%)")]/100
            Emis['NH3-N'] = I['mass']*I['N']*EF_NH3N
        elif NH3_method == "Chen2018":
            EF_NH3N = Chen_2018.all_abs_manure_managements().loc["CK", "NH3-N"]/100
            Emis['NH3-N'] = I['mass']*I['N']*EF_NH3N
        else:
            raise ValueError(f"NH3_method {NH3_method} not defined")

        ## NO emissions
        if NO_method == 0:
            Emis['NO-N'] = 0
        elif NO_method == None:
            Emis['NO-N'] = np.nan
        elif NO_method == "Haenel2018":
            # Similar to the storage assumption here assumed that the NO-N emission factor is one tenth of the N2O-N emission factor."
            Emis['NO-N'] = Emis['N2O-N']*0.1
        else:
            raise ValueError(f"NO_method {NO_method} not defined")

        ## N2 emissions
        if N2_method == 0:
            Emis['N2-N'] = 0
        elif N2_method == None:
            Emis['N2-N'] = np.nan
        elif N2_method == "IPCC2019":
            # Same assumption as for storage
            # 10.104: "Webb & Misselbrook (2004) reviewed available data and concluded that as first approximation, emissions of N2 might be 3-times those of N2O. [3 kg N2-N/kg N2O-N]"
            Emis['N2-N'] = Emis['N2O-N']*3
        else:
            raise ValueError(f"N2_method {N2_method} not defined")

        ## aggregated losses of NH3 and NOx (only reported in sum by IPCC2019 method)
        # these are the total losses of manure management (should include emissions from spreading, where much of the volotalization happens)
        # from table 10.22 (better read in table):
        if NH3_NOx_method == 0:
            Emis['NH3-N & NOx-N'] = 0
        elif NH3_NOx_method == None:
            Emis['NH3-N & NOx-N'] = np.nan
        elif NH3_NOx_method == "IPCC2019":
            if self.type == "windrow":
                EF_NH3_NOx = 0.65 # for poultry and intensive windrow
            elif self.type == "in-vessel":
                EF_NH3_NOx = 0.60 # for poultry and in-vessel composting
            Emis['NH3-N & NOx-N'] = I['mass']*I['N']*EF_NH3_NOx
            # NH3-N and NO-N set to 0 to avoid double accounting
            Emis['NH3-N'] = 0
            Emis['NO-N'] = 0
        else:
            raise ValueError(f"NH3_NOx_method {NH3_NOx_method} not defined")

        # emissions of H2O as water vapor
        if H2O_method == "Tiquia2000":
            H2O_EMIS_TON = self.water_vaporation()*1000
            Emis.loc[I.index,'H2O'] = H2O_EMIS_TON
        else:
            raise ValueError(f"H2O_method {H2O_method} not defined")


        ## emissions for machinery usage + and infrastructure
        # windrow composting assumed to be on farm with specific machinery and ground below sealed with concrete plates to avoid leakage etc.
        # emissions assumed for an average compost turning machine (details of potential machinery from http://www.gujerinnotec.com/compost-turners.html)      

        if self.type == "windrow":
            # Emissions for a concreate base plate
            Emis = Emis.append(ConcreteBasePlate(**self.base_plate_size(), use_days=self.time_period, lifetime_years=25).emissions(), sort=False)
            
            # Emissions for windrow composting machine construction and usage
            Emis = Emis.append(WindrowCompostingMachine(turning_time_h=self.windrow_turning_time()).emissions(), sort=False)


        # not yet defined if in-vessel composting facility is to be represented within emissions or input
        elif self.type == "in-vessel":
            # emissions assumed for the in-vessel system/site described in ecoinveent: https://v34.ecoquery.ecoinvent.org/Details/PDF/7b3983d2-419e-4557-8bbd-156f2f0ebead/290c1f85-4cc4-4fa1-b0c8-2cb7f4276dce

            # emissions construction/wear
            PROCESSING_PER_YEAR = 10000 # t, assumption made by ecoinvent item: 'composting facility construction, open', 'RoW' 
            LONGEVITY = 25 # years, assumption made by ecoinvent item: 'composting facility construction, open', 'RoW' 
            # as part of the year
            # usage_per_year = (float(time.split(" ",1)[0])/12)
            usage_per_year = self.time_period/365
            # related to the time of usage -> capacity per time
            act_processing = PROCESSING_PER_YEAR * usage_per_year
            # material related to the capacity
            material_utilization = I['Q'].sum()/act_processing
            # time of usage related to longevity of plant
            time_utilization = usage_per_year/LONGEVITY
            # results in specific factor which is used to scale ecoinvent data
            specific_factor = material_utilization * time_utilization

            EmisConstruction = specific_factor * Ecoinvent34("composting facility construction, open", "composting facility, open", unit="unit", location="RoW").emissions()
            EmisConstruction.index = ["composting facility construction"]
            Emis = Emis.append(EmisConstruction)

        else:
            raise ValueError(f"type {self.type} not defined.")

        Emis = Emis.sort_index(axis=1)

        return Emis

    def output(self):
        I = self.I.copy()

        if self.method in ["default", "IPCC2019", "Chen2018"]:
            # sum up specific mass losses by emissions
            Emis = self.emissions().copy()
            
            if self.method in ["default", "IPCC2019"]:
                # using I.index to select emissions caused by the input (not by other processes)
                orgC_LOSS_TON = Emis.loc[I.index,["CH4-C","CO2-C"]].sum().sum() / 1000
                N_LOSS_TON = Emis.loc[I.index,["NH3-N","N2O-N","NO-N","N2-N","NH3-N & NOx-N"]].sum().sum()/1000 # in kg since N content is in kg/ton

            elif self.method == "Chen2018":
                orgC_LOSS_TON = (I["mass"]*I["DM"]*I["oDM"]*I["orgC"]).sum()*Chen_2018.all_abs_manure_managements().loc["CK", "Total C losses"]/100
                N_LOSS_TON = (I["mass"]*I["N"]).sum()/1000*Chen_2018.all_abs_manure_managements().loc["CK", "Total N losses"]/100

            H2O_LOSS_TON = self.water_vaporation()-self.water_addition()

            Output = O_from_massloss(I, H2O_LOSS_TON=H2O_LOSS_TON, orgC_LOSS_TON=orgC_LOSS_TON, N_LOSS_TON=N_LOSS_TON, scale_by_mass=["P", "K"], index=I.index)

            # relative NH4-N change as in Tiquia & Tam papers
            TiquiaTamComposition2 = self.TiquiaTamComposition.drop(columns=["Unit", "Pile Locations"])
            Tiquia_Tam_Adjust = TiquiaTamComposition2.loc["NH4-N",:]*TiquiaTamComposition2.loc["mass loss",:]
            NH4N_REL = (Tiquia_Tam_Adjust.loc["NH4-N",str(self.time_period)] / Tiquia_Tam_Adjust.loc["NH4-N","0"]).mean()
            Output["NH4-N"] = I.loc[:,"Q"]* I.loc[:,"NH4-N"] * NH4N_REL / Output["Q"]

        elif self.method == "Pardo2015":
            Pardo = Pardo_2015.gas_emissions_solid_waste()/100

            ## orgC
            orgC_LOSS_REL = Pardo.loc["Turned",[("CO2-C", "Mean (%)"),("CH4-C", "Mean (%)")]].sum()
            orgC_IN_TON = (I["mass"]*I["DM"]*I["oDM"]*I["orgC"]).sum()
            orgC_LOSS_TON = orgC_IN_TON * orgC_LOSS_REL
 
            ## total N
            N_LOSS_REL = Pardo.loc["Turned",("Total N", "Mean (%)")]
            N_IN_TON = (I["mass"]*I["N"]/1000).sum()
            N_LOSS_TON = N_IN_TON * N_LOSS_REL

            ## NH3
            NH3_LOSS_REL = Pardo.loc["Turned",("NH3-N", "Mean (%)")]
            NH3_IN_TON = (I["mass"]*I["N"]/1000).sum()
            NH3_LOSS_TON = NH3_IN_TON * NH3_LOSS_REL

            # water
            H2O_LOSS_TON = self.water_vaporation()-self.water_addition()

            Output = O_from_massloss(I, H2O_LOSS_TON=H2O_LOSS_TON, orgC_LOSS_TON=orgC_LOSS_TON, N_LOSS_TON=N_LOSS_TON, NH4N_LOSS_TON=NH3_LOSS_TON, scale_by_mass=["P", "K"], index=I.index)

        # NOTE: Not used in modelling for paper now
        elif self.method == "Chen2018&Tiquia2002":
            # changes in broiler manure composition parameterized according to the publication:
            # Tiquia, S.M., Tam, N.F.Y., 2000. Fate of nitrogen during composting of chicken litter. Environ. Pollut. 110, 535–541. https://doi.org/10.1016/S0269-7491(99)00319-X
            
            Output = I.copy()

            ## calculate output Q
            # According to table 2 of Tiquia and Tam 2000 the relative massloss the same as the organic matter loss

            DM_LOSS_TON = (I["Q"]*I["DM"]).sum()*(1-self.TiquiaTamComposition.loc["mass loss",str(self.time_period)])
            H2O_LOSS_TON = self.water_vaporation()-self.water_addition()

            Output.loc[:,["Q","mass"]] = I["Q"].sum() - H2O_LOSS_TON - DM_LOSS_TON

            Output.loc[:, "DM"] = ((I.loc[:,"Q"]* I.loc[:,"DM"]).sum() - DM_LOSS_TON) / Output.loc[:,"Q"]

            # adjust values to fresh matter
            # Table 4 of Tiquia and Tam 2002 reports values relative to dry matter. Since here the loss compared to fresh matter is relevant, these values have to be adjusted with total mass loss
            TiquiaTamComposition2 = self.TiquiaTamComposition.drop(columns=["Unit", "Pile Locations"])
            Tiquia_Tam_Adjust = TiquiaTamComposition2.loc[["Total N", "NH4-N", "Total C", "Total P", "Total K", "OM"],:]*TiquiaTamComposition2.loc["mass loss",:]

            # assume same relative changes in contents:
            oDM_REL = (Tiquia_Tam_Adjust.loc["OM",str(self.time_period)] / Tiquia_Tam_Adjust.loc["OM","0"]).mean()
            Output["oDM"] = I.loc[:,"Q"]* I.loc[:,"DM"] * I.loc[:,"oDM"] * oDM_REL / (Output.loc[:,"Q"] * Output.loc[:,"DM"])

            orgC_REL = (Tiquia_Tam_Adjust.loc["Total C",str(self.time_period)] / Tiquia_Tam_Adjust.loc["Total C","0"]).mean()
            Output["orgC"] = I.loc[:,"Q"]* I.loc[:,"DM"] * I.loc[:,"oDM"] * I.loc[:,"orgC"] * orgC_REL / (Output.loc[:,"Q"] * Output.loc[:,"DM"] * Output.loc[:,"oDM"])

            N_REL = (Tiquia_Tam_Adjust.loc["Total N",str(self.time_period)] / Tiquia_Tam_Adjust.loc["Total N","0"]).mean()
            Output["N"] = I.loc[:,"Q"]* I.loc[:,"N"] * N_REL / Output["Q"]

            NH4N_REL = (Tiquia_Tam_Adjust.loc["NH4-N",str(self.time_period)] / Tiquia_Tam_Adjust.loc["NH4-N","0"]).mean()
            Output["NH4-N"] = I.loc[:,"Q"]* I.loc[:,"NH4-N"] * NH4N_REL / Output["Q"]

            # for P and K adjust simply to new mass
            Output["K"] = I.loc[:,"Q"]* I.loc[:,"K"] / Output["Q"]
            Output["P"] = I.loc[:,"Q"]* I.loc[:,"P"] / Output["Q"]

        else:
            raise ValueError(f"method {self.method} not defined.")

        ## density
        # irrespective of method assume the density changes as in Tiquia Tam:
        DENSITY_START = self.TiquiaTamComposition.loc["bulk density", "0"].mean()
        DENSITY_END = self.TiquiaTamComposition.loc["bulk density", str(self.time_period)].mean()
        REL_DENSITY = DENSITY_END/DENSITY_START

        Output["density"] = I["density"] * REL_DENSITY

        # humus reproduction according to VDLUFA
        Output["HEQ"] = VDLUFA.humus_reproduction().loc["manure composted", "HEQ"]

        # N fertilization efficacy assumed to remain unchanged.
        Output["N_fert_eff"] = I["N_fert_eff"]

        Output = Output.rename(index = {'broiler manure': 'broiler manure composted'})
        Output = Output.sort_index(axis=1)

        return Output


class CompostingLeaves(Process):
    """
    |   The CompostingLeaves class is designed for composting tree leaves. It is based on a one-year composting results.
    |   The type of composting considered is 'windrow'

    :type method: str
    :param method: Research publication with specific C-org balance of CO2 emissions rate. 
    :type CH4_emis: float
    :param CH4_emis: Fraction of carbon emissions released as CH4
    :type land_purchase: boolean
    :param land_purchase: if true, land purchase is considered in the cashflow in the CAPEX section
    :type land_rent: boolean
    :param land_rent: if true, rent of land is considered in the cashflow in the OPEX section
    :type fixed_operation_cost: str
    :param fixed_operation_cost: when true, operation cost are totalized based on the output (compost production)
    :type new_infraestructure: boolean
    :param new_infraestructure: when true, new infrastructure is considered in the cashflow calculations
    :type lifetime: int
    :param lifetime: lifetime of the infrastructure  
    """
    
    def __init__(self, I = prod("tree leaves", 1, "ton"), method="Andersen_2010", CH4_emis=0.02, land_purchase=True, land_rent=False, 
                fixed_operation_cost=True, new_infrastructure=True, lifetime=20, id=None):
        if any(I["U"] != "ton"):
            warnings.warn("Input to CompostingLeaves has to be defined in 'ton'")
        self.I = I
        self.method = method
        if self.method not in ["Andersen_2010", "Vergara_2019"]:
            warnings.warn("method of GHG emissions has to be 'Andersen_2010' or 'Vergara_2010'")
        self.CH4_emis=CH4_emis
        self.land_purchase=land_purchase 
        self.land_rent=land_rent
        self.fixed_operation_cost=fixed_operation_cost
        self.new_infrastructure=new_infrastructure
        self.lifetime=lifetime
        self.id = id
        #NOTE: include lifetime

    def windrow_specifications(self):
        
        LEAVES_DENSITY=0.6
        MASS_LEAVES=self.I["Q"].item()
        VOLUME_LEAVES=MASS_LEAVES/LEAVES_DENSITY
        
        windrow_specifications=pd.Series(index=[])
        #NOTE: measures are  given in meters, areas in square meters, volume in cubic meters
        windrow_specifications.loc["long"]=100
        windrow_specifications.loc["wide"]=4
        windrow_specifications.loc["height"]=1.8
        windrow_specifications.loc["volume"]=(windrow_specifications.loc["long"]*windrow_specifications.loc["wide"]
                                                *windrow_specifications.loc["height"]*0.5)
        windrow_specifications.loc["number windrows"]=math.ceil(VOLUME_LEAVES/ windrow_specifications.loc["volume"])
        windrow_specifications.loc["area windrows"]=(windrow_specifications.loc["long"]*windrow_specifications.loc["wide"]*
                                                    windrow_specifications.loc["number windrows"])
        #windrow_specifications.loc["area facilities"]=(windrow_specifications.loc["area windrows"]*2*1.1) #in m2 #twice the area of windrows + 10% for offices.

        return windrow_specifications

    def area_specifications(self):
        windrow_specifications=self.windrow_specifications()
        #the break down of space demand according to Bundesamt (2015)
        area_specifications=pd.DataFrame(index=[], columns=[])
        area_specifications.loc["rotting area", "Q"]=windrow_specifications.loc["area windrows"]
        area_specifications.loc["total rotting area", "Q"]=windrow_specifications.loc["area windrows"]*(1+40/60) #windrows=60%, area for movement=40%
        area_specifications.loc["temporary storage", "Q"]=area_specifications.loc["total rotting area", "Q"]*(10/75)
        area_specifications.loc["compost storage area", "Q"]=area_specifications.loc["total rotting area", "Q"]*(10/75)
        area_specifications.loc["receipt area", "Q"]=area_specifications.loc["total rotting area", "Q"]*(5/75)
        area_specifications.loc["total area", "Q"]=area_specifications["Q"].sum()
        area_specifications["U"]="m2"

        return area_specifications

    def windrow_turning_time(self):
        """
        The windrow turning time for composting leaves will depend on the dimensions of the windrow,
        the density of leaves (ton/m³) and the speed of the windrow turning machine. A standard windrow of 
        100m*4m*1.8m is proposed, with a speed of 1500 m/h and 5 times of turning per year.
        """
        windrow_specifications=self.windrow_specifications()

        SPEED_WINDROW_MACHINE= 1500
        TIMES_PER_YEAR=5
        LENGHT_WINDROW=windrow_specifications.loc["number windrows"]*windrow_specifications.loc["long"]
        HOURS_OPERATION=LENGHT_WINDROW/SPEED_WINDROW_MACHINE
        TOTAL_TURNING_TIME=HOURS_OPERATION*TIMES_PER_YEAR
        
        return TOTAL_TURNING_TIME
    
    def emissions(self): 

        #assuming a default 10000 tonnes input
        if self.new_infrastructure==True:
            specific_factor=self.I["Q"].item()/10000                
            EmisConstruction = specific_factor * Ecoinvent34("composting facility construction, open", "composting facility, open", unit="unit", location="RoW").emissions()/self.lifetime
            EmisConstruction.index = ["composting facility construction"]
        else:
            EmisConstruction=pd.DataFrame(index=[], columns=[])
        
        Emis_windrow_machine=WindrowCompostingMachine(turning_time_h=self.windrow_turning_time()).emissions()

        comp_input=self.I.copy()

        if self.method=="Andersen_2010": #for garden waste (leaves, branches, sticks, shrubs)
            
            orgC_loss=comp_input["Q"].item()*comp_input["DM"].item()*comp_input["oDM"].item()*comp_input["orgC"].item()*0.65 #assuming 65% loss of carbon according to Andersen et al. 2010

            Emis_composting = Emissions(index=["leaves composting"], columns=["CO2-C", "CH4-C", "N2O-N", "NH3-N"])
            Emis_composting.loc["leaves composting","CO2-C"]=orgC_loss*(1-self.CH4_emis)*1000 # 98% as CO2-C, according to Andersen et al. 2010
            Emis_composting.loc["leaves composting","CH4-C"]=orgC_loss*self.CH4_emis*1000 # 2% as CH4-C, according to Andersen et al. 2010
            Emis_composting.loc["leaves composting","N2O-N"] = Andersen_2010.Table5().loc["N2O-N","Emission"] * self.I["Q"].item()
            Emis_composting.loc["leaves composting","NH3-N"] = self.I["N"].item()*self.I["Q"].item()*0.068 #6.8% of N input according to Andersen et al. 2010 (Mass balance paper)

        elif self.method=="Vergara_2019": #Vergara & Silver (2019) express the emissions in g of gas/kg of feedstock(ww) = kg gas/ ton of feedstock(ww)
            # -->IMPORTANT: The emissions rate given by Vergara & Silver (2019) are for a mix of manure (91%) and green waste (9%) for a period of 14 weeks=98 days ~ 3 months
            # -->Therefore, emissions were recalculated to a daily basis and reset for 365 days = 1 year 
            Emis_composting = Emissions(index=["leaves composting"], columns=["CO2", "CH4", "N2O"])
            # Emis_composting.loc["leaves composting","CO2"] = Vergara_2019.Page5().loc["CO2","Emission"]/98*365 * self.I["Q"].item()
            # Emis_composting.loc["leaves composting","CH4"] = Vergara_2019.Page5().loc["CH4","Emission"]/98*365 * self.I["Q"].item()
            Emis_composting.loc["leaves composting","CO2"] = Vergara_2019.Page5().loc["CO2","Emission"] * self.I["Q"].item()
            Emis_composting.loc["leaves composting","CH4"] = Vergara_2019.Page5().loc["CH4","Emission"] * self.I["Q"].item()
            #N2O emissions were below of the sensor detection system, hence is assumed zero.
            Emis_composting=convert_molmasses(Emis_composting, to="element") 

        Emis = pd.concat([EmisConstruction,Emis_windrow_machine,Emis_composting])
        return  Emis

    def output(self): 
        Output = prod("leaves compost")

        Output=pd.DataFrame(index=["leaves compost"], columns=["Q", "U", "DM", "oDM", "B0", "CH4 content", "orgC","P", "K", "N", "HEQ"])

        #Mass loss calculation
        if self.method=="Andersen_2010":
            REMAIN_MASS = Andersen_2010.Table1().loc["Material", "Output"]/Andersen_2010.Table1().loc["Material", "Input"]
        elif self.method=="Vergara_2019":
            REMAIN_MASS=0.5
        Output.loc["leaves compost","Q"]= self.I["Q"].item() * REMAIN_MASS

        #Unit "tons" remain the same
        Output.loc["leaves compost","U"]=self.I["U"].item()

        #Dry matter concentration change
        REMAIN_DRY_MATTER = (Andersen_2010.Table1().loc["DM", "Output"]/Andersen_2010.Table1().loc["Material", "Output"]) / (Andersen_2010.Table1().loc["DM", "Input"]/Andersen_2010.Table1().loc["Material", "Input"]) #13.7% concentration increase according to Andersen et al. 2010
        Output.loc["leaves compost","DM"]=self.I["DM"].item() * REMAIN_DRY_MATTER

        #Organic Matter concentration change
        REMAIN_ORGANIC_MATTER = (Andersen_2010.Table1().loc["Total oDM", "Output"]/Andersen_2010.Table1().loc["DM", "Output"]) / (Andersen_2010.Table1().loc["Total oDM", "Input"]/Andersen_2010.Table1().loc["DM", "Input"]) #52.5% dry organic matter loss according to Andersen et al. 2010
        Output.loc["leaves compost","oDM"]=self.I["oDM"].item() * REMAIN_ORGANIC_MATTER

        #Assuming no changes in B0
        Output.loc["leaves compost","B0"]=self.I["B0"].item()

        #Assumning no changes in CH4 content
        Output.loc["leaves compost","CH4 content"]=self.I["CH4 content"].item()

        #Nitrogen concentration change
        REMAIN_NITROGEN = (Andersen_2010.Table1().loc["Total N", "Output"]/Andersen_2010.Table1().loc["Material", "Output"]) / (Andersen_2010.Table1().loc["Total N", "Input"]/Andersen_2010.Table1().loc["Material", "Input"]) #35% nitrogen concentration increase according to Andersen et al. 2010
        Output.loc["leaves compost","N"]=self.I["N"].item() * REMAIN_NITROGEN

        #Phosphorus concentration change
        REMAIN_PHOSPHORUS = (Boldrin_and_Christensen.Table3().loc["P","November"]*Boldrin_and_Christensen.Table3().loc["DM","November"]/10)/(Boldrin_and_Christensen.Table3().loc["P","January"]*Boldrin_and_Christensen.Table3().loc["DM","January"]/10) # according to Boldrin & Christensen, 2010. 
        Output.loc["leaves compost","P"]=self.I["P"].item() * REMAIN_PHOSPHORUS

        #Potasium concentration change
        REMAIN_POTASIUM = (Boldrin_and_Christensen.Table3().loc["K","November"]*Boldrin_and_Christensen.Table3().loc["DM","November"]/10)/(Boldrin_and_Christensen.Table3().loc["K","January"]*Boldrin_and_Christensen.Table3().loc["DM","January"]/10) # according to Boldrin & Christensen, 2010.
        Output.loc["leaves compost","K"]=self.I["K"].item() * REMAIN_POTASIUM

        #Organic carbon loss
        REMAIN_ORGANIC_CARBON = (Andersen_2010.Table1().loc["Total C", "Output"]/Andersen_2010.Table1().loc["Total oDM", "Output"]) / (Andersen_2010.Table1().loc["Total C", "Input"]/Andersen_2010.Table1().loc["Total oDM", "Input"]) #45.9% carbon loss according to Andersen et al. 2010
        Output.loc["leaves compost","orgC"]=self.I["orgC"].item() * REMAIN_ORGANIC_CARBON

        #Humus production ratio
        Output.loc["leaves compost","HEQ"]= VDLUFA.humus_reproduction().loc["bio waste composted (grade 4-5)", "HEQ"]

        return Output

    def cashflow(self):
        """
        |  All cashflows for invesment and operation on composting
        |  **NOTE:**
        |  - These costs are not yet related to the production quantities.
        """
        #Definitions
        area_specifications=self.area_specifications()
        output=self.output().copy()
        comp_input=self.I.copy()

        ##CAPEX    
        #Information from installation and infraestructre retrived from Umwelt Bundesamt (2015) p.8 (https://www.cleaner-production.de/images/BestPractice/data_en/COM.pdf)
        #NOTE: There is room for interpretation on that publication whether the cost rates are for input or for throughput(compost produced), 
        #at the moment costs are modelled in terms of output
        CONSTRUCTIONAL_ELEMENTS=80 # range of 70-100 EUR/Mg*a
        PAVEMENT_COST=30 #range of 20-45 EUR/m2
        MACHINERY_INVESTMENT=120 #range 110-140 EUR/tonne (tonnes expected to be processed in one year)

        Cashflow_capex = pd.DataFrame(index=[], columns=[])
        if self.land_purchase==True:
            Cashflow_capex.loc["land purchase", "EUR"]=area_specifications.loc["total area","Q"]*LandPrice().purchase()["Q"].item() #default method delivers price in EUR/m2
        else: 
            Cashflow_capex.loc["land purchase", "EUR"]=1  #to avoid 'float division by zero' error
        
        if self.new_infrastructure==True:
            Cashflow_capex.loc["installation and infrastructure", "EUR"]=(CONSTRUCTIONAL_ELEMENTS*output["Q"].item() + PAVEMENT_COST*area_specifications.loc["total area", "Q"])
            Cashflow_capex.loc["machinery investment"]=MACHINERY_INVESTMENT*output["Q"].item()
        else: 
            Cashflow_capex.loc["installation and infraestructure", "EUR"]=1  #to avoid 'float division by zero' error

        Cashflow_capex["period_years"] = 20
        Cashflow_capex["section"]="capex"

        ##OPEX
        #Operation parameters
        WINDROW_DIESEL_CONSUMP= 25  #consumption in liter/hour https://www.eggersmann-recyclingtechnology.com/fileadmin/media/Broschueren/20220519_Broschuere_Umsetzen_A4_3_EN.pdf? and http://uatrident.com/wp-content/uploads/2019/11/tad552ve_technical_data.22480116.09.pdf 
        COST_DIESEL=InputPrice().diesel()["Q"].item()
        PRICE_COMPOST= 14 #EUR/m3 source: https://www.galle-gmbh.de/verkauf/
        COMPOST_DENSITY= 0.5 #ton/m3
        FOREMEN_SALARY=50000 #EUR/year. Salary for a "Vorarbeiter" taken from https://www.gehalt.de/beruf/vorarbeiter-vorarbeiterin
        OPERATION_STAFF_SALARY= 30000 #EUR/year. Salary for "Produktionsmitarbeiter" https://www.steuerklassen.com/gehalt/produktionsmitarbeiter/
        GATE_FEE=45 #EUR/tonne of biowaste (Kühner, 2013)

        Cashflow_opex = pd.DataFrame(index=[], columns=[])
        if self.land_rent==True:
            Cashflow_opex.loc["land rent", "EUR"]=area_specifications.loc["total area","Q"]*LandPrice().rent()["Q"].item()
        else:
            pass
        
        #NOTE: fixed operation prices are considered as part of sensibitiy analysis. Rates from Umwelt Bundesamt (2015) p.8
        if self.fixed_operation_cost==True:
            Cashflow_opex.loc["operation - maintenance - personnel", "EUR"]= 35 * output["Q"].item()
        else:       
            #NOTE: for additional input is just considered fuel (diesel) costs at the moment
            Cashflow_opex.loc["fuel consumption turning machine", "EUR"]=self.windrow_turning_time()*WINDROW_DIESEL_CONSUMP*COST_DIESEL #from turning machine
            Cashflow_opex.loc["maintenance", "EUR"]=Cashflow_capex.loc["machinery investment", "EUR"]*0.15 #15% for machinery, electronics, structural elements
            #NOTE: Umwelt Bundesamt (2015) suggests that por avarge plants (between 3000 to 10000 tonnes of output per year) 1 foreman and 7 operation staff would be needed.
            #Based on this approach, quantities greater than 10000 tons of compost output would increse progresively in staff demand
            if output["Q"].item()<=10000:
                Cashflow_opex.loc["personnel", "EUR"]=1*FOREMEN_SALARY+7*OPERATION_STAFF_SALARY
            else:
                Cashflow_opex.loc["personnel", "EUR"]=(output["Q"].item()/10000)*(1*FOREMEN_SALARY+7*OPERATION_STAFF_SALARY)
        
        Cashflow_opex.loc["feedstock purchase", "EUR"]=0 #NOTE: In case purchase of feedstock in considered, this section should be adapted
    
        #revenues
        Cashflow_opex.loc["compost sales", "EUR"]= (PRICE_COMPOST / COMPOST_DENSITY) * output.loc["leaves compost", "Q"] * -1
        Cashflow_opex.loc["gate fee", "EUR"]= GATE_FEE * comp_input["Q"].item() * -1
        Cashflow_opex["period_years"] = 1
        Cashflow_opex["section"]="opex"
        
        return pd.concat([Cashflow_capex, Cashflow_opex])