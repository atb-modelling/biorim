import pandas as pd
import numpy as np
import math
from declarations import Emissions, Process, Product
from tools import prod, convert_molmasses, LHV_correction
from read.Ecoinvent import Ecoinvent34
from read.Prices import LandPrice, InputPrice
from read import GHG_protocol

class BiomassDrying(Process):

    VAPORIZATION_HEAT_WATER =  2260 #kJ/kg
    KJ_TO_KWH = 0.0002778 #kWh/kJ
    MJ_TO_KWH = 0.27778 #kWh/MJ
    GJ_TO_KWH = 277.778

    def __init__(self, I=prod("tree leaves", 1, "ton"), type="rotary drum dryer", method="Haque", final_moisture=0.1, thermal_efficiency=0.7,
                  new_infrastructure=True, id=None):
        self.I=I
        self.final_moisture=final_moisture
        self.method=method
        self.type=type
        self.thermal_efficiency=thermal_efficiency #more info: https://tridenttnz.com/documents/cutsheets/Trident-Rotary-Drum-Dryer-Cutsheet.pdf
        self.new_infrastructure=new_infrastructure #at the moment assumed to be for machinery investment
        self.id=id

        #NOTE: Information about the dryers: https://www.buettner-energy-dryer.com/en/industrial-dryer-systems/
        #NOTE: Although this link is good for knowning, not technical specifications like fuel consumption are expressed. Also, no prices detailed


    def energy_input(self):

        """
        Energy requiered for the biomass drying process
        """
    
        biomass_input=self.I.copy()

        energy_input=pd.DataFrame(index=[], columns=[])
        water_amount=biomass_input["Q"][0]*((1-self.final_moisture)-biomass_input["DM"][0])
        if self.method=="vaporization heat":
            if biomass_input["U"][0]=="ton":
                energy_input.loc["energy input drying", "Q"]=water_amount*1000*self.VAPORIZATION_HEAT_WATER*self.KJ_TO_KWH
                energy_input.loc["energy input drying", "U"]="kWh"
            elif biomass_input["U"][0]=="kg":
                energy_input.loc["energy input drying", "Q"]=water_amount*self.VAPORIZATION_HEAT_WATER*self.KJ_TO_KWH
                energy_input.loc["energy input drying", "U"]="kWh"
            else:
                raise ValueError(f"Units must be 'ton' or 'kg'")
        
        elif self.method=="Haque":
            HEAT_RATE=1.65 #GJ/t, according to Haque and Somerville (2013)
            if biomass_input["U"][0]=="ton":
                energy_input.loc["energy input drying", "Q"]=water_amount*HEAT_RATE*self.GJ_TO_KWH
                energy_input.loc["energy input drying", "U"]="kWh"
            elif biomass_input["U"][0]=="kg":
                energy_input.loc["energy input drying", "Q"]=water_amount*HEAT_RATE*self.GJ_TO_KWH/1000
                energy_input.loc["energy input drying", "U"]="kWh"

        return energy_input     

    def output(self):
        biomass_input=self.I.copy()

        #Water vapor is calculated here por determining the amount of dry biomass, but reported as emission within the 'emisions' function
        water_vapour=pd.DataFrame(index=["water vapor"], columns=["Q","U"])
        water_vapour["Q"][0]=((1-self.final_moisture)-biomass_input["DM"][0])*biomass_input["Q"][0] #assuming all water will be released in water vapour
        water_vapour["U"][0]=biomass_input["U"][0]

        #dry biomass
        biomass_dry=biomass_input.rename(index={f"{biomass_input.index[0]}": f"{biomass_input.index[0]} dry"})
        biomass_dry["Q"]=biomass_input["Q"][0]-water_vapour["Q"][0]
        biomass_dry["DM"][0]=(1-self.final_moisture)
        #no changes in oDM and orgC, since they are expresed at a DM basis
        #changes in nutrient concentration
        for nutrient in ["N", "P", "K"]:
            biomass_dry[nutrient][0]=biomass_input["Q"][0]*biomass_input[nutrient][0]/biomass_input["Q"][0]
        
        #changes in ash concentration, as ash content is based on fresh basis
        total_ash=biomass_input["Q"][0]*biomass_input["ash content"][0]
        biomass_dry["ash content"]=total_ash/biomass_dry["Q"]

        #LHV correction 
        DM=biomass_dry["DM"][0]
        biomass_dry=LHV_correction(biomass_dry, H=5.4, DM=DM)
        
        output=pd.concat([biomass_dry])
        return output

    def fuel_required(self):
        """
        Fuel required for the energy input required. At the moment set for diesel
        """
        biomass_input=self.I.copy()
        LHV_DIESEL=11.83 #kWh/kg
        DIESEL_DENSITY=0.83 #kg/liter

        if self.type=="rotary drum dryer": 
            FUEL_REQUIRED = self.energy_input()["Q"][0] * self.thermal_efficiency / LHV_DIESEL # in kg

        return FUEL_REQUIRED 
    
    def additional_input(self):
        Input=Product()
        #dry_biomass=self.output()
        
        if self.type=="rotary drum dryer": 
            #info for biomass dryers: https://www.jtdryer.com/product/biomass/52.html, last row (model JTSG2912/3)
            CAPACITY_DRYER=12.14 #tonne biomass/hour
            POWER_DRYER=155 #kW
            ENERGY_DRYER=self.I["Q"][0]/CAPACITY_DRYER*POWER_DRYER

            electricity_dryer=pd.DataFrame({"Q":ENERGY_DRYER, "U": "kWh"}, index=["electrical energy - dryer"])

            #total electricity input
            EXTERNAL_ELECTRICAL_ENERGY=electricity_dryer["Q"][0]
        
        Input = pd.concat([Input,
                        prod('electrical energy', EXTERNAL_ELECTRICAL_ENERGY, "kWh", location="DEU", source='external.Electricity')])

        return Input


    def water_vapour(self):
        biomass_input=self.I.copy()
        #NOTE: Water vapor is a GHG emissions, but is neglected within the GWP100
        Water_vapour=pd.DataFrame(index=["water vapor"], columns=["Q","U"])
        Water_vapour["Q"][0]=((1-self.final_moisture)-biomass_input["DM"][0])*biomass_input["Q"][0] #assuming all water will be released in water vapour
        Water_vapour["U"][0]=biomass_input["U"][0]

        return Water_vapour
    
    def emissions(self):

        Emis=pd.DataFrame(index=[], columns=[])
        #TODO: Include also emissions for construction of a rotary drum, at the moment neglected

        #NOTE: Initial approach was to assume diesel burning as emissions, but causes double accounting with electricity. Therefore, is not longer included
        # #Emissions from fuel consumption, assumed to be diesel
        # Emis.loc["fuel consumption dryer", "CO2"]=self.fuel_required()*GHG_protocol.Table1().loc["Gas/Diesel oil", "Mass basis"]/1000 #mass basis is in kg CO2 per tonne of fuel
        # Emis.loc["fuel consumption dryer", "CH4"]=self.fuel_required()*GHG_protocol.Table2().loc["Gas/Diesel oil", "Mass basis"]/1000 
        # Emis.loc["fuel consumption dryer", "N2O"]=self.fuel_required()*GHG_protocol.Table3().loc["Gas/Diesel oil", "Mass basis"]/1000 

        return convert_molmasses(Emis, to="element")
    
    def cashflow(self):
        #CAPEX
        Cashflow_capex = pd.DataFrame(columns=["EUR", "period_years", "section"])
        if self.new_infrastructure==True:
            Cashflow_capex.loc["machinery investment", "EUR"]= 30000 #source: https://vangton.en.made-in-china.com/product/SvjJwXqdiLYC/China-Rotary-Drum-Dryer-Economical-and-Practical.html
        else:
            Cashflow_capex.loc["machinery investment", "EUR"]= 1 #to avoid division by zero error
        
        Cashflow_capex["period_years"]= 20
        Cashflow_capex["section"]= "capex"

        #OPEX
        PRICE_DIESEL=InputPrice().diesel()["Q"][0]
        DIESEL_DENSITY=0.83 #kg/liter

        Cashflow_opex = pd.DataFrame(columns=["EUR", "period_years", "section"])
        #NOTE: Previuosly fuel consumption (Diesel) was assumed, but might be double counting
        # Cashflow_opex.loc["fuel consumption dryer", "EUR"]=self.fuel_required()*PRICE_DIESEL/DIESEL_DENSITY
        Cashflow_opex["period_years"]= 1
        Cashflow_opex["section"]= "opex"

        return pd.concat([Cashflow_capex, Cashflow_opex])
        

class CoFiring(Process):
    """
    |  Describes the process of biomass combustion in a co-firing plant

    :type I: Product
    :param I: substrates to feed the cofiring plant (currently: autumn tree leaves)
    :type kW: int or float
    :param kW: power capacity of the cofiring plant
    :type electrical_efficiency: float
    :param electrical_efficiency: electrical efficiency of the cofiring plant
    :type co_firing_ratio: float
    :param co_firing_ratio: share of biomass fuel to replace the fossil fuel
    :type main_substrate: str
    :param main_substrate: type of fossil fuel in the cofiring plant
    :type include_main_substrate: boolean
    :param include_main_substrate: if true, the share of fossil fuel is included in the calculation
    :type new_infrastructure: boolean
    :param new_infrastructure: if true, new infraestructure is included in the calculation
    :type retrofitting: boolean
    :param retrofitting: if true, retroffiting is included in the calculation
    :type lifetime: int
    :param lifetime: number of years for the lifetime of the project
    :type operational_hours: int
    :param operational_hours: amount of operational hour of the cofiring plant per year
    :type EEG_tariff: str
    :param EEG_tariff: version of the EEG to be applied for the feed-in-tariff
    :type year_assessment: int
    :param year_assessment: year of the assessment
    :type land_purchase: boolean
    :param land_purchase: if true, land purchase is considered in the cashflow
    :type land_rent: boolean
    :param land_rent: if true, land rent is considered in the cashflow
    :type emissions_combustion: str
    :param emissions_combustion: method for calculating GHG emissions (mainly focused in pellet production)
    :type operation_approach: str
    :param operation_approach: method to calculate 
    """

    #NOTE: Cofiring is considering a fossil fuel as main feedstock (substrate), while the biomass is consireded as a co-substrate

    VAPORIZATION_HEAT = 4.18 #kJ/kg
    KJ_TO_KWH = 0.0002778 #kWh/kJ
    MJ_TO_KWH = 0.27778 #kWh/MJ
    GJ_TO_KWH = 277.778

    def __init__(self, I=prod("tree leaves", 1, "ton"),  kW=25000, energy_usage_type="CHP plant", electrical_efficiency=0.36, include_thermal= True, 
                 thermal_energy_use=0.75, co_firing_ratio=0.10, main_substrate="coal", include_main_substrate=False, new_infrastructure=True, 
                 retrofitting=False, lifetime=20, operation_hours=8000, electricity_tariff="surcharge", year_assessment=2022, land_purchase=True, 
                 energy_tariff=("bidding average", None), land_rent=False, emissions_combustion="carbon_content", operation_approach="per year", 
                 fixed_carbon_share=0.0, id=None):
        
        self.I = I #co-substrate
        self.kW = kW
        self.energy_usage_type=energy_usage_type
        self.electrical_efficiency = electrical_efficiency # 36% (pure biomass) and 49% (pure coal) according to Knapp et al. 2019 (Table 3), other sources 30 or 40%
        self.include_thermal=include_thermal
        if self.include_thermal==True:
            self.thermal_efficiency=0.46 #NOTE: Default 
        else:
            pass
        self.thermal_energy_use=thermal_energy_use #NOTE:at the moment set as the same thermal use as biogas since its conceived to be coupled to a CHP unit
        self.co_firing_ratio = co_firing_ratio #10% to woodchips according to Knapp et al. 2019 (Table 3)
        self.main_substrate = main_substrate
        self.include_main_substrate=include_main_substrate
        self.retrofitting = retrofitting
        self.lifetime = lifetime
        self.operation_hours = operation_hours
        self.electricity_tariff = electricity_tariff
        self.year_assessment = year_assessment
        self.land_purchase=land_purchase
        self.energy_tariff=energy_tariff
        self.land_rent=land_rent
        self.new_infrastructure=new_infrastructure
        self.emissions_combustion=emissions_combustion
        self.operation_approach = operation_approach
        self.fixed_carbon_share = fixed_carbon_share
        self.id = id

        # if ("LHV fresh" not in list(self.I.columns) or "LHV dry" not in list(self.I.columns))==True:
        #     raise ValueError (f"Lower heating value must be specified under 'LHV fresh' or 'LHV dry'")
    
    def feedstock_energy_content(self):
        """
        Function to determine the energetic content of the feedstock
        """
        energy_content=pd.DataFrame(index=[], columns=[])
        if self.main_substrate=="coal":
            energy_content.loc[f"energy content - {self.main_substrate}", "Q"] = 27.6 * self.GJ_TO_KWH #originally in GJ/tonne (MJ/kg), according to ZuV 2012 (Anhang 1), converted to kWh/tonne
            energy_content.loc[f"energy content - {self.main_substrate}", "U"] = "kWh/tonne"
        
        if "tree leaves" in str(self.I.index):
            if self.I["U"][0]=="ton":
                energy_content.loc[f"energy content - {self.I.index[0]}", "Q"]=self.I.loc[f"{self.I.index[0]}", "LHV"]*self.MJ_TO_KWH * 1000 #originally in MJ/kg, converted in kWh/tonne
                energy_content.loc[f"energy content - {self.I.index[0]}", "U"]="kWh/tonne"
            if self.I["U"][0]=="kg":
                energy_content.loc[f"energy content - {self.I.index[0]}", "Q"]=self.I.loc[f"{self.I.index[0]}", "LHV"]*self.MJ_TO_KWH #originally in MJ/kg, converted in kWh/kg
                energy_content.loc[f"energy content - {self.I.index[0]}", "U"]="kWh/kg"     
        
        return energy_content

    def supply_information(self):
        forecasted_energy_year = self.kW * self.electrical_efficiency * self.operation_hours #in kWh/year
        ENERGY_CONTENT_CO_SUBS=self.feedstock_energy_content().loc[f"energy content - {self.I.index[0]}", "Q"] #kWh/tonne
        ENERGY_CONTENT_MAIN_SUBS=self.feedstock_energy_content().loc[f"energy content - {self.main_substrate}", "Q"] #kWh/tonne
        
        if self.include_main_substrate==True:
            RATIO_MAIN_TO_CO=(1-self.co_firing_ratio)/self.co_firing_ratio #ratio of the times of main substrate is to co-substrate
            MASS_MAIN_SUBS=self.I["Q"][0]*RATIO_MAIN_TO_CO
            potential_energy_year=self.electrical_efficiency*(self.I["Q"][0]*ENERGY_CONTENT_CO_SUBS + MASS_MAIN_SUBS*ENERGY_CONTENT_MAIN_SUBS)
        else:
            RATIO_MAIN_TO_CO=0
            potential_energy_year=self.electrical_efficiency*(self.I["Q"][0]*ENERGY_CONTENT_CO_SUBS)

        supply_information=pd.DataFrame(index=[], columns=[])
        if self.I["U"][0]=="ton":
            supply_information.loc["fuel (co-subs) one year (dry)", "Q"]=self.operation_hours*self.kW/ENERGY_CONTENT_CO_SUBS
            supply_information.loc["fuel (co-subs) one year (dry)", "U"]="ton"
            supply_information.loc["fuel (co-subs) one year (wet)", "Q"]=supply_information.loc["fuel (co-subs) one year (dry)", "Q"]/self.I["DM"][0]
            supply_information.loc["fuel (co-subs) one year (wet)", "U"]="ton"
            supply_information.loc[f"{self.main_substrate} required one year", "Q"]=supply_information.loc["fuel (co-subs) one year (dry)", "Q"]*RATIO_MAIN_TO_CO
            supply_information.loc[f"{self.main_substrate} required one year", "U"]="ton"
            supply_information.loc["supply time of feedstock (dry co-subs)", "Q"]=(potential_energy_year/forecasted_energy_year)*365
            supply_information.loc["supply time of feedstock (dry co-subs)", "U"]="day"
            supply_information.loc["total amount units from feedstock (dry)", "Q"]=potential_energy_year/forecasted_energy_year
            supply_information.loc["total amount units from feedstock (dry)", "U"]="unit"

        #NOTE:print a warning the total amount of organic feedstock that would be procsses and a limit
        return supply_information

    def operation_share(self):
        supply_information=self.supply_information()
        oper_share=supply_information.loc["supply time of feedstock (dry co-subs)", "Q"]/365

        if oper_share>1:
            oper_share=1
            print ("The current amount of feedstock is greater than needed for a full year operation")

        return oper_share

    def lifetime_share(self):
        supply_information=self.supply_information()
        lifetime_share=supply_information.loc["supply time of feedstock (dry co-subs)", "Q"]/(365*20)
 
        return lifetime_share

    # NOTE: This fucntion is under development/review
    # def amount_redefinition(self):
    #     """
    #     Redefines the amount of input feedstock (wet basis) when the operation approach is "per year".
    #     """
    #     biomass_input=self.I.copy()
    #     biomass_per_year=self.supply_information().loc["fuel (co-subs) one year (wet)", "Q"]
    #     if self.operation_approach=="per year":
    #         if self.I["Q"][0]>biomass_per_year:
    #             biomass_input["Q"]=biomass_per_year
    #     else:
    #         pass

    #     return biomass_input
    
    
    def output(self):
        #NOTE: Electrical energy generation is focused only in the biomass share?
        ENERGY_CONTENT_MAIN_SUBS=self.feedstock_energy_content().loc[f"energy content - {self.main_substrate}", "Q"] #kWh/tonne
        ENERGY_CONTENT_CO_SUBS=self.feedstock_energy_content().loc[f"energy content - {self.I.index[0]}", "Q"] #kWh/tonne
        RATIO_MAIN_TO_CO=(1-self.co_firing_ratio)/self.co_firing_ratio #ratio of the times of main substrate is to co-substrate
        MASS_MAIN_SUBS=self.I["Q"][0]*RATIO_MAIN_TO_CO
        
        biomass_input=self.I.copy().loc[f"{self.I.index[0]}"]

        output=pd.DataFrame(index=[], columns=[])
        #NOTE: Check energy generation 
        if self.I["U"][0]=="ton":
            output.loc["electrical energy - biomass share", "Q"]=ENERGY_CONTENT_CO_SUBS * biomass_input["Q"] * self.electrical_efficiency
            output.loc["electrical energy - biomass share", "U"]="kWh"
            output.loc[f"electrical energy - {self.main_substrate} share", "Q"]=ENERGY_CONTENT_MAIN_SUBS*MASS_MAIN_SUBS*self.electrical_efficiency
            output.loc[f"electrical energy - {self.main_substrate} share", "U"]="kWh"
        if self.I["U"][0]=="kg":
            output.loc["electrical energy - biomass share", "Q"]=(ENERGY_CONTENT_CO_SUBS/1000) * biomass_input["Q"] * self.electrical_efficiency
            output.loc["electrical energy - biomass share", "U"]="kWh"
            output.loc[f"electrical energy - {self.main_substrate} share", "Q"]=(ENERGY_CONTENT_MAIN_SUBS/1000)*MASS_MAIN_SUBS*self.electrical_efficiency
            output.loc[f"electrical energy - {self.main_substrate} share", "U"]="kWh"

        if self.include_thermal==True:
            output.loc["thermal energy - biomass share", "Q"]=ENERGY_CONTENT_CO_SUBS * biomass_input["Q"] * self.thermal_efficiency
            if self.I["U"][0]=="kg":
                output.loc["thermal energy - biomass share", "Q"]=output.loc["thermal energy - biomass share", "Q"]/1000
            output.loc["thermal energy - biomass share", "U"]="kWh"

        
        #NOTE: according to Pnakovic and Dzurenda, ashes can be used as a fertilizer after combustions. Lemars et al (2018) have also stated that
        # ashes from wood could be used as fertilizers in germnay
        #NOTE: Check if amount the ashes varies for dry and fresh biomass (it should not vary that much)
        output.loc["ashes", "Q"]=biomass_input["Q"]*biomass_input["ash content"] #around 15% of ashes Pnakovic & Dzurenda, 2015 for fresh leaves
        output.loc["ashes", "U"]=biomass_input["U"]

        return output   
    
    def emissions(self):
        
        output=self.output()
        biomass_input=self.I.copy().loc[f"{self.I.index[0]}"]

        ENERGY_CONTENT_MAIN_SUBS=self.feedstock_energy_content().loc[f"energy content - {self.main_substrate}", "Q"] #kWh/tonne
        ENERGY_CONTENT_CO_SUBS=self.feedstock_energy_content().loc[f"energy content - {self.I.index[0]}", "Q"] #kWh/tonne
        RATIO_MAIN_TO_CO=(1-self.co_firing_ratio)/self.co_firing_ratio #ratio of the times of main substrate is to co-substrate
        MASS_MAIN_SUBS=self.I["Q"][0]*RATIO_MAIN_TO_CO
        
        #emissions from plant construction/lifetime
        if self.new_infrastructure==True:
            if self.main_substrate=="coal":
                emis_construction=Ecoinvent34(activity='hard coal power plant construction, 500MW', product="hard coal power plant", unit="unit", location= "GLO").emissions() * self.lifetime_share() 
                emis_construction=emis_construction.rename(index={"hard coal power plant construction, 500MW":"coal power plant construction"})
                emis_construction=convert_molmasses(emis_construction, to="molecule")
                FACTOR=self.kW/500000 # since this emissions are calculated for a 500 000 kW plant, it has to be recalculated
                emis_construction=emis_construction*FACTOR
        else:
            emis_construction=pd.DataFrame(index=[], columns=[])
        
        #NOTE: retrofitting emissions as new furnance installation (Gil & Rubiera, 2018)
        if self.retrofitting==True:
            if self.kW<=300:
                emis_retrofitting=Ecoinvent34(activity='market for furnace, wood chips, with silo, 300kW', product="furnace, wood chips, with silo, 300kW", unit="unit", location= "GLO").emissions() * self.lifetime_share() 
                emis_retrofitting=emis_retrofitting.rename(index={"market for furnace, wood chips, with silo, 300kW":"retrofitting, furnace"})
            elif self.kW<=1000:
                emis_retrofitting=Ecoinvent34(activity='market for furnace, wood chips, with silo, 1000kW', product="furnace, wood chips, with silo, 1000kW", unit="unit", location= "GLO").emissions() * self.lifetime_share()             
                emis_retrofitting=emis_retrofitting.rename(index={"market for furnace, wood chips, with silo, 1000kW":"retrofitting, furnace"})
            elif self.kW<=5000:
                emis_retrofitting=Ecoinvent34(activity='market for furnace, wood chips, with silo, 5000kW', product="furnace, wood chips, with silo, 5000kW", unit="unit", location= "GLO").emissions() * self.lifetime_share()             
                emis_retrofitting=emis_retrofitting.rename(index={"market for furnace, wood chips, with silo, 5000kW":"retrofitting, furnace"})
            elif self.kW>5000:
                FACTOR=self.kW/5000
                emis_retrofitting=Ecoinvent34(activity='market for furnace, wood chips, with silo, 5000kW', product="furnace, wood chips, with silo, 5000kW", unit="unit", location= "GLO").emissions() * FACTOR * self.lifetime_share()             
                emis_retrofitting=emis_retrofitting.rename(index={"market for furnace, wood chips, with silo, 5000kW":"retrofitting, furnace"})
            emis_retrofitting=convert_molmasses(emis_retrofitting, to="molecule")
        
        else:
            emis_retrofitting=pd.DataFrame(index=[], columns=[])   
            
        #NOTE: no evidence of literature explaining about emissions from additional input, electricity and otherinputs

        #emissions from combustion
        emis_combustion=pd.DataFrame(index=[], columns=[])
        if self.emissions_combustion=="Sebastian_et_al_2011":
            emis_combustion.loc["biomass combustion", "CO2"] = 11 * (output["Q"].sum()/1000) #According to Sebastian et al. 2011, section 3.3
        
        elif self.emissions_combustion=="Chen_et_al_2020":
            #NOTE:emission factors from Chen et al. 2020 (Table 1) for mixed-combustion given in term of energy produced, NOT from mass basis
            elec_delivered = output.loc["electrical energy - biomass share","Q"] + output.loc[f"electrical energy - {self.main_substrate} share", "Q"]
            #NOTE:Check whether to calculate biomass allocation on electricity delivered
            emis_combustion.loc["biomass combustion", "CO2"] = elec_delivered * 0.871
            emis_combustion.loc["biomass combustion", "CH4"] = elec_delivered * 0.00209
            emis_combustion.loc["biomass combustion", "NOx"] = elec_delivered * 0.00578

        elif self.emissions_combustion=="GHG_protocol":
            #biomass_input=self.biomass_drying().loc[f"{self.I.index[0]} dry"]
            emis_combustion.loc["biomass combustion", "CO2"] = biomass_input["Q"] * GHG_protocol.Table1().loc["Wood or wood waste","Mass basis"]
            emis_combustion.loc["biomass combustion", "CH4"] = biomass_input["Q"] * GHG_protocol.Table2().loc["Wood or wood waste","Mass basis"]
            emis_combustion.loc["biomass combustion", "N2O"] = biomass_input["Q"] * GHG_protocol.Table3().loc["Wood or wood waste","Mass basis"]

        elif self.emissions_combustion=="volatile_matter":
            #biomass_input=self.biomass_drying().loc[f"{self.I.index[0]} dry"]
            volatile_matter=biomass_input["Q"]*biomass_input["DM"]*biomass_input["oDM"] * 0.86 #normalized based on 65% of volatile matter, 15% ashes, 10% fixed carbon (char), 10% moisture (Gonzalez et al. 2020)
            #NOTE: in this approach, based on the emissions (normalized) from the GHG protocol (for each gas), is assumed that the volatile matter would generate GHG emissions in fixed proportions
            # Volatile matter has to be multiplied times 1000 (from tonne to kg)
            emis_combustion.loc["biomass combustion", "CO2"] = volatile_matter * 0.992 * 1000  
            emis_combustion.loc["biomass combustion", "CH4"] = volatile_matter * 0.007 * 1000
            emis_combustion.loc["biomass combustion", "N2O"] = volatile_matter * 0.001 * 1000
            #emis_combustion=convert_molmasses(emis_combustion, to="molecule")
        
        elif self.emissions_combustion=="carbon_content":
            volatile_carbon=biomass_input["Q"]*biomass_input["DM"]*biomass_input["oDM"]*biomass_input["orgC"] * (1-self.fixed_carbon_share)
            emis_combustion.loc["biomass combustion", "CO2-C"] = volatile_carbon * 0.992 * 1000  
            emis_combustion.loc["biomass combustion", "CH4-C"] = volatile_carbon * 0.008 * 1000
            emis_combustion=convert_molmasses(emis_combustion, to="molecule")

        #NOTE: Ashes assumed to be used as fertilizer. 

        if self.include_main_substrate==True:
            # Value for 'Steinkohlenkoks' in Anhang 1
            emis_combustion.loc[f"{self.main_substrate} combustion", "CO2"] = 0.105 * 27.6 * MASS_MAIN_SUBS

        # emis_combustion = emis_combustion * self.operation_share()

        return convert_molmasses(pd.concat([emis_construction, emis_retrofitting, emis_combustion]), to="element")

    def area_requirements(self):
        AREA_DEMAND=0.55 #m2/kW installed based on Boyd et al (2010), table 8.2.
        area_requirement = AREA_DEMAND * self.kW # in m²

        print(f"The area required for a {self.kW} kW plant is {area_requirement} m2")
        return area_requirement
    
    #NOTE: According to Lüschen & Madlener (2013), feed-in-tariff does not apply for co-firing plants
    # def feed_in_tariff(self):
    #     #Tariff for biomass, regardless the method of convertion
    #     dict_EEG_21_tarrif={2021:12.60,
    #                         2022:12.54,
    #                         2023:12.47,
    #                         2024:12.41}

    #     if self.EEG_tariff=="EEG 2012":
    #     #based on the digression equation derived from "Leitfaden Biogas" by FNR (2016). Part of the EEG
    #     #tariff in EUR/kWh

    #         if self.kW<150:
    #             tariff=(-0.2662*self.year_assessment+549.8)/100
    #         elif self.kW>=150 and self.kW<500:
    #             tariff=(-0.2292*self.year_assessment+473.3)/100
    #         elif self.kW>=500 and self.kW<5000:
    #             tariff=(-0.205*self.year_assessment+423.44)/100
    #         elif self.kW>5000:
    #             tariff=(-0.112*self.year_assessment+231.33)/100

    #     if self.EEG_tariff=="EEG 2021":
    #         if self.kW<150:
    #             tariff=dict_EEG_21_tarrif[self.year_assessment]/100 #EUR/kWh fixed price (https://biogas.fnr.de/rahmenbedingungen/eeg-2021)
    #         elif self.kW>=150:
    #             tariff=0.1728 # EUR/kWh mean value the bidding in 2022 (https://www.bundesnetzagentur.de/DE/Fachthemen/ElektrizitaetundGas/Ausschreibungen/Biomasse/Gebotstermin01092022/start.html)
    
    #     return tariff
    
    def energetic_tariff(self):

        #Information about the tariff here: https://www.amprion.net/Energy-Market/Levies/Combined-Heat-and-Power-Act/CHP-surcharges-since-2002.html

        CHP_SURCHARGE=pd.DataFrame.from_dict({2023: 0.00357, 
                                            2022: 0.00378, 
                                            2021: 0.00357, 
                                            2020: 0.00254, 
                                            2019: 0.00226, 
                                            2018: 0.00345, 
                                            2017: 0.00438}, orient="index", columns=["CHP_surcharge"]) # values of surcharhe in EUR/kWh

        if self.energy_usage_type=="CHP plant":
            if self.energy_tariff[0]=="CHP surcharge":
                tariff=CHP_SURCHARGE.loc[self.year_assessment, "CHP_surcharge"]

            elif self.energy_tariff[0]=="bidding average":
                tariff=0.0614 #Value for 2022. Source: https://www.bundesnetzagentur.de/DE/Fachthemen/ElektrizitaetundGas/Ausschreibungen/KWK/BeendeteAusschreibungen/KWK1122022/start.html

            elif self.energy_tariff[0]=="Knapp et al 2019":
                tariff=0.075         #Electrical revenues based on Knapp et al. 2019, assumed to be 0.075 EUR/kWhe

            elif self.energy_tariff[0]=="manual":
                tariff=self.energy_tariff[1]           

        return tariff
    
    def cashflow(self):
        INVESTMENT_COST=1500 #EUR/kWel, according to Lüschen & Madlener (2013), originally 1120, but forecasted to be increased up to 1700 
        #NOTE: Knapp et al. 2019, have reported installation of 1374 EUR/kWhel for pure biomass-based power plants, table 4
        RETROFITTING_COST=346 #EUR/kWel for retrofitting existent coal plants, according to Knapp et al. 2019 (Table 3)
        # SURCHARGE_VALUE=0.0614 #Value for 2022. Source: https://www.bundesnetzagentur.de/DE/Fachthemen/ElektrizitaetundGas/Ausschreibungen/KWK/BeendeteAusschreibungen/KWK1122022/start.html

        electrical_energy_biomass=self.output().loc["electrical energy - biomass share"]
        electrical_energy_main_subs=self.output().loc[f"electrical energy - {self.main_substrate} share"]

        #Costs of invesment and installation
        Cashflow_capex = pd.DataFrame(columns=["EUR", "period_years", "section"])
        if self.retrofitting==True:
            Cashflow_capex.loc["retrofitting cost", "EUR"]=RETROFITTING_COST*self.kW*self.electrical_efficiency

        if self.new_infrastructure==True:
            Cashflow_capex.loc["installation and construction", "EUR"]=INVESTMENT_COST*self.kW*self.electrical_efficiency
        else:
            Cashflow_capex.loc["installation and construction", "EUR"]=1 #NOTE: this value just to avoid ZeroDivisionError
        
        if self.land_purchase==True:
            Cashflow_capex.loc["land purchase", "EUR"]= self.area_requirements() * LandPrice().purchase()["Q"][0]


        Cashflow_capex["period_years"] = 20
        Cashflow_capex["section"] = "capex"

        #Costs of operation
        Cashflow_opex = pd.DataFrame(columns=["EUR", "period_years", "section"])
        if self.land_rent==True:
            Cashflow_opex.loc["land rent", "EUR"]= self.area_requirements() * LandPrice().rent()["Q"][0]

        #NOTE: REVIEW AGAIN VALUES OF COSTS<-------
        COST_PERSONNEL= 80000 #EUR/person*year, according to Lüschen & Madlener (2013)
        #NOTE: For a 800 MW plant (Lüschen & Madlener, 2013) 4 people is required --> 1 person for 200 MW 
        Cashflow_opex.loc["personnel", "EUR"]=(1/(200*1000)) * self.kW * COST_PERSONNEL 
        #NOTE: For a 800 MW plant (Lüschen & Madlener, 2013), 100K EUR for cleaning --> 0.125 EUR per KW
        Cashflow_opex.loc["plant cleaning", "EUR"]= 0.125*  self.kW 
        #NOTE: For a 800 MW plant (Lüschen & Madlener, 2013), 1.5 EUR/kWel and 200K EUR (other costs) for 800 MW --> 0.25 EUR per KW
        Cashflow_opex.loc["fixed operation cost", "EUR"]= (1.5 * self.kW * self.electrical_efficiency) + (0.25 * self.kW)

        # Revenues
        Cashflow_opex.loc["electricity revenues - biomass share"]=self.energetic_tariff() * electrical_energy_biomass["Q"] * -1 * 0.9 #assuming 10% of self consumption (Ericsson, 2007)
        if self.include_main_substrate==True:
            Cashflow_opex.loc[f"electricity revenues - surcharge tariff ({self.main_substrate})"]=self.energetic_tariff() * electrical_energy_main_subs["Q"]*-1
        
        #thermal energy (if applies)EEG_pricestehr
        #TODO: Update the revenues prices for thermal energy is applies()
        if self.include_thermal==True:
            thermal_energy_biomass=self.output().loc["thermal energy - biomass share"]
            Cashflow_opex.loc["themal energy revenues - biomass share"]=self.energetic_tariff() * thermal_energy_biomass["Q"] * self.thermal_energy_use * -1
            if self.include_main_substrate==True:
                Cashflow_opex.loc[f"themal energy revenues - ({self.main_substrate})"]=self.energetic_tariff() *electrical_energy_main_subs["Q"]*-1
                    
        Cashflow_opex["period_years"] = 1
        Cashflow_opex["section"] = "opex"
        
        return pd.concat([Cashflow_capex, Cashflow_opex])
