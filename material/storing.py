import pandas as pd
import numpy as np
import warnings
import math

from scipy.integrate import quad

from parameters import GasDensity
from material import DecayContinuousExtraction, ConcreteBunkerSilo, ConcreteCircularPit
from read import IPCC, EEA, Pardo_2015, VDLUFA, Bacenetti_2015, Kupper_2020, Ecoinvent
from read.Prices import InputPrice
from read import EMeRGE
from tools import get_content, convert_molmasses, prod, O_from_massloss, valid_item
from declarations import Emissions, Process


class ManureStorageEmerge(Process):

    """
    |  This represents the storage of liquid cow manure or slurry with conditions as determined during the EMeRGE project.
    |  
    |  **NOTE:**
    |  - Under development. Emissions and outputs not determined yet!
    |  The following steps are probably required:
    |  1. Calculate emissions factors for all gases that have been measured, e.g. CH4, N2O, ... outside of this module
    |  2. Use these emissions factors for the calculation in the 'emissions(self)' method
    |  3. Calculate relative changes in composition from the measured composition, e.g. change in total N concentration
    |  4. Use these factors to define outputs
    |  5. Assure that for total mass, N and C: Input = Output + Emissions
    |     To this end, assume that the difference in carbon is released as CO2, the difference in N as N2 (to be discussed)
    """

    def __init__(self,
                 I = prod("cow manure, liquid", 1, "ton"),
                 condition = "winter", # "summer"
                 treatment = "continous, untreated", # alternatives: "batch, untreated"  "batch, with Eminex"  "continous, untreated"  "continous, with Eminex"
                 id = None
                 ):
        self.I = get_content(I, required=["DM", "oDM", "N"])
        self.condition = condition # This should reflect, whether storage is at higher or lower temperatures and shorter or longer
        self.treatment = treatment # Whether Eminex is used or not
        self.id = id
    
    def additional_input(self):
        """
        In the IBC trials 2.2 g Eminex per kg of manure were used
        """
        pass

    def output(self):
        """
        Output from manure storage: Should be the manure in lower quantity and with lower C and N contents, because of losses as emission
        """
        # To use the data from the input (the liquid manure). Copy is done, not to change the input accidentally
        Input = self.I.copy()
        
        # Assuming that the output is determined by compositional changes
        # NOTE: For the moment assume no change in composition
        Output = Input
        return Output

    def emissions(self):
        """
        Emissions from manure storage
        """
        Input = self.I.copy()

        EmisFactWinter = EMeRGE.emission_factor_winter_IBC()

        # Emission factors need to be calculated for all gases, and best be added with a dedicated read function
        if self.condition == "winter":
            EF_NH3 = EmisFactWinter.loc["NH3-N emission factor 180 days", self.treatment]
            EF_N2O = EmisFactWinter.loc["N2O-N emissions factor 180 days", self.treatment]
            EF_CO2 = EmisFactWinter.loc["CO2-C emission factor 180 days", self.treatment]
            EF_CH4 = EmisFactWinter.loc["CH4-C emission factor 180 days", self.treatment]

        Emis = Emissions()

        Emis["NH3-N"] = Input["Q"] * Input["N"] * EF_NH3
        Emis["N2O-N"] = Input["Q"] * Input["N"] * EF_N2O

        Emis["CO2-C"] = Input["Q"] * Input["DM"] * Input["oDM"] * Input["orgC"] * 1000 * EF_CO2
        Emis["CH4-C"] = Input["Q"] * Input["DM"] * Input["oDM"] * Input["orgC"] * 1000 * EF_CH4

        Emis.index = ["manure (" + self.treatment + ")"]

        return Emis



class ManureStorage(Process):

    """
    |  ManureStorage represents the storing of manure after removal from the stable. Currently this only supports broiler manure as input.

    :type I: Product
    :param I: the manure that should be stored
    :type time_period: numeric
    :param time_period: currently only affects the size of the storage structure needed, and thus these emissions
    :type climate: string
    :param climate: affects the MCF and therefore the CH4 emissions in the "default", "IPCC2019", and "EEA2016" method
    :type method: string
    :param method: method of how the emissions should be calculated. For "cow manure, liquid" only "default", For "broiler manure" either "default", "IPCC2019", "EEA2016", "Pardo2015" or "Moore2011"
    :param id: numeric
    :param id: id of the class instance

    |  **Note**
    |  Emission factors by Haenel2018, IPCC2019, and EEA2016 do not always clearly distingush between housing and storage. The emissions calculated here therefore partly comprise both. Avoid double accounting!
    |  EEA/EMEP NH3 emissions are however clearly seperated in housing and storage. These can therefore be combined.
    |
    """

    def __init__(self, I = prod("broiler manure", 1, "ton"), climate="Cool Temperate Moist", method="default", timestep_transfer=None, id=None): 
        
        if I is not None:
            if valid_item(I, ['broiler manure', 'cow manure, liquid']):
                self.I = I
                self.stk = get_content(I, required=["DM", "oDM", "orgC", "B0"])

        if all(I.index == "broiler manure"):
            self.liquid_manure = False
        elif all(I.index == "cow manure, liquid"):
            self.liquid_manure = True
        else:
            raise ValueError("Currently cannot deal with several manures at the same time")
        self.climate = climate
        self.method = method
        self.timestep_transfer = timestep_transfer
        self.id = id

    def emissions(self):
        """
        | Calculation of emissions for CH4-C, CO2-C, NH3-N, N2O-N, NO-N, N2-N, and/or NH3-N & NOx-N combined

        **emissions from manure:**
        are calculated based on emission factors from different sources, depending on method

        **indirect emissions:**
        silo construction emissions are considered (ecoinvent) of a conceivable substructure for the storage of the substrate
 
        |
        """
        if self.stk is not None:

            I = self.stk.copy()
            Emis = Emissions(index=I.index)

            if self.method == "default":
                CH4_method = "IPCC2019"
                if self.liquid_manure:
                    CO2_method = "Kupper2020"
                elif not self.liquid_manure:
                    CO2_method = "Pardo2015"
                NH3_method = "EEA2016"
                N2O_method = "EEA2016"
                NO_method = "Haenel2018"
                N2_method = "IPCC2019"
                NH3_NOx_method = 0
            elif self.method == "IPCC2019":
                CH4_method = "IPCC2019"
                if self.liquid_manure:
                    CO2_method = "Kupper2020"
                elif not self.liquid_manure:
                    CO2_method = "Pardo2015"
                NH3_method = 0
                NO_method = 0
                N2O_method = "IPCC2019"
                N2_method = "IPCC2019"
                NH3_NOx_method = "IPCC2019"
            elif self.method == "EEA2016":
                CH4_method = "IPCC2019"
                if self.liquid_manure:
                    CO2_method = "Kupper2020"
                elif not self.liquid_manure:
                    CO2_method = "Pardo2015"
                NH3_method = "EEA2016"
                N2O_method = "EEA2016"
                NO_method = "EEA2016"
                N2_method = "EEA2016"
                NH3_NOx_method = 0
            elif self.method == "Pardo2015":
                CH4_method = "Pardo2015"
                if self.liquid_manure:
                    CO2_method = "Kupper2020"
                elif not self.liquid_manure:
                    CO2_method = "Pardo2015"
                NH3_method = "Pardo2015"
                N2O_method = "Pardo2015"
                NO_method = None
                N2_method = None    # possibly introduce a residual N method, that calculates the N2 emissions as the remainder to balance the N balance
                NH3_NOx_method = 0
            elif self.method == "Moore2011":
                CH4_method = "IPCC2019"
                CO2_method = "Moore2011"
                NH3_method = "Moore2011"
                N2O_method = "Moore2011"
                NO_method = "Haenel2018"
                N2_method = "IPCC2019"
                NH3_NOx_method = 0
            else:
                raise ValueError(f"method {self.method} not defined")

            print("CO2 method is set to:", CO2_method)

            ## NH3 emissions
            # NH3-N emissions factor [kg NH3-N/kg UAN or TAN] (related to the amount of UAN (Poultry) or TAN (other manure) entering the storage)
            # simplified assumption TAN = UAN (see also chapter 3.3.3.2 of Haenel el al. 2018); here we take NH4-N as reference    

            if NH3_method == 0:
                Emis['NH3-N'] = 0
            elif NH3_method == None:
                Emis['NH3-N'] = np.nan
            elif NH3_method == "EEA2016":
                NH3_N_EF = EEA.NH3_N_EF_manure_management()
                NH3hash = pd.Series({'cow manure, liquid': ("Dairy cattle", "Slurry"),
                                    'broiler manure': ("Broilers (broilers and parents)","Solid"),
                                    'turkey manure': ("Turkeys","Solid")})
                EF_NH3 = pd.Series(NH3_N_EF.loc[NH3hash[I.index],"EF_storage"].values, index=I.index)
                Emis['NH3-N'] = I['Q']*I['NH4-N']*EF_NH3
            elif NH3_method == "DEFRA2016":
                EF_NH3 =  pd.Series({'broiler manure': 0.096})[0] # # NH3 emission factor from DEFRA (2016): Inventory of Ammonia Emissions from UK Agriculture 2015 [kg NH3-N / kg TAN]
                Emis['NH3-N'] = I['Q']*I['NH4-N']*EF_NH3
            elif NH3_method == "Pardo2015":
                EF_NH3N = Pardo_2015.gas_emissions_solid_waste().loc[:, ("NH3-N", "Mean (%)")]/100
                NH3hash = pd.Series({'cow manure, liquid': "Cattle manure",
                                    'broiler manure': "Poultry manure"})
                Emis['NH3-N'] = I['Q']*I['N']*EF_NH3N.loc[NH3hash[I.index]][0]
            elif NH3_method == "Moore2011":
                EF_NH3N = 0.172 / 17*14 # Moore et al. 2011 (http://doi.org/10.2134/jeq2009.0383) for a 16 days storage period 172 g NH3 per ton litter (likely higher emissions for longer period)
                Emis['NH3-N'] = I['Q'] * EF_NH3N
            else:
                raise ValueError(f"NH3_method {NH3_method} not defined")
        
            ## Direct N2O emissions (Indirect N2O emissions are not considered here, but computed in the calc_emissions function)
            if N2O_method == 0:
                Emis['N2O-N'] = 0
            elif N2O_method == None:
                Emis['N2O-N'] = np.nan
            elif N2O_method == "IPCC2019":
                N2O_N_EF = IPCC.N2O_EF_manure_management() # Proportion of total N
                N2Ohash = pd.Series({'broiler manure': "Poultry manure with litter",
                                     'cow manure, liquid': "Liquid/slurry, with natural crust cover"})
                EF_N2O = pd.Series(N2O_N_EF.loc[N2Ohash[I.index],"EF"].values, index=I.index)
                Emis['N2O-N'] = I['Q']*I['N']*EF_N2O
            elif N2O_method == "EEA2016":
                N2O_N_EF = EEA.N2O_EF_manure_management() # Proportion of TAN
                N2Ohash = pd.Series({'broiler manure': "Broiler manure heaps, solid",
                                    'cow manure, liquid': "Cattle slurry with natural crust"})
                EF_N2O = pd.Series(N2O_N_EF.loc[N2Ohash[I.index],"EF"].values, index=I.index)
                Emis['N2O-N'] = I['Q']*I['NH4-N']*EF_N2O
            elif N2O_method == "Pardo2015":
                EF_N2ON = Pardo_2015.gas_emissions_solid_waste().loc[:, ("N2O-N", "Mean (%)")]/100
                N2ONhash = pd.Series({'cow manure, liquid': "Cattle manure",
                                    'broiler manure': "Poultry manure"})
                Emis['N2O-N'] = float(I['Q']*I['N']*EF_N2ON.loc[N2ONhash[I.index]][0])
            elif N2O_method == "Moore2011": 
                EF_N2ON = 0.07 / 44*28 # Moore et al. 2011 (http://doi.org/10.2134/jeq2009.0383) for a 16 days storage period 70 g N2O per ton litter
                Emis['N2O-N'] = I['Q'] * EF_N2ON
            else:
                raise ValueError(f"N2O_method {N2O_method} not defined")

            ## NO emissions
            if NO_method == 0:
                Emis['NO-N'] = 0
            elif NO_method == None:
                Emis['NO-N'] = np.nan
            elif NO_method == "Haenel2018":
                Emis['NO-N'] = Emis['N2O-N']*0.1    # According to HAENEL et al. (2018), the NO-N emission factor is one tenth of the N2O-N emission factor.
            elif NO_method == "EEA2016":
                EF_NO = 0.01 # Proportion of TAN; according to EMEP/EEA Table 3.10. only valid for solids.
                Emis['NO-N'] = I['Q']*I['NH4-N']*EF_NO
            else:
                raise ValueError(f"NO_method {NO_method} not defined")

            ## N2 emissions
            if N2_method == 0:
                Emis['N2-N'] = 0
            elif N2_method == None:
                Emis['N2-N'] = np.nan
            elif N2_method == "IPCC2019":
                Emis['N2-N'] = Emis['N2O-N'] * 3    # 10.104: "Webb & Misselbrook (2004) reviewed available data and concluded that as first approximation, emissions of N2 might be 3-times those of N2O. [3 kg N2-N/kg N2O-N]"
            elif N2_method == "EEA2016": 
                EF_N2 = 0.3     # Proportion of TAN; # According to EMEP/EEA Table 3.10. only valid for solids.
                Emis['N2-N'] = I['Q']*I['NH4-N']*EF_N2
            else:
                raise ValueError(f"N2_method {N2_method} not defined")
            
            ## Aggregated losses of NH3 and NOx (only reported in sum by IPCC2019 method)
            # these are the total losses of manure management (should include emissions from spreading, where much of the volotalization happens)
            if NH3_NOx_method == 0:
                Emis['NH3-N & NOx-N'] = 0
            elif NH3_NOx_method == None:
                Emis['NH3-N & NOx-N'] = np.nan
            elif NH3_NOx_method == "IPCC2019":
                EF_NH3_NOx = IPCC.NH3_and_NOx_EF_manure_management()
                NH3_NOx_row = pd.Series({'broiler manure': "Solid storage",
                                        'cow manure, liquid': "Liquid/slurry, with natural crust cover"})
                NH3_NOx_col = pd.Series({'broiler manure': ("Poultry", "Frac_gas"),
                                        'cow manure, liquid': ("Dairy Cow", "Frac_gas")})
                
                Emis['NH3-N & NOx-N'] = I['Q']*I['N']*EF_NH3_NOx.loc[NH3_NOx_row[I.index][0], NH3_NOx_col[I.index][0]]

            ## CH4 emissions
            if CH4_method == 0:
                Emis['CH4-C'] = 0
            elif CH4_method == None:
                Emis['CH4-C'] = np.nan
            elif CH4_method == "IPCC2019":
                MCF_tab = IPCC.mcf_manure_managements()/100
                MCFhash = pd.Series({'broiler manure': "Poultry manure with and without litter",
                                    'cow manure, liquid': "Liquid/Slurry, and Pit storage below animal confinements, 6 month"})
                MCF = pd.Series(MCF_tab.loc[MCFhash[I.index], self.climate].values, index=I.index)
                Emis['CH4-C'] = I['Q']*1000*I['DM']*I['oDM']*I['B0']*MCF*GasDensity['CH4']/16*12
            elif CH4_method == "Pardo2015":
                CH4_C_EF = Pardo_2015.gas_emissions_solid_waste()/100
                CH4hash = pd.Series({'broiler manure': "Poultry manure",
                                        'pig manure': "Pig manure"})
                EF_CH4C = pd.Series(CH4_C_EF.loc[CH4hash[I.index],("CH4-C", "Mean (%)")].values, index=I.index)
                Emis['CH4-C'] = I['Q']*1000*I['DM']*I['oDM']*I['orgC']*EF_CH4C
            else:
                raise ValueError(f"CH4_method {CH4_method} not defined")

            ## CO2 emissions
            if CO2_method == 0:
                Emis['CO2-C'] = 0
            elif CO2_method == None:
                Emis['CO2-C'] = np.nan
            elif CO2_method == "Pardo2015":
                # NOTE: Only for soild manures. Cannot be used for liquid cow manure!
                CO2_C_EF = Pardo_2015.gas_emissions_solid_waste()/100
                CO2hash = pd.Series({'broiler manure': "Poultry manure",
                                    'pig manure': "Pig manure",
                                    'cow manure, liquid': "Dairy manure"})
                EF_CO2C = pd.Series(CO2_C_EF.loc[CO2hash[I.index],("CO2-C", "Mean (%)")].values, index=I.index)
                Emis['CO2-C'] = I['Q']*1000*I['DM']*I['oDM']*I['orgC']*EF_CO2C
            elif CO2_method == "Kupper2020":
                # NOTE: For slurries
                CO2_EF_VS = Kupper_2020.emission_factor_slurry_storage().loc["Cattle", "CO2 [% of VS]"]/100
                Emis["CO2-C"] = I['Q']*1000*I['DM']*I['oDM']*CO2_EF_VS/44*12
            elif CO2_method == "Moore2011":
                # according to Moore strong linear relationship between N2O-N emissions and CO2-C
                def moore_NO2_CO2(N2ON):
                    CO2C = (N2ON + 1.03)/0.012
                    return CO2C
                Emis["CO2-C"] = moore_NO2_CO2(Emis["N2O-N"])
            else:
                raise ValueError(f"CO2_method {CO2_method} not defined")

            ## Emissions form building the silo
            if not self.liquid_manure:
                Emis = pd.concat([Emis, ConcreteBunkerSilo(volume=(I["Q"]/I["density"]).sum(), use_fraction=1, lifetime_years=25).emissions()]).sort_index(axis=1)
            elif self.liquid_manure:
                Emis = pd.concat([Emis, ConcreteCircularPit(volume=(I["Q"]/I["density"]).sum(), use_fraction=1, lifetime_years=25).emissions()]).sort_index(axis=1)

            return Emis
        else:
            return None

    def output(self):
        """
        | The ``output`` method returns the manure after storage
        
        - for the "default", "EEA2016", "IPCC2019" and Moore2011 method, the output is calculated via the nutrient and carbon losses as emissions.
        - for the "Pardo2015" method the output is calculated from reported changed in composition of orgC, total N and NH3.
        
        |
        """
        if self.stk is not None:
            
            I = self.stk
            Output = I.copy()

            ## When using the "Haenel2018", "IPCC2019" or "EEA2016" as method results all N and C losses should be defined. These losses are therefore used to calculate the output.
            if self.method in ["default", "EEA2016", "IPCC2019", "Moore2011"]:
                Emis = self.emissions()

            # using I.index to select emissions caused by the input (not by other processes)
                MASSLOSS_TON = Emis.loc[I.index,:].sum().sum() / 1000 # conversion into ton
                orgC_LOSS_TON = Emis.loc[I.index,["CH4-C","CO2-C"]].sum().sum() / 1000
                N_LOSS_TON = Emis.loc[I.index,["NH3-N","N2O-N","NO-N","N2-N","NH3-N & NOx-N"]].sum().sum()/1000 # in kg since N content is in kg/ton
                
                if self.method in ["default", "EEA2016", "Moore2011"]:
                    NH3N_LOSS_TON = Emis.loc[I.index, "NH3-N",].sum().sum()/1000
                    Output = O_from_massloss(I, MASSLOSS_TON=MASSLOSS_TON, orgC_LOSS_TON=orgC_LOSS_TON, N_LOSS_TON=N_LOSS_TON, NH4N_LOSS_TON=NH3N_LOSS_TON, scale_by_mass=["P", "K"], index=I.index)

                elif self.method == "IPCC2019":
                    Output = O_from_massloss(I, MASSLOSS_TON=MASSLOSS_TON, orgC_LOSS_TON=orgC_LOSS_TON, N_LOSS_TON=N_LOSS_TON, scale_by_mass=["P", "K"], index=I.index)

                    # for NH4-N assume the same ratio of NH4-N to total N as before:
                    NH4N_to_N_RATIO = I["NH4-N"]/I["N"]
                    Output["NH4-N"] = Output["N"] * NH4N_to_N_RATIO

            ## When using the "Pardo2015" not all emissions are calculated. Output therefore calculated from total N and C losses.
            elif self.method == "Pardo2015":
                Pardo = Pardo_2015.gas_emissions_solid_waste()/100

                ## orgC
                orgC_LOSS_REL = Pardo.loc["Poultry manure",[("CO2-C", "Mean (%)"),("CH4-C", "Mean (%)")]].sum()
                orgC_IN_TON = (I["mass"]*I["DM"]*I["oDM"]*I["orgC"]).sum()
                orgC_LOSS_TON = orgC_IN_TON * orgC_LOSS_REL
    
                ## total N
                N_LOSS_REL = Pardo.loc["Poultry manure",("Total N", "Mean (%)")]
                N_IN_TON = (I["mass"]*I["N"]/1000).sum()
                N_LOSS_TON = N_IN_TON * N_LOSS_REL

                ## NH3
                NH3_LOSS_REL = Pardo.loc["Poultry manure",("NH3-N", "Mean (%)")]
                NH3_IN_TON = (I["mass"]*I["N"]/1000).sum()
                NH3_LOSS_TON = NH3_IN_TON * NH3_LOSS_REL

                Output = O_from_massloss(I, orgC_LOSS_TON=orgC_LOSS_TON, N_LOSS_TON=N_LOSS_TON, NH4N_LOSS_TON=NH3_LOSS_TON, scale_by_mass=["P", "K"], index=I.index)


            ## when using the Moore2011 method losses of N2, NO and CH4 are missing, and the output contents cannot be calculated
            # it was therefore decided to use changes in composition from another study instead.
            # Rodhe & Karlsson 2002 (http://doi.org/10.1006/bioe.2002.0081)
            # and Moore et al. 2011 (http://doi.org/10.2134/jeq2009.0383)

            # elif self.method == "Moore2011&Rodhe2002":
            #     ## According to the study, the amount (Q) of the uncovered heap remained the same

            #     Output.loc[:,"mass"] = I.loc[:,"mass"]
            #     Output.loc[:,"Q"] = I.loc[:,"Q"]

            #     ## Change in DM, N, NH4, P and K (Assumptions from Rodhe & Karlsson 2002)
            #     Rodhe_Karlsson = Rodhe_Karlsson_2002.all_manure_managements()
            #     REL_DM_CHANGE = Rodhe_Karlsson.loc["dry matter","end (no cover)"]/Rodhe_Karlsson.loc["dry matter","start"]
            #     Output.loc[:,"DM"] = I["DM"]*REL_DM_CHANGE

            #     REL_N_CHANGE = Rodhe_Karlsson.loc["total N","end (no cover)"]/Rodhe_Karlsson.loc["total N","start"]
            #     Output.loc[:,"N"] = I["N"]*REL_N_CHANGE

            #     REL_NH4_CHANGE = Rodhe_Karlsson.loc["NH4-N","end (no cover)"]/Rodhe_Karlsson.loc["NH4-N","start"]
            #     Output.loc[:,"NH4-N"] = I["NH4-N"]*REL_NH4_CHANGE

            #     REL_P_CHANGE = Rodhe_Karlsson.loc["P","end (no cover)"]/Rodhe_Karlsson.loc["P","start"]
            #     Output.loc[:,"P"] = I["P"]*REL_P_CHANGE

            #     REL_K_CHANGE = Rodhe_Karlsson.loc["K","end (no cover)"]/Rodhe_Karlsson.loc["K","start"]
            #     Output.loc[:,"K"] = I["K"]*REL_K_CHANGE

            else:
                raise KeyError(f"method {self.method} is not defined")
            
            Output["density"] = I["density"]    # no density change assumed
            Output["N_fert_eff"] = I["N_fert_eff"]  # N fertilization efficacy assumed to remain unchanged
            Output["HEQ"] = VDLUFA.humus_reproduction().loc["manure stored", "HEQ"]     # humus reproduction according to VDLUFA


            Output = Output.sort_index(axis=1)

            return Output

        else:
            return None

class DigestateStorage(Process):
    """
    |  DigestateStorage represents the storing of biogas digesate.

    :type I: Product
    :param I: the digestate that should be stored
    :type timestep_transfer: numeric
    :param timestep_transfer: number of time steps the manure is stored. Not necessarily consistent with the time_period argument
    :param id: numeric
    :param id: id of the class instance

    |  **Note**
    |  - Currently just a simple input = output function without any emissions, since emissions for digestate storage are included in the BiogasPlant already
    |  - The main functionality is to transfer between timesteps

    |
    """

    # I = prod(["biogas digestate", "B"], [1,100], ["ton","ton"])

    # O = prod(["biogas digestate", "B"], [0.5,100], ["ton","ton"])

    # O = prod("biogas digestate", 50, "%")
    # O = prod("biogas digestate", 1, "ton")
    # prod("")

    def __init__(self, I = None, O = None, timestep_transfer = 0, id=None): 
        if I is not None:
            self.stk = I # simplified representation of stock. Currently stock can only be created when initializing the class with I
            if not I.index.get_level_values("item") in ['biogas digestate']:
                warnings.warn("Currently only 'biogas digestate' supported as input")
        else:
            self.stk = None
        self.I = I
        self.O = O
        self.timestep_transfer = timestep_transfer
        self.id = id


    def output(self):
        # if self.stk is not None:
        #     if any(self.O.index.isin(self.I.index)):
        #         if all(self.O["U"] == self.stk["U"]):
        #             if all(self.stk.loc[self.O.index, "Q"] >= self.O["Q"]):
        #                 Output = self.stk.loc[self.O.index,:]
        #                 Output["Q"] = self.O["Q"].values

        #             else:
        #                 print("item not in stock in requested quantities")

        #         elif all(self.O["U"] == "%"):
        #             Output = self.stk.loc[self.O.index,:]
        #             Output["Q"] = self.stk["Q"] * self.O["Q"].values / 100

                
        #         return add_system(Output, module = self.__module__, class_name = self.__class__.__name__, id = self.id)

        # else:
        #     return None

        # stock (stk) is used here, because timestep_chains deletes I when transfering from one timestep to another
        if self.stk is not None:
            return self.stk
        else:
            return None


class LeafStorageBatch(Process):
    """
    |  LeafStorageBatch represents the mathematical expresion of leaf litter decomposition by bacteria in the environment base on Olsen's equation (1963).
    |  Depending on the composition of the leaves, it is assumed the amount decomposed litter would released the same quantity of gases to the atmosphere.   
    |  It depends on the time expresed in years, and the factor k, which varies depending the weather conditions. 
    |  It is designed to be coupled with a biogas plant using a batch feeding.

    :type I: Product
    :param I: type of leaves to be stored 
    :type time_period: numeric
    :param time_period: time period of storage entry in days but converted to years
    :type initial_day: numeric
    :param initial_day: initial day for assessment of GHG emissions in a specified period.
    :type final_day: final_day
    :param final_day: final day for assessment of GHG emissions in a specified period.
    :type retention_time: integer
    :param retention_time: time in days of the substrate retention in the biogas plant
    :type plant_load: integer
    :param plant_load: capacity in tons of substrate input for the retention_time
    :param id: numeric
    :param id: id of the class instance
    """
    def __init__(self, I = prod("tree leaves", 1, "ton"), time_period = 30, initial_day=0, final_day=30, retention_time= 0, plant_load = 0, id=None):
        self.I = I
        self.time_period = time_period # entry in days, later converted in years.
        self.id = id
        self.initial_day = initial_day
        self.final_day = final_day
        self.retention_time = retention_time
        self.plant_load = plant_load
    
    def single_emissions(self):
        """
        | Calculation of emissions for carbon equivalent and nitrogen equivalent. 
        | Equation for carbon release calculation (Larionova, 2017):
        |  CO2-C= M0(1 - A1*e^(-k1*t) - A2)
        |  where:
        |   M0 = Initial mass
        |   A1= Labile mass
        |   e = number e
        |   k1 = mineralization factor, adjusted to literature findings (days^-1)
        |   t = time in years 
        |   A2= Recalcitrant mass
        """ 
        Stored_leaves = self.I.copy()
        k1= 0.01 # Larionova et al. 2017. Between 30-70% humidity (WHC) and 12 °C
        A1=0.6 # Larionova et al. 2017. Between 30-70% humidity (WHC) and 12 °C
        A2=0.4 # Larionova et al. 2017. Between 30-70% humidity (WHC) and 12 °C
        Mass_initial = Stored_leaves["Q"][0]
        def carbon_release(t):
            return Mass_initial - Mass_initial*A1*pow(math.exp(1), -k1 * t) - A2*Mass_initial
        Emis_carbon=Emissions(index=["GHG release"], columns=["CO2-C"])
        Emis_carbon.loc["GHG release","CO2-C"]=quad(carbon_release, self.initial_day, self.final_day)[0]
      
        #Nitrogen emissions
        k = 0.20 #dummy value
        N_EMISSIONS= 7.71*pow(10, -6) # kg of N2O-N / ton of wet leaves. Calculation obtain from Bernal et al. 2003. 
        PERIOD=range(0, self.time_period+1)

        Emis_nitrogen_daily=pd.DataFrame(index=["Nitrogen release"], columns=["Day","Mass","N2O-N"])
        columns=["Day", "Mass","N2O-N"]
        Daily_emissions=[]
        for x in PERIOD:
            Mass_initial = Mass_initial*pow(math.exp(1), -k*(x/365))
            values = [x, Mass_initial, Mass_initial*N_EMISSIONS]
            zipped = zip(columns, values)
            a_dictionary = dict(zipped)
            Daily_emissions.append(a_dictionary)

        Emis_nitrogen_daily= Emis_nitrogen_daily.append(Daily_emissions, True)
        Emis_nitrogen=Emissions(index=["GHG release"], columns=["N2O-N"])
        Emis_nitrogen.loc["GHG release","N2O-N"] = Emis_nitrogen_daily["N2O-N"].sum()
       
        Emis = pd.concat([Emis_carbon, Emis_nitrogen], sort=True).sort_index(axis=1)
        return Emis
    
    def single_output(self):
        """
        |  Based on Olson's equation (1963):
        |  Mt=M0*e^-kt
        |  where:
        |   Mt = Mass of litter in time "t"
        |   M0 = Initial mass liter
        |   e = number e
        |   k = factor, adjusted to literute findings
        |   t = time in years 
        """
        Output = self.I.copy()
        Mass_initial = Output["Q"][0]
        time_storing = self.time_period/365
        k = 0.40 # value considered from findings of Kim (2007) and Straigytė et al. (2009)
        Output["Q"][0] = Mass_initial*pow(math.exp(1), -k*time_storing)
        
        return Output
    
    def emissions(self):
        """
        Function for using BiogasPlant and LeafDecay classes simulatenously through iterations of specified time periods and plant loads.
        It determines the emissions from LeafDecay based on loads for biogas plant and time periods.
        """
        if self.retention_time != 0 and self.plant_load !=0:

            leaves_amount=self.I.copy()
            iterations = round(leaves_amount["Q"][0]/self.plant_load)
            Emis_decay=pd.DataFrame(index=["GHG release decay"], columns=["Day","Mass left aside","CO2-C", "Output"])
            columns=["Day", "Mass left aside","CO2-C", "N2O-N", "Output"]
            Daily_emissions=[]
            for x in range(0, iterations):
                Mass_left_aside=leaves_amount["Q"][0]-self.plant_load
                Carbon_emissions = LeafStorageBatch(I=prod("tree leaves", Mass_left_aside, "ton"), time_period = 30, initial_day=x*self.retention_time, final_day=(x+1)*self.retention_time, id=None).single_emissions()["CO2-C"].sum()
                Nitrogen_emissions = LeafStorageBatch(I=prod("tree leaves", Mass_left_aside, "ton"), time_period = self.retention_time, id=None).single_emissions()["N2O-N"].sum()
                Output_decay= LeafStorageBatch(I=prod("tree leaves", Mass_left_aside, "ton"), time_period = self.retention_time, id=None).single_output()["Q"][0]
                leaves_amount["Q"][0]=Output_decay
                if Output_decay < 0:
                    break  
                values = [(x+1)*30, Mass_left_aside, Carbon_emissions, Nitrogen_emissions, Output_decay]
                zipped = zip(columns, values)
                a_dictionary = dict(zipped)
                Daily_emissions.append(a_dictionary)

            Emis_decay= Emis_decay.append(Daily_emissions, True)
            Emis_from_decay=Emissions(index=["GHG release from decay"], columns=["CO2-C","N2O-N"])
            Emis_from_decay.loc["GHG release from decay","CO2-C"] = Emis_decay["CO2-C"].sum()
            Emis_from_decay.loc["GHG release from decay","N2O-N"] = Emis_decay["N2O-N"].sum()

            #Total emissions
            Emis = pd.concat([Emis_from_decay], sort=True).sort_index(axis=1)
            return Emis
        
        else: 
            raise ValueError(f"Plant load and retention time need to be defined in order to use combined_emissions")
    
    def output(self):
        """
        Function for using BiogasPlant and LeafDecay classes simulatenously through iterations of specified time periods and plant loads.
        It determines the output from LeafDecay based on loads for biogas plant and time periods.
        """      
        #same loop of final input to determine the ouput
        if self.retention_time != 0 and self.plant_load !=0:

            leaves_amount=self.I.copy()
            iterations = round(leaves_amount["Q"][0]/self.plant_load)
            Input_biogas=pd.DataFrame(index=["GHG release biogas plant"], columns=["Iteration","Input mass", "Mass left aside", "Output"])
            columns=["Iteration","Input mass", "Mass left aside", "Output"]
            Input_mass=[]
            for x in range(0, iterations+1):
                Input=self.plant_load
                Mass_left_aside=leaves_amount["Q"][0]-self.plant_load
                Output_decay= LeafStorageBatch(I=prod("tree leaves", Mass_left_aside, "ton"), time_period = self.retention_time, id=None).single_output()["Q"][0]
                leaves_amount["Q"][0]=Output_decay
                if Mass_left_aside<0:
                    Input=Input_mass[x-1]["Output"]
                    Mass_left_aside=0
                    Output_decay=0
                values = [x, Input, Mass_left_aside, Output_decay]
                zipped = zip(columns, values)
                b_dictionary = dict(zipped)
                Input_mass.append(b_dictionary)
            
            Input_biogas= Input_biogas.append(Input_mass, True)
            Final_input=Input_biogas["Input mass"].sum()
            
            #Determining output from the final input
            FinalInputLeaves=self.I.copy()
            FinalInputLeaves["Q"][0]=Final_input
            Output=FinalInputLeaves
            return Output
        
        else: 
            raise ValueError(f"Plant load and retention time need to be defined in order to use combined_output")


class LeafStorageContinuous(Process):

    """
    |  LeafStorageContinuous determines the CO2-C emissions expressed in labile and recalcitrant carbon dynamics with a continuous output. 
    |  This class also determines N2O-N emissions based on litter decomposition. 
    |  It is based on Olson's biomass decay equation (1963), its modification for continuous output and recalcitrant and labile dynamics determined by Larinova et al. (2017)
    |  Currently designed for tree leaves storage. 

    :type I: Product
    :param I: type of leaves to be stored 
    :type daily_load: numeric
    :param daily_load: daily load in tons to the biogas plants.
    :type k: numeric
    :param k: mineralization constant for leaves decay (Based on Olson's equation)
    :type k1: numeric
    :param k1: mineralization constant for labile organic carbon (Based on Larinova et al. (2017) findings)
    :type A1: numeric
    :param A1: labile fraction of organic carbon (Based on Larinova et al. (2017) findings)
    :type A2: numeric
    :param A2: recalcitrant fraction of organic carbon (Based on Larinova et al. (2017) findings)
    :type daily_display: boolean
    :param daily_display: enable the displaying of daily_emissions.
    :type N_emis: string
    :param N_emis: method of N emissions calculation

    | **NOTE:**
    |  - If daily_display is enabled, the function will return a dataframe with daily emissions or outputs, if not, the final sum of emissions
    |
    """

    def __init__(self, I = prod("tree leaves", 1, "ton"), daily_load=300, k=0.4, k1=0.01, A1=0.6, A2=0.4, daily_display=False, N_emis="Bernal", id=None):
        self.I=I
        self.daily_load=daily_load
        self.daily_display=daily_display
        self.k=k
        self.k1=k1
        self.A1=A1
        self.A2=A2
        self.id=id
        self.N_emis=N_emis
    
    def emissions(self):

        #CO2-C emissions
        #definitions:
        stored_leaves=self.I.copy()
        initial_mass=stored_leaves["Q"][0]
        DM=stored_leaves["DM"][0]
        oDM=stored_leaves["oDM"][0]
        orgC=stored_leaves["orgC"][0]
        initial_orgC=initial_mass*DM*oDM*orgC
        extraction=self.daily_load

        carbon_dynamics_df=DecayContinuousExtraction(stored_leaves, extraction, k=self.k, k1=self.k1, A1=self.A1, A2=self.A2).carbon_dynamics()
        co2_emissions=initial_orgC-carbon_dynamics_df["C_labile"]-carbon_dynamics_df["C_recalcitrant"]-carbon_dynamics_df["C_labile_ext"]-carbon_dynamics_df["C_recal_ext"]
        
        #N2O-N emissions:
        if self.N_emis=="Bernal":

            nitrogen_emissions=pd.DataFrame(columns=["final mass","N2O-N"])
            nitrogen_emissions["final mass"]=DecayContinuousExtraction(stored_leaves, extraction, k=self.k, k1=self.k1, A1=self.A1, A2=self.A2).total_biomass()["final mass"]
            emissions_per_area_day=17.2 #μg N₂O-N m⁻²day⁻¹, according to Bernal et al. 2003
            leaves_per_area=562.2 #g of leaves (ww) m⁻²
            emissions_per_day=(emissions_per_area_day/leaves_per_area)*1*pow(10, -3) # kg of N₂O-N ton (ww)⁻¹ day¹
            nitrogen_emissions["N2O-N emissions"]=nitrogen_emissions["final mass"]*emissions_per_day
        
        # The "decay" approach has to be adjusted
        # if self.N_emis=="decay":
        #     last_day=DecayContinuousExtraction(stored_leaves, extraction, k=self.k, k1=self.k1, A1=self.A1, A2=self.A2).last_day()
        #     nitrogen_emissions=pd.DataFrame(columns=["final mass","N2O-N"])
        #     initial_N_content=stored_leaves["Q"][0]*stored_leaves["N"][0]
        #     total_N_extracted=DecayContinuousExtraction(stored_leaves, extraction, k=self.k, k1=self.k1, A1=self.A1, A2=self.A2).total_mass_extraction().loc[last_day, "N_mass_extracted"]
        #     total_N_emis=initial_N_content-total_N_extracted

        #total emissions:
        Emis=Emissions(index=["leaf decay emissions"], columns=["CO2-C","N2O-N"])
        Emis.loc["leaf decay emissions", "CO2-C"]=list(co2_emissions)[-1]*1000

        if self.N_emis=="Bernal":
            Emis.loc["leaf decay emissions", "N2O-N"]=nitrogen_emissions["N2O-N emissions"].sum()
        if self.N_emis=="decay":
           # Emis.loc["leaf decay emissions", "N2O-N"]=total_N_emis
           pass
        
        return Emis

    def output(self):

        Output=self.I.copy()

        #definitions:
        stored_leaves=self.I.copy()
        extraction=self.daily_load

        final_product=DecayContinuousExtraction(stored_leaves, extraction, k=self.k, k1=self.k1, A1=self.A1, A2=self.A2).final_product()

        Output["Q"]=final_product["Q"][0]
        Output["DM"]=final_product["DM"][0]
        Output["oDM"]=final_product["oDM"][0]
        if self.N_emis=="Bernal":
            initial_N=stored_leaves["Q"][0]*stored_leaves["N"][0]
            N_emis=self.emissions()["N2O-N"][0]
            final_N=initial_N-N_emis
            Output["N"]=final_N/Output["Q"][0]
        else:
            Output["N"]=final_product["N"][0]
        Output["P"]=final_product["P"][0]
        Output["K"]=final_product["K"][0]
        Output["orgC"]=final_product["orgC"][0]

        return Output

    def cashflow(self):
        """
        |  All cashflows for the machinery investment and operation of continuous leaf storage
        |  **NOTE:**
        |  - These costs are not yet related to the production quantities.
        """
        Cashflow = pd.DataFrame(columns=["EUR", "period_years"])

        #Since this class is concieved to operate toghether with the biogas plant, cost of operation and investment are considered to be
        #embed in the biogas plant. 

        #Costs of plantation (installation)
        Cashflow.loc["leaf storage investment", "EUR"] = float(np.NaN)
        Cashflow.loc["leaf storage investment", "period_years"] = float(np.NaN)

        #Costs of leaves production (operation)
        Cashflow.loc["leaf storage operation", "EUR"] = float(np.NaN)
        Cashflow.loc["leaf storage operation", "period_years"] = 1

        return Cashflow
    

class LeafSilageStorage(Process):
    """
    |  LeafSilageStorage models the mass loss and GHG emissions from different types of silage techniques.
    |  It is based on literature review from different sources. The class is set to "silobag" techniques as default.
    |  NOTE: GHG emissions from silobag technique was measured for corn. However, it is assumed for tree leaves for calculation purposes.
    
    :type I: Product
    :param I: type of plant to be silaged
    :type method: string
    :param method: silage method
    :type DM_loss: float
    :param DM_loss: normalized DM loss specification
    :type fixed_DM_loss: boolean
    :param fixed_DM_loss: when True, it establishes a fixed DM_loss, independent from the method of ensiling
    
    """

    def __init__(self, I = prod("tree leaves", 1, "ton"), method="silobag", DM_loss=0.08, fixed_DM_loss=False, id=None):
        self.I=I
        self.method=method
        if self.method not in ["silobag", "bales", "conventional tower", "gas tight tower", "silo bunker - covered - Bacenetti & Fusi", "silo bunker - covered - Holmes & Muck", "silo bunker - no cover", "other"]:
            raise ValueError("Type must be: 'silobag', 'bales', 'conventional tower', 'gas tight tower', 'silo bunker - covered - Bacenetti & Fusi', 'silo bunker - covered - Holmes & Muck' ,'silo bunker - no cover', 'other'")
        self.fixed_DM_loss=fixed_DM_loss
        self.DM_loss=DM_loss
        self.id=id
            
    def mass_loss(self):
        """
        Function that determines the mass loss, according to the dry matter loss literature. It assumes that the water content will not vary. 
        Returns the object with changes in Q and DM concentration. 
        """
        silage_input=self.I.copy()
        #assuming water amount would not vary
        moisture=(1-silage_input["DM"][0])
        water_amount=moisture*silage_input["Q"][0]
        
        if self.fixed_DM_loss==False:
            #these conditions are general for leaves DM conditions
            if self.method=="conventional tower":
                self.DM_loss=0.15 #Holmes & Muck, 2000
            
            if self.method=="gas tight tower":
                self.DM_loss=0.095 #Holmes & Muck, 2000

            if self.method=="silo bunker - covered - Bacenetti & Fusi":
                self.DM_loss=0.125
            
            if self.method=="silo bunker - covered - Holmes & Muck":
                self.DM_loss=0.245
            
            if self.method=="silo bunker - no cover":
                self.DM_loss=0.365 #Holmes & Muck, 2000

            if self.method=="silobag":
                self.DM_loss=0.08 # assuming 8% of TS loss, according to Christiane Herrmann (unpublished) and  Bacenetti & Fusi (2015).
                
            ##NOTE:these conditions could be further developed according to the DM content
            # if self.type=="conventional tower":
            #     if 1-silage_input["DM"][0]<0.65:# Assuming 14% of DM loss by a moisture up to 65% (Holmes & Muck, 2000)
            #         silage_input["DM"][0]=silage_input["DM"][0]*0.86
            #     elif 1-silage_input["DM"][0]<0.70 and 1-silage_input["DM"][0]>=0.65:# Assuming 16% of DM loss by a moisture from 65 to 70% (Holmes & Muck, 2000)
            #         silage_input["DM"][0]=silage_input["DM"][0]*0.84
            #     else:
            #         raise ValueError (f"{self.method} do not support moisture lower than 30%")
                
            # if self.type=="gas tight tower":
            #     if 1-silage_input["DM"][0]<0.60:# Assuming 9% of DM loss by a moisture up to 60% (Holmes & Muck, 2000)
            #         silage_input["DM"][0]=silage_input["DM"][0]*0.91
            #     elif 1-silage_input["DM"][0]<0.70 and 1-silage_input["DM"][0]>=0.60:# Assuming 10% of DM loss by a moisture from 60 to 70% (Holmes & Muck, 2000)
            #         silage_input["DM"][0]=silage_input["DM"][0]*0.90
            #     else:
            #         raise ValueError (f"{self.method} do not support moisture lower than 40%")
        
        initial_DM=silage_input["Q"][0]*silage_input["DM"][0]
        final_DM=initial_DM*(1-self.DM_loss)
        silage_input["Q"]=water_amount+final_DM
        silage_input["DM"][0]=final_DM/silage_input["Q"][0]
        return silage_input
        
    def emissions(self):
        if self.method=="silobag": #Based on Bacenetti & Fusi (2015). 
            Emis=Emissions()
            inventory=Bacenetti_2015.Table3()
            Emis=inventory.loc["Plastic film", "Silobag"]*Ecoinvent.Ecoinvent34(activity='polyethylene production, high density, granulate', product='polyethylene, high density, granulate', unit="kg", location="RER").emissions()*self.I["Q"][0]
            Emis.rename({'polyethylene production, high density, granulate':"HDPE film"}, inplace=True)
            
            Emis=pd.concat([Emis, inventory.loc["Diesel fuel", "Silobag"]*Ecoinvent.Ecoinvent34(activity='market group for diesel, low-sulfur', product='diesel, low-sulfur', unit="kg", location="RER").emissions()*self.I["Q"][0]])
            Emis.rename({'market group for diesel, low-sulfur':"diesel fuel"}, inplace=True)
            
            Emis=pd.concat([Emis, inventory.loc["Lubricant oil", "Silobag"]*Ecoinvent.Ecoinvent34(activity='lubricating oil production', product='lubricating oil', unit="kg", location="RER").emissions()*self.I["Q"][0]])
            Emis.rename({'lubricating oil production':"lubricating oil"}, inplace=True)
            
            Emis=pd.concat([Emis, inventory.loc["Tractors", "Silobag"]*Ecoinvent.Ecoinvent34(activity='tractor production, 4-wheel, agricultural', product='tractor, 4-wheel, agricultural', unit="kg", location="CH").emissions()*self.I["Q"][0]])
            Emis.rename({'tractor production, 4-wheel, agricultural':"tractor"}, inplace=True)
            
            Emis=pd.concat([Emis, inventory.loc["Implement", "Silobag"]*Ecoinvent.Ecoinvent34(activity='agricultural machinery production, unspecified', product='agricultural machinery, unspecified', unit="kg", location="CH").emissions()*self.I["Q"][0]])
            Emis.rename({'agricultural machinery production, unspecified':"implement"}, inplace=True)

        if self.method in ["silo bunker - covered - Bacenetti & Fusi", "silo bunker - covered - Holmes & Muck", "silo bunker - no cover"]: #Based on Bacenetti & Fusi (2015) findings. 
            Emis=Emissions()
            inventory=Bacenetti_2015.Table3()
            Emis=inventory.loc["Plastic film", "Bunker silos"]*Ecoinvent.Ecoinvent34(activity='polyethylene production, high density, granulate', product='polyethylene, high density, granulate', unit="kg", location="RER").emissions()*self.I["Q"][0]
            Emis.rename({'polyethylene production, high density, granulate':"HDPE film"}, inplace=True)
            
            Emis=pd.concat([Emis, inventory.loc["Concrete", "Bunker silos"]*Ecoinvent.Ecoinvent34(activity='concrete production, for building construction, with cement CEM II/A', product='concrete, high exacting requirements', unit="m3", location="RoW").emissions()/2420*self.I["Q"][0]]) #concrete density: 2420 kg/m3
            Emis.rename({'concrete production, for building construction, with cement CEM II/A':"concrete"}, inplace=True)
           
            Emis=pd.concat([Emis, inventory.loc["Diesel fuel", "Bunker silos"]*Ecoinvent.Ecoinvent34(activity='market group for diesel, low-sulfur', product='diesel, low-sulfur', unit="kg", location="RER").emissions()*self.I["Q"][0]])
            Emis.rename({'market group for diesel, low-sulfur':"diesel fuel"}, inplace=True)
            
            Emis=pd.concat([Emis, inventory.loc["Lubricant oil", "Bunker silos"]*Ecoinvent.Ecoinvent34(activity='lubricating oil production', product='lubricating oil', unit="kg", location="RER").emissions()*self.I["Q"][0]])
            Emis.rename({'lubricating oil production':"lubricating oil"}, inplace=True)
           
            Emis=pd.concat([Emis, inventory.loc["Tractors", "Bunker silos"]*Ecoinvent.Ecoinvent34(activity='tractor production, 4-wheel, agricultural', product='tractor, 4-wheel, agricultural', unit="kg", location="CH").emissions()*self.I["Q"][0]])
            Emis.rename({'tractor production, 4-wheel, agricultural':"tractor"}, inplace=True)
            
            Emis=pd.concat([Emis, inventory.loc["Implement", "Bunker silos"]*Ecoinvent.Ecoinvent34(activity='agricultural machinery production, unspecified', product='agricultural machinery, unspecified', unit="kg", location="CH").emissions()*self.I["Q"][0]])
            Emis.rename({'agricultural machinery production, unspecified':"implement"}, inplace=True)
        
        if self.method not in ["silobag", "silo bunker - covered - Bacenetti & Fusi", "silo bunker - covered - Holmes & Muck", "silo bunker - no cover"]:
            raise warnings.WarningMessage (f"The method {self.method} has not yet been specified for inventory emissions calculation, only emissions from mass loss will be calculated")

        mass_loss=self.mass_loss()
        Emis_GHG=Emissions(index=["mass loss"])
        initial_C=self.I["Q"][0]*self.I["DM"][0]*self.I["oDM"][0]*self.I["orgC"][0]
        final_C=mass_loss["Q"][0]*mass_loss["DM"][0]*mass_loss["oDM"][0]*mass_loss["orgC"][0]
        #determining carbon loss
        C_loss=initial_C-final_C
        #assuming that emissions will split in CO2-C (99.999%) and CH4 (0.001%) (Krommweh et al. 2020)
        #check if carbon loss is expressed in tons
        if self.I["U"][0]=="ton":
            Emis_GHG.loc["mass loss","CO2-C"]=C_loss*0.99999*1000
            Emis_GHG.loc["mass loss","CH4-C"]=C_loss*0.00001*1000

        #assuming asumptions of N2O according to Krommweh et al. 2020-->emissions 0.06 mg/kg FM (g/ton FM)
        emission_N2O_rate=0.06 #mg/kg FM --> g/ton FM
        #emissions of N2O
        Emis_N2O=pd.DataFrame(index=["mass loss"])
        Emis_N2O.loc["mass loss","N2O"]=self.I["Q"][0]*emission_N2O_rate/1000
        #conversion to N2O-N
        Emis_GHG.loc["mass loss","N2O-N"]=convert_molmasses(Emis_N2O, to="element").loc["mass loss","N2O-N"]
            
        Emis=pd.concat([Emis, Emis_GHG])
        return Emis

    def output(self):

        Output=self.mass_loss()
        #changes in N
        initial_amount_N=self.I.copy()["N"][0]*self.I.copy()["Q"][0] #kg/ton FM
        final_amount_N=initial_amount_N-self.emissions().loc["mass loss", "N2O-N"]
        #final_amount_N=initial_amount_N-self.emissions().reset_index(level=0, drop=True).loc["mass loss", "N2O-N"]
        Output["N"][0]=final_amount_N/Output["Q"][0]

        #Density change
        if self.I.index=="tree leaves":
            if self.method in ["bales", "silobag"]: #NOTE: Check if the density change would apply only for bales or also to silobags
                Output["density"][0]=0.6 #600 kg/m3 according to Fraunhofer (2019), Figure 11 (p.39)

        return Output

    def cashflow(self):
        """
        |  All cashflows for the investment (machinery) and operation of leaf ensiling
        |  **NOTE:**
        |  - These costs are not yet related to the production quantities.
        """
        Cashflow_capex = pd.DataFrame(columns=["EUR", "period_years", "section"])
        Cashflow_opex = pd.DataFrame(columns=["EUR", "period_years", "section"])
        inventory=Bacenetti_2015.Table3() #Based on Bacenetti & Fusi (2015) - Inventory is based on functional unit (FU), assumed to be fresh mass in this case
        #NOTE: all units of the inventory are set in kg/FU. 
        
        if self.method in ["silobag", "bales"]: 
            #NOTE: Check whether bales and silobags methods would have similaries in inventories
            #capex - cost of tractor purchase
            PRICE_TRACTOR=70000 #EUR. More info: https://www.traktorpool.de/gebraucht/a-Erntemaschinen/6/b-Pressen/168/c-Krone/58/model/Comprima+CF+155+XC/
            WEIGHT_TRACTOR=2500 #kg 

            PRICE_BALER=20000 #EUR. More info: https://www.traktorpool.de/gebraucht/a-Erntemaschinen/6/b-Pressen/168/c-Claas/39/model/Variant+385+RC/ 
            WEIGHT_BALER=2600 #kg

            #NOTE: inventory data of machinery (tractor/baler) is given in kg/FU and has to be divided by the approx weight of one single tractor
            Cashflow_capex.loc["machinery investment","EUR"]=inventory.loc["Tractors", "Silobag"]*self.I["Q"][0]*(PRICE_TRACTOR/WEIGHT_TRACTOR + PRICE_BALER/WEIGHT_BALER)
            if Cashflow_capex.loc["machinery investment","EUR"]<(PRICE_TRACTOR + PRICE_BALER): #switch en case the investment is lower than the price of one unit of machinery
                Cashflow_capex.loc["machinery investment","EUR"]=PRICE_TRACTOR + PRICE_BALER
            Cashflow_capex["period_years"] = 20
            Cashflow_capex["section"] = "capex"

            #opex - cost of materials:
            PRICE_HDPE_FILM=InputPrice().HDPE_film()["Q"][0]/1000 #EUR/kg 
            DIESEL_DENSITY=0.85 #kg/liter
            PRICE_DIESEL=InputPrice().diesel()["Q"][0] #EUR per liter
            PRICE_LUBRICANT_OIL=InputPrice().lubricating_oil()["Q"][0] #NOTE: Dummy value
            PRICE_IMPLEMENTS=0 #NOTE: Check whether 

            Cashflow_opex.loc["HDPE film", "EUR"]=inventory.loc["Plastic film", "Silobag"]*self.I["Q"][0]*PRICE_HDPE_FILM
            Cashflow_opex.loc["diesel consumption", "EUR"]=inventory.loc["Diesel fuel", "Silobag"]*self.I["Q"][0]/DIESEL_DENSITY*PRICE_DIESEL
            Cashflow_opex.loc["lubricating oil", "EUR"]=inventory.loc["Lubricant oil", "Silobag"]*self.I["Q"][0]*PRICE_LUBRICANT_OIL
            Cashflow_opex.loc["implementation", "EUR"]=inventory.loc["Implement", "Silobag"]*self.I["Q"][0]*PRICE_IMPLEMENTS
            Cashflow_opex["period_years"] = 1
            Cashflow_opex["section"] = "opex"

        if self.method in ["silo bunker - covered - Bacenetti & Fusi", "silo bunker - covered - Holmes & Muck", "silo bunker - no cover"]: #Based on Bacenetti & Fusi (2015) findings.
            #capex - cost of tractor purchase
            PRICE_CONCRETE=100#NOTE: Dummy value
            PRICE_TRACTOR=10000 #NOTE: Dummy value
            Cashflow_capex.loc["concrete","EUR"]=inventory.loc["Concrete", "Bunker silos"]*self.I["Q"][0]*PRICE_CONCRETE
            Cashflow_capex.loc["tractor purchase","EUR"]=inventory.loc["Tractors", "Silobag"]*self.I["Q"][0]*PRICE_TRACTOR
            Cashflow_capex["period_years"] = 20
            Cashflow_capex["section"] = "capex"

            #opex - cost of materials:
            PRICE_HDPE_FILM=InputPrice().HDPE_film()["Q"][0]/1000 #EUR/kg 
            DIESEL_DENSITY=0.85 #kg/liter
            PRICE_DIESEL=InputPrice().diesel()["Q"][0] #EUR per liter
            PRICE_LUBRICANT_OIL=InputPrice().lubricating_oil()["Q"][0]
            PRICE_IMPLEMENTS=0 #NOTE: Check whether none cost would apply

            Cashflow_opex.loc["HDPE film", "EUR"]=inventory.loc["Plastic film", "Bunker silos"]*self.I["Q"][0]*PRICE_HDPE_FILM
            Cashflow_opex.loc["diesel consumption", "EUR"]=inventory.loc["Diesel fuel", "Bunker silos"]*self.I["Q"][0]/DIESEL_DENSITY*PRICE_DIESEL
            Cashflow_opex.loc["lubricating oil", "EUR"]=inventory.loc["Lubricant oil", "Bunker silos"]*self.I["Q"][0]*PRICE_LUBRICANT_OIL
            Cashflow_opex.loc["implementation", "EUR"]=inventory.loc["Implement", "Bunker silos"]*self.I["Q"][0]*PRICE_IMPLEMENTS
            Cashflow_opex["period_years"] = 1
            Cashflow_opex["section"] = "opex"
           
        if self.method not in ["silobag", "silo bunker - covered - Bacenetti & Fusi", "silo bunker - covered - Holmes & Muck", "silo bunker - no cover"]:
            raise warnings.WarningMessage (f"The method {self.method} has not yet been specified for inventory emissions calculation, only emissions from mass loss will be calculated")

        return pd.concat([Cashflow_capex, Cashflow_opex])