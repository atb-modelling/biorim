import pandas as pd
import numpy as np
import warnings

from read import Song_Guo_2011, Peters_2015, FernandezLopez_2015, IPCC, Ecoinvent, Ro_2010
from tools import prod, get_content
from material import ConcreteBasePlate
from declarations import Emissions, Process
from parameters import ConversionFactor

class PyrolysisPlant(Process):
    
    """     
    | The PyrolysisPlant converts biogenic material into biochar.
    | The assumption is that biochar is the main product, therefore no additional syngas, tar or heat are produced
    
    :type I: Product
    :param I: Input to the Pyrolysis Plant
    :type heating_temp: numeric
    :param heating_temp: heating temperature of substrate within the plant (in °C)
    :type ton_DM_per_year: numeric  
    :param ton_DM_per_year: mass (in t) which is generally processed per year
    :type CH4_content_exhaust: numeric
    :param CH4_content_exhaust: share of CH4 in exhaust (e.g. 0.0001)

    |
    """

    LONGEVITY_PYREG_YEARS = 10 # 10 years of longevity, general assumption # subprocess.Popen(datapath("PYREG\Das PYREG Verfahren - Pflanzenkohle und ihre Einsatzmöglichkeiten.pdf"), shell=True) page 9 (Investionskosten)
    LONGEVITY_BASEPLATE_YEARS = 25

    MAX_TON_DM_PER_YEAR = 750
    MAX_UTILIZATION_HOURS_PER_YEAR = 7500 
    PLANT_WEIGHT_TON = 14 # without gaseous emission treatment module (another 4000 t, retrieved both values from telephone call with PYREG company) # subprocess.Popen(datapath("PYREG\pyreg_website_26_06_2019.pdf"),shell=True)
    ELECTRICITY_REQUIREMENT_KW = 16
    LOADING_TIME_H_PER_TON = 0.352

    def __init__(self,
                 I = prod("broiler manure", 1, "ton"),
                 heating_temp = 550,
                 ton_DM_per_year = 750,
                 method = "Lopez_Song",
                 CH4_content_exhaust = 0.0001,
                 id = None):

        self.I = get_content(I, required=["DM", "oDM", "orgC", "N", "P", "K"])
        self.HEATING_TEMP = heating_temp
        self.TON_DM_PER_YEAR = ton_DM_per_year # 750 ton/a maximum feasible for the PYREG P500
        self.CH4_CONTENT_EXHAUST = CH4_content_exhaust
        self.method = method
        self.id = id

    def _use_time_h(self): # in hours
        SUBSTRATE_PROCESSING_TON_DM_PER_H = self.MAX_TON_DM_PER_YEAR/self.MAX_UTILIZATION_HOURS_PER_YEAR # substrate beeing processed per time [ton/h]
        return (self.I["mass"]*self.I["DM"]).sum() / SUBSTRATE_PROCESSING_TON_DM_PER_H # time the plant is beeing used for processing the substrate

    def additional_input(self):
        ELECTRICITY_CONSUMPTION_kWh = self.ELECTRICITY_REQUIREMENT_KW * self._use_time_h()
        return prod('electrical energy', ELECTRICITY_CONSUMPTION_kWh, "kWh", location="DEU", source='external.Electricity')

    def output(self):
        Input = self.I.copy()
        Output = prod("broiler manure pyrochar")

        # Libra_2014 = read.Libra_2014.pyrolysis_material_characteristics()
        # Cimo_2014 = read.Cimo_2014.material_characteristics()
        TempYieldData = Song_Guo_2011.material_characteristics()

        if not self.HEATING_TEMP in TempYieldData.index:
            nearest_temp_SongGuo = min(TempYieldData.index.tolist(), key=lambda x:abs(x-self.HEATING_TEMP))
            warnings.warn("HEATING_TEMP " + str(self.HEATING_TEMP) + "°C has been adjusted to " + nearest_temp_SongGuo + "°C which is the nearest value for which data is available.")
            YieldData = TempYieldData.loc[nearest_temp_SongGuo,:]
        else:
            YieldData = TempYieldData.loc[self.HEATING_TEMP,:]

        ## New calculations based on Song, W., Guo, M., 2012. Quality variations of poultry litter biochar generated at different pyrolysis temperatures. J. Anal. Appl. Pyrolysis 94, 138–145. https://doi.org/10.1016/j.jaap.2011.11.018
        Output["DM"] = 1 # assumption that char does not include considerable amount of water, therefore DM = 1
        Output["Q"] = (Input["Q"] * Input["DM"]).sum() * YieldData["Biochar yield [%]"]/100
        Output["mass"] = Output["Q"]

        Output["ash"] = (Input["Q"] * Input["DM"] *(1-Input["oDM"])).sum() / (Output["Q"]).sum() # ash content consistent with the input oDM definition
        Output["oDM"] = 1 - Output["ash"]

        Output["orgC"] = ((Input["Q"] * Input["DM"] * Input["oDM"] * Input["orgC"]).sum() * YieldData["Feed OC retention [%]"]/100) / (Output["Q"] * Output["DM"] * Output["oDM"])
        Output["N"] = ((Input["Q"] * Input["N"]).sum() * YieldData["Feed N retention [%]"]/100) / Output["Q"]

        # define permanence in soil
        if self.HEATING_TEMP <= 450:
            F_permp = IPCC.fraction_of_biocharC_remaining_in_soil().loc["Low (350-450 °C)","Value for Fpermp"] # "Medium temperature pyrolysis (450-600 °C)" to be made dynamically
        elif self.HEATING_TEMP < 600:
            F_permp = IPCC.fraction_of_biocharC_remaining_in_soil().loc["Medium temperature pyrolysis (450-600 °C)","Value for Fpermp"] # "Medium temperature pyrolysis (450-600 °C)" to be made dynamically
        elif self.HEATING_TEMP >= 600:
            F_permp = IPCC.fraction_of_biocharC_remaining_in_soil().loc["High temperature pyrolysis and gasification (> 600 °C)","Value for Fpermp"] # "Medium temperature pyrolysis (450-600 °C)" to be made dynamically
        Output.loc[:,"F_permp"] = F_permp
        
        ## NOTE: Unclear what happens to P and K

        # no other processes and/or outputs defined yet that differ very much from the ones in the storage class (parental class)
        Output.rename(index = {'broiler manure': 'broiler manure pyrochar'}, inplace=True)

        return Output


    def emissions(self):

        Input = self.I.copy()
        Output = self.output()
        Emis = Emissions(index=(Input.index  + " pyrolysis"))

        ## Calculate water vapor from DM loss
        Emis["H2O"] = ((Input["Q"]*(1-Input["DM"])).sum() - (Input["Q"]*(1-Output["DM"])).sum()) * 1000

        ## carbon and nitrogen losses
        C_MASSLOSS_KG = ((Input["Q"]*Input["DM"]*Input["oDM"]*Input["orgC"]).sum() - (Output["Q"]*Output["DM"]*Output["oDM"]*Output["orgC"]).sum()) * 1000
        N_MASSLOSS = (Input["Q"]*Input["N"]).sum()-(Output["Q"]*Output["N"]).sum()

        # exhaust volume
        # wenn CO2 15 % des Abgases entspricht # similar to the 
        EXHAUST_GAS_KG = C_MASSLOSS_KG / 0.15

        # CH4_CONTENT = 0.0001  # similar to CxHx = 0.007 # % CxHx Abb 2 von Holweg, C., Schill, F., 2010. Machbarkeitsstudie zum Einsatz einer innovativen Technologie zur Bioenergieerzeugung mittels Pyrolyse mit niedrigen Staub- emissionen und hohem CO2-Reduktionspotential.
        # Emis["CH4-C"] = EXHAUST_GAS_KG * CH4_CONTENT
        Emis["CH4-C"] = EXHAUST_GAS_KG * self.CH4_CONTENT_EXHAUST


        CO_CONTENT = 0.001 # = 1000 ppm
        Emis["CO-C"] = EXHAUST_GAS_KG * CO_CONTENT

        # all remaining C converted to CO2
        Emis["CO2-C"] = C_MASSLOSS_KG - Emis["CH4-C"] - Emis["CO-C"]

        GAS_CONSTANT = 8.3145 # m3⋅Pa⋅K−1⋅mol−1
        ABSOLUTE_TEMP = 273 #K
        PRESSURE = 101325 # Pa
        EXHAUST_VOLUME_M3 = EXHAUST_GAS_KG.sum().sum() * 1000 / 12 * GAS_CONSTANT * ABSOLUTE_TEMP / PRESSURE

        # print("Exhaust volume:", EXHAUST_VOLUME_M3)

        NOx_CONTENT = 500 # NOx [mg*m-3]

        Emis["NOx-N"] = EXHAUST_VOLUME_M3 * NOx_CONTENT * 1e-6 * ConversionFactor.loc[("NOx","NOx-N"), "factor"]

        # the remainder of the N is assumed to be converted to N2
        Emis["N2-N"] = N_MASSLOSS - Emis["NOx-N"]

        CAPACITY_UTILIZATION = self.TON_DM_PER_YEAR/ self.MAX_TON_DM_PER_YEAR

        ## Emissions for the pyrolysis machinery
        PYROLYSIS_PLANT_WEAR =  self._use_time_h() / (self.MAX_UTILIZATION_HOURS_PER_YEAR * CAPACITY_UTILIZATION * self.LONGEVITY_PYREG_YEARS)  # share of lifetime used for processing the substrate and whole lifetime -> wear
        SPECIFIC_WEIGHT_KG = PYROLYSIS_PLANT_WEAR * self.PLANT_WEIGHT_TON * 1e3 # refer to weight of plant -> "specific weight"

        PlantConstructionEmis = SPECIFIC_WEIGHT_KG * Ecoinvent.Ecoinvent34("agricultural machinery production, unspecified", "agricultural machinery, unspecified", unit="kg", location="RoW").emissions()
        PlantConstructionEmis.index = ["pyrolysis unit wear"]
        Emis = Emis.append(PlantConstructionEmis)

        ## Emissions for base plate
        BASEPLATE_USE_TO_LIFETIME = self._use_time_h() / (self.MAX_UTILIZATION_HOURS_PER_YEAR * CAPACITY_UTILIZATION * self.LONGEVITY_BASEPLATE_YEARS)
        BasePlateEmis = ConcreteBasePlate(length = 9, width = 3, thickness = 0.3, use_to_lifetime = BASEPLATE_USE_TO_LIFETIME).emissions()
        BasePlateEmis.index = ["baseplate wear"]
        Emis = Emis.append(BasePlateEmis)

        ## Emissions from loading
        ACT_LOADING_H = self.LOADING_TIME_H_PER_TON * Input['Q'].sum() # time for loading the substrate into the plant
        LoadingEmis = ACT_LOADING_H * Ecoinvent.Ecoinvent34("machine operation, diesel, >= 74.57 kW, low load factor", "machine operation, diesel, >= 74.57 kW, low load factor", unit="h", location="GLO").emissions()
        LoadingEmis.index = ["substrate loading"]
        Emis = Emis.append(LoadingEmis)

        return Emis

