## possible development steps:
# at the moment the electrical energy that is produced is already considered under emissions (with negative sign). This should possibly be moved to outside the function.


import pandas as pd
import numpy as np
import math

from read import KTBL, LfL_BY, VDLUFA, Jurgutis_et_al_2021
from read.Prices import InputPrice, LandPrice
from read.Ecoinvent import Ecoinvent34
from declarations import Product, Emissions, Process, Costs
from parameters import GasDensity, ConversionFactor
from tools import convert_molmasses, get_content, prod, O_from_massloss

from scipy import stats

class BiogasPlant(Process):
    """
    Represents biogas production in a biogas plant, and calculates emissions and outputs based on the specified input (I).
    Considered are emissions occuring during the different production steps (e.g. from digester and poststorage).
    Two options of biogas usage are currently implemented: combustion of biogas to produce energy (CHP plant), or direct biogas feed in (biomethane)
    The calculations are largely based on the KTBL calculation example: ("Rechenbeispiel-Biogas.xls").

    :param I: substrates put into the biogas plant (e.g. maize silage, broiler manure)
    :param residue_storage_type: "closed" storage or an "open" residue storage
    :param energy_usage_type: "biomethane" production or biogas usage within a "CHP plant"
    :param thermal_energy_use: share of potential thermal energy used
    :param CHP_kW: size of CHP plant in terms of kWh
    :param CHP_kW_reshape: when true, resizes the power of the CHP plant according to the feedstock output to a maximum capacity (plant_resize_shape function)
    :param CH4_loss_conv_method: source for CH4 emissions from conversion. either "KTBL" or ("Liebetrau_2010","low" or "mean" or "high")
    :param TAN_change: the relative change in TAN input to output (e.g. value of 1.5 represents a 50% increase) before losses
    :param year_assessment: year of assessment for determining the feed-in-tariff
    :param lifetime: number of years of the plant's lifespam
    :param land_purchase: when true, land purchase is considered in the cashflow
    :param rent: when true, land rent is considered in the cashflow
    :param EEG_tariff: set the feed-in-tariff based on the version of the EEG (Law for Renewable Energies in Germany)
    :param labour_costs: method for calculation of labour cost in the biogas plant
    :param maintenance_costs: method for calculation of maintenance cost in the biogas plant
    :param feedstock_allocation: if I consists in two or more feedstocks, when true, the calculations will be based on the first feedstock
    
    |  
    """

    # default biogas plant parameters
    RESIDUAL_GAS_POTENTIAL = KTBL.biogas_fist_numbers().loc["residual gas potential","value"]
    #NOTE: CHP_EFFICIENCY_ELECTRICAL will be defined manually for enable sensitivity analysis
    #CHP_EFFICIENCY_ELECTRICAL = KTBL.biogas_fist_numbers().loc["CHP efficiency, electrical","value"]
    CHP_EFFICIENCY_THERMAL = KTBL.biogas_fist_numbers().loc["CHP efficiency, thermal","value"]
    ENGINE_OIL_CONSUMPTION = KTBL.biogas_fist_numbers().loc["engine oil consumption","value"]
    INTERNAL_ELECTRICITY_USE = KTBL.biogas_fist_numbers().loc["internal electricity use","value"]

    CH4_LEAKAGE_FERMENTER = KTBL.biogas_climate_balances_BEK().loc["CH4 leakage fermenter","value"]
    CH4_LEAKAGE_CLOSED_POSTSTORAGE = KTBL.biogas_climate_balances_BEK().loc["CH4 leakage closed poststorage","value"]
    MCF_OPEN_DIGESTATE_STORAGE = KTBL.biogas_climate_balances_BEK().loc["MCF open digestate storage","value"]
    N2ON_EF_OPEN_DIGESTATE_STORAGE = KTBL.biogas_climate_balances_BEK().loc["N2O-N EF open poststorage","value"]
    NH3N_EF_OPEN_DIGESTATE_STORAGE = KTBL.biogas_climate_balances_BEK().loc["NH3-N EF open poststorage","value"]
    CH4_LEAKAGE_CHP = KTBL.biogas_climate_balances_BEK().loc["CH4 leakage CHP","value"]
    CH4_LEAKAGE_BIOMETHANE = KTBL.biogas_climate_balances_BEK().loc["CH4 leakage biomethane","value"]
    EF_BIOGAS_PLANT_CONSTRUCTION = KTBL.biogas_climate_balances_BEK().loc["EF biogas plant construction","value"]
    AVERAGE_HYDRATISATION = KTBL.biogas_climate_balances_BEK().loc["average hydratisation","value"]

    UTILIZATION_HOURS = 8000

    def __init__(self,
                 I: Product = prod("broiler manure", 1, "ton"),
                 first_feedstock_analysis: bool = False,
                 residue_storage_type: str = "closed",
                 energy_usage_type: str = "CHP plant", 
                 thermal_energy_use: float = 0.75,
                 operation_hours: int = 8000,
                 CHP_kW: int = 500,
                 CHP_EFFICIENCY_ELECTRICAL: float = 0.38,
                 CHP_kW_reshape: bool = False,
                 CH4_loss_conv_method: str = "KTBL", 
                 TAN_change: float = None,
                 TAN_digestate_kg_per_ton: float = 3,
                 year_assessment: float = 2022,
                 lifetime: int = 20,
                 land_purchase: bool = True,
                 land_rent: bool = False,
                 EEG_tariff: tuple = ("EEG_2021", None), 
                 labour_costs: str = "FNR_2016",
                 thermal_energy_tariff: tuple = ("manual", 0.03),
                 new_infrastructure: bool = True,
                 maintenance_costs: str = "FNR_2016", 
                 feedstock_allocation: bool = False,
                 id: str = None):

        if not all(I["U"] == "ton"):
            raise ValueError("Currently only 'ton' supported as unit")

        self.I = get_content(I, required=["DM", "oDM", "orgC", "N", "P", "K", "B0", "CH4 content"])

        self.first_feedstock_analysis=first_feedstock_analysis
        if self.first_feedstock_analysis==True:
            self.original_I=self.I.copy()
            self.I=self.I[self.I.index.isin([self.I.index[0]])]

        if residue_storage_type in ["open", "closed"]:
            self.residue_storage_type = residue_storage_type
        else:
            raise ValueError(f"residue_storage_type {residue_storage_type} not defined")
        
        if energy_usage_type in ["CHP plant", "biomethane"]:
            self.energy_usage_type = energy_usage_type
        else:
            raise ValueError(f"energy_usage_type {energy_usage_type} not defined")

        self.thermal_energy_use = thermal_energy_use
        self.operation_hours=operation_hours
        self.CHP_kW = CHP_kW
        self.CHP_EFFICIENCY_ELECTRICAL=CHP_EFFICIENCY_ELECTRICAL
        self.CHP_kW_reshape = CHP_kW_reshape 
        self.CH4_loss_conv_method = CH4_loss_conv_method
        self.TAN_change = TAN_change # change in TAN concentration, 1.5 represents 50% increase (for chicken manure similar to Duan et al. 2018)
        self.TAN_digestate_kg_per_ton = TAN_digestate_kg_per_ton # alternative to TAN_change. Set TAN content of digestate directly
        self.year_assessment = year_assessment
        self.lifetime = lifetime
        self.land_purchase = land_purchase
        self.land_rent = land_rent
        self.EEG_tariff = EEG_tariff
        self.thermal_energy_tariff=thermal_energy_tariff
        self.new_infrastructure=new_infrastructure
        self.labour_costs = labour_costs
        self.maintenance_costs = maintenance_costs
        self.feedstock_allocation=feedstock_allocation
        self.id = id

    def massloss_ton(self):
        # if self.first_feedstock_analysis==True:
        #     self.I=self.I[self.I.index.isin([self.I.index[0]])]

        biogas_yield = self.I['B0']/self.I['CH4 content']*1000*self.I['DM']*self.I['oDM'] # [m³/t FM]
        biogas_mass = (self.I['CH4 content']*16 + (1-self.I['CH4 content'])*44)/22.4 # [kg/m³]. Molar mass CH4: 16 g/mol; Molar mass CO2: 44 g/mol; Volume ideal gas: 22.4 l/mol
        massloss_kg_per_tonFM = biogas_yield * biogas_mass # [kg/t FM]
        return (massloss_kg_per_tonFM * self.I["Q"]/1000).sum()

    def attainable_CH4_C_kg(self):
        # if self.first_feedstock_analysis==True:
        #     self.I=self.I[self.I.index.isin([self.I.index[0]])]
        return convert_molmasses(((self.I['Q']*self.I['DM']*self.I['oDM']*self.I['B0']*GasDensity['CH4']*1000).to_frame("CH4")), to="element")

    def microbial_produced_CO2_C_kg(self):
        # if self.first_feedstock_analysis==True:
        #     self.I=self.I[self.I.index.isin([self.I.index[0]])]
        return convert_molmasses(((self.I['Q']*self.I['DM']*self.I['oDM']*self.I['B0']/self.I['CH4 content']*(1-self.I['CH4 content'])*GasDensity['CO2']*1000).to_frame("CO2")), to="element")

    # The overall production potential of CH4-C and CO2-C
    def microbial_C_prod_pot_kg(self):
        return pd.concat([self.attainable_CH4_C_kg(), self.microbial_produced_CO2_C_kg()], axis=1, sort=True).sum()

    # CH4 and CO2 produced in digester (reduced by the amount of residual gas that could be gained in digestate storage)
    def C_prod_digester_kg(self):
        return self.microbial_C_prod_pot_kg()*(1-self.RESIDUAL_GAS_POTENTIAL)

    def TAN_digestate_kg(self):
        # if self.first_feedstock_analysis==True:
        #     self.I=self.I[self.I.index.isin([self.I.index[0]])]

        if self.TAN_change is not None:
            return (self.I["Q"]*self.I["NH4-N"]*self.TAN_change).sum()
        elif self.TAN_digestate_kg_per_ton is not None:
            return self.I["Q"].sum() * self.TAN_digestate_kg_per_ton

    def digester_poststorage_emissions(self):

        Emis = Emissions({'CO2-C': np.nan, 'CH4-C': np.nan}, index=["digester", "poststorage"])

        C_prod_digester_kg = self.C_prod_digester_kg()

        # CH4 anc CO2 emissions from digestate storage (CH4 Emissionen aus Gärrestlager, Zeile 134f)
        Emis.loc["digester"] = C_prod_digester_kg * self.CH4_LEAKAGE_FERMENTER

        if self.residue_storage_type == "closed":
            Emis.loc["poststorage", "CH4-C"] = self.attainable_CH4_C_kg().sum().sum() * self.RESIDUAL_GAS_POTENTIAL * self.CH4_LEAKAGE_CLOSED_POSTSTORAGE
            Emis.loc["poststorage", "CO2-C"] = self.microbial_produced_CO2_C_kg().sum().sum() * self.RESIDUAL_GAS_POTENTIAL * self.CH4_LEAKAGE_CLOSED_POSTSTORAGE

            Emis.loc["poststorage","N2O-N"]  = 0
            Emis.loc["poststorage","NH3-N"] = 0

        elif self.residue_storage_type == "open":
            Emis.loc["poststorage", "CH4-C"] = self.attainable_CH4_C_kg().sum().sum() * self.RESIDUAL_GAS_POTENTIAL*self.MCF_OPEN_DIGESTATE_STORAGE
            Emis.loc["poststorage", "CO2-C"] = self.microbial_produced_CO2_C_kg().sum().sum() * self.RESIDUAL_GAS_POTENTIAL*self.MCF_OPEN_DIGESTATE_STORAGE

            Emis.loc["poststorage","N2O-N"] = (self.I['Q'] * self.I['N']).sum() * self.N2ON_EF_OPEN_DIGESTATE_STORAGE
            Emis.loc["poststorage","NH3-N"] = self.TAN_digestate_kg() * self.NH3N_EF_OPEN_DIGESTATE_STORAGE
        
        return Emis

    def CH4_C_for_conversion(self):
        Emis = self.digester_poststorage_emissions()
        C_prod_digester_kg = self.C_prod_digester_kg()
        if self.residue_storage_type == "closed":
            C_prod_poststorage = self.microbial_C_prod_pot_kg() * self.RESIDUAL_GAS_POTENTIAL
            CH4_C_for_conversion = C_prod_digester_kg.loc["CH4-C"] - Emis.loc["digester","CH4-C"] + C_prod_poststorage.loc["CH4-C"] - Emis.loc["poststorage","CH4-C"]
        elif self.residue_storage_type == "open":
            CH4_C_for_conversion = C_prod_digester_kg.loc["CH4-C"] - Emis.loc["digester","CH4-C"]
        return CH4_C_for_conversion

    def CHP_CH4_C_leakage(self):
        Emis = Emissions()
        if self.CH4_loss_conv_method=="KTBL":
            return self.CH4_C_for_conversion() * self.CH4_LEAKAGE_CHP
        elif self.CH4_loss_conv_method.split(".").item()=="Liebetrau_2010":
            CH4_Leakage = {"low": 0.0017,
                            "mean": 0.017,
                            "high": 0.0372}
            return self.CH4_C_for_conversion() * CH4_Leakage[self.CH4_loss_conv_method.split(".")[1]]
        else:
            raise ValueError(f"CH4_loss_conv_method {self.CH4_loss_conv_method}")
        return Emis

    def energy_prod_potential_kWh(self, type="electrical"):

        USABLE_CH4C = self.CH4_C_for_conversion() - self.CHP_CH4_C_leakage()

        LOWER_HEATING_VALUE_CH4 = 13.8925 # kWh/kg CH4

        biogas_energy = USABLE_CH4C * ConversionFactor.loc[("CH4-C","CH4"), "factor"] * LOWER_HEATING_VALUE_CH4

        electrical_energy = pd.DataFrame({'Q' : biogas_energy * self.CHP_EFFICIENCY_ELECTRICAL, 'U' : "kWh"}, index=["electrical energy"])
        thermal_energy = pd.DataFrame({'Q' : biogas_energy * self.CHP_EFFICIENCY_THERMAL, 'U' : "kWh"}, index=["thermal energy"])

        if type=="electrical":
            return electrical_energy
        elif type=="thermal":
            return thermal_energy
        elif type=="thermal+electrical":
            return pd.concat([electrical_energy, thermal_energy])
        else:
            raise KeyError("Specified type does not exist")


    def oDM_requirement(self):
        """
        Organic dry matter (oDM) requirements for the current biogas plant based Faustzahlen Biogas (FNR, 2013), p.79. Assumed to be a linear regression. 
        Electricity capacity originally expressed in kWel and must be converted to kW installed. 
        """

        #Based on FNR(2013) - Faustzahlen Biogas 2013. p.79. Assumed to be a linear regression. Electricity capacity in expressed in kWel
        #and has to be converted to kW installed
        x=[150/self.CHP_EFFICIENCY_ELECTRICAL, 500/self.CHP_EFFICIENCY_ELECTRICAL] #electrical capacity in kW installed
        y=[1015, 3015] #tonnes of oDM per year

        slope, intercept, r, p, std_err=stats.linregress(x,y)

        oDM_year=slope*self.CHP_kW+intercept #in tonnes/year.

        #print(f"The requirement for organic dry matter is {oDM_year} tonnes per year")
        return oDM_year
    
    def supply_information(self, method="electricity potential"):
        #determining the amount of feedstock supply per year ussing cross-multiplication
        #total amount per year (wet) =(current wet biomass*ODM_year)/current_ODM

        supply_information=pd.DataFrame(index=[],columns=[])
        if method=="oDM requirement":
            current_ODM=(self.I["Q"]*self.I["DM"]*self.I["oDM"]).sum()
            current_biomass_wet=(self.I["Q"]).sum()
            oDM_year=self.oDM_requirement()
            biomass_wet_year=current_biomass_wet*oDM_year/current_ODM

            supply_information.loc["fuel one year (wet)", "Q"]=biomass_wet_year
            supply_information.loc["fuel one year (wet)", "U"]="ton"
            supply_information.loc["supply time of feedstock (wet)", "Q"]=(current_biomass_wet/biomass_wet_year)*365
            supply_information.loc["supply time of feedstock (wet)", "U"]="day"
            supply_information.loc["total amount units from feedstock (wet)", "Q"]=math.ceil(current_biomass_wet/supply_information.loc["fuel one year (wet)", "Q"])
            supply_information.loc["total amount units from feedstock (wet)", "U"]="unit"
        
        elif method=="electricity potential":
            electrical_energy=self.energy_prod_potential_kWh().loc["electrical energy","Q"]
            electrical_energy_in_CHP_kW=self.CHP_kW*self.operation_hours*self.CHP_EFFICIENCY_ELECTRICAL #maximum theoritical electrical energy potential at full capactiy a year

            # supply_information.loc["fuel one year (wet)", "Q"]=biomass_wet_year
            # supply_information.loc["fuel one year (wet)", "U"]="ton"
            supply_information.loc["supply time of feedstock (wet)", "Q"]=(electrical_energy/electrical_energy_in_CHP_kW)*365
            supply_information.loc["supply time of feedstock (wet)", "U"]="day"
            supply_information.loc["total amount units from feedstock (wet)", "Q"]=math.ceil(electrical_energy/electrical_energy_in_CHP_kW)
            supply_information.loc["total amount units from feedstock (wet)", "U"]="unit"

        return supply_information

    def lifetime_share(self):
        """
        Returns the share (0 to 1) of the amount of feedstock passed would supply the operation of the biogas unit over
        the amount of feedstock that would be processed for the lifetime period at full capacity.
        """
        supply_info=self.supply_information()
        LIFETIME_SHARE=supply_info.loc["supply time of feedstock (wet)", "Q"]/(365*self.lifetime)
        return LIFETIME_SHARE


    def operation_share(self):
        """
        Returns the share (0 to 1) of the amount of feedstock passed would supply the operation of the biogas unit over
        the amount of feedstock that would be processed for one year at full capacity.
        """
        supply_info=self.supply_information()
        if supply_info.loc["supply time of feedstock (wet)", "Q"]<365:
            OPERATION_SHARE=supply_info.loc["supply time of feedstock (wet)", "Q"]/365
        elif supply_info.loc["supply time of feedstock (wet)", "Q"]>=365: 
            OPERATION_SHARE=supply_info.loc["supply time of feedstock (wet)", "Q"]/365
            print(f"The current amount of feedstock exceed the requirement for one year of operation")
        return OPERATION_SHARE

    #NOTE: include a check when the amount of biogas passed is greater than the one that could processed within a year of full operation?

    def plant_size_reshape(self):
        if self.CHP_kW_reshape==True:
            electrical_energy=self.energy_prod_potential_kWh().loc["electrical energy","Q"]
            self.CHP_kW=electrical_energy/(self.operation_hours*self.CHP_EFFICIENCY_ELECTRICAL)
        return self.CHP_kW

    def ignition_oil_consumption_l(self):
        IGNITION_OIL_L_PER_KWH = 0.0092 # equals 2.3 litre per 250 kWh electricity, similar to example in Aschmann, V., Kissel, R., Effenberger, M., Eichelser, R., Gronauer, A., 2007. Effizienzsteigerung, Emissionsminderung und CO2-Einsparung durch optimierte Motor- einstellung bei Biogas-Blockheizkraftwerken zur dezentralen Stromversorgung. Bayerisches Landesanstalt für Landwirtschaft (LfL). Table 6
        return IGNITION_OIL_L_PER_KWH * self.energy_prod_potential_kWh("electrical")["Q"].item()

    def engine_oil_consumption_kg(self):
        return self.ENGINE_OIL_CONSUMPTION * self.energy_prod_potential_kWh(type="electrical")["Q"].item()

    def conversion_emissions(self):
        preEmis = self.digester_poststorage_emissions()
        Emis = Emissions()
        CH4_C_for_conversion = self.CH4_C_for_conversion()

        if (self.energy_usage_type == "biomethane"):
            Emis.loc["conversion","CH4-C"] = CH4_C_for_conversion * self.CH4_LEAKAGE_BIOMETHANE
        elif (self.energy_usage_type == "CHP plant"):
            Emis.loc["CHP methane leakage","CH4-C"] = self.CHP_CH4_C_leakage()

            Emis.loc["CHP methane combustion", "CO2-C"] = CH4_C_for_conversion - Emis.loc["CHP methane leakage","CH4-C"] # CH4 is converted to CO2
            Emis.loc["CHP methane combustion", "H2O"] = Emis.loc["CHP methane combustion", "CO2-C"] / 12 * 18 * 2   # Per molecule of CO2 two H2O molecules are created as well
            Emis.loc["CHP CO2 from biogas", "CO2-C"] = self.microbial_produced_CO2_C_kg().sum().item() - preEmis.loc["digester", "CO2-C"] - preEmis.loc["poststorage", "CO2-C"]

            KG_CO2_PER_L_FUEL = 2.67
            Emis.loc["CHP ignition oil combustion", "CO2-C"] = self.ignition_oil_consumption_l() * KG_CO2_PER_L_FUEL * ConversionFactor.loc[("CO2","CO2-C"), "factor"]
            Emis.loc["CHP ignition oil combustion", "H2O"] = Emis.loc["CHP ignition oil combustion", "CO2-C"] / 12 * 18

            ## Calculation of the exhaust volume:
            # calculations based on: Aschmann, V., Kissel, R., Effenberger, M., Eichelser, R., Gronauer, A., 2007. Effizienzsteigerung, Emissionsminderung und CO2-Einsparung durch optimierte Motoreinstellung bei Biogas-Blockheizkraftwerken zur dezentralen Stromversorgung. Bayerisches Landesanstalt für Landwirtschaft (LfL).

            MolarWeight_g_per_mol = pd.Series({'CH4-C': 12,
                                    'CO2-C': 12,
                                    'H2O': 18})

            EmissionsMol = Emis * 1000 / MolarWeight_g_per_mol

            LAMBDA = 1.5 # incomplete combustion (more O2 used than needed). Similar to values use in Aschmann et al. 2007
            O2_SHARE_AIR = 0.2095
            N2_SHARE_AIR = 0.7905
            GAS_CONSTANT = 8.3145 # m3⋅Pa⋅K−1⋅mol−1
            ABSOLUTE_TEMP = 273 #K
            PRESSURE = 101325 # Pa

            EmissionsMol.loc["CHP ignition oil combustion", "O2"] = EmissionsMol.loc["CHP ignition oil combustion", "CO2-C"] * 1.5 * LAMBDA    # ignition oil combustion process: CnH2n + 1,5n O2 → n CO2 + n H2O
            EmissionsMol.loc["CHP methane combustion", "O2"] = EmissionsMol.loc["CHP methane combustion", "CO2-C"] * 2 * LAMBDA   # methane combustion: CH4 + 2 O2 → CO2 + 2 H2O

            EmissionsMol["N2"] = EmissionsMol["O2"] / O2_SHARE_AIR * N2_SHARE_AIR

            CHP_EXHAUST_VOLUME_M3 = EmissionsMol.sum().sum() * GAS_CONSTANT * ABSOLUTE_TEMP / PRESSURE

            NOx_CONTENT = 1000 # NOx [mg*m-3]

            Emis.loc["CHP methane combustion", "NOx-N"] = CHP_EXHAUST_VOLUME_M3 * NOx_CONTENT * 1e-6 * ConversionFactor.loc[("NOx","NOx-N"), "factor"]

            # if (self.CH4_loss_conv_method == "Aschmann"):
            #     CnHm = 500 # mg /m³
            #     Emis.loc["CHP methane leakage","CH4-C"] = CHP_EXHAUST_VOLUME_M3 * CnHm * 1e-6 * 0.9 * ConversionFactor.loc[("CH4","CH4-C"), "factor"]

        return Emis


    def construction_emissions(self):   # additional machinery (as for example usage of a wheel loader) are considered via inputs

        Emis = Emissions({'CO2': np.nan}, index=["construction"])
        if self.new_infrastructure==True:
            Emis.loc["construction", "CO2"] = self.energy_prod_potential_kWh(type="electrical")["Q"].item() * self.EF_BIOGAS_PLANT_CONSTRUCTION # emissions from constructing a biogas plant (l.221 ff. & values from BEK (2016), p.40)
        else:
            Emis.loc["construction", "CO2"] = 0
            
        return convert_molmasses(Emis, to="element")

    def wheel_loader_emissions(self):

        # wheel loader for distribution of substrate - e.g. from storage to digester
        LOADER_TIME = 0.5 # hours per day for a 250 kW CHP - https://www.fh-muenster.de/fb4/downloads/personen/wetter/Broschuere_Biogas_Endfassung.pdf
        oDM = (self.I['Q']*self.I['DM']*self.I['oDM']).item()
        ACT_HOURS_FACTOR = oDM/(6.3*self.CHP_kW) #6.3 empirical factor based on Faustzahlen Biogas, p.79 ratio oDM - CHP_kW  
        TIME_H = LOADER_TIME * 365 * ACT_HOURS_FACTOR

        LoaderEmis = TIME_H * Ecoinvent34('market for machine operation, diesel, >= 18.64 kW and < 74.57 kW, high load factor',
                                    'machine operation, diesel, >= 18.64 kW and < 74.57 kW, high load factor',
                                    unit= "h",
                                    location="GLO").emissions()
        LoaderEmis.index = pd.Index(["wheel loader"])
        return LoaderEmis

    def emissions(self):
        Emis = pd.concat([self.digester_poststorage_emissions(), self.conversion_emissions(), self.construction_emissions(), self.wheel_loader_emissions()], sort=True).sort_index(axis=1)
        return Emis

    def allocation_ratio(self, approach="electricity"):
        """
        Function that determines the allocation of costs for two or more co-substrates, using main approach the potential electrical energy from each feedstock.
        """
        #Use here the original feedstock
        if approach=="electricity":
            if len(self.original_I.index)>1:
                elec_prod=[]
                for item in self.original_I.index:
                    elec_prod.append(BiogasPlant(self.original_I[self.original_I.index.isin([item])]).energy_prod_potential_kWh())
        #NOTE: Other methods be also introduced, for example, based on the amount of feedstock that it is being inputed

        return elec_prod[0]["Q"].item()/(elec_prod[0]["Q"].item()+elec_prod[1]["Q"].item())

    def additional_input(self):  # any additional inputs that are needed (co-substrates, water, energy, lubricates, etc.) should be added here

        Input = Product()

        if self.energy_usage_type == "CHP plant":
            EXTERNAL_ELECTRICAL_ENERGY = self.energy_prod_potential_kWh(type="electrical")["Q"].item() * self.INTERNAL_ELECTRICITY_USE

            DENSITY_RAPESEED_OIL = 0.913 # kg/l
            IGNITION_OIL_CONSUMPTION_KG = self.ignition_oil_consumption_l() * DENSITY_RAPESEED_OIL 

            # the prechain emissions from the engine oil
            ENGINE_OIL_CONSUMPTION_KG = self.engine_oil_consumption_kg()

            Input = pd.concat([Input,
                               prod('electrical energy',EXTERNAL_ELECTRICAL_ENERGY, "kWh", location="DEU", source='external.Electricity'),
                               prod("rape oil, crude",IGNITION_OIL_CONSUMPTION_KG, "kg", location="CH", activity="market for rape oil, crude", source='external.ExternInput'),
                               prod("lubricating oil",ENGINE_OIL_CONSUMPTION_KG, "kg", location="GLO", activity="market for lubricating oil", source='external.ExternInput')])
            
        return Input

    def output(self, report_sys=True):

        Emis = self.emissions().copy()

        C_MASSLOSS_TON = self.massloss_ton() # massloss due to CH4 and CO2 production
        USAGE_DM_TON = C_MASSLOSS_TON*(1-self.AVERAGE_HYDRATISATION)    # Usage DM for the production of biogas [ton/t DM]

        N_LOSS_TON = Emis.loc[["digester", "poststorage"],["NH3-N","N2O-N"]].sum().sum()/1000

        orgC_LOSS_TON = Emis.loc[["digester", "poststorage", "CHP methane leakage", "CHP methane combustion", "CHP CO2 from biogas"],["CO2-C","CH4-C"]].sum().sum()/1000
        oDM_LOSS_TON = USAGE_DM_TON + N_LOSS_TON # in contrast to KTBL calculations assume this to be the oDM (not DM) loss, and N losses are also considered:

        WATER_CONSUMPTION_TON = C_MASSLOSS_TON * self.AVERAGE_HYDRATISATION # Water Consumption in ton (l.57 ff.)

        # NOTE: otherwise oDM losses would be larger than DM?
        # Output = O_from_massloss(self.I, H2O_LOSS_TON=WATER_CONSUMPTION_TON, DM_LOSS_TON=USAGE_DM_TON, oDM_LOSS_TON=oDM_LOSS_TON, orgC_LOSS_TON=orgC_LOSS_TON, N_LOSS_TON=N_LOSS_TON, scale_by_mass=["P", "K"], index="biogas digestate")
        Output = O_from_massloss(self.I, H2O_LOSS_TON=WATER_CONSUMPTION_TON, oDM_LOSS_TON=oDM_LOSS_TON, orgC_LOSS_TON=orgC_LOSS_TON, N_LOSS_TON=N_LOSS_TON, scale_by_mass=["P", "K"], index="biogas digestate")

        Output.loc["biogas digestate", "NH4-N"] = (self.TAN_digestate_kg() - Emis.loc[["digester", "poststorage"],["NH3-N"]].sum().sum()) / Output["Q"].sum()
       
        if self.energy_usage_type == "biomethane":
            CH4_mass = self.CH4_C_for_conversion()*(1-self.CH4_LEAKAGE_BIOMETHANE)*ConversionFactor.loc[("CH4-C","CH4"), "factor"]
            Output = pd.concat([Output,prod("biomethane", CH4_mass/GasDensity['CH4'], "m³")])
        if self.energy_usage_type == "CHP plant":
            Energy_prod_pot = self.energy_prod_potential_kWh(type="thermal+electrical")
            Energy_used = Energy_prod_pot.copy()
            Energy_used.loc["thermal energy","Q"] = Energy_prod_pot.loc["thermal energy","Q"] * self.thermal_energy_use
            Output = pd.concat([Output,Energy_used])

        Output.loc["biogas digestate", "HEQ"] = VDLUFA.humus_reproduction().loc["digestate liquid", "HEQ"] # NOTE: could introduce difference between liquid (DM < 0.25) and solid digestate (DM > 0.25)
        Output.loc["biogas digestate", "N_fert_eff"] = LfL_BY.nutrition_contents_efficacy().loc["digestate liquid", "min. efficacy (total N)"] # N fertilization efficacy from LfL Bayern

        Output.loc["biogas digestate", "density"] = 1 # NOTE: For the moment just assumed a density of 1 ton / m³


        Output = Product(Output.sort_index(axis=1))

        if report_sys:
            return Output
        else:
            return Output
    
    def validate(self):
       # plausability check of output
       # (range according to: Wendland, M., Lichti, F., 2012. Biogasgärreste. Einsatz von Gärresten aus der Biogasproduktion als Düngemittel. LfL Bayern)
       Digestate = self.output().loc["biogas digestate", :]
       RangeDigNutri = pd.DataFrame({'DM': [0.029,0.132, Digestate["DM"]],
                                     'N': [2.4,9.1, Digestate["N"]],
                                     'P2O5': [0.9,6.0, Digestate["P"]*ConversionFactor.loc[("P2O5-P", "P2O5"), "factor"]],
                                     'K2O': [2.0,10.9, Digestate["K"]*ConversionFactor.loc[("K2O-K", "K2O"), "factor"]]},
                                    index=['min','max', 'calc'])

       if any(RangeDigNutri.loc['max',:] < RangeDigNutri.loc['calc',:]) or any(RangeDigNutri.loc['min',:] > RangeDigNutri.loc['calc',:]):
           print("Calculated digestate contents outside of common range from literature. Manual checking recommended. \n", RangeDigNutri)

    def area_requirements(self):

        x=[75/self.CHP_EFFICIENCY_ELECTRICAL, 1000/self.CHP_EFFICIENCY_ELECTRICAL]
        y=[0.21, 1.65]

        slope, intercept, r, p, std_err=stats.linregress(x,y)

        AREA_REQUIREMENTS=(slope*self.CHP_kW+intercept) * 10000 #in m2
        print(f"The area requirement for a biogas plant of {self.CHP_kW} kW installed is {AREA_REQUIREMENTS} m2")

        return AREA_REQUIREMENTS
    
    def labour_requirement(self):
        """
        Return the total amount of labour (in hours) required for the operation of the biogas plant based in the electrical power
        delivered (kWel) in one year
        """

        #Based in Figure 9.3. of FNR (2016)
        labour_rate=94.556*math.pow(self.CHP_kW*self.CHP_EFFICIENCY_ELECTRICAL, -0.663) # in Labour-hour/kWel*year
        return labour_rate*self.CHP_kW*self.CHP_EFFICIENCY_ELECTRICAL

    def feed_in_tariff(self):

        dict_EEG_21_tarrif={2021:12.60,
                            2022:12.54,
                            2023:12.47,
                            2024:12.41}

        if self.EEG_tariff[0]=="EEG_2012":
        #based on the digression equation derived from "Leitfaden Biogas" by FNR (2016). Part of the EEG
        #tariff in EUR/kWh

            if self.CHP_kW<150:
                tariff=(-0.2662*self.year_assessment+549.8)/100
            elif self.CHP_kW>=150 and self.CHP_kW<500:
                tariff=(-0.2292*self.year_assessment+473.3)/100
            elif self.CHP_kW>=500 and self.CHP_kW<5000:
                tariff=(-0.205*self.year_assessment+423.44)/100
            elif self.CHP_kW>5000:
                tariff=(-0.112*self.year_assessment+231.33)/100

        if self.EEG_tariff[0]=="EEG_2021":
            if self.CHP_kW<150:
                tariff=dict_EEG_21_tarrif[self.year_assessment]/100 #EUR/kWh fixed price (https://biogas.fnr.de/rahmenbedingungen/eeg-2021)
            elif self.CHP_kW>=150:
                tariff=0.1728 # EUR/kWh mean value the bidding in 2022 (https://www.bundesnetzagentur.de/DE/Fachthemen/ElektrizitaetundGas/Ausschreibungen/Biomasse/BeendeteAusschreibungen/Gebotstermin01092022/start.html#:~:text=Gebotstermin%20ist%20der%201.,Geboten%20f%C3%BCr%20eine%20Ausschreibung%20abl%C3%A4uft.)
                #https://www.bundesnetzagentur.de/SharedDocs/Pressemitteilungen/DE/2022/20221012_EE_ausschreibungen.html#:~:text=Die%20Bundesnetzagentur%20hat%20heute%20die,drei%20Gebotsrunden%20waren%20deutlich%20unterzeichnet. 
        
        if self.EEG_tariff[0]=="manual":
            tariff = self.EEG_tariff[1]
        return tariff


    def heating_tariff(self):

        #NOTE: Heat tariff at the moment is under the considerations if the CHP surcharge (Kraft-Wärme-Kopplung Gesetz [KWKG]) and its amendments
        #Information about the tariff here: https://www.amprion.net/Energy-Market/Levies/Combined-Heat-and-Power-Act/CHP-surcharges-since-2002.html

        CHP_SURCHARGE=pd.DataFrame.from_dict({2023: 0.00357, 
                                            2022: 0.00378, 
                                            2021: 0.00357, 
                                            2020: 0.00254, 
                                            2019: 0.00226, 
                                            2018: 0.00345, 
                                            2017: 0.00438}, orient="index", columns=["CHP_surcharge"]) # values of surcharhe in EUR/kWh

        if self.energy_usage_type=="CHP plant":
            if self.thermal_energy_tariff[0]=="CHP surcharge":
                tariff=CHP_SURCHARGE.loc[self.year_assessment, "CHP_surcharge"]

            elif self.thermal_energy_tariff[0]=="bidding average":
                tariff=0.0614 #Value for 2022. Source: https://www.bundesnetzagentur.de/DE/Fachthemen/ElektrizitaetundGas/Ausschreibungen/KWK/BeendeteAusschreibungen/KWK1122022/start.html

            elif self.thermal_energy_tariff[0]=="Herbes et al 2018":
                tariff=0.026 #Value for 2018

            elif self.thermal_energy_tariff[0]=="manual":
                tariff=self.thermal_energy_tariff[1]           
        
        return tariff

    def cashflow(self):
        """
        |  All cashflows for the installation and operation
        |  **NOTE:**
        |  - These costs are not yet related to the production quantities.
        """
        #Definitions
        Output=self.output()

        ## CAPEX
        #NOTE: Exponential relationship should be adjusted with the kWel
        INSTALL_COST = 21650 * math.pow(self.CHP_kW,-0.366) #exponential relationship FNR (2016), Tables 8.9-11
        Cashflow_capex = pd.DataFrame(index=[], columns=[])
        if self.new_infrastructure==True:
            Cashflow_capex.loc["infrastructure and construction", "EUR"] = INSTALL_COST * self.CHP_kW
        else:
            Cashflow_capex.loc["installation and construction", "EUR"]=1 #NOTE: this value just to avoid ZeroDivisionError
        if self.land_purchase==True:
            Cashflow_capex.loc["land purchase", "EUR"]=LandPrice().purchase()["Q"].item()*self.area_requirements() #EUR/m2
        Cashflow_capex["period_years"] = 20
        Cashflow_capex["section"] = "capex"
        
        if self.first_feedstock_analysis==True:
            Cashflow_capex["EUR"]=Cashflow_capex["EUR"]*self.allocation_ratio()

        ## OPEX
        FEED_IN_TARIFF=self.feed_in_tariff()
        HEATING_TARIFF=self.heating_tariff()
        #SURCHARGE_VALUE=0.0614 #Value for 2022. Source: https://www.bundesnetzagentur.de/DE/Fachthemen/ElektrizitaetundGas/Ausschreibungen/KWK/KWK1122022/start.html
        Cashflow_opex = pd.DataFrame(index=[], columns=[])

        if self.labour_costs=="Saracevic et al. 2019":
        # Cost of personnel according to Saracevic et al. 2019, Table 5. Unit: EUR/kW*a
        # NOTE: For plant lower than 750 kW installed. Might be overstimated for larger plants, but kept for sensitivity analysis purposes
            if self.CHP_kW<=150:
                PERSONNEL_COSTS = 177
            elif self.CHP_kW >150 and self.CHP_kW<=250:
                PERSONNEL_COSTS = 136
            elif self.CHP_kW > 250:
                PERSONNEL_COSTS = 120
            Cashflow_opex.loc["personnel","EUR"] = float(PERSONNEL_COSTS * self.CHP_kW)
        elif self.labour_costs=="FNR_2016":
            #FNR (2016) p. 161. Labour payment established to 15 EUR per labour-hour-->Outdated, changed to 25 EUR per labour-hour
            Cashflow_opex.loc["personnel","EUR"] = self.labour_requirement()*25

        # Cost of land rent
        if self.land_rent==True:
            Cashflow_opex.loc["land rent", "EUR"]=LandPrice().rent()["Q"].item() * self.area_requirements() 
        
        #Cost of additional input
        #NOTE: Cost of additional input (electrical energy + rape oil + lubricating oil) calculated in the the ExternInput module

        #Cost of maintenance
        if self.maintenance_costs=="Achinas_Euverink_2019": #According to Achinas & Euverink (2019), Table 3. For a 50 kW biogas plant, for larger plants can be overstimated
            Cashflow_opex.loc["maintenance", "EUR"] = Output.loc["electrical energy","Q"] * 0.065 #EUR/kWh delivered
        elif self.maintenance_costs=="FNR_2016": #FNR (2016) p.161
            Cashflow_opex.loc["maintenance", "EUR"] = Output.loc["electrical energy","Q"] * 0.01485 #EUR/kWhel delivered

        # Assuming al costs above mentioned are per year in a full capacity operation, shares have to be taken into account. 
        Cashflow_opex["EUR"]=Cashflow_opex["EUR"] * self.operation_share()

        #Costs of feedstock purchase
        #NOTE: To be implemented when with other modules or market prices, at the moment set as zero/NaN for residues 
        Cashflow_opex.loc["feedstock purchase", "EUR"]=float(np.NaN)
        
        # Potential revenues from by-products
        nutrient_price=Jurgutis_et_al_2021.Table6() #Digestate valorisation depend on market price for nutrients. More in Jurgutis et al. (2021)
    	# N, P and K concentrations are in kg/tonne FM, prices are in EUR per kg of nutrient
        for element in ["N", "P", "K"]:
            revs_nutrients=Output.loc["biogas digestate", element]*Output.loc["biogas digestate","Q"]*nutrient_price.loc[element,"Price"]
        #orgC concentration is adimensional, therefore a has to be expressed in kg when the units are in tonnes. 
        revs_orgC=(Output.loc["biogas digestate","orgC"] * Output.loc["biogas digestate","oDM"]*Output.loc["biogas digestate","DM"]*
                    Output.loc["biogas digestate","Q"] *1000 * nutrient_price.loc["OC", "Price"])
        
        Cashflow_opex.loc["biogas digestate revenue","EUR"]=(revs_nutrients+revs_orgC)*-1
        Cashflow_opex.loc["electricity to the grid revenue"]=Output.loc["electrical energy","Q"] * FEED_IN_TARIFF * -1
        Cashflow_opex.loc["heating revenue"]=Output.loc["thermal energy","Q"] * HEATING_TARIFF * -1 
        # NOTE: According to to Madlener et al. (2010), 80% of thermal energy can reach the local heating, with 
        Cashflow_opex["period_years"]=1
        Cashflow_opex["section"]="opex"

        #NOTE: Alternative including depreciation
        # #depreciation
        # #NOTE: using linear method with 20% of salvage value
        # Cashflow_depre=pd.DataFrame(index=[], columns=[])
        # Cashflow_depre.loc["depreciation", "EUR"]=Cashflow_capex.loc["infrastructure and construction", "EUR"]*0.8/Cashflow_capex.loc["infrastructure and construction", "period_years"]
        # Cashflow_depre.loc["depreciation", "section"]="depreciation"

        # #tax rate
        # Cashflow_tax=pd.DataFrame(index=[], columns=[])
        # Cashflow_tax.loc["tax rate", "abs"]=0.3
        # Cashflow_tax.loc["tax rate", "section"]="tax"

        return pd.concat([Cashflow_capex, Cashflow_opex])