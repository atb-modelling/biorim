__all__ = ["ConcreteBunkerSilo","ConcreteBasePlate"]

import numpy as np
import pandas as pd
import warnings
import math
import matplotlib.ticker as mtick
import matplotlib.pyplot as plt

from numpy import *
from scipy.integrate import odeint
from simulation.data_management_and_plotting.scenarios_plotting import LegendWithoutDuplicate
from read import Chen_2018, IPCC, EEA, Tiquia_Tam, Ecoinvent
from tools import convert_unit, convert_molmasses, prod
from external import ExternInput


class ConcreteBunkerSilo:
    """
    |  Represents a square bunker silo, such as for storing maize silage or relatively dry manure (Fahrsilo).
    |  Included are the emissions from concrete production from ecoinvent.

    :param volume: The storage volume required in m³. This determines the length of the bunker, while the width and height are defined by parameters.
    :param width: The width of the silo in meters (typically around 7 meters).
    :param height: The height of the silo in meters (typically around 2 meters).
    :param thickness: The thickness of the walls in meters (typically around 0.2 meters)
    :param use_fraction: This is the fraction the silo is used for this purpose, and is used for allocation. E.g. value of 1 means no other use, 0.5 means pit used for other purposes 50% of the time.
    :param lifetime_years: The total lifetime of the silo in years.

    """
    def __init__(self, volume: float, width: int = 7, height: int = 2, thickness: float = 0.2, use_fraction: int = 1, lifetime_years: int =25):
        
        self.volume = volume
        self.width = width
        self.height = height
        self.thickness = thickness
        self.use_fraction = use_fraction
        self.lifetime_years = lifetime_years

    def length(self):
        return self.volume/(self.width*self.height)

    # def roof_area(self):
    #     return (self.width + 1) * (self.length + 1)

    def concrete_m3(self):
        FLOOR = (self.width + self.thickness*2) * (self.length() + self.thickness) * self.thickness
        SIDEWALLS = 2 * ((self.height + 0.2) * self.length()) * self.thickness # assumption height of wall 20 cm higher than substrate
        BACKWALL = self.width * (self.height + 0.2) * self.thickness
        return FLOOR + BACKWALL + SIDEWALLS

    def emissions(self):
        LIFETIME_FRACTION = 1 / (self.use_fraction * self.lifetime_years)
        SPECIFIC_FACTOR = self.concrete_m3() * LIFETIME_FRACTION
        Emis = Ecoinvent.Ecoinvent34("concrete production, for civil engineering, with cement CEM II/B", "concrete, sole plate and foundation", unit="m³", location="RoW").emissions() * SPECIFIC_FACTOR
        Emis.rename({"concrete production, for civil engineering, with cement CEM II/B": "concrete silo construction"}, inplace=True)
        return Emis


class ConcreteCircularPit:
    """
    |  Represents a circular concrete pit, for instance used as a slurry pit.
    |  Included are the emissions from concrete production from ecoinvent.
    |  **NOTE:**
    |  - Currently the height is determined by the requested volume. May result in unrealistic values.
    |  **Possible improvements:**
    |  - Volume determines not only the height, but also the diameter.
    |  - If there is too much volume for one pit, several should be build.

    :param volume: The storage volume required in m³. This determines the length of the bunker, while the width and height are defined by parameters.
    :param diameter: The diameter of the pit in meters (typically from 8 - 20 meters).
    :param thickness: The thickness of the walls and floor in meters
    :param use_fraction: This is the fraction the silo is used for this purpose, and is used for allocation. E.g. value of 1 means no other use, 0.5 means pit used for other purposes 50% of the time.
    :param lifetime_years: The total lifetime of the silo in years.

    """
    diameter = 8

    def __init__(self, volume: float, thickness: float = 0.2, use_fraction: int = 1, lifetime_years: int = 25):
        self.volume = volume
        self.thickness = thickness
        self.use_fraction = use_fraction
        self.lifetime_years = lifetime_years # in years

    def floorarea(self):
        return math.pi * (self.diameter / 2) ** 2

    def height(self):
        height = self.volume / self.floorarea()

        if height > 5:
            self.diameter = 14
            height = self.volume / self.floorarea()
            if height > 5:
                self.diameter = 20
                height = self.volume / self.floorarea()

        print(f"Pit has a diameter of {round(self.diameter,2)} m and height of {round(height,2)} m")
        return height

    def concrete_m3(self):
        FLOOR = self.thickness * self.floorarea()
        SIDEWALLS = self.thickness * self.diameter * math.pi * self.height()
        return FLOOR + SIDEWALLS

    def emissions(self):
        LIFETIME_FRACTION = 1 / (self.use_fraction * self.lifetime_years)
        SPECIFIC_FACTOR = self.concrete_m3() * LIFETIME_FRACTION
        Emis = Ecoinvent.Ecoinvent34("concrete production, for civil engineering, with cement CEM II/B", "concrete, sole plate and foundation", unit="m³", location="RoW").emissions() * SPECIFIC_FACTOR
        Emis.rename({"concrete production, for civil engineering, with cement CEM II/B": "concrete pit construction"}, inplace=True)
        return Emis


class ConcreteBasePlate:
    """
    Represents a concrete base plate, for instance to do windrow composting, store machinery, etc.
    """
    def __init__(self, width: float, length: int = None, thickness: float = 0.2, use_to_lifetime: int = None, use_days=None, lifetime_years=25):
        self.width = width
        self.thickness = thickness
        self.length = length
        self.use_days = use_days
        self.lifetime_years = lifetime_years
        self.use_to_lifetime = use_to_lifetime

    def concrete_m3(self):
        return self.width * self.thickness * self.length

    def emissions(self):
        if (self.use_to_lifetime is None) and (self.use_days is not None):
            LONGEVITY = 365 * self.lifetime_years
            self.use_to_lifetime = self.use_days/LONGEVITY

        CONCRETE_PER_USE = self.concrete_m3() * self.use_to_lifetime

        Emis = Ecoinvent.Ecoinvent34("concrete production, for civil engineering, with cement CEM II/B", "concrete, sole plate and foundation", unit="m³", location="RoW").emissions() * CONCRETE_PER_USE
        Emis.rename({"concrete production, for civil engineering, with cement CEM II/B": "concrete base plate"}, inplace=True)
        return Emis


class BiomassDecay():
    '''
    Class for determining the dynamics of biomass decay across time. Biomass and water content are determined by Olson´s decay equation, while carbon 
    degradation is calculated assuming labile and recalcitrant masses. 

    :type I: Product
    :param I: type of leaves to be stored 
    :type time: numeric
    :param time: time period for biomass decay assessment
    :type k: numeric
    :param k: minerlization rate of the biomass expressed in years⁻¹
    :type kw: numeric
    :param kw: biomass water loss rate
    :type k1: numeric
    :param k1: minerlization rate of organic carbon labile fraction, expressed in days⁻¹
    :type A1: numeric
    :param A1: organic carbon labile fraction
    :type A2: numeric
    :param A1: organic carbon recalcintrant fraction
    '''
    
    def __init__(self, I = prod("tree leaves", 1, "ton"), time=365, k=0.4, kw=0.8, k1=0.01, A1=0.6, A2=0.4):
        
        self.I=I
        self.time=time
        self.k=k
        self.kw=kw
        self.k1=k1
        self.A1=A1
        self.A2=A2

    def total_biomass(self):
        '''
        Based on Olson's (1963) equation = Mt=M0*e^-kt
            # where:
            # Mt = Mass of litter in time "t"
            # M0 = Initial mass liter (biomass)
            # e = number e
            # k = factor, adjusted to literature findings in years⁻¹
            # t = time in years
        '''
        initial_mass=self.I["Q"][0]
        decay=pd.DataFrame(columns=["time", "total mass"])
        for day in range(0,self.time+1):
            decay.loc[day,"total mass"]=initial_mass*pow(math.exp(1), -self.k*(day/365))
            decay.loc[day,"time"]=day
        return decay

    def water_loss(self):
        """
        Based on Olson's (1963) equation with an specific k for water in years⁻¹
        """
        initial_mass=self.I["Q"][0]
        initial_water=initial_mass*(1-self.I["DM"][0])
        water_loss=pd.DataFrame(columns=["time", "water amount"])
        for day in range (0, self.time+1):
            water_loss.loc[day, "water amount"]=initial_water*pow(math.exp(1), -self.kw*(day/365))
            water_loss.loc[day, "time"]=day
        return water_loss

    def organic_carbon(self):
        '''
        The function is an approach for determing organic carbon pool's distribution in the process of decay.
        '''
        initial_mass=self.I["Q"][0]
        orgC_fraction=self.I["DM"][0]*self.I["oDM"][0]*self.I["orgC"][0]
        labile_decay=lambda t:initial_mass*orgC_fraction*self.A1*pow(math.exp(1), -self.k1*t)
        orgC_decay=pd.DataFrame(columns=["labile_mass", "recalcitrant_mass"])
        for day in range(0,self.time+1):
            orgC_decay.loc[day,"labile_mass"]= labile_decay(day)
            orgC_decay.loc[day,"recalcitrant_mass"]=initial_mass*orgC_fraction*self.A2
        return orgC_decay

    def organic_dry_matter(self):
        '''
        The function is an approach for determing organic dry matter in the process of decay.
        '''
        initial_mass=self.I["Q"][0]
        orgDM_fraction=self.I["DM"][0]*self.I["oDM"][0]
        initial_orgDM=initial_mass*orgDM_fraction
        orgDM_decay=pd.DataFrame(columns=["oDM"])
        for day in range(0,self.time+1):
            orgDM_decay.loc[day,"oDM"]= initial_orgDM*pow(math.exp(1), -self.k*(day/365))
        return orgDM_decay

    def profile_map(self, CO2_emis=False, waterhatch=False):
        '''
        Graphical representation of the mass and concentration profile during the extraction while biomass is decaying. 
        '''
        #definitions:
        initial_mass=self.I["Q"][0]
        total_biomass=self.total_biomass()["total mass"]
        water_content=self.water_loss()["water amount"]
        oDM=self.organic_dry_matter()["oDM"]
        C_labile=self.organic_carbon()["labile_mass"]
        C_recal=self.organic_carbon()["recalcitrant_mass"]
        organic_C_fraction=self.I["DM"][0]*self.I["oDM"][0]*self.I["orgC"][0]
        CO2_C_emis=initial_mass*organic_C_fraction-C_recal-C_labile

        concat=pd.concat([total_biomass,water_content.iloc[::-1]])
        concat=pd.DataFrame(concat, columns=["mass"])
        concat["time"]=concat.index.to_list()
        c_array=concat[["time","mass"]].to_numpy()

        t=np.array(range(0,self.time+1,1))

        fig, ax =plt.subplots(sharey=True)
        ax.plot(t, total_biomass, label="Total biomass", color="#7A660F")
        ax.plot(t, water_content, label="Water content", linestyle="--", color="cyan")
        ax.plot(t, oDM, label="Organic dry matter", color="r", zorder=60)
        ax.plot(t, C_labile+C_recal, label="Organic carbon \nlabile mass", color="green")
        if CO2_emis==True:
            ax.plot(t,CO2_C_emis, label="CO2-C emissions", color="#DD2FC8", linestyle="--", zorder=200)
        else:
            pass

        ax.bar(t, C_recal, label="Organic carbon \nrecalcitrant mass", edgecolor="#5FEA2A", fill=False, hatch="/", width=1, zorder=50)
        ax.tick_params("both",labelsize=13)
        if waterhatch==True:
            ax.fill("time", "mass",
                    data={"time": c_array[:,0], "mass": c_array[:,1]}, label="Water content", color="none", edgecolor="cyan",hatch="/")
        else:
            pass
        ax.set_xlim(0,self.time)
        ax.set_ylim(0,initial_mass)
        ax.set_xlabel("Days", fontsize=13)
        ax.set_ylabel("Tons of leaves", fontsize=13)
        ax.set_title("Leaves decay", fontsize=14)
        ax2=ax.twinx()
        ax2.set_ylim(0,initial_mass)
        ax2.set_ylabel("Remaining biomass", fontsize=13, rotation=270, labelpad=15)
        ax2.yaxis.set_major_formatter(mtick.PercentFormatter(xmax=initial_mass))
        ax2.tick_params("both",labelsize=13)
        LegendWithoutDuplicate(ax, fig, n_cols=5).for_fig()
        plt.show()


class DecayContinuousExtraction():
    '''
    Class for determining the dynamcis of main biomass components while expericing decaying with a fixed rate of continuous extractions.

    :type I: Product
    :param I: type of leaves to be stored 
    :type extraction: numeric
    :param extraction: extraction rate of the biomass (mass unit product/day)
    :type k: numeric
    :param k: minerlization rate of the biomass expressed in years⁻¹
    :type kw: numeric
    :param kw: biomass water loss rate
    :type k1: numeric
    :param k1: minerlization rate of organic carbon labile fraction expressed in days⁻¹
    :type A1: numeric
    :param A1: organic carbon labile fraction
    :type A2: numeric
    :param A1: organic carbon recalcintrant fraction

    '''
    def __init__(self, I=prod("tree leaves", 1, "ton"), extraction=300, k=0.4, kw=0.8, k1=0.01, A1=0.6, A2=0.4, k_N=1.7, k_P=4.1, k_K=4.2, data_display="dataframe", day_notification=False):
        
        self.I=I
        self.extraction=extraction
        self.k=k
        self.kw=kw
        self.k1=k1
        self.A1=A1
        self.A2=A2
        self.k_N=k_N
        self.k_P=k_P
        self.k_K=k_K
        self.data_display=data_display
        self.day_notification=day_notification

    def total_biomass(self):
        '''
        Based on Olson's equation, but modified to integrate a continuous extraction (output):
        Mᵢ=365*O/k*(1-e⁻ᵏᵗ)+M₀*e⁻ᵏᵗ
        where:
        Mᵢ= mass in time t
        O= continous output (ton/day)
        k= factor expressed in years⁻¹
        t= time in days
        M₀= initial mass
        e= number e
        '''
        initial_mass=self.I["Q"][0]
        if self.data_display=="dataframe":
        #output_day=lambda t:-365*cont_O/k*(1-pow(math.exp(1), -k*(t/365)))+initial_mass*pow(math.exp(1), -k*(t/365))
            time=round(initial_mass/self.extraction)
            decay_with_o=pd.DataFrame(columns=["time", "final mass"])
            for t in range(0,time):
                decay_with_o.loc[t,"final mass"]=-365*self.extraction/self.k*(1-pow(math.exp(1), -self.k*(t/365)))+initial_mass*pow(math.exp(1), -self.k*(t/365))
                decay_with_o.loc[t,"time"]=t
                if decay_with_o.loc[t,"final mass"]<0:
                    decay_with_o.loc[t,"final mass"]=0
                
            decay_with_o=decay_with_o[decay_with_o["final mass"]>0]
            last_day=decay_with_o.index[-1]

            if self.day_notification==True:
                print(f'---> NOTE: The biomass will run out in {last_day} days <---')
            else:
                pass

        elif self.data_display=="array":
            time=round(initial_mass/self.extraction)
            decay_with_o=np.array([[0,0]])
            #output_day=lambda t:-365*cont_O/k*(1-pow(math.exp(1), -k*(t/365)))+initial_mass*pow(math.exp(1), -k*(t/365))
            for t in range (0, time):
                if -365*self.extraction/self.k*(1-pow(math.exp(1), -self.k*(t/365)))+initial_mass*pow(math.exp(1), -self.k*(t/365))>0:
                    decay_with_o=np.append(decay_with_o, [[t, -365*self.extraction/self.k*(1-pow(math.exp(1), -self.k*(t/365)))+initial_mass*pow(math.exp(1), -self.k*(t/365))]], 0)
                else:
                    decay_with_o=np.append(decay_with_o, [[t, 0]],0)
            decay_with_o=np.delete(decay_with_o, 0, 0)
            condition=decay_with_o[:,1]>0
            decay_with_o=decay_with_o[condition]

        return decay_with_o

    def last_day(self):
        '''
        Determine the last day (integer) of biomass availability. Based on modified Olson's equation. 
        '''
        initial_mass=self.I["Q"][0]
        time=round(initial_mass/self.extraction)
        decay_with_o=pd.DataFrame(columns=["time", "final mass"])
        for t in range(0,time):
            decay_with_o.loc[t,"final mass"]=-365*self.extraction/self.k*(1-pow(math.exp(1), -self.k*(t/365)))+initial_mass*pow(math.exp(1), -self.k*(t/365))
            decay_with_o.loc[t,"time"]=t
            if decay_with_o.loc[t,"final mass"]<0:
                decay_with_o.loc[t,"final mass"]=0
            
            decay_with_o=decay_with_o[decay_with_o["final mass"]>0]
            last_day=decay_with_o.index[-1]

        return last_day
    
    def carbon_dynamics(self):
        '''
        System of ordinary differential equations (ODE's) which represent the dynamics of labile and recalcitrant carbon in natural decay with continous extraction.

        z = initial conditions (x1,x2,x3,x4)
        C-lab = labile mass of the organic carbon 
        C-recal = recalcintrant mass of the organic carbon 
        x1=C-lab
        x2=C-recal
        x3=C-lab extracted
        x4=C-recal extracted
        E=extracion of organic carbon
        k1=mineralization rate of the labile fraction of organic carbon matter (day⁻¹)          
        k=mineralization rate of the biomass (year⁻¹)

        '''
        E= self.extraction
        initial_mass=self.I["Q"][0]
        orgC_fraction=self.I["DM"][0]*self.I["oDM"][0]*self.I["orgC"][0]
    
        def ode_system(z,t):
            #initial conditions
            x1=z[0]
            x2=z[1]
            x3=z[2]
            x4=z[3]
        
            dx1dt=-E*(x1/(-365*E/self.k*(1-pow(math.exp(1), -self.k*(t/365)))+initial_mass*pow(math.exp(1), -self.k*(t/365))))-x1*self.k1
            dx2dt=-E*(x2/(-365*E/self.k*(1-pow(math.exp(1), -self.k*(t/365)))+initial_mass*pow(math.exp(1), -self.k*(t/365))))
            dx3dt=(x1/(-365*E/self.k*(1-pow(math.exp(1), -self.k*(t/365)))+initial_mass*pow(math.exp(1), -self.k*(t/365))))*E
            dx4dt=(x2/(-365*E/self.k*(1-pow(math.exp(1), -self.k*(t/365)))+initial_mass*pow(math.exp(1), -self.k*(t/365))))*E

            if x1<0:
                x1=0
                x2=0
                dx1dt=0
                dx2dt=0
                dx3dt=0
                dx4dt=0

            return [dx1dt, dx2dt, dx3dt, dx4dt]

        z0=[initial_mass*orgC_fraction*0.6, initial_mass*orgC_fraction*0.4, 0, 0] #initial conditions
        time=self.last_day()+1
        t=np.array(range(0,time,1)) #time
        z=odeint(ode_system, z0, t)
        if self.data_display=="dataframe":
            z_df=pd.DataFrame(z, columns=["C_labile","C_recalcitrant","C_labile_ext","C_recal_ext"])
            z_df["CO2-C_emis"]=initial_mass*orgC_fraction-z_df["C_labile"]-z_df["C_recalcitrant"]-z_df["C_labile_ext"]-z_df["C_recal_ext"]
            return z_df
        elif self.data_display=="array":
            return z

    def organic_dry_matter(self):
        '''
        ODE´s system for organic dry matter decay representation. Based on carbon dynamics, but the k-value of biomass decay is assumed. 
        '''
        E= self.extraction
        initial_mass=self.I["Q"][0]
        organicDM_fraction=self.I["DM"][0]*self.I["oDM"][0]
    
        def ode_system(z,t):
            #initial conditions
            o=z[0]
            o2=z[1]
        
            dodt=-(o/(-365*E/self.k*(1-pow(math.exp(1), -self.k*(t/365)))+initial_mass*pow(math.exp(1), -self.k*(t/365))))*E-(self.k/365)*o
            do2dt=(o/(-365*E/self.k*(1-pow(math.exp(1), -self.k*(t/365)))+initial_mass*pow(math.exp(1), -self.k*(t/365))))*E

            if o<0:
                o=0
                dodt=0
                do2dt=0

            return [dodt, do2dt]

        z0=[initial_mass*organicDM_fraction, 0] #initial conditions
        time=self.last_day()+1
        t=np.array(range(0,time,1)) #time
        z=odeint(ode_system, z0, t)
        if self.data_display=="dataframe":
            z_df=pd.DataFrame(z, columns=["oDM_mass", "oDM_extracted"])
            return z_df
        elif self.data_display=="array":
            return z

    def water_loss(self):
        '''
        ODE´s system for the representation of water loss in the biomass.
        ----> IMPORTANT: At the moment, the k-value of water is fixed to 0.8 due to uncertainties of units of k-values from other studies. 
        '''
        E=self.extraction
        initial_mass=self.I["Q"][0]
        water_fraction=(1-self.I["DM"][0])

        def ode_system(zw, t):
            w=zw[0]
            w2=zw[1]
            dwdt=-(w/(-365*E/self.k*(1-pow(math.exp(1), -self.k*(t/365)))+initial_mass*pow(math.exp(1), -self.k*(t/365))))*E-(self.kw/365)*w #<---Random kw value
            dw2dt=(w/(-365*E/self.k*(1-pow(math.exp(1), -self.k*(t/365)))+initial_mass*pow(math.exp(1), -self.k*(t/365))))*E

            if w<0:
                w=0
                dwdt=0
                dw2dt=0

            return [dwdt, dw2dt]
        
        zw0=[initial_mass*water_fraction, 0]
        time=self.last_day()+1
        t=np.array(range(0,time,1))
        zwater=odeint(ode_system, zw0, t)
        if self.data_display=="array":
            return zwater
        elif self.data_display=="dataframe":
            return pd.DataFrame(zwater, columns=["Water mass remaining", "Water mass extracted"])

    def nutrient_decay(self):
        '''
        ODE´s system for the representation of nutrient decay (loss) in the biomass.  
        '''

        E=self.extraction
        initial_mass=self.I["Q"][0]

        def ode_system(z,t):
            N=z[0]
            P=z[2]
            K=z[4]

            dNdt=-(N/(-365*E/self.k*(1-pow(math.exp(1), -self.k*(t/365)))+initial_mass*pow(math.exp(1), -self.k*(t/365))))*E-(self.k_N/365)*N
            dN2dt=(N/(-365*E/self.k*(1-pow(math.exp(1), -self.k*(t/365)))+initial_mass*pow(math.exp(1), -self.k*(t/365))))*E

            dPdt=-(P/(-365*E/self.k*(1-pow(math.exp(1), -self.k*(t/365)))+initial_mass*pow(math.exp(1), -self.k*(t/365))))*E-(self.k_P/365)*P
            dP2dt=(P/(-365*E/self.k*(1-pow(math.exp(1), -self.k*(t/365)))+initial_mass*pow(math.exp(1), -self.k*(t/365))))*E

            dKdt=-(K/(-365*E/self.k*(1-pow(math.exp(1), -self.k*(t/365)))+initial_mass*pow(math.exp(1), -self.k*(t/365))))*E-(self.k_K/365)*K
            dK2dt=(K/(-365*E/self.k*(1-pow(math.exp(1), -self.k*(t/365)))+initial_mass*pow(math.exp(1), -self.k*(t/365))))*E

            if N<0:
                N=0
                dNdt=0
                dN2dt=0

            if P<0:
                P=0
                dPdt=0
                dP2dt=0

            if K<0:
                K=0
                dKdt=0
                dK2dt=0

            return [dNdt, dN2dt, dPdt, dP2dt, dKdt, dK2dt]

        zn0=[initial_mass*self.I["N"][0], 0, initial_mass*self.I["P"][0], 0, initial_mass*self.I["K"][0], 0]
        time=self.last_day()+1
        t=np.array(range(0,time,1))
        znutrient=odeint(ode_system, zn0, t)
        if self.data_display=="array":
            return znutrient
        elif self.data_display=="dataframe":
            return pd.DataFrame(znutrient, columns=["N_mass_remaining", "N_mass_extracted", "P_mass_remaining", "P_mass_extracted", "K_mass_remaining", "K_mass_extracted"])

    def total_mass_extraction(self):
        '''
        Dataframe of accumulated mass extraction fractions.
        '''

        #FM extracted per day
        FM_extracted=self.total_biomass()
        FM_extracted["final mass"][:-1]=self.extraction
        FM_extracted=FM_extracted.rename(columns={"final mass":"FM_extracted_day"})

        #total FM extracted
        total_FM_extracted=self.total_biomass()
        for i in list(FM_extracted.index):
            total_FM_extracted.loc[i,"final mass"]=i*self.extraction
        total_FM_extracted.loc[self.last_day(), "final mass"]=FM_extracted.loc[self.last_day(), "FM_extracted_day"]+list(total_FM_extracted["final mass"])[-2]
        total_FM_extracted=total_FM_extracted.rename(columns={"final mass":"total_FM_extracted"})

        #total water extracted
        water_extracted=self.water_loss()
        water_df=pd.DataFrame(columns=["Water_mass_extracted"])
        water_df["Water_mass_extracted"]=water_extracted["Water mass extracted"]

        #total dry matter extracted
        total_DM_extracted=pd.DataFrame(index=FM_extracted.index, columns=["total_DM_extracted"])
        total_DM_extracted["total_DM_extracted"]=total_FM_extracted["total_FM_extracted"]-water_df["Water_mass_extracted"]
        
        #total carbon extracted
        carbon_extracted=self.carbon_dynamics()
        carbon_extracted["total_carbon_extracted"]=carbon_extracted["C_labile_ext"]+carbon_extracted["C_recal_ext"]
        carbon_extracted=carbon_extracted.drop(columns=["C_labile_ext", "C_recal_ext"],axis=1)

        #total oDM extracted
        oDM_extracted=self.organic_dry_matter()
        oDM_extracted=oDM_extracted[oDM_extracted["oDM_mass"]>0]["oDM_extracted"]

        #total nutrient extracted
        nutrient_extracted=self.nutrient_decay()
        nutrient_extracted=nutrient_extracted[nutrient_extracted["N_mass_remaining"]>0]
        nutrient_extracted=pd.concat([nutrient_extracted["N_mass_extracted"], nutrient_extracted["P_mass_extracted"], nutrient_extracted["K_mass_extracted"]], axis=1)

        #mass extracted
        mass_extracted=pd.concat([FM_extracted, total_FM_extracted, water_df ,total_DM_extracted, oDM_extracted, carbon_extracted,  nutrient_extracted], axis=1)
        mass_extracted=mass_extracted.dropna()
        mass_extracted=mass_extracted.drop(columns="time")

        return mass_extracted

    def final_product(self):
        '''
        Final product extracted. Displays total mass and concentration characteristics. 
        '''
        #final product
        mass_extracted=self.total_mass_extraction()
        last_day=self.last_day()
        final_product=pd.DataFrame(index=["final_product"], columns=["Q","DM", "oDM","orgC", "N", "P", "K"])
        final_product.loc["final_product","Q"]=mass_extracted.loc[last_day, "total_FM_extracted"]
        final_product.loc["final_product","DM"]=mass_extracted.loc[last_day, "total_DM_extracted"]/mass_extracted.loc[last_day, "total_FM_extracted"]
        final_product.loc["final_product","oDM"]=mass_extracted.loc[last_day, "oDM_extracted"]/mass_extracted.loc[last_day, "total_DM_extracted"]
        final_product.loc["final_product","orgC"]=mass_extracted.loc[last_day, "total_carbon_extracted"]/mass_extracted.loc[last_day, "oDM_extracted"]
        final_product.loc["final_product","N"]=mass_extracted.loc[last_day, "N_mass_extracted"]/mass_extracted.loc[last_day, "total_FM_extracted"]
        final_product.loc["final_product","P"]=mass_extracted.loc[last_day, "P_mass_extracted"]/mass_extracted.loc[last_day, "total_FM_extracted"]
        final_product.loc["final_product","K"]=mass_extracted.loc[last_day, "K_mass_extracted"]/mass_extracted.loc[last_day, "total_FM_extracted"]

        return final_product

    def daily_profile(self):
        '''
        Daily representation of the total mass and concentrations within the biomass that it is being extracted while decaying. 
        '''
        total_mass=self.total_biomass()["final mass"]
        water_remain=self.water_loss()["Water mass remaining"]
        DM_remain=self.total_biomass()["final mass"]-self.water_loss()["Water mass remaining"]
        orgDM_remain=self.organic_dry_matter()["oDM_mass"]
        orgC_remain=self.carbon_dynamics()["C_labile"]+self.carbon_dynamics()["C_recalcitrant"]
        N_remain=self.nutrient_decay()["N_mass_remaining"]
        P_remain=self.nutrient_decay()["P_mass_remaining"]
        K_remain=self.nutrient_decay()["K_mass_remaining"]

        daily_profile=pd.concat([total_mass, DM_remain, water_remain, orgDM_remain, orgC_remain, N_remain, P_remain, K_remain], axis=1)
        daily_profile=daily_profile.rename(columns={0:"DM",
                                                    1:"orgC"})
        return daily_profile

    def daily_concentration(self):
        '''
        Daily concentrations within the biomass that it is being extracted while decaying. 
        '''

        last_day=self.last_day()

        total_mass=self.total_biomass()["final mass"]
        water_remain=self.water_loss()["Water mass remaining"]
        orgDM_remain=self.organic_dry_matter()["oDM_mass"]
        orgC_remain=self.carbon_dynamics()["C_labile"]+self.carbon_dynamics()["C_recalcitrant"]
        N_remain=self.nutrient_decay()["N_mass_remaining"]
        P_remain=self.nutrient_decay()["P_mass_remaining"]
        K_remain=self.nutrient_decay()["K_mass_remaining"]

        daily_concentration=pd.DataFrame(columns=["Q", "DM", "oDM", "orgC", "N", "P", "K"])
        for day in range(0, last_day+1):
            daily_concentration.loc[day, "Q"]=total_mass[day]
            daily_concentration.loc[day, "DM"]=(total_mass[day]-water_remain[day])/total_mass[day]
            daily_concentration.loc[day, "oDM"]=orgDM_remain[day]/(total_mass[day]-water_remain[day])
            daily_concentration.loc[day, "orgC"]=orgC_remain[day]/orgDM_remain[day]
            daily_concentration.loc[day, "N"]=N_remain[day]/total_mass[day]
            daily_concentration.loc[day, "P"]=P_remain[day]/total_mass[day]
            daily_concentration.loc[day, "K"]=K_remain[day]/total_mass[day]

        return daily_concentration
    
    def mean_concentration(self):
        '''
        Mean concentration within the biomass that it is being extracted while decaying. 
        '''
        daily_conc=self.daily_concentration()
        mean_conc=pd.DataFrame(index=["mean concentration"],columns=["DM", "oDM", "orgC", "N", "P", "K"])
        mean_conc.loc["mean concentration","DM"]=daily_conc["DM"].mean()
        mean_conc.loc["mean concentration","oDM"]=daily_conc["oDM"].mean()
        mean_conc.loc["mean concentration","orgC"]=daily_conc["orgC"].mean()
        mean_conc.loc["mean concentration","N"]=daily_conc["N"].mean()
        mean_conc.loc["mean concentration","P"]=daily_conc["P"].mean()
        mean_conc.loc["mean concentration","K"]=daily_conc["K"].mean()

        return mean_conc
    
    def profile_map(self, CO2_emis=False, waterhatch=False, show_nutrient=False):
        '''
        Graphical representation of the mass and concentration profile during the extraction while biomass is decaying. 
        '''
        #definition continuous extracion
        initial_mass=self.I["Q"][0]
        daily_profile=self.daily_profile()
        last_day=self.last_day()
        total_biomass=daily_profile["final mass"]
        water_content=daily_profile["Water mass remaining"]
        oDM=daily_profile["oDM_mass"]
        orgC=daily_profile["orgC"]
        C_labile=self.carbon_dynamics()["C_labile"]
        C_recal=self.carbon_dynamics()["C_recalcitrant"]
        C_lab_ext=self.carbon_dynamics()["C_labile_ext"]
        C_recal_ext=self.carbon_dynamics()["C_recal_ext"]
        dry_matter=daily_profile["DM"]
        CO2_C_emis=daily_profile["orgC"][0]-C_labile-C_recal-C_lab_ext-C_recal_ext
        N_content=self.nutrient_decay()["N_mass_remaining"]/1000
        P_content=self.nutrient_decay()["P_mass_remaining"]/1000
        K_content=self.nutrient_decay()["K_mass_remaining"]/1000

        concat=pd.concat([total_biomass,dry_matter.iloc[::-1]])
        concat=pd.DataFrame(concat, columns=["mass"])
        concat["time"]=concat.index.to_list()
        c_array=concat[["time","mass"]].to_numpy()

        t=np.array(range(0,last_day+1,1))

        fig, ax =plt.subplots(sharey=True)
        ax.plot(t, total_biomass, label="Total biomass", color="#7A660F")
        ax.plot(t, total_biomass-water_content, label="Water content", linestyle="--", color="cyan")
        ax.plot(t, oDM, label="Organic dry matter", color="r", zorder=60)
        ax.plot(t, C_labile+C_recal, label="Organic carbon \nlabile mass", color="green")
        if CO2_emis==True:
            ax.plot(t,CO2_C_emis, label="CO2-C emissions", color="#DD2FC8", linestyle="--", zorder=200)
        else:
            pass
        if show_nutrient==True:
            ax.plot(t, oDM+N_content, label="N content")
            ax.plot(t, oDM+N_content+P_content, label="P content")
            ax.plot(t, oDM+N_content+P_content+K_content, label="K content")
        else:
            pass
        ax.bar(t, C_recal, label="Organic carbon \nrecalcitrant mass", edgecolor="#5FEA2A", fill=False, hatch="/", width=1, zorder=50)
        ax.tick_params("both",labelsize=13)
        if waterhatch==True:
            ax.fill("time", "mass",
                    data={"time": c_array[:,0], "mass": c_array[:,1]}, label="Water content", color="none", edgecolor="cyan",hatch="/")
        else:
            pass
        ax.set_xlim(0,last_day)
        ax.set_ylim(0,initial_mass)
        ax.set_xlabel("Days", fontsize=13)
        ax.set_ylabel("Tons of leaves", fontsize=13)
        ax.set_title("Leaves decay profile with continuous extraction", fontsize=14)
        ax2=ax.twinx()
        ax2.set_ylim(0,initial_mass)
        ax2.set_ylabel("Remaining biomass", fontsize=13, rotation=270, labelpad=15)
        ax2.yaxis.set_major_formatter(mtick.PercentFormatter(xmax=initial_mass))
        ax2.tick_params("both",labelsize=13)
        LegendWithoutDuplicate(ax, fig, n_cols=5).for_fig()
        plt.show()