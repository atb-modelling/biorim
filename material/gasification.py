import pandas as pd
import numpy as np
import math

from declarations import Process, Emissions
from tools import prod, convert_molmasses
from read import GHG_protocol, MontielBohorquez_Perez
from read.Prices import InputPrice, LandPrice
from read.Ecoinvent import Ecoinvent34

class Gasification(Process):
    """
    Represents syngas production from the gasification process and the other supporting processes, such as biomass drying. 
    Currently designed for the use of autumn tree leaves, but it can accept feedstock which volatile matter (VM), ash content (AC)
    and moisture (M) are specified. The syngas production it directly intregated to the generate electricity, using turbine method
    as a default techonology. 

    :type I: Product
    :param I: substrates to feed the gasifier (currently: autumn tree leaves)
    :type type_reactor: string
    :param type_reactor: technology type of the gasifier (currently: "downdraft")
    :type syngas_prod: string
    :param syngas_prod: approach in which sygas is produced. "volatile matter" assuming that syngas is derived enterily from the volatile matter fraction
    :type vaporization_rate: float 0.00-1-00
    :param vaporization_rate: rate of water vaporization when drying where 1.00 represents complete dried
    :type ER: float 0.00-1-00
    :param ER: equivalente ratio (ER), fraction of stecheometrical oxygen to used during the gasification
    :type energy_technology: string
    :param energy_technology: type of technology for energy conversion from the combsution of produced syngas
    :type kW: float or integer
    :param kW: installed power of the gasification unit
    :type electrical_energy_eff: float 
    :param electrical_energy_eff: efficiency of electrical generation technology 
    :type thermal_recirculation: boolean
    :param thermal_recirculation: 'True' if the heat produced is recirculated as an input to the biomass drying process
    :type lifetime: int
    :param lifetime: lifetime in years of the gasification unit
    :type operation_hours: int
    :param operation_hours: operation time (hours) of the gasificatio unit per year
    :type thermal_efficiency: int
    :param thermal_efficiency: thermal efficiency
    :type calculation_approach: int
    :param calculation_approach: fraction of thermal that can be use in recirculation
    :type land_purchase: boolean
    :param land_purchase: if true, land purchase is considered in the cashflow
    :type land_rent: boolean
    :param land_rent: if true, land rent is considered in the cashflow
    :type new_infrastructure: boolean
    :param new_infrastructure: if true, installation and construction is considered in the cashflow (CAPEX)
    :type EEG_tariff: str
    :param EEG_tariff: version of the EEG law considered in the feed-in-tariff function
    :type year_assessment: str
    :param year_assessment: version of the EEG law considered in the feed-in-tariff function
    """

    #NOTE: According to Montiel-Bohorquez & Perez(2022), gasification process is suggested to be conducted below 1000°C
    #In such sense, density of gases are expresed at 800°C and 1 atm of pressure (Montiel-Bohorquez & Perez, 2022; You et al. 2018) 
    #Densities retrieved from: https://www.omnicalculator.com/physics/gas-density 
    H2_DENSITY=0.03 #kg/m3
    CO2_DENSITY=0.5 #kg/m3
    CH4_DENSITY=0.18 #kg/m3
    CO_DENSITY=0.31 #kg/m3
    N2_DENSITY=0.32 #kg/m3

    SVOBODA_DRYING_ENERGY_RATE = 2.6 #MJ/kg wet fuel biomass (Svoboda et al. 2009)
    MJ_to_kWH = 0.27778 #kWh/MJ
    DRYING_ENERGY_RATE = SVOBODA_DRYING_ENERGY_RATE * MJ_to_kWH #kWh/kg

    def __init__(self,
                 I = prod("tree leaves", 1, "ton"),
                 type_reactor = "downdraft",
                 syngas_prod = "volatile matter",
                 vaporization_rate = 0.9, 
                 ER = 0.34,
                 energy_technology = "turbine",
                 kW = 150,
                 electrical_energy_eff = 0.25,
                 thermal_recirculation = True,
                 lifetime = 20,
                 operation_hours_year = 8000,
                 thermal_efficiency = 0.42,
                 thermal_energy_use = 0.75,
                 calculation_approach = "total mass use",
                 land_purchase = True, 
                 thermal_energy_tariff = ("manual", 0.044),
                 land_rent = False,
                 new_infrastructure=True,
                 EEG_tariff=("EEG_2021", None),
                 year_assessment = 2022,
                 id=None):
        
        self.I=I
        self.type_reactor=type_reactor
        self.syngas_prod=syngas_prod
        self.vaporization_rate=vaporization_rate
        self.ER=ER
        if self.ER not in [0.34, 0.36, 0.40, 0.41, 0.43]:
            raise ValueError (f"{self.ER} not considered for the calculation. ER should be 0.34, 0.36, 0.40, 0.40 or 0.43")
        self.energy_technology=energy_technology
        self.kW=kW
        self.electrical_energy_eff=electrical_energy_eff #based on Fig 9 (sankey diagram) of Aguado et al. (2022)
        self.thermal_recirculation=thermal_recirculation
        self.thermal_efficiency=thermal_efficiency
        self.thermal_energy_use=thermal_energy_use
        self.lifetime=lifetime
        self.calculation_approach=calculation_approach
        self.operation_hours_year=operation_hours_year
        self.land_purchase=land_purchase
        self.land_rent=land_rent
        self.thermal_energy_tariff=thermal_energy_tariff
        self.new_infrastructure=new_infrastructure
        self.EEG_tariff=EEG_tariff
        self.year_assessment=year_assessment
        self.id=id

    #NOTE: For use of tree leaves, Montiel-Bohorquez & Perez(2022), fixed-bed downdraft reacter techonology suits the best 
    # due to low tar content (0.1-6 g/Nm3). 
    #Fallen leaves are ash-rich biomass

    def supply_information(self):
        """
        Returns potential supply information of the current feedstock to the gasification unit, based in fuel consumption
        per kWh generated and the operations hours.
        """
        biomass_dry=self.I["Q"][0]*self.I["DM"][0]
        specific_fuel_consumption=2.25 #kg dry biomass/kWh according to Montiel-Bohorquez & Perez(2022) for 100% tree leaves pellets

        # ER_biomass_flow={"biomass mass flow": [7.28, 9.61, 11.70, 13.00, 14.3]} #retrieved from Montiel-Bohorquez & Perez(2022)
        # biomass_flow_df=pd.DataFrame.from_dict(ER_biomass_flow, orient="index", columns=[0.34, 0.36, 0.40, 0.41, 0.43])
        # biomass_flow=biomass_flow_df.loc["biomass mass flow", self.ER]#kg/hour
        # oper_hours_day=self.OPERATION_HOURS_YEAR/365
        # supply_days=biomass_wet["Q"][0]/(biomass_flow*oper_hours_day)

        supply_information=pd.DataFrame(index=[],columns=[])
        fuel_required=self.kW * self.operation_hours_year * self.electrical_energy_eff * specific_fuel_consumption #kg dry biomass/year

        if self.I["U"][0]=="ton":
            supply_information.loc["fuel one year (dry)", "Q"]=fuel_required/1000
            supply_information.loc["fuel one year (dry)", "U"]="ton"
            supply_information.loc["fuel one year (wet)", "Q"]=(fuel_required/1000)/self.I["DM"][0]
            supply_information.loc["fuel one year (wet)", "U"]="ton"
            supply_information.loc["supply time of feedstock (dry)", "Q"]=biomass_dry*1000/(fuel_required/365)
            supply_information.loc["supply time of feedstock (dry)", "U"]="day"
            supply_information.loc["total amount units from feedstock (dry)", "Q"]=math.ceil(biomass_dry/supply_information.loc["fuel one year (dry)", "Q"])
            supply_information.loc["total amount units from feedstock (dry)", "U"]="unit"
        if self.I["U"][0]=="kg":
            supply_information.loc["fuel one year (dry)", "Q"]=fuel_required
            supply_information.loc["fuel one year (dry)", "U"]="kg"
            supply_information.loc["fuel one year (wet)", "Q"]=(fuel_required)/self.I["DM"][0]
            supply_information.loc["fuel one year (wet)", "U"]="ton"
            supply_information.loc["supply time of feedstock (dry)", "Q"]=biomass_dry/(fuel_required/365)
            supply_information.loc["supply time of feedstock (dry)", "U"]="day"
            supply_information.loc["total amount units from feedstock (dry)", "Q"]=math.ceil(biomass_dry/supply_information.loc["fuel one year (dry)", "Q"])
            supply_information.loc["total amount units from feedstock (dry)", "U"]="unit"

        return supply_information

    def lifetime_share(self):
        """
        Returns the share (0 to 1) of the amount of feedstock passed would supply the operation of the gasification unit over
        the amount of feedstock that would be processed for the lifetime period at full capacity.
        """
        supply_info=self.supply_information()
        LIFETIME_SHARE=supply_info.loc["supply time of feedstock (dry)", "Q"]/(365*self.lifetime)
        return LIFETIME_SHARE

    def operation_share(self):
        """
        Returns the share (0 to 1) of the amount of feedstock passed would supply the operation of the gasification unit over
        the amount of feedstock that would be processed for one year at full capacity.
        """
        biomass_dry=self.biomass_drying().loc[f"{self.I.index[0]} dry"]
        supply_info=self.supply_information()
        # if self.calculation_approach=="operation per year":
        if supply_info.loc["fuel one year (dry)", "Q"]>biomass_dry["Q"]:
            OPERATION_SHARE=biomass_dry["Q"]/supply_info.loc["fuel one year (dry)", "Q"]
        else: 
            OPERATION_SHARE=1
        return OPERATION_SHARE

    def biomass_drying(self):
        """
        Represents the chemical reaction during drying stage (R0) according to Montiel-Bohorquez & Perez (2022)
        RO: Wet biommas-->Dry biommas + H2O(v)
        NOTE: It does not considered the amount of energy needed to dry the biomass
        """
        supply_info=self.supply_information()

        #definitions
        biomass_wet=self.I.copy()
        biomass_dry=self.I.copy().rename(index={f"{self.I.index[0]}": f"{self.I.index[0]} dry"})

        if self.calculation_approach=="operation per year":
            if biomass_wet["Q"][0]>=supply_info.loc["fuel one year (wet)", "Q"]:
                biomass_wet["Q"]=supply_info.loc["fuel one year (wet)", "Q"]
                biomass_dry["Q"]=supply_info.loc["fuel one year (wet)", "Q"] #the wet amount will be change in the subsequence operations
            else:
                pass

        #water vapour
        water_vapour=pd.DataFrame(index=["water vapor"], columns=["Q","U"])
        water_vapour["Q"][0]=(1*self.vaporization_rate-biomass_wet["DM"][0])*biomass_wet["Q"][0]
        water_vapour["U"][0]=biomass_wet["U"][0]

        #dry biomass
        biomass_dry["Q"]=biomass_wet["Q"][0]-water_vapour["Q"][0]
        biomass_dry["DM"]=1*self.vaporization_rate
        #no changes in oDM and orgC, since they are expresed at a DM basis
        for nutrient in ["N", "P", "K"]:
            biomass_dry[nutrient][0]=biomass_wet["Q"][0]*biomass_wet[nutrient][0]/biomass_dry["Q"][0]

        return pd.concat([water_vapour, biomass_dry])

    def energy_drying(self):
        """
        Returns the amount of energy needed to dry the biomass
        NOTE: Water vaporization rate is considered 2.26 MJ/kg, but based in Svobda et al. (2019) this rate was set at 2.6 MJ/kg
        """

        #TODO: Modify this approach as it does not considered the moisture content

        #definitions
        biomass_wet=self.I.copy()
        energy_input=pd.DataFrame(index=["energy for drying process"], columns=["Q","U"])
        water_to_vaporize=biomass_wet["Q"][0]*(self.vaporization_rate - biomass_wet["DM"][0])
        if biomass_wet["U"][0]=="ton":
            energy_input.loc["energy for drying process","Q"]=self.DRYING_ENERGY_RATE*water_to_vaporize*1000
        if biomass_wet["U"][0]=="kg":
            energy_input.loc["energy for drying process","Q"]=self.DRYING_ENERGY_RATE*water_to_vaporize
        energy_input.loc["energy for drying process","U"]="kWh"

        return energy_input

    def pyrolysis(self):
        """
        Represents the chemical reaction during the pyrolysis stage (R1) to Montiel-Bohorquez et al (2022) (Table 2)
        R1: Biomass --> Volatiles (H2, CO, CO2, H2O, CH4) + Tars + Chars
        """
        biomass_dry=self.biomass_drying().loc[f"{self.I.index[0]} dry"]

        #Considering the biomass mixtures properties from Montiel-Bohorquez & Perez (2022)
        #Assuming that all volatiles will be released during the pyrolysis stage
        FC=MontielBohorquez_Perez.Table1().loc["FC", "FL100"]/100 #assumed taht all fixed carbon with result into char
        #Ashes are considered the non-oDM ,so it equals to (1-oDM)
        AC=(1-biomass_dry["oDM"]) #assuming that ashes are derivede from non-organic dry matter. 
        VM=1-(FC+AC) #volatile matter

        #volatiles at a dry matter basis
        pyrolysis_prod=pd.DataFrame(index=[], columns=["Q", "U"])
        pyrolysis_prod.loc["volatile matter gases","Q"]=biomass_dry["Q"]*biomass_dry["DM"]*VM
        pyrolysis_prod["U"]=biomass_dry["U"]

        #assuming that char will be derived from fixed carbon (FC)
        pyrolysis_prod.loc["char (biochar from pyrolisis)", "Q"]=biomass_dry["Q"]*biomass_dry["DM"]*FC

        #ash production
        pyrolysis_prod.loc["ashes", "Q"]=biomass_dry["Q"]*biomass_dry["DM"]*AC

        #production of tars are neglected in this part beacuse they are cracked durint the oxidation stage 
        return pyrolysis_prod

    def syngas_production(self):
        """
        Function that returns the production of syngas from de pyrolisis product (volatile gases and char). 
        The default approach is that syngas will be derived from volatile matter (100% converstion rate) and from the partial reduction of char (30% conversion)
        """
        pyrolysis_prod=self.pyrolysis()
        biomass_dry=self.biomass_drying().loc[f"{self.I.index[0]} dry"]
        syngas_production=pd.DataFrame(index=[], columns=[]) 

        if self.syngas_prod=="fixed rate":
            SYNGAS_PROD_RATE=2.5 #m3 of syngas per kg of wood (Mustafa et al., 2017) 
        
            if biomass_dry["U"]=="ton":
                syngas_production.loc["syngas","Q"]=SYNGAS_PROD_RATE*biomass_dry["Q"]*1000
            
            if biomass_dry["U"]=="kg":
                syngas_production.loc["syngas","Q"]=SYNGAS_PROD_RATE*biomass_dry["Q"]
            
            syngas_production.loc["syngas", "U"]="m3"
        
        if self.syngas_prod=="volatile matter":
            pyrolysis_prod=self.pyrolysis()
            #assuming that all volatile matter is converted into gases
            syngas_production.loc["syngas", "Q"]=pyrolysis_prod.loc["volatile matter gases", "Q"]
            syngas_production.loc["syngas", "Q"]=syngas_production.loc["syngas", "Q"]*1000/self.syngas_density()
            syngas_production.loc["syngas", "U"]="m3"

        #syngas composition
        #According to Mustafa el al. (2017) the most valuable gases in syngas are H2 and CO. 
        #Units of H2, CO, CO2, and CH4 are in absolut concentration of %vol
        #Results from Montiel-Bohorquez & Perez (2022) at Equivalance Ratio (ER) = 0.34 (Fig. 5), the rest is assumed to be nitrogen
        #converted char to syngas is assumed to be in the final syngas production
        for gas in ["H2", "CO", "CH4", "CO2"]:
            syngas_production.loc["syngas", gas]=MontielBohorquez_Perez.Fig5().loc[f"ER={self.ER}", gas]/100
        syngas_production.loc["syngas","N2"]=1-(syngas_production.loc["syngas","H2"]+syngas_production.loc["syngas","CO"]+
                                                syngas_production.loc["syngas","CH4"]+ syngas_production.loc["syngas","CO2"])
        
        #Low heating value (LHV) in MJ/Nm3 calculated from the final gas composition of syngas
        #Formula from Salem et al.(2022): LHV=10.78*H2 + 12.63*CO + 35.88*CH4. Congruent with Aguado et al. 2021 (5.25 MJ/Nm3 = 1.41 kWh/Nm3)
        #Low heating value (LHV) originally in MJ/Nm3, converted in kWh/Nm3
        syngas_production.loc["syngas","LHV"]=(syngas_production.loc["syngas","H2"]*10.78+syngas_production.loc["syngas","CO"]*12.63
                                                +syngas_production.loc["syngas","CH4"]*35.88)*self.MJ_to_kWH

        #TODO: 30% of char is converted to syngas add this quantities. 

        return syngas_production

    def syngas_density(self):

        syngas_comp=MontielBohorquez_Perez.Fig5().loc[f"ER={self.ER}"]
        nitrogen_share=100-(syngas_comp.sum()-syngas_comp["LHV"])
        syngas_density=(syngas_comp["H2"]*self.H2_DENSITY+syngas_comp["CO2"]*self.CO2_DENSITY+
                        syngas_comp["CH4"]*self.CH4_DENSITY+syngas_comp["CO"]*self.CO_DENSITY+nitrogen_share*self.N2_DENSITY)/100

        return syngas_density

    def output(self):
        
        SELF_ELEC_CONSUMPTION=0.15 #set in 15% According to Aguado et al. 2021; Montiel-Bohórquez & Perez (2022)
        
        pyrolysis_prod=self.pyrolysis()
        syngas_prod=self.syngas_production()
        output=pd.DataFrame(index=[], columns=[])
        if self.energy_technology=="turbine":
            #NOTE: Efficiency of the microturbine varies in the literature (Aguado et al., 2021 [25% - conservative]; Vargas & Ramirez, 2017[35% - optimistic])
            #assuming a thermal energy efficiency of 42% of LHV syngas produced (Aguado et al. 2021)
            #Thermal loss set as the residual energy after electricity production and thermal reciculation, meaning: potential energy -(elec_energy+thermal_reciculation)
            output.loc["electrical energy", "Q"]=syngas_prod["LHV"][0]*syngas_prod["Q"][0]*self.electrical_energy_eff*(1-SELF_ELEC_CONSUMPTION)
            output.loc["electrical energy", "U"]="kWh"

            #Available thermal energy is energy left after recirculation
            THERMAL_ENERGY_PROCUCED=syngas_prod["LHV"][0]*syngas_prod["Q"][0]*self.thermal_efficiency 
            ENERGY_REQUIREMENT_DRYING=self.energy_drying()["Q"][0]

            if THERMAL_ENERGY_PROCUCED>=ENERGY_REQUIREMENT_DRYING:
                output.loc["available thermal energy", "Q"]=THERMAL_ENERGY_PROCUCED-ENERGY_REQUIREMENT_DRYING 
            else:
                output.loc["available thermal energy", "Q"]=0

            output.loc["available thermal energy", "U"]="kWh"

            #thermal loss
            output.loc["thermal loss", "Q"]=syngas_prod["LHV"][0]*syngas_prod["Q"][0]*(1-(self.electrical_energy_eff+self.thermal_efficiency))
            output.loc["thermal loss", "U"]="kWh"

        #biochar production
        output.loc["unconverted biochar","Q"]=pyrolysis_prod.loc["char (biochar from pyrolisis)", "Q"]*0.70 #char convertion to 30% according to You et al. (2017) Fig 2. 
        output.loc["unconverted biochar","U"]=self.I["U"][0]
        #NOTE: Composition is still in development and refinement
        output.loc["unconverted biochar","DM"]=1 #assuming no water content in biochar
        output.loc["unconverted biochar","oDM"]=0.85 #assuming 15% of ashes, same concentration that on leaves
        output.loc["unconverted biochar","orgC"]=0.28 #for gasification of leaves according IPCC (2019)
        output.loc["unconverted biochar","F_permp"]=0.89 #for gasification (>600 °C)

        #ashes production
        output.loc["ashes","Q"]=pyrolysis_prod.loc["ashes", "Q"]
        output.loc["ashes","U"]=self.I["U"][0]

        return output 

    def fuel_consum_thermal_recirculation(self):
        
        syngas_prod=self.syngas_production()
        THERMAL_ENERGY_PROCUCED=syngas_prod["LHV"][0]*syngas_prod["Q"][0]*self.thermal_efficiency 
        ENERGY_REQUIREMENT_DRYING=self.energy_drying()["Q"][0]

        # energy_input=self.energy_drying()
        # thermal_input_from_recirculation=self.output().loc["thermal energy - recirculation"]
        
        if ENERGY_REQUIREMENT_DRYING>=THERMAL_ENERGY_PROCUCED: #in kWH
            energy_required=THERMAL_ENERGY_PROCUCED-ENERGY_REQUIREMENT_DRYING 
        else:
           energy_required=0

        # output.loc["available thermal energy", "U"]="kWh"


        # if self.thermal_recirculation==True:
        #    energy_required=energy_input.loc["energy for drying process"]["Q"]-thermal_input_from_recirculation["Q"]
        #    if energy_required<0:
        #         energy_required=0
        # else:
        #     energy_required=energy_input.loc["energy for drying process"]["Q"]

        fuel_consum=pd.DataFrame(index=[], columns=[])
        fuel_consum.loc["fuel consumption (diesel)","Q"]=energy_required/(GHG_protocol.Table1().loc["Gas/Diesel oil", "Lower heating value"]*self.MJ_to_kWH) #kg of fuel (Diesel) required
        fuel_consum.loc["fuel consumption (diesel)","U"]="kg"
        
        return fuel_consum

    def syngas_combustion(self): 
        """
        Returns the emissions from combustion of syngas. Emissions rate is given per KJ of energy produced
        """
        #combustion of syngas
        energy_production=self.output().loc["electrical energy"]
        combustion_exhaust=pd.DataFrame(index=[], columns=[])
        
        #TODO: check whether there are another option for  GHG emissions from syngas combusiton
        #CO2 emissions according to Ghenai (2010) for Tampa syngas
        CO2_emis_rate=0.0001 #kg/kJ produced
        NOx_emis_rate=0.00000003 #kg/kJ produced
        combustion_exhaust.loc["exhaust emissions", "CO2"]=CO2_emis_rate/self.MJ_to_kWH*1000*energy_production["Q"]
        combustion_exhaust.loc["exhaust emissions", "NOx"]=NOx_emis_rate/self.MJ_to_kWH*1000*energy_production["Q"]

        return combustion_exhaust

    def emissions(self):

        output=self.output()
        exhaust_emissions=self.syngas_combustion()
        emis=Emissions()

        LIFETIME_SHARE=self.lifetime_share()
        OPERATION_SHARE=self.operation_share()

        #emissions from construction of gasification plant
        #NOTE: According to Ilari et al. 2022, publications to processes of gasification are shown in Table 9. From there information given in
        #Yang et al. 2018 is considered for the emissions during the construction phase
        EMIS_CONSTRUCTION=3200 #kg CO2eq/kW, rounded for a lifetime of 20 years
        
        #Assumption: emissions from construction/operation calculated from the time that the feedstock could supply the plant (shares)      
        if self.new_infrastructure==True:
            emis.loc["construction", "CO2"] = EMIS_CONSTRUCTION * self.kW * LIFETIME_SHARE #kg of CO2eq
        else:
            emis.loc["construction", "CO2"] = 0

        #emissions from operation and maintenance, from Yang et al. 2018
        EMIS_O_AND_M=1655 #kg CO2eq/kW*year
        emis.loc["operation and maintenance","CO2"]=EMIS_O_AND_M*self.kW*OPERATION_SHARE
        
        #emissions wastewater treatment in kg CH4 per year per kW according to Yang et al. 2018
        CH4_EMIS_WASTEWATER=82 #kg CH4/kW*year
        emis.loc["wastewater treatment", "CH4"]=CH4_EMIS_WASTEWATER*self.kW*OPERATION_SHARE

        #drying process fuel consumption
        fuel_consum=self.fuel_consum_thermal_recirculation().loc["fuel consumption (diesel)","Q"]
        emis.loc["drying process", "CO2"]=fuel_consum*GHG_protocol.Table1().loc["Gas/Diesel oil", "Mass basis"]/1000 #mass basis is in kg CO2 per tonne of fuel
        emis.loc["drying process", "CH4"]=fuel_consum*GHG_protocol.Table2().loc["Gas/Diesel oil", "Mass basis"]/1000 
        emis.loc["drying process", "N2O"]=fuel_consum*GHG_protocol.Table3().loc["Gas/Diesel oil", "Mass basis"]/1000 
     
        #syngas combustion
        emis.loc["syngas combustion (exhaust emissions)"]=exhaust_emissions.loc["exhaust emissions"]

        #fuel supply
        fuel_supply_emis=fuel_consum*convert_molmasses(Ecoinvent34(activity='diesel production, low-sulfur', product='diesel, low-sulfur', unit="kg", location="Europe without Switzerland").emissions(),to="molecule")
        fuel_supply_emis.rename({'diesel production, low-sulfur':"fuel supply"}, inplace=True)
        for gas in ["CO2", "CH4", "N2O"]:
            emis.loc["fuel supply", gas]=fuel_supply_emis.loc["fuel supply", gas]

        #emissions (negative ) from char sequestration
        #NOTE: Approach no loger used due to double-counting of GHG emissions from assimulation
        # BIOCHAR_CO2_SEQUESTRATION=-500 #kg of CO2eq/tonne of feedstock --> GWP 100 for carbon sequestration in biochar, according to Tisserant and Cherubini (2019)
        # emis.loc["carbon sequestration from char", "CO2"]=output.loc["unconverted biochar", "Q"]*BIOCHAR_CO2_SEQUESTRATION

        return convert_molmasses(emis, to="element")

    def feed_in_tariff(self):
        #Tariff for biomass, regardless the method of convertion
        dict_EEG_21_tarrif={2021:12.60,
                            2022:12.54,
                            2023:12.47,
                            2024:12.41}

        if self.EEG_tariff[0]=="EEG_2012":
        #based on the digression equation derived from "Leitfaden Biogas" by FNR (2016). Part of the EEG
        #tariff in EUR/kWh

            if self.kW<150:
                elec_tariff=(-0.2662*self.year_assessment+549.8)/100
            elif self.kW>=150 and self.kW<500:
                elec_tariff=(-0.2292*self.year_assessment+473.3)/100
            elif self.kW>=500 and self.kW<5000:
                elec_tariff=(-0.205*self.year_assessment+423.44)/100
            elif self.kW>5000:
                elec_tariff=(-0.112*self.year_assessment+231.33)/100

        if self.EEG_tariff[0]=="EEG_2021":
            if self.kW<150:
                elec_tariff=dict_EEG_21_tarrif[self.year_assessment]/100 #EUR/kWh fixed price (https://biogas.fnr.de/rahmenbedingungen/eeg-2021)
            elif self.kW>=150:
                elec_tariff=0.1728 # EUR/kWh mean value the bidding in 2022 (https://www.bundesnetzagentur.de/DE/Fachthemen/ElektrizitaetundGas/Ausschreibungen/Biomasse/Gebotstermin01092022/start.html)
    
        if self.EEG_tariff[0]=="manual":
            elec_tariff=self.EEG_tariff[1]

        return elec_tariff

    def heating_tariff(self):

        #NOTE: Heat tariff at the moment is under the considerations if the bidding system of December 2022

        if self.thermal_energy_tariff[0]=="bidding average":
            tariff=0.0614 #Value for 2022. Source: https://www.bundesnetzagentur.de/DE/Fachthemen/ElektrizitaetundGas/Ausschreibungen/KWK/BeendeteAusschreibungen/KWK1122022/start.html

        elif self.thermal_energy_tariff[0]=="Herbes et al 2018":
            tariff=0.026 #Value for 2018

        elif self.thermal_energy_tariff[0]=="manual":
            tariff=self.thermal_energy_tariff[1]           
        
        return tariff
    
    def area_requirements(self):

        AREA_REQUIREMENTS=(0.1554*self.kW+35.54)*1.25 #in m2,  
        #Info for area requirmements: https://www.syncraft.at/holzkraftwerke/uebersicht and https://fee-ev.de/11_Branchenguide/2018_Industry_Guide_Biomass_Gasification_EN.pdf. 

        print(f"The area required for a {self.kW} kW gasification plant is {AREA_REQUIREMENTS} m2")
        return AREA_REQUIREMENTS

    def cashflow(self):
        """
        |  All cashflows for invesment and operation on gasification
        |  **NOTE:**
        |  - These costs are not yet related to the production quantities.
        """
        #NOTE:Check if retrofitting options should be also considered in the installation costs
        INSTALLATION_COST=2875*math.pow(self.kW, -0.108) #equation adjusted from several publications on capital costs, price in EUR/kW
        
        #NOTE: Linear correlation assumed, with around 25% more space for additional infraestructure
        
        ### CAPEX
        #Costs of invesment and installation
        #NOTE: The assumption is that the investment cost would happend without any lifetime_share. 
        Cashflow_capex = pd.DataFrame(index=[], columns=[])
        if self.new_infrastructure==True:
            Cashflow_capex.loc["installation and infraestructure", "EUR"]=INSTALLATION_COST*self.kW
        else:
            Cashflow_capex.loc["installation and construction", "EUR"]=1 #NOTE: this value just to avoid ZeroDivisionError
        
        if self.land_purchase==True:
            Cashflow_capex.loc["land purchase", "EUR"]=self.area_requirements()*LandPrice().purchase()["Q"][0] #EUR/m2

        Cashflow_capex["period_years"] = 20
        Cashflow_capex["section"] = "capex"

        ### OPEX
        output=self.output()
        energy_input=self.energy_drying()
        PRICE_DIESEL=InputPrice().diesel()["Q"][0]
        FEED_IN_TARIFF=self.feed_in_tariff()
        OPERATION_SHARE=self.operation_share()
        DIESEL_DENSITY=0.83 #kg/liter
        #SURCHARGE_VALUE=0.0614 #Value for 2022. Source: https://www.bundesnetzagentur.de/verDE/Fachthemen/ElektrizitaetundGas/Ausschreibungen/KWK/KWK1122022/start.html

        Cashflow_opex = pd.DataFrame(index=[], columns=[])
        #NOTE: Approach, if no purchase of land, than is renting of land
        if self.land_rent==True:
            Cashflow_opex.loc["land rent", "EUR"]= LandPrice().rent()["Q"][0] * self.area_requirements() #EUR/m2
        
        #Information retrieved from Aguado et al 2022., Table 4
        OPER_AND_MAIN_COSTS=0.01 # EUR/kWhe
        Cashflow_opex.loc["operation and maintenance", "EUR"]=output.loc["electrical energy"]["Q"]*OPER_AND_MAIN_COSTS*OPERATION_SHARE
        Cashflow_opex.loc["personnel", "EUR"]=80*self.kW*OPERATION_SHARE
        #NOTE: no external electricity consumption as is considered to be self consumption from own generation
        fuel_consum=self.fuel_consum_thermal_recirculation().loc["fuel consumption (diesel)","Q"] #kg of fuel (Diesel) required
        Cashflow_opex.loc["fuel for drying process", "EUR"]=energy_input.loc["energy for drying process"]["Q"]*fuel_consum*PRICE_DIESEL/DIESEL_DENSITY
        #TODO: Include or check what additional inputs should be included
        Cashflow_opex.loc["feedstock purchase", "EUR"]=float(np.NaN)
        
        #Revenues
        Cashflow_opex.loc["electricity to the grid", "EUR"]=output.loc["electrical energy"]["Q"]*FEED_IN_TARIFF*-1
        Cashflow_opex.loc["thermal energy", "EUR"]=output.loc["available thermal energy"]["Q"]*self.thermal_energy_use*self.heating_tariff()*-1 
        Cashflow_opex.loc["char sales"]=output.loc["unconverted biochar"]["Q"]*0.15*1000*-1 #sales in EUR/kg Aguado et al 2022., Table 4
        Cashflow_opex["period_years"] = 1
        Cashflow_opex["section"] = "opex"

        return pd.concat([Cashflow_capex, Cashflow_opex])


#NOTE: Old method for calculation O&M costs calculation (Indrawan, et al. 2020)
# Cashflow_opex.loc["personnel", "EUR"]= 4640*12/EUR_to_USD #price in USD for one month, adapted to one year
# Cashflow_opex.loc["additional input", "EUR"]=(9000*cost_electricity + 2142/EUR_to_USD + 225/EUR_to_USD + 16*EUR_to_USD
#                                             + 480/EUR_to_USD)*12
# Cashflow_opex.loc["mantainance", "EUR"]=280*12/EUR_to_USD