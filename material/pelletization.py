import pandas as pd
import numpy as np
import math

from declarations import Process, Emissions, Product
from tools import prod, convert_molmasses, LHV_correction
from read.Prices import InputPrice, LandPrice
from read.Ecoinvent import Ecoinvent34

class PelletFactory(Process):

    """
    |  Describes the process of biomass pelletization consisting in in two sub-processes: drying and pelletizing. 

    :type production_rate: int or float
    :param production_rate: amount of pellet produced, expressed in tonnes per hour
    :type operation_hours: int
    :param operation_hours: amount of hours per year of operation
    :type binder: str
    :param binder: type of binder sustance (generally glycerol)
    :type binder_concentration: int
    :param binder_concentration: concentration of binder (absolute basis)
    :type final_moisture: int
    :param final_moisture: final moisture content of pellets
    """
    
    SVOBODA_DRYING_ENERGY_RATE = 2.6 # MJ/kg wet fuel biomass (Svoboda et al. 2009)
    MJ_to_kWH = 0.27778 # kWh/MJ
    DRYING_ENERGY_RATE = SVOBODA_DRYING_ENERGY_RATE * MJ_to_kWH #kWh/kg

    def __init__(self,
                 I = prod("tree leaves", 1, "ton"),
                 production_rate = 12,
                 operation_hours=7500,
                 binder="glycerol",
                 binder_concentration=0.05, 
                 only_drying = False,
                 final_moisture = 0.10,
                 new_infrastructure = True,
                 land_purchase = True,
                 land_rent = False,
                 emissions_calculation = "Ecoinvent",
                 id=None):
        self.I = I
        self.production_rate = production_rate #tonnes/h
        self.operation_hours = operation_hours
        self.binder = binder
        self.binder_concentration = binder_concentration
        self.only_drying = only_drying #NOTE: this funcion enables just the calculation of drying
        self.final_moisture = final_moisture
        self.new_infrastructure = new_infrastructure
        self.land_purchase = land_purchase
        self.land_rent = land_rent
        self.emissions_calculation = emissions_calculation
        self.id=id

    def biomass_drying(self):

        biomass_wet=self.I.copy()
        biomass_dry=self.I.copy().rename(index={f"{self.I.index[0]}":f"{self.I.index[0]} dry"})

        #water vapour
        water_vapour=pd.DataFrame(index=["water vapor"], columns=["Q","U"])
        water_vapour["Q"][0]=((1-self.final_moisture)-biomass_wet["DM"][0])*biomass_wet["Q"][0]
        water_vapour["U"][0]=biomass_wet["U"][0]

        #dry biomass
        biomass_dry["Q"]=biomass_wet["Q"][0]-water_vapour["Q"][0]
        biomass_dry["DM"]=(1-self.final_moisture) #Fig 9. (González et al. 2020)
        #no changes in oDM and orgC, since they are expresed at a DM basis
        for nutrient in ["N", "P", "K"]:
            biomass_dry[nutrient][0]=biomass_wet["Q"][0]*biomass_wet[nutrient][0]/biomass_dry["Q"][0]
        
        #changes in biomass
        total_ash=biomass_wet["Q"][0]*biomass_wet["ash content"][0]
        biomass_dry["ash content"]=total_ash/biomass_dry["Q"]

        return pd.concat([water_vapour, biomass_dry])

    def output(self):
        biomass_input=self.biomass_drying()
        biomass_input=biomass_input[biomass_input.index.isin([f"{self.I.index[0]} dry"])]
        biomass_input=biomass_input.rename(index={f"{self.I.index[0]} dry":f"{self.I.index[0]} pellets"})

        if "tree leaves" in self.I.index:
            output=biomass_input.copy()
            output.loc[f"{self.I.index[0]} pellets", "density"]=0.47 #tonne/m3, bulk density, Fig 11 (González et al. 2020)

            # #Fuel value index (FVI) calculation, NOTE: density of leaves in tonne/m3 and has to be changed to kg/m3 (x1000)
            # FVI=(output.loc[f"{self.I.index[0]} pellets", "density"] * 1000 * biomass_input["LHV dry"][0])/(biomass_input["ash content"][0] * (1-biomass_input["DM"][0])) * math.pow(10,-6) #MJ/cm³     
            # output.loc[f"{self.I.index[0]} pellets", "FVI"]=FVI #MJ/cm³
        
        if self.binder=="glycerol":
            output.loc[f"{self.I.index[0]} pellets", "glycerol"]=self.binder_concentration #wet basis (González et al. 2020)
        
        #LHV correction 
        DM=output.loc[f"{self.I.index[0]} pellets", "DM"]
        output=LHV_correction(output, H=5.4, DM=DM)
        #similar values of LHV in Montiel-Bohórquez & Perez (2022)
        
        return output

    def operation_share(self):
        max_prod_pellet = self.production_rate * self.operation_hours #full capacity in one year
        pellet_prod=self.output()["Q"][0]

        return pellet_prod/max_prod_pellet

    def additional_input(self):
        Input=Product()
        dry_biomass=self.output()

        #info for biomass dryers: https://www.jtdryer.com/product/biomass/52.html, last row (model JTSG2912/3)
        CAPACITY_DRYER=12.14 #tonne biomass/hour
        POWER_DRYER=155 #kW
        ENERGY_DRYER=self.I["Q"][0]/CAPACITY_DRYER*POWER_DRYER

        electricity_dryer=pd.DataFrame({"Q":ENERGY_DRYER, "U": "kWh"}, index=["electrical energy - dryer"])

        #info for pelletizer: https://www.alibaba.com/product-detail/8-18-tons-Hour-Drum-Logs_1600305227069.html?spm=a2700.7724857.0.0.5f2c1b63t4vKN3
        #model: JLX216
        CAPACITY_PELLETIZER=10 #tonne biomass/hour
        POWER_PELLETIZER=70 #kW, rounded sum of all components
        ENERGY_PELLETIZER=dry_biomass["Q"][0]/CAPACITY_PELLETIZER*POWER_PELLETIZER

        electricity_pelletizer=pd.DataFrame({"Q":ENERGY_PELLETIZER, "U": "kWh"}, index=["electrical energy - pelletizer"])

        #glycerol (trichloropropane)
        GLYCEROL = dry_biomass["Q"][0] * dry_biomass["glycerol"][0] * 1000 #in kg

        #total electricity input
        EXTERNAL_ELECTRICAL_ENERGY=electricity_dryer["Q"][0]+electricity_pelletizer["Q"][0]
        
        Input = pd.concat([Input,
                        prod('electrical energy', EXTERNAL_ELECTRICAL_ENERGY, "kWh", location="DEU", source='external.Electricity'),
                        prod('trichloropropane', GLYCEROL, "kg", location="GLO", activity="trichloropropane production", source='external.ExternInput')])

        return Input

    def emissions(self):
        output=self.output()
        #NOTE: Data from emissions from a plant in Ecoinvent, wood pellet factory of a 50 000 tonnes per year production
        emis_factory_prod=Ecoinvent34(activity='wood pellet factory production', product="wood pellet factory", unit="unit", location= "RER").emissions()
        emis_factory_prod=emis_factory_prod.rename(index={"wood pellet factory production":"pellet factory production"})
        emis_factory_prod=convert_molmasses(emis_factory_prod*(output["Q"][0]*1000/2500000000), to="element") #lifetime capactity expresed in kg of pellet produced

        #pelletization emissions according to Chen (2009)
        if self.emissions_calculation=="Chen_2009":
            ENERGY_BIOMASS_GJ=output["Q"][0]/output["density"][0]*math.pow(10,6)*output["LHV"][0]/1000
            emis_pellet_prod=pd.DataFrame(index=[], columns=[])
            emis_pellet_prod.loc["pellet production","CO2"]=0.3925*ENERGY_BIOMASS_GJ
            emis_pellet_prod.loc["pellet production","NOx"]=0.0183*ENERGY_BIOMASS_GJ
            emis_pellet_prod=convert_molmasses(emis_pellet_prod, to="element")
        
        elif self.emissions_calculation=="Ecoinvent":
            emis_pellet_prod=Ecoinvent34(activity='wood pellet production', product="wood pellet, measured as dry mass", unit="kg", location= "RER").emissions()
            emis_pellet_prod=emis_pellet_prod.rename(index={"wood pellet production":"pellet production"})
            emis_pellet_prod=emis_pellet_prod * output["Q"][0]*1000 #total production per tonne

        return pd.concat([emis_factory_prod, emis_pellet_prod])

    def area_requirement(self):
        #more information: 
        # biomass dryer: https://www.jtdryer.com/product/biomass/52.html
        # pelletizer: https://www.alibaba.com/product-detail/8-18-tons-Hour-Drum-Logs_1600305227069.html?spm=a2700.7724857.0.0.5f2c1b63t4vKN3
        #NOTE: area changes depending on the capactiy
        area_requirement_dryer = 10 * 24 #m2
        area_requirement_pelletizer = 18 * 19 #m2
        
        return area_requirement_dryer+area_requirement_pelletizer

    def cashflow(self):

        #CAPEX
        PRICE_LAND=LandPrice().purchase()["Q"][0] #EUR/m2
        RENT_LAND=LandPrice().rent()["Q"][0] #EUR/m2
        OPERATION_SHARE=self.operation_share()
        #Dryer
        Cashflow_capex=pd.DataFrame(columns=[], index=[])
        if self.new_infrastructure==True: #new infraestructure in this case is considered for machinery purhcase. 
            Cashflow_capex.loc["biomass rotary dryer, purchase", "EUR"]=30000 #info: https://zzjtian.en.made-in-china.com/product/sZXfYgAVhPUl/China-New-Design-Biomass-Sawdust-Rotary-Drum-Dryer-Equipment-Wood-Chips-Paddy-Straw-Dryer-Sugarcane-Bagasse-Spent-Grain-Industrial-Dryer-Drying-Machine-Price.html 
            Cashflow_capex.loc["pelletizer, pruchase", "EUR"]= 7000 # info: https://www.alibaba.com/product-detail/8-18-tons-Hour-Drum-Logs_1600305227069.html?spm=a2700.7724857.0.0.5f2c1b63t4vKN3, rounded to the next level due to capactiy
        else:
            Cashflow_capex.loc["machinery purchase", "EUR"]=1 # to avoid division per 0 error
        
        if self.land_purchase==True:
            Cashflow_capex.loc["land purchase", "EUR"]=self.area_requirement() * PRICE_LAND
        else:
            Cashflow_capex.loc["land purchase", "EUR"]=1
        
        Cashflow_capex["period_years"] = 20
        Cashflow_capex["section"] = "capex"
        
        #OPEX
        #NOTE: At the moment, just representing cost of personnel and land rent if applies. Electricty consumption calculated in the additional_input function
        Cashflow_opex=pd.DataFrame(columns=[], index=[])
        Cashflow_opex.loc["personnel", "EUR"]=240000 * OPERATION_SHARE #asumming 1 people for pelletizing and 1 for drying, 3 shifts, 40000 EUR p.a, (Hoque et al, 2013)
        if self.land_rent==True:
            Cashflow_opex.loc["land rent", "EUR"]= self.area_requirement() * RENT_LAND
        #NOTE: cost of glycerol calculated in Extern module
        Cashflow_opex["period_years"] = 1
        Cashflow_opex["section"] = "opex"

        return pd.concat([Cashflow_capex, Cashflow_opex])
