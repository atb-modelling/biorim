"""
The regions module works as a wrapper of individual production to the level of whole regions. This is currently under development.
"""

import pandas as pd
from read import Regionaldatenbank
from read import Eurostat
from production.plant.crop import ArableLand
from tools import prod


## define different regions as production entities, basically as a wrapper function to existing crop & livestock functions with predefined values
# a similar approach could be chosen for farms. They are just a wrappe for other functions, which takes values for a certain location (e.g. information on soil, etc.)

## the outputs of a region would have to be all agricultural products of this region
# depending on the region this would have to be based on very different statistics: for German NUTS3 regions for instance Regionaldatenbank

## should it turn back all crops, or only selected ones?

## need a list of region selectors
# this should work to select NUT3 regions based for instance on NUTS2 regions (for Brandenburg return all NUTS3 regions)

## this should be a multiindex: first dimension is the Country, second NUTS2, etc.



## possibly create a hash table from the Bundeslaender
# eurostat.NUTS1_Bundeslaender()


## as an example try to model the production of, winter wheat, 




class NUTS3Region:
    """
    |  This should represent the production within regions.
    |  *NOTE:*
    |  Currently under development.
    """
    def __init__(self, O = prod(["winter wheat, grain", "oat, grain"]), region="DE4", NUTS_level="NUTS1"):
        self.O = O
        self.region = region
        self.NUTS_level = NUTS_level # this is the level at which the "region" parameter is specified
        # year should also be specified later on

    ## the production values (at the moment according to the DESTATIS Regionaldatenbank)
    def production(self):

        ### crop production information
        ## to dos
        # - aggregate to the same definitions, e.g. winter & summer barley to barley
        # - add animal production
        # - possibly read the production values directly if available
        # - remove duplicates (summarizing categories)

        ## read area information from the Regionaldatenbank
        A = Regionaldatenbank.crop_area() # crop area in ha
        A.name = "area"
        # for the moment drop all summarized categories
        A.drop(columns=["arable land", "cereals", "wheat", "green-harvested crops", "oil seeds", "region"], inplace=True)
        A.columns
        A.rename(columns={'winter wheat (incl. dinkel and einkorn wheat)': "winter wheat, grain",
                          'grain maize/corn-cobb-mix': "grain maize, grain",
                          'barley': "barley, grain",
                          'oat': "oat, grain",
                          'rye and maslin': "rye, grain"},
                           inplace=True)

        Y = Regionaldatenbank.yields() # dt/ha
        Y.drop(columns="region", inplace=True)
        Y.rename(columns={'winter wheat': "winter wheat, grain",
                    'oat': "oat, grain",
                    'rye and maslin': "rye, grain"},
                    inplace=True)
        Y.name = "yield"

        # selection of region (this means no aggregation at the moment)
        if self.region is not None:
            A = A.xs(key=self.region, level=self.NUTS_level)
            Y = Y.xs(key=self.region, level=self.NUTS_level)

        # selection of crops
        if self.O is not None:
            indexer = pd.Index(self.O.index)
            A = A.loc[:,indexer]
            Y = Y.loc[:,indexer]

        # Production merge yield and area
        P = A * Y / 10 # production in ton

        # remove the unused top level
        P.index = P.index.droplevel()

        return P

    # input should work in the same way. It should just list the inputs for all regions
    def input(self):
        return None

    # output should work as a wrapper function to the specified inputs
    # it should basically open the respective production function (e.g. production.plant.crop for wheat)
    # and than multiply the production figures with it, and return a geopandas?
    # maybe, as this is supposed to work as a wrapper function, the core function should also be specified?
    # How about the material flows in the background? Where does the manure from animal production go?
    def output(self):
        
        P = self.production()
        # columns to multiindex:
        P = P.stack(dropna=True).to_frame()
        P.columns = pd.Index(["Q"])

        # get the contents from the ArableLand function
        ## at the moment it doesn't really pay off to put the quantities into the crop function.
        # Later on it might make sense, since the production at different locations might lead to different contents, yields etc.
        ## access ArableLand function

        cropinfo = ArableLand(O=self.O).output()

        Out = P.join(cropinfo, on="item", rsuffix="_rel")
        Out.drop(columns="Q_rel", inplace=True)

        # side products need to be added as well
        return Out



    ## There should be a method that returns a geopandas?
    # It could transform the output into a geodataframe
    def geo_output(self):
        NUTS3 = eurostat.NUTS3()



### to do:
## define a list of crops that the crop function can supply

## define all default products. There should be a lookup table where the default values are written to.

"""
## two ways to access all Brandenburg areas
idx = pd.IndexSlice
CropArea.loc[idx[:,"DE4",:],:]

CropArea.xs(key="DE4", level="NUTS1")

"""