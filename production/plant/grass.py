"""
Different types of grassland production
"""

from tools import prod
from declarations import Process, Product, Emissions
from read import LfL_BY, KTBL
import numpy as np

class Meadow(Process):

    """     
    | Represents a grassland that produces green grass mainly for mowing.

    :type O: Product
    :param O: e.g. prod("grass", 1, "ton")

    |  **NOTE:**
    |  Currently this just supplies grass/hay for testing of ruminant production.

    |  **TODO:**
    |  - Differentiate between different grass types
    |  - Include machine usage needed to harvest the grass
    |  - Adapt for several output at once

    |
    """

    def __init__(self, O = prod("grass", 1, "ton"), type="Wiesengras 1. Schnitt, Beginn Blüte", id = None):
        self.O = O
        self.type = type
        self.id = id

    def output(self):
        
        if all(self.O.index.get_level_values("item") == "grass"):
            if all(self.O["U"] == "ton"):

                Composition = LfL_BY.nutrient_fodder_crops(per_FM=True).loc[[self.type], :]

                Output = Product(Composition.values, columns=Composition.columns, index=["grass"])
                Output["N"] = Output["XP"]/6.25

                KTBL_info = KTBL.biogas_biogas_yields().loc["Grassilage",:] # NOTE: This is currently the value for silage gras, not really fresh grass
                Output.loc[:, "B0"] = KTBL_info.loc["CH4 yield"]
                Output.loc[:, "CH4 content"] = KTBL_info.loc["CH4 content"]
                Output.loc[:, "oDM"] = KTBL_info.loc["oDM"]
                Output.loc[:, "B0"] = KTBL_info.loc["CH4 yield"]

                # Organic carbon content of grasses typically around 45% of DM
                Output["orgC"] = 0.45 / Output.loc[:, "oDM"]

                Output["Q"] = self.O["Q"].item()
                Output["U"] = "ton"

            else:
                raise ValueError("Meadow only supports O with unit 'ton'")

        return Output

    def emissions(self):

        # Carbon sequestration in the plant
        Grass = self.output().copy()

        Emis = Emissions()

        Emis.loc[:,"CO2-C"] = -1 * (Grass["Q"] * Grass["DM"] * Grass["oDM"] * Grass["orgC"]) * 1000

        return Emis


# consider whether it makes more sense to put this into a processing module
# however, this is closely linked to the grassland production and has no linkages to other systems.

class Haymaking(Process):
    """
    |  The conversion of wet grass to hay.

    :type O: Product
    :param O: e.g. prod(O="grass", 1, "ton")
    :type I: Product
    :param O: e.g. prod(O="hay", 1, "ton")


    |  **NOTE:**
    |  - Not fully developed yet, rather just a test case.
    |  - Currently it just assumes the new composition from LfL Bayern, instead it should use some decay function

    |  **TODO:**
    |  - Include the collection of grass from the meadow
    |  - This should already include losses from leaf shattering, raking losses, baling losses, etc.
    |  - Possibly include missmanagement (e.g. leaching from rain, or too late harvest)

    |  **Examples:**
    |  Haymaking(O=prod("hay", 10, "ton")).input()
    |  Haymaking(I=prod("grass", 10, "ton")).input()
    |  Haymaking(I=prod("grass", 10, "ton")).output()
    """

    def __init__(self, I = prod(("test", "grass"), 1, "ton"), O = None, loss=0.2, id = None):

        # when O is defined manually and I is left as default, set I to None
        if isinstance(O, Product) and (I.equals(prod(("test", "grass"), 1, "ton"))):
            self.I = None
            self.O = O
        # when I is defined and O is None:
        elif isinstance(I, Product) and (O is None):
            self.I = I
            self.O = O
        # when both (I and O) are set
        elif isinstance(I, Product) and isinstance(O, Product):
            raise ValueError("I and O cannot bet defined at the same time")

        self.loss = loss
        self.id = id


    # For field baling, hay should contain less than 25 per cent moisture (Monroe 1946)
    # Consider 
    # Consider that yield data from statistical offices probably already considers the losses, since this is what farmers will report
    # Seems as if hay drying is mainly done for cows that are in majority fed with hay (Heumilchkühe)

    # Function can either be defined by input or output

    def input(self):
        if self.I is not None:
            Input = self.I
        else:
            Input = prod(("production.plant.grass.Meadow","grass"),self.O["Q"].item() * (1 + self.loss), "ton")

        return Input

    # for the moment assume that "Wiesengras 1. Schnitt, Beginn Blüte" is the input and "Wiesenheu 1.Schnitt, Mitte der Blüte" is the output
    def output(self):

        if isinstance(self.O, Product):
            QUANTITY = self.O["Q"].item()
        elif isinstance(self.I, Product):
            QUANTITY = self.I["Q"].item() * (1 - self.loss)

        Composition = LfL_BY.nutrient_fodder_crops(per_FM=True).loc[["Wiesenheu 1.Schnitt, Mitte der Blüte"], :]

        Output = Product(Composition.values, columns=Composition.columns, index=["hay"])
        Output["N"] = Output["XP"]/6.25
        Output["oDM"] = 0.90 # Average value from LfL Bayern: Biogasausbeuten verschiedener Substrate https://www.lfl.bayern.de/iba/energie/049711/?sel_list=15%2Cb&anker0=substratanker#substratanker
        Output["Q"] = QUANTITY
        Output["U"] = "ton"

        return Output
