"""
Street trees script content the information of fallen tree leaves (FTL) from streets as feedstock for compost
and biogas production. Currenty design for FTL from Berlin and set of species reported by laboratory experiments
conducted by Herrmann et al. (unpublished)
"""

import pandas as pd
import numpy as np

from declarations import Emissions, Product, Process 
from read import Herrmann, Terboven, Pnakovic_Dzurenda_2015
from tools import prod
import warnings

class StreetTrees(Process):

    """
    Represents the chemical properties from autumn tree leaves depending of the specified origin of the leaves or mixture of leaves
    :type O: Product
    :param O: fallen tree leaves to be used as feedstock
    :type origin: string 
    :param origin: For mixutres-> Berlin_mixture. For single trees-> species reported by Herrmann et al. (Unpublished)
    """

    def __init__(self, O = prod("tree leaves", 1, "ton"), origin="Berlin_mixture", density=0.20, dryness_level=None, id = None):
        self.origin = origin
        if self.origin not in ["Maple", "Walnut", "Oak", "Alder", "Lime tree", "Poplar", "Black locust", "Plane tree", "Birch", "Horbeam", "Chestnut", "Berlin_mixture"]:
            raise warnings.warn("Type must be 'Maple', 'Walnut', 'Oak', 'Alder', 'Lime tree', 'Poplar', 'Black locust','Plane tree', 'Birch', 'Horbeam', 'Chestnut', 'Berlin_mixture'")
        self.O = O
        self.density=density
        self.dryness_level=dryness_level
        self.id = id


    def output(self):
        if self.origin == "Berlin_mixture": #this leaves mixture is reported by Terboven et al. 2017, it includes other non-leaves materials.
            
            Tree_Share = Terboven.Table2().loc["Maple":"Plane tree",:]
            Tree_Share["mass share"] = Tree_Share["Mass"]/Tree_Share["Mass"].sum()
            Tree_Share.drop(columns="Mass", inplace=True)

            Leaves_composition = Herrmann.Chemical_composition()
            Leaves_composition["DM"] = Leaves_composition["DM"]/100
            Leaves_composition["oDM"] = Leaves_composition["oDM"]/100
            Leaves_composition["orgC"] = Leaves_composition["C"]/Leaves_composition["oDM"]/100
            Leaves_composition["N"] = Leaves_composition["N"]*Leaves_composition["DM"]*10

            #LHV value according 
            #Determining elementary oxigen based on the elemental summatory approach
            Elemental_comp=Herrmann.Chemical_composition()[["DM", "C", "N", "H"]]
            Elemental_comp["O"]=100-Elemental_comp[["C", "N", "H"]].sum(axis=1)
            #According to Sheng & Azevedo (2005): HHV = -1.3675 + 0.3137C + 0.7009H + 0.0318O
            Leaves_composition["HHV"]=-1.3675 + 0.3137*Elemental_comp["C"] + 0.7009*Elemental_comp["H"]+ 0.0318*Elemental_comp["O"]
            #According to USEPA LHV = HHV - 10.55*(W + 9H)
            Leaves_composition["LHV"]=Leaves_composition["HHV"] - 10.55*((100-Elemental_comp["DM"]) + 9*Elemental_comp["H"])/100

            Leaves_Methane = Herrmann.Methane_production().loc[:,["Methane yield","Methane content"]]
            Leaves_Methane["Methane yield"] = Leaves_Methane["Methane yield"]/1000 #Methane yield in Herrmann expresed in liters, while the model uses m3. 
            Leaves_Methane["Methane content"] = Leaves_Methane["Methane content"]/100
            Leaves_Methane.rename({'Methane yield':"B0", 'Methane content':"CH4 content" }, axis=1, inplace=True)

            Leaves_composition = Leaves_composition.join(Leaves_Methane)
            Leaves_composition.loc["Beech",:] = Leaves_composition.loc["Mean",:] # No values for Beech. Assume mean value
            #Leaves_composition.loc["Erlen",:] = Leaves_composition.loc["Mean",:]
            #Leaves_composition.loc["Linden",:] = Leaves_composition.loc["Mean",:]

            Tree_Data = Tree_Share.join(Leaves_composition, how="left")

            Weighted_Composition = Tree_Data[["DM", "oDM", "B0", "CH4 content","N", "P", "K", "orgC", "LHV"]].multiply(Tree_Data["mass share"], axis="index").sum().to_frame(name="tree leaves").transpose()
            Output = self.O.copy()
            Weighted_Composition.index = Output.index
            TreeLeaves = Output.join(Weighted_Composition)
        
        else:
            TreeLeaves = self.O.copy()
            TreeLeaves["DM"] = Herrmann.Chemical_composition().loc[self.origin,"DM"]/100
            TreeLeaves["oDM"] = Herrmann.Chemical_composition().loc[self.origin,"oDM"]/100
            TreeLeaves["B0"]=Herrmann.Methane_production().loc[self.origin,"Methane yield"]/1000 #Methane yield in Herrmann expresed in liters, while the model uses m3
            TreeLeaves["CH4 content"]=Herrmann.Methane_production().loc[self.origin,"Methane content"]/100
            TreeLeaves["orgC"]=Herrmann.Chemical_composition().loc[self.origin,"C"]/Herrmann.Chemical_composition().loc[self.origin,"oDM"]
            TreeLeaves["P"]=Herrmann.Chemical_composition().loc[self.origin,"P"]
            TreeLeaves["K"]=Herrmann.Chemical_composition().loc[self.origin,"K"]
            TreeLeaves["N"]=Herrmann.Chemical_composition().loc[self.origin,"N"]*TreeLeaves["DM"]*10

        TreeLeaves["ash content"]=0.15 #in dry basis, NOTE: to be specified according to the type of leaf
        
        #scientific names of tree leaves--> in case of comparison with academic literature
        scientific_name_dict={"Maple": "Acer platanoides", 
                            "Walnut": "Juglans regia",
                            "Oak": "Quercus robur",
                            "Alder":"Alnus glutinosa",
                            "Lime tree":"Tilia cordata",
                            "Poplar":"Populus maximowiczii x P. nigra and Populus maximowiczii x P. trichocarpa",
                            "Black locust":"Robinia pseudoacacia",
                            "Plane tree":"Platanus x hispanica",
                            "Birch":"Betula pendula",
                            "Hornbeam":"Carpinus betulus",
                            "Chestnut":"Aesculus hippocastanum"
        }
        if self.origin in scientific_name_dict:
            TreeLeaves["scientific name"]=scientific_name_dict[self.origin]
        else:
            TreeLeaves["scientific name"]="not specified"
        
        ##NOTE:This section was meant to create a logic flow when some conditions of density definition are met, however not working due to conflict with the __init__ in start.py
        # #this will be solved eventually. Now, densitiy can be manually defined with a default value of 0.2       
        # density_dict={  "Pimont et al., 2015": 0.1,
        #                 "Chojnacky et al., 2009":0.03,
        #                 "Green waste":0.2,
        # }

        # if type(self.density) == float and self.density not in list(density_dict.keys()):
        #     TreeLeaves["density"]=self.density
        #     raise warnings.WarningMessage (f"The density has been specified manually. Other options are: {list(density_dict.keys())}")
        # elif type(self.density) != float and self.density not in list(density_dict.keys()):
        #     raise ValueError (f"Density values must been expressed as float format. String-arguments supported are: {list(density_dict.keys())}")
        # elif self.density in list(density_dict.keys()):
        #     TreeLeaves["density"]=density_dict[self.density] # g/cm3=kg/l=ton/m3
        # else:
        #     TreeLeaves["density"]=self.density
        TreeLeaves["density"]=self.density #g/cm3 or kg/l or tonne/m3
        # Scien_name=TreeLeaves["scientific name"][0]
        

        #TODO: Change approach for determining LHV
        # if Scien_name in list(scientific_name_dict.values()):
        #     TreeLeaves["LHV fresh"]=Pnakovic_Dzurenda_2015.Fig3().loc[Scien_name,"LHV-Wr"] / 1000 # in MJ/kg, originally in kJ/kg
        #     TreeLeaves["LHV dry"]=Pnakovic_Dzurenda_2015.Fig3().loc[Scien_name,"LHV-Wr0"] / 1000 # in MJ/kg, originally in kJ/kg
        # else: 
        #     TreeLeaves["LHV fresh"]=Pnakovic_Dzurenda_2015.Fig3().loc["Mean","LHV-Wr"] / 1000 # in MJ/kg, originally in kJ/kg
        #     TreeLeaves["LHV dry"]=Pnakovic_Dzurenda_2015.Fig3().loc["Mean","LHV-Wr0"] / 1000 # in MJ/kg, originally in kJ/kg

        if self.dryness_level != None: 
            TreeLeaves["DM"]=self.dryness_level

        return TreeLeaves
    

    def emissions(self):
        Emis=Emissions(index=["carbon assimilation by leaves"], columns=["CO2-C"])
        Leaves_composition=self.output()
        #assuming all the organic carbon as CO2-C
        Emis.loc["carbon assimilation by leaves", "CO2-C"]=Leaves_composition["orgC"][0]*Leaves_composition["oDM"][0]*Leaves_composition["DM"][0]*Leaves_composition["Q"][0]*(-1000)
        return Emis 

    def carbon_nitrogen(self):
        """
        Tool for C/N calculation for leaves species or mixtures
        """
        if self.origin == "Berlin_mixture": #this leaves mixture is reported by Terboven et al. 2017, it includes other non-leaves materials.
            
            Tree_Share = Terboven.Table2().loc["Maple":"Plane tree",:]
            Tree_Share["mass share"] = Tree_Share["Mass"]/Tree_Share["Mass"].sum()
            Tree_Share.drop(columns="Mass", inplace=True)

            Leaves_C_N = Herrmann.Chemical_composition().loc["Maple":"Chestnut","C/N"]
            Mixture_C_N = Tree_Share.join(Leaves_C_N, how="left")
            Mixture_C_N.loc["Beech","C/N"] = Herrmann.Chemical_composition().loc["Mean","C/N"] # No values for Beech, Erlen and Linden. Assume mean value
            #Mixture_C_N.loc["Erlen","C/N"] = Herrmann.Chemical_composition().loc["Mean","C/N"]
            #Mixture_C_N.loc["Linden","C/N"] = Herrmann.Chemical_composition().loc["Mean","C/N"]
            Final_C_N=(Mixture_C_N["mass share"]*Mixture_C_N["C/N"]).sum()
        
        else: 
            Final_C_N=Herrmann.Chemical_composition().loc[self.origin,"C/N"]
        
        return Final_C_N

    def cashflow(self):
        """
        |  All cashflows for the plantation (installation) and operation (leaves production)
        |  **NOTE:**
        |  - These costs are not yet related to the production quantities.
        """
        #NOTE: possible information for cashflows of tree leaves generation: https://www.berlin.de/sen/uvk/natur-und-gruen/stadtgruen/stadtbaeume/stadtbaumkampagne/
        # and also here: https://use.metropolis.org/case-studies/berlin-s-city-tree-campaign#casestudydetail 
        Cashflow = pd.DataFrame(columns=["EUR", "period_years", "section"])

        #Costs of plantation (installation)
        Cashflow.loc["tree plantation", "EUR"] = float(np.NaN)
        Cashflow.loc["tree plantation", "period_years"] = float(np.NaN)
        Cashflow.loc["tree plantation", "section"] = "capex"

        #Costs of leaves production (operation)
        Cashflow.loc["leaves production", "EUR"] = float(np.NaN)
        Cashflow.loc["leaves production", "period_years"] = 1
        Cashflow.loc["leaves production", "section"] = "opex"

        return Cashflow
