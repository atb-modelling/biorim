import pandas as pd
import numpy as np
import warnings

from declarations import Product, Process, Emissions 
from read import Thannimalay_2013
from tools import prod, convert_molmasses


class PretreatmentLeaves(Process):
    """
    Represents the chemical pretreatment of FTL with NaOH, according to the results of Liew et al, 2011.
    :type I: Product
    :param I: fallen tree leaves to be used as feedstock
    :type method: string
    :param method: method of pretreatment. Currently "NaOH" is supported. 
    """

    def __init__(self, I = prod("tree leaves", 1, "ton"), method="NaOH", id = None): 
        self.method=method
        if self.method not in ["NaOH"]:
            warnings.warn("Method must be 'NaOH'")
        self.I = I
        self.id = id 

    def emissions(self):
        NaOH_requirement=self.I["DM"][0]*self.I["Q"][0]*0.035 #3.5% of NaOH in TS according to Liew et al. 2011. Requirement expresed in tones. 
        Emis=Emissions(index=["sodium hydroxide manufacture"], columns=["CO2", "CH4", "N2O", "NOx"])
        Emis_NaOH_production=Thannimalay_2013.Table2()
        Emis.loc["sodium hydroxide manufacture", "CO2"]=Emis_NaOH_production.loc["CO2","Total"]*NaOH_requirement*1000
        Emis.loc["sodium hydroxide manufacture", "CH4"]=Emis_NaOH_production.loc["CH4","Total"]*NaOH_requirement*1000
        Emis.loc["sodium hydroxide manufacture", "N2O"]=Emis_NaOH_production.loc["N2O","Total"]*NaOH_requirement*1000
        Emis.loc["sodium hydroxide manufacture", "NOx"]=(Emis_NaOH_production.loc["NOx","Total"]+Emis_NaOH_production.loc["NOx mobile","Total"])*NaOH_requirement*1000
        Emis=convert_molmasses(Emis, to="element")
        return Emis

    def output(self):
        Output = self.I.copy()
        Output["B0"][0]=Output["B0"][0]*1.20 #20% more methane in yield acording to Liew et al. 2011.
        Output["Q"][0]=Output["Q"][0] + self.I["DM"][0]*self.I["Q"][0]*0.035 #NaOH_requirements
        return Output
    
    def cashflow(self):
        """
        |  All cashflows for invesment and operation on leaves pretreatment
        |  **NOTE:**
        |  - These costs are not yet related to the production quantities.
        """
        Cashflow_capex = pd.DataFrame(index=[], columns=[])

        #Costs of inevesment of machinery
        Cashflow_capex.loc["machinery investment", "EUR"] = float(np.NaN)
        Cashflow_capex["period_years"] = 20
        Cashflow_capex["section"] = "capex"

        #Costs of operation
        Cashflow_opex = pd.DataFrame(index=[], columns=[])
        #Price of NaOH retrieved from https://www.icis.com/explore/resources/news/2020/01/28/10461663/europe-caustic-soda-q1-price-falls-10-as-market-readjusts/
        #NOTE: Check the possibility to include NaOH price to the InputPrices module
        PRICE_NAOH=450 # EUR/tonne
        Cashflow_opex.loc["NaOH procurement", "EUR"] = self.I["DM"][0]*self.I["Q"][0]*0.035 * PRICE_NAOH #0.035 reprensents 3.5% NaOH application in dry matter basis
        Cashflow_opex["period_years"] = 1
        Cashflow_opex["section"] = "opex"

        return pd.concat([Cashflow_capex, Cashflow_opex])

