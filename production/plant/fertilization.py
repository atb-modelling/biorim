__all__ = ["BiocharSpreading", "ManureSpreading"]

"""
**The Fertilization script contains classes, methods and functions to calculate the field application fate
of biogas digestate, composted manure and untreated manure.**

"""

import pandas as pd
import numpy as np
import warnings

from tools import get_content, convert_molmasses, prod, O_from_massloss, valid_item
from read import Boldrin_et_al_2010, EEA, Ecoinvent, IPCC, LfL_BY, Rodhe_Karlsson_2002, Thuenen, VDLUFA
from read.Prices import InputPrice
from declarations import Process, Emissions
from material.composting import CompostingLeaves



class MineralFertilizerProduction(Process):
    """
    |  Production of mineral fertilizer. Emissions currently from ecoinvent for ammonium nitrate phosphate.
    |  Excluded from MineralFertilization to show flows of nitrogen.
    |  
    |  **NOTE**:
    |  - This returns the amount of UAN fertilizer, not only the N content.
    |
    |  **TODO**:
    |  - Include other fertilizers
    |  - Include other sources for production emissions

    """

    def __init__(self, O = prod("urea ammonium nitrate", 1, "ton"), id = None):
        if valid_item(O, "urea ammonium nitrate"):
            self.O = O
        else:
            print(f"requested fertilizer {O} not supported")
        self.id = id

    def output(self):
        Output = self.O.copy()
        Output["N"] = 332 #kg N /ton
        return Output

    def emissions(self):
        # assumed UAN has a N content of 32% -> 3.13 kg UAN needed for 1 kg of N
        Emis = Ecoinvent.Ecoinvent34('urea ammonium nitrate production', 'nitrogen fertiliser, as N', unit="kg", location="RER").emissions() * self.O["Q"].item() * 1000 * 3.13
        return Emis



class MineralFertilization(Process):
    """
    |  Fertilization with ammonium nitrate phosphate.
    |  This can currently only be used to account for mineral fertilizer savings (from the use of manure)

    :type I: Product
    :param I: The amount of N fertilizer saved.
    
    |  **NOTE:**
    |  - Mineral fertilization now already included in crop production
    |  - Usage of ammonium nitrate phosphate fertilizer assumed, even though only "N fertilizer" requested. Phosphate also provided.
    |  **TODO:**
    |  - Still missing are emissions from machine usage: trailor which distributes the manure

    """
    def __init__(self, I = prod("N fertilizer", 1, "ton"), id = None):
        self.I = I
        self.id = id

        if not (self.I.index in ['N fertilizer']):
            raise ValueError("Currently only 'N fertilizer' supported as input")

    def emissions(self):
        # obtain potential emissions for similar amount of synthetic fertilizers and declare as compensated/negative emissions
        Emis = Ecoinvent.Ecoinvent34('ammonium nitrate phosphate production',"nitrogen fertiliser, as N", unit="kg", location="RER").emissions() * self.I["Q"].item() * 1000

        # From IPCC 2019 Table 11.1 (N2O emissions from managed soild for other N inputs in wet climates)
        EF_N2O = IPCC.direct_N2O_EF_managed_soils().loc[("EF_1", "synthetic fertiliser inputs in wet climates"), "value"].item()
        Emis.loc['field emissions from mineral N fertilization', 'N2O-N'] = self.I['Q'].item()*1000*EF_N2O

        EF_NH3_NOx =  IPCC.indirect_N2O_EF_soils().loc[("Frac_GASF", "ammonium-nitrate-based"), "value"] # kg NH3-N+NOx-N N volatilization / kg N applied
        Emis.loc['field emissions from mineral N fertilization', 'NH3-N & NOx-N'] = self.I['Q'].item()*1000*EF_NH3_NOx

        return Emis
    



class BiocharSpreading(Process):
    """    
    |  Field spreading of biochars to sequester carbon in soils.
    |  Considers emissions for machinery usage and the decomposition of char to CO2 for fraction not permanent for 100 years.

    :type I: Product
    :param I: the manure or digestate that is spread on the field

    | **NOTE**:
    | - Currently not considered is any potential fertilization effect, or influence on crop yields

    |
    """
    def __init__(self, I = prod("broiler manure pyrochar", 1, "ton"), method="default", id = None):
        self.I = get_content(I, required=["DM","oDM","orgC"])
        self.method = method
        self.id = id

    #TODO: Include the method of application per ha and dose and emissions
    def emissions(self):

        Emis = Emissions(index=["biochar field decomposition"])

        if self.method == "default":
            # The fraction that is not permanent for 100 years (F_permp) is assumed to be emitted as CO2
            F_emit = (1-self.I["F_permp"])
            Emis["CO2-C"] = (self.I["Q"] * self.I["DM"] * self.I["oDM"] * self.I["orgC"] * 1000 * F_emit).sum()

        ## machinery emissions:
        AMOUNT_KG = self.I["Q"].item()*1000
        Emis_spread_machinery = AMOUNT_KG * Ecoinvent.Ecoinvent34('solid manure loading and spreading, by hydraulic loader and spreader', 'solid manure loading and spreading, by hydraulic loader and spreader', unit="kg", location="CH").emissions()
        Emis_spread_machinery.index = ["biochar loading and spreading"]

        return pd.concat([Emis, Emis_spread_machinery])

    def output(self):
        return None

    def cashflow(self):
        GALLON_TO_LITER = 3.78 #converstion rate 1 gallon = 3.78 liter
        DENSITY_DIGESTATE = 0.99 #kg/l

        Input=self.I.copy()

        #Costs of inevesment of machinery
        Cashflow_capex = pd.DataFrame(index=[], columns=[])
        Cashflow_capex.loc["machinery investment", "EUR"] = int(20000) # Rough value from quick search: https://www.landwirt.com/en/used-farm-machinery/categories,1295,13,compost-dung-spreaders-Germany.html
        Cashflow_capex["period_years"] = int(20)
        Cashflow_capex["section"]="capex"

        #Costs of operation
        #Application rate and fuel consumption information taken from: https://cadmanpower.com/manure/pump-set-packages.html 
        FUEL_CONSUMP = 9.6 * GALLON_TO_LITER / 60 #originally gallon per hour, converted to liter diesel / min
        APPLICATION_RATE = 1750 * GALLON_TO_LITER * DENSITY_DIGESTATE #originally in gallon per minute, converted in kg per minute
        DIESEL_PRICE=InputPrice().diesel().loc["diesel", "Q"]
        Cashflow_opex = pd.DataFrame(index=[], columns=[])
        if self.I["U"].item()=="ton":
            Cashflow_opex.loc["fuel consumption - spreading", "EUR"] = (Input["Q"].item() * 1000 * FUEL_CONSUMP * DIESEL_PRICE)/APPLICATION_RATE
        if self.I["U"].item()=="kg":
            Cashflow_opex.loc["fuel consumption - spreading", "EUR"] = (Input["Q"].item() * FUEL_CONSUMP * DIESEL_PRICE)/APPLICATION_RATE
        Cashflow_opex["period_years"] = int(1)
        Cashflow_opex["section"]="opex"

        return pd.concat([Cashflow_capex, Cashflow_opex])


class ManureSpreading(Process):
    """    
    | Field spreading of manures and biogas digestates as a fertilizer.

    :type I: Product
    :param I: the manure or digestate that is spread on the field
    :type type: string
    :param type: "solid" (manure) or "liquid" (mostly slurry) - has impacts on spreading technique and thus emissions
    :type incorporation: str
    :param incorporation: "incorporation <= 4h" or "no incorporation": relevant for emissions of NH3
    :type avg_temp: string
    :param avg_temp: string of numerical value + " °C"
    :type min_fert_savings: string
    :param min_fert_savings: if set to True consider savings of mineral fertilizer

    |
    |  **NOTE**:
    |  * Needs to be checked whether is consistent for cow manure!

    """

    def __init__(self, I = prod("broiler manure", 1, "ton"), incorporation = "incorporation <= 4h", method = "default", avg_temp="10 °C", min_fert_savings=True, id = None):
        # if not (I.index.get_level_values("item") in ['broiler manure composted', 'broiler manure', 'biogas digestate', 'cow manure, liquid']):
        #     raise ValueError("Currently only 'broiler manure composted' or 'broiler manure' supported as input")
        if np.any(I.index) not in ['broiler manure composted', 'broiler manure', 'biogas digestate', 'cow manure, liquid']:
            raise ValueError("Currently only 'broiler manure composted', 'broiler manure', 'biogas digestate' or 'cow manure, liquid' supported as input")
        self.I = get_content(I, required=["N","NH4-N", "P", "K", "DM", "oDM"])
        self.avg_temp = avg_temp
        if I.index in ["cow manure, liquid", "biogas digestate"]:
            self.type = "liquid"
        else:
            self.type = "solid"
        self.incorporation = incorporation # "incorporation <= 4h" or "no incorporation"
        self.method = method # default
        self.min_fert_savings = min_fert_savings
        self.id = id

    def emissions(self):
        """     
        Emissions are calculated for NH3, N2O and NO, see also related functions.
        If drymatter content "DM" is < 0.5 then set type to "liquid" - spreading. This is relevant for biogas digestate or slurry.
        """        
        # everything below 25% DM should be considered liquid. Currently not implemented, because this way impossible to assume implicit mixtures (e.g. cow slurry not modelled explicitly)
#        if self.I["DM"].item() < 0.5: # ?
#            type = "liquid"
#        else:
#            type = "solid"

        I = self.I

        if self.method == "default":
            CO2_method = "VDLUFA"
            N2O_method = "IPCC2019"
            NH3_method = "Haenel2018"
            NO_method = "EEA2016"
            NH3_NOx_method = 0
        elif self.method == "EEA2016":
            CO2_method = "VDLUFA"
            N2O_method = "IPCC2019"
            NH3_method = "EEA2016"
            NO_method = "EEA2016"
            NH3_NOx_method = 0
        elif self.method == "IPCC2019":
            CO2_method = "VDLUFA"
            N2O_method = "IPCC2019"
            NH3_method = 0
            NO_method = 0
            NH3_NOx_method = "IPCC2019"
        else:
            raise ValueError(f"method {self.method} not defined")

        # Emis = Emissions(index=I.index)

        emis_name = "decomposition " + I.index
        Emis = Emissions(index=emis_name)


        ## CO2 emissions resulting from decomposition
        if CO2_method == 0:
            Emis['CO2-C'] = 0
        elif NH3_method == None:
            Emis['CO2-C'] = np.nan
        if CO2_method=="VDLUFA":
            # assumption: carbon that does not contribute to humus production is emitted as CO2
            Emis['CO2-C'] = (I["Q"] * I["DM"] * I["oDM"] * I["orgC"] * 1000 * (1-I["HEQ"])).sum()

        ### Direct emissions resulting from the spreading of manures that usually occur within a few hours after spreading ### 

        ## NH3 emissions
        if NH3_method == 0:
            Emis['NH3-N'] = 0
        elif NH3_method == None:
            Emis['NH3-N'] = np.nan
        elif NH3_method == "Haenel2018":
            # Emission factors relate to UAN. Here simplified NH4-N is used.
            if self.type == "solid":
                NH3_EF = Thuenen.NH3_EF_solid_poultry_manure_spreading().loc[str("broadcast, " + self.incorporation),"value"]
            elif self.type == "liquid":
                # The NH3 emission factors for liquid cattle manure are also used for biogas digestate in Haenel 2018
                NH3_EF = Thuenen.NH3_EF_liquid_cattle_manure_spreading().loc[str("trailing hose, " + self.incorporation),"value"]
            Emis.loc[:,'NH3-N'] = I.loc[:,'Q']*I.loc[:,'NH4-N']*NH3_EF
        elif NH3_method == "EEA2016":
            NH3_N_EF = EEA.NH3_N_EF_manure_management()
            if self.type == "solid":
                NH3hash = pd.Series({'broiler manure': ("Broilers (broilers and parents)","Solid"),
                                    'broiler manure composted': ("Broilers (broilers and parents)","Solid"), # assuming the same emissions in case of composted manure
                                    'turkey manure': ("Turkeys","Solid")})
            elif self.type == "liquid":
                NH3hash = pd.Series({'biogas digestate': ("Dairy cattle","Slurry")})
            EF_NH3 = pd.Series(NH3_N_EF.loc[NH3hash[I.index],"EF_spreading"].values, index=I.index)
            Emis['NH3-N'] = I['Q']*I['NH4-N']*EF_NH3
        elif NH3_method == "Rodhe2002":
            # Emission factors relate to total N content
            NH3_EF = Rodhe_Karlsson_2002.losses_manure_spreading().loc[str("broiler manure, " + self.incorporation),"lost N"]*1e-2
            Emis.loc[:,'NH3-N'] = (I.loc[:,'Q']*I.loc[:,'N']).item()*NH3_EF

        ## N2O emissions
        if N2O_method == 0:
            Emis['N2O-N'] = 0
        elif N2O_method == None:
            Emis['N2O-N'] = np.nan
        elif N2O_method == "IPCC2019":
            # From IPCC 2019 Table 11.1 (N2O emissions from managed soild for other N inputs in wet climates)
            EF_N2O = IPCC.direct_N2O_EF_managed_soils().loc[("EF_1", "other N inputs in wet climates"), "value"].item()
            Emis['N2O-N'] = I['Q']*I['N']*EF_N2O
        else:
            raise ValueError(f"N2O_method {N2O_method} not defined")

        ## aggregated losses of NH3 and NOx (only reported in sum by IPCC2019 method)
        # these are the total losses of manure management (should include emissions from spreading, where much of the volotalization happens)
        if NH3_NOx_method == 0:
            Emis['NH3-N & NOx-N'] = 0
        elif NH3_NOx_method == None:
            Emis['NH3-N & NOx-N'] = np.nan
        elif NH3_NOx_method == "IPCC2019":
            # From IPCC table 11.3:
            EF_NH3_NOx =  IPCC.indirect_N2O_EF_soils().loc[("Frac_GASM", "default"), "value"] # in kg N loss / kg N
            Emis['NH3-N & NOx-N'] = I['Q']*I['N']*EF_NH3_NOx

        ## NO emissions
        if NO_method == 0:
            Emis['NO-N'] = 0
        elif NO_method == None:
            Emis['NO-N'] = np.nan
        elif NO_method == "EEA2016":
            NO_EF = EEA.N_EF_spreading().loc["NO fertilizer, manure and excreta","value"]
            Emis['NO-N'] = (I.loc[:,'Q']*I.loc[:,'N']).sum()*NO_EF

        ### Emission credit for mineral fertilizer savings ###

        # obtain potential emissions for similar amount of synthetic fertilizers and declare as compensated/negative emissions (production and spreading emission)
        if self.min_fert_savings:

            ## compensated/negative emissions, with conversion to kg
            needed_N_ton = -1 * (I["Q"] * I["N"] * I["N_fert_eff"]).sum() / 1000

            Emis = pd.concat([Emis,
                              MineralFertilization(prod("N fertilizer", needed_N_ton, "ton")).emissions()
                            ])

        ### Machinery emissions ###
        if self.type=="liquid":
            ASSUMED_DENSITY = 1 # TODO: replace with real density
            AMOUNT_M3 = (I["Q"]/ASSUMED_DENSITY).sum()
            Emis_spread_machinery = AMOUNT_M3 * Ecoinvent.Ecoinvent34('liquid manure spreading, by vacuum tanker', 'liquid manure spreading, by vacuum tanker', unit="m³", location="CH").emissions()
            Emis_spread_machinery.rename(index={'liquid manure spreading, by vacuum tanker':'liquid manure spreading machine'}, inplace=True)
            Emis = pd.concat([Emis, Emis_spread_machinery])
        elif self.type=="solid":
            AMOUNT_KG = (I["Q"]*1000).sum()
            Emis_spread_machinery = AMOUNT_KG * Ecoinvent.Ecoinvent34('solid manure loading and spreading, by hydraulic loader and spreader', 'solid manure loading and spreading, by hydraulic loader and spreader', unit="kg", location="CH").emissions()
            Emis_spread_machinery.rename(index={'solid manure loading and spreading, by hydraulic loader and spreader':'solid manure spreading machine'}, inplace=True)
            Emis = pd.concat([Emis, Emis_spread_machinery])

        return Emis

    def output(self):
        """     
        The output method is adjusted to the fact that Q balance cannot be computed due to missing CH4 and CO2 emissions. 
        According to Chadwick et al (2011) CH4 emissions from field application are negliable.
        |
        """
        I = self.I
        Output = I.copy()
        Emis = self.emissions()

        # currently not carbon and no N2 emissions assumed
        C_LOSS_TON = Emis.loc[:,["CH4-C", "CO2-C"]].sum().sum() / 1000
        N_LOSS_TON = Emis.filter(regex="-N$", axis=1).loc[I.index,:].sum().sum() / 1000

        Output = O_from_massloss(I, orgC_LOSS_TON=C_LOSS_TON, N_LOSS_TON=N_LOSS_TON, scale_by_mass=["P", "K"], index=I.index)
   
        return Output

    def cashflow(self):
        """
        |  All cashflows for invesment and operation on manure spreading
        |  **NOTE:**
        |  - These costs are not yet related to the production quantities.
        """
        GALLON_TO_LITER = 3.78 #converstion rate 1 gallon = 3.78 liter
        DENSITY_DIGESTATE = 0.99 #kg/l

        Input=self.I.copy()

        #Costs of inevesment of machinery
        Cashflow_capex = pd.DataFrame(index=[], columns=[])
        Cashflow_capex.loc["machinery investment", "EUR"] = int(20000) #Rough value from quick search: https://www.landwirt.com/en/used-farm-machinery/categories,1295,13,compost-dung-spreaders-Germany.htm
        Cashflow_capex["period_years"] = int(20)
        Cashflow_capex["section"]="capex"

        #Costs of operation
        #Application rate and fuel consumption information taken from: https://cadmanpower.com/manure/pump-set-packages.html 
        FUEL_CONSUMP = 9.6 * GALLON_TO_LITER / 60 #originally gallon per hour, converted to liter diesel / min
        APPLICATION_RATE = 1750 * GALLON_TO_LITER * DENSITY_DIGESTATE #originally in gallon per minute, converted in kg per minute
        DIESEL_PRICE=InputPrice().diesel().loc["diesel", "Q"]
        Cashflow_opex = pd.DataFrame(index=[], columns=[])
        if self.I["U"].item()=="ton":
            Cashflow_opex.loc["fuel consumption - spreading", "EUR"] = (Input["Q"].item() * 1000 * FUEL_CONSUMP * DIESEL_PRICE)/APPLICATION_RATE
        if self.I["U"].item()=="kg":
            Cashflow_opex.loc["fuel consumption - spreading", "EUR"] = (Input["Q"].item() * FUEL_CONSUMP * DIESEL_PRICE)/APPLICATION_RATE
        Cashflow_opex["period_years"] = int(1)
        Cashflow_opex["section"]="opex"

        return pd.concat([Cashflow_capex, Cashflow_opex])


class PeatUse(Process):
    """    
    | Peat use for soil fertilization. Assumed to be used for gardening purposes.
    :type I: Product
    :param I: peat
    :type method: string
    :param method: Literature source for the emissions values. Currently set for "Boldrin et al" 
    """

    def __init__(self, I = prod("peat", 1, "ton"), method="Boldrin et al", id=None):
        self.I=I
        if method=="Boldrin et al":
            self.method=method
        else:
            raise ValueError("method must be 'Boldrin et al'")
        self.id = id
    
    def emissions(self):
        Emis = Emissions(Boldrin_et_al_2010.Table7() * self.I["Q"].item())
        return convert_molmasses(Emis, to="element")

class CompostSpreading(Process):
    """    
    | CompostSpreading determine the GHG emissions of spreading finished compost depending on its maturity.
    | It is mostly based on Witchuk & McCartney (2010) findings and uses emissions of CO₂ by mass unit of organic matter (OM).
    :type I: Product
    :param I: leaves compost
    :type maturity: string
    :param maturity: maturity level of compost = "inmature", "mature or "very mature". Applies only to method "Witchuk & McCartney"
    :type method: string
    :param method: method for emissions calculation
    :type peat_substitution: tuple (bool, float)
    :param peat_substitution: if bool=True, then emissions for compost substitution with peat will be calculated, replacement ratio (mass basis) as the float
    """
    

    def __init__(self, I = prod("leaves compost", 1, "ton", DM=0.4, oDM=0.6), maturity="very mature", method="VDLUFA", peat_substitution=(False, 0.8), id=None):
        self.I = I
        self.maturity = maturity
        self.method = method
        self.peat_substitution = peat_substitution
        self.id = id
    
    def emissions(self):
        finished_compost=self.I.copy()
        if self.method=="Witchuk & McCartney":
            years=20
            day_emissions=years*365
            organic_matter=finished_compost["Q"].item()*finished_compost["DM"].item()*finished_compost["oDM"].item()
            Emis=Emissions(index=["compost degradation"], columns=["CO2", "NH3", "N2O", "CH4"])

            #Based on Witchuk & McCartney (2010) findings. 
            CO2_rate = {"very mature": 0.2, #mg CO₂*g⁻¹OM*d⁻¹=kg CO₂*ton⁻¹OM*d⁻¹
                        "mature":      0.3,
                        "inmature":    0.4}

            Emis.loc["compost degradation", "CO2"]=CO2_rate[self.maturity]*day_emissions*organic_matter
            Emis=convert_molmasses(Emis, to="element")

        elif self.method=="VDLUFA":
            Emis=Emissions(index=["compost degradation"])
            Emis.loc["compost degradation","CO2-C"]=finished_compost["Q"].item()*finished_compost["DM"].item()*finished_compost["oDM"].item()*finished_compost["orgC"].item()*1000*(1-finished_compost["HEQ"].item())

        if self.peat_substitution[0]==True: 

            #concat
            #Emis_peat negtaive
            Emis = Emissions(index=["compost substitution with peat"])
            Peat_amount = finished_compost
            Peat_amount["Q"][0] = Peat_amount["Q"].item() * self.peat_substitution[1]
            Emis = PeatUse(Peat_amount).emissions()
            Emis.loc["compost substitution with peat"] = Emis.sum()
            Emis = Emis.tail(1)
            Emis = convert_molmasses(Emis, to="element")

        return Emis
    
    def cashflow(self):
        """
        |  All cashflows for invesment and operation on compost spreading
        |  **NOTE:**
        |  - These costs are not yet related to the production quantities.
        """
        Cashflow = pd.DataFrame(columns=["EUR", "period_years", "section"])

        #At the moment, no cost for investment or operation is considered in this method, as it is assumed that the compost use
        #will be in gardening activities

        #Costs of operation
        Cashflow.loc["compost spreading operation", "EUR"] = float(np.NaN)
        Cashflow["period_years"] = int(1)
        Cashflow["section"] = "opex"

        return Cashflow

class DigestateSpreading(Process):
    """    
    | DigestateSpreading determine the GHG emissions of spreading biogas digestate.
    | It is mostly based on Grigatti et al (2020) findings on food waste (90%) and green waste (10%) digestate application to the soil.
    :type I: Product
    :param I: biogas digestate
    :type method: string
    :param method: method for GHG emissions calculation
    """
    
    def __init__(self, I = prod("biogas digestate", 1, "ton", density=1), method="VDLUFA", id=None):
        self.I=I
        self.method=method
        self.id=id
    
    def emissions(self):
        Emis = Emissions()

        # machinery emissions:
        if self.I["U"] == "ton":
            AMOUNT_m3 = self.I["Q"].item()/self.I["density"]
        else:
            raise ValueError("Unit currently not supported")
        
        # AMOUNT_KG = self.I["Q"].item()*1000
        # Emis_spread_machinery = AMOUNT_KG * Ecoinvent.Ecoinvent34('solid manure loading and spreading, by hydraulic loader and spreader', 'solid manure loading and spreading, by hydraulic loader and spreader', unit="kg", location="CH").emissions()
        
        Emis_spread_machinery = AMOUNT_m3 * Ecoinvent.Ecoinvent34('liquid manure spreading, by vacuum tanker', 'liquid manure spreading, by vacuum tanker', unit="m3", location="CH").emissions()
        Emis_spread_machinery.rename(index={'liquid manure spreading, by vacuum tanker':'liquid manure spreading machine'}, inplace=True)
        # Emis_spread_machinery.index = ["machinery use"]

        # emissions from soil:
        if self.method=="Grigatti et al":
            emissions_ratio= 4 # 4000 mg/kg⁻¹soil = 4kg/ton⁻¹soil
            Emis_soil=pd.DataFrame(index=["emissions from soil"], columns=["CO2"])
            Emis_soil.loc["emissions from soil", "CO2"]=self.I["Q"].item()*emissions_ratio
            Emis_soil=convert_molmasses(Emis_soil, to="element")

        elif self.method=="VDLUFA":
            Emis_soil=pd.DataFrame(index=["emissions from soil"], columns=["CO2-C"])
            Emis_soil.loc["emissions from soil", "CO2-C"]=self.I["Q"].item()*self.I["DM"].item()*self.I["oDM"].item()*self.I["orgC"].item()*1000*(1-self.I["HEQ"].item())

        Emis = pd.concat([Emis_spread_machinery, Emis_soil])
        return Emis


class AshSpreading(Process):
    def __init__(self, I = prod("ash", 1, "ton"), method="default", id=None):
        
        self.I=I
        self.method=method
        self.id=id

    def emissions(self):

        # NOTE: There is information regarding the possible long term (>50 years) CO2 emissions when applying ashes in forest soil, but seems that findings are contradictory (Huotari et al. 2015)
        AMOUNT_KG = self.I["Q"].item()*1000
        Emis_spread_machinery = AMOUNT_KG * Ecoinvent.Ecoinvent34('solid manure loading and spreading, by hydraulic loader and spreader', 'solid manure loading and spreading, by hydraulic loader and spreader', unit="kg", location="CH").emissions()
        Emis_spread_machinery.rename(index={'solid manure loading and spreading, by hydraulic loader and spreader':'ash spreading by hydraulic loader'}, inplace=True)
        Emis = pd.concat([Emis_spread_machinery])

        return Emis

    def cashflow(self):
        GALLON_TO_LITER = 3.78 #converstion rate 1 gallon = 3.78 liter
        DENSITY_ASH = 0.7 #kg/l, assuming 700 kg/m3 from a quick Google search

        Input=self.I.copy()

        #Costs of inevesment of machinery
        Cashflow_capex = pd.DataFrame(index=[], columns=[])
        Cashflow_capex.loc["machinery investment", "EUR"] = int(20000) #TODO:Dummy Value, replace it with a more accurate value
        Cashflow_capex["period_years"] = int(20)
        Cashflow_capex["section"]="capex"

        #Costs of operation
        #Application rate and fuel consumption information taken from: https://cadmanpower.com/manure/pump-set-packages.html 
        FUEL_CONSUMP = 9.6 * GALLON_TO_LITER / 60 #originally gallon per hour, converted to liter diesel / min
        APPLICATION_RATE = 1750 * GALLON_TO_LITER * DENSITY_ASH #originally in gallon per minute, converted in kg per minute
        DIESEL_PRICE=InputPrice().diesel().loc["diesel", "Q"]
        Cashflow_opex = pd.DataFrame(index=[], columns=[])
        if self.I["U"].item()=="ton":
            Cashflow_opex.loc["fuel consumption - ash spreading", "EUR"] = (Input["Q"].item() * 1000 * FUEL_CONSUMP * DIESEL_PRICE)/APPLICATION_RATE
        if self.I["U"].item()=="kg":
            Cashflow_opex.loc["fuel consumption - ash spreading", "EUR"] = (Input["Q"].item() * FUEL_CONSUMP * DIESEL_PRICE)/APPLICATION_RATE
        Cashflow_opex["period_years"] = int(1)
        Cashflow_opex["section"]="opex"

        return pd.concat([Cashflow_capex, Cashflow_opex])