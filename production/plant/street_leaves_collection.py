import pandas as pd
import numpy as np
from tools import prod, convert_molmasses
from declarations import Process, Emissions
from read import EPA, Ecoinvent
from read.Prices import InputPrice
from parameters import ConversionFactor


class LeafBlower: 
    LIFETIME_H = 540 # 6 years and 90 hours per year (same assumptions as by Bianka), although this seems a quite short. Assumption for chainsaw is 2500 h in ecoinvent.
    FUEL_CONSUMPTION_L_PER_H = 0.56 # 2.5 hours per 1.4 l tank
    LEAF_BLOWER_SPEED_KM_PER_H = 0.4 * 3.6 # 0.4 m/s (Bianka's assumption)
    FUEL_CONSUMPTION_L_KM=FUEL_CONSUMPTION_L_PER_H/LEAF_BLOWER_SPEED_KM_PER_H #(liter/km) variable for cashflow calculation 

    def __init__(self, operation_h):
        self.operation_h = operation_h

    def emissions(self):
        # production emissions: estimated to be the same as for chainsaw production (rough estimate)
        Emis_Production = Ecoinvent.Ecoinvent34(activity='power saw production, without catalytic converter', product='power saw, without catalytic converter', unit="n", location="RER").emissions() / self.LIFETIME_H * self.operation_h
        Emis_Production.rename({'power saw production, without catalytic converter':"leaf blower construction"}, inplace=True)

        # emissions for fuel supply: two-stroke petrol
        PETROL_DENSITY_KG_PER_L = 0.74
        Emis_Fuel_Supply = self.operation_h * self.FUEL_CONSUMPTION_L_PER_H * PETROL_DENSITY_KG_PER_L * Ecoinvent.Ecoinvent34(activity='petrol blending for two-stroke engines', product='petrol, two-stroke blend', unit="kg", location="Europe without Switzerland").emissions()
        Emis_Fuel_Supply.rename({'petrol blending for two-stroke engines':"leaf blower fuel supply"}, inplace=True)

        # only CO2 emissions assumed for combustion
        KG_CO2_PER_L_FUEL = 2.33
        Emis_Fuel_Combustion = Emissions({'CO2-C':self.operation_h * self.FUEL_CONSUMPTION_L_PER_H * KG_CO2_PER_L_FUEL * ConversionFactor.loc[("CO2","CO2-C"), "factor"]}, index=["leaf blower fuel combustion"])

        return pd.concat([Emis_Production, Emis_Fuel_Supply, Emis_Fuel_Combustion])


class SmallSweeper: 
    FUEL_CONSUMPTION_L_PER_H = 4.37 #data from Abgeordnetenhaus Berlin for Kleinkehrmaschine (2019)
    LIFETIME_DIST_KM= 300000 # rough assumption
    SERVICE_SPEED_KM_H = 9 # same assumption as Bianka
    LIFETIME_H = LIFETIME_DIST_KM/SERVICE_SPEED_KM_H
    FUEL_CONSUMPTION_L_KM=FUEL_CONSUMPTION_L_PER_H/SERVICE_SPEED_KM_H #(liter/km) variable for cashflow calculation 

    def __init__(self, operation_h):
        self.operation_h = operation_h
    
    def emissions(self):
        #production emissions based on a lorry of 16 metric ton
        Emis_Production = Ecoinvent.Ecoinvent34(activity='lorry production, 16 metric ton', product='lorry, 16 metric ton', unit="n", location="RER").emissions() / self.LIFETIME_H * self.operation_h
        Emis_Production.rename({'lorry production, 16 metric ton':"small sweeper production"}, inplace=True)

        # emissions for fuel supply: diesel
        DIESEL_DENSITY_KG_PER_L = 0.876
        Emis_Fuel_Supply = self.operation_h * self.FUEL_CONSUMPTION_L_PER_H * DIESEL_DENSITY_KG_PER_L * Ecoinvent.Ecoinvent34(activity='diesel production, low-sulfur', product='diesel, low-sulfur', unit="kg", location="Europe without Switzerland").emissions()
        Emis_Fuel_Supply.rename({'diesel production, low-sulfur':"small sweeper fuel supply"}, inplace=True)

        # diesel combustion
        AMERICAN_GALLON_TO_LITER = 3.78541
        MILE_TO_KM = 1.60934
        Emis_Fuel_Combustion = Emissions(index=["small sweeper fuel combustion"], columns=["CO2", "CH4", "N2O", "NH3"])
        Emis_Fuel_Combustion.loc["small sweeper fuel combustion","CO2"] = EPA.Table2().loc["Diesel fuel","CO2"]/AMERICAN_GALLON_TO_LITER* self.FUEL_CONSUMPTION_L_PER_H * self.operation_h
        Emis_Fuel_Combustion.loc["small sweeper fuel combustion","CH4"] = EPA.Table4().loc["Diesel light-duty trucks","CH4"]/MILE_TO_KM * self.operation_h * self.SERVICE_SPEED_KM_H
        Emis_Fuel_Combustion.loc["small sweeper fuel combustion","N2O"] = EPA.Table4().loc["Diesel light-duty trucks","N2O"]/MILE_TO_KM * self.operation_h * self.SERVICE_SPEED_KM_H
        Emis_Fuel_Combustion = convert_molmasses(Emis_Fuel_Combustion, to="element")

        return pd.concat([Emis_Production, Emis_Fuel_Supply, Emis_Fuel_Combustion])

class LargeSweeper: 
    FUEL_CONSUMPTION_L_KM = 0.9763 #Data from Abgeordnetenhaus Berlin for Großkehrmaschine (2019), useful for costs calculatio
    LIFETIME_DIST_KM= 300000 # rough assumption
    SERVICE_SPEED_KM_H = 12 # rough asumption
    FUEL_CONSUMPTION_L_PER_H = FUEL_CONSUMPTION_L_KM * SERVICE_SPEED_KM_H
    LIFETIME_H = LIFETIME_DIST_KM/SERVICE_SPEED_KM_H

    def __init__(self, operation_h):
        self.operation_h = operation_h

    def emissions(self):

        # production emissions based on a lorry of 40 metric ton
        Emis_Production = Ecoinvent.Ecoinvent34(activity='lorry production, 40 metric ton', product='lorry, 40 metric ton', unit="n", location="RER").emissions() / self.LIFETIME_H * self.operation_h
        Emis_Production.rename({'lorry production, 40 metric ton':"large sweeper production"}, inplace=True)
             
        # emissions for fuel supply: diesel
        DIESEL_DENSITY_KG_PER_L = 0.876
        Emis_Fuel_Supply = self.operation_h * self.FUEL_CONSUMPTION_L_PER_H * DIESEL_DENSITY_KG_PER_L * Ecoinvent.Ecoinvent34(activity='diesel production, low-sulfur', product='diesel, low-sulfur', unit="kg", location="Europe without Switzerland").emissions()
        Emis_Fuel_Supply.rename({'diesel production, low-sulfur':"large sweeper fuel supply"}, inplace=True)

        # diesel combustion
        AMERICAN_GALLON_TO_LITER = 3.78541
        MILE_TO_KM = 1.60934
        Emis_Fuel_Combustion = Emissions(index=["large sweeper fuel combustion"], columns=["CO2", "CH4", "N2O", "NH3"])
        Emis_Fuel_Combustion.loc["large sweeper fuel combustion","CO2"] = EPA.Table2().loc["Diesel fuel","CO2"]/AMERICAN_GALLON_TO_LITER* self.FUEL_CONSUMPTION_L_PER_H * self.operation_h
        Emis_Fuel_Combustion.loc["large sweeper fuel combustion","CH4"] = EPA.Table4().loc["Diesel medium- and heavy-duty vehicles","CH4"] * self.operation_h * self.SERVICE_SPEED_KM_H
        Emis_Fuel_Combustion.loc["large sweeper fuel combustion","N2O"] = EPA.Table4().loc["Diesel medium- and heavy-duty vehicles","N2O"] * self.operation_h * self.SERVICE_SPEED_KM_H
        Emis_Fuel_Combustion = convert_molmasses(Emis_Fuel_Combustion, to="element")

        return pd.concat([Emis_Production, Emis_Fuel_Supply, Emis_Fuel_Combustion])


class StreetLeavesCollection(Process):
    """  
    Process of fallen tree leaves (FTL) collection considering leaf blower, and small and large sweeper usage
    """
    LEAF_BLOWER_SHARE = 0.7
    SMALL_SWEEPER_SHARE = 0.8
    LARGE_SWEEPER_SHARE = 0.9

    def __init__(self, I = prod("tree leaves", 1, "ton"), road_length_km = 100, id = None):
        self.I = I
        self.road_length_km = road_length_km
        self.id = id
    
    def operation_hours(self):

        LEAF_BLOWER_SPEED_KM_PER_H = 0.4 * 3.6 # 0.4 m/s (Bianka's assumption)
        LEAF_BLOWER_OPERATION_H = self.road_length_km * self.LEAF_BLOWER_SHARE / LEAF_BLOWER_SPEED_KM_PER_H

        SMALL_SWEEPER_SPEED_KM_PER_H = 9 #Bianka's assumption
        SMALL_SWEEPER_OPERATION_H = self.road_length_km * self.SMALL_SWEEPER_SHARE / SMALL_SWEEPER_SPEED_KM_PER_H

        LARGE_SWEEPER_SPEED_KM_PER_H = 12 #Rough assumption
        LARGE_SWEEPER_OPERATION_H = self.road_length_km * self.LARGE_SWEEPER_SHARE / LARGE_SWEEPER_SPEED_KM_PER_H
        
        oper_hours=pd.DataFrame(index=[], columns=[])
        oper_hours.loc["operation hours", "leaf blower"]=LEAF_BLOWER_OPERATION_H
        oper_hours.loc["operation hours", "small sweeper"]=SMALL_SWEEPER_OPERATION_H
        oper_hours.loc["operation hours", "large sweeper"]=LARGE_SWEEPER_OPERATION_H

        return oper_hours

    def emissions(self):

        oper_hours=self.operation_hours()

        Emis_leaf_blower = LeafBlower(operation_h = oper_hours.loc["operation hours", "leaf blower"]).emissions()

        Emis_small_sweeper = SmallSweeper(operation_h = oper_hours.loc["operation hours", "small sweeper"]).emissions()

        Emis_large_sweeper = LargeSweeper(operation_h = oper_hours.loc["operation hours", "large sweeper"]).emissions()

        return pd.concat([Emis_leaf_blower, Emis_small_sweeper, Emis_large_sweeper])

    def output(self):
        return self.I

    def cashflow(self):
        """
        |  All cashflows for invesment of machinery and operation on street leaves collection
        |  **NOTE:**
        |  - These costs are not yet related to the production quantities.
        """
        oper_hours=self.operation_hours()
        Cashflow = pd.DataFrame(columns=["EUR", "period_years", "section"])

        #Costs of inevesment of machinery
        Cashflow.loc["machinery investment", "EUR"] = float(np.NaN)
        Cashflow.loc["machinery investment", "period_years"] = float(np.NaN)
        Cashflow.loc["machinery investment", "section"] = "capex"

        #Costs of operation
        #Calculations of total fuel (petrol and diesel) consumption for the length given
        PETROL_CONSUMPTION=(LeafBlower(oper_hours.loc["operation hours", "leaf blower"]).FUEL_CONSUMPTION_L_KM*self.LEAF_BLOWER_SHARE*self.road_length_km)
        DIESEL_CONSUMPTION=(SmallSweeper(oper_hours.loc["operation hours", "small sweeper"]).FUEL_CONSUMPTION_L_KM*self.LEAF_BLOWER_SHARE*self.road_length_km +
                            LargeSweeper(oper_hours.loc["operation hours", "large sweeper"]).FUEL_CONSUMPTION_L_KM*self.LEAF_BLOWER_SHARE*self.road_length_km)

        
        Cashflow.loc["fuel consumption", "EUR"] = (DIESEL_CONSUMPTION*InputPrice().diesel()["Q"].item()+
                                                    PETROL_CONSUMPTION*InputPrice().petrol()["Q"].item())
        Cashflow.loc["fuel consumption", "period_years"] = 1
        Cashflow.loc["fuel consumption", "section"] = "opex"

        return Cashflow