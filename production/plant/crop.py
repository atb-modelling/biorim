__all__ = ["ArableLand"]

## define a very simple crops function, at the moment only based on LCA or other literature data
# import start
import pandas as pd
import warnings
from tools import relpath, prod, get_content, valid_item
from read import Ecoinvent, LfL_BY, KTBL, IPCC, VDLUFA
from read.Prices import InputPrice, LandPrice
from declarations import Emissions, Process, Product
from production.plant.fertilization import ManureSpreading
from functools import cached_property
from tools import report

class ArableLand(Process):

    """     
    | Represents crop production on arable land. E.g. production of winter wheat, maize, etc.
    | Crop contents are derived from LfL Bayern, Basisdaten and Gruber Tables
    | Fertilization with UAN fertilizer, to compensate for missing N not supplied via I

    :type I: Product
    :param I: only manure allowed as fertilizer
    :type O: Product
    :param O: e.g. prod(O="winter wheat, grain")
    :type side_products: boolean
    :param side_products: Whether side-products such as straw should be returned as well. If not than straw improves the humus balance.
    :type mineral_fertilizer: str
    :param mineral_fertilizer: Currently only "UAN-32" supported
    :type area_ha: str
    :param area_ha: The area of crop production. Currently only relevant for the fertilization.
    :type Nmin: int
    :param Nmin: Nmin content in soil. Used to calculate fertilizer demand.
    :type avg_yield_dt_ha: int
    :param avg_yield_dt_ha: Average yield in dt per ha. Used to calculate fertilizer demand.

    |  **TODO:**
    |  - Quantities of main products not calculated, e.g. grain quanitiy for O="oat, straw" and side_products=True
    |  - potentially change from Process determined by O to Process determined by X (or whatever the state variable is)
    |    both should be possible: define area and crop, function returns output; define crop, function determines required area
    |  - Add CaCO addition to compensate for N fertilization and respective CO2 emissions
    |  - Add values for orgC that differentiate between crops. Currently just 50% in DM assumed.
    |  - Differentiate between different wheat qualities. Currently values for food quality and fodder quality mixed.
    |  - Add proper data sources for data that is currently added just for single crops (e.g. oDM of rapeseed)

    |  **Examples:**
    |  ArableLand().avail_crops
    |  ArableLand(O=prod("winter wheat, grain", 5, "ton")).output()
    |  ArableLand(O=prod("silage maize, whole", 5, "ton")).output()
    |  ArableLand(O=prod("silage maize, whole", 5, "ton")).input()
    |  ArableLand(O=prod(["barley, grain", "silage maize, whole"], [5, 10], ["ton","ton"])).output()
    |  ArableLand(O=prod("winter wheat, grain", 5, "ton"), side_products=False).output()
    |
    """
    # NOTE: Assume a carbon content in dry mass of 50% until better data available
    # For silage maize a content of 0.45 in dry matter is reported in https://www.kompost.de/fileadmin/news_import/HUK11_08_1u2_Gaerrueckstaende__1_.pdf
    # For lettuce about 0.35 (https://www.tandfonline.com/doi/full/10.1080/01904167.2022.2043369)     
    orgC_of_DM = 0.5


    def __init__(self, I = None, O = prod("winter wheat, grain", 1, "ton"),
                 side_products=False,
                 mineral_fertilizer="UAN-32",
                 Nmin = 50,
                 avg_yield_dt_ha = None,
                 rent_land=True,
                 id = None):
        
        self.side_products = side_products

        if I is None:
            self.I = None
        elif valid_item(I, ["broiler manure", "cow manure, liquid", "biogas digestate"]):
            self.I = get_content(I, required=["N", "DM", "oDM"])

        self.avail_crops = [i + ", " + e for i, e in pd.read_csv(relpath("production\\plant\\crop_items.csv"), sep=";", encoding="latin-1", index_col=[0,1]).index]
        
        if valid_item(O, self.avail_crops):
            self.O = O
        else:
            raise Exception("ArableLand: requested crop not available")
        
        self.Nmin = Nmin

        self.mineral_fertilizer = mineral_fertilizer
        self.rent_land=rent_land
        self.id = id

        ## indexer for item&element combinations (specifies one element, e.g. "winter wheat, grain")
        self._item_element_indexer = pd.MultiIndex.from_tuples([tuple(i) for i in self.O.index.str.split(", ").to_list()], names=["item", "element"])
        ## indexer for all elements of on item (e.g. "winter wheat" so that later on "winter wheat, grain" and "winter wheat, straw" are selected)
        self._item_indexer = pd.Index([i[0] for i in self.O.index.str.split(", ").to_list()], name="item")
        
        # make the selection whether side products are reported at the end of the output method
        self._idx = pd.read_csv(relpath("production\\plant\\crop_items.csv"), sep=";", encoding="latin-1", index_col=[0,1]).loc[self._item_indexer,:]

        # yield per ha either defined by parameter or taken from table value
        if avg_yield_dt_ha is None:
            nutient_crops_indexer = [(row["LfL BY Hauptfrucht"], row["LfL BY Ernteprodukt"]) for index, row in self._idx.iterrows()]
            self.avg_yield_ton_ha = LfL_BY.nutrient_crops(to_element=False).loc[nutient_crops_indexer,"avg yield (ton/ha)"].dropna(axis="index").item()
        else:
            self.avg_yield_dt_ha = avg_yield_dt_ha

        self.area_ha = self.O["Q"].item() / self.avg_yield_ton_ha


    # @cached_property
    def mineral_N_fertilizer_demand_ton(self):
        """
        |  Calculates the demand for mineral nitrogen fertilizer based on default values from LfL Bayern, the input of organic fertilizer via I.
        |  Missing in comparison with DüV are the effect of fertilization from the previous year, cover crops, and N from humus

        |  **TODO**:
        |  - Adapt to consider actual yields of the crops
        """

        # Düngebedarfsermittlung zu Winterraps nach DüV 2020
        # https://www.landwirtschaftskammer.de/landwirtschaft/ackerbau/duengung/duengeverordnung/duev-2020.htm
        # N-Bedarfswert (N kg/ha) 	200
        # Korrekturen: 	 
        # 1. Ertragsdurchschnitt letzten 5 Jahre =  ø 45 dt/ha 	+ 10
        # 2. Herbstdüngung (30 kg/ha Ammonium-N als Gülle) 	- 30
        # 3. Nmin-Gehalt (kg/ha):  gemessen = 25 kg/ha N              	- 25
        # 4. Standort (Bodenart, Bodentyp, Klima…) „Humusgehalt > 4,0%“ 	- 0
        # 5. Organische Düngung im Vorjahr (ja/nein): 170 kg/ha N     	- 17
        # 6. Vor- und Zwischenfrüchte 	 
        #         Vorfrucht: Getreide  	- 0
        #         Zwischenfrucht: keine 	- 0
        # N-Düngebedarf (kg/ha):  	138


        idx = self._idx

        # fertilizer demand for specific crop
        # NOTE: the unique removes the demand for side-products. Currently no allocation in the model
        N_Fertilizer_Demand = LfL_BY.N_fertilizer_demand_arable().loc[idx["LfL BY fertilizer demand"].unique(),:]

        TABULAR_YIELD_VALUE = N_Fertilizer_Demand["yield_level_dt_per_ha"].item()

        # basic nitrogen demand of the crop (for standard yield levels)
        BASIC_N_DEMAND_CROP = N_Fertilizer_Demand["N_need_kg_per_ha"].item() # depends on culture, e.g. winter wheat a/b

        ## adjustment due to difference of average historical yields to table values
        if self.avg_yield_ton_ha is not None:
            YIELD_DIFF_TABLE = self.avg_yield_ton_ha * 10 - TABULAR_YIELD_VALUE
        else:
            YIELD_DIFF_TABLE = 0

        if YIELD_DIFF_TABLE == 0:
            N_ADDITION_YIELD_LEVEL = 0
        elif YIELD_DIFF_TABLE > 0:
            RATIO_ACT_TAB = YIELD_DIFF_TABLE/N_Fertilizer_Demand["per_difference_dt"].item()
            N_ADDITION_YIELD_LEVEL = N_Fertilizer_Demand["increase"].item() * RATIO_ACT_TAB
        elif YIELD_DIFF_TABLE < 0:
            RATIO_ACT_TAB = YIELD_DIFF_TABLE/N_Fertilizer_Demand["per_difference_dt"]
            N_ADDITION_YIELD_LEVEL = N_Fertilizer_Demand["decrease"].item() * RATIO_ACT_TAB

        ## input via organic fertilization
        # slight deviation from DüV, since N_fert_eff values may differ, and since losses are not accounted here (maybe indirectly via losses during ManureSpreading)

        if self.I is not None:
            N_ORGANIC_FERT_I_KG_PER_HA = (self.I["Q"]*self.I["N"]*self.I["N_fert_eff"]).sum() / self.area_ha
            report(f"calculated N input by organic fertilizer is {N_ORGANIC_FERT_I_KG_PER_HA} kg/ha")
        else:
            N_ORGANIC_FERT_I_KG_PER_HA = 0

        MINERAL_N_FERTILIZER_DEMAND_KG_PER_HA = float(BASIC_N_DEMAND_CROP + N_ADDITION_YIELD_LEVEL - self.Nmin - N_ORGANIC_FERT_I_KG_PER_HA)

        if MINERAL_N_FERTILIZER_DEMAND_KG_PER_HA < 0 and N_ORGANIC_FERT_I_KG_PER_HA > 0:
            report("organic fertilizer input already exceeds demand")
            MINERAL_N_FERTILIZER_DEMAND_KG_PER_HA = 0
        else:
            report(f"calculated mineral N fertilizer demand is {MINERAL_N_FERTILIZER_DEMAND_KG_PER_HA} kg/ha")

        return MINERAL_N_FERTILIZER_DEMAND_KG_PER_HA / 1000 * self.area_ha


    def humus_balance_crop_HEQ_per_ha(self):
        """
        |  Calculates the change in soil organic matter content based on crop demand. Organic fertilization not yet considered.
        |  If side_products=False than straw is assumed to remain on the field and improves the humus balance.
        |  Unit in HEQ/(ha a)
        | 
        | **NOTE:**
        | - Currently does not consider use of side products properly. Especially when they are requested as output?
        | - It would be better to assess crop rotations that include cover crops (which add humus) 
        """
        idx = self._idx
        item_indexer = self._item_indexer
        item_element_indexer = self._item_element_indexer
        Products = self._products()


        ## Humus consumption by crops
        # NOTE: Whether low, middle or high values should be chosen depends on soils and management
        HumusBalance = VDLUFA.humus_consumption_crops().loc[idx.loc[item_element_indexer, "VDLUFA"],"middle value"]
        HumusBalance.index = [i[0] + ", " + i[1] + " " + "consumption"  for i in item_element_indexer]
        
        ## Humus reproduction by (cover) crops
        # NOTE: Currently not implemented, because only single crops are considered
        # For the future it would be better to consider crop rotations

        ## Humus reproduction by leaving straw on the field (assumption if side_products=False)
        if not self.side_products:
            sideprod_idx = idx.loc[item_indexer].index.difference(idx.loc[item_element_indexer].index)
            if "straw" in sideprod_idx.get_level_values("element"):
                Straw = Products.xs('straw', level="element", drop_level=False)
                HEQ = VDLUFA.humus_reproduction().loc["plant straw", "HEQ [/t mass]"]
                StrawHumusReprod = Straw["Q"] * HEQ / self.area_ha
                StrawHumusReprod.index = [i[0] + ", " + i[1] + " " + "reproduction"  for i in list(Straw.index)]
                HumusBalance = pd.concat([HumusBalance, StrawHumusReprod])

        return HumusBalance


    def humus_balance_HEQ_per_ha(self):
        """
        Humus balance including organic fertilization
        """
        I = self.I
        HumusBalance = self.humus_balance_crop_HEQ_per_ha()

        # NOTE: Maybe put this into a seperate method, so that is can be used seperately in the emissions method
        if I is not None:
            OrgFertHumusReprod = I["Q"] * I["DM"] * I["oDM"] * I["orgC"] * 1000 * I["HEQ"] / self.area_ha
            OrgFertHumusReprod.index = [i + " reproduction" for i in list(I.index)]
            HumusBalance = pd.concat([HumusBalance, OrgFertHumusReprod])
 
        return HumusBalance


    def additional_input(self):
        """
        |  Additional inputs of mineral fertilizer potentially required 
        """
        MINERAL_N_FERTILIZER_DEMAND_TON = self.mineral_N_fertilizer_demand_ton()
        if MINERAL_N_FERTILIZER_DEMAND_TON > 0:
            if self.mineral_fertilizer == "UAN-32":
                # assumed UAN has a N content of 32% -> 3.13 kg UAN needed for 1 kg of N
                # N content added as intermediate solution
                return prod("urea ammonium nitrate", MINERAL_N_FERTILIZER_DEMAND_TON * 3.13, "ton", source="production.plant.fertilization.MineralFertilizerProduction")
            else:
                raise ValueError("selected mineral_fertilizer not supported")
        else:
            return None

    def emissions(self):
        """
        Emissions from crop production. This considers the carbon content of crops (current assumption is 50% of dry matter) as negative emissions.
        |  **NOTE:**
        | - Including the emissions from ManureSpreading directly simplifies the modelling, but it is not longer possible to set any arguments (e.g. emission method)
        |
        |  **TODO:**
        |  - Include emissions from machinery.
        |  - Add some allocation between main crops and side-products?
        |  - Include option for emission factor from: Mathivanan GP, Eysholdt M, Zinnbauer M, et al (2021) New N2O emission factors for crop residues and fertiliser inputs to agricultural soils in Germany. Agric Ecosyst Environ 322:. https://doi.org/10.1016/j.agee.2021.107640
        |  - And also include options for different fertilizers, which have very different NH4 emission factors
        |  - Include effect on soil humus: VDLUFA approach !! Currently not consistent. CO2 emissions also in fertilization

        """
        Emis = Emissions()

        # 1. Organic fertilization
        if self.I is not None:
            # NOTE: This currently assumes that all carbon that does not contribute to the formation of humus is released.

            ManureEmis = ManureSpreading(I=self.I, min_fert_savings=False).emissions()

            Emis = pd.concat([Emis, ManureEmis])

        # 2. Carbon fixation from the atmosphere
        Crop = self.output().copy()

        CARBON_CROP_KG = (Crop["Q"] * Crop["DM"] * self.orgC_of_DM).sum() * 1000

        HUMUS_C_BUILDUP_KG = (self.humus_balance_HEQ_per_ha() * self.area_ha).sum()
        HUMUS_C_BUILDUP_CROP_KG = (self.humus_balance_crop_HEQ_per_ha() * self.area_ha).sum()
        CARBON_FIXATION_KG = CARBON_CROP_KG  + HUMUS_C_BUILDUP_CROP_KG

        report("HUMUS_C_BUILDUP_KG", HUMUS_C_BUILDUP_KG)
        report("HUMUS_C_BUILDUP_CROP_KG", HUMUS_C_BUILDUP_CROP_KG)
        report("CARBON_CROP_KG", CARBON_CROP_KG)
        report("CARBON_FIXATION_KG", CARBON_FIXATION_KG)

        Emis.loc["carbon fixation", "CO2-C"] = -1 * CARBON_FIXATION_KG

        # 3. Mineral fertilization
        MINERAL_N_FERTILIZER_DEMAND_TON = self.mineral_N_fertilizer_demand_ton()
        report(f"running ArableLand with id {self.id} N fertilizer calculation")
        report("mineral N fertilizer demand in ton: ", MINERAL_N_FERTILIZER_DEMAND_TON)

        if MINERAL_N_FERTILIZER_DEMAND_TON > 0:
            # From IPCC 2019 Table 11.1 (N2O emissions from managed soild for other N inputs in wet climates)
            EF_N2O = IPCC.direct_N2O_EF_managed_soils().loc[("EF_1", "synthetic fertiliser inputs in wet climates"), "value"].item()
            Emis.loc['field emissions mineral N fertilization', 'N2O-N'] = MINERAL_N_FERTILIZER_DEMAND_TON * 1000 * EF_N2O

            EF_NH3_NOx =  IPCC.indirect_N2O_EF_soils().loc[("Frac_GASF", "urea"), "value"] # kg NH3-N+NOx-N N volatilization / kg N applied
            Emis.loc['field emissions mineral N fertilization', 'NH3-N & NOx-N'] = MINERAL_N_FERTILIZER_DEMAND_TON * 1000 * EF_NH3_NOx

            # NOTE: Field emissions calculated directly and not via MineralFertilization, because the additional demand for fertilizer is calculated via additional_inputs
            # Emis = pd.concat([Emis, remove_system(MineralFertilization(I=prod("N fertilizer", MINERAL_N_FERTILIZER_DEMAND_TON, "ton")).emissions())])

        return Emis



    ## output function should provide the grain and the straw even if the function is only called for grain

    # try to achieve caching, see: https://docs.python.org/3/library/functools.html#functools.cached_property
    # @cached_property

    def _products(self):
        """
        Calculates products and side-products (e.g. straw) and returns a multiindex dataframe.
        """

        # print(f"\nexecute ._products of ArableLand with id {self.id}.")

        O = self.O.copy()

        idx = self._idx
        item_element_indexer = self._item_element_indexer

        # contents as described in "Basisdaten" (which is to calculate nutrient balances)
        nutient_crops_indexer = [(row["LfL BY Hauptfrucht"], row["LfL BY Ernteprodukt"]) for index, row in idx.iterrows()]
        nutrient_crops = LfL_BY.nutrient_crops(to_element=True).loc[nutient_crops_indexer,:]
        nutrient_crops.index = idx.index #self.index

        # contents as described in "Gruber Tabellen" (which contains crops for animal feeding)
        nutrient_fodder_crops = LfL_BY.nutrient_fodder_crops(per_FM=True).loc[idx["LfL BY Gruber Bezeichnung"],:]
        nutrient_fodder_crops.index = idx.index #self.index

        # NOTE:
        # for "winter wheat, grain" there are still quite some differences between the two sources for instance the N content,
        # because Gruber tables contain crops for fodder (lower quality)
        # probably better use protein content to calculate N content
        # or differentiate between fodder grade and A/B quality

        # as a general rule: use the information from the fodder crops table if available (since it is more detailed)
        # only the information not in nutrient_fodder_crops
        # this is basically leaves on N content, rel_Q where available, and average yield
        nutrient_crops = nutrient_crops.loc[:,nutrient_crops.columns.difference(nutrient_fodder_crops.columns)]
        nutrient_crops.dropna(axis=1, inplace=True, how="all")

        Output = pd.concat([nutrient_fodder_crops, nutrient_crops],axis=1)

        ## if some KTBL about biogas yields exists, add them
        if not all(idx["KTBL biogas yield"].isna()):
            KTBL_indexer = idx["KTBL biogas yield"].dropna(axis="index")
            KTBL_info = KTBL.biogas_biogas_yields().loc[KTBL_indexer,:]
            KTBL_info.index = KTBL_indexer.index
            Output.loc[:, "B0"] = KTBL_info.loc[:,"CH4 yield"]
            Output.loc[:, "CH4 content"] = KTBL_info.loc[:,"CH4 content"]
            Output.loc[:, "oDM"] = KTBL_info.loc[:,"oDM"]
            Output.loc[:, "B0"] = KTBL_info.loc[:,"CH4 yield"]


        ## Some manually added data
        # TODO: Look for good dataset(s)

        ## oDM content for rape seeds
        if ('rapeseed', 'grain') in Output.index:
            Output.loc[('rapeseed', 'grain'),"oDM"] = 0.955  # From LfL Bayern Biogasausbeuten-Datenbank https://www.lfl.bayern.de/iba/energie/049711/?sel_list=21%2Cb&anker0=substratanker#substratanker
        
        ## org C content
        if "oDM" in Output.columns:
            Output["orgC"] = self.orgC_of_DM / Output["oDM"] 


        ## calculate the quantities:
        # the element that is specified in O (e.g. winter wheat, grain) is returned in this quantity
        # all side-products are returned in relative quantities

        # make Output and O accessible with same indexer -> change to multiindex
        O.index = item_element_indexer

        # for the elements where the quantity is defined, copy this value to the Output
        Output.loc[item_element_indexer,"Q"] = O.loc[item_element_indexer,"Q"]

        # if there is any output for which the quantity is not defined, but rel_Q is
        if any(Output["Q"].isna()):
            for undefined_element in Output[Output["Q"].isna() & Output["rel_Q"].notna()].index:
                # print("Output rel_Q: ", Output["rel_Q"]) # NOTE: There is some bug for oat, straw and side_products=False
                item = undefined_element[0]
                defined_element = (undefined_element[0],Output.loc[item].index[0])

                Output.loc[undefined_element,"Q"] = Output.loc[defined_element,"Q"] * Output.loc[undefined_element,"rel_Q"]/Output.loc[defined_element,"rel_Q"]
                Output.drop(columns=["rel_Q"], inplace=True)

        if all(O["U"] == "ton"):
            Output["U"] = "ton"
        else:
            warnings.warn("Currently only 'ton' supported as unit")

        return Output

    def output(self):
        Output = self._products()

        if not self.side_products:
            Output = Output.loc[self._item_element_indexer,:]

        ## change the multiindex back to the original (comma seperated)
        Output.index = pd.Index([i[0] + ", " + i[1] for i in Output.index], name="item")

        return Product(Output)

    ### for testing purposes initiate a costs
    # should these be relative costs (as there are relative quantities for the contents), or should these be absolute? 
    def cashflow(self):
        """
        |  **NOTE**:
        |  - New methodology added. Currently available for the section of "operation"
        |  - Check whether these are actually cashflows (independent of production) or factor costs or prices

        """
        AddInput = self.additional_input()
        # NOTE: Should be replaced with read function
        COST_UREA=InputPrice().urea()["Q"].item() #EUR/tonne

        Cashflow = pd.DataFrame(index=[], columns=["EUR"])
        #cost labour & management
        # source: https://ahdb.org.uk/knowledge-library/costs-growing-maize-silage
        COST_MANAGEMENT=170 #EUR/ha
        Cashflow.loc["labour and management", "EUR"] = COST_MANAGEMENT * self.area_ha

        #cost rent
        if self.rent_land==True:
            COST_RENT=LandPrice().rent()
            Cashflow.loc["rent cost", "EUR"] = COST_RENT*self.area_ha
        else: 
            Cashflow.loc["rent cost", "EUR"] = 0

        #cost fertilizer supply
        Cashflow.loc["fertilizer supply", "EUR"] = AddInput.loc["urea ammonium nitrate","Q"] * COST_UREA

        return Cashflow