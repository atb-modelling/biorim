"""
Poultry production systems
"""

__all__ = ["BroilerHatchery", "BroilerFarm"]


import pandas as pd
import numpy as np
import warnings

from parameters import ConversionFactor
from tools import convert_molmasses, convert_unit, prod
from read import read_mycsv, datapath, Cobb, DLG, LfL_BY, LWK_NRW, LWK_NS, IPCC, KTBL, Nicholson1996, Tiquia_Tam, VDLUFA
from declarations import Product, Emissions, Process


# NOTE:
# the first step in broiler production: broiler_breeder_farm
# the second step in broiler production: broiler_hatchery
# third and final step is the BroilerFarm


# class broiler_breeder_farm:
#     def  __init__(self,n=1):
#         self.n = n


class BroilerHatchery(Process):
    
    """     
    | This is where eggs are breeded to produce one day old broiler which are later on raised in the BroilerFarm
    
    :type O: string  
    :param O: only "broiler chicklet" available.
    :type O: Q  
    :param O: amount of broiler chicklets in terms of number of units ``U = "n"``
    :type O: U 
    :param O: only available unit is "n"


    |  **Notes**  
    |  This is for testing purposes only.
    |  Currently no emissions and output methods are included, therefore no further information is provided for the methods
 
    |
    """        

    def __init__(self, O=prod('broiler chicklet', 1, "n")):
        if O.index != 'broiler chicklet':
            warnings.warn("broiler_hatchery has only 'broiler chicklet' as output")
        if O["U"] == "n":
            self.n = O["Q"]
        elif O["U"] != "n":
            warnings.warn("broiler_hatchery takes only 'n' as Unit (U)")

    # emissions defined just for testing purposes at the moment
    def emissions(self):
        Emis = pd.DataFrame({'CO2': np.nan}, index=["total"])
        Emis = convert_molmasses(Emis, to="element")
        return Emis 

    # this is defined as a only 1 day year old broiler from BroilerFarm:
    def output(self):
        Chicklet = BroilerFarm().animal(n=self.n, age_d=0)
        Chicklet.rename({'broiler': 'broiler chicklet'}, axis='index', inplace=True)
        return Chicklet


class BroilerFarm(Process):
        
    """     
    | *The* ``BroilerFarm`` *class requires information about the quantity of broilers to be produced*
    | *The* ``emissions`` *as well as the* ``output`` *method are based on the available literature information - currently no emissions defined!*
    | *Potential indirect LCA emissions are currently not defined but should be calculated based upon ecoinvent database entries*
    
    :type O: string  
    :param O: "broiler" and "broiler manure" available.
    :type Q: numeric
    :param Q: amount of product to be produced.
    :type U: string
    :param U: unit of Q - "n" for ``Q = "broiler"``, "kg" and "ton" for ``Q="broiler manure"``
    :type fattening_system: string 
    :param fattening_system: "long", "middle", "short"
    :type bedding_type: string 
    :param bedding_type: "straw", "corncob"
    :type loss_rate: numeric 
    :param loss_rate: loss rate of chicklets/broilers

    **Note**  
    
    - ``BroilerFarm`` includes more methods compared to other classes. Besides small utility functions these are ``manure``, ``carcass``, ``animal``, ``chicklet``, ``fodder``
    - besides it also includes the standard methods ``emissions``, ``input`` and ``output``
    - ``input`` and ``emissions`` are currently not specified
    - ``output`` sums up the results from ``carcass``, ``manure``

    **Future Development**
    TODO:

    different management systems
    - include the influence of a run (Auslauf) and its influence on the amount of litter, and the amount of nutrient leaching
    - conventional vs. organic farming (differences in m² needs)

    different fodder
    - define different feed baskets defined by the animal needs

    emissions
    - GHG emissions during production should be included
    - respiration (CO2) emissions from animals
    - liquid emissions (nitrate leaching), especially in combination with production systems that include outdoor production (run)

    different bedding materials
    - should be possible to have straw, wood chips, etc.

    litter composition
    - consider the influence of bedding material and diets on litter

    |
    """        

    def __init__(self, O = prod('broiler manure', 1, "ton"), fattening_system="middle", bedding_type="straw", loss_rate=0.025, B0_source="KTBL", id = None):
        self.O = O
        self.fattening_system = fattening_system
        self.bedding_type = bedding_type
        self.loss_rate = loss_rate
        self.B0_source = B0_source
        self.systems = DLG.broiler_systems()
        self.growth = Cobb.Cobb500()
        self.BroilerContent = LWK_NS.broiler_content().loc["broiler"]
        self.id = id

        # if broiler manure is requested then calculate the required number from there (currently neglects the manure from losses)
        # better way would be to find a standardized way to calculate this from running the function once and evaluating the output
        if any(self.O.index == 'broiler'):
            if (self.O.loc['broiler',"U"] == "n"):
                self.n = self.O.loc["broiler","Q"]
        elif any(self.O.index == 'broiler manure'):
            if (self.O.loc["broiler manure","U"] == "ton"):
                self.n = self.O.loc["broiler manure","Q"]*1e6/self.systems.loc["litter",self.fattening_system]
            else:
                warnings.warn("Unit of broiler manure has to be 'ton'")

    # the area of farm building that is needed to remain below the maxdensity threshold
    def farm_area(self):
        FarmArea = self.systems.loc["maxweight",self.fattening_system]/1000/self.systems.loc["maxdensity",self.fattening_system]*self.n
        return FarmArea

    def bedding(self):
        ## amount of bedding [g/animal]
        # Bedding material will make a difference when litter is processed in biogas: Woody less digestable.
        # normal: g/m² of straw (chips, flour, pellets), maize_silage, or wood chips
        # corncob: requires higher amounts (is more expensive)
        Bedding_m2 = read_mycsv(datapath("LWK_NS\\broiler_bedding.csv"), index_col=0)
        if self.bedding_type == "corncob":
            BEDDING = Bedding_m2.loc["amount","corncob"]
        else:
            BEDDING = Bedding_m2.loc["amount","normal"]
        return pd.DataFrame({'mass': self.farm_area()*BEDDING/1e6,
                             'Q': self.farm_area()*BEDDING/1e6,
                             'U': "ton"},
                             index=[self.bedding_type])

    # the amount of manure that is leaving the farm
    # calculated as the number of chicken and depending on the fattening system
    def manure(self):
        
        # density according to Tiquia Tam
        density = Tiquia_Tam.all_E_manure_managements().loc["bulk density","0"].mean()*1e-3

        # Litter contents according to LWK NRW:
        LitterContent = LWK_NRW.manure_nutrient().loc["broiler litter, 60% DM"]

        if self.B0_source == "KTBL":
            # KTBL is the preferred source here (!) since it also contains the same information about all other feedstock
            B0 = KTBL.biogas_biogas_yields().loc["Geflügelmist","CH4 yield"]
        elif self.B0_source == "IPCC":
            # maximum CH4 yield (Bo) value from IPCC 2019 table 10.16
            B0 = IPCC.Bo_manure_managements().loc["Chicken-Broilers", "Western Europe"]
        else:
            raise ValueError(f"biogas_yield {self.B0_source} type does not exist")

        # CH4 content in biogas
        CH4content = KTBL.biogas_biogas_yields().loc["Geflügelmist","CH4 content"]
        
        # volatile solids from Nicholson et al. 1996 (share of DM)
        # a range is also provided, use this at a later point
        oDM = round(Nicholson1996.Table4().loc["Broiler", "loss on ignition (% dw).mean"]/100, 3)
        orgC = round(Nicholson1996.Table4().loc["Broiler", "organic carbon (% dw).mean"]/ Nicholson1996.Table4().loc["Broiler", "loss on ignition (% dw).mean"], 3)
        
        # humus reproduction value from VDLUFA
        HEQ = VDLUFA.humus_reproduction().loc["manure fresh", "HEQ"]

        # N fertilization efficacy from LfL Bayern
        N_fert_efficacy = LfL_BY.nutrition_contents_efficacy().loc["broiler manure", "min. efficacy (total N)"]

        Manure = pd.DataFrame({'mass': self.n*self.systems.loc["litter",self.fattening_system]/1e6,
                               'Q': self.n*self.systems.loc["litter",self.fattening_system]/1e6,
                               'U': "ton",
                               'DM': LitterContent["DM"],
                               'oDM': oDM,
                               'orgC': orgC,
                               'N': LitterContent["N"],
                               'NH4-N': LitterContent["NH4-N"],
                               'P': LitterContent["P2O5"]*ConversionFactor.loc[("P2O5","P2O5-P"), "factor"],
                               'K': LitterContent["K2O"]*ConversionFactor.loc[("K2O","K2O-K"), "factor"],
                               'CH4 content': CH4content,
                               'B0': B0,
                               'HEQ': HEQ,
                               'N_fert_eff': N_fert_efficacy,
                               'density': density},
                               index=pd.Index(["broiler manure"], name="item"))
        return Manure

    def age(self):
        return int(self.systems.loc["maxdays",self.fattening_system])

    # define the animals that leave the farm
    # also used in defining the chicklets that enter the farm (broiler_hatchery)
    def animal(self, n = None, age_d = None):
        age_d = self.age() if age_d is None else age_d
        n = self.n if n is None else n
        FM = n * self.growth.loc[age_d,"Weight (g)"]/1e6
        Animals = pd.DataFrame({'mass': FM,
                                'Q': n,
                                'U': "n",
                                'DM': self.BroilerContent["DM"],
                                'N': self.BroilerContent["N"],
                                'P': self.BroilerContent["P"],
                                'K': self.BroilerContent["K"]},
                                index=pd.Index(['broiler'], name="item"))
        return Animals

    def carcass(self):
        ## assume that the same share of animals dies every day:
        Carcass = pd.DataFrame({'mass': sum(self.growth.loc[range(1,self.age()),"Weight (g)"])/len(range(1,self.age()))/1e6*self.n*self.loss_rate,
                                'Q': self.n * self.loss_rate,
                                'U': "n",
                                'DM': self.BroilerContent["DM"],
                                'N': self.BroilerContent["N"],
                                'P': self.BroilerContent["P"],
                                'K': self.BroilerContent["K"]},
                                 index=pd.Index(['broiler carcass'], name="item"))
        return Carcass

    ## assume the same content as the grown up chicken for the chicklet. Current solution: Only outputs are defined by the function.
    def chicklet(self):
        Chicklet = pd.DataFrame({'mass': self.growth.loc[0,"Weight (g)"]/1e6*self.n*self.loss_rate,
                                 'Q': self.n * (1 + self.loss_rate),
                                 'U': "n"},
                                  index=['broiler chicklet'])

        Chicklet.index.name ="Item"
        Chicklet["Element"] = [np.nan]*len(Chicklet)
        Chicklet = Chicklet.set_index("Element",append=True)
#        return pd.Series({'broiler chicklet': self.n * (1 + self.loss_rate)}) # requires as many broiler as produced + the losses
        return Chicklet

    def fodder(self):
        Feed = read_mycsv(datapath("Kebreab_et_al_2016\\broiler_feed.csv"), encoding="latin-1", index_col=0)
        Fodder_tot_kg = self.n*self.growth.loc[self.age(), "Cumulative Feed Consumption (g)"]/1000
        Fodder = Fodder_tot_kg*Feed.iloc[:6,:]["Diet2"].to_frame("Q")/1000 # For the moment only assume one fixed diet. This needs to be improved
        Fodder["U"] = "kg"
        Fodder["Element"] = ["grain"]*len(Fodder)
        Fodder = Fodder.set_index("Element",append=True)
        Fodder = convert_unit(Fodder,to="ton")
        return Fodder

    # NOTE: missing are inputs of:
    # - energy (heating and electricity)
    # - fodder
    def input(self):
        return None

    def output(self):
        return Product(pd.concat([self.animal(),self.carcass(),self.manure()], sort=True)).sort_index(axis=1)

    def emissions(self):
        Emis = Emissions(index=["Manure"], columns=["CO2-C"])
        # NOTE: For the moment, as the prechains are not considered, assume sequestration of carbon in the amount contained in the manure
        Manure = self.manure()
        Emis.loc["Manure","CO2-C"] = -1 * (Manure["Q"] * Manure["DM"] * Manure["oDM"] * Manure["orgC"]).sum() * 1000

        return Emis