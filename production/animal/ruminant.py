# production of ruminants and ruminant products (milk, ect.)
# create simplified functions first: this is first only to create the manure emissions from production

import pandas as pd
import warnings
from functools import cached_property
from tools import prod, get_content, report, convert_molmasses
from read import LfL_BY, LWK_NRW, LAZBW, KTBL, VDLUFA, LfL_BY, EEA
from declarations import Product, Emissions, Process
from parameters import ConversionFactor

### first define the manure very basic so use is as a co-substate in biogas?
# do not differentiate between different production systems. Does this make sense? Do I need the overall production?


## Key parameters of the calves farm (Kälberaufzucht)
# Parameterized mainly according to "KTBL Faustzahlen aus der Landwirtschaft 2018" and Haenel et al. 2018
# KTBL p. 947: start weight 38-50 kg, end weight 80-250 kg | Haenel: p.144: start 41kg, end 125 kg. Here assumed 40 kg
# Haenel: livespan 126 days
# KTBL p. 312 required are 0.5-3 kg of straw per place per day as bedding. Here assumed 2 kg

# Notes:
# In the case of calfrearing the finished "product" is one calf. Therefore all inputs and emissions have to be calculated for the timespan of production (150 days)

# Kälbersterblichkeit von 12,6 bis 14,4% in Bayern (LKV Journal 1/2017)


class CalfRearing(Process):
    """
    | Represents the raising of cavles for milk and meat production until an age of about 150 days
    
    :type O: Product
    :param O: requested output
    :type Q: numeric
    :param Q: amount of product to be produced.
    :type livespan: numeric
    :param livespan: the assumed livespan in days. Currently only influences the amount of straw needed
    :type milk_feed: string
    :param milk_feed: the duration and type of feeding with milk: "milk, 8 weeks" , "milk, 10 weeks", "milk substitute, 8 weeks" 
    :type concentrate_mix: string
    :param concentrate_mix: one of 7 possible mixtures defined by LfL Bayern: e.g. "1"
    :type concentrate_mix: numeric
    :param concentrate_mix: maximal amount of daily concentrate feed in kg

    |
    |  **TODO**
    |  - Include losses: About 7.5 % during the first 450 days: https://www.lkv-sh.de/home/archiv/281-wie-hoch-sind-die-kaelberverluste-wirklich-rind-im-bild-03-2016

    """
    AVERAGE_WEIGHT_GAIN_GR_DAY = 850 # options: 700, 850, 1000

    def __init__(self, O = prod("calf", 1, "n"), livespan_days=150, milk_feed="milk, 10 weeks", concentrate_mix="1", concentrate_kg=2, id = None):
        self.O = O
        
        if O.loc[('.', 'calf'),"U"] == "n":
            self.n = O.loc[('.', 'calf'),"Q"]
        
        self.livespan_days = livespan_days
        self.milk_feed = milk_feed # number of weeks the calves are feed with milk (8 or 10)
        self.concentrate_mix = concentrate_mix
        self.concentrate_kg = concentrate_kg
        self.id = id

    def concentrate(self):
        return LfL_BY.feed_example_cavles().loc[:,[self.concentrate_mix]]

    def daily_fodder_demand_per_animal(self):
        """
        The general approach to calculate the fodder demand is to assume a fixed provision of milk.
        The remaining demand for protein and energy is then provided by concentrate feed, but this is limited to a certain amount as set with concentrate_kg.
        The still remaining amount is provided by hay.

        *NOTE:*
        *  contents of the fodder here defined independently of the actual input. Would have to call the inputs already here.
        """

        ## 1. daily energy and protein demand ##
        ME_XP_Demand = LfL_BY.energy_protein_demand_calves().loc[self.AVERAGE_WEIGHT_GAIN_GR_DAY,:]
        # ME_XP_Demand = LfL_BY.energy_protein_demand_calves().loc[850,:]

        ME_XP_Demand.drop(columns=["weight (kg)", "current gain (g)"], inplace=True)
        ME_XP_Demand.rename(columns={'age (week)': "week", 'ME (MJ)': "ME dem (MJ)", 'XP (g)': "XP dem (g)"}, inplace=True)
        ME_XP_Demand.set_index("week", inplace=True)

        ## 2. the supply from milk ##
        Milk_Provision = LAZBW.calf_milk_consumption().loc[:,["milk, 10 weeks"]]
        Milk_Provision.rename(columns={'milk, 10 weeks':"milk (l)"}, inplace=True)
        ME_XP_Demand = ME_XP_Demand.join(Milk_Provision, how="left")
        Nutrient_Content_Milk = LfL_BY.nutrient_milk_products(per_FM=True).loc["cow milk, fresh",["XP", "ME"]]
        ## NOTE: The milk contents independent from input.

        # from the litres per day calculate daily supply of ME and XP (assume density of milk = 1 kg/l)
        # Milk_ME_XP_supply = pd.DataFrame(Milk_Provision.values*Nutrient_Content_Milk.values, columns=Nutrient_Content_Milk.columns, index=Milk_Provision.index)

        MILK_DENSITY = 1

        ME_XP_Demand["ME milk sup"] = ME_XP_Demand["milk (l)"] * MILK_DENSITY * Nutrient_Content_Milk["ME"]
        ME_XP_Demand["XP milk sup"] = ME_XP_Demand["milk (l)"] * MILK_DENSITY * Nutrient_Content_Milk["XP"]
        ME_XP_Demand.fillna(0, inplace=True)

        ## 3. remaining demand to be fulfilled by concentrate and hay
        ME_XP_Demand["ME conc + hay dem"] = ME_XP_Demand["ME dem (MJ)"] - ME_XP_Demand["ME milk sup"]
        ME_XP_Demand["XP conc + hay dem"] = ME_XP_Demand["XP dem (g)"] - ME_XP_Demand["XP milk sup"]
        ME_XP_Demand.clip(lower=0, inplace=True)
        # correct overestimated demand in week 1
        ME_XP_Demand.loc[1,"XP conc + hay dem"] = 0 
        ME_XP_Demand.loc[1,"ME conc + hay dem"] = 0

        ## 3. the concentrate fodder demand ##
        Concentrate_Content = self.concentrate().loc[["XP (g/kg)", "ME (MJ/kg)"], "1"]

        # Concentrate = LfL_BY.feed_example_cavles().loc[:,["1"]]
        # Concentrate_Content = Concentrate.loc[["XP (g/kg)", "ME (MJ/kg)"],"1"]
        # NOTE: concentrate contents independent from input

        # concentrate demand according to ME calculation
        ME_XP_Demand["conc dem ME (kg)"] = ME_XP_Demand["ME conc + hay dem"]/Concentrate_Content["ME (MJ/kg)"]
        ME_XP_Demand["conc dem XP (kg)"] = ME_XP_Demand["XP conc + hay dem"]/Concentrate_Content["XP (g/kg)"]
        # take the bigger of the two, but limit to concentrate_kg value (e.g. 2 kg)
        ME_XP_Demand["conc (kg)"] = ME_XP_Demand[["conc dem ME (kg)", "conc dem XP (kg)"]].max(axis=1).clip(upper=self.concentrate_kg)
        ME_XP_Demand["ME conc sup"] = ME_XP_Demand["conc (kg)"] * Concentrate_Content["ME (MJ/kg)"]
        ME_XP_Demand["XP conc sup"] = ME_XP_Demand["conc (kg)"] * Concentrate_Content["XP (g/kg)"]

        ## 4. the hay demand
        ME_XP_Demand["ME hay dem"] = ME_XP_Demand["ME conc + hay dem"]-ME_XP_Demand["ME conc sup"]
        ME_XP_Demand["XP hay dem"] = ME_XP_Demand["XP conc + hay dem"]-ME_XP_Demand["XP conc sup"]
        ME_XP_Demand.clip(lower=0, inplace=True)

        # take one hay as sample (might be changed)
        Hay_Content = LfL_BY.nutrient_fodder_crops().loc["Wiesenheu 2.u. folg. Schnitte, Mitte der Blüte",["XP", "ME"]]
        # NOTE: Independent of input

        # hay demand according to ME calculation
        ME_XP_Demand["hay dem ME (kg)"] = ME_XP_Demand["ME hay dem"]/Hay_Content["ME"]
        ME_XP_Demand["hay dem XP (kg)"] = ME_XP_Demand["XP hay dem"]/Hay_Content["XP"]
        # take the bigger of the two
        ME_XP_Demand["hay (kg)"] = ME_XP_Demand[["hay dem ME (kg)", "hay dem XP (kg)"]].max(axis=1)

        return ME_XP_Demand.loc[:,["milk (l)", "hay (kg)", "conc (kg)"]]
        

    def fodder_demand_total(self):
        Fodder_Demand_Animal = (self.daily_fodder_demand_per_animal() * 7).sum(0)
        # Fodder_Demand_Animal = (CalfRearing().daily_fodder_demand_per_animal() * 7).sum(0)

        Milk = prod(("production.animal.DairyFarm","cow milk"), Fodder_Demand_Animal["milk (l)"] / 1000 * self.n, "m3")
        # Milk = prod(("production.animal.DairyFarm","cow milk"), Fodder_Demand_Animal["milk (l)"] / 1000 * n, "m3")


        # concentrate
        # TODO: This needs to be changed, since these products are currently not available
        # Potentially define the default locations of these product?
        Concentrate = self.concentrate().iloc[0:14,:]/100 * Fodder_Demand_Animal["conc (kg)"].sum() / 1000 * self.n
        Concentrate.columns = ["Q"]
        Concentrate.dropna(inplace=True)
        Concentrate["U"] = "ton"
        Concentrate.index.name = None

        Hay = prod(("production.grassland.Haymaking","hay"), Fodder_Demand_Animal["hay (kg)"] / 1000 * self.n, "ton")

        return pd.concat([Milk, Concentrate, Hay])

    def bedding_demand_total(self):
        Bedding = Product({
            'Q': 2 /1000 * self.livespan_days * self.n, # assumption 2 kg per animal per day
            'U': "ton",
        }, index=["wheat, straw"])
        return Bedding

    def manure(self):
        #TODO: define manure production depending on input. Orient at calculations from Thünen emission reporting.
        return None

    def additional_input(self):
        # necessary are straw (as bedding material), the fodder and the housing

#        return bedding
        return pd.concat([self.bedding_demand_total(), self.fodder_demand_total()], axis=0, sort=False)


    def output(self):
        # the calves
        NutriContent = LfL_BY.nutrient_content_animals(to_element=True).loc["cattle, for milk",["N", "P", "K"]] # assuming the same contents as grown up cows
        # NOTE: like this the nutrient content is not relative to Q
        # Better specify U as "ton", and add n in addtion
        Calves = Product({
            'n': self.n,
            'N': NutriContent["N"],
            'P': NutriContent["P"],
            'K': NutriContent["K"],
            'Q': self.n * 0.125, 
            'U': "ton"
        }, index=["calf"])

        return Calves


class HeiferRearing(Process):
    """
    |  Very simplified representation of heifers rearing.
    |  **NOTE**:
    |  - Currently works only to supply input to DairyFarm
    |  - Does not include any inputs, emissions, etc.
    """
    def __init__(self,
                 O = prod("heifer", 1, "n"),
                 id = None):
        
        if not all(O.index == "heifer"):
            raise ValueError("HeifersRearing can only supply 'heifer'")
        self.O = O

    def output(self):

        NutriContent = LfL_BY.nutrient_content_animals(to_element=True).loc["cattle, for milk",["N", "P", "K"]] # assuming the same contents as grown up cows

        # Data calculated from average values from Tabelle A-2 from Schneider (2019) see References/Diss_Mariana_Schneider_2019.pdf
        # was based on measurements on Fleckvieh cow carcasses
        DM = 0.474
        oDM = 0.913
        
        # Carbon content of 62.4 % in dry matter taken from References/Fedorowicz 2007 Biomass Gasification as a Means of Carcass and Specified Risk Materials Disposal
        orgC = 0.624 / oDM

        WEIGHT_KG = 385 # 385 kg is average weight at first insemination for cows with 750 kg end weight according to https://www.elite-magazin.de/herdenmanagement/sind-meine-farsen-passend-entwickelt-19390.html

        if self.O["U"].item() == "n":
            n = self.O["Q"]
        else:
            raise ValueError("Currently only 'n'  supported as unit for O")

        Heifers = prod("heifer",
                       n = n,
                       DM = DM,
                       oDM = oDM,
                       orgC = orgC,
                       N = NutriContent["N"],
                       P = NutriContent["P"],
                       K = NutriContent["K"],
                       Q = WEIGHT_KG / 1000 * n,
                       U = "ton")

        return Heifers


class DairyFarm(Process):
    """     
    |  Represents cow milk production, with feed consumption, greenhouse gas emissions, and manure production.
    |  
    Fodder demand is calculated from LfL (2017) Gruber Tabelle zur Fütterung der Milchkühe Zuchtrinder Schafe Ziegen,
    emissions are calculated according to the German emission reporting routine 

    :type I: Product
    :param I: The amount of fodder input supplied from the farm
    :type n: numeric
    :param n: The number of cows (on average over the year)

    |  **NOTE:**
    |  -Output quantities (e.g. manure) not based on inputs yet

    |
    """        
    REPLACEMENT_RATE = 0.25 # German: Remontierungsrate
    LIVING_MASS_KG = 700
    AVG_MILK_YEAR_KG = 9200 # Source: Holstein (Schwarzbunte Farbrichtung) https://www.praxis-agrar.de/tier/rinder/rinderrassen-vorgestellt/milchrassen/?L=0
    LACTATION_PERIOD_days = 305
    MILK_PER_DAY_kg = AVG_MILK_YEAR_KG/LACTATION_PERIOD_days
    FAT_CONTENT_MILK = 4.0 # %
    PROTEIN_CONTENT_MILK = 3.4 # g / 100 g
    PROTEIN_N_FAKTOR_MILK = 6.38 # g protein per g N
    LACTOSE_CONTENT_MILK = 4.8 # %

    def __init__(self,
                 I = None,
                 O = None,
                 n = 20,
                 enteric_ferment_method = "IPCC2019_Tier1",
                 co2_respiration_method = "difference",
                 id = None):

        if (I is not None) and  (O is not None):
            raise ValueError("Currently O and I cannot be defined at the same time")
        
        self.O = O

        if I is not None:
            if not all(I["U"] == "ton"):
                print("input: \n", input)
                raise ValueError("Input must be in 'ton'")

        # self.I = get_content(I, required=["NEL","XP","nXP","RNB"])
        # print("I to DairyFarm \n", I)
        if I is not None:
            self.I = I
        else:
            warnings.warn("using default input hay 15 ton as input to Dairy Farm")
            self.I = get_content(prod('hay', 15, "ton"), required=["NEL","XP","nXP","RNB"])

        self.enteric_ferment_method = enteric_ferment_method
        self.co2_respiration_method = co2_respiration_method
        self.id = id
        self.n = n # average number of dairy cows


    def nXP_NEL_demand_per_animal_year(self):
        """
        |  nXP [g] and NEL [MJ] demand per animal considering basal maintaninance, lactation, and 
        """

        # DM_fodder_IT_kg_day = 20

        ### Energy demand ###

        ## Daily energy demand [MJ NEL / day]
        # Basal
        BASAL_MAINTAINANCE_NEL_day = 0.293 * (self.LIVING_MASS_KG ** 0.75) # Source: Barth K, Brade W, Doluschitz R, et al (2005) Rinderzucht und Milcherzeugung: Empfehlungen für die Praxis, 2. Auflage. Bundesforschungsanstalt für Landwirtschaft (FAL)
        
        LACTATION_NEL_KG_MILK = 0.38 * self.FAT_CONTENT_MILK + 0.21 * self.PROTEIN_CONTENT_MILK + 0.95 # Source: Table 11. Weltin J, Jilg T (2013) Versuchsbericht Nr. 3 - 2013. Verringerung der N-Ausscheidungen durch proteinangepasste Fütterung bei der Milchkuh. Landwirtschaftliches Zentrum für Rinderhaltung, Grünlandwirtschaft, Milchwirtschaft, Wild und Fischerei Baden-Württemberg
        # LACTATION_NEL_DAY = LACTATION_NEL_KG_MILK * self.MILK_PER_DAY_kg

        ADD_NEL_6to4wk_CALVING_day = 13 # Additional daily requirements 6th to 4th week before calving [MJ NEL/day]. In this time frame no lactation.
        ADD_NEL_3to1wk_CALVING_day = 18 # Additional daily requirements 6th to 4th week before calving [MJ NEL/day]

        ## Energy per year [MJ NEL/year]
        BASAL_MAINTAINANCE_NEL_year = BASAL_MAINTAINANCE_NEL_day * 365
        # print("BASAL_MAINTAINANCE_NEL_year", BASAL_MAINTAINANCE_NEL_day)
        LACTATION_NEL_year = LACTATION_NEL_KG_MILK * self.AVG_MILK_YEAR_KG
        # print("LACTATION_NEL_year", LACTATION_NEL_year)
        CALVING_NEL_year = ADD_NEL_6to4wk_CALVING_day * 7 * 3 + ADD_NEL_3to1wk_CALVING_day * 7 * 3
        # print("CALVING_NEL_year", CALVING_NEL_year)

        NEL_year = BASAL_MAINTAINANCE_NEL_year + LACTATION_NEL_year + CALVING_NEL_year

        ### Protein demand ###

        ## Daily demand
        # Basal
        BASAL_MAINTAINANCE_nXP_day = 190 + 0.4 * self.LIVING_MASS_KG # Source: Table 55. Barth K, Brade W, Doluschitz R, et al (2005) Rinderzucht und Milcherzeugung: Empfehlungen für die Praxis, 2. Auflage. Bundesforschungsanstalt für Landwirtschaft (FAL)

        # Milk
        PROTEIN_USAGE_FACTOR = 2.1      # Inverse of the usage factor (see page 113 of Barth et al. 2005)
        LACTATION_nXP_day = self.MILK_PER_DAY_kg * (13.6 + self.PROTEIN_CONTENT_MILK * 10 * PROTEIN_USAGE_FACTOR) # equivalent to 85 g nXP/kg milk for 3.4 % protein content

        # Dry period
        TOT_nXP_6to4wk_CALVING_day = 1135 # Total daily requirements 6th to 4th week before calving [nXP/day].
        TOT_nXP_3to1wk_CALVING_day = 1230

        ## Protein demand per year [g nXP/year]
        BASAL_MAINTAINANCE_nXP_year = BASAL_MAINTAINANCE_nXP_day * (365 - 6*7) # For six weeks the dry period requirements are used
        CALVING_xNP_year = TOT_nXP_6to4wk_CALVING_day * 3 * 7 + TOT_nXP_3to1wk_CALVING_day * 3 * 7
        LACTATION_nXP_year = LACTATION_nXP_day * self.LACTATION_PERIOD_days

        nXP_year = BASAL_MAINTAINANCE_nXP_year + CALVING_xNP_year + LACTATION_nXP_year

        Demand_per_Animal = pd.DataFrame({'nXP': nXP_year, 'NEL': NEL_year}, index=["demand per animal"])
        return Demand_per_Animal


    def nXP_NEL_demand_per_year(self):
        """
        Total demand for nXP [g] and NEL [MJ] for all cows
        """
        Demand_per_Year = self.nXP_NEL_demand_per_animal_year() * self.n
        Demand_per_Year.rename({'demand per animal': "demand per year"}, inplace=True)
        return Demand_per_Year

    #     # RNB = (XP - nXP)/6.25

    def heifer_demand(self):
        return prod("heifer", self.n * self.REPLACEMENT_RATE, "n", source="production.animal.ruminant.HeiferRearing")

    # NOTE: This method is called several times. Consider making it a cachedproperty
    @cached_property
    def additional_fodder(self):
        """
        |  Additional fodder required besides the one already supplied as I.
        |  Simplistic approach for the moment: if additional energy is required this is supplied by barley. If then still protein is required this is supplied by peas
        
        |  **TODO**:
        |  Adjust so that nXP demand is fulfilled by rapeseed
        """
        # # For testing
        # Grass = Meadow(O=prod("grass", 500, "ton")).output()
        # Maize = ArableLand(O=prod("silage maize, whole", 400, "ton")).output()
        # Fodder = pd.concat([Grass, Maize])

        Input = self.I.copy()
        # Input = Fodder
        Supply_by_I = Input[["nXP", "NEL"]].mul(Input["Q"] * 1000, axis=0).sum().to_frame(name="supply by I").T

        SupplyDemand = pd.concat([self.nXP_NEL_demand_per_year(), Supply_by_I])
        # SupplyDemand = pd.concat([DairyFarm(I=Fodder, n = 100).nXP_NEL_demand_per_year(), Supply_by_I])

        SupplyDemand.loc["supply gap",:] = SupplyDemand.loc["demand per year",:] - SupplyDemand.loc["supply by I",:]

        AddDemand = Product()

        # report("Fodder supply vs demand \n", SupplyDemand)

        if  SupplyDemand.loc["supply gap","NEL"] < 0:
            dem = SupplyDemand.loc["demand per year","NEL"]
            sup = SupplyDemand.loc["supply by I","NEL"]
            report(f"provided fodder sufficient to fulfil NEL demand. Demand was {dem}, supply {sup}")

        elif SupplyDemand.loc["supply gap","NEL"] > 0:
            print("Additional NEL demand fulfilled by barley")

            ## First missing NEL is filled by barley grains
            Barley = get_content(prod("barley, grain"), required = ["nXP", "NEL"])
            Barley["Q"] = (SupplyDemand.loc["supply gap","NEL"] / (Barley["NEL"].item() * 1000)) # NEL is in g / kg
            Barley["source"] = "production.plant.crop.ArableLand"

            AddDemand = pd.concat([AddDemand, Barley])

            SupplyDemand = pd.concat([SupplyDemand, (Barley[["nXP","NEL"]] * 1000 * Barley["Q"].item()).rename({"barley, grain": "supply by barley"})])

            SupplyDemand.loc["supply gap",:] = SupplyDemand.loc["demand per year",:] - SupplyDemand.loc["supply by I",:] - SupplyDemand.loc["supply by barley",:]


        if SupplyDemand.loc["supply gap","nXP"] < 0:
            dem = SupplyDemand.loc["demand per year","nXP"]
            sup = SupplyDemand.loc["supply by I","nXP"]
            print(f"provided fodder sufficient to fulfil nXP demand. Demand was {dem}, supply {sup}")
        elif SupplyDemand.loc["supply gap","nXP"] > 0:
            print("Additional nXP demand fulfilled by rapeseeds")

            # NOTE: This is rapeseeds directly. Rapeseed cake would be more meaningful
            Rapeseed = get_content(prod("rapeseed, grain"), required = ["nXP", "NEL"])

            Rapeseed["Q"] = (SupplyDemand.loc["supply gap","nXP"] / (Rapeseed["nXP"] * 1000)) # nXP is in g / kg
            Rapeseed["source"] = "production.plant.crop.ArableLand"

            AddDemand = pd.concat([AddDemand, Rapeseed])

            SupplyDemand = pd.concat([SupplyDemand, (Rapeseed[["nXP","NEL"]] * 1000 * Rapeseed["Q"].item()).rename({"rapeseed, grain": "supply by rapeseed"})])

            SupplyDemand.loc["supply gap",:] = SupplyDemand.loc["supply gap",:] - SupplyDemand.loc["supply by rapeseed",:]

        return AddDemand

    def fodder(self):
        """
        Sum of fodder in I and additional_fodder.
        |  **NOTE:**
        |  - All I assumed to be fodder (may be something else, or may lead to oversupply)
        |  - additional_input not considered here (so heifers there are no problem)
        """
        return pd.concat([self.I, self.additional_fodder])
    
    def milk_production(self):
        """
        |  The amount of milk produced by the cows
        """
        MILK_TON = self.AVG_MILK_YEAR_KG / 1000 * self.n
        Milk = prod("cow milk", MILK_TON, "ton")
        Milk["N"] = self.PROTEIN_CONTENT_MILK * 10 / self.PROTEIN_N_FAKTOR_MILK # g protein / 100 g milk * 10 / (6.38 g protein / g N)
        Milk["DM"] = 0.13
        Milk["oDM"] = (0.13 - 0.01) / 0.13 # assuming 1 % ash content in fresh milk
        Milk["GE"] = (0.0384 * self.FAT_CONTENT_MILK * 10) + (0.0223 * self.PROTEIN_CONTENT_MILK * 10) + (0.0199 * self.LACTOSE_CONTENT_MILK * 10) - 0.108   # Equation from Cabezas-Garcia EH, Gordon AW, Mulligan FJ, Ferris CP (2021) Revisiting the relationships between fat-to-protein ratio in milk and energy balance in dairy cows of different parities, and at different stages of lactation. Animals 11:. https://doi.org/10.3390/ani11113256
        Milk["orgC"] = Milk["GE"] * 21 / (Milk["DM"] * Milk["oDM"] * 1000)  # 21 g C / MJ from Felber R (2016) Determination of the carbon budget of a pasture : effect of system Supplementary material S1 Uncertainty estimation of selected C budget components. 0–7. https://doi.org/10.5194/bg-13-2959-2016-supplement
        return Milk

    def cows_to_slaugter(self):
        """
        Cows leaving the herd. Number simply calculated from the replacement rate.
        """

        # Same assumptions on carcass contents as for heifers
        NutriContent = LfL_BY.nutrient_content_animals(to_element=True).loc["cattle, for milk",["N", "P", "K"]] # assuming the same contents as grown up cows

        # Data calculated from average values from Tabelle A-2 from Schneider (2019) see References/Diss_Mariana_Schneider_2019.pdf
        # was based on measurements on Fleckvieh cow carcasses
        DM = 0.474
        oDM = 0.913
        
        # Carbon content of 62.4 % in dry matter taken from References/Fedorowicz 2007 Biomass Gasification as a Means of Carcass and Specified Risk Materials Disposal
        orgC = 0.624 / oDM

        number = self.n * self.REPLACEMENT_RATE

        Cows = prod("dairy cow",
                       n = number,
                       DM = DM,
                       oDM = oDM,
                       orgC = orgC,
                       N = NutriContent["N"],
                       P = NutriContent["P"],
                       K = NutriContent["K"],
                       Q = self.LIVING_MASS_KG / 1000 * number,
                       U = "ton")

        return Cows

    def calves(self):
        """
        Calves produced, that can go to rearing, slaughtering, etc.
        Assuming 1 calf per cow in the herd with a weight of 40 kg
        """

        # Same assumptions on body contents as for cows (see above)
        NutriContent = LfL_BY.nutrient_content_animals(to_element=True).loc["cattle, for milk",["N", "P", "K"]] # assuming the same contents as grown up cows

        Calves = prod("calf, newborn",
                       n = self.n,
                       DM = 0.474,
                       oDM = 0.913,
                       orgC = 0.624 / 0.913,
                       N = NutriContent["N"],
                       P = NutriContent["P"],
                       K = NutriContent["K"],
                       Q = 40 / 1000 * self.n,
                       U = "ton")

        return Calves
    
    @property
    def N_excretion_kg(self):
        """
        The total amount of N excreted by all cows.
        Calculated as the intake in fodder, minus outputs my milk and difference in body (heifers input, cows output)
        """
        Fodder = self.fodder()
        Fodder_N_kg = (Fodder["Q"] * Fodder["N"]).sum().item()

        Input = self.input()
        if Input.loc[["heifer"],"N"].isna().item():
            # If heifers are not already supplied, make some assumptions.
            HEIFER_NUMBER = self.heifer_demand()["Q"].item()
            WEIGHT_KG = 385
            N_CONTENT = LfL_BY.nutrient_content_animals(to_element=True).loc["cattle, for milk","N"] # assuming the same contents as grown up cows
            Heifer_N_kg = HEIFER_NUMBER * WEIGHT_KG / 1000 * N_CONTENT
        else:
            Heifers = Input.loc[["heifer"],:]
            if Heifers["U"] == "ton":
                Heifer_N_kg = Heifers["Q"] * Heifers["N"]
            else:
                warnings.warn("Heifer Q not in correct unit")

        Milk = self.milk_production()
        Milk_N_kg = (Milk["Q"] * Milk["N"]).item()

        Calves = self.calves()
        Calves_N_kg = (Calves["Q"] * Calves["N"]).item()

        SlaughterCows = self.cows_to_slaugter()
        SlaughterCows_N_kg = (SlaughterCows["Q"] * SlaughterCows["N"]).item()

        report("Fodder_N_kg", Fodder_N_kg)
        report("Heifer_N_kg", Heifer_N_kg)
        report("Milk_N_kg", Milk_N_kg)
        report("Calves_N_kg", Calves_N_kg)
        report("SlaughterCows_N_kg", SlaughterCows_N_kg)

        N_excetion_kg = Fodder_N_kg + Heifer_N_kg - Milk_N_kg - Calves_N_kg - SlaughterCows_N_kg

        return N_excetion_kg


    def manure_production(self):
        """
        |  The amount of manure produced.
        |  **TODO:**
        |  - Adapt manure amount with different fodder consumption or at least cow size or milk yield.
        |  - Calculate quantities and nitrogen contents based on fodder.
        """

        ManureQ = KTBL.cow_manure_amounts().loc[("Milchviehhaltung, mittlere und schwere Rassen", "Ackerfutterbaubetrieb ohne Weidegang mit Heu","10000 kg ECM"),:]

        # Manure defined by default values, because output of manure is requested 
        if self.O is not None:
            print("Set manure contents with default values")
            if self.O.index == "cow manure, liquid" and self.O["U"].item() == "ton":
                MANURE_TON = self.O["Q"].item()

                Manure = prod("cow manure, liquid", MANURE_TON, "ton")

                ## NOTE: N output higher than input. This cannot be the case. Test with lower DM content
                Manure["N"] = LWK_NRW.manure_nutrient().loc["cow manure, liquid, 10% DM", "N"]
                Manure["NH4-N"] = LWK_NRW.manure_nutrient().loc["cow manure, liquid, 10% DM", "NH4-N"]
                # Manure["N"] = LWK_NRW.manure_nutrient().loc["cow manure, liquid, 6% DM", "N"]
                # Manure["NH4-N"] = LWK_NRW.manure_nutrient().loc["cow manure, liquid, 6% DM", "NH4-N"]

        # Manure amount and N contents determined by cow number
        else:
            ## Manure amount
            # NOTE: Does not change automatically with definition of cow yet
            MANURE_TON = ManureQ.loc["Menge_t_TPa"] * self.n

            Manure = prod("cow manure, liquid", MANURE_TON, "ton")

            # Take the ratio of N to NH4-N from the source
            Manure_N = LWK_NRW.manure_nutrient().loc["cow manure, liquid, 10% DM", "N"]
            Manure_NH4N = LWK_NRW.manure_nutrient().loc["cow manure, liquid, 10% DM", "NH4-N"]

            NH4_SHARE_N = Manure_NH4N / Manure_N    # about 53%

            N_CONTENT_MANURE = (self.N_excretion_kg - self.housing_NH3_emissions()["NH3-N"].item()) / MANURE_TON
            print(f"Calculated N content is in manure is {N_CONTENT_MANURE}. Should be between 3 and 5")
            Manure["N"] = N_CONTENT_MANURE
            Manure["NH4-N"] = N_CONTENT_MANURE * NH4_SHARE_N




        # basic (nutrient) contents
        Manure["DM"] = ManureQ.loc["TM"]/100
        Manure["oDM"] = KTBL.biogas_biogas_yields().loc["Rindergülle","oDM"]
        # Corg content is taken as the ratio of C content and organic substance from liquid dairy cow manure from conventional farms
        # Table 4.7-3 in References/Paulsen 2013 Zusammensetzung, Lagerung und Ausbringung von Wirtschaftsdüngern.pdf doi: 10.3220/LBF_2013_29-36
        Manure["orgC"] = 511/741

        Manure["P"] = ManureQ.loc["P2O5"] * ConversionFactor.loc[("P2O5", "P2O5-P"), "factor"]
        Manure["K"] = ManureQ.loc["K2O"] * ConversionFactor.loc[("K2O", "K2O-K"), "factor"]

        Manure["density"] = 1 # ton/m³

        # biogas values
        Manure["B0"] = KTBL.biogas_biogas_yields().loc["Rindergülle","CH4 yield"]
        Manure["CH4 content"] = KTBL.biogas_biogas_yields().loc["Rindergülle","CH4 content"]

        # humus formation
        Manure["HEQ"] = VDLUFA.humus_reproduction().loc["cow slurry", "HEQ"]
        Manure["N_fert_eff"] = LfL_BY.nutrition_contents_efficacy().loc["cow slurry", "min. efficacy (total N)"]

        return Manure


    def output(self):
        """
        The outputs of milk, manure, cows going to slaughter and calves
        """
        Output = pd.concat([self.milk_production(), self.manure_production(), self.cows_to_slaugter(), self.calves()])

        return Output


    def housing_NH3_emissions(self):
        """
        NH3-N emissions that occur in housing. Calculated from N excreted, TAN content and an emission factor
        """
        EEA_data = EEA.NH3_N_EF_manure_management().loc[("Dairy cattle", "Slurry")]
        TAN_content = EEA_data["Proportion of TAN"]
        NH3_N_EF = EEA_data["EF_housing"]

        return Emissions({"NH3-N": self.N_excretion_kg * TAN_content * NH3_N_EF}, index = ["housing emissions"])

    def emissions_enteric_fermentation(self):
        """
        |  CH4 emissions caused by the enteric fermentation of the cows
        """

        Emis = Emissions()

        if self.enteric_ferment_method == "IPCC2019_Tier1":
            EF = 126 # kg CH4 / (head * year) Dairy Cattle value for Western Europe. From Table 10.11 1. 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories
            Emis.loc["enteric fermentation", "CH4"] = EF * self.n

        # elif self.enteric_ferment_method == "IPCC2006_Tier2":
        #     Ym = 6.5 / 100 # CH4 CONVERSION FACTORS. From Table 10.12 1. Dong H, Mangino J, McAllister TA, Hatfield JL, Johnson DE, Lassey KR, et al. Emissions From Livestock and Manure Management. In: Eggleston S, Buendia L, Miwa K, Ngara T, Tanabe K, editors. 2006 IPCC Guidelines for National Greenhouse Gas Inventories. 2006. 

        #     ## GE is the gross energy contained in the fodder. Information from LfL only contains GE.

        #     EF = (GE * Ym * 365)/55.65
        #     Emis.loc["enteric fermentation", "CH4"] = EF * self.n

        elif self.enteric_ferment_method == "Kirchgeßner_1994":
            """
            |  Calculation of enteric fermentation based on fodder composition
            |  
            |  Equation taken from Dämmgen U, Rösemann C, Haenel HD, Hutchings NJ. Enteric methane emissions from German dairy cows. Landbauforsch Volkenrode. 2012;62: 21–31. 
            |  Original source not available
            |  
            |  **NOTE:**
            |  - If animals are oversupplied with fodder, this currently would increase emissions (also beyond what is realistic)


            """

            Fodder = self.fodder()
            
            a = 0.079 # kg/kg
            b = 0.010 # kg/kg
            c = 0.026 # kg/kg
            d = -0.212 # kg/kg
            e = 365 * 0.063 # kg animal-1 a-1
            

            M_XFi = (Fodder["Q"] * Fodder["XF"]).sum() / self.n  # intake rate of crude fibre (in kg /(animal * a))
            print("M_XFi:", M_XFi)
            M_XP  = (Fodder["Q"] * Fodder["XP"]).sum() / self.n  # intake rate of crude protein (in kg (animal * a))
            print("M_XP:", M_XP)
            M_XFa = (Fodder["Q"] * Fodder["XL"]).sum() / self.n  # intake rate of ether extract (fat) (in kg /(animal * a)) 
            print("M_XFa:", M_XFa)

            ## N-freie Extraktstoffe = Trockenmasse – (Rohasche + Rohprotein + Rohfett + Rohfaser)
            ## XF Rohfaser; XL Rohfett; XP Rohprotein

            # NOTE: actually should be (oDM missing!):
            #  M_NFE = (Fodder["Q"] * Fodder["DM"] * Fodder ["oDM"] * 1000 - Fodder["Q"] * (Fodder["XP"] + Fodder["XL"] + Fodder["XF"])) / self.n

            M_NFE = (Fodder["Q"] * Fodder["DM"] * Fodder["oDM"] * 1000 - Fodder["Q"] * (Fodder["XP"] + Fodder["XL"] + Fodder["XF"])).sum() / self.n    # intake rate of N-free extracts (in kg/(animal* a)) NOTE: Currently ash missing (could multiply with oDM)
            print("M_NFE:", M_NFE)

            # Emission factor in methane emission rate (factor) (in kg animal-1 a-1 )
            # In Dämmgen the equation reads as M_XF, but seems to be M_XFa (fat!)
            EF_CH4 = a * M_XFi + b * M_NFE + c * M_XP + d * M_XFa + e
            print("EF_CH4:", EF_CH4)

            Emis.loc["enteric fermentation", "CH4"] = self.n * EF_CH4


        elif self.enteric_ferment_method == "Piatkowski_2021":
            # Relatively simple calculation for CH4 only based on cow weight and dry matter intake

            # Equation from Piatkowski B, Jentsch W, Derno M (2010) Neue Ergebnisse zur Methanproduktion und zu deren quantitativer Vorhersage beim Rind. Zuchtungskunde 82:400–407

            Fodder = self.fodder()
    
            total_Fodder_DM_gr = (Fodder["Q"]*Fodder["DM"]).sum() * 1e6
 
            # NOTE just for printing
            Fodder_DM_per_cow_per_day_kg = (Fodder["Q"]*Fodder["DM"]).sum() * 1000 / (365 * self.n)
            print("Fodder_DM_per_cow_per_day_kg", Fodder_DM_per_cow_per_day_kg)

            daily_DM_per_kg_cow = total_Fodder_DM_gr / (365 * self.n * self.LIVING_MASS_KG)

            CH4_gr_per_kg_DM = 32.76 - 0.384 * daily_DM_per_kg_cow    # Methane production in gram, per kg fodder DM

            Emis.loc["enteric fermentation", "CH4"] = (CH4_gr_per_kg_DM * total_Fodder_DM_gr) / 1e6 # conversion from gram CH4 to kg, and gram fodder to kg

        return convert_molmasses(Emis, to="element")

        # MAINTAINANCE_N = URINE_NITROGEN_UNe + FECES_NITROGEN_FNe + SURFACE_LOSS_VN

        # # Net protein demand
        # N_TO_PROTEIN = 6.25
        # NET_MAINTAINANCE_nXP = MAINTAINANCE_N * N_TO_PROTEIN


        # DEMAND_nXP = NET_MAINTAINANCE_nXP + NET_MILK_nXP
        # # NOTE: This calculation achieves similar to the values mentioned in Tab 54 of Barth et al. 2005.
        # # However, the factor (2.1) is not applied to all nXP. Strange!

        # ## Alternative method from based on tabular values from Tabelle G (2020) Gruber Tabelle zur Fütterung der Milchkühe Zuchtrinder Schafe Ziegen., 46. Auflag. Bayerische Landesanstalt für Landwirtschaft (LfL), Freising-Weihenstephan
        # ## Or just tabular values from Table 55 of Barth et al. 2005


    def additional_input(self):
        """
        |  The sum of the additional fodder demand, and potentially for heifers?
        """

        return pd.concat([self.heifer_demand(), self.additional_fodder])



    def emissions(self):
        """
        |  Total emissions are the sum of emissions from intrastructure, manure storage, and enteric fermentation
        """
        # Enteric fermentation emissions
        Emis = pd.concat([self.emissions_enteric_fermentation(), self.housing_NH3_emissions()])

        if self.co2_respiration_method == "difference":
            """
            |  If this is selected, assume that the difference between Corg input <-> Corg output and emissions is emitted as CO2 (from cow respiration)
            |
            | **NOTE**:
            | - Method probably not working as it should in standalone setup, because orgC of hay for instance not known
            """
            Input = self.input()
            C_input_kg = (Input["Q"] * Input["DM"] * Input["oDM"] * Input["orgC"]).sum() * 1000
            print("C_input_kg", C_input_kg)

            Output = self.output()
            C_output_kg = (Output["Q"] * Output["DM"] * Output["oDM"] * Output["orgC"]).sum() * 1000

            C_emis_CH4_kg = Emis.loc["enteric fermentation", "CH4-C"]

            CO2C_emis = C_input_kg - C_output_kg - C_emis_CH4_kg

            CO2_l_per_cow_day = round((CO2C_emis * ConversionFactor.loc["CO2-C", "factor"].item() * 509) / (365 * self.n), 2)
            print(f"Calculated CO2 emissions equate to {CO2_l_per_cow_day} l CO2 per cow per day.\
                  Normal range usually 5000 - 7500 l according to Kinsman et al. 1995")

            Emis.loc["enteric fermentation", "CO2-C"] = CO2C_emis

        elif self.co2_respiration_method == "Kinsman_1995":

            # Mean CO2 emissions per cow per day (24 h): 6137 mean; range 5032 to 7427 litre CO2
            # source: Kinsman et al. (1995) https://doi.org/10.3168/jds.S0022-0302(95)76907-7

            # 6137 * 1.96 * 365 / 1000 # 4390 kg per cow per year

            Emis.loc["enteric fermentation", "CO2-C"] = 6137 * 1.96 * 365 / 1000 * self.n * ConversionFactor.loc["CO2","factor"].item()


        return Emis


"""
import start
from tools import prod
from production.animal.ruminant import DairyFarm

DairyFarm(enteric_ferment_method = "IPCC2019_Tier1", co2_respiration_method="Kinsman_1995", n=10).emissions()   # 943.74 kg CH4-C for 10 cows
DairyFarm(enteric_ferment_method = "Piatkowski_2021", n=10).emissions()  # 1134 kg CH4-C for 10 cows
DairyFarm(enteric_ferment_method = "Kirchgeßner_1994", n=10).emissions()  # 351 kg CH4-C for 10 cows # NOTE: Value much lower

Milk = DairyFarm(enteric_ferment_method = "IPCC2019_Tier1", n=10).output().loc[["cow milk"],:]

from external import ExternOutput

ExternOutput(I=Milk).input()
ExternOutput(I=Milk).emissions() # Throws an error. Needs to be solved



## TODO: Add oDM content to fodder, to calculate the emissions according to Krichgeßner

from production.plant.crop import ArableLand
ArableLand(O=prod("rapeseed, grain")).output()

import pandas as pd
A = pd.DataFrame({"Q": 10}, index=pd.MultiIndex.from_tuples([("rape", "grain")]))

A.loc[('rape', 'grain'),"oDM"] = 0.9
A

('rape', 'grain') in A.index

DairyFarm(enteric_ferment_method = "IPCC2006_Tier1").n


DairyFarm(enteric_ferment_method = "Piatkowski_2021", n=2).emissions()  # 246 kg CH4-C



Fodder = DairyFarm(enteric_ferment_method = "Piatkowski_2021", n=10).fodder() # 15 tons hay

Fodder.columns
Fodder["oDM"]


Farm1 = DairyFarm(enteric_ferment_method = "IPCC2006_Tier1")

Fodder = Farm1.fodder()
Fodder

Input = Farm1.I.copy()
# Input = Fodder
Supply_by_I = Input[["nXP", "NEL"]].mul(Input["Q"] * 1000, axis=0).sum().to_frame(name="supply by I").T

SupplyDemand = pd.concat([Farm1.nXP_NEL_demand_per_year(), Supply_by_I])
# SupplyDemand = pd.concat([DairyFarm(I=Fodder, n = 100).nXP_NEL_demand_per_year(), Supply_by_I])

SupplyDemand.loc["supply gap",:] = SupplyDemand.loc["demand per year",:] - SupplyDemand.loc["supply by I",:]
"""

## solve the following problems with DairyFarm:
# there are no emissions yet
# Higher inputs of grass seem to change the need for additional rapeseed only little. Why?



"""
from production.plant.grass import Meadow
from production.plant.crop import ArableLand
from production.animal.ruminant import DairyFarm


Grass = Meadow(O=prod("grass", 1000, "ton")).output()
Maize = ArableLand(O=prod("silage maize, whole", 400, "ton")).output()

Fodder = pd.concat([Grass, Maize])

DairyFarm(I=Fodder, n = 100).additional_fodder()


# Kuh frisst ca. 50 - 150 kg FM pro Tag.
Grass40ton = Meadow(O=prod("grass", 40, "ton")).output()

# strangely there is still mainly protein undersupply, even if feed only with grass
DairyFarm(I=Meadow(O=prod("grass", 35, "ton")).output(), n = 1).additional_fodder()
## NEL demand seems low!!


DairyFarm(I=Fodder, n = 100).additional_fodder()



DairyFarm(I=ArableLand(O=prod("silage maize, whole", 400, "ton")).output(), n = 100).additional_fodder()
DairyFarm(I=ArableLand(O=prod("silage maize, whole", 800, "ton")).output(), n = 100).additional_fodder()

DairyFarm(I=ArableLand(O=prod("silage maize, whole", 400, "ton")).output(), n = 100).nXP_NEL_demand_per_year()

Input = DairyFarm(I=Fodder).I.copy()
Input = Fodder

DairyFarm(I=Fodder, n = 100).nXP_NEL_demand_per_year()


Supply_by_I = Input[["nXP", "NEL"]].mul(Input["Q"] * 1000, axis=0).sum().to_frame(name="supply by I").T


DairyFarm(I=Fodder, n=1).nXP_NEL_demand_per_year()



## nXP etwa 3000 g * 365

"""
