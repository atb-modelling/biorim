import pandas as pd
import numpy as np

from declarations import Emissions, Product, Process 
from read import Herrmann, Terboven, Pnakovic_Dzurenda_2015
from tools import prod
import warnings

class FoodWaste(Process):

    """
    Represents the chemical properties from autumn tree leaves depending of the specified origin of the leaves or mixture of leaves
    :type O: Product
    :param O: food waste produced by households
    :type food_type: string 
    :param food_type: type of food according to Salangsang et al. 2022
    """

    def __init__(self, I= None, O = prod("food waste", 1, "ton"), food_type="Salangsang_B", id = None):
        self.I = I
        self.O = O 
        self.food_type=food_type
        self.id = id

    def output(self):

        #output based on the food waste type B in Table 1 (Salangsang et al. 2022)
        #food waste type B based in 43% vegetables, 4% rice and pasta, 45% fruits, 3% tea leaves, 4% meat
        #composition variation on food waste has an effect in the chemical characteristics
        Output=self.O.copy()
        Output.loc["food waste", "DM"]=0.157
        Output.loc["food waste", "oDM"]=(0.118 * Output["Q"][0])/(Output["Q"][0]*Output["DM"][0])
        Output.loc["food waste", "orgC"]=(0.461 * Output["Q"][0] * Output["DM"][0])/(Output["Q"][0] * Output["DM"][0] *Output["oDM"][0])
        Output.loc["food waste", "N"]=(0.026 * Output["Q"][0] * Output["DM"][0]/Output["Q"][0])*1000
        Output.loc["food waste", "B0"]=0.4 #lCH4/gVS Salangsang et al. 2022. Fig 1a. 
        Output.loc["food waste", "CH4 content"]=0.6 # Tanimu et al. 2014. Fig 2. 
        Output.loc["food waste", "P"]=1975*0.001 # Table 1 from Rahman et al. 2021. Orginally in mg/kg, converted to kg/tonne
        Output.loc["food waste", "K"]=17033*0.001 # Table 1 from Rahman et al. 2021. Orginally in mg/kg, converted to kg/tonne


        return Output

    def emissions(self):
        #assumed that all carbon comes from CO2 from atmosphere
        Output=self.output()
        Emis=pd.DataFrame(index=[], columns=[])
        if Output["U"][0]=="ton":
            Emis.loc["carbon assimilation by food waste", "CO2-C"]=-Output["Q"][0]*Output["DM"][0]*Output["oDM"][0]*Output["orgC"][0]*1000
        elif Output["U"][0]=="kg":
            Emis.loc["carbon assimilation by food waste", "CO2-C"]=-Output["Q"][0]*Output["DM"][0]*Output["oDM"][0]*Output["orgC"][0]

        return Emis

    def cashflow(self):
        Cashflow_capex=pd.DataFrame(index=[], columns=[])
        Cashflow_capex.loc["infrastructure and construction", "EUR"] = 0
        Cashflow_capex["period_years"] = 20
        Cashflow_capex["section"] = "capex"

        Cashflow_opex=pd.DataFrame(index=[], columns=[])
        Cashflow_opex.loc["generation of food waste", "EUR"] = 0
        Cashflow_opex["period_years"] = 1
        Cashflow_opex["section"] = "opex"

        return pd.concat([Cashflow_capex, Cashflow_opex])
        

# food_waste=FoodWaste(O = prod("food waste", 100, "ton")).output()
# food_waste

# carbon=food_waste["Q"][0]*food_waste["DM"][0]*food_waste["oDM"][0]*food_waste["orgC"][0]
# carbon

# nitrogen=(food_waste["Q"][0]*food_waste["N"][0])/1000
# nitrogen

# carbon/nitrogen

