## test procedure for functions
## possible functionality:
# for every class in the model:
# - have one test input dataset to run with
# - execute the function
# - evaluate the output for:
#  - format (pandas document)
#  - plausible range of values if validation dataset is available
#  - check if input of N,P,K,Q,... = output



import pandas as pd
from tools import relpath
from plotnine import *


def class_test():
    """
    |  Check all functions for basic functionality
    |  **NOTE:**
    |  - This is currently just an unfinished test routine.
    """
    TestArg = pd.read_csv(relpath("dev_utils\\class_test.csv"), sep=";", index_col=[0])

    Checks = pd.DataFrame()

    for funname, funarg in TestArg.iterrows():
        # instance of the class:
        fun = eval(funname)(**eval(funarg["arg"]))

        ## tests for output
        Output = fun.output()
        Input = fun.input()
        Emissions = fun.emissions()

        C = pd.DataFrame(index=[funname])

        # check whether output is a pandas DataFrame
        C["Object type"] = isinstance(Output, pd.DataFrame)

        # Check whether all Units are in "ton"
        C["Unit"] = all(Output["U"] == "ton")

        # Check whether all values (except for the unit) are positive
        C["all positive values"] = all(Output.loc[:, Output.columns != "U"] > 0)

        # Compare the input to the output quantity
        # at the example of the BroilerFarm the input is still not defined in tons
        # only the N, P, K, should be the same, considering also liquid and gaseous emissions
        # DM of inputs and outputs should be in the same order or magnitude
        C["Input Output ratio"] = Input["Q"].sum()/Output["Q"].sum() # needs modification

        # N inputs in absolute terms
#        N_in = Input
#        Output

        ## need to use the get_content to derive the contents
        # or define them as pandas in the class_test.csv already??


        Checks = Checks.append(C)

    return Checks


def text_plot(text):
    plot = (ggplot() + geom_blank()
    + theme_minimal()
    + annotate("text", x=0, y=0, label=text)
    + theme(panel_grid_major = element_blank(),
            panel_grid_minor = element_blank(),
            axis_text = element_blank(),
            axis_ticks = element_blank(),
            axis_title = element_blank()
            )
    )
    return plot

def input_output_plot(system, sysname, input=None, element = "nitrogen"):
    """
    |  Create a plot for the specified system that shows the negative emissions and the input as one bar, and the output and positive emissions as another.
    |  This allows for a quick comparison of whether inputs of carbon and nitrogen match outputs.

    :type system: class
    :param system: a system (e.g. material.biogas.BiogasPlant)
    :type sysname: str
    :param sysname: The name of the system (will appear as heading in the figure)
    :type element: str
    :param element: Element to compare inputs vs. outputs. 'nitrogen' or 'carbon'

    |  **NOTE:**
    |  - Not all emissions are currently associated with inputs. E.g. fossil emissions in many functions do not have any equivalent input
    |  - NOx emissions are often a combination of nitrogen from ambient air and burnt substrate
    |  - Therefore mismatches between input and output do not necessarily point to problems.

    |  **Examples:**
    |  input_output_plot(system = ManureStorage(), sysname = "ManureStorage", element = "nitrogen")
    """

    Output = system.output()
    print(f"system output \n {Output}")

    print("input dev_utils l 112", input)

    if input is not None:
        Input = input
    else:
        Input = system.input()
    # print(f"system input \n {Input}")


    if element == "nitrogen":
        Input = Input.dropna(subset=["N"])
    if element == "carbon":
        Input = Input.dropna(subset=["orgC"])

    if Input.empty:
        Input = None

    print("Input dev_utils l 124", Input)

    Emis = system.emissions()

    if Input is None:
        # ProdIn = pd.Series({'0':0}, index=["None"])
        ProdIn = pd.Series(dtype='float64')
        infotext = "Input is None"
    else:
        if element == "nitrogen":
            # ProdIn = (Input["Q"]*Input["N"]).droplevel("system")
            ProdIn = Input["Q"]*Input["N"]
        elif element == "carbon":
            # ProdIn = (Input["Q"]*Input["DM"]*Input["oDM"]*Input["orgC"]*1000).droplevel("system")
            ProdIn = Input["Q"]*Input["DM"]*Input["oDM"]*Input["orgC"]*1000
        else:
            raise ValueError("element can only be 'nitrogen' or 'carbon'")

    if Output is None:
        # ProdOut = pd.Series({'Q':0}, index=["None"])
        ProdOut = pd.Series(dtype='float64')
        infotext = "Output is None"
    else:
        if element == "nitrogen":
            ProdOut = Output["Q"]*Output["N"]
            # ProdOut = (Output["Q"]*Output["N"]).droplevel("system")
        elif element == "carbon":
            ProdOut = Output["Q"]*Output["DM"]*Output["oDM"]*Output["orgC"]*1000
            # ProdOut = (Output["Q"]*Output["DM"]*Output["oDM"]*Output["orgC"]*1000).droplevel("system")

    if Emis is None:
        # Emis = pd.Series({'Q':0}, index=["None"])
        Emis = pd.Series(dtype='float64')
    else:
        if element == "nitrogen":
            Emis = Emis.loc[:,[s for s in Emis.columns.tolist() if "-N" in s]].sum()
        elif element == "carbon":
            Emis = Emis.loc[:,[s for s in Emis.columns.tolist() if "-C" in s]].sum()

    ProdIn = ProdIn.dropna(axis="index")
    ProdOut = ProdOut.dropna(axis="index")

    negEmis = Emis[Emis < 0]
    posEmis = Emis[Emis > 0]

    if len(posEmis.index) == 0:
        Out = ProdOut
    else:
        Out = pd.DataFrame(pd.concat([ProdOut, posEmis]))
    Out = Out.reset_index(level=0)
    Out.columns = ["item", "value"]
    Out["category"] = "Output"
    Out["element"] = element

    if len(negEmis.index) == 0:
        In = ProdIn
    else:
        negEmis = negEmis*-1
        In = pd.DataFrame(pd.concat([ProdIn, negEmis]))

    In = In.reset_index(level=0)
    In.columns = ["item", "value"]
    In["category"] = "Input"
    In["element"] = element

    dat = pd.concat([In, Out])

    dat["value"] = pd.to_numeric(dat["value"], downcast="float")

    Outsum = sum(Out["value"])
    Insum  = sum(In["value"])

    if all([Input is not None, Output is not None]):
        InOutRatio = round(Outsum / Insum * 100, 2)
        infotext = element + " output is " + str(InOutRatio) + "% of input"

    ylabtext = element + " [kg]"

    print("dev_ultis line 202 dat: \n", dat)

    plot = (
    ggplot(dat, aes(x='category', y='value', fill='item'))
    + geom_col()
    + labs(title=sysname, subtitle=infotext, y=ylabtext, x=infotext)
    # + annotate('text', x=0.5, y=0, label=infotext)
    + theme_minimal()
    + theme(panel_grid_major_x = element_blank(),
            panel_grid_major_y = element_blank(),
            )
    )

    return plot