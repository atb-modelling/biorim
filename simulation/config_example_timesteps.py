"""
Config example that works together with timestep_chains analysis.
Example for a two timesteps representing two years. Cow manure produced in one year is used as a fertilizer in the next year.
"""

import start
from tools import prod
from analysis.timestep_chains import start_simulation

config = {
    ### Main settings
    "simulation"   :{
        "name": "storage", 
        "type": "single", # No sensitivity analysis conducted
        "starting_system" : "production.plant.crop.ArableLand_1", 
        "timesteps": ["t1","t2"] # NOTE: Two timesteps
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
    },

    "systems"   :{
        
        "production.plant.crop.ArableLand_1" :{
            'I': {"t1": None, # NOTE: in the first timestep no manure available as input yet
                  "t2": prod('cow manure, liquid', 100, "%", source='material.storing.ManureStorage_1')},
            'O': prod("barley, grain", 10, "ton")
                },

        "production.plant.grass.Meadow_1" :{
            'O': prod("grass", 50, "ton")
                },

        "production.animal.ruminant.DairyFarm_1" :{
            'I' : prod(["barley, grain","grass"], 
                        [100, 100],
                        ["%","%"],
                        source= ["production.plant.crop.ArableLand_1", "production.plant.grass.Meadow_1"])
                },

        "material.storing.ManureStorage_1" :{
            'I': {"t1": prod('cow manure, liquid', 100, "%", source='production.animal.ruminant.DairyFarm_1'),
                "t2": None},
            'timestep_transfer': 1 # NOTE: This initiates that the output is provided in the next timestep, where it is the input for ArableLand_1
            },

    }
}



# Example results
result = start_simulation(config)

# unconverted emissions (pandas with emissions as elements of N and C)
result["storage"]["t1"][0]["emissions"]

# converted emissions (xarrays with all time steps)
result["storage"]["emissions"]
result["storage"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas()

result["storage"]["t1"][0]["outputs"].loc[:,["Q","U"]]
result["storage"]["t1"]["parameters"]
