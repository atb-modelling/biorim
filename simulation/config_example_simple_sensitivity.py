"""
Config example that works together with timestep_chains analysis.
Example for a single timestep and starting system as the beginning of the chain from where on outputs are passed on (downstream modelling only).
If not timestep is defined it is just called "t1".
A sensitivity analysis is done for parameters specified as lists.
"""

import start
from tools import prod
from analysis.timestep_chains import start_simulation

config = {
    ### Main settings
    "simulation"   :{
        'name': "storage",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.animal.poultry.BroilerFarm_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
    },

    "systems"   :{
        "production.animal.poultry.BroilerFarm_1" :{
                'O' : prod('broiler manure', 10, "ton"),
                'B0_source' : "KTBL",
                },

        "transport.LandTransport_1" :{
                'I': prod('broiler manure', 100, "%", source='production.animal.poultry.BroilerFarm_1'),
                'dist_km': 5,
                'type': "tractor, small size"
                },

        "material.storing.ManureStorage_1" :{
                'I': prod('broiler manure', 100, '%', source='transport.LandTransport_1'),
                'method' : ["default", "IPCC2019"], # NOTE: Sensitivity analysis done for these parameters
                'time_period' : 168
                },

        "transport.LandTransport_2" :{
                'I': prod('broiler manure', 100, '%', source='material.storing.ManureStorage_1'),
                'dist_km': 10,
                'type': "lorry, medium size"
                },

        # "production.plant.crop.ArableLand_1" :{
        #         'I': prod('broiler manure', 100, '%', source='transport.LandTransport_2'),
        #         },

        "production.plant.fertilization.ManureSpreading_1" :{
            'I': prod('broiler manure', 100, "%", source='transport.LandTransport_2'),
            'method': ["default", "EEA2016"], # NOTE: Sensitivity analysis done for these parameters
            'type' : "solid",
            },
    }
}

"""
# Example results
result = start_simulation(config)
result.to_folder()

# unconverted emissions (pandas with emissions as elements of N and C)
result["storage"]["t1"][0]["emissions"]

# converted emissions (xarrays with all time steps)
result["storage"]["emissions"]
result["storage"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas()

result["storage"]["t1"][0]["outputs"].loc[:,["Q","U"]]
result["storage"]["t1"]["parameters"]
"""