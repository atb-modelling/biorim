from read import datapath
import start
import matplotlib.pyplot as plt
import pylab
import numpy as np
import pandas as pd

from tools import prod
from analysis.downstream_chains import start_simulation
from analysis import simdict
from simulation.data_management_and_plotting.subsetting import SubSetting
from simulation.data_management_and_plotting.biogenic_reassignment import biogenic_reassignment
from simulation.sensitivity_analysis.sensitivity_analysis import SensitivityAnalysis

composting_density = {
    ### Main settings
    "simulation"   :{
        'name': "composting",
        'type': "all combinations",
        'reference_system' : "production.plant.street_trees.StreetTrees_1" # "reference_system" : "production.animal.poultry.BroilerFarm_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod('tree leaves', 40260, "ton"),
                'density':[0.2, 0.1, 0.03]
                },

        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I' : prod(('production.plant.street_trees.StreetTrees_1', 'tree leaves')),
                'road_length_km' : 29531
                },

        "transport.LandTransport_1" :{
                'I': prod(('production.plant.street_leaves_collection.StreetLeavesCollection_1', 'tree leaves')),
                'dist_km': 122,
                'type': "lorry, large size",
                'method':"LBE",
                'load_basis':"volume"
                },

        "material.composting.CompostingLeaves_1" :{
                'I': prod(('transport.LandTransport_1', 'tree leaves')),
                'method' : "Andersen_2010",
                'CH4_emis': 0.02
                },

        "production.plant.fertilization.CompostSpreading_1" :{
                'I': prod(('material.composting.CompostingLeaves_1', 'leaves compost')),
                'maturity' : "very mature",
                'method' : "VDLUFA"
                },
    }
}

composting_density_dict=SensitivityAnalysis(composting_density, type_scenario="composting").scenario_subsetting()
composting_density_dict.keys()
composting_density_dict["simulation_composting_gwp_2"]

