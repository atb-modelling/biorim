from read import datapath
import start
import matplotlib.pyplot as plt
import pylab
import numpy as np
import pandas as pd

from tools import prod
from analysis.downstream_chains import start_simulation
from analysis import simdict
from simulation.data_management_and_plotting.subsetting import SubSetting
from simulation.data_management_and_plotting.biogenic_reassignment import biogenic_reassignment
from simulation.sensitivity_analysis.sensitivity_analysis import SensitivityAnalysis

biogas_density = {
    ### Main settings
    "simulation"   :{
        'name': "composting",
        'type': "all combinations",
        'reference_system' : "production.plant.street_trees.StreetTrees_1" # "reference_system" : "production.animal.poultry.BroilerFarm_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod('tree leaves', 40260, "ton"),
                'density':[0.2, 0.1, 0.03]
                },

        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I' : prod(('production.plant.street_trees.StreetTrees_1', 'tree leaves')),
                'road_length_km' : 29531
                },

        "transport.LandTransport_1" :{
                'I': prod(('production.plant.street_leaves_collection.StreetLeavesCollection_1', 'tree leaves')),
                'dist_km': 657,
                'type': "lorry, large size",
                'method':"LBE",
                'load_basis':"volume"
                },

        "material.storing.LeafStorageContinuous_1" :{
                'I': prod(('transport.LandTransport_1', 'tree leaves')),
                },

        "material.biogas.BiogasPlant_1" :{
                'I': prod(('material.storing.LeafStorageContinuous_1', 'tree leaves')),
                'residue_storage_type': "closed",
                'thermal_energy_use': 0.4,
                'CH4_loss_conv_method': "KTBL"
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod(('material.biogas.BiogasPlant_1', 'biogas digestate')),
                'method': "default",
                'type' : "liquid",
                "incorporation" : "incorporation <= 4h", 
                "avg_temp":"10 °C"
                },

        "external.Electricity_1" :{
                'I': prod(('material.biogas.BiogasPlant_1', 'electrical energy'))
                },

        "external.Heating_1" :{
                'I': prod(('material.biogas.BiogasPlant_1', 'thermal energy'))
                },
    }
}

biogas_density_dict=SensitivityAnalysis(biogas_density, type_scenario="biogas").scenario_subsetting()
biogas_density_dict
biogas_density_dict.keys()
biogas_density_dict["simulation_composting_gwp_2"]

