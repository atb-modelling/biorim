from read import datapath
import start
import matplotlib.pyplot as plt
import pylab
import numpy as np
import pandas as pd

from tools import prod
from analysis.downstream_chains import start_simulation
from analysis import simdict
from simulation.data_management_and_plotting.subsetting import SubSetting
from simulation.data_management_and_plotting.biogenic_reassignment import biogenic_reassignment

compostingconfig_vol = {
    ### Main settings
    "simulation"   :{
        'name': "composting",
        'type': "single",
        'reference_system' : "production.plant.street_trees.StreetTrees_1" # "reference_system" : "production.animal.poultry.BroilerFarm_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod('tree leaves', 40260, "ton"),
                'density':0.2
                },

        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I' : prod(('production.plant.street_trees.StreetTrees_1', 'tree leaves')),
                'road_length_km' : 29531
                },

        "transport.LandTransport_1" :{
                'I': prod(('production.plant.street_leaves_collection.StreetLeavesCollection_1', 'tree leaves')),
                'dist_km': 122,
                'type': "lorry, large size",
                'method':"LBE",
                'load_basis':"volume"
                },

        "material.composting.CompostingLeaves_1" :{
                'I': prod(('transport.LandTransport_1', 'tree leaves')),
                'method' : "Andersen_2010",
                'CH4_emis': 0.02
                },

        "production.plant.fertilization.CompostSpreading_1" :{
                'I': prod(('material.composting.CompostingLeaves_1', 'leaves compost')),
                'maturity' : "very mature",
                'method' : "VDLUFA"
                },
    }
}

resultcomp= start_simulation(compostingconfig_vol)
simdict(resultcomp)

#to pandas
emissions_composting_vol=resultcomp["composting"]["emissions"][0].to_pandas()
emissions_composting_vol

emissions_composting_vol_gwp=resultcomp["composting"]["gwp"][0].loc[:,:,"GWP100"].to_pandas()
emissions_composting_vol_gwp

#to csv
emissions_composting_vol.to_csv("emissions_composting.csv")
emissions_composting_vol=pd.read_csv("emissions_composting.csv")
emissions_composting_vol

emissions_composting_vol_gwp.to_csv("emissions_composting_gwp.csv")
emissions_composting_vol_gwp=pd.read_csv("emissions_composting_gwp.csv")
emissions_composting_vol_gwp

#Streetleaves collection graphs, functions from plotting_template.py

# SLC_GHG=SubSetting(dataframe=emissions_composting).SLC_subsetting()
# horizontal_bar_plotting(dataframe=SLC_GHG, type="GHG emissions", figuresize=(8,8))

# SLC_process_GHG=SubSetting(dataframe=emissions_composting).SLC_process_subsetting()
# horizontal_bar_plotting(dataframe=SLC_process_GHG, type="GHG emissions",figuresize=(8,8))

# SLC_GWP=SubSetting(dataframe=emissions_composting_gwp).SLC_subsetting()
# horizontal_bar_plotting(dataframe=SLC_GWP, type="GWP100")

# SLC_process_GWP=SubSetting(dataframe=emissions_composting_gwp).SLC_process_subsetting()
# SLC_process_GWP
# horizontal_bar_plotting(dataframe=SLC_process_GWP, type="GWP100",figuresize=(8,8))

#Composting scenario graphs, functions from plotting_template.py

composting_scenario_GHG=SubSetting(dataframe=emissions_composting_vol).composting_scenario()
#horizontal_bar_plotting(dataframe=composting_scenario_GHG, type="GHG emissions",figuresize=(8,8))

composting_scenario_GWP_vol=SubSetting(dataframe=emissions_composting_vol_gwp).composting_scenario()
composting_scenario_GWP_vol_perunit=composting_scenario_GWP_vol/40260
composting_scenario_GWP_vol_perunit
#horizontal_bar_plotting(dataframe=composting_scenario_GWP, type="GWP100", figuresize=(8,8))

#biogenic reassigment
composting_reas_GWP_vol=biogenic_reassignment(emissions_composting_vol_gwp)
composting_reas_GWP_vol=SubSetting(dataframe=composting_reas_GWP_vol).composting_scenario()
composting_reas_GWP_vol


subset_transport=composting_reas_GWP_vol[composting_reas_GWP_vol.index.str.contains("transport, freight")]
string=list(subset_transport.index)[0]
string

if composting_reas_GWP_vol.index.str.contains("transport, freight")==True:
    composting_reas_GWP_vol

composting_reas_GWP_vol.values.sum()/40260

composting_reas_GWP_vol.mul(composting_reas_GWP_vol.gt(0)).sum().sum()

composting_reas_GWP_vol.loc["leaves composting"].sum()/composting_reas_GWP_vol.mul(composting_reas_GWP_vol.gt(0)).sum().sum()*100
