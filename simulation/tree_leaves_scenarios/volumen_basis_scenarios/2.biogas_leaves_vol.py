import start
import matplotlib.pyplot as plt
import pylab
import numpy as np
import pandas as pd

from simulation.data_management_and_plotting.subsetting import SubSetting
from simulation.data_management_and_plotting.biogenic_reassignment import biogenic_reassignment
from tools import prod, convert_molmasses
from analysis.downstream_chains import start_simulation
from mpl_axes_aligner import align
from analysis import simdict
import seaborn as sns

biogasconfig_vol = {
    ### Main settings
    "simulation"   :{
        'name': "biogas",
        'type': "single", #could be set to 'single' (first combination) or 'all combinations' 
        'reference_system' : "production.plant.street_trees.StreetTrees_1" # "reference_system" : "production.animal.poultry.BroilerFarm_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod('tree leaves', 40260, "ton"),
                'density':0.2
                },

        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I' : prod(('production.plant.street_trees.StreetTrees_1', 'tree leaves')),
                'road_length_km' : 29531
                },

        "transport.LandTransport_1" :{
                'I': prod(('production.plant.street_leaves_collection.StreetLeavesCollection_1', 'tree leaves')),
                'dist_km': 657,
                'type': "lorry, large size",
                'method':"LBE",
                'load_basis':"volume"
                },

        "material.storing.LeafStorageContinuous_1" :{
                'I': prod(('transport.LandTransport_1', 'tree leaves')),
                'daily_load' : 300,
                },

        "material.biogas.BiogasPlant_1" :{
                'I': prod(('material.storing.LeafStorageContinuous_1', 'tree leaves')),
                'residue_storage_type': ["closed", "open"],
                'thermal_energy_use': [0.4, 0],
                'CH4_loss_conv_method': ["KTBL", "Liebetrau_2010.low", "Liebetrau_2010.high"]
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod(('material.biogas.BiogasPlant_1', 'biogas digestate')),
                'method': "default",
                'type' : "liquid",
                "incorporation" : "incorporation <= 4h", 
                "avg_temp":"10 °C"
                },

        "external.Electricity_1" :{
                'I': prod(('material.biogas.BiogasPlant_1', 'electrical energy'))
                },

        "external.Heating_1" :{
                'I': prod(('material.biogas.BiogasPlant_1', 'thermal energy'))
                },
    }
}

resultbio = start_simulation(biogasconfig_vol)
simdict(resultbio)


#to pandas
emissions_biogas_vol=resultbio["biogas"]["emissions"][0].to_pandas()
emissions_biogas_vol

emissions_biogas_vol_gwp=resultbio["biogas"]["gwp"][0].loc[:,:,"GWP100"].to_pandas()
emissions_biogas_vol_gwp

#to csv
emissions_biogas_vol.to_csv("emissions_biogas.csv")
emissions_biogas_vol=pd.read_csv("emissions_biogas.csv")
emissions_biogas_vol=emissions_biogas_vol.fillna(0)
emissions_biogas_vol

emissions_biogas_vol_gwp.to_csv("emissions_biogas_gwp.csv")
emissions_biogas_vol_gwp=pd.read_csv("emissions_biogas_gwp.csv")
emissions_biogas_vol_gwp

#joining biogas+compost replacement
emissions_biogas_wr=pd.concat([emissions_biogas, emissions_replace1])
emissions_biogas_wr

emissions_biogas_wr_gwp=pd.concat([emissions_biogas_gwp, emissions_replace_gwp1])
emissions_biogas_wr_gwp
biogas_reas_wr_GWP=biogenic_reassignment(emissions_biogas_wr_gwp)
biogas_reas_wr_GWP=SubSetting(dataframe=biogas_reas_wr_GWP).biogas_scenario()
biogas_reas_wr_GWP

#Composting scenario graphs, functions from plotting_template.py

biogas_scenario_vol_GHG=SubSetting(dataframe=emissions_biogas_vol).biogas_scenario()
biogas_scenario_vol_GHG
#horizontal_bar_plotting(dataframe=biogas_scenario_GHG, type="GHG emissions", figuresize=(7,7))

biogas_scenario_vol_GWP=SubSetting(dataframe=emissions_biogas_vol_gwp).biogas_scenario()
biogas_scenario_vol_GWP
#horizontal_bar_plotting(dataframe=biogas_scenario_GWP, type="GWP100", figuresize=(8,8))

#biogenic reassigment
biogas_reas_vol_GWP=biogenic_reassignment(emissions_biogas_vol_gwp)
biogas_reas_vol_GWP=SubSetting(dataframe=biogas_reas_vol_GWP).biogas_scenario()
biogas_reas_vol_GWP

# biogas_reas_GWP.values.sum()/40260

# biogas_reas_GWP.mul(biogas_reas_GWP.gt(0)).sum().sum()

# #% of decay compared to toal CO2 in leaves
# biogas_reas_GWP.loc["leaf decay emissions","CO2 biogenic"]*12/44
# biogas_reas_GWP.loc["leaf decay emissions","CO2 biogenic"]/biogas_reas_GWP.loc["carbon in leaves","CO2 biogenic"]*-1*100

# #% CO2 fossil
# biogas_reas_GWP["CO2 fossil"]
# biogas_reas_GWP["CO2 fossil"].gt(0)
# biogas_reas_GWP["CO2 fossil"].mul(biogas_reas_GWP["CO2 fossil"].gt(0)).sum()/biogas_reas_GWP.mul(biogas_reas_GWP.gt(0)).sum().sum()*100

# #largest possitive emissions
# biogas_reas_GWP.loc["digestate spreading"].mul(biogas_reas_GWP.loc["digestate spreading"].gt(0)).sum()/biogas_reas_GWP.mul(biogas_reas_GWP.gt(0)).sum().sum()*100
# biogas_reas_GWP.loc["biogas production"].mul(biogas_reas_GWP.loc["biogas production"].gt(0)).sum()/biogas_reas_GWP.mul(biogas_reas_GWP.gt(0)).sum().sum()*100
# biogas_reas_GWP.loc["leaf decay emissions"].mul(biogas_reas_GWP.loc["leaf decay emissions"].gt(0)).sum()/biogas_reas_GWP.mul(biogas_reas_GWP.gt(0)).sum().sum()*100

# #largest negative emissions
# biogas_reas_GWP.loc["electricity use balance"].mul(biogas_reas_GWP.loc["electricity use balance"].lt(0)).sum()/biogas_reas_GWP.mul(biogas_reas_GWP.lt(0)).sum().sum()*100
# biogas_reas_GWP.loc["heating"].mul(biogas_reas_GWP.loc["heating"].lt(0)).sum()/biogas_reas_GWP.mul(biogas_reas_GWP.lt(0)).sum().sum()*100
# biogas_reas_GWP.loc["biogas production"].mul(biogas_reas_GWP.loc["biogas production"].lt(0)).sum()/biogas_reas_GWP.mul(biogas_reas_GWP.lt(0)).sum().sum()*100















