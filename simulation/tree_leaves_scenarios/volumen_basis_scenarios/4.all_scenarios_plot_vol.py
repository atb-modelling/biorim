import matplotlib.pyplot as plt
import matplotlib.pylab as plb
import pandas as pd
import numpy as np
from simulation.data_management_and_plotting.scenarios_plotting import AllScenariosBarplot

#########################################
#This plotting es for scenarios WITHOUT peat replacement
#########################################

# #recommended datasets (from scenarios scripts) to use for dictionary definition
# composting_scenario_GWP
# biogas_scenario_GWP
# biogasman_scenario_GWP

# dict_scenario={"composting":composting_scenario_GWP, "biogas":biogas_scenario_GWP,
#         "biogas with managment":biogasman_scenario_GWP}

# #reassigned datasets (CO2 fossil and biogenic)
# composting_reas_GWP
# biogas_reas_GWP
# biogasman_reas_GWP

# dict_scenario_reas={"composting":composting_reas_GWP, "biogas":biogas_reas_GWP,
#         "pretreatment\nand biogas":biogasman_reas_GWP}

# AllScenariosBarplot(dict_scenario_reas, dict_scenario2=None).scenarios_barplot()
# AllScenariosBarplot(SLC_scenario_dict_km, dict_scenario2=None, offset_left=(0.30,0.98),offset_right=(0.72,0.98)).scenarios_barplot()


#emissions per ton of leaves
composting_reas_vol_GWP_perton=composting_reas_GWP_vol/40260
biogas_reas_vol_GWP_perton=biogas_reas_vol_GWP/40260
biogasman_reas_vol_GWP_perton=prebio_reas_vol_GWP/40260

dict_scenario_reas_vol_perton={"composting":composting_reas_vol_GWP_perton, "biogas":biogas_reas_vol_GWP_perton,
        "pretreatment\nand biogas":biogasman_reas_vol_GWP_perton}

AllScenariosBarplot(dict_scenario_reas_vol_perton).scenarios_barplot(per_ton=True,n_cols=4, subs="of leaves")


# #emissions per ton of leaves/SLC inclusion
# composting_reas_GWP_perton=composting_reas_GWP/40260
# biogas_reas_GWP_perton=biogas_reas_GWP/40260
# biogasman_reas_GWP_perton=biogasman_reas_GWP/40260

# SLC_scenario_dict_km

# dict_scenario_reas_perton={"composting":composting_reas_GWP_perton, "biogas":biogas_reas_GWP_perton,
#         "pretreatment\nand biogas":biogasman_reas_GWP_perton}
# dict_scenario_reas_perton

# AllScenariosBarplot(dict_scenario1=SLC_scenario_dict_km, dict_scenario2=dict_scenario_reas_perton).scenarios_barplot_double(subs="of leaves", per_km=(True,"top"), per_ton=(True,"bottom"))

#emissions per ton of leaves, k1=0.005
# composting_reas_GWP_perton=composting_reas_GWP/40260
# biogas__k1_005_reas_GWP_perton=biogas_k1_005_reas_GWP/40260
# biogasman_reas_GWP_perton=biogasman_reas_GWP/40260

# dict_scenario_reas_perton_k1_005={ "composting":composting_reas_GWP_perton, "biogas":biogas__k1_005_reas_GWP_perton,
#         "pretreatment\nand biogas":biogasman_reas_GWP_perton}
# AllScenariosBarplot(dict_scenario_reas_perton_k1_005, dict_scenario2=None).scenarios_barplot()


# #emissions per ton of leaves/with and witout fossil
# composting_reas_GWP_perton=composting_reas_GWP/40260
# biogas_reas_GWP_perton=biogas_reas_GWP/40260
# biogasman_reas_GWP_perton=biogasman_reas_GWP/40260

# composting_reas_GWP_perton_no_bio=composting_reas_GWP_perton.drop("CO2 biogenic", axis=1)
# biogas_reas_GWP_perton_no_bio=biogas_reas_GWP_perton.drop("CO2 biogenic", axis=1)
# biogasman_reas_GWP_perton_no_bio=biogasman_reas_GWP_perton.drop("CO2 biogenic", axis=1)

# composting_reas_GWP_perton_no_bio.values.sum()
# biogas_reas_GWP_perton_no_bio.values.sum()
# biogasman_reas_GWP_perton_no_bio.values.sum()

# biogas_reas_GWP_perton_no_bio.sum(axis=1)[1]/biogas_reas_GWP_perton_no_bio.mul(biogas_reas_GWP_perton_no_bio.gt(0)).sum().sum()*100
# biogas_reas_GWP_perton_no_bio.mul(biogas_reas_GWP_perton_no_bio.gt(0)).sum().sum()

# dict_scenario_reas_perton={"composting":composting_reas_GWP_perton, "biogas":biogas_reas_GWP_perton,
#         "pretreatment\nand biogas":biogasman_reas_GWP_perton}

# dict_scenario_reas_perton_no_bio={"composting":composting_reas_GWP_perton_no_bio, "biogas":biogas_reas_GWP_perton_no_bio,
#         "pretreatment\nand biogas":biogasman_reas_GWP_perton_no_bio}

# AllScenariosBarplot(dict_scenario1=dict_scenario_reas_perton, dict_scenario2=dict_scenario_reas_perton_no_bio).scenarios_barplot_double(subs="of leaves", net_emissions=(True,True), per_km=(False,"none"), per_ton=(True,"both"))
