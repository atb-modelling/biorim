import matplotlib.pyplot as plt
import matplotlib.pylab as plb
import pandas as pd
import numpy as np
from simulation.data_management_and_plotting.scenarios_plotting import AllScenariosBarplot, LegendWithoutDuplicate, GHG_colors, process_colors

#########################################
#This plotting es for scenarios WITHOUT peat replacement
#########################################

#recommended datasets (from scenarios scripts) to use for dictionary definition
composting_scenario_GWP
biogas_scenario_GWP
biogasman_scenario_GWP

dict_scenario={"composting":composting_scenario_GWP, "biogas":biogas_scenario_GWP,
        "biogas with managment":biogasman_scenario_GWP}

#reassigned datasets (CO2 fossil and biogenic)
composting_reas_GWP
biogas_reas_GWP
biogasman_reas_GWP

dict_scenario_reas={"composting":composting_reas_GWP, "biogas":biogas_reas_GWP,
        "pretreatment\nand biogas":biogasman_reas_GWP}

AllScenariosBarplot(dict_scenario_reas, dict_scenario2=None).scenarios_barplot()
AllScenariosBarplot(SLC_scenario_dict_km, dict_scenario2=None, offset_left=(0.30,0.98),offset_right=(0.72,0.98)).scenarios_barplot()


#emissions per ton of leaves
composting_reas_GWP_perton=composting_reas_GWP/40260
biogas_reas_GWP_perton=biogas_reas_GWP/40260
biogasman_reas_GWP_perton=biogasman_reas_GWP/40260

dict_scenario_reas_perton={"composting":composting_reas_GWP_perton, "biogas":biogas_reas_GWP_perton,
        "pretreatment\nand biogas":biogasman_reas_GWP_perton}

AllScenariosBarplot(dict_scenario_reas_perton, offset_left=(0.30,0.98), offset_right=(0.72,0.98)).scenarios_barplot(per_ton=True,subs="of leaves")


#emissions per ton of leaves/SLC inclusion
composting_reas_GWP_perton=composting_reas_GWP/40260
biogas_reas_GWP_perton=biogas_reas_GWP/40260
biogasman_reas_GWP_perton=biogasman_reas_GWP/40260

SLC_scenario_dict_km

dict_scenario_reas_perton={"composting":composting_reas_GWP_perton, "biogas":biogas_reas_GWP_perton,
        "pretreatment\nand biogas":biogasman_reas_GWP_perton}
dict_scenario_reas_perton

AllScenariosBarplot(dict_scenario1=SLC_scenario_dict_km, dict_scenario2=dict_scenario_reas_perton).scenarios_barplot_double(subs="of leaves", per_km=(True,"top"), per_ton=(True,"bottom"))

#emissions per ton of leaves, k1=0.005
# composting_reas_GWP_perton=composting_reas_GWP/40260
# biogas__k1_005_reas_GWP_perton=biogas_k1_005_reas_GWP/40260
# biogasman_reas_GWP_perton=biogasman_reas_GWP/40260

# dict_scenario_reas_perton_k1_005={ "composting":composting_reas_GWP_perton, "biogas":biogas__k1_005_reas_GWP_perton,
#         "pretreatment\nand biogas":biogasman_reas_GWP_perton}
# AllScenariosBarplot(dict_scenario_reas_perton_k1_005, dict_scenario2=None).scenarios_barplot()


#emissions per ton of leaves/with and witout fossil
composting_reas_GWP_perton=composting_reas_GWP/34000
biogas_reas_GWP_perton=biogas_reas_GWP/34000
biogasman_reas_GWP_perton=biogasman_reas_GWP/34000

composting_reas_GWP_perton_no_bio=composting_reas_GWP_perton.drop(["CO2 biogenic", "CH4 biogenic", "N2O biogenic"], axis=1)
composting_reas_GWP_perton_no_bio

biogas_reas_GWP_perton_no_bio=biogas_reas_GWP_perton.drop(["CO2 biogenic", "CH4 biogenic", "N2O biogenic"], axis=1)
biogas_reas_GWP_perton_no_bio

biogasman_reas_GWP_perton_no_bio=biogasman_reas_GWP_perton.drop(["CO2 biogenic", "CH4 biogenic", "N2O biogenic"], axis=1)
biogasman_reas_GWP_perton_no_bio

composting_reas_GWP_perton_no_bio.values.sum()
biogas_reas_GWP_perton_no_bio.values.sum()
biogasman_reas_GWP_perton_no_bio.values.sum()



biogas_reas_GWP_perton_no_bio.sum(axis=1)[1]/biogas_reas_GWP_perton_no_bio.mul(biogas_reas_GWP_perton_no_bio.gt(0)).sum().sum()*100

biogas_reas_GWP_perton_no_bio.mul(biogas_reas_GWP_perton_no_bio.gt(0)).sum().sum()

dict_scenario_reas_perton={"composting":composting_reas_GWP_perton, "biogas":biogas_reas_GWP_perton,
        "pretreatment\nand biogas":biogasman_reas_GWP_perton}

dict_scenario_reas_perton_no_bio={"composting":composting_reas_GWP_perton_no_bio, "biogas":biogas_reas_GWP_perton_no_bio,
        "pretreatment\nand biogas":biogasman_reas_GWP_perton_no_bio}

AllScenariosBarplot(dict_scenario1=dict_scenario_reas_perton, dict_scenario2=dict_scenario_reas_perton_no_bio).scenarios_barplot_double(subs="of leaves", net_emissions=(True,False), per_km=(False,"none"), per_ton=(True,"both"), font=18)


AllScenariosBarplot(dict_scenario1=dict_scenario_reas_perton, dict_scenario2=dict_scenario_reas_perton_no_bio).scenarios_barplot_double(subs="of leaves", net_emissions=(True,False), ax_config="gas_process", n_cols=(4,4), per_km=(False,"none"), per_ton=(True,"both"), font=14, legend_font=(15,15))


# top=0.85,
# bottom=0.22,
# left=0.29,
# right=0.73,
# hspace=0.31,
# wspace=0.305























#########################################
#This plotting es for scenarios WITH peat replacement, which has a especific type of display 
#########################################

##########
biogas=biogas_reas_GWP
biogas=biogas[["CO2 biogenic", "CO2 fossil", "CH4", "N2O", "N2O indirect", "NOx"]]
biogas

##########
pretreatmentbiogas=biogasman_reas_GWP
pretreatmentbiogas=managementbiogas[["CO2 biogenic", "CO2 fossil", "CH4", "N2O", "N2O indirect", "NOx"]]
pretreatmentbiogas

##########
replacement=emissions_replace_gwp1
replacement=replacement.drop(["system"], axis=1)
replacement=replacement.rename(columns={"CO2":"CO2 fossil"})
replacement=replacement.set_index("item")
replacement


replacement_positiv=replacement.mul(replacement.gt(0))
replacement_positiv
replacement_negativ=replacement.mul(replacement.lt(0))
replacement_negativ

#####################
fig, (ax1,ax2) = plt.subplots(ncols=2, sharey=True)
ax1.yaxis.grid(color='gray', linestyle='dashed')
ax2.yaxis.grid(color='gray', linestyle='dashed')
ax1.axhline(0, color='black', linestyle="--")
ax2.axhline(0, color='black', linestyle="--")

###gases
processes=list(biogas.index)
gases=list(biogas.columns)

gases_resume_pos=pd.DataFrame()
for gas in gases:
    gases_resume_pos.loc["biogas positive", gas]=biogas.loc[biogas[gas]>0,gas].sum()
gases_resume_pos

gases_resume_neg=pd.DataFrame()
for gas in gases:
    gases_resume_neg.loc["biogas negative", gas]=biogas.loc[biogas[gas]<0,gas].sum()
gases_resume_neg

y_pos=0
ax1.bar("biogas", gases_resume_pos.loc["biogas positive","CO2 biogenic"], bottom=y_pos, label="CO2 biogenic", color=GHG_colors["CO2 biogenic"])
y_pos=y_pos + gases_resume_pos.loc["biogas positive","CO2 biogenic"]
ax1.bar("biogas", gases_resume_pos.loc["biogas positive","CO2 fossil"], bottom=y_pos, label="CO2 fossil", color=GHG_colors["CO2 fossil"])
y_pos=y_pos + gases_resume_pos.loc["biogas positive","CO2 fossil"]
ax1.bar("biogas", replacement["CO2 fossil"], bottom=y_pos, label="CO2 fossil", color=GHG_colors["CO2 fossil"], hatch ="///", edgecolor='k')
y_pos=y_pos + replacement["CO2 fossil"]
ax1.bar("biogas", gases_resume_pos.loc["biogas positive","CH4"], bottom=y_pos, label="CH4", color=GHG_colors["CH4"])
y_pos=y_pos + gases_resume_pos.loc["biogas positive","CH4"]
ax1.bar("biogas", gases_resume_pos.loc["biogas positive","N2O"], bottom=y_pos, label="N2O", color=GHG_colors["N2O"])
y_pos=y_pos + gases_resume_pos.loc["biogas positive","N2O"]
ax1.bar("biogas", gases_resume_pos.loc["biogas positive","N2O indirect"], bottom=y_pos, label="N2O indirect", color=GHG_colors["N2O indirect"])
y_pos=y_pos + gases_resume_pos.loc["biogas positive","N2O indirect"]
ax1.bar("biogas", gases_resume_pos.loc["biogas positive","NOx"], bottom=y_pos, label="NOx", color=GHG_colors["NOx"])
y_pos=y_pos + gases_resume_pos.loc["biogas positive","NOx"]
   
y_pos=0
for gas in gases:  
    ax1.bar("biogas", gases_resume_neg[gas][0], bottom=y_pos,  label=str(gas), color=GHG_colors[gas])
    y_pos=y_pos+gases_resume_neg[gas][0]

###
processes=list(pretreatmentbiogas.index)
gases=list(pretreatmentbiogas.columns)
gases=["CO2 biogenic", "CO2 fossil", "CH4", "N2O", "N2O indirect", "NOx"]

gases_resume_pos=pd.DataFrame()
for gas in gases:
    gases_resume_pos.loc["pretreatment and biogas positive", gas]=pretreatmentbiogas.loc[pretreatmentbiogas[gas]>0,gas].sum()
gases_resume_pos

gases_resume_neg=pd.DataFrame()
for gas in gases:
    gases_resume_neg.loc["pretreatment and biogas negative", gas]=pretreatmentbiogas.loc[pretreatmentbiogas[gas]<0,gas].sum()

gases_resume_neg=gases_resume_neg[["CO2 biogenic", "CO2 fossil", "CH4", "N2O", "N2O indirect", "NOx"]]
gases_resume_neg

y_pos=0
ax1.bar("pretreatment and biogas", gases_resume_pos.loc["pretreatment and biogas positive","CO2 biogenic"], bottom=y_pos, label="CO2 biogenic", color=GHG_colors["CO2 biogenic"])
y_pos=y_pos + gases_resume_pos.loc["pretreatment and biogas positive","CO2 biogenic"]
ax1.bar("pretreatment and biogas", gases_resume_pos.loc["pretreatment and biogas positive","CO2 fossil"], bottom=y_pos, label="CO2 fossil", color=GHG_colors["CO2 fossil"])
y_pos=y_pos + gases_resume_pos.loc["pretreatment and biogas positive","CO2 fossil"]
ax1.bar("pretreatment and biogas", replacement["CO2 fossil"], bottom=y_pos, label="CO2 fossil", color=GHG_colors["CO2 fossil"], hatch ="///", edgecolor='k')
y_pos=y_pos + replacement["CO2 fossil"]
ax1.bar("pretreatment and biogas", gases_resume_pos.loc["pretreatment and biogas positive","CH4"], bottom=y_pos, label="CH4", color=GHG_colors["CH4"])
y_pos=y_pos + gases_resume_pos.loc["pretreatment and biogas positive","CH4"]
ax1.bar("pretreatment and biogas", gases_resume_pos.loc["pretreatment and biogas positive","N2O"], bottom=y_pos, label="N2O", color=GHG_colors["N2O"])
y_pos=y_pos + gases_resume_pos.loc["pretreatment and biogas positive","N2O"]
ax1.bar("pretreatment and biogas", gases_resume_pos.loc["pretreatment and biogas positive","N2O indirect"], bottom=y_pos, label="N2O indirect", color=GHG_colors["N2O indirect"])
y_pos=y_pos + gases_resume_pos.loc["pretreatment and biogas positive","N2O indirect"]
ax1.bar("pretreatment and biogas", gases_resume_pos.loc["pretreatment and biogas positive","NOx"], bottom=y_pos, label="NOx", color=GHG_colors["NOx"])
y_pos=y_pos + gases_resume_pos.loc["pretreatment and biogas positive","NOx"]

y_pos=0
for gas in gases:  
    ax1.bar("pretreatment and biogas", gases_resume_neg[gas][0], bottom=y_pos,  label=str(gas), color=GHG_colors[gas])
    y_pos=y_pos+gases_resume_neg[gas][0]

###
processes=list(biogas.index)
gases=list(biogas.columns)

processes_resume_pos=pd.DataFrame()
for process in processes:
    processes_resume_pos.loc["biogas positive", process]=sum (x for x in biogas.loc[process].values if x>0)
processes_resume_pos

processes_resume_neg=pd.DataFrame()
for process in processes:
    processes_resume_neg.loc["biogas negative", process]=sum (x for x in biogas.loc[process].values if x<0)
processes_resume_neg

y_pos=0
for process in processes:
    ax2.bar("biogas", processes_resume_pos[process][0], bottom=y_pos,  label=str(process), color=process_colors[process])
    y_pos=y_pos + processes_resume_pos[process][0]

ax2.bar("biogas", replacement_positiv.values.sum(), bottom=y_pos, label="compost reaplecement with peat", color=process_colors["compost replacement with peat"], hatch ="///", edgecolor='k' )

y_pos=0
for process in processes:
    ax2.bar("biogas", processes_resume_neg[process][0], bottom=y_pos,  label=str(process),color=process_colors[process])
    y_pos=y_pos + processes_resume_neg[process][0]
ax2.bar("biogas", replacement_negativ.values.sum(), bottom=y_pos, label="compost reaplecement with peat", color=process_colors["compost replacement with peat"], hatch ="///", edgecolor='k' )

###

processes=list(pretreatmentbiogas.index)
gases=list(pretreatmentbiogas.columns)

processes_resume_pos=pd.DataFrame()
for process in processes:
    processes_resume_pos.loc["pretreatment and biogas positive", process]=sum (x for x in pretreatmentbiogas.loc[process].values if x>0)
processes_resume_pos

processes_resume_neg=pd.DataFrame()
for process in processes:
    processes_resume_neg.loc["pretreatment and biogas negative", process]=sum (x for x in pretreatmentbiogas.loc[process].values if x<0)
processes_resume_neg

y_pos=0
for process in processes:
    ax2.bar("pretreatment and biogas", processes_resume_pos[process][0], bottom=y_pos,  label=str(process), color=process_colors[process])
    y_pos=y_pos + processes_resume_pos[process][0]

ax2.bar("pretreatment and biogas", replacement_positiv.values.sum(), bottom=y_pos, label="compost reaplecement with peat", color=process_colors["compost replacement with peat"], hatch ="///", edgecolor='k' )

y_pos=0
for process in processes:
    ax2.bar("pretreatment and biogas", processes_resume_neg[process][0], bottom=y_pos,  label=str(process),color=process_colors[process])
    y_pos=y_pos + processes_resume_neg[process][0]
ax2.bar("pretreatment and biogas", replacement_negativ.values.sum(), bottom=y_pos, label="compost reaplecement with peat", color=process_colors["compost replacement with peat"], hatch ="///", edgecolor='k' )


ax1.scatter("biogas", biogas.values.sum(), label="net emissions", marker="D", color="black", zorder=100)
ax1.scatter("biogas", biogas_reas_wr_GWP.values.sum(), label="net emissions\nwith replacement", marker="D", color="red", zorder=100)
ax1.scatter("pretreatment and biogas", pretreatmentbiogas.values.sum(), label="net emissions", marker="D", color="black", zorder=100)
ax1.scatter("pretreatment and biogas", biogasman_reas_wr_GWP.values.sum(), label="net emissions\nwith replacement", marker="D", color="red", zorder=100)

ax2.scatter("biogas", biogas.values.sum(), label="net emissions", marker="D", color="black", zorder=100)
ax2.scatter("biogas", biogas_reas_wr_GWP.values.sum(), label="net emissions\nwith replacement", marker="D", color="red", zorder=100)
ax2.scatter("pretreatment and biogas", pretreatmentbiogas.values.sum(), label="net emissions", marker="D", color="black", zorder=100)
ax2.scatter("pretreatment and biogas", biogasman_reas_wr_GWP.values.sum(), label="net emissions\nwith replacement", marker="D", color="red", zorder=100)
#ax1.arrow(0, biogas.values.sum(), 0, (biogas_reas_wr_GWP.values.sum()-biogas.values.sum()), head_width=0.1, head_length=0.5, ec="blue", fc="blue")
LegendWithoutDuplicate(ax1, fig, offset=(0.30,0.98)).for_fig()
LegendWithoutDuplicate(ax2, fig, offset=(0.72,0.98)).for_fig()
ax1.set_ylabel(ylabel="Kilograms of CO₂eq", labelpad=15, fontsize=16)
ax1.tick_params("both",labelsize=14)
ax2.tick_params("both",labelsize=14)
#fig.legend()
plt.show()


#########################################
#This plotting es for scenarios WITH peat replacement, PER TON OF LEAVES
#########################################

##########
biogas=biogas_reas_GWP
biogas=biogas[["CO2 biogenic", "CO2 fossil", "CH4", "N2O", "N2O indirect", "NOx"]]
biogas_perton=biogas/40260
biogas_perton

##########
pretreatmentbiogas=biogasman_reas_GWP
pretreatmentbiogas=pretreatmentbiogas[["CO2 biogenic", "CO2 fossil", "CH4", "N2O", "N2O indirect", "NOx"]]
pretreatmentbiogas_perton=pretreatmentbiogas/40260
pretreatmentbiogas_perton

##########
replacement=emissions_replace_gwp1
replacement=replacement.drop(["system"], axis=1)
replacement=replacement.rename(columns={"CO2":"CO2 fossil"})
replacement=replacement.set_index("item")
replacement_perton=replacement/40260
replacement_perton

replacement_positiv=replacement_perton.mul(replacement.gt(0))
replacement_positiv
replacement_negativ=replacement_perton.mul(replacement.lt(0))
replacement_negativ

#####################
fig, (ax1,ax2) = plt.subplots(ncols=2, sharey=True)
ax1.yaxis.grid(color='gray', linestyle='dashed')
ax2.yaxis.grid(color='gray', linestyle='dashed')
ax1.axhline(0, color='black', linestyle="--")
ax2.axhline(0, color='black', linestyle="--")

###gases
processes=list(biogas_perton.index)
gases=list(biogas_perton.columns)

gases_resume_pos=pd.DataFrame()
for gas in gases:
    gases_resume_pos.loc["biogas positive", gas]=biogas_perton.loc[biogas_perton[gas]>0,gas].sum()
gases_resume_pos

gases_resume_neg=pd.DataFrame()
for gas in gases:
    gases_resume_neg.loc["biogas negative", gas]=biogas_perton.loc[biogas_perton[gas]<0,gas].sum()
gases_resume_neg

y_pos=0
ax1.bar("biogas", gases_resume_pos.loc["biogas positive","CO2 biogenic"], bottom=y_pos, label="CO2 biogenic", color=GHG_colors["CO2 biogenic"])
y_pos=y_pos + gases_resume_pos.loc["biogas positive","CO2 biogenic"]
ax1.bar("biogas", gases_resume_pos.loc["biogas positive","CO2 fossil"], bottom=y_pos, label="CO2 fossil", color=GHG_colors["CO2 fossil"])
y_pos=y_pos + gases_resume_pos.loc["biogas positive","CO2 fossil"]
ax1.bar("biogas", replacement_perton["CO2 fossil"], bottom=y_pos, label="CO2 fossil", color=GHG_colors["CO2 fossil"], hatch ="///", edgecolor='k')
y_pos=y_pos + replacement_perton["CO2 fossil"]
ax1.bar("biogas", gases_resume_pos.loc["biogas positive","CH4"], bottom=y_pos, label="CH4", color=GHG_colors["CH4"])
y_pos=y_pos + gases_resume_pos.loc["biogas positive","CH4"]
ax1.bar("biogas", gases_resume_pos.loc["biogas positive","N2O"], bottom=y_pos, label="N2O", color=GHG_colors["N2O"])
y_pos=y_pos + gases_resume_pos.loc["biogas positive","N2O"]
ax1.bar("biogas", gases_resume_pos.loc["biogas positive","N2O indirect"], bottom=y_pos, label="N2O indirect", color=GHG_colors["N2O indirect"])
y_pos=y_pos + gases_resume_pos.loc["biogas positive","N2O indirect"]
ax1.bar("biogas", gases_resume_pos.loc["biogas positive","NOx"], bottom=y_pos, label="NOx", color=GHG_colors["NOx"])
y_pos=y_pos + gases_resume_pos.loc["biogas positive","NOx"]
   
y_pos=0
for gas in gases:  
    ax1.bar("biogas", gases_resume_neg[gas][0], bottom=y_pos,  label=str(gas), color=GHG_colors[gas])
    y_pos=y_pos+gases_resume_neg[gas][0]

###
processes=list(pretreatmentbiogas_perton.index)
gases=list(pretreatmentbiogas_perton.columns)
gases=["CO2 biogenic", "CO2 fossil", "CH4", "N2O", "N2O indirect", "NOx"]

gases_resume_pos=pd.DataFrame()
for gas in gases:
    gases_resume_pos.loc["pretreatment and biogas positive", gas]=pretreatmentbiogas_perton.loc[pretreatmentbiogas_perton[gas]>0,gas].sum()
gases_resume_pos

gases_resume_neg=pd.DataFrame()
for gas in gases:
    gases_resume_neg.loc["pretreatment and biogas negative", gas]=pretreatmentbiogas_perton.loc[pretreatmentbiogas_perton[gas]<0,gas].sum()

gases_resume_neg=gases_resume_neg[["CO2 biogenic", "CO2 fossil", "CH4", "N2O", "N2O indirect", "NOx"]]
gases_resume_neg

y_pos=0
ax1.bar("pretreatment and biogas", gases_resume_pos.loc["pretreatment and biogas positive","CO2 biogenic"], bottom=y_pos, label="CO2 biogenic", color=GHG_colors["CO2 biogenic"])
y_pos=y_pos + gases_resume_pos.loc["pretreatment and biogas positive","CO2 biogenic"]
ax1.bar("pretreatment and biogas", gases_resume_pos.loc["pretreatment and biogas positive","CO2 fossil"], bottom=y_pos, label="CO2 fossil", color=GHG_colors["CO2 fossil"])
y_pos=y_pos + gases_resume_pos.loc["pretreatment and biogas positive","CO2 fossil"]
ax1.bar("pretreatment and biogas", replacement_perton["CO2 fossil"], bottom=y_pos, label="CO2 fossil", color=GHG_colors["CO2 fossil"], hatch ="///", edgecolor='k')
y_pos=y_pos + replacement_perton["CO2 fossil"]
ax1.bar("pretreatment and biogas", gases_resume_pos.loc["pretreatment and biogas positive","CH4"], bottom=y_pos, label="CH4", color=GHG_colors["CH4"])
y_pos=y_pos + gases_resume_pos.loc["pretreatment and biogas positive","CH4"]
ax1.bar("pretreatment and biogas", gases_resume_pos.loc["pretreatment and biogas positive","N2O"], bottom=y_pos, label="N2O", color=GHG_colors["N2O"])
y_pos=y_pos + gases_resume_pos.loc["pretreatment and biogas positive","N2O"]
ax1.bar("pretreatment and biogas", gases_resume_pos.loc["pretreatment and biogas positive","N2O indirect"], bottom=y_pos, label="N2O indirect", color=GHG_colors["N2O indirect"])
y_pos=y_pos + gases_resume_pos.loc["pretreatment and biogas positive","N2O indirect"]
ax1.bar("pretreatment and biogas", gases_resume_pos.loc["pretreatment and biogas positive","NOx"], bottom=y_pos, label="NOx", color=GHG_colors["NOx"])
y_pos=y_pos + gases_resume_pos.loc["pretreatment and biogas positive","NOx"]

y_pos=0
for gas in gases:  
    ax1.bar("pretreatment and biogas", gases_resume_neg[gas][0], bottom=y_pos,  label=str(gas), color=GHG_colors[gas])
    y_pos=y_pos+gases_resume_neg[gas][0]

###
processes=list(biogas_perton.index)
gases=list(biogas_perton.columns)

processes_resume_pos=pd.DataFrame()
for process in processes:
    processes_resume_pos.loc["biogas positive", process]=sum (x for x in biogas_perton.loc[process].values if x>0)
processes_resume_pos

processes_resume_neg=pd.DataFrame()
for process in processes:
    processes_resume_neg.loc["biogas negative", process]=sum (x for x in biogas_perton.loc[process].values if x<0)
processes_resume_neg

y_pos=0
for process in processes:
    ax2.bar("biogas", processes_resume_pos[process][0], bottom=y_pos,  label=str(process), color=process_colors[process])
    y_pos=y_pos + processes_resume_pos[process][0]

ax2.bar("biogas", replacement_positiv.values.sum(), bottom=y_pos, label="compost reaplecement with peat", color=process_colors["compost replacement with peat"], hatch ="///", edgecolor='k' )

y_pos=0
for process in processes:
    ax2.bar("biogas", processes_resume_neg[process][0], bottom=y_pos,  label=str(process),color=process_colors[process])
    y_pos=y_pos + processes_resume_neg[process][0]
ax2.bar("biogas", replacement_negativ.values.sum(), bottom=y_pos, label="compost reaplecement with peat", color=process_colors["compost replacement with peat"], hatch ="///", edgecolor='k' )

###

processes=list(pretreatmentbiogas_perton.index)
gases=list(pretreatmentbiogas_perton.columns)

processes_resume_pos=pd.DataFrame()
for process in processes:
    processes_resume_pos.loc["pretreatment and biogas positive", process]=sum (x for x in pretreatmentbiogas_perton.loc[process].values if x>0)
processes_resume_pos

processes_resume_neg=pd.DataFrame()
for process in processes:
    processes_resume_neg.loc["pretreatment and biogas negative", process]=sum (x for x in pretreatmentbiogas_perton.loc[process].values if x<0)
processes_resume_neg

y_pos=0
for process in processes:
    ax2.bar("pretreatment and biogas", processes_resume_pos[process][0], bottom=y_pos,  label=str(process), color=process_colors[process])
    y_pos=y_pos + processes_resume_pos[process][0]

ax2.bar("pretreatment and biogas", replacement_positiv.values.sum(), bottom=y_pos, label="compost reaplecement with peat", color=process_colors["compost replacement with peat"], hatch ="///", edgecolor='k' )

y_pos=0
for process in processes:
    ax2.bar("pretreatment and biogas", processes_resume_neg[process][0], bottom=y_pos,  label=str(process),color=process_colors[process])
    y_pos=y_pos + processes_resume_neg[process][0]
ax2.bar("pretreatment and biogas", replacement_negativ.values.sum(), bottom=y_pos, label="compost reaplecement with peat", color=process_colors["compost replacement with peat"], hatch ="///", edgecolor='k' )


ax1.scatter("biogas", biogas_perton.values.sum(), label="net emissions", marker="D", color="black", zorder=100)
ax1.scatter("biogas", biogas_reas_wr_GWP.values.sum()/40260, label="net emissions\nwith replacement", marker="D", color="red", zorder=100)
ax1.scatter("pretreatment and biogas", pretreatmentbiogas_perton.values.sum(), label="net emissions", marker="D", color="black", zorder=100)
ax1.scatter("pretreatment and biogas", biogasman_reas_wr_GWP.values.sum()/40260, label="net emissions\nwith replacement", marker="D", color="red", zorder=100)

ax2.scatter("biogas", biogas_perton.values.sum(), label="net emissions", marker="D", color="black", zorder=100)
ax2.scatter("biogas", biogas_reas_wr_GWP.values.sum()/40260, label="net emissions\nwith replacement", marker="D", color="red", zorder=100)
ax2.scatter("pretreatment and biogas", pretreatmentbiogas_perton.values.sum(), label="net emissions", marker="D", color="black", zorder=100)
ax2.scatter("pretreatment and biogas", biogasman_reas_wr_GWP.values.sum()/40260, label="net emissions\nwith replacement", marker="D", color="red", zorder=100)
#ax1.arrow(0, biogas.values.sum(), 0, (biogas_reas_wr_GWP.values.sum()-biogas.values.sum()), head_width=0.1, head_length=0.5, ec="blue", fc="blue")
LegendWithoutDuplicate(ax1, fig, offset=(0.30,0.98)).for_fig()
LegendWithoutDuplicate(ax2, fig, offset=(0.72,0.98)).for_fig()
ax1.set_ylabel(ylabel="Kilograms of CO₂eq / ton of leaves", labelpad=15, fontsize=16)
ax1.tick_params("both",labelsize=14)
ax2.tick_params("both",labelsize=14) 
#fig.legend()
plt.show()


biogas_perton.values.sum()
biogas_reas_wr_GWP.values.sum()/40260

pretreatmentbiogas_perton.values.sum()
biogasman_reas_wr_GWP.values.sum()/40260