import start
import matplotlib.pyplot as plt
import pylab
import numpy as np
import pandas as pd

from simulation.data_management_and_plotting.subsetting import SubSetting
from simulation.data_management_and_plotting.biogenic_reassignment import biogenic_reassignment
from tools import prod, convert_molmasses
from analysis.downstream_chains import start_simulation
from mpl_axes_aligner import align
from analysis import simdict
import seaborn as sns

biogasmanconfig = {
    ### Main settings
    "simulation"   :{
        'name': "biogas",
        'type': "single",
        'reference_system' : "production.plant.street_trees.StreetTrees_1" # "reference_system" : "production.animal.poultry.BroilerFarm_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod('tree leaves', 34000, "ton"),
                'origin': "Berlin_mixture"
                },

        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I' : prod(('production.plant.street_trees.StreetTrees_1', 'tree leaves')),
                'road_length_km' : 29531
                },

        "transport.LandTransport_1" :{
                'I': prod(('production.plant.street_leaves_collection.StreetLeavesCollection_1', 'tree leaves')),
                'dist_km': 657,
                'type': "lorry, large size",
                'method':"LBE",
                'load_basis':"volume"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod(('transport.LandTransport_1', 'tree leaves')),
                'method' : "silobag",
                },

        "production.plant.pretreatment_leaves.PretreatmentLeaves_1" :{
                'I': prod(('material.storing.LeafSilageStorage_1', 'tree leaves')),
                'method' : "NaOH",
                },

        "material.biogas.BiogasPlant_1" :{
                'I': prod(('production.plant.pretreatment_leaves.PretreatmentLeaves_1', 'tree leaves')),
                'residue_storage_type': ["closed", "open"],
                'thermal_energy_use': [0.4, 0],
                'CH4_loss_conv_method': ["KTBL", "Liebetrau_2010.low", "Liebetrau_2010.high"]
                },

        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod(('material.biogas.BiogasPlant_1', 'biogas digestate')),
                'method': "default",
                'type' : "liquid",
                "incorporation" : "incorporation <= 4h", 
                "avg_temp":"10 °C"
                },

        "external.Electricity_1" :{
                'I': prod(('material.biogas.BiogasPlant_1', 'electrical energy'))
                },

        "external.Heating_1" :{
                'I': prod(('material.biogas.BiogasPlant_1', 'thermal energy'))
                },
    }
}

resultbioman = start_simulation(biogasmanconfig)
#resultbioman.input_output_plots_pdf(filename="testplot_bioman_2.pdf", path="C:/Users/AVargas/Desktop")
simdict(resultbioman)

#to pandas
emissions_biogasman=resultbioman["biogas"]["emissions"][0].to_pandas()
emissions_biogasman

emissions_biogasman_gwp=resultbioman["biogas"]["gwp"][0].loc[:,:,"GWP100"].to_pandas()
emissions_biogasman_gwp

#to csv
emissions_biogasman.to_csv("emissions_biogas.csv")
emissions_biogasman=pd.read_csv("emissions_biogas.csv")
emissions_biogasman=emissions_biogasman.fillna(0)
emissions_biogasman

emissions_biogasman_gwp.to_csv("emissions_biogas_gwp.csv")
emissions_biogasman_gwp=pd.read_csv("emissions_biogas_gwp.csv")
emissions_biogasman_gwp

#joining biogas+compost replacement
emissions_biogasman_wr=pd.concat([emissions_biogasman, emissions_replace1])
emissions_biogasman_wr

emissions_biogasman_wr_gwp=pd.concat([emissions_biogasman_gwp, emissions_replace_gwp1])
emissions_biogasman_wr_gwp
biogasman_reas_wr_GWP=biogenic_reassignment(emissions_biogasman_wr_gwp)
biogasman_reas_wr_GWP=SubSetting(dataframe=biogasman_reas_wr_GWP).biogas_scenario()
biogasman_reas_wr_GWP


#Composting scenario graphs, functions from plotting_template.py

biogasman_scenario_GHG=SubSetting(dataframe=emissions_biogasman).biogas_scenario()
biogasman_scenario_GHG
#horizontal_bar_plotting(dataframe=biogasman_scenario_GHG, type="GHG emissions", figuresize=(7,7))

biogasman_scenario_GWP=SubSetting(dataframe=emissions_biogasman_gwp).pretreatment_biogas_scenario()
biogasman_scenario_GWP
#horizontal_bar_plotting(dataframe=biogasman_scenario_GWP, type="GWP100", figuresize=(8,8))

#biogenic reassigment
biogasman_reas_GWP=biogenic_reassignment(emissions_biogasman_gwp)
biogasman_reas_GWP=SubSetting(dataframe=biogasman_reas_GWP).pretreatment_biogas_scenario()
biogasman_reas_GWP
biogasman_reas_GWP.info()

from pandas_profiling import ProfileReport

profile=ProfileReport(biogasman_reas_GWP, title="Pretreatment and biogas report")
type(profile)

biogasman_reas_GWP.values.sum()/1000
biogasman_reas_GWP.values.sum()/34000


#% of CO2 fossil in possitive emissions
biogasman_reas_GWP["CO2 fossil"].mul(biogasman_reas_GWP["CO2 fossil"].gt(0)).sum()/biogasman_reas_GWP.mul(biogasman_reas_GWP.gt(0)).sum().sum()*100

#largest possitive emissions
biogasman_reas_GWP.loc["digestate spreading"].mul(biogasman_reas_GWP.loc["digestate spreading"].gt(0)).sum()/biogasman_reas_GWP.mul(biogasman_reas_GWP.gt(0)).sum().sum()*100
biogasman_reas_GWP.loc["biogas production"].mul(biogasman_reas_GWP.loc["biogas production"].gt(0)).sum()/biogasman_reas_GWP.mul(biogasman_reas_GWP.gt(0)).sum().sum()*100
biogasman_reas_GWP.loc["silaging emissions"].mul(biogasman_reas_GWP.loc["silaging emissions"].gt(0)).sum()/biogasman_reas_GWP.mul(biogasman_reas_GWP.gt(0)).sum().sum()*100
biogasman_reas_GWP.loc["sodium hydroxide manufacture"].mul(biogasman_reas_GWP.loc["sodium hydroxide manufacture"].gt(0)).sum()/biogasman_reas_GWP.mul(biogasman_reas_GWP.gt(0)).sum().sum()*100

#largest negative emissions
biogasman_reas_GWP.loc["electricity use balance"].mul(biogasman_reas_GWP.loc["electricity use balance"].lt(0)).sum()/biogasman_reas_GWP.mul(biogasman_reas_GWP.lt(0)).sum().sum()*100
biogasman_reas_GWP.loc["heating"].mul(biogasman_reas_GWP.loc["heating"].lt(0)).sum()/biogasman_reas_GWP.mul(biogasman_reas_GWP.lt(0)).sum().sum()*100
biogasman_reas_GWP.loc["biogas production"].mul(biogasman_reas_GWP.loc["biogas production"].lt(0)).sum()/biogasman_reas_GWP.mul(biogasman_reas_GWP.lt(0)).sum().sum()*100

biogasman_reas_GWP.sum(axis=1)[1]/biogasman_reas_GWP.mul(biogasman_reas_GWP.gt(0)).sum().sum()*100

biogasman_reas_GWP.mul(biogasman_reas_GWP.gt(0)).sum().sum()

