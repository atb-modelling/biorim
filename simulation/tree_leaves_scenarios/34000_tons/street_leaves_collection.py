import start
import matplotlib.pyplot as plt
import pylab
import numpy as np
import pandas as pd


from simulation.data_management_and_plotting.scenarios_plotting import LegendWithoutDuplicate, AllScenariosBarplot
from simulation.data_management_and_plotting.subsetting import SubSetting
from simulation.data_management_and_plotting.biogenic_reassignment import biogenic_reassignment
from tools import prod, convert_molmasses
from analysis.downstream_chains import start_simulation
from mpl_axes_aligner import align
from analysis import simdict
import seaborn as sns

SLC_config = {
    ### Main settings
    "simulation"   :{
        'name': "stree leaves collection",
        'type': "all combinations",
        'reference_system' : "production.plant.street_trees.StreetTrees_1" # "reference_system" : "production.animal.poultry.BroilerFarm_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod('tree leaves', 40260, "ton"),
                'origin': "Berlin_mixture"
                },
    
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I' : prod(('production.plant.street_trees.StreetTrees_1', 'tree leaves')),
                'road_length_km' : 29531
                },     
    }
}

result_SLC = start_simulation(SLC_config)
simdict(result_SLC)

road_lenght=29531

#to pandas
emissions_SLC=result_SLC["stree leaves collection"]["emissions"][0].to_pandas()
emissions_SLC

emissions_SLC_gwp=result_SLC["stree leaves collection"]["gwp"][0].loc[:,:,"GWP100"].to_pandas()
emissions_SLC_gwp

#to csv
emissions_SLC.to_csv("emissions_SLC.csv")
emissions_SLC=pd.read_csv("emissions_SLC.csv")
emissions_SLC=emissions_SLC.fillna(0)
emissions_SLC

emissions_SLC_gwp.to_csv("emissions_SLC_gwp.csv")
emissions_SLC_gwp=pd.read_csv("emissions_SLC_gwp.csv")
emissions_SLC_gwp

#drop of carbon in leaves

emissions_SLC1=emissions_SLC.drop(index=0)
emissions_SLC1

emissions_SLC_gwp1=emissions_SLC_gwp.drop(index=0)
emissions_SLC_gwp1

SLC_scenario_GWP=SubSetting(dataframe=emissions_SLC_gwp1).SLC_process_subsetting()
SLC_scenario_GWP

####plotting
SLC_leaf_blower=emissions_SLC_gwp1.iloc[0:3]
SLC_leaf_blower=SLC_leaf_blower.drop(["system"], axis=1)
SLC_leaf_blower=SLC_leaf_blower.set_index("item")
SLC_leaf_blower=SLC_leaf_blower.fillna(0)
SLC_leaf_blower=SLC_leaf_blower.rename(columns={"CO2":"CO2 fossil"})
SLC_leaf_blower
SLC_leaf_blower_km=SLC_leaf_blower/road_lenght
SLC_leaf_blower_km

SLC_small_sweeper=emissions_SLC_gwp1.iloc[3:6]
SLC_small_sweeper=SLC_small_sweeper.drop(["system"], axis=1)
SLC_small_sweeper=SLC_small_sweeper.set_index("item")
SLC_small_sweeper=SLC_small_sweeper.rename(columns={"CO2":"CO2 fossil"})
SLC_small_sweeper_km=SLC_small_sweeper/road_lenght

SLC_large_sweeper=emissions_SLC_gwp1.iloc[6:9]
SLC_large_sweeper=SLC_large_sweeper.drop(["system"], axis=1)
SLC_large_sweeper=SLC_large_sweeper.set_index("item")
SLC_large_sweeper=SLC_large_sweeper.rename(columns={"CO2":"CO2 fossil"})
SLC_large_sweeper_km=SLC_large_sweeper/road_lenght

SLC_scenario_dict={"leaf blowers":SLC_leaf_blower, "small sweepers":SLC_small_sweeper, "large sweepers": SLC_large_sweeper}
SLC_scenario_dict_km={"leaf blowers":SLC_leaf_blower_km, "small sweepers":SLC_small_sweeper_km, "large sweepers": SLC_large_sweeper_km}

AllScenariosBarplot(SLC_scenario_dict, dict_scenario2=None, offset_left=(0.30,0.98), offset_right=(0.72,0.98), net_emissions=False).scenarios_barplot()
AllScenariosBarplot(SLC_scenario_dict_km, dict_scenario2=None, offset_left=(0.30,0.98), offset_right=(0.72,0.98), net_emissions=False).scenarios_barplot(per_km=True)