import start
import matplotlib.pyplot as plt
import pylab
import numpy as np
import pandas as pd


from simulation.data_management_and_plotting.subsetting import SubSetting
from simulation.data_management_and_plotting.biogenic_reassignment import biogenic_reassignment
from tools import prod, convert_molmasses
from analysis.timestep_chains import start_simulation
from mpl_axes_aligner import align
from analysis import simdict
import seaborn as sns

biogasconfig = {
    ### Main settings
    "simulation"   :{
        'name': "biogas",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 34000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
    
        "transport.LandTransport" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'dist_km': 657,
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransport'),
                'CHP_kW':3800,
                'labour_costs':"FNR_2016"
                },
                
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='material.biogas.BiogasPlant_1'),
                'method': "default",
                'type' : "liquid",
                "incorporation" : "incorporation <= 4h", 
                "avg_temp":"10 °C"
                },
        
        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}


resultbio = start_simulation(biogasconfig)
#resultbio.input_output_plots_pdf(filename="testplot_bio_4.pdf", path="C:/Users/AVargas/Desktop")
simdict(resultbio)
resultbio["biogas"]["gwp"][0].loc[:,:,"GWP100"]


#to pandas
emissions_biogas=resultbio["biogas"]["emissions"][0].to_pandas()
emissions_biogas

emissions_biogas_gwp=resultbio["biogas"]["gwp"][0].loc[:,:,"GWP100"].to_pandas()
emissions_biogas_gwp

#to csv
emissions_biogas.to_csv("emissions_biogas.csv")
emissions_biogas=pd.read_csv("emissions_biogas.csv")
emissions_biogas=emissions_biogas.fillna(0)
emissions_biogas

emissions_biogas_gwp.to_csv("emissions_biogas_gwp.csv")
emissions_biogas_gwp=pd.read_csv("emissions_biogas_gwp.csv")
emissions_biogas_gwp

#joining biogas+compost replacement
emissions_biogas_wr=pd.concat([emissions_biogas, emissions_replace])
emissions_biogas_wr

emissions_biogas_wr_gwp=pd.concat([emissions_biogas_gwp, emissions_replace_gwp1])
emissions_biogas_wr_gwp
biogas_reas_wr_GWP=biogenic_reassignment(emissions_biogas_wr_gwp)
biogas_reas_wr_GWP=SubSetting(dataframe=biogas_reas_wr_GWP).biogas_scenario()
biogas_reas_wr_GWP

#Composting scenario graphs, functions from plotting_template.py

biogas_scenario_GHG=SubSetting(dataframe=emissions_biogas).biogas_scenario()
biogas_scenario_GHG
#horizontal_bar_plotting(dataframe=biogas_scenario_GHG, type="GHG emissions", figuresize=(7,7))

biogas_scenario_GWP=SubSetting(dataframe=emissions_biogas_gwp).biogas_scenario()
biogas_scenario_GWP
#horizontal_bar_plotting(dataframe=biogas_scenario_GWP, type="GWP100", figuresize=(8,8))

#biogenic reassigment
biogas_reas_GWP=biogenic_reassignment(emissions_biogas_gwp)
biogas_reas_GWP=SubSetting(dataframe=biogas_reas_GWP).biogas_scenario()
biogas_reas_GWP

biogas_reas_GWP.values.sum()/1000
biogas_reas_GWP.values.sum()/34000

biogas_reas_GWP["NOx"].sum()

font=(16,14)
font[1]

biogas_reas_GWP.mul(biogas_reas_GWP.gt(0)).sum().sum()

#% of decay compared to toal CO2 in leaves
biogas_reas_GWP.loc["leaf decay emissions","CO2 biogenic"]*12/44
biogas_reas_GWP.loc["leaf decay emissions","CO2 biogenic"]/biogas_reas_GWP.loc["carbon in leaves","CO2 biogenic"]*-1*100

#% CO2 fossil
biogas_reas_GWP["CO2 fossil"]
biogas_reas_GWP["CO2 fossil"].gt(0)
biogas_reas_GWP["CO2 fossil"].mul(biogas_reas_GWP["CO2 fossil"].gt(0)).sum()/biogas_reas_GWP.mul(biogas_reas_GWP.gt(0)).sum().sum()*100

#largest possitive emissions
biogas_reas_GWP.loc["digestate spreading"].mul(biogas_reas_GWP.loc["digestate spreading"].gt(0)).sum()/biogas_reas_GWP.mul(biogas_reas_GWP.gt(0)).sum().sum()*100
biogas_reas_GWP.loc["biogas production"].mul(biogas_reas_GWP.loc["biogas production"].gt(0)).sum()/biogas_reas_GWP.mul(biogas_reas_GWP.gt(0)).sum().sum()*100
biogas_reas_GWP.loc["leaf decay emissions"].mul(biogas_reas_GWP.loc["leaf decay emissions"].gt(0)).sum()/biogas_reas_GWP.mul(biogas_reas_GWP.gt(0)).sum().sum()*100

#largest negative emissions
biogas_reas_GWP.loc["electricity use balance"].mul(biogas_reas_GWP.loc["electricity use balance"].lt(0)).sum()/biogas_reas_GWP.mul(biogas_reas_GWP.lt(0)).sum().sum()*100
biogas_reas_GWP.loc["heating"].mul(biogas_reas_GWP.loc["heating"].lt(0)).sum()/biogas_reas_GWP.mul(biogas_reas_GWP.lt(0)).sum().sum()*100
biogas_reas_GWP.loc["biogas production"].mul(biogas_reas_GWP.loc["biogas production"].lt(0)).sum()/biogas_reas_GWP.mul(biogas_reas_GWP.lt(0)).sum().sum()*100















