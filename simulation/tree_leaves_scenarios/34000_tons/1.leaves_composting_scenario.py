from read import datapath
import start
import matplotlib.pyplot as plt
import pylab
import numpy as np
import pandas as pd

from tools import prod
from analysis.timestep_chains import start_simulation
from analysis import simdict
from simulation.data_management_and_plotting.subsetting import SubSetting
from simulation.data_management_and_plotting.biogenic_reassignment import biogenic_reassignment

compostingconfig = {
    ### Main settings
    "simulation"   :{
        'name': "composting",
        'type': "all combinations", 
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 34000, "ton"),
                'origin': "Berlin_mixture"
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        #NOTE: Composting to Hennickendorf
        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[122]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.composting.CompostingLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "Andersen_2010",
                'CH4_emis': 0.02, 
                'land_purchase':[False]
                },

        "production.plant.fertilization.CompostSpreading_1" :{
                'I': prod('leaves compost', 100, "%", source='transport.LandTransport_1'),
                'maturity' : "very mature",
                'method' : "VDLUFA"
                },
    }
}



#######


resultcomp= start_simulation(compostingconfig)
simdict(resultcomp)


resultcomp["composting"]["emissions"][0]
#to pandas
emissions_composting=resultcomp["composting"]["t1"][0]["emissions"]
emissions_composting

emissions_composting_gwp=resultcomp["composting"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas()
emissions_composting_gwp

#to csv
# emissions_composting.to_csv("emissions_composting.csv")
# emissions_composting=pd.read_csv("emissions_composting.csv")
# emissions_composting

# emissions_composting_gwp.to_csv("emissions_composting_gwp.csv")
# emissions_composting_gwp=pd.read_csv("emissions_composting_gwp.csv")
# emissions_composting_gwp

#Streetleaves collection graphs, functions from plotting_template.py

# SLC_GHG=SubSetting(dataframe=emissions_composting).SLC_subsetting()
# horizontal_bar_plotting(dataframe=SLC_GHG, type="GHG emissions", figuresize=(8,8))

# SLC_process_GHG=SubSetting(dataframe=emissions_composting).SLC_process_subsetting()
# horizontal_bar_plotting(dataframe=SLC_process_GHG, type="GHG emissions",figuresize=(8,8))

# SLC_GWP=SubSetting(dataframe=emissions_composting_gwp).SLC_subsetting()
# horizontal_bar_plotting(dataframe=SLC_GWP, type="GWP100")

# SLC_process_GWP=SubSetting(dataframe=emissions_composting_gwp).SLC_process_subsetting()
# SLC_process_GWP
# horizontal_bar_plotting(dataframe=SLC_process_GWP, type="GWP100",figuresize=(8,8))

#Composting scenario graphs, functions from plotting_template.py

composting_scenario_GHG=SubSetting(dataframe=emissions_composting).composting_scenario()
#horizontal_bar_plotting(dataframe=composting_scenario_GHG, type="GHG emissions",figuresize=(8,8))

composting_scenario_GWP=SubSetting(dataframe=emissions_composting_gwp).composting_scenario()
composting_scenario_GWP_perunit=composting_scenario_GWP/34000
composting_scenario_GWP_perunit
#horizontal_bar_plotting(dataframe=composting_scenario_GWP, type="GWP100", figuresize=(8,8))

#biogenic reassigment
composting_reas_GWP=biogenic_reassignment(emissions_composting_gwp)
composting_reas_GWP=SubSetting(dataframe=composting_reas_GWP).composting_scenario()
composting_reas_GWP

composting_reas_GWP.values.sum()/1000
composting_reas_GWP.values.sum()/34000

composting_reas_GWP["NOx"].sum()

composting_reas_GWP.mul(composting_reas_GWP.gt(0)).sum().sum()


composting_reas_GWP.loc["street leaves collection"].sum()/composting_reas_GWP.mul(composting_reas_GWP.gt(0)).sum().sum()

composting_reas_GWP.loc["leaves composting"].sum()/composting_reas_GWP.mul(composting_reas_GWP.gt(0)).sum().sum()*100


