import start
import matplotlib.pyplot as plt
import pylab
import numpy as np
import pandas as pd

from simulation.data_management_and_plotting.subsetting import SubSetting
from simulation.data_management_and_plotting.biogenic_reassignment import biogenic_reassignment
from tools import prod, convert_molmasses
from analysis.downstream_chains import start_simulation
from mpl_axes_aligner import align
from analysis import simdict
import seaborn as sns

biogasmanconfig_58 = {
    ### Main settings
    "simulation"   :{
        'name': "biogas",
        'type': "single",
        'reference_system' : "production.plant.street_trees.StreetTrees_1" # "reference_system" : "production.animal.poultry.BroilerFarm_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod('tree leaves', 58000, "ton"),
                'origin': "Berlin_mixture"
                },

        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I' : prod(('production.plant.street_trees.StreetTrees_1', 'tree leaves')),
                'road_length_km' : 29531
                },

        "transport.LandTransport_1" :{
                'I': prod(('production.plant.street_leaves_collection.StreetLeavesCollection_1', 'tree leaves')),
                'dist_km': 657,
                'type': "lorry, large size",
                'method':"LBE",
                'load_basis':"volume"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod(('transport.LandTransport_1', 'tree leaves')),
                'method' : "silobag",
                },

        "production.plant.pretreatment_leaves.PretreatmentLeaves_1" :{
                'I': prod(('material.storing.LeafSilageStorage_1', 'tree leaves')),
                'method' : "NaOH",
                },

        "material.biogas.BiogasPlant_1" :{
                'I': prod(('production.plant.pretreatment_leaves.PretreatmentLeaves_1', 'tree leaves')),
                'residue_storage_type': ["closed", "open"],
                'thermal_energy_use': [0.4, 0],
                'CH4_loss_conv_method': ["KTBL", "Liebetrau_2010.low", "Liebetrau_2010.high"]
                },

        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod(('material.biogas.BiogasPlant_1', 'biogas digestate')),
                'method': "default",
                'type' : "liquid",
                "incorporation" : "incorporation <= 4h", 
                "avg_temp":"10 °C"
                },

        "external.Electricity_1" :{
                'I': prod(('material.biogas.BiogasPlant_1', 'electrical energy'))
                },

        "external.Heating_1" :{
                'I': prod(('material.biogas.BiogasPlant_1', 'thermal energy'))
                },
    }
}

resultbioman_58 = start_simulation(biogasmanconfig_58)
simdict(resultbioman_58)

#to pandas
emissions_biogasman_58=resultbioman_58["biogas"]["emissions"][0].to_pandas()
emissions_biogasman_58

emissions_biogasman_gwp_58=resultbioman_58["biogas"]["gwp"][0].loc[:,:,"GWP100"].to_pandas()
emissions_biogasman_gwp_58

#to csv
emissions_biogasman_58.to_csv("emissions_biogas.csv")
emissions_biogasman_58=pd.read_csv("emissions_biogas.csv")
emissions_biogasman_58=emissions_biogasman_58.fillna(0)
emissions_biogasman_58

emissions_biogasman_gwp_58.to_csv("emissions_biogas_gwp.csv")
emissions_biogasman_gwp_58=pd.read_csv("emissions_biogas_gwp.csv")
emissions_biogasman_gwp_58

#joining biogas+compost replacement
emissions_biogasman_wr_58=pd.concat([emissions_biogasman_58, emissions_replace1_58])
emissions_biogasman_wr_58

emissions_biogasman_wr_gwp_58=pd.concat([emissions_biogasman_gwp_58, emissions_replace_gwp1_58])
emissions_biogasman_wr_gwp_58
biogasman_reas_wr_GWP_58=biogenic_reassignment(emissions_biogasman_wr_gwp_58)
biogasman_reas_wr_GWP_58=SubSetting(dataframe=biogasman_reas_wr_GWP_58).biogas_scenario()
biogasman_reas_wr_GWP_58


#Composting scenario graphs, functions from plotting_template.py

biogasman_scenario_GHG_58=SubSetting(dataframe=emissions_biogasman_58).biogas_scenario()
biogasman_scenario_GHG_58
#horizontal_bar_plotting(dataframe=biogasman_scenario_GHG, type="GHG emissions", figuresize=(7,7))

biogasman_scenario_GWP_58=SubSetting(dataframe=emissions_biogasman_gwp_58).pretreatment_biogas_scenario()
biogasman_scenario_GWP_58
#horizontal_bar_plotting(dataframe=biogasman_scenario_GWP, type="GWP100", figuresize=(8,8))

#biogenic reassigment
biogasman_reas_GWP_58=biogenic_reassignment(emissions_biogasman_gwp_58)
biogasman_reas_GWP_58=SubSetting(dataframe=biogasman_reas_GWP_58).pretreatment_biogas_scenario()
biogasman_reas_GWP_58
biogasman_reas_GWP_58.info()

from pandas_profiling import ProfileReport

profile=ProfileReport(biogasman_reas_GWP_58, title="Pretreatment and biogas report")
type(profile)

biogasman_reas_GWP_58.values.sum()/58000


#% of CO2 fossil in possitive emissions
biogasman_reas_GWP_58["CO2 fossil"].mul(biogasman_reas_GWP_58["CO2 fossil"].gt(0)).sum()/biogasman_reas_GWP_58.mul(biogasman_reas_GWP_58.gt(0)).sum().sum()*100

#largest possitive emissions
biogasman_reas_GWP_58.loc["digestate spreading"].mul(biogasman_reas_GWP_58.loc["digestate spreading"].gt(0)).sum()/biogasman_reas_GWP_58.mul(biogasman_reas_GWP_58.gt(0)).sum().sum()*100
biogasman_reas_GWP_58.loc["biogas production"].mul(biogasman_reas_GWP_58.loc["biogas production"].gt(0)).sum()/biogasman_reas_GWP_58.mul(biogasman_reas_GWP_58.gt(0)).sum().sum()*100
biogasman_reas_GWP_58.loc["silaging emissions"].mul(biogasman_reas_GWP_58.loc["silaging emissions"].gt(0)).sum()/biogasman_reas_GWP_58.mul(biogasman_reas_GWP_58.gt(0)).sum().sum()*100
biogasman_reas_GWP_58.loc["sodium hydroxide manufacture"].mul(biogasman_reas_GWP_58.loc["sodium hydroxide manufacture"].gt(0)).sum()/biogasman_reas_GWP_58.mul(biogasman_reas_GWP_58.gt(0)).sum().sum()*100

#largest negative emissions
biogasman_reas_GWP_58.loc["electricity use balance"].mul(biogasman_reas_GWP_58.loc["electricity use balance"].lt(0)).sum()/biogasman_reas_GWP_58.mul(biogasman_reas_GWP_58.lt(0)).sum().sum()*100
biogasman_reas_GWP_58.loc["heating"].mul(biogasman_reas_GWP_58.loc["heating"].lt(0)).sum()/biogasman_reas_GWP_58.mul(biogasman_reas_GWP_58.lt(0)).sum().sum()*100
biogasman_reas_GWP_58.loc["biogas production"].mul(biogasman_reas_GWP_58.loc["biogas production"].lt(0)).sum()/biogasman_reas_GWP_58.mul(biogasman_reas_GWP_58.lt(0)).sum().sum()*100

biogasman_reas_GWP_58.sum(axis=1)[1]/biogasman_reas_GWP_58.mul(biogasman_reas_GWP_58.gt(0)).sum().sum()*100

biogasman_reas_GWP_58.mul(biogasman_reas_GWP_58.gt(0)).sum().sum()

