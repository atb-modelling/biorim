import start
import matplotlib.pyplot as plt
import pylab
import numpy as np
import pandas as pd


from simulation.data_management_and_plotting.subsetting import SubSetting
from simulation.data_management_and_plotting.scenarios_plotting import AllScenariosBarplot
from tools import prod, convert_molmasses
from analysis.downstream_chains import start_simulation
from mpl_axes_aligner import align
from analysis import simdict
import seaborn as sns

replaceconfig = {
    ### Main settings
    "simulation"   :{
        'name': "compost replacement",
        'type': "all combinations",
        'reference_system' : "production.plant.street_trees.StreetTrees_1" # "reference_system" : "production.animal.poultry.BroilerFarm_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod('tree leaves', 40260, "ton"),
                'origin': "Berlin_mixture"
                },
    
        "production.plant.fertilization.CompostReplacement_1":{
                'I': prod(('production.plant.street_trees.StreetTrees_1', 'tree leaves'), 40260, "ton"),
                'ratio':0.9
                },      
    }
}

resultreplace = start_simulation(replaceconfig)
simdict(resultreplace)
resultreplace["compost replacement"].keys()
resultreplace["compost replacement"]["parameters"]
resultreplace["compost replacement"]["emissions"][0]

#to pandas
emissions_replace=resultreplace["compost replacement"]["emissions"][0].to_pandas()
emissions_replace

emissions_replace_gwp=resultreplace["compost replacement"]["gwp"][0].loc[:,:,"GWP100"].to_pandas()
emissions_replace_gwp

#to csv
emissions_replace.to_csv("emissions_replace.csv")
emissions_replace=pd.read_csv("emissions_replace.csv")
emissions_replace=emissions_replace.fillna(0)
emissions_replace

emissions_replace_gwp.to_csv("emissions_replace_gwp.csv")
emissions_replace_gwp=pd.read_csv("emissions_replace_gwp.csv")
emissions_replace_gwp

#drop plant trees
emissions_replace1=emissions_replace.drop(index=0)
emissions_replace1

emissions_replace_gwp1=emissions_replace_gwp.drop(index=0)
emissions_replace_gwp1





