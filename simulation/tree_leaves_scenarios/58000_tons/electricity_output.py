#this script is for displaying the electricity outputs from biogas and pretreatment & biogas scenarios
#it is recommended to use the names from the scenarios dictionary

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sbs
import numpy as np

from production.animal.ruminant import DairyFarm
from material.biogas import BiogasPlant
from tools import prod
from simulation.data_management_and_plotting.scenarios_plotting import LegendWithoutDuplicate

#outputs from biogas scenario and pretreatment & biogas scenarios
resultbio["biogas"][0]["outputs"][["Q","U"]]
outputs_biogas=resultbio["biogas"][0]["outputs"][["Q","U"]].to_csv("outputs_biogas.csv")
outputs_biogas=pd.read_csv("outputs_biogas.csv")
outputs_biogas=outputs_biogas.fillna(0)
outputs_biogas=outputs_biogas.set_index(["system", "item"])
outputs_biogas.loc[("material.biogas.BiogasPlant_1","electrical energy"),"Q"]
outputs_biogas.loc[("material.biogas.BiogasPlant_1","thermal energy"),"Q"]
outputs_biogas

resultbioman["biogas"][0]["outputs"][["Q","U"]]
outputs_biogas_man=resultbioman["biogas"][0]["outputs"][["Q","U"]].to_csv("outputs_biogas_man.csv")
outputs_biogas_man=pd.read_csv("outputs_biogas_man.csv")
outputs_biogas_man=outputs_biogas_man.fillna(0)
outputs_biogas_man=outputs_biogas_man.set_index(["system", "item"])
outputs_biogas_man.loc[("material.biogas.BiogasPlant_1","electrical energy"),"Q"]
outputs_biogas_man.loc[("material.biogas.BiogasPlant_1","thermal energy"),"Q"]
outputs_biogas_man

#comparison of total electricity and thermal energy output
eB=outputs_biogas.loc[("material.biogas.BiogasPlant_1","electrical energy"),"Q"]
eBM=outputs_biogas_man.loc[("material.biogas.BiogasPlant_1","electrical energy"),"Q"]
eBM/eB

hB=outputs_biogas.loc[("material.biogas.BiogasPlant_1","thermal energy"),"Q"]
hBM=outputs_biogas_man.loc[("material.biogas.BiogasPlant_1","thermal energy"),"Q"]
hBM/hB


#electrical output and net electricity
output_biogas_net_electricity=outputs_biogas.loc[("material.biogas.BiogasPlant_1","electrical energy"),"Q"]-outputs_biogas.loc[("external.Electricity","electrical energy"),"Q"]
elec_per_tonleaves_biogas=output_biogas_net_electricity/outputs_biogas.loc[("material.storing.LeafStorageContinuous_1","tree leaves"),"Q"]
elec_per_tonleaves_biogas


output_biogasman_net_electricity=outputs_biogas_man.loc[("material.biogas.BiogasPlant_1","electrical energy"),"Q"]-outputs_biogas.loc[("external.Electricity","electrical energy"),"Q"]
output_biogasman_net_electricity
consump_percapita= 5850
population=output_biogasman_net_electricity/consump_percapita
population

elec_per_tonleaves_biogasman=output_biogasman_net_electricity/outputs_biogas_man.loc[("production.plant.pretreatment_leaves.PretreatmentLeaves_1","tree leaves"),"Q"]
elec_per_tonleaves_biogasman

(elec_per_tonleaves_biogasman-elec_per_tonleaves_biogas)/elec_per_tonleaves_biogasman*100

#heating output
heating_biogas=outputs_biogas.loc[("material.biogas.BiogasPlant_1","thermal energy"),"Q"]
heating_biogas_man=outputs_biogas_man.loc[("material.biogas.BiogasPlant_1","thermal energy"),"Q"]

heating_biogas_net=heating_biogas/outputs_biogas.loc[("material.storing.LeafStorageContinuous_1","tree leaves"),"Q"]
heating_biogas_man_net=heating_biogas_man/outputs_biogas_man.loc[("production.plant.pretreatment_leaves.PretreatmentLeaves_1","tree leaves"),"Q"]
heating_biogas_net
heating_biogas_man_net

(heating_biogas_man_net-heating_biogas_net)/heating_biogas_net*100

#dataframe for electrical and thermal output
output_scenarios=pd.DataFrame(index=["electrical energy output","electrical energy input","heat delivery","net electricity/ton", "heat delivery/ton"], columns=["biogas","pretreatment & biogas"])

output_scenarios.loc["electrical energy output","biogas"]=outputs_biogas.loc[("material.biogas.BiogasPlant_1","electrical energy"),"Q"]
output_scenarios.loc["electrical energy input","biogas"]=outputs_biogas.loc[("external.Electricity","electrical energy"),"Q"]
output_scenarios.loc["heat delivery","biogas"]=heating_biogas
output_scenarios.loc["net electricity/ton","biogas"]=elec_per_tonleaves_biogas
output_scenarios.loc["heat delivery/ton","biogas"]=heating_biogas_net

output_scenarios.loc["electrical energy output","pretreatment & biogas"]=outputs_biogas_man.loc[("material.biogas.BiogasPlant_1","electrical energy"),"Q"]
output_scenarios.loc["electrical energy input","pretreatment & biogas"]=outputs_biogas_man.loc[("external.Electricity","electrical energy"),"Q"]
output_scenarios.loc["heat delivery","pretreatment & biogas"]=heating_biogas_man
output_scenarios.loc["net electricity/ton","pretreatment & biogas"]=elec_per_tonleaves_biogasman
output_scenarios.loc["heat delivery/ton","pretreatment & biogas"]=heating_biogas_man_net

output_scenarios.loc["electrical energy input"]=output_scenarios.loc["electrical energy input"]*[-1]
output_scenarios


# % of electricity per ton of leaves between scenarios
(output_scenarios.loc["net electricity/ton", "pretreatment & biogas"]-output_scenarios.loc["net electricity/ton", "biogas"])/output_scenarios.loc["net electricity/ton", "biogas"]*100
(output_scenarios.loc["heat delivery/ton", "pretreatment & biogas"]-output_scenarios.loc["heat delivery/ton", "biogas"])/output_scenarios.loc["heat delivery/ton", "biogas"]*100

# % of electricity output/input between scenarios
(output_scenarios.loc["electrical energy output", "pretreatment & biogas"]-output_scenarios.loc["electrical energy output", "biogas"])/output_scenarios.loc["electrical energy output", "biogas"]*100
(output_scenarios.loc["electrical energy input", "pretreatment & biogas"]-output_scenarios.loc["electrical energy input", "biogas"])/output_scenarios.loc["electrical energy input", "biogas"]*100


#plotting output per ton of leaves v2
labels=output_scenarios.index.to_list()
labels
scenarios=["biogas", "pretreatment & biogas"]
width=0.35
x_axis=np.arange(len(scenarios))
x_axis
import matplotlib.ticker as tkr
import matplotlib.pylab as pylab

def numfmt(x, pos): # your custom formatter function: divide by 1000000.0
    s = '{}'.format(x / 1000000)
    return s


fig=plt.figure()

ax1=fig.add_subplot(211)
ax1.bar(x=x_axis-width/2, height=output_scenarios.loc["electrical energy output"].to_list(), width=width, label="electrical energy production", color="green")
ax1.bar(x=x_axis-width/2, height=output_scenarios.loc["electrical energy input"].to_list(), width=width, label="electrical energy consumption", color="lime")
ax1.bar(x=x_axis+width/2, height=output_scenarios.loc["heat delivery"].to_list(), width=width, label="heat usage", color="orange")
ax1.set_ylabel(ylabel="GWh", labelpad=15, fontsize=16)
ax1.tick_params("both",labelsize=16)
yfmt = tkr.FuncFormatter(numfmt) 
pylab.gca().yaxis.set_major_formatter(yfmt)
ax1.set_xticks([0,1])
ax1.ticklabel_format(style="plain")
ax1.set_xticklabels(scenarios, fontsize=16)
ax1.axhline(y=0, linestyle="--", color="grey")

ax2=fig.add_subplot(212)
ax2.scatter(x=[0.25, 0.75], y=output_scenarios.loc["net electricity/ton"].to_list(), label="net electricity/ton", color="blue", marker="o", s=100)
ax2.scatter(x=[0.25, 0.75], y=output_scenarios.loc["heat delivery/ton"].to_list(), label="heat delivery/ton", color="purple", marker="o", s=100)
ax2.set_ylim(0,300)
ax2.tick_params("both",labelsize=16)
ax2.set_xticks([0,0.25, 0.75, 1])
ax2.set_xticklabels(["","biogas","pretreatment & biogas", ""], fontsize=16)
ax2.set_ylabel("kWh/ton leaves", labelpad=15, fontsize=16)

LegendWithoutDuplicate(ax1,fig).for_list_axes([ax1,ax2])
plt.show()



## plotting ouput in twin x
# fig, ax1 = plt.subplots()
# ax1.bar(x=x_axis-width/2, height=output_scenarios.loc["electrical energy output"].to_list(), width=width, label="electrical energy output", color="green")
# ax1.bar(x=x_axis-width/2, height=output_scenarios.loc["electrical energy input"].to_list(), width=width, label="electrical energy input", color="lime")
# ax1.bar(x=x_axis+width/2, height=output_scenarios.loc["heat delivery"].to_list(), width=width, label="heat delivery", color="orange")
# ax1.set_ylabel(ylabel="kWh", labelpad=15, fontsize=16)
# ax1.tick_params("both",labelsize=16)
# ax2=ax1.twinx()
# ax2.scatter(x=x_axis-width/2, y=output_scenarios.loc["net electricity/ton"].to_list(), label="net electricity/ton", color="blue", marker="o", s=100)
# ax2.scatter(x=x_axis+width/2, y=output_scenarios.loc["heat delivery/ton"].to_list(), label="heat delivery/ton", color="purple", marker="o", s=100)
# ax1.set_xticks([0,1])
# ax1.set_xticklabels(scenarios, fontsize=16)
# ax1.axhline(y=0, linestyle="--", color="grey")
# ax2.set_ylim(0,300)
# ax2.set_ylabel("kWh/ton leaves", rotation=270, labelpad=15, fontsize=16)
# ax2.tick_params("both",labelsize=16)
# LegendWithoutDuplicate(ax1,fig).for_list_axes([ax1,ax2])
# plt.show()


# #plotting output per ton of leaves
# labels=output_scenarios.index.to_list()
# fig, ax1 = plt.subplots()
# ax1.bar(x=output_scenarios.columns.to_list(), height=output_scenarios.loc["electrical energy output"].to_list(), label="electrical energy output", color="green")
# ax1.bar(x=output_scenarios.columns.to_list(), height=output_scenarios.loc["electrical energy input"].to_list(), label="electrical energy input", color="lime")
# ax1.bar(x=output_scenarios.columns.to_list(), height=output_scenarios.loc["heating"].to_list(), bottom= (output_scenarios.loc["electrical energy output"].to_list()) ,label="heating", color="orange")

# ax1.set_ylabel(ylabel="kWh", labelpad=15, fontsize=16)
# ax1.set_xticklabels(output_scenarios.columns.to_list(), fontsize=16)
# ax1.tick_params("both",labelsize=16)
# ax2=ax1.twinx()
# ax2.scatter(x=output_scenarios.columns.to_list(), y=output_scenarios.loc["net electricity/ton"].to_list(), label="net electricity/ton", color="blue", marker="o", s=100)
# ax2.scatter(x=output_scenarios.columns.to_list(), y=output_scenarios.loc["heat delivery/ton"].to_list(), label="heat delivery/ton", color="purple", marker="o", s=100)
# ax2.set_ylim(0,300)
# ax2.set_ylabel("kWh/ton leaves", rotation=270, labelpad=15, fontsize=16)
# ax2.tick_params("both",labelsize=16)
# #ax1.legend(labels, loc="upper left")
# #ax2.legend([labels[2]], loc="upper right") #warning: labels for legend HAVE to be a list, not set of strings. 
# #align.yaxes(ax1, 0, ax2, 0, 0.1)
# fig.legend(labels, ncol=3, loc=9, fontsize=16)
# plt.show()

#Plotting electricity per ton of feedstock mix ############
##definitions

output_scenarios

CowManure=DairyFarm(O = prod('cow manure, liquid', 111783, "ton")).manure()
CowManure
cowmanure_biogas_ouput=BiogasPlant(CowManure).output()
cowmanure_biogas_ouput

cowmanure_biogas_additional=BiogasPlant(CowManure).additional_input()
cowmanure_biogas_additional

cowmanure_electricity=pd.DataFrame(index=["electrical energy output", "electrical energy input", "heating", "net electricity/ton of mix", "heat delivery/ton of mix"], columns=["biogas", "pretreatment & biogas"])
cowmanure_electricity.loc["electrical energy output", "biogas"]=cowmanure_biogas_ouput.loc[("material.biogas.BiogasPlant","electrical energy"), "Q"]
cowmanure_electricity.loc["electrical energy output", "pretreatment & biogas"]=cowmanure_biogas_ouput.loc[("material.biogas.BiogasPlant","electrical energy"), "Q"]
cowmanure_electricity.loc["electrical energy input", "biogas"]=cowmanure_biogas_additional.loc[("external.Electricity","electrical energy"), "Q"]*-1
cowmanure_electricity.loc["electrical energy input", "pretreatment & biogas"]=cowmanure_biogas_additional.loc[("external.Electricity","electrical energy"), "Q"]*-1
cowmanure_electricity.loc["heating", "biogas"]=cowmanure_biogas_ouput.loc[("material.biogas.BiogasPlant","thermal energy"), "Q"]
cowmanure_electricity.loc["heating", "pretreatment & biogas"]=cowmanure_biogas_ouput.loc[("material.biogas.BiogasPlant","thermal energy"), "Q"]
cowmanure_electricity.loc["net electricity/ton of mix", "biogas"]=output_scenarios.loc["net electricity/ton", "biogas"]+cowmanure_biogas_ouput.loc[("material.biogas.BiogasPlant","electrical energy"), "Q"]/CowManure["Q"][0]
cowmanure_electricity.loc["net electricity/ton of mix", "pretreatment & biogas"]=output_scenarios.loc["net electricity/ton", "pretreatment & biogas"]+cowmanure_biogas_ouput.loc[("material.biogas.BiogasPlant","electrical energy"), "Q"]/CowManure["Q"][0]
cowmanure_electricity.loc["heat delivery/ton of mix", "biogas"]=output_scenarios.loc["heat delivery/ton", "biogas"]+cowmanure_biogas_ouput.loc[("material.biogas.BiogasPlant","thermal energy"), "Q"]/CowManure["Q"][0]
cowmanure_electricity.loc["heat delivery/ton of mix", "pretreatment & biogas"]=output_scenarios.loc["heat delivery/ton", "pretreatment & biogas"]+cowmanure_biogas_ouput.loc[("material.biogas.BiogasPlant","thermal energy"), "Q"]/CowManure["Q"][0]
cowmanure_electricity

##plotting
#labels=output_scenarios.index.to_list()
labels=["electricity output\ntree leaves", "electricity output\nliquid cow manure", 
        "electricity input\ntree leaves", "electricity input\nliquid cow manure",
        "heating from\ntree leaves", "heating from\nliquid cow manure",
        "net electricity/ton of leaves", "net electricity/ton of mix", 
        "heat delivery/ton of leaves", "heat delivery/ton of mix"]

fig, (ax1, ax3) = plt.subplots(ncols=2, sharey=True)
#electricity
ax1.bar(x=output_scenarios.columns.to_list(), height=output_scenarios.loc["electrical energy output"].to_list(), label="electricity output\ntree leaves", color="#196F3D")
ax1.bar(x=cowmanure_electricity.columns.to_list(), height=cowmanure_electricity.loc["electrical energy output"].to_list(), bottom=(output_scenarios.loc["electrical energy output"].to_list()), label="electricity output\nliquid cow manure", color="#224E1C" )
ax1.bar(x=output_scenarios.columns.to_list(), height=output_scenarios.loc["electrical energy input"].to_list(), label="electricity input\ntree leaves", color="#64D742")
ax1.bar(x=cowmanure_electricity.columns.to_list(), height=cowmanure_electricity.loc["electrical energy input"].to_list(), bottom=output_scenarios.loc["electrical energy input"].to_list(), label="electricity input\nliquid cow manure", color="#A6EB1B" )
ax1.set_ylabel(ylabel="kWh", labelpad=15, fontsize=16)
ax1.set_xticklabels(output_scenarios.columns.to_list(), fontsize=16)
ax1.tick_params("both",labelsize=16)
ax1.set_title("Electricity balance", fontsize=16)
ax2=ax1.twinx()
ax2.scatter(x=output_scenarios.columns.to_list(), y=output_scenarios.loc["net electricity/ton"].to_list(), label="net electricity/ton of leaves", color="red", marker="o", s=100)
ax2.scatter(x=output_scenarios.columns.to_list(), y=cowmanure_electricity.loc["net electricity/ton of mix"].to_list(), label="net electricity/ton of mix", color="red", marker="*", s=150)
ax2.set_ylim(0,300)
ax2.tick_params("both",labelsize=16)

#heating
ax3.bar(x=output_scenarios.columns.to_list(), height=output_scenarios.loc["heating"].to_list() ,label="heating from\ntree leaves", color="#EBE81B")
ax3.bar(x=output_scenarios.columns.to_list(), height=cowmanure_electricity.loc["heating"].to_list(), bottom= output_scenarios.loc["heating"].to_list(), label="heating from\nliquid cow manure", color="#EBA61B")
ax3.tick_params("both",labelsize=16)
ax3.set_title("Heating delivery", fontsize=16)
ax4=ax3.twinx()
ax4.scatter(x=output_scenarios.columns.to_list(), y=output_scenarios.loc["heat delivery/ton"].to_list(), label="heat delivery/ton of leaves", color="purple", marker="o", s=100)
ax4.scatter(x=output_scenarios.columns.to_list(), y=cowmanure_electricity.loc["heat delivery/ton of mix"].to_list(), label="heat delivery/ton of mix", color="purple", marker="*", s=150)
ax4.set_ylim(0,300)
ax4.tick_params("both",labelsize=16)
ax4.set_ylabel("kWh/ton of feedstock", rotation=270, labelpad=15, fontsize=16)
#ax1.legend(labels, loc="upper left")
#ax2.legend([labels[2]], loc="upper right") #warning: labels for legend HAVE to be a list, not set of strings. 
#align.yaxes(ax1, 0, ax2, 0, 0.1)
#LegendWithoutDuplicate(ax1, fig, offset=(0.4, 1.0), n_cols=3).for_fig()
#LegendWithoutDuplicate(ax2, fig, offset=(0.7, 1.0), n_cols=3).for_fig()
#(h1, l1)=ax1.get_legends_handles_labels()
fig.legend(labels, ncol=5, loc=9, fontsize=14)
plt.show()
