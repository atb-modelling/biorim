import start
import matplotlib.pyplot as plt
import pylab
import numpy as np
import pandas as pd

from tools import prod, convert_molmasses
from analysis.downstream_chains import start_simulation
from analysis import simdict
from simulation.ploting_template import SubSetting, horizontal_bar_plotting

biogaspreconfig = {
    ### Main settings
    "simulation"   :{
        'name': "biogaspre",
        'type': "all combinations",
        'reference_system' : "production.plant.street_trees.StreetTrees_1" # "reference_system" : "production.animal.poultry.BroilerFarm_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod('tree leaves', 40260, "ton")
                # 'B0_source' : "Hermann" # not working. Parameter not existing in current version
                },

        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I' : prod(('production.plant.street_trees.StreetTrees_1', 'tree leaves')),
                'road_length_km' : 29531
                },

        "transport.LandTransport_1" :{
                'I': prod(('production.plant.street_leaves_collection.StreetLeavesCollection_1', 'tree leaves')),
                'dist_km': 657,
                'type': "lorry, large size"
                },

        "material.storing.LeafStorageContinuous_1" :{
                'I': prod(('transport.LandTransport_1', 'tree leaves')),
                'daily_load' : 300,
                'time_period' : 90
                },

        "production.plant.pretreatment_leaves.PretreatmentLeaves_1" :{
                'I': prod(('material.storing.LeafStorageContinuous_1', 'tree leaves')),
                'method' : "NaOH",
                },                

        "material.biogas.BiogasPlant_1" :{
                'I': prod(('production.plant.pretreatment_leaves.PretreatmentLeaves_1', 'tree leaves')),
                'residue_storage_type': ["closed", "open"],
                'thermal_energy_use': [0.4, 0],
                'CH4_loss_conv_method': ["KTBL", "Liebetrau_2010.low", "Liebetrau_2010.high"]
                },
    }
}

resultbiopre = start_simulation(biogaspreconfig)
simdict(resultbiopre)
resultbiopre["biogaspre"].keys()
resultbiopre["biogaspre"]["parameters"]
resultbiopre["biogaspre"]["emissions"][0]

####
resultbiopre["biogaspre"][0]["outputs"]

result["dairy"][0]["outputs"][["Q","U"]]
result["dairy"]["emissions"]
result["dairy"][0]["emissions"]

#to pandas
emissions_biogaspre=resultbiopre["biogaspre"]["emissions"][0].to_pandas()
emissions_biogaspre

emissions_biogaspre_gwp=resultbiopre["biogaspre"]["gwp"][0].loc[:,:,"GWP100"].to_pandas()
emissions_biogaspre_gwp

#to csv
emissions_biogaspre.to_csv("emissions_biogaspre.csv")
emissions_biogaspre=pd.read_csv("emissions_biogaspre.csv")
emissions_biogaspre=emissions_biogaspre.fillna(0)
emissions_biogaspre

emissions_biogaspre_gwp.to_csv("emissions_biogaspre_gwp.csv")
emissions_biogaspre_gwp=pd.read_csv("emissions_biogaspre_gwp.csv")
emissions_biogaspre_gwp

#joining biogas+compost replacement
emissions_biogaspre=pd.concat([emissions_biogaspre, emissions_replace1])
emissions_biogaspre

emissions_biogaspre_gwp=pd.concat([emissions_biogaspre_gwp, emissions_replace_gwp1])
emissions_biogaspre_gwp

#Composting w/pretreatment scenario graphs, functions from plotting_template.py

biogaspre_scenario_GHG=SubSetting(dataframe=emissions_biogaspre).biogaspre_scenario()
horizontal_bar_plotting(dataframe=biogaspre_scenario_GHG, type="GHG emissions", figuresize=(7,7))

biogaspre_scenario_GWP=SubSetting(dataframe=emissions_biogaspre_gwp).biogaspre_scenario()
biogaspre_scenario_GWP
horizontal_bar_plotting(dataframe=biogaspre_scenario_GWP, type="GWP100", figuresize=(7,7))

###ouputs
resultbiopre["biogaspre"][0]["outputs"][["Q","U"]]
outputs_biogaspre=resultbiopre["biogaspre"][0]["outputs"][["Q","U"]].to_csv("outputs_biogaspre.csv")
outputs_biogaspre=pd.read_csv("outputs_biogaspre.csv")
outputs_biogaspre=outputs_biogaspre.fillna(0)
outputs_biogaspre=outputs_biogaspre.set_index(["system", "item"])
outputs_biogaspre.loc[("material.biogas.BiogasPlant_1","electrical energy"),"Q"]
outputs_biogaspre



