from read import datapath
import start
import matplotlib.pyplot as plt
import pylab
import numpy as np
import pandas as pd

from tools import prod
from analysis.downstream_chains import start_simulation
from analysis import simdict
from simulation.data_management_and_plotting.subsetting import SubSetting
from simulation.data_management_and_plotting.biogenic_reassignment import biogenic_reassignment

compostingconfig_58 = {
    ### Main settings
    "simulation"   :{
        'name': "composting",
        'type': "single",
        'reference_system' : "production.plant.street_trees.StreetTrees_1" # "reference_system" : "production.animal.poultry.BroilerFarm_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod('tree leaves', 58000, "ton")
                },

        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I' : prod(('production.plant.street_trees.StreetTrees_1', 'tree leaves')),
                'road_length_km' : 29531
                },

        "transport.LandTransport_1" :{
                'I': prod(('production.plant.street_leaves_collection.StreetLeavesCollection_1', 'tree leaves')),
                'dist_km': 122,
                'type': "lorry, large size",
                'method':"LBE",
                'load_basis':"volume"
                },

        "material.composting.CompostingLeaves_1" :{
                'I': prod(('transport.LandTransport_1', 'tree leaves')),
                'method' : "Andersen_2010",
                'CH4_emis': 0.02
                },

        "production.plant.fertilization.CompostSpreading_1" :{
                'I': prod(('material.composting.CompostingLeaves_1', 'leaves compost')),
                'maturity' : "very mature",
                'method' : "VDLUFA"
                },
    }
}

resultcomp_58= start_simulation(compostingconfig_58)
simdict(resultcomp_58)

#to pandas
emissions_composting_58=resultcomp_58["composting"]["emissions"][0].to_pandas()
emissions_composting_58

emissions_composting_gwp_58=resultcomp_58["composting"]["gwp"][0].loc[:,:,"GWP100"].to_pandas()
emissions_composting_gwp_58

#to csv
emissions_composting_58.to_csv("emissions_composting.csv")
emissions_composting_58=pd.read_csv("emissions_composting.csv")
emissions_composting_58

emissions_composting_gwp_58.to_csv("emissions_composting_gwp.csv")
emissions_composting_gwp_58=pd.read_csv("emissions_composting_gwp.csv")
emissions_composting_gwp_58

#Streetleaves collection graphs, functions from plotting_template.py

# SLC_GHG=SubSetting(dataframe=emissions_composting).SLC_subsetting()
# horizontal_bar_plotting(dataframe=SLC_GHG, type="GHG emissions", figuresize=(8,8))

# SLC_process_GHG=SubSetting(dataframe=emissions_composting).SLC_process_subsetting()
# horizontal_bar_plotting(dataframe=SLC_process_GHG, type="GHG emissions",figuresize=(8,8))

# SLC_GWP=SubSetting(dataframe=emissions_composting_gwp).SLC_subsetting()
# horizontal_bar_plotting(dataframe=SLC_GWP, type="GWP100")

# SLC_process_GWP=SubSetting(dataframe=emissions_composting_gwp).SLC_process_subsetting()
# SLC_process_GWP
# horizontal_bar_plotting(dataframe=SLC_process_GWP, type="GWP100",figuresize=(8,8))

#Composting scenario graphs, functions from plotting_template.py

composting_scenario_GHG_58=SubSetting(dataframe=emissions_composting_58).composting_scenario()
#horizontal_bar_plotting(dataframe=composting_scenario_GHG, type="GHG emissions",figuresize=(8,8))

composting_scenario_GWP_58=SubSetting(dataframe=emissions_composting_gwp_58).composting_scenario()
composting_scenario_GWP_perunit_58=composting_scenario_GWP_58/58000
composting_scenario_GWP_perunit_58
#horizontal_bar_plotting(dataframe=composting_scenario_GWP, type="GWP100", figuresize=(8,8))

#biogenic reassigment
composting_reas_GWP_58=biogenic_reassignment(emissions_composting_gwp_58)
composting_reas_GWP_58=SubSetting(dataframe=composting_reas_GWP_58).composting_scenario()
composting_reas_GWP_58

composting_reas_GWP_58.values.sum()/58000

composting_reas_GWP_58.mul(composting_reas_GWP_58.gt(0)).sum().sum()

composting_reas_GWP_58.loc["leaves composting"].sum()/composting_reas_GWP_58.mul(composting_reas_GWP_58.gt(0)).sum().sum()*100
