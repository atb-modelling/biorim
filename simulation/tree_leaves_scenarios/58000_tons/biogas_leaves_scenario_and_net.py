
import start
import matplotlib.pyplot as plt
import pylab
import numpy as np
import pandas as pd


from simulation.data_management_and_plotting.subsetting import SubSetting
from tools import prod, convert_molmasses
from analysis.downstream_chains import start_simulation
from mpl_axes_aligner import align
import seaborn as sns

biogasconfig = {
    ### Main settings
    "simulation"   :{
        'name': "biogas",
        'type': "all combinations",
        'reference_system' : "production.plant.street_trees.StreetTrees_1" # "reference_system" : "production.animal.poultry.BroilerFarm_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod('tree leaves', 40260, "ton")
                # 'B0_source' : "Hermann" # not working. Parameter not existing in current version
                },

        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I' : prod(('production.plant.street_trees.StreetTrees_1', 'tree leaves')),
                'road_length_km' : 29531
                },

        "transport.LandTransport_1" :{
                'I': prod(('production.plant.street_leaves_collection.StreetLeavesCollection_1', 'tree leaves')),
                'dist_km': 657,
                'type': "lorry, large size"
                },

        "material.storing.LeafStoragev2_1" :{
                'I': prod(('transport.LandTransport_1', 'tree leaves')),
                'daily_load' : 300,
                'time_period' : 90
                },

        "material.biogas.BiogasPlant_1" :{
                'I': prod(('material.storing.LeafStoragev2_1', 'tree leaves')),
                'residue_storage_type': ["closed", "open"],
                'thermal_energy_use': [0.4, 0],
                'CH4_loss_conv_method': ["KTBL", "Liebetrau_2010.low", "Liebetrau_2010.high"]
                },

        "external.Electricity_1" :{
                'I': prod(('material.biogas.BiogasPlant_1', 'electrical energy'))
                },

        "external.Heating_1" :{
                'I': prod(('material.biogas.BiogasPlant_1', 'thermal energy'))
                },
    }
}

resultbionet = start_simulation(biogasconfig)
simdict(resultbionet)
resultbio["biogas"].keys()
resultbio["biogas"]["parameters"]
resultbio["biogas"]["emissions"][0]

#to pandas
emissions_biogas=resultbionet["biogas"]["emissions"][0].to_pandas()
emissions_biogas_net

emissions_biogas_gwp=resultbionet["biogas"]["gwp"][0].loc[:,:,"GWP100"].to_pandas()
emissions_biogas_net_gwp

#to csv
emissions_biogas.to_csv("emissions_biogas.csv")
emissions_biogas=pd.read_csv("emissions_biogas.csv")
emissions_biogas=emissions_biogas.fillna(0)
emissions_biogas

emissions_biogas_gwp.to_csv("emissions_biogas_gwp.csv")
emissions_biogas_gwp=pd.read_csv("emissions_biogas_gwp.csv")
emissions_biogas_gwp

#Composting scenario graphs, functions from plotting_template.py

biogas_scenario_GHG=SubSetting(dataframe=emissions_biogas).biogas_scenario()
biogas_scenario_GHG
horizontal_bar_plotting(dataframe=biogas_scenario_GHG, type="GHG emissions", figuresize=(7,7))

biogas_scenario_GWP=SubSetting(dataframe=emissions_biogas_gwp).biogas_scenario()
biogas_scenario_GWP
horizontal_bar_plotting(dataframe=biogas_scenario_GWP, type="GWP100", figuresize=(8,8))

#outputs
resultbio["biogas"][0]["outputs"][["Q","U"]]
outputs_biogas=resultbio["biogas"][0]["outputs"][["Q","U"]].to_csv("outputs_biogas.csv")
outputs_biogas=pd.read_csv("outputs_biogas.csv")
outputs_biogas=outputs_biogas.fillna(0)
outputs_biogas=outputs_biogas.set_index(["system", "item"])
outputs_biogas.loc[("material.biogas.BiogasPlant_1","electrical energy"),"Q"]
outputs_biogas


output_biogas_net_electricity=outputs_biogas.loc[("material.biogas.BiogasPlant_1","electrical energy"),"Q"]-outputs_biogas.loc[("external.Electricity","electrical energy"),"Q"]
elec_per_tonleaves_biogas=output_biogas_net_electricity/outputs_biogas.loc[("material.storing.LeafStoragev2_1","tree leaves"),"Q"]
elec_per_tonleaves_biogas

output_biogaspre_net_electricity=outputs_biogaspre.loc[("material.biogas.BiogasPlant_1","electrical energy"),"Q"]-outputs_biogas.loc[("external.Electricity","electrical energy"),"Q"]
elec_per_tonleaves_biogaspre=output_biogaspre_net_electricity/outputs_biogaspre.loc[("production.plant.pretreatment_leaves.PretreatmentLeaves_1","tree leaves"),"Q"]
elec_per_tonleaves_biogaspre

#pandas for net electricity
net_electricity=pd.DataFrame(index=["electrical energy output","electrical energy input","net electricity/ton"], columns=["biogas scenario","biogas/pretreatment scenario"])
net_electricity.loc["electrical energy output","biogas scenario"]=outputs_biogas.loc[("material.biogas.BiogasPlant_1","electrical energy"),"Q"]
net_electricity.loc["electrical energy input","biogas scenario"]=outputs_biogas.loc[("external.Electricity","electrical energy"),"Q"]
net_electricity.loc["net electricity/ton","biogas scenario"]=elec_per_tonleaves_biogas

net_electricity.loc["electrical energy output","biogas/pretreatment scenario"]=outputs_biogaspre.loc[("material.biogas.BiogasPlant_1","electrical energy"),"Q"]
net_electricity.loc["electrical energy input","biogas/pretreatment scenario"]=outputs_biogaspre.loc[("external.Electricity","electrical energy"),"Q"]
net_electricity.loc["net electricity/ton","biogas/pretreatment scenario"]=elec_per_tonleaves_biogaspre

net_electricity.loc["electrical energy input"]=net_electricity.loc["electrical energy input"]*[-1]
net_electricity

#plotting electricity per ton of leaves (needs improvement)
labels=net_electricity.index.to_list()
fig, ax1 = plt.subplots()
ax1.bar(x=net_electricity.columns.to_list(), height=net_electricity.loc["electrical energy output"].to_list(), label="electrical energy output", color="green")
ax1.bar(x=net_electricity.columns.to_list(), height=net_electricity.loc["electrical energy input"].to_list(), label="electrical energy input", color="lime")
ax1.set_ylabel(ylabel="kWh", labelpad=15)
ax2=ax1.twinx()
ax2.scatter(x=net_electricity.columns.to_list(), y=net_electricity.loc["net electricity/ton"].to_list(), label="net electricity/ton", color="black", marker="D", s=60)
ax2.set_ylim(0,300)
ax2.set_ylabel("kWh/ton leaves", rotation=270, labelpad=15)
#ax1.legend(labels, loc="upper left")
#ax2.legend([labels[2]], loc="upper right") #warning: labels for legend HAVE to be a list, not set of strings. 
align.yaxes(ax1, 0, ax2, 0, 0.1)
fig.legend(labels, ncol=3, loc=8)
plt.show()






