"""
Example for timestep_chains adapted to consider expenditures
"""

import start
from tools import prod
from analysis.timestep_chains import start_simulation
from tools import convert_molmasses

config_biogas = {
    ### Main settings
    "simulation"   :{
        'name': "biogas",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 34000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransport_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'dist_km': 657,
                'type': "lorry, large size",
                'method':"LBE",
                'load_basis':"volume"
                },

        "material.storing.LeafStorageContinuous_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransport_1'),
                'daily_load' : 300
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='material.storing.LeafStorageContinuous_1')
                },
                
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='material.biogas.BiogasPlant_1')
                },
        
    }
}




# Example results
result_biogas = start_simulation(config_biogas)
result_biogas["biogas"]["t1"][0].keys()


result_biogas["biogas"]["t1"][0]["emissions"]
result_biogas["biogas"]["t1"][0]["emissions"]["CH4-C"]
convert_molmasses(result_biogas["biogas"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas())["CH4-C"]

result_biogas["biogas"]["t1"][0]["outputs"].loc[:,["Q","U"]]
result_biogas["biogas"]["t1"]["parameters"]

result_biogas["biogas"]["t1"][0]["cashflow"]
