import start
import pandas as pd

from tools import prod
from production.plant.street_trees import StreetTrees
from material.gasification import Gasification

from tools import convert_molmasses

Leaves = StreetTrees(O=prod("tree leaves", 1, "ton")).output()

Gasification(I=Leaves).emissions().drop(["construction"])

GasificationCEmis = Gasification(I=Leaves).emissions().drop(["construction"]).loc[:,["CO2-C", "CH4-C"]].sum().sum() #   97 kg of carbon
GasificationCEmis
Biochar = Gasification(I=Leaves).output().loc["unconverted biochar"]
Biochar

GasificationCOutput = Biochar["Q"] * Biochar["DM"] * Biochar["oDM"] * Biochar["orgC"] * 1000  # 8 kg

GasificationCEmis + GasificationCOutput # 106 kg, another 106 kg of carbon have disappeared


### Carbon accumulation in the tree leaves: Considered as negative emissions!
StreetTrees(O=prod("tree leaves", 1, "ton")).emissions()    # Every tonne of leaves is storing 212 kg of carbon

