import start
from tools import prod
from analysis.timestep_chains import start_simulation
from tools import convert_molmasses
from analysis.economic.macc import MACC


config_composting = {
    ### Main settings
    "simulation"   :{
        'name': "composting",
        'type': "all combinations", 
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 14000, "ton"),
                'origin': "Berlin_mixture"
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        #NOTE: Composting to Hennickendorf
        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[98]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.composting.CompostingLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "Andersen_2010",
                'CH4_emis': 0.02, 
                'land_purchase':[True, False]
                },

        "production.plant.fertilization.CompostSpreading_1" :{
                'I': prod('leaves compost', 100, "%", source='transport.LandTransport_1'),
                'maturity' : "very mature",
                'method' : "VDLUFA"
                },
    }
}

result_config_composting=start_simulation(config_composting)

# config_biogas_RHL_comp = {
#     ### Main settings
#     "simulation"   :{
#         'name': "biogas and composting",
#         'type': "all combinations", # NOTE: All possible parameter combinations are computed
#         'starting_system' : "production.plant.street_trees.StreetTrees_1"
#     },

#     ### Conducted analyses
#     "analyses": {
#         "emissions":{
#             "outtype": "molecule"
#         },
#         "gwp":{
#             "timerange": [20, 100]
#         },
#         "cashflows":{
#             "discount_rate": 0.07
#         },
#     },

#     "systems"   :{
#         "production.plant.street_trees.StreetTrees_1" :{
#                 'O' : prod("tree leaves", 15000, "ton"),
#                 'origin': "Berlin_mixture",
#                 },
        
#         "production.household.food_waste.FoodWaste_1" :{
#                 'O' : prod("food waste", 60000, "ton"),
#                 },
        
#         "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
#                 'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
#                 'road_length_km' : 15000
#                 },
        
#         "material.storing.LeafSilageStorage_1" :{
#                 'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
#                 'method' : "silobag",
#                 },

#         "transport.LandTransportMultiple_1" :{
#                 'I': prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
#                 'mass_allocation': [[0.167, 0.076, 0.086, 0.076, 0.166, 0.259, 0.167]],
#                 'dist_km': [[24, 15, 6, 15, 19, 23, 18]],
#                 'type_transp': "lorry, large size",
#                 'method':"LBE",
#                 'load_basis':"mass",
#                 'transport_cost':"sommer_elso"
#                 },

        
#         "material.biogas.BiogasPlant_1" :{
#                 'I':prod(['tree leaves', 'food waste'], [100,100], ["%","%"], source=['transport.LandTransportMultiple_1','production.household.food_waste.FoodWaste_1']),
#                 'CHP_kW':5000, #check variability in kW installed
#                 'calculation_approach': "operation per year",
#                 'land_purchase': [True, False], 
#                 'labour_costs':"FNR_2016"
#                 },
        
#         # "material.composting.CompostingLeaves_1" :{
#         #         'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
#         #         'method' : "Andersen_2010",
#         #         'CH4_emis': 0.02, 
#         #         'land_purchase':[True, False]
#         #         },

#         # "production.plant.fertilization.CompostSpreading_1" :{
#         #         'I': prod('leaves compost', 100, "%", source='transport.LandTransport_1'),
#         #         'maturity' : "very mature",
#         #         'method' : "VDLUFA"
#         #         },        
#     }
# }


config_biogas = {
    ### Main settings
    "simulation"   :{
        'name': "biogas",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 15000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.household.food_waste.FoodWaste_1" :{
                'O' : prod("food waste", 60000, "ton"),
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 15000
                },
        
        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'method' : "silobag",
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'mass_allocation': [[0.167, 0.076, 0.086, 0.076, 0.166, 0.259, 0.167]],
                'dist_km': [[24, 15, 6, 15, 19, 23, 18]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        
        "material.biogas.BiogasPlant_1" :{
                'I':prod(['tree leaves', 'food waste'], [100,100], ["%","%"], source=['transport.LandTransportMultiple_1','production.household.food_waste.FoodWaste_1']),
                'CHP_kW':6000,
                'land_purchase': [True, False], 
                'labour_costs':"FNR_2016"
                },
        
                
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='material.biogas.BiogasPlant_1')
                },
        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}        

result_config_biogas=start_simulation(config_biogas)

config_gasification = {
    ### Main settings
    "simulation"   :{
        'name': "gasification",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 34000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransport_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'dist_km': 150,
                'type': "lorry, large size",
                'method':"LBE",
                'load_basis':"volume"
                },

        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransport_1'),
                'kW':850,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': [True, False]
                },
          
    }
}

result_config_gasification=start_simulation(config_gasification)

#cofiring scenario
# config_cofiring = {
#     ### Main settings
#     "simulation"   :{
#         'name': "cofiring",
#         'type': "all combinations", # NOTE: All possible parameter combinations are computed
#         'starting_system' : "production.plant.street_trees.StreetTrees_1"
#     },

#     ### Conducted analyses
#     "analyses": {
#         "emissions":{
#             "outtype": "molecule"
#         },
#         "gwp":{
#             "timerange": [20, 100]
#         },
#         "cashflows":{
#             "discount_rate": 0.07
#         },
#     },

#     "systems"   :{
#         "production.plant.street_trees.StreetTrees_1" :{
#                 'O' : prod("tree leaves", 34000, "ton"),
#                 'origin': "Berlin_mixture",
#                 },
        
#         "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
#                 'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
#                 'road_length_km' : 29531
#                 },

#         "transport.LandTransportMultiple_1" :{
#                 'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
#                 'mass_allocation': [[1]],
#                 'dist_km': [[100]],
#                 'type_transp': "lorry, large size",
#                 'method':"LBE",
#                 'load_basis':"mass",
#                 'transport_cost':"sommer_elso"
#                 },

#         "material.cofiring.CoFiring_1" :{
#                 'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
#                 'kW':6000,
#                 'operation_hours':8000
#                 },
#     }
# }

config_pellet_gasification = {
    ### Main settings
    "simulation"   :{
        'name': "pelletization and gasification",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 34000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransport_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'dist_km': 150,
                'type': "lorry, large size",
                'method':"LBE",
                'load_basis':"volume"
                },

        "material.pelletization.PelletFactory_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransport_1'),
                },

        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves pellets', 100, "%", source='material.pelletization.PelletFactory_1'),
                'kW':850,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': [True, False]
                },
          
    }
}

result_config_pellet_gasification=start_simulation(config_pellet_gasification)


config_cofiring_retro_dry = {
    ### Main settings
    "simulation"   :{
        'name': "cofiring - retrofitting - dry",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 34000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'kW':450000,
                'operation_hours':8000, 
                'retrofitting': True
                },
    }
}

result_config_cofiring_retro_dry=start_simulation(config_cofiring_retro_dry)


config_cofiring_retro_wet = {
    ### Main settings
    "simulation"   :{
        'name': "cofiring - retrofitting - wet",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 34000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'kW':450000,
                'operation_hours':8000, 
                'retrofitting': True,
                'dry_biomass': False
                },
    }
}

result_config_cofiring_retro_wet=start_simulation(config_cofiring_retro_wet)


###test of MACC function
# list_scenarios=[config_composting, config_biogas_RHL, config_biogas_RHL_comp, config_gasification, config_cofiring]

list_scenarios=[result_config_composting, result_config_biogas, result_config_gasification, result_config_pellet_gasification, result_config_cofiring_retro_dry, result_config_cofiring_retro_wet]
#marginal abatement test

MACC(list_scenarios, BAU_scenario="composting").marginal_abatement_cost()
MAC_df=MACC(list_scenarios, BAU_scenario="composting").marginal_abatement_cost()
MAC_df

#graph
MACC(list_scenarios, BAU_scenario="composting").graphic(MAC_df, fixed_df=True)
