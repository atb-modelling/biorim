import start
import pandas as pd
import numpy as np
import numpy_financial as npf


cf_1=[-15000]+[5000]*9
cf_2=[-150000]+[20000]*9

cf_project_x=[-588000, 130000, 145000, 151000, (139000+280000)]
cf_project_x

cf_1
cf_2

len(cf_1)
len(cf_2)

npf.npv(0.07, cf_1)
npf.npv(0.07, cf_2)
npf.npv(0.12, cf_project_x)

#biogas example WITH investment
cf_biogas_invest_old=[-3699395.4]+[1835435.39]*19
cf_biogas_invest_new=[-3699395.4]+[1694213.423]*19

npf.npv(0.07, cf_biogas_invest_old)
npf.npv(0.07, cf_biogas_invest_new)

#biogas example NO investment
cf_biogas_old=[1835435.39]*20
cf_biogas_new=[1694213.423]*20

npf.npv(0.07, cf_biogas_old)
npf.npv(0.07, cf_biogas_new)

#biogas with refurbishment
cf_biogas_old_reinvestment=[1694213.423]*19+[-194578.02]
cf_biogas_new_reinvestment=[1694213.423]*9+[-194578.02]+[1500154.3]*10

npf.npv(0.07, cf_biogas_old_reinvestment)
npf.npv(0.07, cf_biogas_new_reinvestment)

