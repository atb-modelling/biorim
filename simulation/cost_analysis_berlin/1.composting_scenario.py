import start
import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plts
import matplotlib.ticker as tkr
import pylab

from tools import prod, pie_chart, pie_chart_cashflow
from analysis.timestep_chains import start_simulation
from tools import convert_molmasses
from simulation.data_management_and_plotting.npv_plotting import npv_graphic, multiple_npv_graphic
from simulation.data_management_and_plotting.scenarios_plotting import LegendWithoutDuplicate


config_composting = {
    ### Main settings
    "simulation"   :{
        'name': "composting",
        'type': "all combinations", 
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.05
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 34000, "ton"),
                'origin': "Berlin_mixture"
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.42, 0.18, 0.10, 0.07, 0.06, 0.06, 0.05, 0.03, 0.01]],
                'dist_km': [[98, 50, 188, 146, 21, 11, 44, 189, 153]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.composting.CompostingLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "Andersen_2010",
                'CH4_emis': 0.02, 
                'land_purchase':[True],
                'land_rent':[False],
                'fixed_operation_cost':True
                },

        "production.plant.fertilization.CompostSpreading_1" :{
                'I': prod('leaves compost', 100, "%", source='material.composting.CompostingLeaves_1'),
                'maturity' : "very mature",
                'method' : "VDLUFA"
                },
    }
}


# Example results
result_composting = start_simulation(config_composting)

npv_graphic(result_composting, 0) #land purchasing
result_composting["composting"]["t1"][0]["npv_value"]
# npv_graphic(result_composting, 1) #no purchase of land
# result_composting["composting"]["t1"][1]["npv_value"]

result_composting["composting"]["t1"][0]["cashflow"]
# result_composting["composting"]["t1"][1]["cashflow"]




config_composting_no_transport = {
    ### Main settings
    "simulation"   :{
        'name': "composting",
        'type': "all combinations", 
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.05
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 34000, "ton"),
                'origin': "Berlin_mixture"
                },
        

        "material.composting.CompostingLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'),
                'method' : "Andersen_2010",
                'CH4_emis': 0.02, 
                'land_purchase':[True],
                'land_rent':[False],
                'fixed_operation_cost':True
                },

        # "production.plant.fertilization.CompostSpreading_1" :{
        #         'I': prod('leaves compost', 100, "%", source='transport.LandTransport_1'),
        #         'maturity' : "very mature",
        #         'method' : "VDLUFA"
        #         },
    }
}


# Example results
result_composting_no_transport = start_simulation(config_composting_no_transport)

result_composting_no_transport

npv_graphic(result_composting_no_transport, 0) #land purchasing
result_composting_no_transport["composting"]["t1"][0]["npv_value"]
npv_graphic(result_composting_no_transport, 1)  #no purchase of land
result_composting_no_transport["composting"]["t1"][1]["npv_value"]

result_composting_no_transport["composting"]["t1"][0]["cashflow"]
result_composting_no_transport["composting"]["t1"][1]["cashflow"]

#graph
multiple_npv_graphic([result_composting],[result_composting_no_transport], fontsize=18, left_extra_title=[": scope of the study"], 
                     right_extra_title=[": point of view executor"], supxlabel_pos= [0.5, 0.01], supylabel_pos= [0.03, 0.5])






#Galle, Sonnewalde
config_composting_galle = {
    ### Main settings
    "simulation"   :{
        'name': "composting",
        'type': "all combinations", 
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 14000, "ton"),
                'origin': "Berlin_mixture"
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        #NOTE: Composting to Hennickendorf
        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[98]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.composting.CompostingLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "Andersen_2010",
                'CH4_emis': 0.02, 
                'land_purchase':[True, False],
                'fixed_operation_cost':[False, True]
                },

        "production.plant.fertilization.CompostSpreading_1" :{
                'I': prod('leaves compost', 100, "%", source='transport.LandTransport_1'),
                'maturity' : "very mature",
                'method' : "VDLUFA"
                },
    }
}

result_composting_galle = start_simulation(config_composting_galle)

npv_graphic(result_composting_galle, 0) #land purchasing
result_composting_galle["composting"]["t1"][0]["npv_value"]
result_composting_galle["composting"]["t1"][1]["config"]
npv_graphic(result_composting_galle, 1) #land purchase with fixed operation
result_composting_galle["composting"]["t1"][1]["npv_value"]
npv_graphic(result_composting_galle, 2) #no land purchasing, no fixed operation
npv_graphic(result_composting_galle, 3) #no purchase with fixed operation


result_composting_galle["composting"]["t1"][0]["cashflow"]#land purchasing, no fixed operation
result_composting_galle["composting"]["t1"][1]["cashflow"] #land purchase with fixed operation
result_composting_galle["composting"]["t1"][2]["cashflow"]#no land purchasing, no fixed operation
result_composting_galle["composting"]["t1"][3]["cashflow"] #no purchase with fixed operation

# cashflow=result_composting_galle["composting"]["t1"][1]["cashflow"]
# cashflow.index.get_level_values(1)
# cashflow[(cashflow["section"]=="opex")]["EUR"].sum()
# cashflow[(cashflow["section"]=="opex")&(cashflow["EUR"]>0)]

# fields_opex_positive=cashflow[(cashflow["section"]=="opex")&(cashflow["EUR"]>0)].index
# fields_opex_positive_uni=[]
# for i in range(0,len(fields_opex_positive)):
#     fields_opex_positive_uni.append(fields_opex_positive[i][1])
# fields_opex_positive_uni

# fields_opex_negative=cashflow[(cashflow["section"]=="opex")&(cashflow["EUR"]<0)].index
# fields_opex_negative_uni=[]
# for i in range(0,len(fields_opex_negative)):
#     fields_opex_negative_uni.append(fields_opex_negative[i][1])
# fields_opex_negative_uni

# fields_capex=cashflow[(cashflow["section"]=="capex")&(cashflow["EUR"]>0)].index
# fields_capex_uni=[]
# for i in range(0,len(fields_capex)):
#     fields_capex_uni.append(fields_capex[i][1])
# fields_capex_uni


# values_OPEX_pos=cashflow[(cashflow["section"]=="opex")&(cashflow["EUR"]>0)]["%"].values
# values_OPEX_neg=cashflow[(cashflow["section"]=="opex")&(cashflow["EUR"]<0)]["%"].values
# values_CAPEX=cashflow[(cashflow["section"]=="capex")&(cashflow["EUR"]>0)]["%"].values
# values_CAPEX

# amounts_CAPEX=cashflow[(cashflow["section"]=="capex")&(cashflow["EUR"]>0)]["EUR"].values
# amounts_OPEX_pos=cashflow[(cashflow["section"]=="opex")&(cashflow["EUR"]>0)]["EUR"].values
# amounts_OPEX_neg=cashflow[(cashflow["section"]=="opex")&(cashflow["EUR"]<0)]["EUR"].values



# pie_chart(y=values_OPEX_pos, labels=fields_opex_positive, title="OPEX - Galle", loc="lower center")
# pie_chart(y=values_OPEX_neg, labels=fields_opex_negative, title="OPEX (revenues) - Galle", loc="lower center")
# pie_chart(y=values_CAPEX, labels=fields_capex, title="CAPEX - Galle", loc="lower center")

# pie_chart_cashflow(y_capex=values_CAPEX, y_opex_pos=values_OPEX_pos, y_opex_neg=values_OPEX_neg, 
#                     labels_capex=fields_capex_uni,labels_opex_neg=fields_opex_negative_uni, labels_opex_pos=fields_opex_positive_uni)


# len(values_CAPEX)
# values_OPEX_neg
# fields_opex_negative_uni




# color_cashflows = ["#"+''.join([random.choice('0123456789ABCDEF') for j in range(6)])
#              for i in range(20)]
# color_cashflows

# total_fields=fields_capex_uni+fields_opex_negative_uni+fields_opex_positive_uni
# total_fields


# dict_cashflows={}
# for i in range (0, len(total_fields)):
#     dict_cashflows[total_fields[i]]=color_cashflows[i]

# dict_cashflows={'installation and infrastructure': '#666633', 
#                 'machinery investment': '#8F34A5', 
#                 'compost sales': '#ff9966', 
#                 'fuel consumption': '#0066ff', 
#                 'transport price - multiple routes': '#0CC4D9', 
#                 'additional input - diesel consumption': '#37017B', 
#                 'maintenance': '#731D51', 
#                 'personnel': '#80A8C3'}

# def numfmt(x, pos): # custom formatter function: divide by 1000.0
#     """
#     Custom formatter function: divide by 1000. 
#     """
#     th = '{}'.format(x / 1000)
#     return th


# #####
# fig, (ax1, ax2) =plt.subplots(ncols=2, nrows=1)

# y_pos=0
# ax1.yaxis.grid(color='gray', linestyle='dashed')
# for i in range (0, len(values_CAPEX)):  
#     ax1.bar("CAPEX", -amounts_CAPEX[i], bottom=y_pos, width=0.01, color=dict_cashflows[fields_capex_uni[i]], label=fields_capex_uni[i])
#     y_pos=y_pos+(-amounts_CAPEX[i])
# ax1.tick_params("both", labelsize=18)
# ax1.yaxis.offsetText.set_fontsize(18)
# ax1.set_ylabel("EUR", fontsize=18)

# y_pos=0
# for i in range (0, len(values_OPEX_pos)):  
#     ax2.bar("OPEX", -amounts_OPEX_pos[i], bottom=y_pos, width=0.01, color=dict_cashflows[fields_opex_positive_uni[i]], label=fields_opex_positive_uni[i])
#     y_pos=y_pos+(-amounts_OPEX_pos[i])

# y_pos=0
# ax2.yaxis.grid(color='gray', linestyle='dashed')
# for i in range (0, len(values_OPEX_neg)):  
#     ax2.bar("OPEX",-amounts_OPEX_neg[i], bottom=y_pos, width=0.01, color=dict_cashflows[fields_opex_negative_uni[i]], label=fields_opex_negative_uni[i])
#     y_pos=y_pos+(-amounts_OPEX_neg[i])

# ax2.tick_params("both", labelsize=18)
# fig.legend(fontsize=16, loc=9, ncol=4)
# plt.show()















#URD, Grüneberg
config_composting_URD = {
    ### Main settings
    "simulation"   :{
        'name': "composting",
        'type': "all combinations", 
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 6150, "ton"),
                'origin': "Berlin_mixture"
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        #NOTE: Composting to Hennickendorf
        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.composting.CompostingLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "Andersen_2010",
                'CH4_emis': 0.02, 
                'land_purchase':[True, False]
                },

        "production.plant.fertilization.CompostSpreading_1" :{
                'I': prod('leaves compost', 100, "%", source='transport.LandTransport_1'),
                'maturity' : "very mature",
                'method' : "VDLUFA"
                },
    }
}

result_composting_URD = start_simulation(config_composting_URD)

npv_graphic(result_composting_URD, 0) #land purchasing
result_composting_URD["composting"]["t1"][0]["npv_value"]
npv_graphic(result_composting_URD, 1) #no purchase of land
result_composting_URD["composting"]["t1"][1]["npv_value"]

result_composting_URD["composting"]["t1"][0]["cashflow"]
result_composting_URD["composting"]["t1"][1]["cashflow"]