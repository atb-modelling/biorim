import start 
import pandas as pd
import math

from material.biochar import PyrolysisPlant
from material.gasification import Gasification
from production.plant.street_trees import StreetTrees
from production.plant.fertilization import BiocharSpreading
from tools import  convert_molmasses, prod, determine_c_n
from read import Lauf_2019
from read.Ecoinvent import Ecoinvent34
from external import Electricity, ExternInput
from material.pelletization import PelletFactory
from declarations import Product

start
#generating maple
TreeLeavesBerlin=StreetTrees(O=prod("tree leaves", 36000, "ton"), origin="Berlin_mixture", id = None).output()
TreeLeavesBerlin2000=StreetTrees(O=prod("tree leaves", 2000, "ton"), origin="Berlin_mixture", id = None).output()
TreeLeavesBerlin5000=StreetTrees(O=prod("tree leaves", 5000, "ton"), origin="Berlin_mixture", id = None).output()
TreeLeavesBerlin8000=StreetTrees(O=prod("tree leaves", 8000, "ton"), origin="Berlin_mixture", id = None).output()
TreeLeavesBerlin16000=StreetTrees(O=prod("tree leaves", 16000, "ton"), origin="Berlin_mixture", id = None).output()
TreeLeavesBerlin


#pelletization
#biomass drying
biomass_dry=PelletFactory(TreeLeavesBerlin).biomass_drying()
biomass_dry[biomass_dry.index.isin(["tree leaves dry"])]
PelletFactory(TreeLeavesBerlin).additional_input()

PelletFactory(TreeLeavesBerlin).output()
PelletFactory(TreeLeavesBerlin).emissions()
PelletFactory(TreeLeavesBerlin).cashflow()

TreeLeavesPellets=PelletFactory(TreeLeavesBerlin).output()
TreeLeavesPellets


TreeLeavesPellets["Q"][0]/TreeLeavesPellets["density"][0] * math.pow(10,6) * TreeLeavesPellets["FVI"][0] / 1000

TreeLeavesPellets["Q"][0] * TreeLeavesPellets["glycerol"][0] * 1000 *0.74


ExternInput(prod("glycerol",TreeLeavesPellets["Q"][0] * TreeLeavesPellets["glycerol"][0] *1000, "kg", location="GLO", activity="trichloropropane production", source='external.ExternInput') ).emissions()





#supply days test
# Gasification(TreeLeavesPellets,  kW=4000, electrical_energy_eff=0.25).supply_information()
Gasification(TreeLeavesBerlin, kW=4000, electrical_energy_eff=0.25).supply_information()

Gasification(TreeLeavesBerlin).supply_information()
Gasification(TreeLeavesBerlin, kW=850).supply_information()
Gasification(TreeLeavesBerlin5000).supply_information()
Gasification(TreeLeavesBerlin8000, kW=500).supply_information()
Gasification(TreeLeavesBerlin16000, kW=500).supply_information()



#lifetime_share
Gasification(TreeLeavesBerlin, kW=1000).lifetime_share()
Gasification(TreeLeavesBerlin5000).lifetime_share()
Gasification(TreeLeavesBerlin8000, kW=500).lifetime_share()
Gasification(TreeLeavesBerlin16000, kW=500).lifetime_share()

#oepration_share
Gasification(TreeLeavesBerlin, kW=1000).operation_share()
Gasification(TreeLeavesBerlin2000).operation_share()
Gasification(TreeLeavesBerlin2000, operation_hours_year=7000).operation_share()
Gasification(TreeLeavesBerlin5000).operation_share()
Gasification(TreeLeavesBerlin8000, kW=500).operation_share()
Gasification(TreeLeavesBerlin16000, kW=500).operation_share()
Gasification(TreeLeavesBerlin, kW=850).operation_share()


#biomass drying test
Gasification(TreeLeavesBerlin, kW=150).biomass_drying()

Gasification(TreeLeavesBerlin2000).biomass_drying()
Gasification(TreeLeavesBerlin5000).biomass_drying()
Gasification(TreeLeavesBerlin, kW=500).biomass_drying()
Gasification(TreeLeavesBerlin).biomass_drying().loc["tree leaves dry"]["Q"]
Gasification(TreeLeavesBerlin, vaporization_rate=0.95).biomass_drying()

#energy drying
Gasification(TreeLeavesBerlin, kW=12000).energy_drying()
Gasification(TreeLeavesPellets, kW=12000).energy_drying()

#syngas density test
Gasification(TreeLeavesBerlin).syngas_density()
Gasification(TreeLeavesBerlin, kW=4000).syngas_production()
Gasification(TreeLeavesBerlin, kW=850, syngas_prod="fixed rate").syngas_production()

#pyrolysis test
Gasification(TreeLeavesBerlin).pyrolysis()
11325/14868

#syngas production
syngas_prod=Gasification(TreeLeavesBerlin, kW=4000).syngas_production()
syngas_prod

#syngas production 
TreeLeavesPellets=PelletFactory(TreeLeavesBerlin).output()
TreeLeavesPellets

syngas_prod_pellets=Gasification(TreeLeavesPellets, kW=4000).syngas_production()
syngas_prod_pellets


Gasification(TreeLeavesBerlin, kW=4000).syngas_density()
Gasification(TreeLeavesBerlin, kW=850, thermal_recirculation=True).fuel_consum_thermal_recirculation()


#output test
Gasification(TreeLeavesPellets,  kW=12000).output()
Gasification(TreeLeavesBerlin, kW=12000).output()

Gasification(TreeLeavesBerlin).output()
biochar_gas=Gasification(TreeLeavesBerlin).output().loc["unconverted biochar"]
biochar_gas=biochar_gas.to_frame().T
biochar_gas
biochar_gas=Product(biochar_gas)
biochar_gas
BiocharSpreading(biochar_gas).emissions()

PyrolysisPlant(TreeLeavesBerlin).output()



Gasification(TreeLeavesBerlin2000).output()
Gasification(TreeLeavesBerlin5000).output()
Gasification(TreeLeavesBerlin, kW=1000).output()
Gasification(TreeLeavesBerlin, calculation_approach="per mass unit").output()
Gasification(TreeLeavesBerlin, kW=850).output()
Gasification(TreeLeavesBerlin, kW=500).output()

#syngas combustion
Gasification(TreeLeavesBerlin, kW=850).syngas_combustion()


#emissions test
Gasification(TreeLeavesPellets,  kW=850).emissions()
Gasification(TreeLeavesBerlin, kW=850).emissions()


Gasification(TreeLeavesBerlin2000).emissions()
Gasification(TreeLeavesBerlin5000).emissions()
Gasification(TreeLeavesBerlin, calculation_approach="per mass unit").emissions()
Gasification(TreeLeavesBerlin, thermal_recirculation=False).emissions()
Gasification(TreeLeavesBerlin, kW=850).emissions()


#cashflow tests
Gasification(TreeLeavesBerlin, kW=12000).cashflow()
Gasification(TreeLeavesBerlin2000).cashflow()
Gasification(TreeLeavesBerlin5000).cashflow()
Gasification(TreeLeavesBerlin, calculation_approach="per mass unit").cashflow()
Gasification(TreeLeavesBerlin, kW=850).cashflow()
Gasification(TreeLeavesBerlin).cashflow()["EUR"].sum()
cashflow_gas=Gasification(TreeLeavesBerlin).cashflow()
cashflow_gas[cashflow_gas["section"]=="opex"]["EUR"].sum()



#area requirements

Gasification(TreeLeavesBerlin, kW=4000).area_requirements()

