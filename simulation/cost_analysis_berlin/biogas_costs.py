import start
import pandas as pd
import numpy as np


from material.biogas import BiogasPlant
from production.plant.street_trees import StreetTrees
from production.plant.crop import ArableLand
from production.plant.fertilization import CompostSpreading, ManureSpreading
from production.animal.ruminant import DairyFarm
from production.household.food_waste import FoodWaste
from material.composting import CompostingLeaves, CompostPlant
from material.storing import LeafSilageStorage, LeafStorageContinuous
from tools import  convert_molmasses, prod, determine_c_n, determine_c_n_v2, oDM_mix
from read.Ecoinvent import Ecoinvent34
from read import KTBL
from external import ExternInput
from transport import LandTransport
from declarations import Product
from production.plant.pretreatment_leaves import PretreatmentLeaves


start
#generating tree leaves
TreeLeavesBerlin=StreetTrees(O=prod("tree leaves", 36000, "ton"), origin="Berlin_mixture", id = None).output()
TreeLeavesBerlin
TreeLeavesBerlin_RHL=StreetTrees(O=prod("tree leaves", 15000, "ton"), origin="Berlin_mixture", id = None).output()
TreeLeavesBerlin_RHL

#generating food residues
Food_waste_RHL=FoodWaste(O=prod("food waste", 80000, "ton")).output()
Food_waste_RHL

#oDM content
oDM_leaves_RHL=TreeLeavesBerlin_RHL["Q"][0]*TreeLeavesBerlin_RHL["DM"][0]*TreeLeavesBerlin_RHL["oDM"][0]
oDM_leaves_RHL
oDM_leaves_RHL/TreeLeavesBerlin_RHL["Q"][0]

oDM_waste_RHL=Food_waste_RHL["Q"][0]*Food_waste_RHL["DM"][0]*Food_waste_RHL["oDM"][0]
oDM_waste_RHL
oDM_waste_RHL/Food_waste_RHL["Q"][0]


pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL])
RHL_mix=pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL])
RHL_mix

elec_prod=[]
for item in RHL_mix.index:

    elec_prod.append(BiogasPlant(pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL])[pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL]).index.isin([item])], CHP_kW=6000).energy_prod_potential_kWh())

elec_prod[0]["Q"][0]/(elec_prod[0]["Q"][0]+elec_prod[1]["Q"][0])

type(pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL]))
pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL])

BiogasPlant(RHL_mix[RHL_mix.index.isin(["tree leaves"])]).emissions()

RHL_mix.index[0]
if len(RHL_mix.index)>1:
    RHL_mix_1=RHL_mix[RHL_mix.index.isin([RHL_mix.index[0]])]

type(RHL_mix_1)






#potential electrical energy
BiogasPlant(TreeLeavesBerlin, CHP_kW=6000).CH4_C_for_conversion()
BiogasPlant(TreeLeavesBerlin, CHP_kW=6000).CHP_CH4_C_leakage()
available_methane=BiogasPlant(TreeLeavesBerlin, CHP_kW=6000).CH4_C_for_conversion()-BiogasPlant(TreeLeavesBerlin, CHP_kW=6000).CHP_CH4_C_leakage()
available_methane*1.336

#potential electrical energy pre-treated leaves
TreeLeavesBerlin_pretreated=PretreatmentLeaves(TreeLeavesBerlin).output()
TreeLeavesBerlin_pretreated

available_methane_pretreated=BiogasPlant(TreeLeavesBerlin_pretreated, CHP_kW=6000).CH4_C_for_conversion()-BiogasPlant(TreeLeavesBerlin_pretreated, CHP_kW=6000).CHP_CH4_C_leakage()
available_methane_pretreated*1.336

(available_methane_pretreated-available_methane)/available_methane


BiogasPlant(TreeLeavesBerlin_RHL, CHP_kW=6000).energy_prod_potential_kWh()

BiogasPlant(TreeLeavesBerlin, CHP_kW=6000).energy_prod_potential_kWh(type="thermal+electrical")
BiogasPlant(Food_waste_RHL, CHP_kW=6000).energy_prod_potential_kWh()

BiogasPlant(pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL]), CHP_kW=6000).energy_prod_potential_kWh()
BiogasPlant(pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL]), CHP_kW=6000, first_feedstock_analysis=True).energy_prod_potential_kWh()

BiogasPlant(pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL]), CHP_kW=6000).allocation_ratio()
BiogasPlant(pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL]), CHP_kW=6000, first_feedstock_analysis=True).allocation_ratio()

#additional input
BiogasPlant(pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL]), CHP_kW=6000).additional_input()
BiogasPlant(pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL]), CHP_kW=6000, first_feedstock_analysis=True).additional_input()
BiogasPlant(TreeLeavesBerlin_RHL, CHP_kW=6000).additional_input()
BiogasPlant(Food_waste_RHL, CHP_kW=6000).additional_input()


#Emissions
BiogasPlant(pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL]), CHP_kW=6000, first_feedstock_analysis=True).emissions()
BiogasPlant(pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL]), CHP_kW=6000).emissions()
BiogasPlant(TreeLeavesBerlin_RHL, CHP_kW=6000).emissions()
BiogasPlant(Food_waste_RHL, CHP_kW=6000).emissions()

#Output
BiogasPlant(pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL]), CHP_kW=6000, first_feedstock_analysis=True).output()
BiogasPlant(pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL]), CHP_kW=6000).output()
digestate=BiogasPlant(TreeLeavesBerlin, CHP_kW=6000).output()
digestate=digestate.drop("electrical energy").drop("thermal energy")
digestate

BiogasPlant(Food_waste_RHL, CHP_kW=6000).output()

####Composting normal
CompostPlant(digestate).output()


BiogasPlant(pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL]), CHP_kW=6000).cashflow()
BiogasPlant(pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL]), first_feedstock_analysis=True, CHP_kW=6000).cashflow()

BiogasPlant(TreeLeavesBerlin_RHL, CHP_kW=6000).cashflow()
cf_bio=BiogasPlant(TreeLeavesBerlin, CHP_kW=2700).cashflow()

cf_bio.loc[cf_bio["section"]=="depreciation"]["EUR"][0]
cf_bio.loc[cf_bio["section"]=="tax"]["abs"][0]

#supply information
BiogasPlant(TreeLeavesBerlin, CHP_kW=2700).supply_information(method="electricity potential")
BiogasPlant(TreeLeavesBerlin, CHP_kW=2500).supply_information(method="oDM requirement")

BiogasPlant(TreeLeavesBerlin_RHL, CHP_kW=2500).supply_information(method="oDM requirement")
BiogasPlant(TreeLeavesBerlin_RHL, CHP_kW=1100).supply_information(method="electricity potential")
BiogasPlant(Food_waste_RHL, CHP_kW=6000).supply_information()
BiogasPlant(pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL]), CHP_kW=6000).supply_information()
BiogasPlant(pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL]), CHP_kW=6000).supply_information(method="oDM requirement")

#operation share
BiogasPlant(pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL]), CHP_kW=6000).operation_share()
BiogasPlant(pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL]), CHP_kW=6000, first_feedstock_analysis=True).operation_share()
BiogasPlant(pd.concat([TreeLeavesBerlin_RHL, Food_waste_RHL]), CHP_kW=6000, CHP_kW_reshape=True).plant_size_reshape()

BiogasPlant(TreeLeavesBerlin, CHP_kW=2500).operation_share()

#area requirements
BiogasPlant(TreeLeavesBerlin, CHP_kW=2700).area_requirements()


BiogasPlant(TreeLeavesBerlin, CHP_kW=2700, EEG_prices=("manual", 0.19)).feed_in_tariff()


EEG_prices=("manual", 0.19)
EEG_prices[1]

######NOTE: when applying this, changes the amount input, has to be modified
BiogasPlant(RHL_mix, calculation_approach="operation per year", CHP_kW=6000).output()
BiogasPlant(TreeLeavesBerlin_RHL, calculation_approach="operation per year").output()


BiogasPlant(TreeLeavesBerlin, CHP_kW=2500).labour_requirement()


#Biomethena
BiogasPlant(TreeLeavesBerlin, CHP_kW=1500, energy_usage_type="biomethane").emissions()
BiogasPlant(TreeLeavesBerlin, CHP_kW=1500).emissions()

BiogasPlant(TreeLeavesBerlin, CHP_kW=1500, energy_usage_type="biomethane").output()
BiogasPlant(TreeLeavesBerlin, CHP_kW=1500).output()