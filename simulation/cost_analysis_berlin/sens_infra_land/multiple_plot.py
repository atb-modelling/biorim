import start
import matplotlib.pyplot as plt
import pandas as pd

from tools import prod
from analysis.timestep_chains import start_simulation
from tools import convert_molmasses
from analysis.economic.macc import MACC, circle_plot_MAC, multiple_circle_plot, single_graphic_MACC, multiple_graphic_MACC, circle_plot_NPV_emissions_panel
from simulation.data_management_and_plotting.npv_plotting import npv_graphic



MAC_df_BN_LP
MAC_df_BN_LR
MAC_df_BN_OL

MAC_df_EI_LP
MAC_df_EI_LR
MAC_df_EI_OL

# right=[MAC_df_EI_LP, MAC_df_EI_LR, MAC_df_EI_OL]
# left=[MAC_df_BN_LP, MAC_df_BN_LR, MAC_df_BN_OL]

right=[MAC_df_EI_LP, MAC_df_EI_LR]
left=[MAC_df_EI_LP, MAC_df_EI_LR]


# infra_land=pd.concat([MAC_df_BN_LP, MAC_df_BN_LR, MAC_df_BN_OL])
infra_land=pd.concat([MAC_df_BN_LP, MAC_df_BN_LR, MAC_df_EI_LP, MAC_df_EI_LR])
infra_land


#concatenating all databases in multiindex
left_df_concat=pd.concat(left)
left_df_concat.to_csv(r"C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\BN_column.csv", sep=";")
right_df_concat=pd.concat(right)
right_df_concat.to_csv(r"C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\EI_column.csv", sep=";")


# max_NPV_right=[]
# for df in right

right[0]["NPV"][0]>left[0]["NPV"][0]

# single_graphic_MACC(MAC_df_purchase)
# single_graphic_MACC(MAC_df_purchase, show_legend=False)
multiple_graphic_MACC(right_column=right, left_column=left, supxlabel_pos=[0.5, 0.01], supylabel_pos=[0.01, 0.5], left_extra_title=[": BN-LP", ": BN-LR", ": BN-OL"], 
                     right_extra_title=[": EI-LP", ": EI-LR", ": EI-OL"], sharey=True, side_sharex="left", ncol=2, fontsize=16)

# circle_plot_MAC(MAC_df_purchase, show_legend=False, show_art=False, ncol= 3, loc=9, ax_legend=False)
# circle_plot_MAC(MAC_df_purchase, show_legend=False, show_art=False, ncol= 3, loc=9, save_fig_memory=True)
# circle_plot_MAC(MAC_df_purchase, show_legend=True, size_factor=10000, ax_margins=[0.4,0.4])

# circle_plot_MAC(MAC_df_rent)
# circle_plot_MAC(MAC_df_own)
multiple_circle_plot(right_column=right, left_column=left, supxlabel_pos=[0.5, 0.01], supylabel_pos=[0.01, 0.5], ax_margins=[0.3,0.3],
                     size_factor=700, loc=9, ncol=2, fontsize=16, grid=True, left_extra_title=[": BN-LP", ": BN-LR"], sharey=True,
                     right_extra_title=[": EI-LP", ": EI-LR"])


infra_land

circle_plot_NPV_emissions_panel(infra_land, size_factor=500, fontsize=14, ncol=3, supxlabel_pos=[0.5, 0.01], supylabel_pos=[0.05, 0.5], 
                                obs_legend=False)