import start
from tools import prod
from analysis.timestep_chains import start_simulation
from tools import convert_molmasses
from simulation.data_management_and_plotting.npv_plotting import npv_graphic

config_cofiring = {
    ### Main settings
    "simulation"   :{
        'name': "cofiring",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[150]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'kW':20000,
                'operation_hours':8000, 
                'retrofitting': False, 
                'land_purchase': True,
                'land_rent': False,
                'new_infrastructure': True
                },
    }
}

result_cofiring = start_simulation(config_cofiring)
npv_graphic(result_cofiring, 0)
result_cofiring["cofiring"]["t1"][0]["config"]
result_cofiring["cofiring"]["t1"][0]["cashflow"]
result_cofiring["cofiring"]["t1"][0]["emissions"]
result_cofiring["cofiring"]["t1"][0]["npv_value"]

#####

config_cofiring_pellet = {
    ### Main settings
    "simulation"   :{
        'name': "pelletization and cofiring",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[150]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.pelletization.PelletFactory_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves pellets', 100, "%", source='material.pelletization.PelletFactory_1'),
                'kW':700000,
                'operation_hours':8000, 
                'retrofitting': False, 
                'land_purchase': True,
                'land_rent': False,
                'new_infrastructure': True
                },
    }
}

result_cofiring_pellet = start_simulation(config_cofiring_pellet)
npv_graphic(result_cofiring_pellet, 0)
result_cofiring_pellet["pelletization and cofiring"]["t1"][0]["config"]
result_cofiring_pellet["pelletization and cofiring"]["t1"][0]["cashflow"]
result_cofiring_pellet["pelletization and cofiring"]["t1"][0]["emissions"]
result_cofiring_pellet["pelletization and cofiring"]["t1"][0]["npv_value"]






#Very likely not to have wet leaves as a feedstock
config_cofiring_dry_wet = {
    ### Main settings
    "simulation"   :{
        'name': "cofiring",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[150]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'kW':20000,
                'operation_hours':8000, 
                'retrofitting': False, 
                'land_purchase': True,
                'land_rent': False,
                'new_infrastructure': True,
                'dry_biomass': False
                },
    }
}

result_cofiring_dry_wet = start_simulation(config_cofiring_dry_wet)

npv_graphic(result_cofiring_dry_wet, 0)
result_cofiring_dry_wet["cofiring"]["t1"][0]["config"]
result_cofiring_dry_wet["cofiring"]["t1"][0]["cashflow"]
result_cofiring_dry_wet["cofiring"]["t1"][0]["emissions"]
result_cofiring_dry_wet["cofiring"]["t1"][0]["npv_value"]

### 