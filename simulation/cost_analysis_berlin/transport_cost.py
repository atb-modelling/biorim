import start

import pandas as pd

from transport import LandTransport, LandTransportMultiple
from material.storing import LeafSilageStorage
from production.plant.street_trees import StreetTrees
from tools import  convert_molmasses, prod, determine_c_n
from read.Ecoinvent import Ecoinvent34

from production.plant.fertilization import ManureSpreading


#generating maple
TreeLeavesBerlin=StreetTrees(O=prod("tree leaves", 36000, "ton"), origin="Berlin_mixture", id = None).output()
TreeLeavesBerlin
TreeLeavesBerlin_dens=LeafSilageStorage(TreeLeavesBerlin).output()
TreeLeavesBerlin_dens

LandTransport(TreeLeavesBerlin, dist_km=40, method="LBE").emissions().sum().sum()
LandTransport(TreeLeavesBerlin, dist_km=41, method="LBE").emissions().sum().sum()
LandTransport(TreeLeavesBerlin, dist_km=42, method="LBE").emissions().sum().sum()

LandTransport(TreeLeavesBerlin, dist_km=225, method="LBE", type="lorry, large size")._total_distance()
LandTransport(TreeLeavesBerlin, dist_km=41, load_basis="volume", transport_cost="sommer_elso", method="LBE", type="lorry, large size").cashflow()

LandTransport(TreeLeavesBerlin, dist_km=40, method="LBE", type="lorry, medium size")._total_distance()
LandTransport(TreeLeavesBerlin, dist_km=225, method="LBE", type="lorry, medium size").cashflow()

LandTransport(TreeLeavesBerlin, dist_km=40, method="LBE", type="tractor, small size")._total_distance()
LandTransport(TreeLeavesBerlin, dist_km=40, method="LBE", type="tractor, small size").cashflow()


##
LandTransport(TreeLeavesBerlin, dist_km=225, method="LBE", type="lorry, large size").cashflow()
LandTransport(TreeLeavesBerlin_dens, dist_km=225, method="LBE", type="lorry, large size").cashflow()
LandTransport(TreeLeavesBerlin_dens, dist_km=225, load_basis="volume", method="LBE", type="lorry, large size").cashflow()
LandTransport(TreeLeavesBerlin_dens, dist_km=225, load_basis="volume", method="LBE", type="lorry, medium size").cashflow()


LandTransport((TreeLeavesBerlin, TreeLeavesBerlin_dens), dist_km=(31,20)).cashflow()


#Updated example, using just one product with mass allocation option
TreeLeavesBerlin_4=StreetTrees(O=prod("tree leaves", 14000, "ton"), origin="Berlin_mixture", id = None).output()

dist_km=[100, 300, 400] #800 km in total
type_trans_list=["lorry, large size", "lorry, medium size", "lorry, large size"]


#New approach
LandTransportMultiple(TreeLeavesBerlin_4, mass_allocation=[0.2, 0.5, 0.3], dist_km=dist_km, type_transp="lorry, large size").route_plan()
LandTransportMultiple(TreeLeavesBerlin_4, mass_allocation=[0.2, 0.5, 0.3], dist_km=dist_km, type_transp=type_trans_list).route_plan()

LandTransportMultiple(TreeLeavesBerlin_4, mass_allocation=[0.2, 0.5, 0.3], dist_km=dist_km, type_transp="lorry, large size").emissions()

LandTransportMultiple(TreeLeavesBerlin_4, mass_allocation=[0.2, 0.5, 0.3], dist_km=dist_km, type_transp="lorry, large size").cashflow()

#Current approach of the model
LandTransport(TreeLeavesBerlin_4, dist_km=800, type="lorry, large size").emissions()
LandTransport(TreeLeavesBerlin_4, dist_km=800, type="lorry, large size").cashflow()

test_1=LandTransport(StreetTrees(O=prod("tree leaves", 2800, "ton"), origin="Berlin_mixture", id = None).output(), dist_km=100, type="lorry, large size").emissions()
test_2=LandTransport(StreetTrees(O=prod("tree leaves", 7000, "ton"), origin="Berlin_mixture", id = None).output(), dist_km=300, type="lorry, large size").emissions()
test_3=LandTransport(StreetTrees(O=prod("tree leaves", 4200, "ton"), origin="Berlin_mixture", id = None).output(), dist_km=400, type="lorry, large size").emissions()

pd.concat([test_1, test_2, test_3]).loc["diesel production"].sum()


###Fixing of the approach
dist_km=[100, 300, 400]
mass_allocation=[0.2, 0.5, 0.3]
route_plan_case=LandTransportMultiple(TreeLeavesBerlin_4, mass_allocation=[0.2, 0.5, 0.3], 
                                      dist_km=dist_km, type_transp="lorry, large size").route_plan()
mass_share=TreeLeavesBerlin_4.copy()
mass_share["Q"] = mass_share ["Q"] * mass_allocation[0]
mass_share

