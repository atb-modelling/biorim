import start

from tools import prod
from analysis.timestep_chains import start_simulation
from tools import convert_molmasses
from simulation.data_management_and_plotting.npv_plotting import npv_graphic

#For 850 kW

config_gasification_850kW = {
    ### Main settings
    "simulation"   :{
        'name': "gasification",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransport_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'dist_km': 150,
                'type': "lorry, large size",
                'method':"LBE",
                'load_basis':"volume"
                },

        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransport_1'),
                'kW':850,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': True,
                'land_rent': False
                },
          
    }
}

result_gasification_850kW=start_simulation(config_gasification_850kW)


npv_graphic(result_gasification_850kW, 0)
result_gasification_850kW["gasification"]["t1"][0]["config"]

npv_graphic(result_gasification_850kW, 1)
result_gasification_850kW["gasification"]["t1"][1]["config"]

result_gasification_850kW["gasification"]["t1"][0]["npv_value"]
result_gasification_850kW["gasification"]["t1"][1]["npv_value"]

result_gasification_850kW["gasification"]["t1"][0]["cashflow"]
result_gasification_850kW["gasification"]["t1"][1]["cashflow"]


config_pellet_gasification_850kW = {
    ### Main settings
    "simulation"   :{
        'name': "gasification",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 34000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransport_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'dist_km': 150,
                'type': "lorry, large size",
                'method':"LBE",
                'load_basis':"volume"
                },

        "material.pelletization.Pelletization_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransport_1'),
                },

        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves pellets', 100, "%", source='material.pelletization.Pelletization_1'),
                'kW':850,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': [True, False]
                },
          
    }
}

result_pellet_gasification_850kW=start_simulation(config_pellet_gasification_850kW)


npv_graphic(result_pellet_gasification_850kW, 0)
result_pellet_gasification_850kW["gasification"]["t1"][0]["config"]

npv_graphic(result_pellet_gasification_850kW, 1)
result_pellet_gasification_850kW["gasification"]["t1"][1]["config"]

result_pellet_gasification_850kW["gasification"]["t1"][0]["npv_value"]
result_pellet_gasification_850kW["gasification"]["t1"][1]["npv_value"]

result_pellet_gasification_850kW["gasification"]["t1"][0]["cashflow"]
result_pellet_gasification_850kW["gasification"]["t1"][1]["cashflow"]


result_pellet_gasification_850kW["gasification"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas()

list(result_pellet_gasification_850kW.keys())[0]