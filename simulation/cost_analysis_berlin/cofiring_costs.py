import start
import pandas as pd
import numpy as np


from material.cofiring import CoFiring, BiomassDrying
from material.pelletization import PelletFactory
from production.plant.street_trees import StreetTrees
from tools import  convert_molmasses, prod, determine_c_n, determine_c_n_v2, oDM_mix, LHV_correction
from read import GHG_protocol

start
#generating tree leaves
TreeLeavesBerlin=StreetTrees(O=prod("tree leaves", 36000, "ton"), origin="Berlin_mixture", id = None).output()
TreeLeavesBerlin

LHV_correction(TreeLeavesBerlin, H=5.4, DM=0.9)

"tree leaves" in TreeLeavesBerlin.index

TreeLeaves_1tonne=StreetTrees(O=prod("tree leaves", 1, "ton"), origin="Berlin_mixture", id = None).output()
TreeLeaves_1tonne

#Biomass drying
BiomassDrying(TreeLeavesBerlin, final_moisture=0.15).output()
BiomassDrying(TreeLeavesBerlin, final_moisture=0.10).output()

BiomassDrying(TreeLeavesBerlin, final_moisture=0.15).fuel_required()
BiomassDrying(TreeLeavesBerlin, final_moisture=0.10).fuel_required()
BiomassDrying(TreeLeavesBerlin, final_moisture=0.10, method="vaporization heat").fuel_required()

BiomassDrying(TreeLeaves_1tonne, final_moisture=0.10).energy_input()
BiomassDrying(TreeLeavesBerlin, final_moisture=0.10, method="vaporization heat").energy_input()
BiomassDrying(TreeLeavesBerlin, final_moisture=0.10, method="Haque").energy_input()

BiomassDrying(TreeLeavesBerlin, final_moisture=0.10).output()
BiomassDrying(TreeLeavesBerlin, final_moisture=0.10).energy_input()

BiomassDrying(TreeLeavesBerlin, final_moisture=0.10).water_vapour()
BiomassDrying(TreeLeavesBerlin, final_moisture=0.15).water_vapour()

BiomassDrying(TreeLeavesBerlin, final_moisture=0.10).additional_input()
BiomassDrying(TreeLeavesBerlin, final_moisture=0.15).additional_input()

BiomassDrying(TreeLeavesBerlin, final_moisture=0.10).emissions()
BiomassDrying(TreeLeavesBerlin, final_moisture=0.15).emissions()

BiomassDrying(TreeLeavesBerlin, final_moisture=0.1).cashflow()
BiomassDrying(TreeLeavesBerlin, final_moisture=0.15).cashflow()



TreeLeaves_1tonne=StreetTrees(O=prod("tree leaves", 1, "ton"), origin="Berlin_mixture", id = None).output()

carbon_trees=StreetTrees(O=prod("tree leaves", 34000, "ton"), origin="Berlin_mixture", id = None).emissions()
convert_molmasses(carbon_trees, to="molecule")


PelletFactory(TreeLeavesBerlin, emissions_calculation="Chen_2009").emissions()
PelletFactory(TreeLeavesBerlin).emissions()

TreeLeavesPellets=PelletFactory(TreeLeavesBerlin, emissions_calculation="Chen_2009").output()
TreeLeavesPellets

TreeLeavesPellets=PelletFactory(TreeLeavesBerlin).output()
TreeLeavesPellets

#leaves energy content
TreeLeavesBerlin["Q"]*TreeLeavesBerlin["LHV fresh"]
TreeLeavesBerlin["Q"]*TreeLeavesBerlin["DM"]*TreeLeavesBerlin["LHV dry"]

TreeLeaves_1tonne["Q"]*TreeLeaves_1tonne["LHV fresh"]*1000*0.2778 #LHV is en MJ/kg
TreeLeaves_1tonne["Q"]*TreeLeaves_1tonne["DM"]*TreeLeaves_1tonne["LHV dry"] *1000*0.2278


#Energy content
TreeLeavesBerlinDry=BiomassDrying(TreeLeavesBerlin, final_moisture=0.10).output()
TreeLeavesBerlinDry

CoFiring(TreeLeavesBerlin, kW=6000).feedstock_energy_content()
CoFiring(TreeLeavesBerlinDry, kW=6000).feedstock_energy_content()
CoFiring(TreeLeavesPellets, kW=6000).feedstock_energy_content()

CoFiring(TreeLeavesBerlin, kW=6000).supply_information()
CoFiring(TreeLeavesBerlin, kW=20000, electrical_efficiency=0.14).supply_information()
CoFiring(TreeLeavesBerlin, kW=12000).supply_information()
CoFiring(TreeLeavesPellets, kW=10000).supply_information()


CoFiring(TreeLeavesBerlin, kW=10000).supply_information()
CoFiring(TreeLeavesBerlinDry, kW=10000).supply_information()


CoFiring(TreeLeavesBerlin, kW=12000).lifetime_share()





CoFiring(TreeLeavesBerlin, kW=450000, include_main_substrate=True).supply_information()
CoFiring(TreeLeavesBerlin, kW=22000).supply_information()

CoFiring(TreeLeavesBerlin, kW=26000).operation_share()
CoFiring(TreeLeavesBerlin, kW=20000).lifetime_share()

CoFiring(TreeLeavesBerlin, kW=20000).area_requirements()

CoFiring(TreeLeavesBerlin, kW=25000).biomass_drying()
dry_biomass=CoFiring(TreeLeavesBerlin, kW=25000).biomass_drying().drop("water vapor")
dry_biomass["Q"][0]*dry_biomass["DM"][0]*dry_biomass["oDM"][0]*dry_biomass["orgC"][0]


CoFiring(TreeLeavesBerlin, kW=25000).biomass_drying().loc["tree leaves dry", "Q"]*0.65
CoFiring(TreeLeavesBerlin, kW=25000).energy_drying()

CoFiring(TreeLeavesBerlin, kW=20000).output()
CoFiring(TreeLeavesPellets, kW=20000).output()


CoFiring(TreeLeavesBerlin, kW=20000, new_infrastructure=False, retrofitting=True).emissions()
CoFiring(TreeLeavesBerlin, kW=20000, emissions_combustion="Chen_et_al_2020").emissions()
CoFiring(TreeLeavesBerlin, kW=20000, emissions_combustion="GHG_protocol").emissions()
CoFiring(TreeLeavesBerlin, kW=20000, emissions_combustion="volatile_matter").emissions()
convert_molmasses(CoFiring(TreeLeavesBerlin, kW=25000, emissions_combustion="volatile_matter").emissions(), to="molecule")

CoFiring(TreeLeavesBerlin, kW=20000, thermal_efficiency=(True, 0.4)).cashflow()


#area requirements
CoFiring(TreeLeavesBerlin, kW=12000).area_requirements()


7.169182e+06/(7.169182e+06 + 52685.608507 + 298.528334)
52685.608507/(7.169182e+06 + 52685.608507 + 298.528334)
298.528334/(7.169182e+06 + 52685.608507 + 298.528334)