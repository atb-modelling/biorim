import start
import pandas as pd
import numpy as np

from material.biogas import BiogasPlant
from material.composting import CompostingLeaves
from production.plant.street_trees import StreetTrees
from tools import  convert_molmasses, prod, determine_c_n
from read.Ecoinvent import Ecoinvent34


start
#generating maple
TreeLeavesBerlin=StreetTrees(O=prod("tree leaves", 36000, "ton"), origin="Berlin_mixture", id = None).output()
TreeLeavesBerlin14000=StreetTrees(O=prod("tree leaves", 14000, "ton"), origin="Berlin_mixture", id = None).output()
TreeLeavesBerlin3000=StreetTrees(O=prod("tree leaves", 3000, "ton"), origin="Berlin_mixture", id = None).output()
TreeLeavesBerlin["Q"][0]/10000

#Composting Leaves

CompostingLeaves(TreeLeavesBerlin).windrow_specifications()
CompostingLeaves(TreeLeavesBerlin).area_specifications()


CompostingLeaves(TreeLeavesBerlin3000).windrow_specifications()["area facilities"]/1000
CompostingLeaves(TreeLeavesBerlin).windrow_turning_time()
CompostingLeaves(TreeLeavesBerlin).emissions()
CompostingLeaves(TreeLeavesBerlin).output()["Q"]
CompostingLeaves(TreeLeavesBerlin).output()["Q"][0]*35

CompostingLeaves(TreeLeavesBerlin).cashflow()
opex_composting=CompostingLeaves(TreeLeavesBerlin).cashflow()
opex_composting=opex_composting[opex_composting["section"]=="opex"]
opex_composting=opex_composting[opex_composting["EUR"]>0]
opex_composting["EUR"].sum()/34000 #operation cost per tonne of fresh leaves input
opex_composting["EUR"].sum()/23060.48906
opex_composting.loc["personnel", "EUR"]/opex_composting["EUR"].sum()

CompostingLeaves(TreeLeavesBerlin, land_purchase=True).cashflow()


#cash flows and npv
#for cash flows of a compost project, a config dictionary has to be first established

Ecoinvent34('solid manure loading and spreading, by hydraulic loader and spreader', 'solid manure loading and spreading, by hydraulic loader and spreader', unit="kg", location="CH").emissions()