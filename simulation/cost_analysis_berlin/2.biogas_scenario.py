import start
from tools import prod
from analysis.timestep_chains import start_simulation
from tools import convert_molmasses, pie_chart
from simulation.data_management_and_plotting.npv_plotting import npv_graphic

import random
import matplotlib.pyplot as plt


config_biogas_sum = {
    ### Main settings
    "simulation"   :{
        'name': "biogas",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07,
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.42, 0.18, 0.10, 0.07, 0.06, 0.06, 0.05, 0.03, 0.01]],
                'dist_km': [[98, 50, 188, 146, 21, 11, 44, 189, 153]],
                # 'mass_allocation': [[0.202, 0.227, 0.179, 0.126, 0.133, 0.133]],
                # 'dist_km': [[42, 33, 65, 27, 30, 32]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "material.biogas.BiogasPlant_1" :{
                'I': prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'CHP_kW':2700,
                'labour_costs':"FNR_2016",
                'maintenance_costs':"FNR_2016",
                'new_infrastructure': True
                },
                
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='material.biogas.BiogasPlant_1')
                },   
    }
}

# Example results
result_biogas_sum = start_simulation(config_biogas_sum)
npv_graphic(result_biogas_sum, 0) #renting land, transportation=sommer_elso
NPV_sum_method=result_biogas_sum["biogas"]["t1"][0]["npv_value"]
NPV_sum_method

result_biogas_sum["biogas"]["t1"][0]["cashflow"]
result_biogas_sum["biogas"]["t1"][0]["total_cashflow"]


#OCF Method

config_biogas_OCF = {
    ### Main settings
    "simulation"   :{
        'name': "biogas",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07,
            #"method":"sum",
            "method":"OCF"
            #"method":["sum", "OCF"]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.42, 0.18, 0.10, 0.07, 0.06, 0.06, 0.05, 0.03, 0.01]],
                'dist_km': [[98, 50, 188, 146, 21, 11, 44, 189, 153]],
                # 'mass_allocation': [[0.202, 0.227, 0.179, 0.126, 0.133, 0.133]],
                # 'dist_km': [[42, 33, 65, 27, 30, 32]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "material.biogas.BiogasPlant_1" :{
                'I': prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'CHP_kW':2700,
                'labour_costs':"FNR_2016",
                'maintenance_costs':"FNR_2016",
                'new_infrastructure': True
                },
                
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='material.biogas.BiogasPlant_1')
                },   
    }
}

# Example results
result_biogas_OCF = start_simulation(config_biogas_OCF)
npv_graphic(result_biogas_OCF, 0) #renting land, transportation=sommer_elso
NPV_OCF_method=result_biogas_OCF["biogas"]["t1"][0]["npv_value"]
result_biogas_OCF["biogas"]["t1"][0]["cashflow"]
result_biogas_OCF["biogas"]["t1"][0]["total_cashflow"]

(NPV_sum_method-NPV_OCF_method)/NPV_sum_method*100






####ANOTHER SCENARIO FOR DIGESTATE COMPOSTING

config_biogas_NaOH= {
    ### Main settings
    "simulation"   :{
        'name': "biogas and composting",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.202, 0.227, 0.179, 0.126, 0.133, 0.132]],
                'dist_km': [[42, 33, 65, 27, 30, 32]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "production.plant.pretreatment_leaves.PretreatmentLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'method' : "NaOH",
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.pretreatment_leaves.PretreatmentLeaves_1'),
                'CHP_kW':2700,
                'labour_costs':"FNR_2016",
                'maintenance_costs':"FNR_2016",
                'new_infrastructure': True
                },
    
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='material.biogas.BiogasPlant_1')
                },
    }
}

# Example results
result_biogas_NaOH = start_simulation(config_biogas_NaOH)

#npv curve
result_biogas_NaOH["biogas"]["t1"][0]["config"]
npv_graphic(result_biogas_NaOH, 0) #no land purchase, transportation=sommer_elso
result_biogas_NaOH["biogas"]["t1"][0]["npv_value"]

result_biogas_NaOH["biogas"]["t1"][0]["cashflow"]





































#Simulation biogas plant to Hennickendorf (HNK)
config_biogas_HNK = {
    ### Main settings
    "simulation"   :{
        'name': "biogas",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 15000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 15000
                },
        
        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'method' : "silobag",
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'mass_allocation': [[0.202, 0.227, 0.179, 0.126, 0.133, 0.132]],
                'dist_km': [[42, 33, 65, 27, 30, 32]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'CHP_kW':1100,
                'labour_costs':"FNR_2016"
                },
                
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='material.biogas.BiogasPlant_1')
                },
        
    }
}

# Example results
result_biogas_HNK = start_simulation(config_biogas_HNK)
result_biogas_HNK["biogas"]["t1"][0].keys()
result_biogas_HNK["biogas"]["t1"][0]["emissions"].sum().sum()


#npv curve
result_biogas_HNK["biogas"]["t1"][0]["config"]
npv_graphic(result_biogas_HNK, 0) #renting land, transportation=sommer_elso
result_biogas_HNK["biogas"]["t1"][0]["npv_value"]

# result_biogas_HNK["biogas"]["t1"][1]["config"]
# npv_graphic(result_biogas_HNK, 1) #purchasing land, transportation=sommer_elso
# result_biogas_HNK["biogas"]["t1"][1]["npv_value"]


result_biogas_HNK["biogas"]["t1"][0]["cashflow"]
#result_biogas_HNK["biogas"]["t1"][1]["cashflow"]








#biogas plant to Ruhleben (RHL)
#considering co-digestion and digestate spreading

config_biogas_RHL = {
    ### Main settings
    "simulation"   :{
        'name': "biogas",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 15000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.household.food_waste.FoodWaste_1" :{
                'O' : prod("food waste", 60000, "ton"),
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 15000
                },
        
        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'method' : "silobag",
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'mass_allocation': [[0.167, 0.076, 0.086, 0.076, 0.166, 0.259, 0.167]],
                'dist_km': [[24, 15, 6, 15, 19, 23, 18]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "material.biogas.BiogasPlant_1" :{
                'I':prod(['tree leaves', 'food waste'], [100,100], ["%","%"], source=['transport.LandTransportMultiple_1','production.household.food_waste.FoodWaste_1']),
                'CHP_kW':6000,
                'land_purchase': [False, True], 
                'labour_costs':"FNR_2016",
                'first_feedstock_analysis': True
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='material.biogas.BiogasPlant_1')
                },
        
        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}

# Example results
result_biogas_RHL = start_simulation(config_biogas_RHL)
result_biogas_RHL["biogas"]["t1"][0].keys()
result_biogas_RHL["biogas"]["t1"][0]["emissions"]


#npv curve
result_biogas_RHL["biogas"]["t1"][0]["config"]
npv_graphic(result_biogas_RHL, 0) #no land purchase, transportation=sommer_elso
result_biogas_RHL["biogas"]["t1"][0]["npv_value"]

result_biogas_RHL["biogas"]["t1"][1]["config"]
npv_graphic(result_biogas_RHL, 1) #purchasing land, transportation=sommer_elso
result_biogas_RHL["biogas"]["t1"][1]["npv_value"]

result_biogas_RHL["biogas"]["t1"][0]["cashflow"]
result_biogas_RHL["biogas"]["t1"][1]["cashflow"]



