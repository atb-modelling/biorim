import start
from tools import prod
from analysis.timestep_chains import start_simulation
from tools import convert_molmasses
from analysis.economic.macc import MACC, circle_plot_MAC, single_graphic_MACC
from simulation.data_management_and_plotting.npv_plotting import npv_graphic

#NOTE: This set of config is meant for the approach of most likely infrastructure not considering the use of the total amount of leaves

config_composting_berlin = {
    ### Main settings
    "simulation"   :{
        'name': "composting",
        'type': "all combinations", 
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture"
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.42, 0.18, 0.10, 0.07, 0.06, 0.06, 0.05, 0.03, 0.01]],
                'dist_km': [[98, 50, 188, 146, 21, 11, 44, 189, 153]],
                # 'mass_allocation': [[1]],
                # 'dist_km': [[40]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.composting.CompostingLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "Andersen_2010",
                'CH4_emis': 0.02, 
                'land_purchase' : False, 
                'land_rent' : False,
                'fixed_operation_cost' : True,
                'new_infrastructure': False
                },

        "production.plant.fertilization.CompostSpreading_1" :{
                'I': prod('leaves compost', 100, "%", source='material.composting.CompostingLeaves_1'),
                'maturity' : "very mature",
                'method' : "VDLUFA", 
                },
    }
}

result_config_composting_berlin=start_simulation(config_composting_berlin)
# npv_graphic(result_config_composting, 0)
# result_config_composting["composting"]["t1"][0]["cashflow"]
# result_config_composting["composting"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas()


config_pre_NaOH_biogas_berlin = {
    ### Main settings
    "simulation"   :{
        'name': "pretreatment and biogas - with NaOH",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 15000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 12000
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.202, 0.227, 0.179, 0.126, 0.133, 0.132]],
                'dist_km': [[42, 33, 65, 27, 30, 32]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "production.plant.pretreatment_leaves.PretreatmentLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'method' : "NaOH",
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.pretreatment_leaves.PretreatmentLeaves_1'),
                'CHP_kW':1100,
                'land_purchase': True,
                'land_rent': False,
                'labour_costs':"FNR_2016", 
                'maintenance_costs':"FNR_2016",
                'new_infrastructure': True
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}        

result_config_pre_NaOH_biogas_berlin=start_simulation(config_pre_NaOH_biogas_berlin)
#npv_graphic(result_config_pre_NaOH_biogas, 0)
# result_config_pre_NaOH_biogas["pretreatment and biogas - with NaOH"]["t1"][0]["cashflow"]


config_pre_biogas_berlin = {
    ### Main settings
    "simulation"   :{
        'name': "pretreatment and biogas - no NaOH",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 15000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 12000
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.202, 0.227, 0.179, 0.126, 0.133, 0.132]],
                'dist_km': [[42, 33, 65, 27, 30, 32]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.pretreatment_leaves.PretreatmentLeaves_1'),
                'CHP_kW':1100,
                'land_purchase': True,
                'land_rent': False,
                'labour_costs':"FNR_2016", 
                'maintenance_costs':"FNR_2016",
                'new_infrastructure': True
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='material.biogas.BiogasPlant_1')
                },
        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}

result_config_pre_biogas_berlin=start_simulation(config_pre_biogas_berlin)
# npv_graphic(result_config_pre_biogas, 0)
# result_config_pre_biogas["pretreatment and biogas - no NaOH"]["t1"][0]["cashflow"]


config_gasification_berlin = {
    ### Main settings
    "simulation"   :{
        'name': "gasification",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 24000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 20000
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'kW':600,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': False,
                'land_rent': False,
                'new_infrastructure': True
                },
          
    }
}

result_config_gasification_berlin=start_simulation(config_gasification_berlin)
# npv_graphic(result_config_gasification, 0)
# result_config_gasification["gasification"]["t1"][0]["cashflow"]


config_pellet_gasification_berlin = {
    ### Main settings
    "simulation"   :{
        'name': "pelletization and gasification",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 25000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 20000
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.pelletization.PelletFactory_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                },

        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'kW':600,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': True,
                'land_rent': False,
                'new_infrastructure': True
                },
          
    }
}

result_config_pellet_gasification_berlin=start_simulation(config_pellet_gasification_berlin)
#npv_graphic(result_config_pellet_gasification_berlin, 0)
# result_config_pellet_gasification["pelletization and gasification"]["t1"][0]["cashflow"]



config_cofiring_dry_berlin = {
    ### Main settings
    "simulation"   :{
        'name': "cofiring - dry",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'kW':20000,
                'operation_hours':8000, 
                'retrofitting': True, 
                'land_purchase': False,
                'land_rent': False,
                'new_infrastructure': False,
                'dry_biomass': True,
                },
    }
}

result_config_cofiring_dry_berlin=start_simulation(config_cofiring_dry_berlin)
# npv_graphic(result_config_cofiring_dry, 0)
# result_config_cofiring_dry["cofiring - dry"]["t1"][0]["cashflow"]

#NOTE: CHECK pelletization and cofiring
# config_cofiring_pellet = {
#     ### Main settings
#     "simulation"   :{
#         'name': "pelletization and cofiring",
#         'type': "all combinations", # NOTE: All possible parameter combinations are computed
#         'starting_system' : "production.plant.street_trees.StreetTrees_1"
#     },

#     ### Conducted analyses
#     "analyses": {
#         "emissions":{
#             "outtype": "molecule"
#         },
#         "gwp":{
#             "timerange": [20, 100]
#         },
#         "cashflows":{
#             "discount_rate": 0.07
#         },
#     },

#     "systems"   :{
#         "production.plant.street_trees.StreetTrees_1" :{
#                 'O' : prod("tree leaves", 36000, "ton"),
#                 'origin': "Berlin_mixture",
#                 },
        
#         "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
#                 'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
#                 'road_length_km' : 29531
#                 },

#         "transport.LandTransportMultiple_1" :{
#                 'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
#                 'mass_allocation': [[1]],
#                 'dist_km': [[150]],
#                 'type_transp': "lorry, large size",
#                 'method':"LBE",
#                 'load_basis':"mass",
#                 'transport_cost':"sommer_elso"
#                 },

#         "material.pelletization.PelletFactory_1" :{
#                 'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
#                 },

#         "material.cofiring.CoFiring_1" :{
#                 'I': prod('tree leaves pellets', 100, "%", source='material.pelletization.PelletFactory_1'),
#                 'kW':700000,
#                 'operation_hours':8000, 
#                 'retrofitting': False, 
#                 'land_purchase': True,
#                 'land_rent': False,
#                 'new_infrastructure': True
#                 },
#     }
# }

# result_cofiring_pellet = start_simulation(config_cofiring_pellet)


config_cofiring_wet_berlin = {
    ### Main settings
    "simulation"   :{
        'name': "cofiring - wet",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'kW':20000,
                'operation_hours':8000, 
                'retrofitting': True, 
                'land_purchase': False,
                'land_rent': False,
                'new_infrastructure': False,
                'dry_biomass': False,
                },
    }
}

result_config_cofiring_wet_berlin=start_simulation(config_cofiring_wet_berlin)
# result_config_cofiring_wet["cofiring - wet"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas()
# convert_molmasses(result_config_cofiring_wet["cofiring - wet"]["t1"][0]["emissions"], to="molecule")
# result_config_cofiring_wet["cofiring - wet"]["t1"][0]["cashflow"]


###test of MACC function
# list_scenarios=[config_composting, config_biogas_RHL, config_biogas_RHL_comp, config_gasification, config_cofiring]

#NOTE: Check scenario with cofiring and pelletization
#list_scenarios=[result_config_composting, result_config_pre_NaOH_biogas, result_config_pre_biogas, result_config_gasification, result_cofiring_pellet, result_config_pellet_gasification, result_config_cofiring_dry, result_config_cofiring_wet]
list_scenarios_berlin=[result_config_composting_berlin, result_config_pre_NaOH_biogas_berlin, result_config_pre_biogas_berlin, 
                result_config_gasification_berlin, result_config_pellet_gasification_berlin, result_config_cofiring_dry_berlin, 
                result_config_cofiring_wet_berlin]
#marginal abatement test

MACC(list_scenarios_berlin, BAU_scenario="composting").marginal_abatement_cost()
MAC_df_berlin=MACC(list_scenarios_berlin, BAU_scenario="composting").marginal_abatement_cost()
MAC_df_berlin

#graph
single_graphic_MACC(MAC_df_berlin, fixed_df=True, legend_ncol=3)

circle_plot_MAC(MAC_df_berlin, BAU_scenario="composting")
