import start
import pandas as pd
import matplotlib.pyplot as plt

from tools import prod
from analysis.timestep_chains import start_simulation
from tools import convert_molmasses
from analysis.economic.macc import MACC, circle_plot_MAC, single_graphic_MACC, circle_plot_NPV_emissions
from simulation.data_management_and_plotting.npv_plotting import npv_graphic

config_composting_berlin = {
    ### Main settings
    "simulation"   :{
        'name': "composting",
        'type': "all combinations", 
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture"
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.42, 0.18, 0.10, 0.07, 0.06, 0.06, 0.05, 0.03, 0.01]],
                'dist_km': [[98, 50, 188, 146, 21, 11, 44, 189, 153]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.composting.CompostingLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "Andersen_2010",
                'CH4_emis': 0.02, 
                'land_purchase' : False, 
                'land_rent' : False,
                'fixed_operation_cost' : True,
                'new_infrastructure': False
                },

        "production.plant.fertilization.CompostSpreading_1" :{
                'I': prod('leaves compost', 100, "%", source='material.composting.CompostingLeaves_1'),
                'maturity' : "very mature",
                'method' : "VDLUFA", 
                },
    }
}

result_config_composting_berlin=start_simulation(config_composting_berlin)
# npv_graphic(result_config_composting_berlin, 0)
result_config_composting_berlin["composting"]["t1"][0]["cashflow"]
# result_config_composting["composting"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas()


config_pre_NaOH_biogas_berlin = {
    ### Main settings
    "simulation"   :{
        'name': "biogas (with NaOH pretreatment)",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.09, 0.10, 0.08, 0.06, 0.06, 0.06, 0.09, 0.04, 0.05, 0.04, 0.09, 0.15, 0.09]],
                'dist_km': [[42, 33, 65, 27, 30, 32, 75, 79, 51, 72, 67, 47, 85]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag", #this is considering investment of tractors has CAPEX
                },

        "production.plant.pretreatment_leaves.PretreatmentLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'method' : "NaOH", #no CAPEX, only OPEX
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.pretreatment_leaves.PretreatmentLeaves_1'),
                'CHP_kW':2700,
                'land_purchase': True,
                'land_rent': False,
                'labour_costs':"FNR_2016", 
                'maintenance_costs':"FNR_2016",
                'new_infrastructure': True,
                'thermal_energy_tariff':("manual", 0.03)
                },
        
        "transport.LandTransportMultiple_2" :{
                'I': prod('biogas digestate', 100, "%", source='material.biogas.BiogasPlant_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}        

result_config_pre_NaOH_biogas_berlin=start_simulation(config_pre_NaOH_biogas_berlin)
#npv_graphic(result_config_pre_NaOH_biogas, 0)
result_config_pre_NaOH_biogas_berlin["biogas (with NaOH pretreatment)"]["t1"][0]["cashflow"]
result_config_pre_NaOH_biogas_berlin["biogas (with NaOH pretreatment)"]["t1"][0]["outputs"]
result_config_pre_NaOH_biogas_berlin["biogas (with NaOH pretreatment)"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas()

config_pre_biogas_berlin = {
    ### Main settings
    "simulation"   :{
        'name': "biogas",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.09, 0.10, 0.08, 0.06, 0.06, 0.06, 0.09, 0.04, 0.05, 0.04, 0.09, 0.15, 0.09]],
                'dist_km': [[42, 33, 65, 27, 30, 32, 75, 79, 51, 72, 67, 47, 85]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'CHP_kW':2700,
                'land_purchase': True,
                'land_rent': False,
                'labour_costs':"FNR_2016", 
                'maintenance_costs':"FNR_2016",
                'new_infrastructure': True,
                'thermal_energy_tariff':("manual", 0.03)
                },
        
        "transport.LandTransportMultiple_2" :{
                'I': prod('biogas digestate', 100, "%", source='material.biogas.BiogasPlant_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}

result_config_pre_biogas_berlin=start_simulation(config_pre_biogas_berlin)
# npv_graphic(result_config_pre_biogas, 0)
result_config_pre_biogas_berlin["biogas"]["t1"][0]["cashflow"]
result_config_pre_biogas_berlin["biogas"]["t1"][0]["outputs"]




config_gasification_berlin = {
    ### Main settings
    "simulation"   :{
        'name': "gasification",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.09, 0.10, 0.08, 0.06, 0.06, 0.06, 0.09, 0.04, 0.05, 0.04, 0.09, 0.15, 0.09]],
                'dist_km': [[42, 33, 65, 27, 30, 32, 75, 79, 51, 72, 67, 47, 85]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'kW':4000,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': False,
                'land_rent': False,
                'new_infrastructure': False,
                'thermal_energy_tariff':("manual", 0.03)
                },

        "production.plant.fertilization.BiocharSpreading_1" :{
                'I': prod("unconverted biochar", 100, "%", source='material.gasification.Gasification_1')
                },    
                
        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.gasification.Gasification_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.gasification.Gasification_1')
                },
          
    }
}

result_config_gasification_berlin=start_simulation(config_gasification_berlin)
# npv_graphic(result_config_gasification_berlin, 0)
result_config_gasification_berlin["gasification"]["t1"][0]["cashflow"]
result_config_gasification_berlin["gasification"]["t1"][0]["outputs"]
result_config_gasification_berlin["gasification"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas()


config_pellet_gasification_berlin = {
    ### Main settings
    "simulation"   :{
        'name': "gasification (with pelletization)",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.09, 0.10, 0.08, 0.06, 0.06, 0.06, 0.09, 0.04, 0.05, 0.04, 0.09, 0.15, 0.09]],
                'dist_km': [[42, 33, 65, 27, 30, 32, 75, 79, 51, 72, 67, 47, 85]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.pelletization.PelletFactory_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'land_purchase': True, 
                'land_rent': False,
                'new_infrastructure': True
                },

        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves pellets', 100, "%", source='material.pelletization.PelletFactory_1'),
                'kW': 4000,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': False,
                'land_rent': False,
                'new_infrastructure': False,
                'thermal_energy_tariff':("manual", 0.03)
                },

        "production.plant.fertilization.BiocharSpreading_1" :{
                'I': prod("unconverted biochar", 100, "%", source='material.gasification.Gasification_1')
                },    

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.gasification.Gasification_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.gasification.Gasification_1')
                },
          
    }
}

result_config_pellet_gasification_berlin=start_simulation(config_pellet_gasification_berlin)
# npv_graphic(result_config_pellet_gasification_berlin, 0)
result_config_pellet_gasification_berlin["gasification (with pelletization)"]["t1"][0]["cashflow"]
result_config_pellet_gasification_berlin["gasification (with pelletization)"]["t1"][0]["outputs"]
result_config_pellet_gasification_berlin["gasification (with pelletization)"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas()

config_cofiring_dry_berlin = {
    ### Main settings
    "simulation"   :{
        'name': "cofiring",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.09, 0.10, 0.08, 0.06, 0.06, 0.06, 0.09, 0.04, 0.05, 0.04, 0.09, 0.15, 0.09]],
                'dist_km': [[13, 16, 30, 37, 19, 20, 24, 10, 17, 16, 22, 3, 23]], 
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.cofiring.BiomassDrying_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'new_infrastructure': True
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves dry', 100, "%", source='material.cofiring.BiomassDrying_1'),
                'kW':12000,
                'operation_hours':8000, 
                'electrical_efficiency': 0.36,
                'retrofitting': True, 
                'land_purchase': False,
                'land_rent': False,
                'new_infrastructure': False,
                'include_thermal': True,
                'emissions_combustion':"carbon_content",
                "fixed_carbon_share":0.05
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.cofiring.CoFiring_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },    

        "external.Electricity_1" :{
                'I': prod('electrical energy - biomass share', 100, "%", source='material.cofiring.CoFiring_1')
                },
    }
}

result_config_cofiring_dry_berlin=start_simulation(config_cofiring_dry_berlin)
# npv_graphic(result_config_cofiring_dry, 0)
result_config_cofiring_dry_berlin["cofiring"]["t1"][0]["cashflow"]
result_config_cofiring_dry_berlin["cofiring"]["t1"][0]["outputs"]
result_config_cofiring_dry_berlin["cofiring"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas()


config_cofiring_dry_pellet_berlin= {
    ### Main settings
    "simulation"   :{
        'name': "cofiring (with pelletization)",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.09, 0.10, 0.08, 0.06, 0.06, 0.06, 0.09, 0.04, 0.05, 0.04, 0.09, 0.15, 0.09]],
                'dist_km': [[13, 16, 30, 37, 19, 20, 24, 10, 17, 16, 22, 3, 23]], 
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.pelletization.PelletFactory_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'land_purchase': True, 
                'land_rent': False,
                'new_infrastructure': True
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves pellets', 100, "%", source='material.pelletization.PelletFactory_1'),
                'kW':12000,
                'operation_hours':8000, 
                'electrical_efficiency': 0.36,
                'retrofitting': True, 
                'land_purchase': False,
                'land_rent': False,
                'new_infrastructure': False,
                'include_thermal': True,
                'emissions_combustion':"carbon_content",
                "fixed_carbon_share":0.05
                },
        
        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.cofiring.CoFiring_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },    

        "external.Electricity_1" :{
                'I': prod('electrical energy - biomass share', 100, "%", source='material.cofiring.CoFiring_1')
                },
    }
}

result_config_cofiring_dry_pellet_berlin=start_simulation(config_cofiring_dry_pellet_berlin)
# npv_graphic(result_config_cofiring_dry, 0)
result_config_cofiring_dry_pellet_berlin["cofiring (with pelletization)"]["t1"][0]["cashflow"]
result_config_cofiring_dry_pellet_berlin["cofiring (with pelletization)"]["t1"][0]["outputs"]
result_config_cofiring_dry_pellet_berlin["cofiring (with pelletization)"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas()


###test of MACC function
# list_scenarios=[config_composting, config_biogas_RHL, config_biogas_RHL_comp, config_gasification, config_cofiring]

#NOTE: Check scenario with cofiring and pelletization
#list_scenarios=[result_config_composting, result_config_pre_NaOH_biogas, result_config_pre_biogas, result_config_gasification, result_cofiring_pellet, result_config_pellet_gasification, result_config_cofiring_dry, result_config_cofiring_wet]
list_scenarios_berlin=[result_config_composting_berlin, result_config_pre_NaOH_biogas_berlin, result_config_pre_biogas_berlin, 
                result_config_gasification_berlin, result_config_pellet_gasification_berlin, result_config_cofiring_dry_berlin, 
                result_config_cofiring_dry_pellet_berlin]
#marginal abatement test

MACC(list_scenarios_berlin, BAU_scenario="composting").marginal_abatement_cost()
MAC_df_berlin=MACC(list_scenarios_berlin, BAU_scenario="composting").marginal_abatement_cost()
MAC_df_berlin


#graph
# single_graphic_MACC(MAC_df_berlin, fixed_df=True, ncol=2)

circle_plot_NPV_emissions(MAC_df_berlin, BAU_scenario="composting", size_factor=2500, fontsize=20, ncol=3, font=16)


##relationship between electrical output and emissions
#adding electricity ouputs to the MAC_df

# for scenario in list_scenarios_berlin:
#     key=list(scenario.keys())[0]
#     outputs=scenario[key]["t1"][0]["outputs"]
#     #print(outputs)
#     try:
#         produced_energy=outputs.iloc[outputs.index.get_level_values(2).str.contains("Elec", na=False)].sum()["Q"]
#         consumed_energy=outputs.iloc[outputs.index.get_level_values(0).str.contains("Elec", na=False)].sum()["Q"]
#         MAC_df_berlin.loc[key, "Electricity (kWh)"]=produced_energy-consumed_energy
#     except:
#         TypeError
# MAC_df_berlin
# MAC_df_berlin.to_csv(r"C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\MAC_df_Berlin.csv", sep=";")

# # fig, ax=plt.subplots()
# # ax.bar(MAC_df_berlin.index.drop("composting"), MAC_df_berlin.drop("composting")["emissions - GWP100"], label="Emissions")
# # ax.tick_params("x", rotation=45)
# # ax1=ax.twinx()
# # ax1.plot(MAC_df_berlin.index.drop("composting"), MAC_df_berlin.drop("composting")["Electricity (kWh)"], label="Electricity (kWh)", marker="h", color="k")
# # fig.legend(loc=9, ncol=2)
# # plt.show()