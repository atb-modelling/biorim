import start
import pandas as pd

from tools import prod
from production.plant.street_trees import StreetTrees
from material.cofiring import CoFiring, BiomassDrying
from tools import convert_molmasses

### Carbon accumulation in the tree leaves: Considered as negative emissions!
StreetTrees(O=prod("tree leaves", 1, "ton")).emissions()    # Every tonne of leaves is storing 212 kg of carbon

# As calculated from the carbon content
Leaves = StreetTrees(O=prod("tree leaves", 1, "ton")).output()
Leaves["Q"]*Leaves["DM"]*Leaves["oDM"]*Leaves["orgC"]*1000 # 212 kg tonnes of carbon input

#NOTE: Leaves should be dried, otherwise will alter the volatile matter approach, because depends on the dry matter content
DriedLeaves=BiomassDrying(Leaves).output()

### Material carbon output and positive carbon emissions later on:

# Carbon in ashes
Ashes = CoFiring(I=DriedLeaves).output().loc["ashes",:] # 0.15 tonnes of ashes

# According to Lanzerstorfer 2015 Table 3, total carbon in ashes is usually lower than 5%
# https://www.sciencedirect.com/science/article/pii/S100107421500025X

Ashes["Q"] * 1000 * 0.05    # Maybe 7.5 kg of carbon in the ashes

# Carbon in emissions
CoFiring(I=Leaves, emissions_combustion="volatile_matter").emissions().loc["biomass combustion",["CH4-C", "CO2-C"]].sum()  # 0.001 kg as CO2 and CH4 emissions
CoFiring(I=Leaves, emissions_combustion="GHG_protocol").emissions().loc["biomass combustion",["CH4-C", "CO2-C"]].sum()  # 0.005 kg as CO2 and CH4 emissions
CoFiring(I=Leaves, emissions_combustion="carbon_content").emissions()



#volatile_matter of tree leaves
volatile_matter=Leaves["Q"]*Leaves["DM"]*Leaves["oDM"] * 0.86
volatile_matter *1000 # 11.7 million kg volatile


TreeLeavesBerlin=StreetTrees(O=prod("tree leaves", 36000, "ton")).output()
TreeLeavesBerlin["Q"]*TreeLeavesBerlin["DM"]*TreeLeavesBerlin["oDM"]*TreeLeavesBerlin["orgC"]*1000 #7.6 million kg carbon
TreeLeavesBerlin

volatile_matter_berlin=TreeLeavesBerlin["Q"]*TreeLeavesBerlin["DM"]*TreeLeavesBerlin["oDM"] * 0.86
volatile_matter_berlin *1000 # 11.7 million kg volatile

#Checking operation share
DriedLeavesBerlin=BiomassDrying(TreeLeavesBerlin).output()
DriedLeavesBerlin

CoFiring(I=DriedLeaves, emissions_combustion="volatile_matter", kW=10000).operation_share() #74% of the total capacity for a 12000 kW plant (15% for cleaning)

CoFiring(I=DriedLeaves, emissions_combustion="volatile_matter", kW=10000).emissions()
volatile_matter_berlin[0] * 0.992 * 1000

CoFiring(I=TreeLeavesBerlin, emissions_combustion="GHG_protocol", kW=12000).emissions()


emis_combustion_manual=pd.DataFrame(index=[], columns=[])
emis_combustion_manual.loc["biomass combustion", "CO2"] = volatile_matter_berlin[0] * 0.992 * 1000  
emis_combustion_manual.loc["biomass combustion", "CH4"] = volatile_matter_berlin[0] * 0.007 * 1000
emis_combustion_manual.loc["biomass combustion", "N2O"] = volatile_matter_berlin[0] * 0.001 * 1000
emis_combustion_manual

convert_molmasses(emis_combustion_manual, to="element")

emis_moleculal_GHG_protocol=pd.DataFrame(index=[], columns=[])
emis_moleculal_GHG_protocol.loc["emissions", "CO2"]=1747.2
emis_moleculal_GHG_protocol.loc["emissions", "CH4"]=4.86
emis_moleculal_GHG_protocol.loc["emissions", "N2O"]=0.0624

emis_moleculal_GHG_protocol.loc["%", "CO2"]=emis_moleculal_GHG_protocol.loc["emissions", "CO2"]/(1747.2+4.86)
emis_moleculal_GHG_protocol.loc["%", "CH4"]=4.86/(1747.2+4.86)

emis_moleculal_GHG_protocol


emis_moleculal_GHG_protocol_element=convert_molmasses(emis_moleculal_GHG_protocol, to="element")
emis_moleculal_GHG_protocol_element.loc["%", "CO2-C"]=476.985600/(476.985600+3.640140)
emis_moleculal_GHG_protocol_element.loc["%", "CH4-C"]=3.640140/(476.985600+3.640140)
emis_moleculal_GHG_protocol_element


CoFiring(I=Leaves, emissions_combustion="volatile_matter").emissions() 

CoFiring(I=Leaves, emissions_combustion="GHG_protocol").emissions()



