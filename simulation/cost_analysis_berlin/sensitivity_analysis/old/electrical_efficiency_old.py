import start
from tools import prod
from analysis.timestep_chains import start_simulation
from tools import convert_molmasses
from analysis.economic.macc import MACC, circle_plot_MAC, single_graphic_MACC, circle_plot_NPV_emissions
from simulation.data_management_and_plotting.npv_plotting import npv_graphic

import matplotlib.pyplot as plt
import pandas as pd


config_composting_elec_eff = {
    ### Main settings
    "simulation"   :{
        'name': "composting",
        'type': "all combinations", 
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture"
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.42, 0.18, 0.10, 0.07, 0.06, 0.06, 0.05, 0.03, 0.01]],
                'dist_km': [[98, 50, 188, 146, 21, 11, 44, 189, 153]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.composting.CompostingLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "Andersen_2010",
                'CH4_emis': 0.02, 
                'land_purchase' : True, 
                'land_rent' : False,
                'fixed_operation_cost' : True,
                'new_infrastructure': True
                },

        "production.plant.fertilization.CompostSpreading_1" :{
                'I': prod('leaves compost', 100, "%", source='material.composting.CompostingLeaves_1'),
                'maturity' : "very mature",
                'method' : "VDLUFA", 
                },
    }
}

result_config_composting_elec_eff=start_simulation(config_composting_elec_eff)

#####

config_pre_NaOH_biogas_elec_eff = {
    ### Main settings
    "simulation"   :{
        'name': "pretreatment and biogas - with NaOH",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                # 'mass_allocation': [[0.202, 0.227, 0.179, 0.126, 0.133, 0.132]],
                # 'dist_km': [[42, 33, 65, 27, 30, 32]],
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "production.plant.pretreatment_leaves.PretreatmentLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'method' : "NaOH",
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.pretreatment_leaves.PretreatmentLeaves_1'),
                'CHP_kW':2500,
                'land_purchase': True,
                'land_rent': False,
                'labour_costs':"FNR_2016", 
                'maintenance_costs':"FNR_2016",
                'new_infrastructure': True,
                'CHP_EFFICIENCY_ELECTRICAL': [0.38, 0.30, 0.34, 0.42, 0.46]
                },
        
        "transport.LandTransportMultiple_2" :{
                'I': prod('biogas digestate', 100, "%", source='material.biogas.BiogasPlant_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}        

result_config_pre_NaOH_biogas_elec_eff=start_simulation(config_pre_NaOH_biogas_elec_eff)

#######

config_pre_biogas_elec_eff = {
    ### Main settings
    "simulation"   :{
        'name': "pretreatment and biogas - no NaOH",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                # 'mass_allocation': [[0.202, 0.227, 0.179, 0.126, 0.133, 0.132]],
                # 'dist_km': [[42, 33, 65, 27, 30, 32]],
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'CHP_kW':2700,
                'land_purchase': True,
                'land_rent': False,
                'labour_costs':"FNR_2016", 
                'maintenance_costs':"FNR_2016",
                'new_infrastructure': True, 
                'CHP_EFFICIENCY_ELECTRICAL': [0.38, 0.30, 0.34, 0.42, 0.46]
                },
        
        "transport.LandTransportMultiple_2" :{
                'I': prod('biogas digestate', 100, "%", source='material.biogas.BiogasPlant_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}

result_config_pre_biogas_elec_eff=start_simulation(config_pre_biogas_elec_eff)
# npv_graphic(result_config_pre_biogas_elec_eff, 2)
# result_config_pre_biogas_elec_eff["pretreatment and biogas - no NaOH"]["t1"][0]["cashflow"]

# list_elec_eff_biogas= [0.30, 0.34, 0.38, 0.42, 0.46]
# list_npv_biogas_elec_eff=[]
# for i in range(0,len(list_elec_eff_biogas)):
#     list_npv_biogas_elec_eff.append(result_config_pre_biogas_elec_eff["pretreatment and biogas - no NaOH"]["t1"][i]["npv_value"])
# list_npv_biogas_elec_eff

# list_GHG_biogas_elec_eff=[]
# for i in range(0,len(list_elec_eff_biogas)):
#     list_GHG_biogas_elec_eff.append(result_config_pre_biogas_elec_eff["pretreatment and biogas - no NaOH"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
# list_GHG_biogas_elec_eff


#gasification
config_gasification_elec_eff = {
    ### Main settings
    "simulation"   :{
        'name': "gasification",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },


        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'kW':3600,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': False,
                'land_rent': False,
                'new_infrastructure': False,
                'electrical_energy_eff':[0.25, 0.17, 0.21, 0.29, 0.33]
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.gasification.Gasification_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.gasification.Gasification_1')
                },

    },
}

result_config_gasification_elec_eff=start_simulation(config_gasification_elec_eff)
# list_elec_eff_gasification=[0.17, 0.21, 0.25, 0.29, 0.33]
# list_npv_gasification_elec_eff=[]
# for i in range(0,len(list_elec_eff_gasification)):
#     list_npv_gasification_elec_eff.append(result_config_gasification_elec_eff["gasification"]["t1"][i]["npv_value"])
# list_npv_gasification_elec_eff

# list_GHG_gasification_elec_eff=[]
# for i in range(0,len(list_elec_eff_gasification)):
#     list_GHG_gasification_elec_eff.append(result_config_gasification_elec_eff["gasification"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
# list_GHG_gasification_elec_eff


# result_config_gasification_elec_eff["gasification"]["t1"][0]["cashflow"]
# result_config_gasification_elec_eff["gasification"]["t1"][1]["cashflow"]
# result_config_gasification_elec_eff["gasification"]["t1"][2]["cashflow"]
# result_config_gasification_elec_eff["gasification"]["t1"][3]["cashflow"]
# result_config_gasification_elec_eff["gasification"]["t1"][4]["cashflow"]

# result_config_gasification_elec_eff["gasification"]["t1"][0]["outputs"]
# result_config_gasification_elec_eff["gasification"]["t1"][1]["outputs"]
# result_config_gasification_elec_eff["gasification"]["t1"][2]["outputs"]
# result_config_gasification_elec_eff["gasification"]["t1"][3]["outputs"]
# result_config_gasification_elec_eff["gasification"]["t1"][4]["outputs"]


#pelletization and gasification

config_pellet_gasification_elec_eff = {
    ### Main settings
    "simulation"   :{
        'name': "pelletization and gasification",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.pelletization.PelletFactory_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'land_purchase': True, 
                'land_rent': False,
                'new_infrastructure': True
                },

        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves pellets', 100, "%", source='material.pelletization.PelletFactory_1'),
                'kW': 4000,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': True,
                'land_rent': False,
                'new_infrastructure': False,
                'electrical_energy_eff':[0.25, 0.17, 0.21, 0.29, 0.33]
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.gasification.Gasification_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.gasification.Gasification_1')
                },

    }
}
result_config_pellet_gasification_elec_eff=start_simulation(config_pellet_gasification_elec_eff)

# list_elec_eff_gasification=[0.17, 0.21, 0.25, 0.29, 0.33]
# list_npv_pellet_gasification_elec_eff=[]
# for i in range(0,len(list_elec_eff_gasification)):
#     list_npv_pellet_gasification_elec_eff.append(result_config_pellet_gasification_elec_eff["pelletization and gasification"]["t1"][i]["npv_value"])
# list_npv_pellet_gasification_elec_eff

# list_GHG_pellet_gasification_elec_eff=[]
# for i in range(0,len(list_elec_eff_gasification)):
#     list_GHG_pellet_gasification_elec_eff.append(result_config_pellet_gasification_elec_eff["pelletization and gasification"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
# list_GHG_pellet_gasification_elec_eff


#cofiring - dry
config_cofiring_dry_elec_eff = {
    ### Main settings
    "simulation"   :{
        'name': "cofiring - dry",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.cofiring.BiomassDrying_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'new_infrastructure': True
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves dry', 100, "%", source='material.cofiring.BiomassDrying_1'),
                'kW':12000,
                'operation_hours':8000, 
                'retrofitting': True, 
                'land_purchase': False,
                'land_rent': False,
                'new_infrastructure': False,
                'electrical_efficiency': [0.36, 0.28, 0.32, 0.40, 0.44]
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.cofiring.CoFiring_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },    

        "external.Electricity_1" :{
                'I': prod('electrical energy - biomass share', 100, "%", source='material.cofiring.CoFiring_1')
                },
                
    }
}

result_config_cofiring_dry_elec_eff=start_simulation(config_cofiring_dry_elec_eff)



config_cofiring_dry_pellet_elec_eff= {
    ### Main settings
    "simulation"   :{
        'name': "cofiring and pelletization",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.pelletization.PelletFactory_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'land_purchase': True, 
                'land_rent': False,
                'new_infrastructure': True
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves pellets', 100, "%", source='material.pelletization.PelletFactory_1'),
                'kW':10000,
                'operation_hours':8000, 
                'retrofitting': False, 
                'land_purchase': True,
                'land_rent': False,
                'new_infrastructure': True,
                # 'thermal_efficiency': (True, 0.2)
                'electrical_efficiency': [0.36, 0.28, 0.32, 0.40, 0.44]
                },
        
        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.cofiring.CoFiring_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },    

        "external.Electricity_1" :{
                'I': prod('electrical energy - biomass share', 100, "%", source='material.cofiring.CoFiring_1')
                },
    }
}

result_config_dry_pellet_elec_eff=start_simulation(config_cofiring_dry_pellet_elec_eff)

list_electrical_efficiency=[result_config_composting_elec_eff, result_config_pre_NaOH_biogas_elec_eff, result_config_pre_biogas_elec_eff, 
                            result_config_gasification_elec_eff, result_config_pellet_gasification_elec_eff, result_config_cofiring_dry_elec_eff, 
                            result_config_dry_pellet_elec_eff]

df_sensitivity_elec_eff=MACC(list_electrical_efficiency, BAU_scenario="composting", sensitivity=True, obs="electrical efficiency").marginal_abatement_cost()
df_sensitivity_elec_eff
circle_plot_NPV_emissions(df_sensitivity_elec_eff, fixed_df=True, legend_ncol=3, sensitivity=True, size_factor=1000)


