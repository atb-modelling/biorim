import matplotlib.pyplot as plt
import pandas as pd
from simulation.data_management_and_plotting.scenarios_plotting import LegendWithoutDuplicate as lwd

#TODO: use the legend without duplicate

def unique_handles_labels(ax1, ax2=None):
    h=[]
    l=[]
    handles, labels = ax1.get_legend_handles_labels()
    for handle, label in zip(handles, labels):
        if label not in l:
            h.append(handle)
            l.append(label)

    handles2, labels2 = ax2.get_legend_handles_labels()
    for handle, label in zip(handles2, labels2):
        if label not in l:
            h.append(handle)
            l.append(label)
    return (h, l)

scenario_colors={   "compost": "#D49C36", #1
                    "biogas": "#60CC9E",#1 
                    "gasification": "#28864A",#2
                    "cofiring": "#1A639E",#3
                    "pretreatment - biogas": "#1F2988",#4
                    "pretreatment (NaOH) - biogas": "#bf00ff",
                    "biogas and composting":"#AB4242", #5
                    "pelletization and gasification":"#D49C36",#6
                    "cofiring-dry":"#CFD25F",#7 
                    "cofiring-wet":"#B3B296", #8
                    "pelletization and cofiring":"#676EA6"}


list_scenarios=["pretreatment (NaOH) - biogas", "pretreatment - biogas", "gasification", "pelletization and gasification", "cofiring-dry", "cofiring-wet"]


#transportation sensitivity
fig=plt.figure()
ax1=fig.add_subplot(321)
for i in list_scenarios:
    ax1.plot(GHG_variation.index, GHG_variation[i], label=i, marker="v", color=scenario_colors[i])
ax1.set_xlabel("Distance (km)")
#ax1.set_title("(a): Emissions")
ax1.set_title("(a)")
ax2=fig.add_subplot(322)
for i in list_scenarios:
    ax2.plot(npv_variation.index, npv_variation[i], label=i, marker="*", color=scenario_colors[i])
ax2.set_xlabel("Distance (km)")
#ax2.set_title("(b): NPV")
ax2.set_title("(b)")

#operation hours
ax3=fig.add_subplot(323, sharey=ax1)
for i in list_scenarios:
    ax3.plot(GHG_variation_operhours.index, GHG_variation_operhours[i], label=i, marker="v", color=scenario_colors[i])
ax3.set_xlabel("Operation hours")
# ax3.set_title("(c): Emissions")
ax3.set_title("(c)")
ax4=fig.add_subplot(324 )
for i in list_scenarios:
    ax4.plot(npv_variation_operhours.index, npv_variation_operhours[i], label=i, marker="*", color=scenario_colors[i])
ax4.set_xlabel("Operation hours")
# ax4.set_title("(d): NPV")
ax4.set_title("(d)")

#electrical efficiency
ax5=fig.add_subplot(325, sharey=ax1)
for i in list_scenarios:
    ax5.plot(GHG_variation_elec_eff.index, GHG_variation_elec_eff[i], label=i, marker="v", color=scenario_colors[i])
ax5.set_xlabel("Electrical efficiency")
# ax5.set_title("(e): Emissions")
ax5.set_title("(e)")

ax6=fig.add_subplot(326)
for i in list_scenarios:
    ax6.plot(npv_variation_elec_eff.index, npv_variation_elec_eff[i], label=i, marker="*", color=scenario_colors[i])
ax6.set_xlabel("Electrical efficiency")
# ax6.set_title("(f): NPV")
ax6.set_title("(f)")
fig.supylabel("Emissions abatement (tonne CO₂eq)")
ax7=fig.add_subplot(122, frameon=False) 
ax7.set_yticks([]) 
ax7.set_xticks([])
ax7.set_ylabel("Net present value (EUR)", fontsize=12)
ax7.yaxis.set_label_coords(-0.18, 0.5)
unique_handles, unique_labels = unique_handles_labels(ax1=ax1, ax2=ax2)
plt.subplots_adjust(top=0.91,
                    bottom=0.075,
                    left=0.085,
                    right=0.92,
                    hspace=0.45,
                    wspace=0.5)
fig.set_size_inches(7,10)
fig.legend(unique_handles, unique_labels, loc=9, ncol=3)
plt.show()







































# ##copy

# fig=plt.figure()
# ax1=fig.add_subplot(321)
# # for i in list_scenarios:
# #     ax1.plot(GHG_variation.index, GHG_variation[i], label=i, marker="v", color=scenario_colors[i])
# ax1.plot(GHG_variation.index, GHG_variation["pretreatment (NaOH) - biogas"], label="pretreatment (NaOH) - biogas", marker="v", color=scenario_colors["pretreatment (NaOH) - biogas"])
# ax1.plot(GHG_variation.index, GHG_variation["pretreatment - biogas"], label="pretreatment - biogas", marker="v", color=scenario_colors["pretreatment - biogas"])
# ax1.plot(GHG_variation.index, GHG_variation["gasification"], label="gasification", marker="v", color=scenario_colors["gasification"])
# ax1.plot(GHG_variation.index, GHG_variation["pelletization and gasification"], label="pelletization and gasification", marker="v", color=scenario_colors["pelletization and gasification"])
# ax1.plot(GHG_variation.index, GHG_variation["cofiring-dry"], label="cofiring-dry", marker="v", color=scenario_colors["cofiring-dry"])
# ax1.plot(GHG_variation.index, GHG_variation["cofiring-wet"], label="cofiring-wet", marker="v", color=scenario_colors["cofiring-wet"])
# ax1.set_xlabel("Distance (km)")
# #ax1.set_ylabel("Emissions abatement (kg CO2eq)")
# ax2=fig.add_subplot(322, sharey=ax1 )
# ax2.plot(npv_variation.index, npv_variation["pretreatment (NaOH) - biogas"], label="pretreatment (NaOH) - biogas", marker="*", color=scenario_colors["pretreatment (NaOH) - biogas"])
# ax2.plot(npv_variation.index, npv_variation["pretreatment - biogas"], label="pretreatment - biogas", marker="*", color=scenario_colors["pretreatment - biogas"])
# ax2.plot(npv_variation.index, npv_variation["gasification"], label="gasification", marker="*", color=scenario_colors["gasification"])
# ax2.plot(npv_variation.index, npv_variation["pelletization and gasification"], label="pelletization and gasification", marker="*", color=scenario_colors["pelletization and gasification"])
# ax2.plot(npv_variation.index, npv_variation["cofiring-dry"], label="cofiring-dry", marker="*", color=scenario_colors["cofiring-dry"])
# ax2.plot(npv_variation.index, npv_variation["cofiring-wet"], label="cofiring-wet", marker="*", color=scenario_colors["cofiring-wet"])
# ax2.set_xlabel("Distance (km)")
# ax2.set_ylabel("NPV")


# #operation hours
# ax3=fig.add_subplot(323, sharey=ax1)
# ax3.plot(GHG_variation_operhours.index, GHG_variation_operhours["pretreatment (NaOH) - biogas"], label="pretreatment (NaOH) - biogas", marker="v", color=scenario_colors["pretreatment (NaOH) - biogas"])
# ax3.plot(GHG_variation_operhours.index, GHG_variation_operhours["pretreatment - biogas"], label="pretreatment - biogas", marker="v", color=scenario_colors["pretreatment - biogas"])
# ax3.plot(GHG_variation_operhours.index, GHG_variation_operhours["gasification"], label="gasification", marker="v", color=scenario_colors["gasification"])
# ax3.plot(GHG_variation_operhours.index, GHG_variation_operhours["pelletization and gasification"], label="pelletization and gasification", marker="v", color=scenario_colors["pelletization and gasification"])
# ax3.plot(GHG_variation_operhours.index, GHG_variation_operhours["cofiring-dry"], label="cofiring-dry", marker="v", color=scenario_colors["cofiring-dry"])
# ax3.plot(GHG_variation_operhours.index, GHG_variation_operhours["cofiring-wet"], label="cofiring-wet", marker="v", color=scenario_colors["cofiring-wet"])
# ax3.set_xlabel("Operation hours")
# #ax3.set_ylabel("Emissions abatement (kg CO2eq)")
# ax4=fig.add_subplot(324, sharey=ax3 )
# ax4.plot(npv_variation_operhours.index, npv_variation_operhours["pretreatment (NaOH) - biogas"], label="pretreatment (NaOH) - biogas", marker="*", color=scenario_colors["pretreatment (NaOH) - biogas"])
# ax4.plot(npv_variation_operhours.index, npv_variation_operhours["pretreatment - biogas"], label="pretreatment - biogas", marker="*", color=scenario_colors["pretreatment - biogas"])
# ax4.plot(npv_variation_operhours.index, npv_variation_operhours["gasification"], label="gasification", marker="*", color=scenario_colors["gasification"])
# ax4.plot(npv_variation_operhours.index, npv_variation_operhours["pelletization and gasification"], label="pelletization and gasification", marker="*", color=scenario_colors["pelletization and gasification"])
# ax3.plot(npv_variation_operhours.index, npv_variation_operhours["cofiring-dry"], label="cofiring-dry", marker="*", color=scenario_colors["cofiring-dry"])
# ax4.plot(npv_variation_operhours.index, npv_variation_operhours["cofiring-wet"], label="cofiring-wet", marker="*", color=scenario_colors["cofiring-wet"])
# ax4.set_xlabel("Operation hours")
# ax4.set_ylabel("NPV")

# #electrical efficiency
# ax5=fig.add_subplot(325, sharey=ax1)
# ax5.plot(GHG_variation_elec_eff.index, GHG_variation_elec_eff["pretreatment (NaOH) - biogas"], label="pretreatment (NaOH) - biogas", marker="v", color=scenario_colors["pretreatment (NaOH) - biogas"])
# ax5.plot(GHG_variation_elec_eff.index, GHG_variation_elec_eff["pretreatment - biogas"], label="pretreatment - biogas", marker="v", color=scenario_colors["pretreatment - biogas"])
# ax5.plot(GHG_variation_elec_eff.index, GHG_variation_elec_eff["gasification"], label="gasification", marker="v", color=scenario_colors["gasification"])
# ax5.plot(GHG_variation_elec_eff.index, GHG_variation_elec_eff["pelletization and gasification"], label="pelletization and gasification", marker="v", color=scenario_colors["pelletization and gasification"])
# ax5.plot(GHG_variation_elec_eff.index, GHG_variation_elec_eff["cofiring-dry"], label="cofiring-dry", marker="v", color=scenario_colors["cofiring-dry"])
# ax5.plot(GHG_variation_elec_eff.index, GHG_variation_elec_eff["cofiring-wet"], label="cofiring-wet", marker="v", color=scenario_colors["cofiring-wet"])
# #ax5.set_ylabel("Emissions abatement (kg CO2eq)")

# ax6=fig.add_subplot(326, sharey=ax5)
# ax6.plot(npv_variation_elec_eff.index, npv_variation_elec_eff["pretreatment (NaOH) - biogas"], label="pretreatment (NaOH) - biogas", marker="*", color=scenario_colors["pretreatment (NaOH) - biogas"])
# ax6.plot(npv_variation_elec_eff.index, npv_variation_elec_eff["pretreatment - biogas"], label="pretreatment - biogas", marker="*", color=scenario_colors["pretreatment - biogas"])
# ax6.plot(npv_variation_elec_eff.index, npv_variation_elec_eff["gasification"], label="gasification", marker="*", color=scenario_colors["gasification"])
# ax6.plot(npv_variation_elec_eff.index, npv_variation_elec_eff["pelletization and gasification"], label="pelletization and gasification", marker="*", color=scenario_colors["pelletization and gasification"])
# ax6.plot(npv_variation_elec_eff.index, npv_variation_elec_eff["cofiring-dry"], label="cofiring-dry", marker="*", color=scenario_colors["cofiring-dry"])
# ax6.plot(npv_variation_elec_eff.index, npv_variation_elec_eff["cofiring-wet"], label="cofiring-wet", marker="*", color=scenario_colors["cofiring-wet"])
# ax6.set_ylabel("NPV")
# fig.supylabel("Emissions abatement (kg CO2eq)")
# #lwd(ax1, fig).for_list_axes(list_axes=[ax1, ax2])
# unique_handles, unique_labels = unique_handles_labels(ax1=ax1, ax2=ax2)
# fig.legend(unique_handles, unique_labels, loc=9, ncol=4)
# plt.show()
