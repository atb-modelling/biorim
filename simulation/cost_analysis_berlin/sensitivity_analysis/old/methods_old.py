import start
from tools import prod
from analysis.timestep_chains import start_simulation
from tools import convert_molmasses
from analysis.economic.macc import MACC, circle_plot_MAC, single_graphic_MACC, circle_plot_NPV_emissions
from simulation.data_management_and_plotting.npv_plotting import npv_graphic

import matplotlib.pyplot as plt
import pandas as pd


config_composting_method = {
    ### Main settings
    "simulation"   :{
        'name': "composting",
        'type': "all combinations", 
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture"
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.42, 0.18, 0.10, 0.07, 0.06, 0.06, 0.05, 0.03, 0.01]],
                'dist_km': [[98, 50, 188, 146, 21, 11, 44, 189, 153]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.composting.CompostingLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "Andersen_2010",
                'CH4_emis': 0.02, 
                'land_purchase' : True, 
                'land_rent' : False,
                'fixed_operation_cost' : True,
                'new_infrastructure': True
                },

        "production.plant.fertilization.CompostSpreading_1" :{
                'I': prod('leaves compost', 100, "%", source='material.composting.CompostingLeaves_1'),
                'maturity' : "very mature",
                'method' : "VDLUFA", 
                },
    }
}

result_config_composting_method=start_simulation(config_composting_method)
#npv_graphic(result_config_composting_transport)
result_config_composting_method["composting"]["t1"][0]["cashflow"].loc["transport.LandTransportMultiple_1"].loc["transport price - multiple routes", "%"]
result_config_composting_method["composting"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas().sum().sum()

# list_method=["default"]
# list_npv_comp_method=[]
# for i in range(0,len(list_method)):
#     list_npv_comp_method.append(result_config_composting_method["composting"]["t1"][i]["npv_value"])
# list_npv_comp_method

# list_GHG_comp_method=[]
# for i in range(0,len(list_method)):
#     list_GHG_comp_method.append(result_config_composting_method["composting"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
# list_GHG_comp_method


#####

config_pre_NaOH_biogas_method = {
    ### Main settings
    "simulation"   :{
        'name': "pretreatment and biogas - with NaOH",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.202, 0.227, 0.179, 0.126, 0.133, 0.132]],
                'dist_km': [[42, 33, 65, 27, 30, 32]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "production.plant.pretreatment_leaves.PretreatmentLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'method' : "NaOH",
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.pretreatment_leaves.PretreatmentLeaves_1'),
                'CHP_kW':2500,
                'land_purchase': True,
                'land_rent': False,
                'labour_costs':["FNR_2016", "Saracevic et al. 2019"],
                'maintenance_costs': ["FNR_2016", "Achinas_Euverink_2019"],
                'new_infrastructure': True,
                },
        
        "transport.LandTransportMultiple_2" :{
                'I': prod('biogas digestate', 100, "%", source='material.biogas.BiogasPlant_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}        

result_config_pre_NaOH_biogas_method=start_simulation(config_pre_NaOH_biogas_method)
# npv_graphic(result_config_pre_NaOH_biogas_method, 2)
# result_config_pre_NaOH_biogas_method["pretreatment and biogas - with NaOH"]["t1"][0]["cashflow"]
# result_config_pre_NaOH_biogas_method["pretreatment and biogas - with NaOH"]["t1"][1]["cashflow"]
# result_config_pre_NaOH_biogas_method["pretreatment and biogas - with NaOH"]["t1"][2]["cashflow"]
# result_config_pre_NaOH_biogas_method["pretreatment and biogas - with NaOH"]["t1"][3]["cashflow"]


# list_method_biogas_NaOH=["Bio-NaOH-1", "Bio-NaOH-2", "Bio-NaOH-3", "Bio-NaOH-4"]
# list_npv_biogas_NaOH_method=[]
# for i in range(0,len(list_method_biogas_NaOH)):
#     list_npv_biogas_NaOH_method.append(result_config_pre_NaOH_biogas_method["pretreatment and biogas - with NaOH"]["t1"][i]["npv_value"])
# list_npv_biogas_NaOH_method

# list_GHG_biogas_NaOH_method=[]
# for i in range(0,len(list_method_biogas_NaOH)):
#     list_GHG_biogas_NaOH_method.append(result_config_pre_NaOH_biogas_method["pretreatment and biogas - with NaOH"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
# list_GHG_biogas_NaOH_method

#######

config_pre_biogas_method = {
    ### Main settings
    "simulation"   :{
        'name': "pretreatment and biogas - no NaOH",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.202, 0.227, 0.179, 0.126, 0.133, 0.132]],
                'dist_km': [[42, 33, 65, 27, 30, 32]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'CHP_kW':2700,
                'land_purchase': True,
                'land_rent': False,
                'labour_costs':["FNR_2016", "Saracevic et al. 2019"],
                'maintenance_costs': ["FNR_2016", "Achinas_Euverink_2019"],
                'new_infrastructure': True, 
                },
        
        "transport.LandTransportMultiple_2" :{
                'I': prod('biogas digestate', 100, "%", source='material.biogas.BiogasPlant_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}

result_config_pre_biogas_method=start_simulation(config_pre_biogas_method)
# npv_graphic(result_config_pre_biogas_method, 2)
# result_config_pre_biogas_method["pretreatment and biogas - no NaOH"]["t1"][0]["cashflow"]
# result_config_pre_biogas_method["pretreatment and biogas - no NaOH"]["gwp"].loc["t1",2,:,:,"GWP100"].to_pandas()
# sens_numb=[key for key in result_config_pre_biogas_method["pretreatment and biogas - no NaOH"]["t1"].keys() if type(key) is int]
# len(sens_numb)


# list_biogas_method_example=[result_config_composting_method, result_config_pre_NaOH_biogas_method, result_config_pre_biogas_method]
# list_biogas_method_example

# MACC(list_biogas_method_example, BAU_scenario="composting", sensitivity=True).marginal_abatement_cost()
# MACC(list_biogas_method_example, BAU_scenario="composting", sensitivity=False).marginal_abatement_cost()



# list_method_biogas= ["Bio-1", "Bio-2", "Bio-3", "Bio-4"]
# list_npv_biogas_method=[]
# for i in range(0,len(list_method_biogas)):
#     list_npv_biogas_method.append(result_config_pre_biogas_method["pretreatment and biogas - no NaOH"]["t1"][i]["npv_value"])
# list_npv_biogas_method

# list_GHG_biogas_method=[]
# for i in range(0,len(list_method_biogas)):
#     list_GHG_biogas_method.append(result_config_pre_biogas_method["pretreatment and biogas - no NaOH"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
# list_GHG_biogas_method


#gasification
config_gasification_method = {
    ### Main settings
    "simulation"   :{
        'name': "gasification",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },


        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'kW':3600,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': False,
                'land_rent': False,
                'new_infrastructure': False,
                'syngas_prod': ["volatile matter", "fixed rate"]
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.gasification.Gasification_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.gasification.Gasification_1')
                },

    },
}

result_config_gasification_method=start_simulation(config_gasification_method)
# list_method_gasification=["Gas-1", "Gas-2"]
# list_npv_gasification_method=[]
# for i in range(0,len(list_method_gasification)):
#     list_npv_gasification_method.append(result_config_gasification_method["gasification"]["t1"][i]["npv_value"])
# list_npv_gasification_method

# list_GHG_gasification_method=[]
# for i in range(0,len(list_method_gasification)):
#     list_GHG_gasification_method.append(result_config_gasification_method["gasification"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
# list_GHG_gasification_method


#pelletization and gasification

config_pellet_gasification_method = {
    ### Main settings
    "simulation"   :{
        'name': "pelletization and gasification",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.pelletization.PelletFactory_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'land_purchase': True, 
                'land_rent': False,
                'new_infrastructure': True,
                'emissions_calculation': ["Ecoinvent", "Chen_2009"]
                },

        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves pellets', 100, "%", source='material.pelletization.PelletFactory_1'),
                'kW': 4000,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': True,
                'land_rent': False,
                'new_infrastructure': False,
                'syngas_prod': ["volatile matter", "fixed rate"]
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.gasification.Gasification_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.gasification.Gasification_1')
                },

    }
}
result_config_pellet_gasification_method=start_simulation(config_pellet_gasification_method)
# result_config_pellet_gasification_method["pelletization and gasification"]["t1"][1]["cashflow"]


# list_method_pellet_gasification=["Gas-p-1", "Gas-p-2", "Gas-p-3", "Gas-p-4"]
# list_npv_pellet_gasification_method=[]
# for i in range(0,len(list_method_pellet_gasification)):
#     list_npv_pellet_gasification_method.append(result_config_pellet_gasification_method["pelletization and gasification"]["t1"][i]["npv_value"])
# list_npv_pellet_gasification_method

# list_GHG_pellet_gasification_method=[]
# for i in range(0,len(list_method_pellet_gasification)):
#     list_GHG_pellet_gasification_method.append(result_config_pellet_gasification_method["pelletization and gasification"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
# list_GHG_pellet_gasification_method


#cofiring - dry
config_cofiring_dry_method = {
    ### Main settings
    "simulation"   :{
        'name': "cofiring - dry",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.cofiring.BiomassDrying_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'new_infrastructure': True, 
                'method': ["Haque", "vaporization heat"]
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves dry', 100, "%", source='material.cofiring.BiomassDrying_1'),
                'kW':12000,
                'operation_hours':8000, 
                'retrofitting': True, 
                'land_purchase': False,
                'land_rent': False,
                'new_infrastructure': False,
                'emissions_combustion': ["volatile_matter", "GHG_protocol", "Chen_et_al_2020"]
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.cofiring.CoFiring_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },    

        "external.Electricity_1" :{
                'I': prod('electrical energy - biomass share', 100, "%", source='material.cofiring.CoFiring_1')
                },
                
    }
}

result_config_cofiring_dry_method=start_simulation(config_cofiring_dry_method)
# list_method_cofiring_dry=["Cof-dry-1", "Cof-dry-2", "Co-dry-3", "Co-dry-4", "Co-dry-5", "Co-dry-6"]
# list_npv_cofiring_dry_method=[]
# for i in range(0,len(list_method_cofiring_dry)):
#     list_npv_cofiring_dry_method.append(result_config_cofiring_dry_method["cofiring - dry"]["t1"][i]["npv_value"])
# list_npv_cofiring_dry_method

# list_GHG_cofiring_dry_method=[]
# for i in range(0,len(list_method_cofiring_dry)):
#     list_GHG_cofiring_dry_method.append(result_config_cofiring_dry_method["cofiring - dry"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
# list_GHG_cofiring_dry_method

#wet biomass
config_cofiring_wet_method = {
    ### Main settings
    "simulation"   :{
        'name': "cofiring - wet",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'kW':10000,
                'operation_hours':8000, 
                'retrofitting': True, 
                'land_purchase': False,
                'land_rent': False,
                'new_infrastructure': False,
                'emissions_combustion': ["volatile_matter", "GHG_protocol", "Chen_et_al_2020"]
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.cofiring.CoFiring_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },    

        "external.Electricity_1" :{
                'I': prod('electrical energy - biomass share', 100, "%", source='material.cofiring.CoFiring_1')
                },
    }
}

result_config_cofiring_wet_method=start_simulation(config_cofiring_wet_method)
# list_method_cofiring_wet=["Cof-wet-1", "Cof-wet-2", "Cof-wet-3"]
# list_npv_cofiring_wet_method=[]
# for i in range(0,len(list_method_cofiring_wet)):
#     list_npv_cofiring_wet_method.append(result_config_cofiring_wet_method["cofiring - wet"]["t1"][i]["npv_value"])
# list_npv_cofiring_wet_method

# list_GHG_cofiring_wet_method=[]
# for i in range(0,len(list_method_cofiring_wet)):
#     list_GHG_cofiring_wet_method.append(result_config_cofiring_wet_method["cofiring - wet"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
# list_GHG_cofiring_wet_method

list_methods=[result_config_composting_method, result_config_pre_NaOH_biogas_method, result_config_pre_biogas_method, result_config_gasification_method, 
              result_config_pellet_gasification_method, result_config_cofiring_dry_method, result_config_cofiring_wet_method]

MAC_methods_df=MACC(list_methods, BAU_scenario="composting", sensitivity=True).marginal_abatement_cost()
MAC_methods_df

circle_plot_NPV_emissions(MAC_methods_df, fixed_df=True, legend_ncol=3, sensitivity=True, size_factor=1000)
circle_plot_NPV_emissions(MAC_methods_df, fixed_df=True, legend_ncol=3, size_factor=1000)



sce="pelletization and gasification - 1"
sce[:-4]

sce2="pretreatment and biogas - with NaOH - 1"
sce2[:-4]





























#####Dataframes of variation
list_GHG_comp_method
list_GHG_biogas_NaOH_method
list_GHG_biogas_method
list_GHG_gasification_method
list_GHG_pellet_gasification_method
list_GHG_cofiring_dry_method
list_GHG_cofiring_wet_method

GHG_variation_method=pd.DataFrame(index=[], columns=[])
GHG_variation_method.loc["default", "composting"]=list_GHG_comp_method[0]
GHG_variation_method


for i in range(0,len(list_method_biogas_NaOH)):
    GHG_variation_method.loc[list_method_biogas_NaOH[i], "pretreatment (NaOH) - biogas"]=GHG_variation_method.loc["default", "composting"]-list_GHG_biogas_NaOH_method[i]
for i in range(0,len(list_method_biogas)):   
    GHG_variation_method.loc[list_method_biogas[i], "pretreatment - biogas"]=GHG_variation_method.loc["default", "composting"]-list_GHG_biogas_method[i]

for i in range(0,len(list_method_gasification)):
    GHG_variation_method.loc[list_method_gasification[i], "gasification"]=GHG_variation_method.loc["default", "composting"]-list_GHG_gasification_method[i]
for i in range(0,len(list_method_pellet_gasification)):    
    GHG_variation_method.loc[list_method_pellet_gasification[i], "pelletization and gasification"]=GHG_variation_method.loc["default", "composting"]-list_GHG_pellet_gasification_method[i]

for i in range(0,len(list_method_cofiring_dry)):
    GHG_variation_method.loc[list_method_cofiring_dry[i], "cofiring-dry"]=GHG_variation_method.loc["default", "composting"]-list_GHG_cofiring_dry_method[i]
for i in range(0,len(list_method_cofiring_wet)):
    GHG_variation_method.loc[list_method_cofiring_wet[i], "cofiring-wet"]=GHG_variation_method.loc["default", "composting"]-list_GHG_cofiring_wet_method[i]

#GHG_variation_method.sort_index(inplace=True)
GHG_variation_method

npv_variation_method=pd.DataFrame(index=[], columns=[])

for i in range(0,len(list_method_biogas_NaOH)):
    npv_variation_method.loc[list_method_biogas_NaOH[i], "pretreatment (NaOH) - biogas"]=list_npv_biogas_NaOH_method[i]
for i in range(0,len(list_method_biogas)):   
    npv_variation_method.loc[list_method_biogas[i], "pretreatment - biogas"]=list_npv_biogas_method[i]

for i in range(0,len(list_method_gasification)):
    npv_variation_method.loc[list_method_gasification[i], "gasification"]=list_npv_gasification_method[i]
for i in range(0,len(list_method_pellet_gasification)):    
    npv_variation_method.loc[list_method_pellet_gasification[i], "pelletization and gasification"]=list_npv_pellet_gasification_method[i]

for i in range(0,len(list_method_cofiring_dry)):
    npv_variation_method.loc[list_method_cofiring_dry[i], "cofiring-dry"]=list_npv_cofiring_dry_method[i]
for i in range(0,len(list_method_cofiring_wet)):
    npv_variation_method.loc[list_method_cofiring_wet[i], "cofiring-wet"]=list_npv_cofiring_wet_method[i]

npv_variation_method


# GHG_variation_method["pretreatment (NaOH) - biogas"]
# GHG_variation_method.index

list_method_biogas_NaOH
GHG_variation_method["pretreatment (NaOH) - biogas"]
GHG_variation_method.loc[list_method_biogas_NaOH]

#TODO: establish art

scenario_colors={   "compost": "#D49C36", #1
                    "biogas": "#60CC9E",#1 
                    "gasification": "#28864A",#2
                    "cofiring": "#1A639E",#3
                    "pretreatment - biogas": "#1F2988",#4
                    "pretreatment (NaOH) - biogas": "#bf00ff",
                    "biogas and composting":"#AB4242", #5
                    "pelletization and gasification":"#D49C36",#6
                    "cofiring-dry":"#CFD25F",#7 
                    "cofiring-wet":"#B3B296", #8
                    "pelletization and cofiring":"#676EA6"}


list_scenarios=["pretreatment (NaOH) - biogas", "pretreatment - biogas", "gasification", "pelletization and gasification", "cofiring-dry", "cofiring-wet"]
list_methods=[list_method_biogas_NaOH, list_method_biogas, list_method_gasification, list_method_pellet_gasification, list_method_cofiring_dry, list_method_cofiring_wet]



#NOTE: automated method
import math

fig=plt.figure(figsize=(8,12))
for i in range(1,len(list_scenarios)*2+1):
    name_scenario=list_scenarios[math.ceil(i/2)-1]
    result_method=list_methods[math.ceil(i/2)-1]
    if i%2!=0:
        ax1=fig.add_subplot(6,2,i)
        ax1.scatter(result_method, GHG_variation_method.loc[result_method][name_scenario], label=name_scenario, color=scenario_colors[name_scenario])
    elif i%2==0:
        ax2=fig.add_subplot(6,2,i)
        ax2.scatter(result_method, npv_variation_method.loc[result_method][name_scenario], label=name_scenario, marker="*", color=scenario_colors[name_scenario])
    else:
        pass

ax1=fig.add_subplot(1,2,1, frameon=False)
ax1.set_yticks([]) 
ax1.set_xticks([])
ax1.set_title("Emissions abatement", pad=20)

ax1=fig.add_subplot(1,2,2, frameon=False)
ax1.set_yticks([]) 
ax1.set_xticks([])
ax1.set_title("NPV", pad=20)

#labels for scenarios
ax1=fig.add_subplot(3,1,1, frameon=False)
ax1.set_yticks([]) 
ax1.set_xticks([])
ax1.set_ylabel("Biogas\nscenarios", labelpad=30)

ax1=fig.add_subplot(3,1,2, frameon=False)
ax1.set_yticks([]) 
ax1.set_xticks([])
ax1.set_ylabel("Gasification\nscenarios", labelpad=30)

ax1=fig.add_subplot(3,1,3, frameon=False)
ax1.set_yticks([]) 
ax1.set_xticks([])
ax1.set_ylabel("Co-firing\nscenarios", labelpad=30)

plt.subplots_adjust(top=0.925,
                    bottom=0.11,
                    left=0.125,
                    right=0.9,
                    hspace=0.5,
                    wspace=0.5)
plt.show()






#NOTE: Old version of plotting
fig=plt.figure(figsize=(8,12))

name_scenario="pretreatment (NaOH) - biogas"
ax1=fig.add_subplot(621)
ax1.scatter(list_method_biogas_NaOH, GHG_variation_method.loc[list_method_biogas_NaOH][name_scenario], label=name_scenario, color=scenario_colors[name_scenario])
ax2=fig.add_subplot(622)
ax2.scatter(list_method_biogas_NaOH, npv_variation_method.loc[list_method_biogas_NaOH][name_scenario], label=name_scenario, marker="*", color=scenario_colors[name_scenario])

name_scenario="pretreatment - biogas"
ax3=fig.add_subplot(623, sharey=ax1)
ax3.scatter(list_method_biogas, GHG_variation_method.loc[list_method_biogas][name_scenario], label=name_scenario, color=scenario_colors[name_scenario])
ax4=fig.add_subplot(624, sharey=ax2)
ax4.scatter(list_method_biogas, npv_variation_method.loc[list_method_biogas][name_scenario], label=name_scenario, marker="*", color=scenario_colors[name_scenario])

name_scenario="gasification"
ax5=fig.add_subplot(625)
ax5.scatter(list_method_gasification, GHG_variation_method.loc[list_method_gasification][name_scenario], label=name_scenario, color=scenario_colors[name_scenario])
ax6=fig.add_subplot(626)
ax6.scatter(list_method_gasification, npv_variation_method.loc[list_method_gasification][name_scenario], label=name_scenario, marker="*", color=scenario_colors[name_scenario])

name_scenario="pelletization and gasification"
ax7=fig.add_subplot(627, sharey=ax5)
ax7.scatter(list_method_pellet_gasification, GHG_variation_method.loc[list_method_pellet_gasification][name_scenario], label=name_scenario, color=scenario_colors[name_scenario])
ax8=fig.add_subplot(628, sharey=ax6)
ax8.scatter(list_method_pellet_gasification, npv_variation_method.loc[list_method_pellet_gasification][name_scenario], label=name_scenario, marker="*", color=scenario_colors[name_scenario])

name_scenario="cofiring-dry"
ax9=fig.add_subplot(6,2,9)
ax9.scatter(list_method_cofiring_dry, GHG_variation_method.loc[list_method_cofiring_dry][name_scenario], label=name_scenario, color=scenario_colors[name_scenario])
ax10=fig.add_subplot(6,2,10)
ax10.scatter(list_method_cofiring_dry, npv_variation_method.loc[list_method_cofiring_dry][name_scenario], label=name_scenario, marker="*", color=scenario_colors[name_scenario])

name_scenario="cofiring-wet"
ax11=fig.add_subplot(6,2,11, sharey=ax9)
ax11.scatter(list_method_cofiring_wet, GHG_variation_method.loc[list_method_cofiring_wet][name_scenario], label=name_scenario,  color=scenario_colors[name_scenario])
ax12=fig.add_subplot(6,2,12, sharey=ax10)
ax12.scatter(list_method_cofiring_wet, npv_variation_method.loc[list_method_cofiring_wet][name_scenario], label=name_scenario, marker="*",  color=scenario_colors[name_scenario])

#
ax1=fig.add_subplot(1,2,1, frameon=False)
ax1.set_yticks([]) 
ax1.set_xticks([])
ax1.set_title("Emissions abatement", pad=20)

ax1=fig.add_subplot(1,2,2, frameon=False)
ax1.set_yticks([]) 
ax1.set_xticks([])
ax1.set_title("NPV", pad=20)

#labels for scenarios
ax1=fig.add_subplot(3,1,1, frameon=False)
ax1.set_yticks([]) 
ax1.set_xticks([])
ax1.set_ylabel("Biogas\nscenarios", labelpad=30)

ax1=fig.add_subplot(3,1,2, frameon=False)
ax1.set_yticks([]) 
ax1.set_xticks([])
ax1.set_ylabel("Gasification\nscenarios", labelpad=30)

ax1=fig.add_subplot(3,1,3, frameon=False)
ax1.set_yticks([]) 
ax1.set_xticks([])
ax1.set_ylabel("Co-firing\nscenarios", labelpad=30)


plt.subplots_adjust(top=0.925,
                    bottom=0.11,
                    left=0.125,
                    right=0.9,
                    hspace=0.5,
                    wspace=0.5)
plt.show()



