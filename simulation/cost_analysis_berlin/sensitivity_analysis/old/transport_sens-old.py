import start
from tools import prod
from analysis.timestep_chains import start_simulation
from tools import convert_molmasses
from analysis.economic.macc import MACC, circle_plot_MAC, single_graphic_MACC
from simulation.data_management_and_plotting.npv_plotting import npv_graphic

import matplotlib.pyplot as plt
import pandas as pd

#NOTE: IMPORTANT! The execution of these simulations has a high time demand (at least 20 min!)

config_composting_transport = {
    ### Main settings
    "simulation"   :{
        'name': "composting",
        'type': "all combinations", 
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture"
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.composting.CompostingLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "Andersen_2010",
                'CH4_emis': 0.02, 
                'land_purchase' : True, 
                'land_rent' : False,
                'fixed_operation_cost' : True,
                'new_infrastructure': True
                },

        "production.plant.fertilization.CompostSpreading_1" :{
                'I': prod('leaves compost', 100, "%", source='material.composting.CompostingLeaves_1'),
                'maturity' : "very mature",
                'method' : "VDLUFA", 
                },
    }
}

result_config_composting_transport=start_simulation(config_composting_transport)
#npv_graphic(result_config_composting_transport)
result_config_composting_transport["composting"]["t1"][0]["cashflow"].loc["transport.LandTransportMultiple_1"].loc["transport price - multiple routes", "%"]
result_config_composting_transport["composting"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas().sum().sum()

list_distances=[25, 50, 75, 100, 125, 150, 175, 200]
list_npv_comp_transport=[]
for i in range(0,len(list_distances)):
    list_npv_comp_transport.append(result_config_composting_transport["composting"]["t1"][i]["npv_value"])
list_npv_comp_transport

list_GHG_comp_transport=[]
for i in range(0,len(list_distances)):
    list_GHG_comp_transport.append(result_config_composting_transport["composting"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
list_GHG_comp_transport

# list_percetage_OPEX=[]
# for i in range(0,len(list_distances)):
#     list_percetage_OPEX.append(result_config_composting_transport["composting"]["t1"][i]["cashflow"].loc["transport.LandTransportMultiple_1"].loc["transport price - multiple routes", "%"])
# list_percetage_OPEX


# #GHG emissions and NPV
# fig, ax1= plt.subplots()
# ax1.plot(list_distances, list_GHG, label="GHG emissions", color="red")
# ax2=ax1.twinx()
# ax2.plot(list_distances, list_npv, label="NPV", color="blue")
# ax1.set_xlabel("Distance (km)", fontsize=16)
# ax1.set_ylabel("kg CO2eq", fontsize=16)
# ax2.set_ylabel("EUR", fontsize=16)
# ax1.tick_params("both", labelsize=16)
# ax2.tick_params("both", labelsize=16)
# fig.legend(ncol=2, loc=9)
# plt.show()

# #GHG emissions and %OPEX
# fig, ax1= plt.subplots()
# ax1.plot(list_distances, list_GHG, label="GHG emissions", color="red")
# ax2=ax1.twinx()
# ax2.plot(list_distances, list_percetage_OPEX, label="OPEX-share", color="blue")
# ax1.set_xlabel("Distance (km)", fontsize=16)
# ax1.set_ylabel("kg CO2eq", fontsize=16)
# ax2.set_ylabel("%", fontsize=16)
# ax1.tick_params("both", labelsize=16)
# ax2.tick_params("both", labelsize=16)
# fig.legend(ncol=2, loc=9)
# plt.show()

# #NPV and %OPEX
# fig, ax1= plt.subplots()
# ax1.plot(list_distances, list_npv, label="NPV", color="red")
# ax2=ax1.twinx()
# ax2.plot(list_distances, list_percetage_OPEX, label="OPEX-share", color="blue")
# ax1.set_xlabel("Distance (km)", fontsize=16)
# ax1.set_ylabel("EUR", fontsize=16)
# ax2.set_ylabel("%", fontsize=16)
# ax1.tick_params("both", labelsize=16)
# ax2.tick_params("both", labelsize=16)
# fig.legend(ncol=2, loc=9)
# plt.show()


config_pre_NaOH_biogas_transport = {
    ### Main settings
    "simulation"   :{
        'name': "pretreatment and biogas - with NaOH",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1],[1],[1],[1],[1],[1],[1],[1]],
                'dist_km': [[25], [50], [75], [100], [125], [150], [175], [200]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "production.plant.pretreatment_leaves.PretreatmentLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'method' : "NaOH",
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.pretreatment_leaves.PretreatmentLeaves_1'),
                'CHP_kW':2500,
                'land_purchase': True,
                'land_rent': False,
                'labour_costs':"FNR_2016", 
                'maintenance_costs':"FNR_2016",
                'new_infrastructure': True
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('biogas digestate', 100, "%", source='material.biogas.BiogasPlant_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}        

result_config_pre_NaOH_biogas_transport=start_simulation(config_pre_NaOH_biogas_transport)
npv_graphic(result_config_pre_NaOH_biogas_transport, 3)

list_distances=[25, 50, 75, 100, 125, 150, 175, 200]
list_npv_biogas_NaOH_transport=[]
for i in range(0,len(list_distances)):
    list_npv_biogas_NaOH_transport.append(result_config_pre_NaOH_biogas_transport["pretreatment and biogas - with NaOH"]["t1"][i]["npv_value"])
list_npv_biogas_NaOH_transport

list_GHG_biogas_NaOH_transport=[]
for i in range(0,len(list_distances)):
    list_GHG_biogas_NaOH_transport.append(result_config_pre_NaOH_biogas_transport["pretreatment and biogas - with NaOH"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
list_GHG_biogas_NaOH_transport

# list_percetage_OPEX=[]
# for i in range(0,len(list_distances)):
#     list_percetage_OPEX.append(result_config_pre_NaOH_biogas_transport["pretreatment and biogas - with NaOH"]["t1"][i]["cashflow"].loc["transport.LandTransportMultiple_1"].loc["transport price - multiple routes", "%"])
# list_percetage_OPEX


# #GHG emissions and NPV
# fig, ax1= plt.subplots()
# ax1.plot(list_distances, list_GHG, label="GHG emissions", color="red")
# ax2=ax1.twinx()
# ax2.plot(list_distances, list_npv, label="NPV", color="blue")
# ax1.set_xlabel("Distance (km)", fontsize=16)
# ax1.set_ylabel("kg CO2eq", fontsize=16)
# ax2.set_ylabel("EUR", fontsize=16)
# ax1.tick_params("both", labelsize=16)
# ax2.tick_params("both", labelsize=16)
# fig.legend(ncol=2, loc=9)
# plt.show()

# #GHG emissions and %OPEX
# fig, ax1= plt.subplots()
# ax1.plot(list_distances, list_GHG, label="GHG emissions", color="red")
# ax2=ax1.twinx()
# ax2.plot(list_distances, list_percetage_OPEX, label="OPEX-share", color="blue")
# ax1.set_xlabel("Distance (km)", fontsize=16)
# ax1.set_ylabel("kg CO2eq", fontsize=16)
# ax2.set_ylabel("%", fontsize=16)
# ax1.tick_params("both", labelsize=16)
# ax2.tick_params("both", labelsize=16)
# fig.legend(ncol=2, loc=9)
# plt.show()

# #NPV and %OPEX
# fig, ax1= plt.subplots()
# ax1.plot(list_distances, list_npv, label="NPV", color="red")
# ax2=ax1.twinx()
# ax2.plot(list_distances, list_percetage_OPEX, label="OPEX-share", color="blue")
# ax1.set_xlabel("Distance (km)", fontsize=16)
# ax1.set_ylabel("EUR", fontsize=16)
# ax2.set_ylabel("%", fontsize=16)
# ax1.tick_params("both", labelsize=16)
# ax2.tick_params("both", labelsize=16)
# fig.legend(ncol=2, loc=9)
# plt.show()


config_pre_biogas_transport = {
    ### Main settings
    "simulation"   :{
        'name': "pretreatment and biogas - no NaOH",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1],[1],[1],[1],[1],[1],[1],[1]],
                'dist_km': [[25], [50], [75], [100], [125], [150], [175], [200]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'CHP_kW':2700,
                'land_purchase': True,
                'land_rent': False,
                'labour_costs':"FNR_2016", 
                'maintenance_costs':"FNR_2016",
                'new_infrastructure': True
                },
        
        "transport.LandTransportMultiple_2" :{
                'I': prod('biogas digestate', 100, "%", source='material.biogas.BiogasPlant_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}

result_config_pre_biogas_transport=start_simulation(config_pre_biogas_transport)
list_distances=[25, 50, 75, 100, 125, 150, 175, 200]
list_npv_biogas_transport=[]
for i in range(0,len(list_distances)):
    list_npv_biogas_transport.append(result_config_pre_biogas_transport["pretreatment and biogas - no NaOH"]["t1"][i]["npv_value"])
list_npv_biogas_transport

list_GHG_biogas_transport=[]
for i in range(0,len(list_distances)):
    list_GHG_biogas_transport.append(result_config_pre_biogas_transport["pretreatment and biogas - no NaOH"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
list_GHG_biogas_transport


#gasification
config_gasification_transport = {
    ### Main settings
    "simulation"   :{
        'name': "gasification",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1],[1],[1],[1],[1],[1],[1],[1]],
                'dist_km': [[25], [50], [75], [100], [125], [150], [175], [200]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'kW':3600,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': False,
                'land_rent': False,
                'new_infrastructure': False
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.gasification.Gasification_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.gasification.Gasification_1')
                },
    },
}

result_config_gasification_transport=start_simulation(config_gasification_transport)
list_distances=[25, 50, 75, 100, 125, 150, 175, 200]
list_npv_gasification_transport=[]
for i in range(0,len(list_distances)):
    list_npv_gasification_transport.append(result_config_gasification_transport["gasification"]["t1"][i]["npv_value"])
list_npv_gasification_transport

list_GHG_gasification_transport=[]
for i in range(0,len(list_distances)):
    list_GHG_gasification_transport.append(result_config_gasification_transport["gasification"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
list_GHG_gasification_transport


#pelletization and gasification

config_pellet_gasification_transport = {
    ### Main settings
    "simulation"   :{
        'name': "pelletization and gasification",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1],[1],[1],[1],[1],[1],[1],[1]],
                'dist_km': [[25], [50], [75], [100], [125], [150], [175], [200]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.pelletization.PelletFactory_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'land_purchase': True, 
                'land_rent': False,
                'new_infrastructure': True
                },

        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves pellets', 100, "%", source='material.pelletization.PelletFactory_1'),
                'kW': 4000,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': True,
                'land_rent': False,
                'new_infrastructure': False
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.gasification.Gasification_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.gasification.Gasification_1')
                },
          
    }
}
result_config_pellet_gasification_transport=start_simulation(config_pellet_gasification_transport)

list_distances=[25, 50, 75, 100, 125, 150, 175, 200]
list_npv_pellet_gasification_transport=[]
for i in range(0,len(list_distances)):
    list_npv_pellet_gasification_transport.append(result_config_pellet_gasification_transport["pelletization and gasification"]["t1"][i]["npv_value"])
list_npv_gasification_transport

list_GHG_pellet_gasification_transport=[]
for i in range(0,len(list_distances)):
    list_GHG_pellet_gasification_transport.append(result_config_pellet_gasification_transport["pelletization and gasification"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
list_GHG_pellet_gasification_transport

#cofiring - dry
config_cofiring_dry_transport = {
    ### Main settings
    "simulation"   :{
        'name': "cofiring - dry",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1],[1],[1],[1],[1],[1],[1],[1]],
                'dist_km': [[25], [50], [75], [100], [125], [150], [175], [200]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.cofiring.BiomassDrying_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'new_infrastructure': True
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves dry', 100, "%", source='material.cofiring.BiomassDrying_1'),
                'kW':12000,
                'operation_hours':8000, 
                'electrical_efficiency': 0.14,
                'retrofitting': True, 
                'land_purchase': False,
                'land_rent': False,
                'new_infrastructure': False,
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.cofiring.CoFiring_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },    

        "external.Electricity_1" :{
                'I': prod('electrical energy - biomass share', 100, "%", source='material.cofiring.CoFiring_1')
                },
    }
}

result_config_cofiring_dry_transport=start_simulation(config_cofiring_dry_transport)
list_distances=[25, 50, 75, 100, 125, 150, 175, 200]
list_npv_cofiring_dry_transport=[]
for i in range(0,len(list_distances)):
    list_npv_cofiring_dry_transport.append(result_config_cofiring_dry_transport["cofiring - dry"]["t1"][i]["npv_value"])
list_npv_cofiring_dry_transport

list_GHG_cofiring_dry_transport=[]
for i in range(0,len(list_distances)):
    list_GHG_cofiring_dry_transport.append(result_config_cofiring_dry_transport["cofiring - dry"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
list_GHG_cofiring_dry_transport

#wet biomass
config_cofiring_wet_transport = {
    ### Main settings
    "simulation"   :{
        'name': "cofiring - wet",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1],[1],[1],[1],[1],[1],[1],[1]],
                'dist_km': [[25], [50], [75], [100], [125], [150], [175], [200]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'kW':10000,
                'operation_hours':8000, 
                'electrical_efficiency': 0.14,
                'retrofitting': True, 
                'land_purchase': False,
                'land_rent': False,
                'new_infrastructure': False,
                'dry_biomass': False,
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.cofiring.CoFiring_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },    

        "external.Electricity_1" :{
                'I': prod('electrical energy - biomass share', 100, "%", source='material.cofiring.CoFiring_1')
                },
    }
}

result_config_cofiring_wet_transport=start_simulation(config_cofiring_wet_transport)
list_distances=[25, 50, 75, 100, 125, 150, 175, 200]
list_npv_cofiring_wet_transport=[]
for i in range(0,len(list_distances)):
    list_npv_cofiring_wet_transport.append(result_config_cofiring_wet_transport["cofiring - wet"]["t1"][i]["npv_value"])
list_npv_cofiring_wet_transport

list_GHG_cofiring_wet_transport=[]
for i in range(0,len(list_distances)):
    list_GHG_cofiring_wet_transport.append(result_config_cofiring_wet_transport["cofiring - wet"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
list_GHG_cofiring_wet_transport



#####Dataframes of variation
list_GHG_comp_transport
list_GHG_biogas_NaOH_transport
list_GHG_biogas_transport
list_GHG_gasification_transport
list_GHG_pellet_gasification_transport
list_GHG_cofiring_dry_transport

GHG_variation=pd.DataFrame(index=list_distances, columns=[])
GHG_variation.loc[100, "composting"]=list_GHG_comp_transport[0]
GHG_variation["pretreatment (NaOH) - biogas"]=[GHG_variation.loc[100, "composting"]-i for i in list_GHG_biogas_NaOH_transport]
GHG_variation["pretreatment - biogas"]=[GHG_variation.loc[100, "composting"]-i for i in list_GHG_biogas_transport]
GHG_variation["gasification"]=[GHG_variation.loc[100, "composting"]-i for i in list_GHG_gasification_transport]
GHG_variation["pelletization and gasification"]=[GHG_variation.loc[100, "composting"]-i for i in list_GHG_pellet_gasification_transport]
GHG_variation["cofiring-dry"]=[GHG_variation.loc[100, "composting"]-i for i in list_GHG_cofiring_dry_transport]
GHG_variation["cofiring-wet"]=[GHG_variation.loc[100, "composting"]-i for i in list_GHG_cofiring_wet_transport]

GHG_variation

npv_variation=pd.DataFrame(index=list_distances, columns=[])
npv_variation.loc[100, "composting"]=list_npv_comp_transport[0]
npv_variation["pretreatment (NaOH) - biogas"]=list_npv_biogas_NaOH_transport
npv_variation["pretreatment - biogas"]=list_npv_biogas_transport
npv_variation["gasification"]=list_npv_gasification_transport
npv_variation["pelletization and gasification"]=list_npv_pellet_gasification_transport
npv_variation["cofiring-dry"]=list_npv_cofiring_dry_transport
npv_variation["cofiring-wet"]=list_npv_cofiring_wet_transport

npv_variation

fig=plt.figure()
ax=fig.add_subplot(121)
ax.plot(GHG_variation.index, GHG_variation["pretreatment (NaOH) - biogas"], label="pretreatment (NaOH) - biogas", marker="v")
ax.plot(GHG_variation.index, GHG_variation["pretreatment - biogas"], label="pretreatment - biogas", marker="v")
ax.plot(GHG_variation.index, GHG_variation["gasification"], label="gasification", marker="v")
ax.plot(GHG_variation.index, GHG_variation["pelletization and gasification"], label="pelletization and gasification", marker="v")
ax.plot(GHG_variation.index, GHG_variation["cofiring-dry"], label="cofiring-dry", marker="v")
ax.plot(GHG_variation.index, GHG_variation["cofiring-wet"], label="cofiring-wet", marker="v")
ax.set_ylabel("Emissions abatement (kg CO2eq)")
ax1=fig.add_subplot(122, sharey=ax )
ax1.plot(npv_variation.index, npv_variation["pretreatment (NaOH) - biogas"], label="pretreatment (NaOH) - biogas", marker="*")
ax1.plot(npv_variation.index, npv_variation["pretreatment - biogas"], label="pretreatment - biogas", marker="*")
ax1.plot(npv_variation.index, npv_variation["gasification"], label="gasification", marker="*")
ax1.plot(npv_variation.index, npv_variation["pelletization and gasification"], label="pelletization and gasification", marker="*")
ax1.plot(npv_variation.index, npv_variation["cofiring-dry"], label="cofiring-dry", marker="*")
ax1.plot(npv_variation.index, npv_variation["cofiring-wet"], label="cofiring-wet", marker="*")
ax1.set_ylabel("NPV")
fig.supxlabel("Distance (km)")

fig.legend(loc=9, ncol=4)
plt.show()