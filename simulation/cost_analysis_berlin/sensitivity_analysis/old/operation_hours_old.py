import start
from tools import prod
from analysis.timestep_chains import start_simulation
from tools import convert_molmasses
from analysis.economic.macc import MACC, circle_plot_MAC, single_graphic_MACC
from simulation.data_management_and_plotting.npv_plotting import npv_graphic

import matplotlib.pyplot as plt
import pandas as pd


config_composting_operhours = {
    ### Main settings
    "simulation"   :{
        'name': "composting",
        'type': "all combinations", 
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture"
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.42, 0.18, 0.10, 0.07, 0.06, 0.06, 0.05, 0.03, 0.01]],
                'dist_km': [[98, 50, 188, 146, 21, 11, 44, 189, 153]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.composting.CompostingLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "Andersen_2010",
                'CH4_emis': 0.02, 
                'land_purchase' : True, 
                'land_rent' : False,
                'fixed_operation_cost' : True,
                'new_infrastructure': True
                },

        "production.plant.fertilization.CompostSpreading_1" :{
                'I': prod('leaves compost', 100, "%", source='material.composting.CompostingLeaves_1'),
                'maturity' : "very mature",
                'method' : "VDLUFA", 
                },
    }
}

result_config_composting_operhours=start_simulation(config_composting_operhours)
#npv_graphic(result_config_composting_transport)
result_config_composting_operhours["composting"]["t1"][0]["cashflow"].loc["transport.LandTransportMultiple_1"].loc["transport price - multiple routes", "%"]
result_config_composting_operhours["composting"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas().sum().sum()
result_config_composting_operhours["composting"]["t1"][0]["cashflow"]



list_operhours=[7500, 7750, 8000, 8250, 8500]
list_npv_comp_operhours=[]
for i in range(0,len(list_operhours)):
    list_npv_comp_operhours.append(result_config_composting_operhours["composting"]["t1"][i]["npv_value"])
list_npv_comp_operhours

list_GHG_comp_operhours=[]
for i in range(0,len(list_operhours)):
    list_GHG_comp_operhours.append(result_config_composting_operhours["composting"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
list_GHG_comp_operhours


#####

config_pre_NaOH_biogas_operhours = {
    ### Main settings
    "simulation"   :{
        'name': "pretreatment and biogas - with NaOH",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.202, 0.227, 0.179, 0.126, 0.133, 0.132]],
                'dist_km': [[42, 33, 65, 27, 30, 32]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "production.plant.pretreatment_leaves.PretreatmentLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'method' : "NaOH",
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.pretreatment_leaves.PretreatmentLeaves_1'),
                'CHP_kW':2500,
                'land_purchase': True,
                'land_rent': False,
                'labour_costs':"FNR_2016", 
                'maintenance_costs':"FNR_2016",
                'new_infrastructure': True,
                'operation_hours': [7500, 7750, 8000, 8250, 8500]
                },
        
        "transport.LandTransportMultiple_2" :{
                'I': prod('biogas digestate', 100, "%", source='material.biogas.BiogasPlant_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}        

result_config_pre_NaOH_biogas_operhours=start_simulation(config_pre_NaOH_biogas_operhours)
#npv_graphic(result_config_pre_NaOH_biogas_transport)
result_config_pre_NaOH_biogas_operhours["pretreatment and biogas - with NaOH"]["gwp"].loc["t1",i,:,:,"GWP100"]
["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas()
result_config_pre_NaOH_biogas_operhours["pretreatment and biogas - with NaOH"]["t1"][1]["cashflow"]
result_config_pre_NaOH_biogas_operhours["pretreatment and biogas - with NaOH"]["t1"][2]["cashflow"]
result_config_pre_NaOH_biogas_operhours["pretreatment and biogas - with NaOH"]["t1"][3]["cashflow"]
result_config_pre_NaOH_biogas_operhours["pretreatment and biogas - with NaOH"]["t1"][4]["cashflow"]

result_config_pre_NaOH_biogas_operhours["pretreatment and biogas - with NaOH"]["t1"][0]["cashflow"]
result_config_pre_NaOH_biogas_operhours["pretreatment and biogas - with NaOH"]["t1"][1]["cashflow"]
result_config_pre_NaOH_biogas_operhours["pretreatment and biogas - with NaOH"]["t1"][2]["cashflow"]
result_config_pre_NaOH_biogas_operhours["pretreatment and biogas - with NaOH"]["t1"][3]["cashflow"]
result_config_pre_NaOH_biogas_operhours["pretreatment and biogas - with NaOH"]["t1"][4]["cashflow"]



list_operhours=[7500, 7750, 8000, 8250, 8500]
list_npv_biogas_NaOH_operhours=[]
for i in range(0,len(list_operhours)):
    list_npv_biogas_NaOH_operhours.append(result_config_pre_NaOH_biogas_operhours["pretreatment and biogas - with NaOH"]["t1"][i]["npv_value"])
list_npv_biogas_NaOH_operhours

list_GHG_biogas_NaOH_operhours=[]
for i in range(0,len(list_operhours)):
    list_GHG_biogas_NaOH_operhours.append(result_config_pre_NaOH_biogas_operhours["pretreatment and biogas - with NaOH"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
list_GHG_biogas_NaOH_operhours

#######

config_pre_biogas_operhours = {
    ### Main settings
    "simulation"   :{
        'name': "pretreatment and biogas - no NaOH",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.202, 0.227, 0.179, 0.126, 0.133, 0.132]],
                'dist_km': [[42, 33, 65, 27, 30, 32]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'CHP_kW':2700,
                'land_purchase': True,
                'land_rent': False,
                'labour_costs':"FNR_2016", 
                'maintenance_costs':"FNR_2016",
                'new_infrastructure': True, 
                'operation_hours': [7500, 7750, 8000, 8250, 8500]
                },
        
        "transport.LandTransportMultiple_2" :{
                'I': prod('biogas digestate', 100, "%", source='material.biogas.BiogasPlant_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}

result_config_pre_biogas_operhours=start_simulation(config_pre_biogas_operhours)
list_operhours=[7500, 7750, 8000, 8250, 8500]
list_npv_biogas_operhours=[]
for i in range(0,len(list_operhours)):
    list_npv_biogas_operhours.append(result_config_pre_biogas_operhours["pretreatment and biogas - no NaOH"]["t1"][i]["npv_value"])
list_npv_biogas_operhours

list_GHG_biogas_operhours=[]
for i in range(0,len(list_operhours)):
    list_GHG_biogas_operhours.append(result_config_pre_biogas_operhours["pretreatment and biogas - no NaOH"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
list_GHG_biogas_operhours


#gasification
config_gasification_operhours = {
    ### Main settings
    "simulation"   :{
        'name': "gasification",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },


        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'kW':3600,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': False,
                'land_rent': False,
                'new_infrastructure': False,
                'operation_hours_year':[7500, 7750, 8000, 8250, 8500]
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.gasification.Gasification_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.gasification.Gasification_1')
                },
    },
}

result_config_gasification_operhours=start_simulation(config_gasification_operhours)
list_operhours=[7500, 7750, 8000, 8250, 8500]
list_npv_gasification_operhours=[]
for i in range(0,len(list_operhours)):
    list_npv_gasification_operhours.append(result_config_gasification_operhours["gasification"]["t1"][i]["npv_value"])
list_npv_gasification_operhours

list_GHG_gasification_operhours=[]
for i in range(0,len(list_operhours)):
    list_GHG_gasification_operhours.append(result_config_gasification_operhours["gasification"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
list_GHG_gasification_operhours


result_config_gasification_operhours["gasification"]["t1"][0]["cashflow"]
result_config_gasification_operhours["gasification"]["t1"][1]["cashflow"]
result_config_gasification_operhours["gasification"]["t1"][2]["cashflow"]
result_config_gasification_operhours["gasification"]["t1"][3]["cashflow"]
result_config_gasification_operhours["gasification"]["t1"][4]["cashflow"]

result_config_gasification_operhours["gasification"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas()
result_config_gasification_operhours["gasification"]["gwp"].loc["t1",1,:,:,"GWP100"].to_pandas()
result_config_gasification_operhours["gasification"]["gwp"].loc["t1",2,:,:,"GWP100"].to_pandas()
result_config_gasification_operhours["gasification"]["gwp"].loc["t1",3,:,:,"GWP100"].to_pandas()
result_config_gasification_operhours["gasification"]["gwp"].loc["t1",4,:,:,"GWP100"].to_pandas()


result_config_gasification_operhours["gasification"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas()


#pelletization and gasification

config_pellet_gasification_operhours = {
    ### Main settings
    "simulation"   :{
        'name': "pelletization and gasification",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.pelletization.PelletFactory_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'land_purchase': True, 
                'land_rent': False,
                'new_infrastructure': True
                },

        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves pellets', 100, "%", source='material.pelletization.PelletFactory_1'),
                'kW': 4000,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': True,
                'land_rent': False,
                'new_infrastructure': False,
                'operation_hours_year':[7500, 7750, 8000, 8250, 8500]
                },
          
    }
}
result_config_pellet_gasification_operhours=start_simulation(config_pellet_gasification_operhours)

list_operhours=[7500, 7750, 8000, 8250, 8500]
list_npv_pellet_gasification_operhours=[]
for i in range(0,len(list_operhours)):
    list_npv_pellet_gasification_operhours.append(result_config_pellet_gasification_operhours["pelletization and gasification"]["t1"][i]["npv_value"])
list_npv_pellet_gasification_operhours

list_GHG_pellet_gasification_operhours=[]
for i in range(0,len(list_operhours)):
    list_GHG_pellet_gasification_operhours.append(result_config_pellet_gasification_operhours["pelletization and gasification"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
list_GHG_pellet_gasification_operhours

#cofiring - dry
config_cofiring_dry_operhours = {
    ### Main settings
    "simulation"   :{
        'name': "cofiring - dry",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.cofiring.BiomassDrying_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'new_infrastructure': True
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves dry', 100, "%", source='material.cofiring.BiomassDrying_1'),
                'kW':12000,
                'electrical_efficiency': 0.14,
                'retrofitting': True, 
                'land_purchase': False,
                'land_rent': False,
                'new_infrastructure': False,
                'operation_hours':[7500, 7750, 8000, 8250, 8500]
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.cofiring.CoFiring_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },    

        "external.Electricity_1" :{
                'I': prod('electrical energy - biomass share', 100, "%", source='material.cofiring.CoFiring_1')
                },
    }
}

result_config_cofiring_dry_operhours=start_simulation(config_cofiring_dry_operhours)
list_operhours=[7500, 7750, 8000, 8250, 8500]
list_npv_cofiring_dry_operhours=[]
for i in range(0,len(list_operhours)):
    list_npv_cofiring_dry_operhours.append(result_config_cofiring_dry_operhours["cofiring - dry"]["t1"][i]["npv_value"])
list_npv_cofiring_dry_operhours

list_GHG_cofiring_dry_operhours=[]
for i in range(0,len(list_operhours)):
    list_GHG_cofiring_dry_operhours.append(result_config_cofiring_dry_operhours["cofiring - dry"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
list_GHG_cofiring_dry_operhours


result_config_cofiring_dry_operhours["cofiring - dry"]["t1"][0]["cashflow"]
result_config_cofiring_dry_operhours["cofiring - dry"]["t1"][1]["cashflow"]
result_config_cofiring_dry_operhours["cofiring - dry"]["t1"][2]["cashflow"]

#wet biomass
config_cofiring_wet_operhours = {
    ### Main settings
    "simulation"   :{
        'name': "cofiring - wet",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'kW':10000,
                'electrical_efficiency': 0.14,
                'retrofitting': True, 
                'land_purchase': False,
                'land_rent': False,
                'new_infrastructure': False,
                'operation_hours':[7500, 7750, 8000, 8250, 8500]
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.cofiring.CoFiring_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },    

        "external.Electricity_1" :{
                'I': prod('electrical energy - biomass share', 100, "%", source='material.cofiring.CoFiring_1')
                },
    }
}

result_config_cofiring_wet_operhours=start_simulation(config_cofiring_wet_operhours)
list_operhours=[7500, 7750, 8000, 8250, 8500]
list_npv_cofiring_wet_operhours=[]
for i in range(0,len(list_operhours)):
    list_npv_cofiring_wet_operhours.append(result_config_cofiring_wet_operhours["cofiring - wet"]["t1"][i]["npv_value"])
list_npv_cofiring_wet_operhours

list_GHG_cofiring_wet_operhours=[]
for i in range(0,len(list_operhours)):
    list_GHG_cofiring_wet_operhours.append(result_config_cofiring_wet_operhours["cofiring - wet"]["gwp"].loc["t1",i,:,:,"GWP100"].to_pandas().sum().sum())
list_GHG_cofiring_wet_operhours



#####Dataframes of variation
list_GHG_comp_operhours
list_GHG_biogas_NaOH_operhours
list_GHG_biogas_operhours
list_GHG_gasification_operhours
list_GHG_pellet_gasification_operhours
list_GHG_cofiring_dry_operhours

GHG_variation_operhours=pd.DataFrame(index=list_operhours, columns=[])
GHG_variation_operhours.loc[8000, "composting"]=list_GHG_comp_operhours[0]
GHG_variation_operhours["pretreatment (NaOH) - biogas"]=[GHG_variation_operhours.loc[8000, "composting"]-i for i in list_GHG_biogas_NaOH_operhours]
GHG_variation_operhours["pretreatment - biogas"]=[GHG_variation_operhours.loc[8000, "composting"]-i for i in list_GHG_biogas_operhours]
GHG_variation_operhours["gasification"]=[GHG_variation_operhours.loc[8000, "composting"]-i for i in list_GHG_gasification_operhours]
GHG_variation_operhours["pelletization and gasification"]=[GHG_variation_operhours.loc[8000, "composting"]-i for i in list_GHG_pellet_gasification_operhours]
GHG_variation_operhours["cofiring-dry"]=[GHG_variation_operhours.loc[8000, "composting"]-i for i in list_GHG_cofiring_dry_operhours]
GHG_variation_operhours["cofiring-wet"]=[GHG_variation_operhours.loc[8000, "composting"]-i for i in list_GHG_cofiring_wet_operhours]

GHG_variation_operhours

npv_variation_operhours=pd.DataFrame(index=list_operhours, columns=[])
npv_variation_operhours.loc[8000, "composting"]=list_npv_comp_operhours[0]
npv_variation_operhours["pretreatment (NaOH) - biogas"]=list_npv_biogas_NaOH_operhours
npv_variation_operhours["pretreatment - biogas"]=list_npv_biogas_operhours
npv_variation_operhours["gasification"]=list_npv_gasification_operhours
npv_variation_operhours["pelletization and gasification"]=list_npv_pellet_gasification_operhours
npv_variation_operhours["cofiring-dry"]=list_npv_cofiring_dry_operhours
npv_variation_operhours["cofiring-wet"]=list_npv_cofiring_wet_operhours

npv_variation_operhours

fig=plt.figure()
ax=fig.add_subplot(121)
ax.plot(GHG_variation_operhours.index, GHG_variation_operhours["pretreatment (NaOH) - biogas"], label="pretreatment (NaOH) - biogas", marker="v")
ax.plot(GHG_variation_operhours.index, GHG_variation_operhours["pretreatment - biogas"], label="pretreatment - biogas", marker="v")
ax.plot(GHG_variation_operhours.index, GHG_variation_operhours["gasification"], label="gasification", marker="v")
ax.plot(GHG_variation_operhours.index, GHG_variation_operhours["pelletization and gasification"], label="pelletization and gasification", marker="v")
ax.plot(GHG_variation_operhours.index, GHG_variation_operhours["cofiring-dry"], label="cofiring-dry", marker="v")
ax.plot(GHG_variation_operhours.index, GHG_variation_operhours["cofiring-wet"], label="cofiring-wet", marker="v")
ax.set_ylabel("Emissions abatement (kg CO2eq)")
ax1=fig.add_subplot(122, sharey=ax )
ax1.plot(npv_variation_operhours.index, npv_variation_operhours["pretreatment (NaOH) - biogas"], label="pretreatment (NaOH) - biogas", marker="*")
ax1.plot(npv_variation_operhours.index, npv_variation_operhours["pretreatment - biogas"], label="pretreatment - biogas", marker="*")
ax1.plot(npv_variation_operhours.index, npv_variation_operhours["gasification"], label="gasification", marker="*")
ax1.plot(npv_variation_operhours.index, npv_variation_operhours["pelletization and gasification"], label="pelletization and gasification", marker="*")
ax1.plot(npv_variation_operhours.index, npv_variation_operhours["cofiring-dry"], label="cofiring-dry", marker="*")
ax1.plot(npv_variation_operhours.index, npv_variation_operhours["cofiring-wet"], label="cofiring-wet", marker="*")
ax1.set_ylabel("NPV")
fig.supxlabel("Operation hours")

fig.legend(loc=9, ncol=4)
plt.show()