#here variation of the discount rate
import start
from tools import prod
from analysis.timestep_chains import start_simulation
from tools import convert_molmasses
from analysis.economic.macc import MACC, circle_plot_MAC, single_graphic_MACC
from simulation.data_management_and_plotting.npv_plotting import npv_graphic

import matplotlib.pyplot as plt
import pandas as pd

#NOTE: For the discount rate, for energy projects tends to be 7%. There are other methods like the WAAC to calculate the discount rate, but
# it requires financial information from the company and more calculation, therefore the discount rate variation would go step-wise 1%


config_composting_disc_rate = {
    ### Main settings
    "simulation"   :{
        'name': "composting",
        'type': "all combinations", 
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07 
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture"
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.42, 0.18, 0.10, 0.07, 0.06, 0.06, 0.05, 0.03, 0.01]],
                'dist_km': [[98, 50, 188, 146, 21, 11, 44, 189, 153]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.composting.CompostingLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "Andersen_2010",
                'CH4_emis': 0.02, 
                'land_purchase' : True, 
                'land_rent' : False,
                'fixed_operation_cost' : True,
                'new_infrastructure': True
                },

        "production.plant.fertilization.CompostSpreading_1" :{
                'I': prod('leaves compost', 100, "%", source='material.composting.CompostingLeaves_1'),
                'maturity' : "very mature",
                'method' : "VDLUFA", 
                },
    }
}

result_config_composting_disc_rate=start_simulation(config_composting_disc_rate)
#npv_graphic(result_config_composting_transport)
result_config_composting_disc_rate["composting"]["t1"][0]["cashflow"].loc["transport.LandTransportMultiple_1"].loc["transport price - multiple routes", "%"]
result_config_composting_disc_rate["composting"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas().sum().sum()

list_disc_rate=[0.05, 0.06, 0.07, 0.08, 0.09]
list_npv_comp_disc_rate=[]
for i in range(0,len(list_disc_rate)):
    list_npv_comp_disc_rate.append(result_config_composting_disc_rate["composting"]["t1"][i]["npv_value"])
list_npv_comp_disc_rate


#####

config_pre_NaOH_biogas_disc_rate = {
    ### Main settings
    "simulation"   :{
        'name': "pretreatment and biogas - with NaOH",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": [0.05, 0.06, 0.07, 0.08, 0.09]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.202, 0.227, 0.179, 0.126, 0.133, 0.132]],
                'dist_km': [[42, 33, 65, 27, 30, 32]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "production.plant.pretreatment_leaves.PretreatmentLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'method' : "NaOH",
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.pretreatment_leaves.PretreatmentLeaves_1'),
                'CHP_kW':2500,
                'land_purchase': True,
                'land_rent': False,
                'labour_costs':"FNR_2016", 
                'maintenance_costs':"FNR_2016",
                'new_infrastructure': True,
                },
        
        "transport.LandTransportMultiple_2" :{
                'I': prod('biogas digestate', 100, "%", source='material.biogas.BiogasPlant_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}        

result_config_pre_NaOH_biogas_disc_rate=start_simulation(config_pre_NaOH_biogas_disc_rate)
npv_graphic(result_config_pre_NaOH_biogas_disc_rate, 2)

result_config_pre_NaOH_biogas_disc_rate["pretreatment and biogas - with NaOH"]["t1"][0]["npv_value"][0]


list_disc_rate_biogas=[0.05, 0.06, 0.07, 0.08, 0.09]
list_npv_biogas_NaOH_disc_rate=[]
for i in range(0,len(list_disc_rate_biogas)):
    list_npv_biogas_NaOH_disc_rate.append(result_config_pre_NaOH_biogas_disc_rate["pretreatment and biogas - with NaOH"]["t1"][0]["npv_value"][i])
list_npv_biogas_NaOH_disc_rate


config_pre_biogas_disc_rate = {
    ### Main settings
    "simulation"   :{
        'name': "pretreatment and biogas - no NaOH",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": [0.05, 0.06, 0.07, 0.08, 0.09]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[0.202, 0.227, 0.179, 0.126, 0.133, 0.132]],
                'dist_km': [[42, 33, 65, 27, 30, 32]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'CHP_kW':2700,
                'land_purchase': True,
                'land_rent': False,
                'labour_costs':"FNR_2016", 
                'maintenance_costs':"FNR_2016",
                'new_infrastructure': True, 
                },
        
        "transport.LandTransportMultiple_2" :{
                'I': prod('biogas digestate', 100, "%", source='material.biogas.BiogasPlant_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}

result_config_pre_biogas_disc_rate=start_simulation(config_pre_biogas_disc_rate)
npv_graphic(result_config_pre_biogas_disc_rate, 2)
result_config_pre_biogas_disc_rate["pretreatment and biogas - no NaOH"]["t1"][0]["npv_value"]

list_disc_rate_biogas= [0.05, 0.06, 0.07, 0.08, 0.09]
list_npv_biogas_disc_rate=[]
for i in range(0,len(list_disc_rate_biogas)):
    list_npv_biogas_disc_rate.append(result_config_pre_biogas_disc_rate["pretreatment and biogas - no NaOH"]["t1"][0]["npv_value"][i])
list_npv_biogas_disc_rate


#gasification
config_gasification_disc_rate = {
    ### Main settings
    "simulation"   :{
        'name': "gasification",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": [0.05, 0.06, 0.07, 0.08, 0.09]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },


        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'kW':3600,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': False,
                'land_rent': False,
                'new_infrastructure': False,
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.gasification.Gasification_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.gasification.Gasification_1')
                },

    },
}

result_config_gasification_disc_rate=start_simulation(config_gasification_disc_rate)
list_disc_rate_gasification=[0.05, 0.06, 0.07, 0.08, 0.09]
list_npv_gasification_disc_rate=[]
for i in range(0,len(list_disc_rate_gasification)):
    list_npv_gasification_disc_rate.append(result_config_gasification_disc_rate["gasification"]["t1"][0]["npv_value"][i])
list_npv_gasification_disc_rate


#pelletization and gasification

config_pellet_gasification_disc_rate = {
    ### Main settings
    "simulation"   :{
        'name': "pelletization and gasification",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": [0.05, 0.06, 0.07, 0.08, 0.09]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.pelletization.PelletFactory_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'land_purchase': True, 
                'land_rent': False,
                'new_infrastructure': True
                },

        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves pellets', 100, "%", source='material.pelletization.PelletFactory_1'),
                'kW': 4000,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': True,
                'land_rent': False,
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.gasification.Gasification_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.gasification.Gasification_1')
                },

    }
}
result_config_pellet_gasification_disc_rate=start_simulation(config_pellet_gasification_disc_rate)

list_disc_rate_gasification=[0.05, 0.06, 0.07, 0.08, 0.09]
list_npv_pellet_gasification_disc_rate=[]
for i in range(0,len(list_disc_rate_gasification)):
    list_npv_pellet_gasification_disc_rate.append(result_config_pellet_gasification_disc_rate["pelletization and gasification"]["t1"][0]["npv_value"][i])
list_npv_pellet_gasification_disc_rate


#cofiring - dry
config_cofiring_dry_disc_rate = {
    ### Main settings
    "simulation"   :{
        'name': "cofiring - dry",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": [0.05, 0.06, 0.07, 0.08, 0.09]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.cofiring.BiomassDrying_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'new_infrastructure': True
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves dry', 100, "%", source='material.cofiring.BiomassDrying_1'),
                'kW':12000,
                'operation_hours':8000, 
                'retrofitting': True, 
                'land_purchase': False,
                'land_rent': False,
                'new_infrastructure': False,
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.cofiring.CoFiring_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },    

        "external.Electricity_1" :{
                'I': prod('electrical energy - biomass share', 100, "%", source='material.cofiring.CoFiring_1')
                },
                
    }
}

result_config_cofiring_dry_disc_rate=start_simulation(config_cofiring_dry_disc_rate)
list_disc_rate_cofiring=[0.05, 0.06, 0.07, 0.08, 0.09]
list_npv_cofiring_dry_disc_rate=[]
for i in range(0,len(list_disc_rate_cofiring)):
    list_npv_cofiring_dry_disc_rate.append(result_config_cofiring_dry_disc_rate["cofiring - dry"]["t1"][0]["npv_value"][i])
list_npv_cofiring_dry_disc_rate


#wet biomass
config_cofiring_wet_disc_rate = {
    ### Main settings
    "simulation"   :{
        'name': "cofiring - wet",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": [0.05, 0.06, 0.07, 0.08, 0.09]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'kW':10000,
                'operation_hours':8000, 
                'retrofitting': True, 
                'land_purchase': False,
                'land_rent': False,
                'new_infrastructure': False,
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.cofiring.CoFiring_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },    

        "external.Electricity_1" :{
                'I': prod('electrical energy - biomass share', 100, "%", source='material.cofiring.CoFiring_1')
                },
    }
}

result_config_cofiring_wet_disc_rate=start_simulation(config_cofiring_wet_disc_rate)
list_disc_rate_cofiring=[0.05, 0.06, 0.07, 0.08, 0.09]
list_npv_cofiring_wet_disc_rate=[]
for i in range(0,len(list_disc_rate_cofiring)):
    list_npv_cofiring_wet_disc_rate.append(result_config_cofiring_wet_disc_rate["cofiring - wet"]["t1"][0]["npv_value"][i])
list_npv_cofiring_wet_disc_rate



npv_variation_disc_rate=pd.DataFrame(index=list_disc_rate, columns=[])

for i in range(0,len(list_disc_rate_biogas)):
    npv_variation_disc_rate.loc[list_disc_rate_biogas[i], "pretreatment (NaOH) - biogas"]=list_npv_biogas_NaOH_disc_rate[i]
    npv_variation_disc_rate.loc[list_disc_rate_biogas[i], "pretreatment - biogas"]=list_npv_biogas_disc_rate[i]

for i in range(0,len(list_disc_rate_gasification)):
    npv_variation_disc_rate.loc[list_disc_rate_gasification[i], "gasification"]=list_npv_gasification_disc_rate[i]
    npv_variation_disc_rate.loc[list_disc_rate_gasification[i], "pelletization and gasification"]=list_npv_pellet_gasification_disc_rate[i]

for i in range(0,len(list_disc_rate_cofiring)):
    npv_variation_disc_rate.loc[list_disc_rate_cofiring[i], "cofiring-dry"]=list_npv_cofiring_dry_disc_rate[i]
    npv_variation_disc_rate.loc[list_disc_rate_cofiring[i], "cofiring-wet"]=list_npv_cofiring_wet_disc_rate[i]

npv_variation_disc_rate

# GHG_variation_disc_rate["pretreatment (NaOH) - biogas"]
# GHG_variation_disc_rate.index

fig=plt.figure()
ax1=fig.add_subplot(111)
ax1.plot(npv_variation_disc_rate.index, npv_variation_disc_rate["pretreatment (NaOH) - biogas"], label="pretreatment (NaOH) - biogas", marker="*")
ax1.plot(npv_variation_disc_rate.index, npv_variation_disc_rate["pretreatment - biogas"], label="pretreatment - biogas", marker="*")
ax1.plot(npv_variation_disc_rate.index, npv_variation_disc_rate["gasification"], label="gasification", marker="*")
ax1.plot(npv_variation_disc_rate.index, npv_variation_disc_rate["pelletization and gasification"], label="pelletization and gasification", marker="*")
ax1.plot(npv_variation_disc_rate.index, npv_variation_disc_rate["cofiring-dry"], label="cofiring-dry", marker="*")
ax1.plot(npv_variation_disc_rate.index, npv_variation_disc_rate["cofiring-wet"], label="cofiring-wet", marker="*")
ax1.set_ylabel("NPV")
fig.supxlabel("Discount rate")

fig.legend(loc=9, ncol=4)
plt.show()