import start
from tools import prod
from analysis.timestep_chains import start_simulation
from tools import convert_molmasses
from analysis.economic.macc import MACC, circle_plot_MAC, single_graphic_MACC, circle_plot_NPV_emissions
from simulation.data_management_and_plotting.npv_plotting import npv_graphic

import matplotlib.pyplot as plt
import pandas as pd


config_composting_elec_tariff = {
    ### Main settings
    "simulation"   :{
        'name': "composting",
        'type': "all combinations", 
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture"
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.composting.CompostingLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "Andersen_2010",
                'CH4_emis': 0.02, 
                'land_purchase' : True, 
                'land_rent' : False,
                'fixed_operation_cost' : True,
                'new_infrastructure': True
                },

        "production.plant.fertilization.CompostSpreading_1" :{
                'I': prod('leaves compost', 100, "%", source='material.composting.CompostingLeaves_1'),
                'maturity' : "very mature",
                'method' : "VDLUFA", 
                },
    }
}

result_config_composting_elec_tariff=start_simulation(config_composting_elec_tariff)
#npv_graphic(result_config_composting_transport)
result_config_composting_elec_tariff["composting"]["t1"][0]["cashflow"].loc["transport.LandTransportMultiple_1"].loc["transport price - multiple routes", "%"]
result_config_composting_elec_tariff["composting"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas().sum().sum()



#####

config_pre_NaOH_biogas_elec_tariff = {
    ### Main settings
    "simulation"   :{
        'name': "biogas (with NaOH pretreatment)",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "production.plant.pretreatment_leaves.PretreatmentLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'method' : "NaOH",
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.pretreatment_leaves.PretreatmentLeaves_1'),
                'CHP_kW':2700,
                'land_purchase': True,
                'land_rent': False,
                'labour_costs':"FNR_2016", 
                'maintenance_costs':"FNR_2016",
                'new_infrastructure': True,
                'EEG_tariff':[("manual", 0.175), ("manual", 0.125), ("manual", 0.15), ("manual", 0.20), ("manual", 0.225)]
                },
        
        "transport.LandTransportMultiple_2" :{
                'I': prod('biogas digestate', 100, "%", source='material.biogas.BiogasPlant_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}        

result_config_pre_NaOH_biogas_elec_tariff=start_simulation(config_pre_NaOH_biogas_elec_tariff)
#npv_graphic(result_config_pre_NaOH_biogas_elec_tariff, 2)
# result_config_pre_NaOH_biogas_elec_tariff["biogas - with NaOH"]["t1"][0]["cashflow"]
# result_config_pre_NaOH_biogas_elec_tariff["biogas - with NaOH"]["t1"][1]["cashflow"]

# result_config_pre_NaOH_biogas_elec_tariff["biogas - with NaOH"]["t1"][1]["config"]

#######

config_pre_biogas_elec_tariff = {
    ### Main settings
    "simulation"   :{
        'name': "biogas",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'CHP_kW':2700,
                'land_purchase': True,
                'land_rent': False,
                'labour_costs':"FNR_2016", 
                'maintenance_costs':"FNR_2016",
                'new_infrastructure': True, 
                'EEG_tariff':[("manual", 0.175), ("manual", 0.125), ("manual", 0.15), ("manual", 0.20), ("manual", 0.225)]
                },
        
        "transport.LandTransportMultiple_2" :{
                'I': prod('biogas digestate', 100, "%", source='material.biogas.BiogasPlant_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}

result_config_pre_biogas_elec_tariff=start_simulation(config_pre_biogas_elec_tariff)
#npv_graphic(result_config_pre_biogas_elec_tariff, 2)
result_config_pre_biogas_elec_tariff["biogas"]["t1"][0]["cashflow"]


#gasification
config_gasification_elec_tariff = {
    ### Main settings
    "simulation"   :{
        'name': "gasification",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },


        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'kW':4000,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': True,
                'land_rent': False,
                'new_infrastructure': True,
                'EEG_tariff':[("manual", 0.175), ("manual", 0.125), ("manual", 0.15), ("manual", 0.20), ("manual", 0.225)]
                },

        "production.plant.fertilization.BiocharSpreading_1" :{
                'I': prod("unconverted biochar", 100, "%", source='material.gasification.Gasification_1')
                },    

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.gasification.Gasification_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.gasification.Gasification_1')
                },

    },
}

result_config_gasification_elec_tariff=start_simulation(config_gasification_elec_tariff)
# result_config_gasification_elec_tariff["gasification"]["t1"][0]["cashflow"]
# result_config_gasification_elec_tariff["gasification"]["t1"][1]["cashflow"]
# result_config_gasification_elec_tariff["gasification"]["t1"][2]["cashflow"]
# result_config_gasification_elec_tariff["gasification"]["t1"][3]["cashflow"]
# result_config_gasification_elec_tariff["gasification"]["t1"][4]["cashflow"]

# result_config_gasification_elec_tariff["gasification"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas()
# result_config_gasification_elec_tariff["gasification"]["gwp"].loc["t1",1,:,:,"GWP100"].to_pandas()
# result_config_gasification_elec_tariff["gasification"]["gwp"].loc["t1",2,:,:,"GWP100"].to_pandas()
# result_config_gasification_elec_tariff["gasification"]["gwp"].loc["t1",3,:,:,"GWP100"].to_pandas()
# result_config_gasification_elec_tariff["gasification"]["gwp"].loc["t1",4,:,:,"GWP100"].to_pandas()



#pelletization and gasification

config_pellet_gasification_elec_tariff = {
    ### Main settings
    "simulation"   :{
        'name': "gasification (with pelletization)",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.pelletization.PelletFactory_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'land_purchase': True, 
                'land_rent': False,
                'new_infrastructure': True
                },

        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves pellets', 100, "%", source='material.pelletization.PelletFactory_1'),
                'kW': 4000,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': True,
                'land_rent': False,
                'new_infrastructure': True,
                'EEG_tariff':[("manual", 0.175), ("manual", 0.125), ("manual", 0.15), ("manual", 0.20), ("manual", 0.225)]
                },

        "production.plant.fertilization.BiocharSpreading_1" :{
                'I': prod("unconverted biochar", 100, "%", source='material.gasification.Gasification_1')
                },    

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.gasification.Gasification_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.gasification.Gasification_1')
                },

    }
}
result_config_pellet_gasification_elec_tariff=start_simulation(config_pellet_gasification_elec_tariff)


#cofiring - dry
config_cofiring_dry_elec_tariff = {
    ### Main settings
    "simulation"   :{
        'name': "cofiring",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]], 
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.cofiring.BiomassDrying_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'new_infrastructure': True
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves dry', 100, "%", source='material.cofiring.BiomassDrying_1'),
                'kW':12000,
                'operation_hours':8000, 
                'electrical_efficiency': 0.36,
                'retrofitting': True, 
                'land_purchase': True,
                'land_rent': False,
                'new_infrastructure': True,
                # 'energy_tariff':[("manual", 0.003), ("manual", 0.0025), ("manual", 0.00275), ("manual", 0.00325), ("manual", 0.0035)],
                'energy_tariff':[("manual", 0.0613), ("manual", 0.041), ("manual", 0.051), ("manual", 0.071), ("manual", 0.081)],
                # 'energy_tariff':[("bidding average", None), ("Knapp et al 2019", None)],
                'include_thermal': True
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.cofiring.CoFiring_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },    

        "external.Electricity_1" :{
                'I': prod('electrical energy - biomass share', 100, "%", source='material.cofiring.CoFiring_1')
                },
                
    }
}

result_config_cofiring_dry_elec_tariff=start_simulation(config_cofiring_dry_elec_tariff)
result_config_cofiring_dry_elec_tariff["cofiring"]["t1"][0]["outputs"]
result_config_cofiring_dry_elec_tariff["cofiring"]["t1"][0]["cashflow"]


config_cofiring_dry_pellet_elec_tariff= {
    ### Main settings
    "simulation"   :{
        'name': "cofiring (with pelletization)",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]], 
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.pelletization.PelletFactory_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'land_purchase': True, 
                'land_rent': False,
                'new_infrastructure': True
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves pellets', 100, "%", source='material.pelletization.PelletFactory_1'),
                'kW':12000,
                'operation_hours':8000, 
                'electrical_efficiency': 0.36,
                'retrofitting': True, 
                'land_purchase': True,
                'land_rent': False,
                'new_infrastructure': True,
                # 'energy_tariff':[("manual", 0.003), ("manual", 0.0025), ("manual", 0.00275), ("manual", 0.00325), ("manual", 0.0035)],
                'energy_tariff':[("manual", 0.0613), ("manual", 0.041), ("manual", 0.051), ("manual", 0.071), ("manual", 0.081)],
                # 'energy_tariff':[("bidding average", None), ("Knapp et al 2019", None)],
                'include_thermal': True
                },
        
        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.cofiring.CoFiring_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },    

        "external.Electricity_1" :{
                'I': prod('electrical energy - biomass share', 100, "%", source='material.cofiring.CoFiring_1')
                },
    }
}

result_config_dry_pellet_elec_tariff=start_simulation(config_cofiring_dry_pellet_elec_tariff)


list_electrical_tarifficiency=[result_config_composting_elec_tariff, result_config_pre_NaOH_biogas_elec_tariff, result_config_pre_biogas_elec_tariff, 
                            result_config_gasification_elec_tariff, result_config_pellet_gasification_elec_tariff, result_config_cofiring_dry_elec_tariff, 
                            result_config_dry_pellet_elec_tariff]

# dict_annotation={"default":{"pelletization and cofiring": 0.36,
#                             "cofiring": 0.36,
#                             "pelletization and gasification": 0.25,
#                             "gasification": 0.25,
#                             "biogas - with NaOH": 0.38,
#                             "biogas - no NaOH": 0.38,
#                             },
                
#                 "sensitivity":{ "pelletization and cofiring": [0.28, 0.32, 0.40, 0.44], 
#                                 "cofiring": [0.28, 0.32, 0.40, 0.44], 
#                                 "pelletization and gasification": [0.17, 0.21, 0.29, 0.33], 
#                                 "gasification": [0.17, 0.21, 0.29, 0.33], 
#                                 "biogas - with NaOH": [0.30, 0.34, 0.42, 0.46],
#                                 "biogas - no NaOH": [0.30, 0.34, 0.42, 0.46]
#                             }
#                 }

# dict_annotation["sensitivity"]["pelletization and cofiring"][1-1]


df_sensitivity_elec_tariff=MACC(list_electrical_tarifficiency, BAU_scenario="composting", sensitivity=True,obs="electricity tariff").marginal_abatement_cost()
df_sensitivity_elec_tariff

# df_sensitivity=df_sensitivity_elec_tariff[df_sensitivity_elec_tariff["type"]=="sensitivity"]
# df_sensitivity


# for sim in df_sensitivity_elec_tariff.index: 
#     if df_sensitivity_elec_tariff.loc[sim, "type"]=="default":
#         df_sensitivity_elec_tariff.loc[sim, "value obs"]=dict_annotation["default"][sim]
#     if df_sensitivity_elec_tariff.loc[sim, "type"]=="sensitivity":
#         df_sensitivity=df_sensitivity_elec_tariff[df_sensitivity_elec_tariff["type"]=="sensitivity"]
#         for sim in df_sensitivity.index:
            

#         index_value_obs=int(dict_annotation["sensitivity"][sim][-1:])
#         df_sensitivity_elec_tariff.loc[sim, "value obs"]=dict_annotation["sensitivity"][sim][index_value_obs-1]





circle_plot_NPV_emissions(df_sensitivity_elec_tariff, fixed_df=True, legend_ncol=3, sensitivity=True, size_factor=100)
#circle_plot_NPV_emissions(df_sensitivity_elec_tariff, fixed_df=True, legend_ncol=3, size_factor=1000)