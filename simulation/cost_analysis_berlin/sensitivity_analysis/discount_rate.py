#here variation of the discount rate
import start
from tools import prod
from analysis.timestep_chains import start_simulation
from tools import convert_molmasses
from analysis.economic.macc import MACC, circle_plot_MAC, single_graphic_MACC, circle_plot_NPV_emissions
from simulation.data_management_and_plotting.npv_plotting import npv_graphic

import matplotlib.pyplot as plt
import pandas as pd

#NOTE: For the discount rate, for energy projects tends to be 7%. There are other methods like the WAAC to calculate the discount rate, but
# it requires financial information from the company and more calculation, therefore the discount rate variation would go step-wise 1%


config_composting_disc_rate = {
    ### Main settings
    "simulation"   :{
        'name': "composting",
        'type': "all combinations", 
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": [0.07, 0.05, 0.06, 0.08, 0.09]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture"
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.composting.CompostingLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "Andersen_2010",
                'CH4_emis': 0.02, 
                'land_purchase' : True, 
                'land_rent' : False,
                'fixed_operation_cost' : True,
                'new_infrastructure': True
                },

        "production.plant.fertilization.CompostSpreading_1" :{
                'I': prod('leaves compost', 100, "%", source='material.composting.CompostingLeaves_1'),
                'maturity' : "very mature",
                'method' : "VDLUFA", 
                },
    }
}

result_config_composting_disc_rate=start_simulation(config_composting_disc_rate)
result_config_composting_disc_rate["composting"]["t1"][0]["cashflow"]
result_config_composting_disc_rate["composting"]["t1"][1]["cashflow"]

#####

config_pre_NaOH_biogas_disc_rate = {
    ### Main settings
    "simulation"   :{
        'name': "biogas (with NaOH pretreatment)",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": [0.07, 0.05, 0.06, 0.08, 0.09]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "production.plant.pretreatment_leaves.PretreatmentLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'method' : "NaOH",
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.pretreatment_leaves.PretreatmentLeaves_1'),
                'CHP_kW':2700,
                'land_purchase': True,
                'land_rent': False,
                'labour_costs':"FNR_2016", 
                'maintenance_costs':"FNR_2016",
                'new_infrastructure': True,
                },
        
        "transport.LandTransportMultiple_2" :{
                'I': prod('biogas digestate', 100, "%", source='material.biogas.BiogasPlant_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}        

result_config_pre_NaOH_biogas_disc_rate=start_simulation(config_pre_NaOH_biogas_disc_rate)



config_pre_biogas_disc_rate = {
    ### Main settings
    "simulation"   :{
        'name': "biogas",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": [0.07, 0.05, 0.06, 0.08, 0.09]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'CHP_kW':2700,
                'land_purchase': True,
                'land_rent': False,
                'labour_costs':"FNR_2016", 
                'maintenance_costs':"FNR_2016",
                'new_infrastructure': True, 
                },
        
        "transport.LandTransportMultiple_2" :{
                'I': prod('biogas digestate', 100, "%", source='material.biogas.BiogasPlant_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}

result_config_pre_biogas_disc_rate=start_simulation(config_pre_biogas_disc_rate)


#gasification
config_gasification_disc_rate = {
    ### Main settings
    "simulation"   :{
        'name': "gasification",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": [0.07, 0.05, 0.06, 0.08, 0.09]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },


        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'kW':4000,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': True,
                'land_rent': False,
                'new_infrastructure': True,
                },

        "production.plant.fertilization.BiocharSpreading_1" :{
                'I': prod("unconverted biochar", 100, "%", source='material.gasification.Gasification_1')
                },    

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.gasification.Gasification_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.gasification.Gasification_1')
                },

    },
}

result_config_gasification_disc_rate=start_simulation(config_gasification_disc_rate)


#pelletization and gasification
config_pellet_gasification_disc_rate = {
    ### Main settings
    "simulation"   :{
        'name': "gasification (with pelletization)",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": [0.07, 0.05, 0.06, 0.08, 0.09]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.pelletization.PelletFactory_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'land_purchase': True, 
                'land_rent': False,
                'new_infrastructure': True
                },

        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves pellets', 100, "%", source='material.pelletization.PelletFactory_1'),
                'kW': 4000,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': True,
                'land_rent': False,
                'new_infrastructure': True
                },

        "production.plant.fertilization.BiocharSpreading_1" :{
                'I': prod("unconverted biochar", 100, "%", source='material.gasification.Gasification_1')
                },    

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.gasification.Gasification_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.gasification.Gasification_1')
                },

    }
}
result_config_pellet_gasification_disc_rate=start_simulation(config_pellet_gasification_disc_rate)


#cofiring - dry
config_cofiring_dry_disc_rate = {
    ### Main settings
    "simulation"   :{
        'name': "cofiring",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": [0.07, 0.05, 0.06, 0.08, 0.09]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.cofiring.BiomassDrying_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'new_infrastructure': True
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves dry', 100, "%", source='material.cofiring.BiomassDrying_1'),
                'kW':12000,
                'operation_hours':8000, 
                'electrical_efficiency': 0.36,
                'retrofitting': True, 
                'land_purchase': True,
                'land_rent': False,
                'new_infrastructure': True,
                'include_thermal': True,
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.cofiring.CoFiring_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },    

        "external.Electricity_1" :{
                'I': prod('electrical energy - biomass share', 100, "%", source='material.cofiring.CoFiring_1')
                },
                
    }
}

result_config_cofiring_dry_disc_rate=start_simulation(config_cofiring_dry_disc_rate)


###
config_cofiring_dry_pellet_disc_rate= {
    ### Main settings
    "simulation"   :{
        'name': "cofiring (with pelletization)",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": [0.07, 0.05, 0.06, 0.08, 0.09]
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.pelletization.PelletFactory_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'land_purchase': True, 
                'land_rent': False,
                'new_infrastructure': True
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves pellets', 100, "%", source='material.pelletization.PelletFactory_1'),
                'kW':12000,
                'operation_hours':8000, 
                'retrofitting': True, 
                'land_purchase': True,
                'land_rent': False,
                'new_infrastructure': True,
                'include_thermal': True,
                },
        
        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.cofiring.CoFiring_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },    

        "external.Electricity_1" :{
                'I': prod('electrical energy - biomass share', 100, "%", source='material.cofiring.CoFiring_1')
                },
    }
}

result_config_dry_pellet_disc_rate=start_simulation(config_cofiring_dry_pellet_disc_rate)


list_disc=[result_config_composting_disc_rate, result_config_pre_NaOH_biogas_disc_rate, result_config_pre_biogas_disc_rate, result_config_gasification_disc_rate,
           result_config_pellet_gasification_disc_rate, result_config_cofiring_dry_disc_rate, result_config_dry_pellet_disc_rate]
list_disc

disc_rate_df=MACC(list_disc, BAU_scenario="composting", sensitivity=False, sensitivity_disc=True, obs="discount rate").marginal_abatement_cost()
disc_rate_df

circle_plot_NPV_emissions(disc_rate_df, fixed_df=True, legend_ncol=3, sensitivity=True, size_factor=1000)
# circle_plot_NPV_emissions(disc_rate_df, fixed_df=True, legend_ncol=3, size_factor=1000)



















# npv_variation_disc_rate=pd.DataFrame(index=list_disc_rate, columns=[])

# for i in range(0,len(list_disc_rate_biogas)):
#     npv_variation_disc_rate.loc[list_disc_rate_biogas[i], "pretreatment (NaOH) - biogas"]=list_npv_biogas_NaOH_disc_rate[i]
#     npv_variation_disc_rate.loc[list_disc_rate_biogas[i], "pretreatment - biogas"]=list_npv_biogas_disc_rate[i]

# for i in range(0,len(list_disc_rate_gasification)):
#     npv_variation_disc_rate.loc[list_disc_rate_gasification[i], "gasification"]=list_npv_gasification_disc_rate[i]
#     npv_variation_disc_rate.loc[list_disc_rate_gasification[i], "pelletization and gasification"]=list_npv_pellet_gasification_disc_rate[i]

# for i in range(0,len(list_disc_rate_cofiring)):
#     npv_variation_disc_rate.loc[list_disc_rate_cofiring[i], "cofiring-dry"]=list_npv_cofiring_dry_disc_rate[i]
#     npv_variation_disc_rate.loc[list_disc_rate_cofiring[i], "cofiring-wet"]=list_npv_cofiring_wet_disc_rate[i]

# npv_variation_disc_rate

# # GHG_variation_disc_rate["pretreatment (NaOH) - biogas"]
# # GHG_variation_disc_rate.index

# fig=plt.figure()
# ax1=fig.add_subplot(111)
# ax1.plot(npv_variation_disc_rate.index, npv_variation_disc_rate["pretreatment (NaOH) - biogas"], label="pretreatment (NaOH) - biogas", marker="*")
# ax1.plot(npv_variation_disc_rate.index, npv_variation_disc_rate["pretreatment - biogas"], label="pretreatment - biogas", marker="*")
# ax1.plot(npv_variation_disc_rate.index, npv_variation_disc_rate["gasification"], label="gasification", marker="*")
# ax1.plot(npv_variation_disc_rate.index, npv_variation_disc_rate["pelletization and gasification"], label="pelletization and gasification", marker="*")
# ax1.plot(npv_variation_disc_rate.index, npv_variation_disc_rate["cofiring-dry"], label="cofiring-dry", marker="*")
# ax1.plot(npv_variation_disc_rate.index, npv_variation_disc_rate["cofiring-wet"], label="cofiring-wet", marker="*")
# ax1.set_ylabel("NPV")
# fig.supxlabel("Discount rate")

# fig.legend(loc=9, ncol=4)
# plt.show()