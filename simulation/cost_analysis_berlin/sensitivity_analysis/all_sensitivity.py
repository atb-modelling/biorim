###sensitivity of several 
import start
from tools import prod
from analysis.timestep_chains import start_simulation
from tools import convert_molmasses
from analysis.economic.macc import MACC, circle_plot_MAC, single_graphic_MACC, circle_plot_NPV_emissions, circle_plot_NPV_emissions_panel
from simulation.data_management_and_plotting.npv_plotting import npv_graphic

import matplotlib.pyplot as plt
import pandas as pd




#electrical efficiency
circle_plot_NPV_emissions(df_sensitivity_elec_eff, fixed_df=True, legend_ncol=3, sensitivity=True, size_factor=1000, save_fig_memory=True)
circle_plot_NPV_emissions(df_sensitivity_elec_eff, fixed_df=True, legend_ncol=3, sensitivity=True, size_factor=1000)
circle_plot_NPV_emissions(df_sensitivity_elec_eff, fixed_df=True, legend_ncol=3, size_factor=1000)


#discount rate
circle_plot_NPV_emissions(disc_rate_df, fixed_df=True, legend_ncol=3, sensitivity=True, size_factor=1000, save_fig_memory=True)
circle_plot_NPV_emissions(disc_rate_df, fixed_df=True, legend_ncol=3, sensitivity=True, size_factor=1000)
circle_plot_NPV_emissions(disc_rate_df, fixed_df=True, legend_ncol=3, size_factor=1000)

#transportation distance



###
fig, ax = plt.subplots()

ax=circle_plot_NPV_emissions(df_sensitivity_elec_eff,  fixed_df=True, legend_ncol=3, sensitivity=True, size_factor=1000, save_fig_memory=True)
ax1=circle_plot_NPV_emissions(disc_rate_df,  fixed_df=True, legend_ncol=3, sensitivity=True, size_factor=1000, save_fig_memory=True)
ax1=ax.twinx()
plt.show()


#
# all_sens_df=pd.concat([df_sensitivity_moisture_level, df_sensitivity_elec_eff, disc_rate_df, df_sensitivity_transport])
# all_sens_df

# circle_plot_NPV_emissions_panel(all_sens_df, size_factor=100, fontsize=14, ncol=2, supxlabel_pos=[0.5, 0.1], supylabel_pos=[0.01, 0.5])


###
all_sens_df=pd.concat([df_sensitivity_dryness_level, df_sensitivity_transport, df_sensitivity_elec_eff,  disc_rate_df, df_sensitivity_elec_tariff])
# all_sens_df.to_csv(r"C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\all_sensitivity.csv", sep=";")



data_sens=all_sens_df.drop("composting")
data_sens=data_sens[data_sens["type"]=="sensitivity"]
list_obs=list(set(data_sens["obs"]))
list_obs


circle_plot_NPV_emissions_panel(all_sens_df, type_panel="3x2 column", size_factor=100, fontsize=12, ncol=3, supxlabel_pos=[0.5, 0.07], supylabel_pos=[0.01, 0.5])


















all_sens_df=pd.concat([df_sensitivity_elec_eff, disc_rate_df])
all_sens_df

list_obs_df=list(set(all_sens_df["obs"]))
list_obs_df

# for obs in list(set(all_sens_df["obs"])):
#     all_sens_df_obs=all_sens_df[all_sens_df["obs"]==obs]
#     print(all_sens_df_obs)

# for obs in list_obs_df:
#     panel_number=list_obs_df.index(obs)
#     data_sens_obs=all_sens_df[all_sens_df["obs"]==obs]
#     if panel_display[1]=="1x3":

# for obs in list_obs:
#     data_sens_obs=data_sens[data_sens["obs"]==obs]
#     if panel_display[0]==True:
#         if panel_display[1]=="1x3":
#             for i in range(0,3):
#                 ax=plt.subplot(1,3,i+1)
#                 ax.plot(data_sens_obs.loc[sim,"NPV"], data_sens_obs.loc[sim, "emissions - GWP100"], marker=sensitivity_markers[obs], c=MACC_colors[sim[:-4]])




# list_db=[df_sensitivity_elec_eff, disc_rate_df]
# all_sens_df=MACC(list_db, BAU_scenario="composting", sensitivity=False, sensitivity_disc=True).marginal_abatement_cost()


circle_plot_NPV_emissions(all_sens_df, size_factor=300, sensitivity=True, ncol=4)
circle_plot_NPV_emissions(all_sens_df, size_factor=300, sensitivity=True, ncol=4, panel_display=(True, "1x3"))