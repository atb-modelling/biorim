import pandas as pd
import numpy as np

from tools import  convert_molmasses, prod, determine_c_n
from material.gasification import Gasification
from material.biogas import BiogasPlant
from material.cofiring import CoFiring
from production.plant.street_trees import StreetTrees

KJ_TO_KWH=0.000277778

TreeLeavesBerlin=StreetTrees(O=prod("tree leaves", 34000, "ton"), origin="Berlin_mixture", id = None).output()
TreeLeavesBerlin

total_energy_leaves=TreeLeavesBerlin["Q"][0]*TreeLeavesBerlin["DM"][0]*1000*TreeLeavesBerlin["LHV dry"][0] *KJ_TO_KWH #kWh
total_energy_leaves

#energy from biogas
BiogasPlant(TreeLeavesBerlin, CHP_kW=2500).supply_information() # CHP_kW power for one year operation
energy_biogas=BiogasPlant(TreeLeavesBerlin, CHP_kW=2500).output().loc["electrical energy", "Q"] #kWh
energy_biogas

energy_biogas/total_energy_leaves*100 #% efficiency biogas

#energy from gasificatiom
Gasification(TreeLeavesBerlin, kW=850).supply_information() # kW power for one year operation
energy_gasification=Gasification(TreeLeavesBerlin, kW=850).output().loc["electrical energy", "Q"] #kWh
energy_gasification

energy_gasification/total_energy_leaves*100 #% efficiency gasification

#energy from co-firing
CoFiring(TreeLeavesBerlin, kW=425000).supply_information() # kW power for one year operation
energy_cofiring=CoFiring(TreeLeavesBerlin, kW=425000).output().loc["electrical energy - biomass share", "Q"] #kWh
energy_cofiring

energy_cofiring/total_energy_leaves*100 #% efficiency co-firing

