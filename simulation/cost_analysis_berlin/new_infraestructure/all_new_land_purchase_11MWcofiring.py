import start
from tools import prod
from analysis.timestep_chains import start_simulation
from tools import convert_molmasses
from analysis.economic.macc import MACC, circle_plot_MAC, single_graphic_MACC, circle_plot_NPV_emissions
from simulation.data_management_and_plotting.npv_plotting import npv_graphic

config_composting_BN_LP = {
    ### Main settings
    "simulation"   :{
        'name': "composting",
        'type': "all combinations", 
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

        ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture"
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.composting.CompostingLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "Andersen_2010",
                'CH4_emis': 0.02, 
                'land_purchase' : True, 
                'land_rent' : False,
                'fixed_operation_cost' : True,
                'new_infrastructure': True
                },

        "production.plant.fertilization.CompostSpreading_1" :{
                'I': prod('leaves compost', 100, "%", source='material.composting.CompostingLeaves_1'),
                'maturity' : "very mature",
                'method' : "VDLUFA", 
                },
    }
}

result_config_composting_BN_LP =start_simulation(config_composting_BN_LP)
# npv_graphic(result_config_composting_BN_LP, 0)
result_config_composting_BN_LP["composting"]["t1"][0]["cashflow"]
cashflow_composting=result_config_composting_BN_LP["composting"]["t1"][0]["cashflow"]
cashflow_composting[cashflow_composting["section"]=="capex"]

result_config_composting_BN_LP["composting"]["t1"][0]["total_cashflow"]
# result_config_composting_BN_LP["composting"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas()

#emis construction
emis_summary_composting=result_config_composting_BN_LP["composting"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas().sum(axis=1)
emis_summary_composting
emis_summary_composting_pos=emis_summary_composting[emis_summary_composting>0]
emis_summary_composting_pos.sum()

2.510028e+05/28752015.951289844*100


config_pre_NaOH_biogas_BN_LP = {
    ### Main settings
    "simulation"   :{
        'name': "biogas (with NaOH pretreatment)",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "production.plant.pretreatment_leaves.PretreatmentLeaves_1" :{
                'I': prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'method' : "NaOH",
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.pretreatment_leaves.PretreatmentLeaves_1'),
                'CHP_kW':2700,
                'land_purchase': True,
                'land_rent': False,
                'labour_costs':"FNR_2016", 
                'maintenance_costs':"FNR_2016",
                'new_infrastructure': True,
                'thermal_energy_tariff':("manual", 0.03)
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('biogas digestate', 100, "%", source='material.biogas.BiogasPlant_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}        

result_config_pre_NaOH_biogas_BN_LP=start_simulation(config_pre_NaOH_biogas_BN_LP)
#npv_graphic(result_config_pre_NaOH_biogas_BN_LP, 0)
result_config_pre_NaOH_biogas_BN_LP["biogas (with NaOH pretreatment)"]["t1"][0]["cashflow"]
result_config_pre_NaOH_biogas_BN_LP["biogas (with NaOH pretreatment)"]["t1"][0]["cashflow"]
cashflow_biogas=result_config_pre_NaOH_biogas_BN_LP["biogas (with NaOH pretreatment)"]["t1"][0]["cashflow"]
cashflow_biogas[cashflow_biogas["section"]=="capex"]

emis_summary_biogas=result_config_pre_NaOH_biogas_BN_LP["biogas (with NaOH pretreatment)"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas().sum(axis=1)
emis_summary_biogas
emis_summary_biogas_pos=emis_summary_biogas[emis_summary_biogas>0]
emis_summary_biogas_pos.sum()

1.319511e+05/26362097.73140318*100


config_pre_biogas_BN_LP = {
    ### Main settings
    "simulation"   :{
        'name': "biogas",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },
        

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "material.biogas.BiogasPlant_1" :{
                'I':prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'CHP_kW':2700,
                'land_purchase': True,
                'land_rent': False,
                'labour_costs':"FNR_2016", 
                'maintenance_costs':"FNR_2016",
                'new_infrastructure': True, 
                'thermal_energy_tariff':("manual", 0.03)
                },
        
        "transport.LandTransportMultiple_2" :{
                'I': prod('biogas digestate', 100, "%", source='material.biogas.BiogasPlant_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod("biogas digestate", 100, "%", source='transport.LandTransportMultiple_2')
                },

                
        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },

        "external.Heating_1" :{
                'I': prod('thermal energy', 100, "%", source='material.biogas.BiogasPlant_1')
                },
    },
}

result_config_pre_biogas_BN_LP=start_simulation(config_pre_biogas_BN_LP)
# npv_graphic(result_config_pre_biogas, 0)
result_config_pre_biogas_BN_LP["biogas"]["t1"][0]["cashflow"]
result_config_pre_biogas_BN_LP["biogas"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas()


config_gasification_BN_LP = {
    ### Main settings
    "simulation"   :{
        'name': "gasification",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
    
        "material.storing.LeafSilageStorage_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'method' : "silobag",
                },

        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves', 100, "%", source='material.storing.LeafSilageStorage_1'),
                'kW':4000,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': True,
                'land_rent': False,
                'new_infrastructure': True,
                'thermal_energy_tariff':("manual", 0.03)
                },

        "production.plant.fertilization.BiocharSpreading_1" :{
                'I': prod("unconverted biochar", 100, "%", source='material.gasification.Gasification_1')
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.gasification.Gasification_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                }, 

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.gasification.Gasification_1')
                },
    }
}

result_config_gasification_BN_LP=start_simulation(config_gasification_BN_LP)
#npv_graphic(result_config_gasification, 0)
result_config_gasification_BN_LP["gasification"]["t1"][0]["cashflow"]
emis_summary_gas=result_config_gasification_BN_LP["gasification"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas().sum(axis=1)

emis_summary_gas_pos=emis_summary_gas[emis_summary_gas>0]
emis_summary_gas_pos.sum()

5.599053e+05/22545275.09417999*100



config_pellet_gasification_BN_LP = {
    ### Main settings
    "simulation"   :{
        'name': "gasification (with pelletization)",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.pelletization.PelletFactory_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'land_purchase': True, 
                'land_rent': False,
                'new_infrastructure': True
                },

        "material.gasification.Gasification_1" :{
                'I':prod('tree leaves pellets', 100, "%", source='material.pelletization.PelletFactory_1'),
                'kW':4000,
                'operation_hours_year':8000,
                'thermal_recirculation': True,
                'land_purchase': True, 
                'land_rent': False,
                'new_infrastructure': True,
                'thermal_energy_tariff':("manual", 0.03)
                },

        "production.plant.fertilization.BiocharSpreading_1" :{
                'I': prod("unconverted biochar", 100, "%", source='material.gasification.Gasification_1')
                },

        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.gasification.Gasification_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },

        "external.Electricity_1" :{
                'I': prod('electrical energy', 100, "%", source='material.gasification.Gasification_1')
                },         
    }
}

result_config_pellet_gasification_BN_LP=start_simulation(config_pellet_gasification_BN_LP)
#npv_graphic(result_config_pellet_gasification, 0)
result_config_pellet_gasification_BN_LP["gasification (with pelletization)"]["t1"][0]["cashflow"]
result_config_pellet_gasification_BN_LP["gasification (with pelletization)"]["t1"][0]["cashflow"]
cashflow_gasification_pellet=result_config_pellet_gasification_BN_LP["gasification (with pelletization)"]["t1"][0]["cashflow"]
cashflow_gasification_pellet[cashflow_gasification_pellet["section"]=="capex"]

config_cofiring_dry_BN_LP = {
    ### Main settings
    "simulation"   :{
        'name': "cofiring",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.cofiring.BiomassDrying_1" :{
                'I': prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'new_infrastructure': True
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves dry', 100, "%", source='material.cofiring.BiomassDrying_1'),
                'kW':12000,
                'operation_hours':8000, 
                'retrofitting': True, 
                'land_purchase': True,
                'land_rent': False,
                'new_infrastructure': True,
                'include_thermal': True,
                'emissions_combustion':"carbon_content",
                "fixed_carbon_share":0.05
                },
        
        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.cofiring.CoFiring_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },    

        "external.Electricity_1" :{
                'I': prod('electrical energy - biomass share', 100, "%composting", source='material.cofiring.CoFiring_1')
                },
    }
}

result_config_cofiring_dry_BN_LP=start_simulation(config_cofiring_dry_BN_LP)
# npv_graphic(result_config_cofiring_dry_BN_LP, 0)
result_config_cofiring_dry_BN_LP["cofiring"]["t1"][0]["cashflow"]
result_config_cofiring_dry_BN_LP["cofiring"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas()
result_config_cofiring_dry_BN_LP["cofiring"]["t1"][0]["outputs"]

emis_summary_cofiring=result_config_cofiring_dry_BN_LP["cofiring"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas().sum(axis=1)
emis_summary_cofiring
emis_summary_cofiring_pos=emis_summary_cofiring[emis_summary_cofiring>0]
emis_summary_cofiring_pos.sum()

#% of construction to the total GHG emissions of the scenario
1.340260e+05/15452799.9633549*100


#pelletization and co-firing dry
config_pellet_cofiring_BN_LP = {
    ### Main settings
    "simulation"   :{
        'name': "cofiring (with pelletization)",
        'type': "all combinations", # NOTE: All possible parameter combinations are computed
        'starting_system' : "production.plant.street_trees.StreetTrees_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
        "cashflows":{
            "discount_rate": 0.07
        },
    },

    "systems"   :{
        "production.plant.street_trees.StreetTrees_1" :{
                'O' : prod("tree leaves", 36000, "ton"),
                'origin': "Berlin_mixture",
                },
        
        "production.plant.street_leaves_collection.StreetLeavesCollection_1" :{
                'I':prod('tree leaves', 100, "%", source='production.plant.street_trees.StreetTrees_1'), 
                'road_length_km' : 29531
                },

        "transport.LandTransportMultiple_1" :{
                'I': prod('tree leaves', 100, "%", source='production.plant.street_leaves_collection.StreetLeavesCollection_1'),
                'mass_allocation': [[1]],
                'dist_km': [[100]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },

        "material.pelletization.PelletFactory_1" :{
                'I':prod('tree leaves', 100, "%", source='transport.LandTransportMultiple_1'),
                'land_purchase': True, 
                'land_rent': False,
                'new_infrastructure': True
                },

        "material.cofiring.CoFiring_1" :{
                'I': prod('tree leaves pellets', 100, "%", source='material.pelletization.PelletFactory_1'),
                'kW':12000,
                'operation_hours':8000, 
                'retrofitting': True, 
                'land_purchase': True,
                'land_rent': False,
                'new_infrastructure': True,
                'include_thermal': True,
                'emissions_combustion':"carbon_content",
                "fixed_carbon_share":0.05
                },
        
        "transport.LandTransportMultiple_2" :{
                'I': prod('ashes', 100, "%", source='material.cofiring.CoFiring_1'),
                'mass_allocation': [[1]],
                'dist_km': [[50]],
                'type_transp': "lorry, large size",
                'method':"LBE",
                'load_basis':"mass",
                'transport_cost':"sommer_elso"
                },
        
        "production.plant.fertilization.AshSpreading_1" :{
                'I': prod("ashes", 100, "%", source='transport.LandTransportMultiple_2')
                },    

        "external.Electricity_1" :{
                'I': prod('electrical energy - biomass share', 100, "%", source='material.cofiring.CoFiring_1')
                },
    }
}

result_config_pellet_cofiring_BN_LP=start_simulation(config_pellet_cofiring_BN_LP)
# npv_graphic(result_config_cofiring_dry_BN_LP, 0)
result_config_pellet_cofiring_BN_LP["cofiring (with pelletization)"]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas()
result_config_pellet_cofiring_BN_LP["cofiring (with pelletization)"]["t1"][0]["cashflow"]
cashflow_cofiring_pellet=result_config_pellet_cofiring_BN_LP["cofiring (with pelletization)"]["t1"][0]["cashflow"]
cashflow_cofiring_pellet[cashflow_cofiring_pellet["section"]=="capex"]


list_scenarios_BN_LP=[result_config_composting_BN_LP, result_config_pre_NaOH_biogas_BN_LP, result_config_pre_biogas_BN_LP, result_config_gasification_BN_LP, 
                      result_config_pellet_gasification_BN_LP, result_config_cofiring_dry_BN_LP, result_config_pellet_cofiring_BN_LP]
#marginal abatement test

MACC(list_scenarios_BN_LP, BAU_scenario="composting").marginal_abatement_cost()
MAC_df_BN_LP=MACC(list_scenarios_BN_LP, BAU_scenario="composting", obs="BN-LP").marginal_abatement_cost()
MAC_df_BN_LP


#new graph
MAC_df_BN_LP.loc["composting"].T.loc["NPV"]
circle_plot_MAC(MAC_df_BN_LP, fixed_df=True, legend_ncol=3, size_factor=1000)

circle_plot_NPV_emissions(MAC_df_BN_LP, fixed_df=True, legend_ncol=3, size_factor=2000)
