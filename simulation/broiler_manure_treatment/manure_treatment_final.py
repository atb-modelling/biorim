## Scenarios for the manure treatment examples and start the evaluation

## to initiate the model:
import sys
sys.path.append("C:/Users/ukreidenweis/Documents/themodel")
import analysis, external, material, production, read, simulation, tools, transport

# from start import *
from tools import prod
from analysis.downstream_chains import start_simulation

##########################
#### Scenario configs ####

### Storage scenario config ###

storageconfig = {
    ### Main settings
    "simulation"   :{
        "name": "storage",
        "type": "all combinations",
        "reference_system" : "production.animal.poultry.BroilerFarm_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
    },

    "systems"   :{
        "production.animal.poultry.BroilerFarm_1" :{
                'O' : prod('broiler manure', 1, "ton"),
                'B0_source' : ["KTBL", "IPCC"],
                },

        # "transport.LandTransport_1" :{
        #         'I': prod(('production.animal.poultry.BroilerFarm_1', 'broiler manure')),
        #         'dist_km': 5,
        #         'type': "tractor, small size"
        #         },

        "material.storing.ManureStorage_1" :{
                # 'I': prod(('transport.LandTransport_1', 'broiler manure')),
                'I': prod(('production.animal.poultry.BroilerFarm_1', 'broiler manure')),
                'method' : ["default", "IPCC2019", "Pardo2015", "Moore2011"],
                'time_period' : 168 # [35,168]
                },

        "transport.LandTransport_2" :{
                'I': prod(('material.storing.ManureStorage_1', 'broiler manure')),
                'dist_km': [10, 500],
                'type': "lorry, medium size"
                },

        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod(('transport.LandTransport_2', 'broiler manure')),
                'method': ["default", "EEA2016", "IPCC2019"],
                'type' : "solid",
                },
    }
}

### Composting scenario config ###

compostconfig = {
    ### Main settings
    "simulation"   :{
        "name": "composting",
        "type": "all combinations",
        "reference_system" : "production.animal.poultry.BroilerFarm_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
    },

    "systems"   :{
        "production.animal.poultry.BroilerFarm_1" :{
                'O' : prod('broiler manure', 1, "ton"),
                'B0_source' : ["KTBL", "IPCC"]
                },

        # "transport.LandTransport_1" :{
        #         'I': prod(('production.animal.poultry.BroilerFarm_1', 'broiler manure')),
        #         'dist_km': 5,
        #         'type': "tractor, small size"
        #         },

        "material.composting.CompostPlant_1" :{
                # 'I': prod(('transport.LandTransport_1', 'broiler manure')),
                'I': prod(('production.animal.poultry.BroilerFarm_1', 'broiler manure')),
                'type': "windrow",
                'H2O_content': 0.55,
                'method': ["default", "Pardo2015", "Chen2018"],
                'time_period': [35, 168]
                },

        "transport.LandTransport_2" :{
                'I': prod(('material.composting.CompostPlant_1', 'broiler manure composted')),
                'dist_km': [10, 500],
                'type': "lorry, medium size"
                },

        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod(('transport.LandTransport_2', 'broiler manure composted')),
                'method': ["default", "EEA2016", "IPCC2019"],
                'type' : "solid",
                },
    }
}


### Biogas scenario config ###

biogasconfig = {
    ### Main settings
    "simulation"   :{
        "name": "biogas",
        "type": "all combinations",
        "reference_system" : "production.animal.poultry.BroilerFarm_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
    },

    "systems"   :{
        "production.animal.poultry.BroilerFarm_1" :{
                'O' : prod('broiler manure', 1, "ton"),
                'B0_source' : ["KTBL", "IPCC"]
                },

        "transport.LandTransport_1" :{
                'I': prod(('production.animal.poultry.BroilerFarm_1', 'broiler manure')),
                'dist_km': 5,
                'type': "tractor, small size"
                },

        "material.biogas.BiogasPlant_1" :{
                'I': prod(('transport.LandTransport_1', 'broiler manure')),
                'residue_storage_type': ["closed", "open"],
                'thermal_energy_use': [0.4, 0],
                'CH4_loss_conv_method': ["KTBL", "Liebetrau_2010.low", "Liebetrau_2010.high"]
                # 'CH4_loss_conv_method': "KTBL"
                },

        "transport.LandTransport_2" :{
                'I': prod(('material.biogas.BiogasPlant_1', 'biogas digestate')),
                'dist_km': [10, 500],
                'type': "lorry, medium size"
                },

        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod(('transport.LandTransport_2', 'biogas digestate')),
                'method': ["default", "EEA2016", "IPCC2019"],
                # 'method': "default", 
                'type' : "liquid",
                },

        "external.Electricity_1" :{
                'I': prod(('material.biogas.BiogasPlant_1', 'electrical energy'))
                },

        "external.Heating_1" :{
                'I': prod(('material.biogas.BiogasPlant_1', 'thermal energy'))
                },
    }
}

### Biochar scenario config ###

biocharconfig = {
    ### Main settings
    "simulation"   :{
        "name": "biochar",
        "type": "all combinations",
        "reference_system" : "production.animal.poultry.BroilerFarm_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
    },

    "systems"   :{
        "production.animal.poultry.BroilerFarm_1" :{
                'O' : prod('broiler manure', 1, "ton"),
                'B0_source' : "KTBL" # doesn't make a difference for biochar
                },

        # "transport.LandTransport_1" :{
        #         'I': prod(('production.animal.poultry.BroilerFarm_1', 'broiler manure')),
        #         'dist_km': 5,
        #         'type': "tractor, small size"
        #         },

        "material.biochar.PyrolysisPlant_1":{
                # 'I': prod(('transport.LandTransport_1', 'broiler manure')),
                'I': prod(('production.animal.poultry.BroilerFarm_1', 'broiler manure')),
                'heating_temp': [400, 500, 600],
                'CH4_content_exhaust': [0.0001, 0.001]
                },

        "transport.LandTransport_2" :{
                'I': prod(('material.biochar.PyrolysisPlant_1', 'broiler manure pyrochar')),
                'dist_km': [10, 500],
                'type': "lorry, medium size"
                },

        "production.plant.fertilization.BiocharSpreading_1" :{
                'I': prod(('transport.LandTransport_2', 'broiler manure pyrochar')),
                'method': "default"
                },
    }
}



## In this case the reference class is the first in the row, therefore only downstream modelling is required (no inputs for the BroilerFarm defined).
# How to figure this out that there is not inputs required.

##########################################
# the storage example with different values
from analysis.downstream_chains import start_simulation


## WARNING: Some biogas setting deactivated for testing

result = start_simulation([storageconfig, compostconfig, biogasconfig, biocharconfig])

# result.to_json("2020-05-15\\result.json")
# result.to_pickle("2020-05-15\\result.pkl")




## again try stepwise:


storage_res = start_simulation(storageconfig)

composting_res = start_simulation(compostconfig) # currently 96 runs

biogas_res = start_simulation(biogasconfig) # currently 72 runs

biochar_res = start_simulation(biocharconfig) # currently only 12 runs


biochar_res["biochar"][0]["emissions"]

biochar_res["biochar"][0]["emissions"]

biochar_res["biochar"]["gwp"][0].loc[:,:,"GWP100"].to_pandas()



## read back resuts
from analysis import read_pickle

result = read_pickle("2020-05-15\\result.pkl")

## number of simulations
result.keys()
result["storage"].keys() # 48
result["composting"].keys() # 72
result["biogas"].keys() # 144
result["biochar"].keys() # 12
#

result["biochar"]["parameters"]

result["composting"][0]["emissions"].sum(axis=0) # 195 kg CO2-C von 208 kg die in die Funktion eingehen

result["composting"]["parameters"].loc[("transport.LandTransport_2","dist_km"),[0,1,2,4,]]
result["composting"]["parameters"].loc[:,[0,3]]


result["composting"]["parameters"].loc[:,[1,4]]

result["composting"]["parameters"].loc[:,[0,3]]

result["composting"][0]["emissions"].sum(axis=0)/12*44
result["composting"][3]["emissions"].sum(axis=0)/12*44



result["composting"]["gwp"][0].loc[:,:,"GWP100"].to_pandas().sum(axis=0).loc["CO2"] - \
result["composting"]["gwp"][3].loc[:,:,"GWP100"].to_pandas().sum(axis=0).loc["CO2"]

result["composting"]["gwp"][1].loc[:,:,"GWP100"].to_pandas().sum(axis=0).loc["CO2"] - \
result["composting"]["gwp"][4].loc[:,:,"GWP100"].to_pandas().sum(axis=0).loc["CO2"]


result["composting"][0]["outputs"]


result["biogas"]["gwp"][0].loc[:,:,"GWP100"].to_pandas().sum(axis=0)
result["biogas"]["gwp"][3].loc[:,:,"GWP100"].to_pandas().sum(axis=0)


## amount of N in biogas and stored manure

result["biogas"]

digestate = result["biogas"][0]["outputs"].loc[("material.biogas.BiogasPlant_1", "biogas digestate"),:]
digestate["Q"] * digestate["N"] # 29.9 kg N in digestate ## there are no N losses at all. Correct?
digestate

storedmanure = result["storage"][0]["outputs"].loc[("material.storing.ManureStorage_1", "broiler manure"), :]

storedmanure["Q"] * storedmanure["N"]

result["composting"]["emissions"][0].to_pandas()
result["composting"][0]["emissions"]

result["biochar"]["emissions"][0].to_pandas()

result["composting"]["emissions"][0].to_pandas().loc[:,["N2O", "N2O indirect"]].sum().sum()

## biochar mass:

result["biochar"][0]["outputs"]


### emissions from biochar under different temperatures


result["biochar"]["parameters"][[2,6,10]]


result["biochar"]["emissions"][2].to_pandas()["CO2"] # 400°C
result["biochar"]["emissions"][6].to_pandas()["CO2"] # 500°C
result["biochar"]["emissions"][10].to_pandas()["CO2"] # 600°C

CO2_400 = result["biochar"]["emissions"][2].to_pandas().loc[("material.biochar.PyrolysisPlant_1","broiler manure pyrolysis"),"CO2"]
CO2_600 = result["biochar"]["emissions"][10].to_pandas().loc[("material.biochar.PyrolysisPlant_1","broiler manure pyrolysis"),"CO2"]

round(CO2_600 - CO2_400) # difference in emissions from the pyrolysis

CO2_400 = result["biochar"]["emissions"][2].to_pandas().loc[("production.plant.fertilization.BiocharSpreading_1","biochar field decomposition"),"CO2"]
CO2_600 = result["biochar"]["emissions"][10].to_pandas().loc[("production.plant.fertilization.BiocharSpreading_1","biochar field decomposition"),"CO2"]

round(CO2_600 - CO2_400) # difference in emissions from the pyrolysis

## C losses from biogas and composting

result["composting"][0]["emissions"].loc[:,["CO2-C", "CH4-C"]] # 164 kg from composting and field spreading
result["biogas"][0]["emissions"].loc[:,["CO2-C", "CH4-C"]] # 185 kg from biogas and spreading






## is there indication that a neglecting of CO2 emissions could lead to other results?




biogas_0 = result["biogas"]["emissions"][0].to_pandas()
biogas_0[(biogas_0[["CH4"]] > 0).all(1)].sum()


biogas_0.groupby(['system']).agg(lambda x: x[x>0].mean())

df.groupby(df['A'])['C'].agg([('negative' , lambda x : x[x < 0].sum()) , ('positive' , lambda x : x[x > 0].sum())])
df.groupby(biogas_0['CH4']).agg([('negative' , lambda x : x[x < 0].sum()) , ('positive' , lambda x : x[x > 0].sum())])

result["biogas"]["gwp"][0].loc[:,:,"GWP100"].to_pandas()
result["biochar"]["gwp"][0].loc[:,:,"GWP100"].to_pandas()




## somehow the ignition oil production is missing!

result["biogas"]["gwp"].loc[0,:,"GWP100"].to_pandas()



biogas_res["biogas"]["gwp"][0].loc[:,:,"GWP100"].to_pandas()
composting_res["composting"]["gwp"][0].loc[:,:,"GWP100"].to_pandas()
composting_res["composting"]["gwp"][34].loc[:,:,"GWP100"].to_pandas()

composting_res["composting"]["gwp"][24].loc[:,:,"GWP100"].to_pandas() # Pardo
composting_res["composting"]["gwp"][30].loc[:,:,"GWP100"].to_pandas() # Chen



biogas_res["biogas"]["parameters"]
composting_res["composting"]["parameters"].loc[("material.composting.CompostPlant_1", "method"),:]

biogas_res["biogas"]["gwp"][6].loc[:,:,"GWP100"].to_pandas()

biogas_res["biogas"]["parameters"][6]


## why are there currently no N2O emissions form biochar?
result["biochar"][0]["emissions"]


result["biochar"][0]["gwp"]



## do NO2 emissions from biochar actually matter?
result["biochar"]["gwp"][0].loc[:,:,"GWP100"].to_pandas()
# yes, at the moment it is as important as CO2




result["biogas"]["parameters"][5]



result_biogas = start_simulation(biogasconfig)
result_biogas["biogas"]["parameters"]

result_biogas["biogas"]["emissions"][0].to_pandas()
result["biogas"]["gwp"][0].loc[:,:,"GWP100"].to_pandas()


result_biogas.to_json("2020-02-13\\biogas_test.json")
result.to_pickle("2020-02-13\\biogas_test.pkl")



result_composting = start_simulation(compostconfig)

result_composting["composting"].keys()
result_composting["composting"][0]["outputs"]
result_composting["composting"]["emissions"][0].to_pandas()


result_biochar = start_simulation(biocharconfig)
result_biochar["biochar"]["emissions"][0].to_pandas()
