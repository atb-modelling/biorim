#########################################################################
# Ploting of results of the configs described in model_description_paper8
#########################################################################


import start

import re
from plotnine import *
import pandas as pd
import matplotlib.pyplot as plt

from tools import prod
from analysis import read_pickle

import plotly.graph_objects as go
from plotly.subplots import make_subplots
import plotly.io as pio


################# Load previously saved results ################

# with new carbon accounting in ArableLand and sensitivity analysis
# result = read_pickle("model_description_paper\\2024-01-06_19-11-40\simdata.pkl")

# result = read_pickle("model_description_paper\\2024-01-09_18-28-42\simdata.pkl")

# with sensitivity analysis of emissions:
result = read_pickle("model_description_paper\\2024-01-11_16-04-05\simdata.pkl")


result.info["scenarios"]



################################################################
########################## Plots ###############################


########################## Plot 4 ##############################
# Sankey diagram of carbon and nitrogen flows for the storage scenario


def sankey_diagram_flows(result, scenario="biogas", t="t2", run=0, element="carbon", summarize_N_gases="NOx_only"):
    Outputs = result[scenario][t][run]["outputs"]
    Out_kg = pd.DataFrame()
        
    if element == "carbon":
        # Carbon outputs
        Out_kg["Q"] = Outputs["Q"] * Outputs["DM"] * Outputs["oDM"]* Outputs["orgC"] * 1000

        # Emissions
        Emis_kg = result[scenario][t][run]["emissions"].loc[:,["CO2-C", "CH4-C"]]

    elif element == "nitrogen":
        # Nitrogen outputs
        Out_kg["Q"] = Outputs["Q"] * Outputs["N"]

        # Emissions
        # Not all timesteps contain the same gases
        Ngas = [gas for gas in result[scenario][t][run]["emissions"].columns if gas in ["NH3-N", "N2O-N", "NH3-N & NOx-N", "NO-N", "NOx-N", "N2-N"]]
        Emis_kg = result[scenario][t][run]["emissions"].loc[:,Ngas]

        if summarize_N_gases == "NH3-N & NOx-N":
            # Add NH3, NO and NOx together
            NH3N_NOxN_gases = [gas for gas in result[scenario][t][run]["emissions"].columns if gas in ["NH3-N", "NO-N", "NOx-N", "NH3-N & NOx-N"]]  # not N2O gases
            # save the sum as "NH3-N & NOx-N emissions" and delete the individual
            Emis_kg["NH3 & NOx emissions"] = Emis_kg.loc[:,NH3N_NOxN_gases].sum(axis=1)
            Emis_kg.drop(columns=NH3N_NOxN_gases, inplace=True)
        
        elif summarize_N_gases == "NOx_only":
            NOxN_gases = [gas for gas in result[scenario][t][run]["emissions"].columns if gas in ["NO-N", "NOx-N"]]  # not N2O gases
            Emis_kg["NOx emissions"] = Emis_kg.loc[:,NOxN_gases].sum(axis=1)


        Emis_kg.rename(columns={'NH3-N & NOx-N': "NH3 & NOx emissions",
                                'NH3-N': "NH3 emissions",
                                'N2O-N': "N2O emissions",
                                'NO-N': "NO emissions",
                                'NOx-N': "NOx emissions",
                                'NO-N': "NO emissions",
                                'N2-N': "N2 emissions"}, inplace=True)


    Out_kg = Out_kg.reset_index()

    # NOTE: Some changes required here. This summing up changes everything. Also, non-fossil emissions need to be removed here
    # Drop non-fossil emissions and those that should not be depicted because they are side-products
    # Emis_kg = Emis_kg.drop(["external.Electricity_1000", "external.ExternInput_1000", "external.ExternInput_1001", "external.Electricity_1", "external.Heating_1"])
    if scenario == "biogas":
        Emis_kg = Emis_kg.drop([("material.biogas.BiogasPlant_1", "construction"), ("material.biogas.BiogasPlant_1", "wheel loader"), ("material.biogas.BiogasPlant_1", "CHP ignition oil combustion")])


    # TODO: First divide between positive and negative emissions
    Emis_neg_kg = Emis_kg[Emis_kg < 0]
    Emis_neg_kg = Emis_neg_kg.groupby("source").sum()
    Emis_neg_kg = Emis_neg_kg.reset_index()
    Emis_neg_kg = pd.melt(Emis_neg_kg, id_vars=["source"], var_name="sink", value_name="Q")
    Emis_neg_kg = Emis_neg_kg[~(Emis_neg_kg["Q"]==0)]
    Emis_neg_kg.columns = ["sink", "source", "Q"]   # for negative emissions source and sink have to be switched
    Emis_neg_kg["Q"] *= -1

    Emis_pos_kg = Emis_kg[Emis_kg > 0] # Zero emissions can be neglected
    Emis_pos_kg = Emis_pos_kg.groupby("source").sum()
    Emis_pos_kg = Emis_pos_kg.reset_index()
    Emis_pos_kg = pd.melt(Emis_pos_kg, id_vars=["source"], var_name="sink", value_name="Q")
    Emis_pos_kg = Emis_pos_kg[~(Emis_pos_kg["Q"]==0)]

    # Name negative emissions: uptake (probably not relevant for nitrogen)
    Emis_neg_kg["source"] = Emis_neg_kg["source"].replace({"CO2-C": 'CO2 uptake'})

    Emis_pos_kg["sink"] = Emis_pos_kg["sink"].replace({"CO2-C": 'CO2 emissions', "CH4-C": 'CH4 emissions'})
    Emis_pos_kg["sink"] = Emis_pos_kg["sink"].replace({"CO2-C": 'CO2 emissions', "CH4-C": 'CH4 emissions'})

    # Combine positive and negative emissions again
    Emis_kg = pd.concat([Emis_pos_kg, Emis_neg_kg])

    # NOTE: I need to differentiate between carbon fixation and release of biogenic carbon on the one hand
    # and fossil carbon emissions on the other
    # For the momemt neglect this, and just consider also too high emissions
    # But it also means for instance negative emissios from electricity

    Emis_kg["item"] = Emis_kg["sink"]

    # Combine outputs and emissions
    Flows_kg = pd.concat([Emis_kg, Out_kg])

    # Drop values with NA. NOTE: There are still some things that should be contained, but where the respective data is missing
    Flows_kg = Flows_kg.dropna(axis=0)

    ### Manual modifications: Renaming etc.

    # Remove some systems

    if element == "carbon":
        Flows_kg = Flows_kg[Flows_kg["source"] != "production.plant.fertilization.MineralFertilizerProduction_1000"]
        Flows_kg = Flows_kg[Flows_kg["source"] != "production.plant.fertilization.MineralFertilizerProduction_1001"]
    Flows_kg = Flows_kg[Flows_kg["source"] != "external.Electricity_1000"]
    Flows_kg = Flows_kg[Flows_kg["source"] != "external.ExternInput_1000"]
    Flows_kg = Flows_kg[Flows_kg["source"] != "external.ExternInput_1001"]

    Flows_kg = Flows_kg[Flows_kg["sink"] != "external.Electricity_1"]
    Flows_kg = Flows_kg[Flows_kg["sink"] != "external.Heating_1"]

    # Short names
    Flows_kg["source_short"] = [sys.split(".")[-1] for sys in Flows_kg["source"]]
    Flows_kg["sink_short"] = [sys.split(".")[-1] for sys in Flows_kg["sink"]]

    # Show some outputs as nodes
    Flows_kg.loc[Flows_kg["sink_short"] == "ExternOutput_1", "sink_short"] = "Milk"
    Flows_kg.loc[Flows_kg["sink_short"] == "ExternOutput_2", "sink_short"] = "Cows to Slaughter"
    Flows_kg.loc[Flows_kg["sink_short"] == "ExternOutput_3", "sink_short"] = "Calves to Rearing"

    # Short name of Mineral Fertilizer Production
    Flows_kg.loc[Flows_kg["source_short"] == "MineralFertilizerProduction_1000", "source_short"] = "FertilizerProduction_1000"
    Flows_kg.loc[Flows_kg["source_short"] == "MineralFertilizerProduction_1001", "source_short"] = "FertilizerProduction_1001"


    # Replace underscore (_) with space
    Flows_kg[["source_short", "sink_short"]] = Flows_kg[["source_short", "sink_short"]].replace(to_replace=r'_', value=' ', regex=True)


    ## Assign numbers to source and sinks:
    systems = list(set(Flows_kg["source_short"]).union(set(Flows_kg["sink_short"])))
    systems.sort()
    systems

    sys_nbr = {}
    for i, sys in enumerate(systems):
        sys_nbr[sys] = i

    Flows_kg["source_nbr"] = Flows_kg["source_short"].copy()
    Flows_kg["sink_nbr"] = Flows_kg["sink_short"].copy()

    Flows_kg = Flows_kg.replace({"source_nbr":sys_nbr})
    Flows_kg = Flows_kg.replace({"sink_nbr":sys_nbr})

    # Set figure parameters
    label = list(sys_nbr.keys())
    source = list(Flows_kg["source_nbr"])
    target = list(Flows_kg["sink_nbr"])
    value = list(Flows_kg["Q"])

    ## NOTE: Only for development
    # set(pd.concat([Flows_kg["sink_short"], Flows_kg["source_short"]]))
    # # There is still some CH4-C
    # Flows_kg["sink_short"]
    # Flows_kg["source_short"]
    # Flows_kg

    if element == "carbon":
        x_pos = {'CO2 uptake': 0.05,
                 'Meadow 1': 0.2,
                 'ArableLand 1': 0.2,
                 'ArableLand 1000': 0.2,
                 'HeiferRearing 1000': 0.2,
                 'DairyFarm 1': 0.4,
                 'Milk': 0.6, # The milk (goint to ExternOutput_1)
                 'Cows to Slaughter': 0.6,
                 'Calves to Rearing': 0.6,
                 'BiogasPlant 1': 0.6,
                 'DigestateStorage 1': 0.8,
                 'ManureStorage 1': 0.7,
                 'CH4 emissions': 0.9,
                 'CO2 emissions': 0.9
                 }

        y_pos = {'CO2 uptake': 0.4,
                 'Meadow 1': 0.15,
                 'ArableLand 1000': 0.25,
                 'ArableLand 1': 0.55,
                 'HeiferRearing 1000': 0.8,
                #  'HeiferRearing 1000': 1,
                 'DairyFarm 1': 0.4,
                 'Milk': 0.5,
                 'Cows to Slaughter': 0.6,
                 'Calves to Rearing': 0.65,
                 'BiogasPlant 1': 0.8,
                 'DigestateStorage 1': 0.8,
                 'ManureStorage 1': 0.8,
                 'CH4 emissions': 0.1,
                 'CO2 emissions': 0.35
                 }
        
        color = {'CO2 uptake': "blue",
                 'Meadow 1': "green",
                 'ArableLand 1': "green",
                 'ArableLand 1000': "green",
                 'HeiferRearing 1000': "green",
                 'DairyFarm 1': "green",
                 'Milk': "grey",
                 'Cows to Slaughter': "grey",
                 'Calves to Rearing': "grey",
                 'BiogasPlant 1': "green",
                 'DigestateStorage 1': "green",
                 'ManureStorage 1': "green",
                 'CH4 emissions': "blue",
                 'CO2 emissions': "blue"
                 }
        
    elif element == "nitrogen":
        x_pos = {'FertilizerProduction 1001': 0.05,
                 'FertilizerProduction 1000': 0.05,
                 'Meadow 1': 0.2,
                 'ArableLand 1': 0.2,
                 'ArableLand 1000': 0.2,
                 'HeiferRearing 1000': 0.2,
                 'DairyFarm 1': 0.4,
                 'Milk': 0.6, # The milk (goint to ExternOutput_1)
                 'Cows to Slaughter': 0.6,
                 'Calves to Rearing': 0.6,
                 'BiogasPlant 1': 0.6,
                 'ManureStorage 1': 0.7,
                 'DigestateStorage 1': 0.8,
                 'N2O emissions': 0.9,
                 'NH3 & NOx emissions': 0.9,
                 'N2 emissions': 0.9
                 }
        
        y_pos = {'FertilizerProduction 1001': 0.28,
                 'FertilizerProduction 1000': 0.45,
                 'Meadow 1': 0.15,
                 'ArableLand 1000': 0.35,
                 'ArableLand 1': 0.65,
                 'HeiferRearing 1000': 0.8,
                 'DairyFarm 1': 0.4,
                 'Milk': 0.41, # 0.38,
                 'Cows to Slaughter': 0.45,
                 'Calves to Rearing': 0.5,
                 'BiogasPlant 1': 0.8,
                 'ManureStorage 1': 0.8,
                 'DigestateStorage 1': 0.8,
                 'NH3 & NOx emissions': 0.1,
                 'N2O emissions': 0.2,
                 'N2 emissions': 0.3
                 }
        
        color = {'FertilizerProduction 1001': "grey",
                 'FertilizerProduction 1000': "grey",
                 'Meadow 1': "brown",
                 'ArableLand 1000': "brown",
                 'ArableLand 1': "brown",
                 'HeiferRearing 1000': "brown",
                 'DairyFarm 1': "brown",
                 'Milk': "grey",
                 'Cows to Slaughter': "grey",
                 'Calves to Rearing': "grey",
                 'BiogasPlant 1': "brown",
                 'ManureStorage 1': "brown",
                 'DigestateStorage 1': "brown",
                 'NH3 & NOx emissions': "blue",
                 'NH3 emissions': "blue",
                 'N2O emissions': "blue",
                 'NO emissions': "blue",
                 'NOx emissions': "blue",
                 'N2 emissions': "blue"
                 }

    fig = go.Figure(go.Sankey(
        arrangement = "snap",
        # arrangement = "fixed",
        node = {
            "label": label,
            "x": list(map(x_pos.get, label)),
            "y": list(map(y_pos.get, label)),
            "color": list(map(color.get, label)),
            },
        link = {
            "source": source,
            "target": target,
            "value": value}))
    
    if element == "carbon":
        letter = "a) "
    elif element == "nitrogen":
        letter = "b) "
    title = letter + element.capitalize() + " flows in the " + scenario + " scenario"

    # NOTE: Try to scale manually, because there seems to be no other option to achieve same node sizes
    if element == "carbon":
        if scenario == "biogas":
            height_px = 600
        elif scenario == "storage":
            height_px = 545

    if element == "nitrogen":
        if scenario == "biogas":
            height_px = 600
        elif scenario == "storage":
            height_px = 545

    fig.update_layout(
        autosize=False,
        width=1200,
        # height=600,
        height=height_px,
        title_text=title,
        title_font_family="Segoe UI Semibold",
        title_font_color="black",
        font_family="Segoe UI",
        font_color="black",
        font_size=22,
        margin=go.layout.Margin(
                l=0, #left margin
                r=0, #right margin
                b=80, #bottom margin
                t=50  #top margin
                )
        )

    return fig

# The different sankey plots
## Only include plots for storage in the manuscript

# Change the scale for saving
pio.kaleido.scope.default_scale = 2

## Storage
CflowStorage = sankey_diagram_flows(result=result, scenario="storage", t="t2", run=0, element="carbon")
CflowStorage.show()
CflowStorage.write_image("C:/Users/ukreidenweis/Nextcloud/Paper - in Planung/2 - BIORIM Model description paper/Figures/Fig4a_CflowStorageSankey.svg")
CflowStorage.write_image("C:/Users/ukreidenweis/Nextcloud/Paper - in Planung/2 - BIORIM Model description paper/Figures/Fig4a_CflowStorageSankey.pdf")
CflowStorage.write_image("C:/Users/ukreidenweis/Nextcloud/Paper - in Planung/2 - BIORIM Model description paper/Figures/Fig4a_CflowStorageSankey.png")

NflowStorage = sankey_diagram_flows(result=result, scenario="storage", t="t2", run=0, element="nitrogen", summarize_N_gases="NH3-N & NOx-N")
NflowStorage.show()
NflowStorage.write_image("C:/Users/ukreidenweis/Nextcloud/Paper - in Planung/2 - BIORIM Model description paper/Figures/Fig4b_NflowStorageSankey.svg")
NflowStorage.write_image("C:/Users/ukreidenweis/Nextcloud/Paper - in Planung/2 - BIORIM Model description paper/Figures/Fig4b_NflowStorageSankey.pdf")
NflowStorage.write_image("C:/Users/ukreidenweis/Nextcloud/Paper - in Planung/2 - BIORIM Model description paper/Figures/Fig4b_NflowStorageSankey.png")


## Biogas: Not included in main text of the paper
CflowBiogas = sankey_diagram_flows(result=result, scenario="biogas", t="t2", run=0, element="carbon")
CflowBiogas.show()
CflowBiogas.write_image("C:/Users/ukreidenweis/Nextcloud/Paper - in Planung/2 - BIORIM Model description paper/Figures/CflowBiogasSankey.svg")
CflowBiogas.write_image("C:/Users/ukreidenweis/Nextcloud/Paper - in Planung/2 - BIORIM Model description paper/Figures/CflowBiogasSankey.pdf")
CflowBiogas.write_image("C:/Users/ukreidenweis/Nextcloud/Paper - in Planung/2 - BIORIM Model description paper/Figures/CflowBiogasSankey.png")


NflowBiogas = sankey_diagram_flows(result=result, scenario="biogas", t="t2", run=0, element="nitrogen", summarize_N_gases="NH3-N & NOx-N")
NflowBiogas.show()
NflowBiogas.write_image("C:/Users/ukreidenweis/Nextcloud/Paper - in Planung/2 - BIORIM Model description paper/Figures/NflowBiogasSankey.svg")
NflowBiogas.write_image("C:/Users/ukreidenweis/Nextcloud/Paper - in Planung/2 - BIORIM Model description paper/Figures/NflowBiogasSankey.pdf")
NflowBiogas.write_image("C:/Users/ukreidenweis/Nextcloud/Paper - in Planung/2 - BIORIM Model description paper/Figures/NflowBiogasSankey.png")


########################## Plot 5 ##############################
# Aggregated greenhouse gas emissions for the different scenarios and timesteps

import pandas as pd
import matplotlib.pyplot as plt

scenarios = result.info["scenarios"]
timesteps = ["t1", "t2"]    # Three timesteps are calculated but only two are shown here, since t3 has the same result as t2

# del EmisScenarios
# EmisSecenarios = pd.DataFrame()

i = 0

for scen in scenarios:
    for t in timesteps:
        emis = result[scen]["gwp"].sel(run=0, gwp="GWP100", timestep=t).sum(axis=1).to_pandas()

        name = f"{scen}_{t}"
        emis = emis.rename(name)

        emis = emis / 1000 # convert to ton CO2eq
                    
        emis = emis.to_frame()

        if i == 0:
            EmisScenarios = emis
        else:
            EmisScenarios = EmisScenarios.join(emis, how="outer")
        
        i += 1

# Problem: Index is already combined. Why??

# rename
EmisScenarios.index = [i[0].split(".")[-1].split("_")[-2] + " | " + i[1] for i in EmisScenarios.index]
EmisScenarios

## alternative renaming:

itm_name = {
'Electricity | electrical energy to grid': "electricity and heat usage or production",
'Electricity | electrical energy from grid': "electricity and heat usage or production",
'ExternInput | market for rape oil, crude': "biogas plant lubricant and fuel",
'ExternInput | market for lubricating oil': "biogas plant lubricant and fuel",
'Heating | leakage production + transmission': "electricity and heat usage or production",
'Heating | natural gas burning': "electricity and heat usage or production",
'BiogasPlant | CHP CO2 from biogas': "biogas exhaust emissions",
'BiogasPlant | CHP ignition oil combustion': "biogas exhaust emissions",
'BiogasPlant | CHP methane combustion': "biogas exhaust emissions",
'BiogasPlant | CHP methane leakage': "biogas exhaust emissions",
'BiogasPlant | construction': "infrastructure",
'BiogasPlant | digester': "storage of manure / digestate",
'BiogasPlant | poststorage': "storage of manure / digestate",
'BiogasPlant | wheel loader': "machinery use",
'ManureStorage | concrete pit construction': "infrastructure",
'ManureStorage | cow manure, liquid': "storage of manure / digestate",
'DairyFarm | enteric fermentation': "enteric fermentation and barn emissions",
'DairyFarm | housing emissions': "enteric fermentation and barn emissions",
'ArableLand | decomposition biogas digestate': "fertilization with manure / digestate",
'ArableLand | carbon fixation': "carbon fixation in fodder biomass",
'ArableLand | decomposition cow manure, liquid': "fertilization with manure / digestate",
'ArableLand | field emissions mineral N fertilization': "mineral fertilizer production and use",
'ArableLand | liquid manure spreading machine': "machinery use",
'MineralFertilizerProduction | urea ammonium nitrate production': "mineral fertilizer production and use",
'Meadow | grass': "carbon fixation in fodder biomass"
}

EmisScenarios = EmisScenarios.rename(index=itm_name)

EmisScenarios4Plot = EmisScenarios.groupby(level=0).sum().T

## Plotting

fig, (ax1, ax2) = plt.subplots(1,2, sharey=True)

ax1.axhline(y = 0, color = 'black', linestyle = '-', linewidth=0.5, zorder=2)
ax2.axhline(y = 0, color = 'black', linestyle = '-', linewidth=0.5, zorder=2)

EmisScenarios4Plot.loc[["storage_t1", "storage_t2"],:].plot.bar(stacked=True, ax=ax1, width=0.7, zorder=1)
EmisScenarios4Plot.loc[["biogas_t1", "biogas_t2"],:].plot.bar(stacked=True, ax=ax2, width=0.7, zorder=1)

# Left: Storage
ax1.legend(labels=[], frameon=False)
ax1.set_title("Storage", pad=20)
ax1.set_xlabel("Timestep")
ax1.set_ylabel("GHG emissions [ton CO$_{2eq}$]")

# Right: Biogas
ax2.set_title("Biogas", pad=20)
ax2.set_xlabel("Timestep")

for ax in [ax1, ax2]:
    # ax.set_xticklabels(ax.get_xticklabels(), rotation=0)
    ax.set_xticklabels(["t1", "t2"], rotation=0)

# General plot settings
plt.subplots_adjust(wspace=0.1, hspace=0.1)
plt.rcParams.update({'font.size': 25})

# Still missing are the net emissions.
NetEmisScenarios4Plot = pd.DataFrame(EmisScenarios4Plot.sum(axis=1), columns=["net emissions"])
ax1.scatter(y=NetEmisScenarios4Plot.loc[["storage_t1", "storage_t2"],:], x=["t1", "t2"], zorder=3, s=80, marker="D", color="black", label='Net emissions')
ax2.scatter(y=NetEmisScenarios4Plot.loc[["biogas_t1", "biogas_t2"],:], x=["t1", "t2"], zorder=3, s=80, marker="D", color="black", label='Net emissions')

# Legend for both parts
ax2.legend(loc='center left', bbox_to_anchor=(1.04, 0.5), frameon=False, prop={'size': 25}, reverse=True)

# Size for plot
fig.set_size_inches(10, 10, forward=True)


plt.show()


# Save figure
fig.savefig('C:\\Users\\ukreidenweis\\Nextcloud\\Paper - in Planung\\2 - BIORIM Model description paper\\Figures\\Fig5_GHG_emissions_scenarios_timesteps.png',
            bbox_inches='tight', dpi=300)
fig.savefig('C:\\Users\\ukreidenweis\\Nextcloud\\Paper - in Planung\\2 - BIORIM Model description paper\\Figures\\Fig5_GHG_emissions_scenarios_timesteps.pdf',
            bbox_inches='tight', dpi=300)
fig.savefig('C:\\Users\\ukreidenweis\\Nextcloud\\Paper - in Planung\\2 - BIORIM Model description paper\\Figures\\Fig5_GHG_emissions_scenarios_timesteps.svg',
            bbox_inches='tight', dpi=300)


########################## Plot 6 ##############################
#################### Sensitivity analysis ######################


# import pandas as pd
# import matplotlib.pyplot as plt

## Parameters that have been varied for the manure scenario:
# DairyFarm 'enteric_ferment_method' : ['Piatkowski_2021', 'IPCC2019_Tier1']

# ManureStorage 'method': ["EEA2016", "IPCC2019"]

## Parameters varied for the biogas scenario:
# DairyFarm 'enteric_ferment_method' : ['Piatkowski_2021', 'IPCC2019_Tier1']
# BiogasPlant 'residue_storage_type': ["closed", "open"]
# BiogasPlant 'thermal_energy_use': [0.4, 0, 0.6]

## New version of param table

def param_table(scenario):

    ParamTable = pd.DataFrame(columns=["run",
                                       "scenario",
                                       "enteric fermentation emissions",
                                       "storage emissions",
                                       "residue storage type",
                                       "thermal energy use share",
                                       "CO2eq_farm_ton"])

    for run in result[scenario]["t2"]["parameters"].columns:
        # MILK_KG = result[scenario]["t2"][run]["outputs"].loc[("production.animal.ruminant.DairyFarm_1", "cow milk"), "Q"].item() * 1000
        TOTAL_EMIS_KG = result[scenario]["gwp"].sel(run=run, gwp="GWP100", timestep="t2").to_pandas().sum().sum()
        # CO2EQ_KG_MILK = TOTAL_EMIS_KG / MILK_KG    # greenhouse gas footprint per kg of milk

        if scenario == "biogas":
            CO2eq_run = pd.DataFrame({
                "run": run,
                "scenario": scenario,
                "enteric fermentation emissions": result[scenario]["t2"]["parameters"].loc[("production.animal.ruminant.DairyFarm_1", "enteric_ferment_method"), run],
                "storage emissions": pd.NA,
                "residue storage type": result[scenario]["t2"]["parameters"].loc[("material.biogas.BiogasPlant_1", "residue_storage_type"), run],
                "thermal energy use share": result[scenario]["t2"]["parameters"].loc[("material.biogas.BiogasPlant_1", "thermal_energy_use"), run],
                "CO2eq_farm_ton": TOTAL_EMIS_KG / 1000
                },
                index =[run])

        elif scenario == "storage":
            CO2eq_run = pd.DataFrame({
                "run": run,
                "scenario": scenario,
                "enteric fermentation emissions": result[scenario]["t2"]["parameters"].loc[("production.animal.ruminant.DairyFarm_1", "enteric_ferment_method"), run],
                "storage emissions": result[scenario]["t2"]["parameters"].loc[("material.storing.ManureStorage_1", "method"), run],
                "residue storage type": pd.NA,
                "thermal energy use share": pd.NA,
                "CO2eq_farm_ton": TOTAL_EMIS_KG / 1000
                },
                index =[run])
        
        ParamTable = pd.concat([ParamTable, CO2eq_run])
    
    return ParamTable

ParamTableBiogas = param_table("biogas")
ParamTableStorage = param_table("storage")

replace_names = {
    "Piatkowski_2021": "Piatkowski et al. 2010",
    "IPCC2019_Tier1": "Tier 1 IPCC 2019",
    "EEA2016": "EEA 2016",
    "IPCC2019": "IPCC 2019"
}

ParamTableBiogas = ParamTableBiogas.replace(replace_names)
ParamTableStorage = ParamTableStorage.replace(replace_names)

# value for text
biogas_default_emis = ParamTableBiogas.loc[0,"CO2eq_farm_ton"]
biogas_open40_emis = ParamTableBiogas.loc[3,"CO2eq_farm_ton"]
biogas_closed0_emis = ParamTableBiogas.loc[1,"CO2eq_farm_ton"]


# Change through open storage
biogas_open40_emis/biogas_default_emis

# Change through 0% heat usage
biogas_closed0_emis/biogas_default_emis



ParamTable = pd.concat([ParamTableBiogas, ParamTableStorage])


import seaborn as sns

## Maybe try again with marker for the shared parameter
fig, (ax1, ax2) = plt.subplots(1,2, sharey=True)

# Common markers for scenarios
markers = {"Piatkowski et al. 2010": "o", "Tier 1 IPCC 2019": "X"}

sns.set_theme(style="whitegrid", palette="pastel")

# Storage
# c="#17BECF" 
sns.scatterplot(x="storage emissions", y="CO2eq_farm_ton", data=ParamTableStorage, c="black", style="enteric fermentation emissions", markers=markers, ax=ax1)
ax1.set_title("Storage", loc="center", pad=10, fontsize=14)
ax1.set_ylabel("GHG emissions [ton CO$_{2eq}$]", fontsize=12)
ax1.set_xlabel("storage emissions \n method", fontsize=12)
ax1.set_xticklabels(ax1.get_xticklabels(), rotation=90, fontsize=12)
ax1.legend([],[], frameon=False)
ax1.set_xlim(-0.75, 1.75)

# Biogas
# c="#1F77B4"
sns.scatterplot(x="residue storage type", y="CO2eq_farm_ton", data=ParamTableBiogas,  hue="thermal energy use share", style="enteric fermentation emissions", markers=markers, ax=ax2)
ax2.set_title("Biogas", loc="center", pad=10, fontsize=14)
# ax2.set_xticklabels(ParamTableBiogas["residue storage type"], rotation=45)
ax2.set_xlabel("residue storage \n type", fontsize=12)
ax2.set_xticklabels(ax2.get_xticklabels(), rotation=90, fontsize=12)
ax2.legend(bbox_to_anchor=(1.05, 0.5), loc='center left', frameon=False, handletextpad=1.5, fontsize=12)
ax2.set_xlim(-0.75, 1.75)

plt.ylim(0, 650)


plt.tight_layout()

plt.show()

fig.savefig('C:\\Users\\ukreidenweis\\Nextcloud\\Paper - in Planung\\2 - BIORIM Model description paper\\Figures\\Fig6_Sensitivity_analysis.png',
            bbox_inches='tight', dpi=300)
fig.savefig('C:\\Users\\ukreidenweis\\Nextcloud\\Paper - in Planung\\2 - BIORIM Model description paper\\Figures\\Fig6_Sensitivity_analysis.pdf',
            bbox_inches='tight', dpi=300)
fig.savefig('C:\\Users\\ukreidenweis\\Nextcloud\\Paper - in Planung\\2 - BIORIM Model description paper\\Figures\\Fig6_Sensitivity_analysis.svg',
            bbox_inches='tight', dpi=300)




########################## Plot 7 ##############################
######## Sensitivity analysis: Interdependence  ################


from matplotlib import colormaps

#### Change from closed to open digestate storage
# TODO: This may need to be adapted if new simulations are run


def difference_table(EmisDefault, EmisVariation):
    EmisDiff = EmisVariation - EmisDefault

    # Conversion to ton CO2eq
    EmisDiff = EmisDiff / 1000

    EmisSysGas = EmisDiff.reset_index()
    EmisSysGas["System"] = [sys.split(".")[-1].split("_")[0] for sys in EmisSysGas["source"]]
    EmisSysGas.loc[EmisSysGas["System"] == "MineralFertilizerProduction", "System"] = "FertilizerProduction"
    EmisSysGas.loc[EmisSysGas["System"] == "ExternInput", "System"] = "External"
    EmisSysGas = EmisSysGas.groupby("System").sum()
    EmisSysGas["N2O (direct + indirect)"] = EmisSysGas["N2O"] + EmisSysGas["N2O indirect"]
    EmisSysGas = EmisSysGas.drop(columns=["source", "item", "N2O", "N2O indirect"])

    return EmisSysGas

EmisDefault = result["biogas"]["gwp"].sel(run=0, gwp="GWP100", timestep="t2").to_pandas()   # closed, 40% thermal energy use

EmisOpenDigestate = result["biogas"]["gwp"].sel(run=3, gwp="GWP100", timestep="t2").to_pandas() # open, 40% thermal energy use
Emis0heat = result["biogas"]["gwp"].sel(run=1, gwp="GWP100", timestep="t2").to_pandas() # closed, 0% thermal energy use

EmisDiffOpenDigestate = difference_table(EmisDefault, EmisOpenDigestate)
EmisDiff0heat = difference_table(EmisDefault, Emis0heat)

fig, (ax1, ax2) = plt.subplots(1,2, sharey=True, figsize=(8, 3))

# palette = plt.get_cmap('tab10')
# colors = [palette(i) for i in np.linspace(0, 1, len(data))]

colors = ["orange", "green", "blue", "brown"]

# EmisSysGas.plot.bar(stacked=True, width=0.7, zorder=1, ax=ax1, color=colors)
# Left: Open digestate storage

EmisDiffOpenDigestate.plot.bar(stacked=True, width=0.7, zorder=1, ax=ax1) # TODO: Use other color scale
ax1.legend(labels=[], frameon=False)
ax1.set_title("a) Change from closed to open \n     biogas digestate storage", loc="left", pad=20)
ax1.set_ylabel("GHG emissions [ton CO$_{2eq}$]")
# ax1.set_xlabel("Timestep")

EmisDiff0heat.plot.bar(stacked=True, width=0.7, zorder=1, ax=ax2) # TODO: Use other color scale
# ax1.legend(labels=[], frameon=False)
ax2.set_title("b) Change from 40 % to 0 % \n     thermal energy use", loc="left", pad=20)
ax2.legend(loc='center left', bbox_to_anchor=(1.04, 0.5), frameon=False,)

ax1.axhline(y = 0, color = 'black', linestyle = '-', linewidth=0.5, zorder=2)
ax2.axhline(y = 0, color = 'black', linestyle = '-', linewidth=0.5, zorder=2)

# ax2.set_ylabel("GHG emissions [ton CO$_{2eq}$]")
# ax1.set_xlabel("Timestep")

# plt.figure(figsize=(16,9))

# w, h = figaspect(2.)
# fig = Figure(figsize=(w, h))

plt.show()

# Save figure
fig.savefig('C:\\Users\\ukreidenweis\\Nextcloud\\Paper - in Planung\\2 - BIORIM Model description paper\\Figures\\Fig6_Sensitivity_analysis.png',
            bbox_inches='tight', dpi=300)
fig.savefig('C:\\Users\\ukreidenweis\\Nextcloud\\Paper - in Planung\\2 - BIORIM Model description paper\\Figures\\Fig6_Sensitivity_analysis.pdf',
            bbox_inches='tight', dpi=300)
fig.savefig('C:\\Users\\ukreidenweis\\Nextcloud\\Paper - in Planung\\2 - BIORIM Model description paper\\Figures\\Fig6_Sensitivity_analysis.svg',
            bbox_inches='tight', dpi=300)

# Values for text:
EmisDiffOpenDigestate.loc["BiogasPlant",:].sum()
EmisDiffOpenDigestate.sum().sum()




# Right: Biogas
ax2.set_title("Biogas", pad=20)
ax2.set_xlabel("Timestep")

for ax in [ax1, ax2]:
    # ax.set_xticklabels(ax.get_xticklabels(), rotation=0)
    ax.set_xticklabels(["t1", "t2"], rotation=0)

# General plot settings
plt.subplots_adjust(wspace=0.1, hspace=0.1)
plt.rcParams.update({'font.size': 25})


plt.show()


# ## Other potentially sensitive parameter:
# # Emissions for electricity

# ## maybe a simple scatterplot for these

# import seaborn as sns

# ## Scatter plot for total emissions of the two parameters: 
# ax = sns.scatterplot(x="residue storage type", y="CO2eq_farm", data=ParamTableSimple, hue="thermal energy use share")
# sns.move_legend(ax, "upper left", bbox_to_anchor=(1, 1))
# plt.show()


# ## The default is the first
# ParamTableSimple[ParamTableSimple["residue storage type"] == "closed"][ParamTableSimple["thermal energy use share"] == 0.4]["run"]

# ParamTableSimple["CO2eq_diff"] = ParamTableSimple["CO2eq_farm"] - ParamTableSimple.loc[0,"CO2eq_farm"]


# ## What I basically need is to calculate the difference in total emissions


# ParamTableSimple



# def param_table(scenario):

#     ParamTable = pd.DataFrame()

#     EmisDefault = result["biogas"]["gwp"].sel(run=0, gwp="GWP100", timestep="t2").to_pandas()   # closed, 40% thermal energy use


#     for run in range(1, len(result["biogas"]["t2"]["parameters"].columns)):
#         print(run)
        
#         EmisVariation = result["biogas"]["gwp"].sel(run=3, gwp="GWP100", timestep="t2").to_pandas() # open, 40% thermal energy use

#         EmisDiff = EmisVariation - EmisDefault

#         # Total emission difference
#         TotalDiff = EmisDiff.sum()

#         # Difference only of biogas plant emissions
#         EmisDiff.loc["material.biogas.BiogasPlant_1",:].sum()



#         print(run)
#         MILK_KG = result[scenario]["t2"][run]["outputs"].loc[("production.animal.ruminant.DairyFarm_1", "cow milk"), "Q"].item() * 1000
#         TOTAL_EMIS_KG = result[scenario]["gwp"].sel(run=run, gwp="GWP100", timestep="t2").to_pandas().sum().sum()

#         CO2EQ_KG_MILK = TOTAL_EMIS_KG / MILK_KG    # greenhouse gas footprint per kg of milk

#         if scenario == "biogas":
#             residue_storage_type = result[scenario]["t2"]["parameters"].loc[("material.biogas.BiogasPlant_1", "residue_storage_type"), run],
#         else:
#             residue_storage_type = "none"

#         # Line entry
#         CO2eq_run = pd.DataFrame({
#             "run": run,
#             "scenario": scenario,
#             # "enteric_ferment_method": result[scenario]["t2"]["parameters"].loc[("production.animal.ruminant.DairyFarm_1", "enteric_ferment_method"), run],
#             "thermal energy use share": result[scenario]["t2"]["parameters"].loc[("material.biogas.BiogasPlant_1", "thermal_energy_use"), run],
#             "residue storage type": result[scenario]["t2"]["parameters"].loc[("material.biogas.BiogasPlant_1", "residue_storage_type"), run],
#             "CO2eq_farm": TOTAL_EMIS_KG,
#             "CO2eq_milk": CO2EQ_KG_MILK
#             },
#             index =[run])

#         Sensitivity_CO2eq_Param = pd.concat([Sensitivity_CO2eq_Param, CO2eq_run])
    
#     return Sensitivity_CO2eq_Param


## simple try out

EmisDefault = result["biogas"]["gwp"].sel(run=0, gwp="GWP100", timestep="t2").to_pandas()   # closed, 40% thermal energy use
EmisVariation = result["biogas"]["gwp"].sel(run=3, gwp="GWP100", timestep="t2").to_pandas() # open, 40% thermal energy use

EmisDiff = EmisVariation - EmisDefault

# Total emission difference
TotalDiff = EmisDiff.sum()

# Difference only of biogas plant emissions
BiogasDiff = EmisDiff.loc["material.biogas.BiogasPlant_1",:].sum()

TotalDiff = TotalDiff.to_frame(name="total").T
BiogasDiff = BiogasDiff.to_frame(name="Biogas Plant").T

EmisChange = pd.concat([TotalDiff, BiogasDiff])

EmisChange = EmisChange / 1000  # conversion to ton CO2eq

EmisChange.plot.bar(stacked=True, ax=ax1, width=0.7, zorder=1)

plt.show()








### Instead of showing the emissions that change, show the systems that change

EmisDiff.sum().sum()    





# The difference between 0 and 3 is the comparison betwe




CO2eq_diff


sns.scatterplot(x="scenario", y="CO2eq_milk", data=Sensitivity_CO2eq_Param, style="residue_storage_type", hue="enteric_ferment_method")






# just a subset for the biogas scenario
ax = sns.scatterplot(x="residue_storage_type", y="CO2eq_milk", data=Sensitivity_CO2eq_Param[Sensitivity_CO2eq_Param["scenario"] == "biogas"], style="residue_storage_type", hue="enteric_ferment_method")
sns.move_legend(ax, "upper left", bbox_to_anchor=(1, 1))

plt.show()

## Compare the two scenarios only
ax = sns.scatterplot(x="scenario", y="CO2eq_milk", data=Sensitivity_CO2eq_Param[Sensitivity_CO2eq_Param["residue_storage_type"] != "open"], hue="enteric_ferment_method")
sns.move_legend(ax, "upper left", bbox_to_anchor=(1, 1))
plt.show()


## Somehow what I want to show: 
# There is a difference between only changing one variable and considering the overall changes

### Whats the difference between total emissions of the different fermentation methods, and the direct emissions from the process

result["biogas"]["t2"]["parameters"].loc[("production.animal.ruminant.DairyFarm_1", "enteric_ferment_method"), :]

# compare 0 and 4: Piatkowski vs Kirchgeßner
result["biogas"]["t2"]["parameters"].loc[:, [0,4]]

## the direct emissions
EntericFermentEmisBiogast2Piatkowski = result["biogas"]["gwp"].sel(run=0, gwp="GWP100", timestep="t2").to_pandas().loc[("production.animal.ruminant.DairyFarm_1", "enteric fermentation"),:].sum()
EntericFermentEmisBiogast2Piatkowski
EntericFermentEmisBiogast2Kirchgeßner = result["biogas"]["gwp"].sel(run=4, gwp="GWP100", timestep="t2").to_pandas().loc[("production.animal.ruminant.DairyFarm_1", "enteric fermentation"),:].sum()
EntericFermentEmisBiogast2Kirchgeßner

EntericFermentEmisBiogast2Piatkowski - EntericFermentEmisBiogast2Kirchgeßner    # 131144


## Total emissions
EntericFermentEmisBiogast2Piatkowski_total = result["biogas"]["gwp"].sel(run=0, gwp="GWP100", timestep="t2").to_pandas().sum().sum()
EntericFermentEmisBiogast2Piatkowski_total
EntericFermentEmisBiogast2Kirchgeßner_total = result["biogas"]["gwp"].sel(run=4, gwp="GWP100", timestep="t2").to_pandas().sum().sum()
EntericFermentEmisBiogast2Kirchgeßner_total

EntericFermentEmisBiogast2Piatkowski_total - EntericFermentEmisBiogast2Kirchgeßner_total    # 131144

## Try the same again with biogas plant open or closed:

result["biogas"]["t2"]["parameters"].loc[("production.animal.ruminant.DairyFarm_1", "enteric_ferment_method"), :]


EmisBiogast2closed = result["biogas"]["gwp"].sel(run=0, gwp="GWP100", timestep="t2").to_pandas().loc["material.biogas.BiogasPlant_1",:].sum().sum()
EmisBiogast2open = result["biogas"]["gwp"].sel(run=3, gwp="GWP100", timestep="t2").to_pandas().loc["material.biogas.BiogasPlant_1",:].sum().sum()
EmisBiogast2open - EmisBiogast2closed


EmisBiogast2closed_total = result["biogas"]["gwp"].sel(run=0, gwp="GWP100", timestep="t2").to_pandas().sum().sum()
EmisBiogast2open_total = result["biogas"]["gwp"].sel(run=3, gwp="GWP100", timestep="t2").to_pandas().sum().sum()
EmisBiogast2open_total - EmisBiogast2closed_total



# compare 0 and 1 (Piatkowski open vs closed)





## Enteric fermentation emissions relative to default
# With bar charts: How much are emissions increased or decreased





## This has to highlight somehow that open storage is not just shifting emissions up

Sensitivity_CO2eq_Param.iloc[1,2]-Sensitivity_CO2eq_Param.iloc[0,2]

Sensitivity_CO2eq_Param.iloc[3,2]-Sensitivity_CO2eq_Param.iloc[2,2]

## The difference between the two is exactly the same. It leads only to an upward shift. Is there any functionality that does something different?
# The example is maybe not so good:
# In reality one would expect there to be a non-linear response. Higher methane emissions earlier on should reduce the potential downstream.
# I want a case where lower emissions earlier on lead to higher emissions later on. Something with fertilization value?

################################################################

### Check input output balance 

# Split it into carbon and nitrogen.
# Solve each problem after another

# nitrogen: NOTE: Currently not working. TODO: Needs to be checked
result.input_output_plots_pdf(filename="2022-06-30_result_in_out_N.pdf", path="C:/Users/ukreidenweis/Documents/biorim/simulation/model_description_paper", elements="nitrogen")

result["biogas"]["t1"][0]["systems"]['material.biogas.BiogasPlant_1'].output()

input = result["biogas"]["t1"][0]["outputs"].xs('material.biogas.BiogasPlant_1', level="sink")

input.dropna(subset=["N"])


result["biogas"]["t1"]["SourceToSink"]

##########################################################################
# Test for total mass:


def sankey_diagram_flows(result, scenario="biogas", t="t2", run=0, element="carbon", summarize_N_gases="NOx_only"):
    Outputs = result[scenario][t][run]["outputs"]
    Out_kg = pd.DataFrame()
        
    if element == "mass":
        # Carbon outputs
        Out_kg["Q"] = Outputs["Q"] * 1000

        # Emissions
        Emis_kg = result[scenario][t][run]["emissions"]



    Out_kg = Out_kg.reset_index()

    Emis_kg = Emis_kg.groupby("source").sum()
    Emis_kg = Emis_kg.reset_index()
    Emis_kg = pd.melt(Emis_kg, id_vars=["source"], var_name="sink", value_name="Q")


    # for negative emissions source and sink have to be switched
    Emis_neg = Emis_kg[Emis_kg["Q"] < 0]
    Emis_pos = Emis_kg[Emis_kg["Q"] > 0] # Zero emissions can be neglected

    Emis_neg.columns = ["sink", "source", "Q"]
    # Emis_neg.loc[:,"Q"] = Emis_neg.loc[:,"Q"] * -1
    Emis_neg["Q"] *= -1

    # Name negative emissions: uptake (probably not relevant for nitrogen)
    Emis_neg["source"] = Emis_neg["source"].replace({"CO2-C": 'CO2 uptake'})

    Emis_pos["sink"] = Emis_pos["sink"].replace({"CO2-C": 'CO2 emissions', "CH4-C": 'CH4 emissions'})
    Emis_pos["sink"] = Emis_pos["sink"].replace({"CO2-C": 'CO2 emissions', "CH4-C": 'CH4 emissions'})


    # Combine positive and negative emissions again
    Emis_kg = pd.concat([Emis_pos, Emis_neg])

    # NOTE: I need to differentiate between carbon fixation and release of biogenic carbon on the one hand
    # and fossil carbon emissions on the other
    # For the momemt neglect this, and just consider also too high emissions
    # But it also means for instance negative emissios from electricity

    Emis_kg["item"] = Emis_kg["sink"]

    # Combine outputs and emissions
    Flows_kg = pd.concat([Emis_kg, Out_kg])

    # Drop values with NA. NOTE: There are still some things that should be contained, but where the respective data is missing
    Flows_kg = Flows_kg.dropna(axis=0)

    ### Manual modifications: Renaming etc.

    # Remove some systems

    if element == "carbon":
        Flows_kg = Flows_kg[Flows_kg["source"] != "production.plant.fertilization.MineralFertilizerProduction_1000"]
        Flows_kg = Flows_kg[Flows_kg["source"] != "production.plant.fertilization.MineralFertilizerProduction_1001"]
    Flows_kg = Flows_kg[Flows_kg["source"] != "external.Electricity_1000"]
    Flows_kg = Flows_kg[Flows_kg["source"] != "external.ExternInput_1000"]
    Flows_kg = Flows_kg[Flows_kg["source"] != "external.ExternInput_1001"]

    Flows_kg = Flows_kg[Flows_kg["sink"] != "external.Electricity_1"]

    # Short names
    Flows_kg["source_short"] = [sys.split(".")[-1] for sys in Flows_kg["source"]]
    Flows_kg["sink_short"] = [sys.split(".")[-1] for sys in Flows_kg["sink"]]

    # Show some outputs as nodes
    Flows_kg.loc[Flows_kg["sink_short"] == "ExternOutput_1", "sink_short"] = "Milk"
    Flows_kg.loc[Flows_kg["sink_short"] == "ExternOutput_2", "sink_short"] = "Cows to Slaughter"
    Flows_kg.loc[Flows_kg["sink_short"] == "ExternOutput_3", "sink_short"] = "Calves to Rearing"

    # Short name of Mineral Fertilizer Production
    Flows_kg.loc[Flows_kg["source_short"] == "MineralFertilizerProduction_1000", "source_short"] = "FertilizerProduction_1000"
    Flows_kg.loc[Flows_kg["source_short"] == "MineralFertilizerProduction_1001", "source_short"] = "FertilizerProduction_1001"


    # Replace underscore (_) with space
    Flows_kg[["source_short", "sink_short"]] = Flows_kg[["source_short", "sink_short"]].replace(to_replace=r'_', value=' ', regex=True)


    ## Assign numbers to source and sinks:
    systems = list(set(Flows_kg["source_short"]).union(set(Flows_kg["sink_short"])))
    systems.sort()
    systems

    sys_nbr = {}
    for i, sys in enumerate(systems):
        sys_nbr[sys] = i

    Flows_kg["source_nbr"] = Flows_kg["source_short"].copy()
    Flows_kg["sink_nbr"] = Flows_kg["sink_short"].copy()

    Flows_kg = Flows_kg.replace({"source_nbr":sys_nbr})
    Flows_kg = Flows_kg.replace({"sink_nbr":sys_nbr})

    # Set figure parameters
    label = list(sys_nbr.keys())
    source = list(Flows_kg["source_nbr"])
    target = list(Flows_kg["sink_nbr"])
    value = list(Flows_kg["Q"])

    if element == "carbon":
        x_pos = {'CO2 uptake': 0.05,
                 'Meadow 1': 0.2,
                 'ArableLand 1': 0.2,
                 'ArableLand 1000': 0.2,
                 'HeiferRearing 1000': 0.2,
                 'DairyFarm 1': 0.4,
                 'Milk': 0.6, # The milk (goint to ExternOutput_1)
                 'Cows to Slaughter': 0.6,
                 'Calves to Rearing': 0.6,
                 'BiogasPlant 1': 0.6,
                 'DigestateStorage 1': 0.8,
                 'ManureStorage 1': 0.7,
                 'CH4 emissions': 0.9,
                 'CO2 emissions': 0.9
                 }

        y_pos = {'CO2 uptake': 0.4,
                 'Meadow 1': 0.15,
                 'ArableLand 1000': 0.25,
                 'ArableLand 1': 0.55,
                 'HeiferRearing 1000': 0.8,
                #  'HeiferRearing 1000': 1,
                 'DairyFarm 1': 0.4,
                 'Milk': 0.5,
                 'Cows to Slaughter': 0.6,
                 'Calves to Rearing': 0.65,
                 'BiogasPlant 1': 0.8,
                 'DigestateStorage 1': 0.8,
                 'ManureStorage 1': 0.8,
                 'CH4 emissions': 0.1,
                 'CO2 emissions': 0.3
                 }
        
        color = {'CO2 uptake': "blue",
                 'Meadow 1': "green",
                 'ArableLand 1': "green",
                 'ArableLand 1000': "green",
                 'HeiferRearing 1000': "green",
                 'DairyFarm 1': "green",
                 'Milk': "grey",
                 'Cows to Slaughter': "grey",
                 'Calves to Rearing': "grey",
                 'BiogasPlant 1': "green",
                 'DigestateStorage 1': "green",
                 'ManureStorage 1': "green",
                 'CH4 emissions': "blue",
                 'CO2 emissions': "blue"
                 }
        
    elif element == "nitrogen":
        x_pos = {'FertilizerProduction 1001': 0.05,
                 'FertilizerProduction 1000': 0.05,
                 'Meadow 1': 0.2,
                 'ArableLand 1': 0.2,
                 'ArableLand 1000': 0.2,
                 'HeiferRearing 1000': 0.2,
                 'DairyFarm 1': 0.4,
                 'Milk': 0.6, # The milk (goint to ExternOutput_1)
                 'Cows to Slaughter': 0.6,
                 'Calves to Rearing': 0.6,
                 'BiogasPlant 1': 0.6,
                 'ManureStorage 1': 0.7,
                 'DigestateStorage 1': 0.8,
                 'N2O emissions': 0.9,
                 'NH3 & NOx emissions': 0.9,
                 'N2 emissions': 0.9
                 }
        
        y_pos = {'FertilizerProduction 1001': 0.28,
                 'FertilizerProduction 1000': 0.45,
                 'Meadow 1': 0.15,
                 'ArableLand 1000': 0.35,
                 'ArableLand 1': 0.65,
                 'HeiferRearing 1000': 0.8,
                 'DairyFarm 1': 0.4,
                 'Milk': 0.38,
                 'Cows to Slaughter': 0.45,
                 'Calves to Rearing': 0.5,
                 'BiogasPlant 1': 0.8,
                 'ManureStorage 1': 0.8,
                 'DigestateStorage 1': 0.8,
                 'NH3 & NOx emissions': 0.1,
                 'N2O emissions': 0.2,
                 'N2 emissions': 0.3
                 }
        
        color = {'FertilizerProduction 1001': "grey",
                 'FertilizerProduction 1000': "grey",
                 'Meadow 1': "brown",
                 'ArableLand 1000': "brown",
                 'ArableLand 1': "brown",
                 'HeiferRearing 1000': "brown",
                 'DairyFarm 1': "brown",
                 'Milk': "grey",
                 'Cows to Slaughter': "grey",
                 'Calves to Rearing': "grey",
                 'BiogasPlant 1': "brown",
                 'ManureStorage 1': "brown",
                 'DigestateStorage 1': "brown",
                 'NH3 & NOx emissions': "blue",
                 'NH3 emissions': "blue",
                 'N2O emissions': "blue",
                 'NO emissions': "blue",
                 'NOx emissions': "blue",
                 'N2 emissions': "blue"
                 }

    fig = go.Figure(go.Sankey(
        arrangement = "snap",
        # arrangement = "fixed",
        node = {
            "label": label
            },
        link = {
            "source": source,
            "target": target,
            "value": value}))

    # fig = go.Figure(go.Sankey(
    #     arrangement = "snap",
    #     # arrangement = "fixed",
    #     node = {
    #         "label": label,
    #         "x": list(map(x_pos.get, label)),
    #         "y": list(map(y_pos.get, label)),
    #         "color": list(map(color.get, label)),
    #         },
    #     link = {
    #         "source": source,
    #         "target": target,
    #         "value": value}))
    
    if element == "carbon":
        letter = "a) "
    elif element == "nitrogen":
        letter = "b) "
    elif element == "mass":
        letter = "a) "
    title = letter + element.capitalize() + " flows in the " + scenario + " scenario"

    # NOTE: Try to scale manually, because there seems to be no other option to achieve same node sizes
    if element == "mass":
        if scenario == "biogas":
            height_px = 600
        elif scenario == "storage":
            height_px = 545
    
    if element == "carbon":
        if scenario == "biogas":
            height_px = 600
        elif scenario == "storage":
            height_px = 545

    if element == "nitrogen":
        if scenario == "biogas":
            height_px = 600
        elif scenario == "storage":
            height_px = 545

    fig.update_layout(
        autosize=False,
        width=1200,
        # height=600,
        height=height_px,
        title_text=title,
        title_font_family="Segoe UI Semibold",
        title_font_color="black",
        font_family="Segoe UI",
        font_color="black",
        font_size=22,
        margin=go.layout.Margin(
                l=0, #left margin
                r=0, #right margin
                b=80, #bottom margin
                t=50  #top margin
                )
        )

    return fig


MassflowBiogas = sankey_diagram_flows(result=result, scenario="biogas", t="t2", run=0, element="mass", summarize_N_gases="NH3-N & NOx-N")

MassflowBiogas.show()
fig.show()


#############################################################################################################




# TODO:
# Check why there are higher CO2 emissions from ArableLand_1000 production: probably reduction in humus?
AR_1000 = result["biogas"]["t2"][0]["systems"]["production.plant.crop.ArableLand_1000"]
AR_1000.emissions() # -18200 kg CO2-C uptake, 4040 kg emissions from humus balance
# => Yes, it is the humus that is reduced




## For the sankey diagram to work everything needs to be specified as source and sink


## first I need the flows. How to get them?

result["biogas"]["t1"][0].keys()

## first start with carbon flows

## flows between systems?
Outputs = result["biogas"]["t2"][0]["outputs"]

Outputs



C_out_kg = Outputs["Q"] * Outputs["DM"] * Outputs["oDM"]* Outputs["orgC"] * 1000




systems_storage = set(systems)

systems_biogas = set(systems)

systems_set = systems_storage.union(systems_biogas)

list(systems_set)





## Figure of  total mass flows, where emissions are not represented
Outputs = result["biogas"]["t2"][0]["outputs"]


mass_flows_ton = Outputs.loc[:,["DM","Q", "U"]]

# only use the inputs that are in tons (excludes the lubricating oil)
mass_flows_ton = mass_flows_ton[mass_flows_ton["U"]=="ton"]

mass_flows_ton["DM_ton"] = mass_flows_ton["Q"] * mass_flows_ton["DM"]

mass_flows_ton = mass_flows_ton.reset_index(level=["source", "sink"])

# drop rows where something is not defined
mass_flows_ton = mass_flows_ton.dropna(axis=0)

# rename to shorter version

mass_flows_ton["source_short"] = [sys.split(".")[-1] for sys in mass_flows_ton["source"]]
mass_flows_ton["sink_short"] = [sys.split(".")[-1] for sys in mass_flows_ton["sink"]]

# assign numbers to systems

systems = list(set(mass_flows_ton["source_short"]).union(set(mass_flows_ton["sink_short"])))
systems.sort()
systems

sys_nbr = {}
for i, sys in enumerate(systems):
    sys_nbr[sys] = i

sys_nbr

mass_flows_ton["source_nbr"] = mass_flows_ton["source_short"].copy()
mass_flows_ton["sink_nbr"] = mass_flows_ton["sink_short"].copy()

mass_flows_ton = mass_flows_ton.replace({"source_nbr":sys_nbr})
mass_flows_ton = mass_flows_ton.replace({"sink_nbr":sys_nbr})

mass_flows_ton


label = list(sys_nbr.keys())
label
source = list(mass_flows_ton["source_nbr"])
target = list(mass_flows_ton["sink_nbr"])
value = list(mass_flows_ton["DM_ton"])

x_pos = {'Meadow_1': 0.3,
         'ArableLand_1000': 0.3,
         'ArableLand_1': 0.3,
         'DairyFarm_1': 0.55,
         'DigestateStorage_1':0.85,
         'ManureStorage_1':0.85,
         'BiogasPlant_1':0.75}

import plotly.graph_objects as go

fig = go.Figure(go.Sankey(
    arrangement = "snap",
    node = {
        "label": label,
        # "x": [0.3, 0.3, 0.75, 0.55, 0.85, 0.3],
        "x": list(map(x_pos.get, label)),
        "y": [0.3, 0.1, 0.30, 0.30, 0.15, 0.4],
        'pad':10},  # 10 Pixels
    link = {
        "source": source,
        "target": target,
        "value": value}))

fig.show()


## What is the reason for the large gap in CO2 emissions from Dairy?

# What is the DM intake of the cows

Fodder = result["biogas"]["t2"][0]["systems"]['production.animal.ruminant.DairyFarm_1'].fodder().copy()
n = result["biogas"]["t2"][0]["systems"]['production.animal.ruminant.DairyFarm_1'].n
n
(Fodder["Q"]*Fodder["DM"]).sum() * 1000 / (n * 365) # dry matter input is about 19.8 kg per day. Seems realistic

result["biogas"]["t2"][0]["systems"]['production.animal.ruminant.DairyFarm_1'].manure_production()


# IMPORTANT: It seems as if higher inputs in organic manure to ArableLand don't increase its emissions. Really
import start
from tools import prod
from production.plant.crop import ArableLand
from production.animal.ruminant import DairyFarm

Manure = DairyFarm(n=10).output().loc[["cow manure, liquid"],:]
Manure["Q"] = 100
ArableLand(I=Manure).emissions()

Manure["Q"] = 1000
ArableLand(I=Manure).emissions()    # Negative emissions from humus balance are much higher in this case. Is this not some form of double accounting?? Should it not in fact lead to positive emissions??


ArableLand(O=prod("winter wheat, grain", 5, "ton")).output()





########
## Plot 2 alternative: version where the two scenarios are seperated as plots:

# fig, (ax1, ax2) = plt.subplots()
# ax1 = EmisScenarios4Plot[["storage_t1", "storage_t1"]].plot.bar(stacked=True)

i = 0

for scen in scenarios:
    for t in timesteps:
        # emis = result[scen]["gwp"].sel(run=0, gwp="GWP100", timestep=t).to_pandas()
        emis = result[scen]["gwp"].sel(run=0, gwp="GWP100", timestep=t).sum(axis=1).to_pandas()

        emis = emis.to_frame()

        # name = f"{scen}_{t}"
        emis.columns = pd.MultiIndex.from_tuples([(scen, t)])

        emis = emis / 1000 # convert to ton CO2eq
            
        emis = emis.rename(index=itm_name)
        
        # emis = emis.droplevel("source")


        if i == 0:
            EmisScenarios = emis
        else:
            EmisScenarios = EmisScenarios.join(emis, how="outer")
        
        i += 1
        

EmisScenarios4Plot = EmisScenarios.droplevel("source")
EmisScenarios4Plot = EmisScenarios4Plot.groupby(level=0).sum()
EmisScenarios4Plot = EmisScenarios4Plot.T

EmisScenarios4Plot.plot.bar(stacked=True)
plt.legend(loc='center left', bbox_to_anchor=(1.04, 0.5))

plt.show()

axes = EmisScenarios4Plot.plot.bar(stacked=True, subplots=True, rot=90)






fig, (ax1, ax2) = plt.subplots(1, 2)

ax1 = EmisScenarios4Plot["storage_t1"].T.plot.bar(stacked=True)
ax2 = EmisScenarios4Plot["storage_t2"].T.plot.bar(stacked=True)

plt.show()



fig, ax = plt.subplots()
EmisScenarios4Plot.loc["storage_t1", :].plot.bar(stacked=True, position=1.5, ax=ax)
EmisScenarios4Plot.loc["storage_t2", :].plot.bar(stacked=True, position=0.5, ax=ax)
plt.show()


fig, ax = plt.subplots()
EmisScenarios4Plot.loc[["storage_t1","storage_t2"]].plot.bar(stacked=True, ax=ax, position=1, width=0.1, align="edge")
EmisScenarios4Plot.loc[["biogas_t1","biogas_t2"]].plot.bar(stacked=True, ax=ax, position=0.5, width=0.1, align="edge")

ax.legend(loc='center left', bbox_to_anchor=(1.04, 0.5))

plt.show()




EmisScenarios4Plot2 = EmisScenarios.groupby(level=0).sum()
# EmisScenarios4Plot2 = EmisScenarios4Plot.T

EmisScenarios4Plot2.T.plot.bar(stacked=True)
plt.show()



# Change plot style

plt.xticks(rotation=0)
plt.ylabel("Emissions [ton CO$_{2eq}$]")
plt.xlabel("Scenario")

plt.show()





# Change plot style

plt.xticks(rotation=0)
plt.ylabel("Emissions [ton CO$_{2eq}$]")
plt.xlabel("Scenario")

plt.show()





## subset only for timestep t2
# Emis_scen_t2 = Emis_scen.loc[Emis_scen["timestep"] == "t2"]

## add net emissions

NetEmis = Emis_scen.groupby(['timestep', 'scenario']).agg({'value':'sum'}).reset_index()

(ggplot()
    + geom_col(aes(x='timestep', y='value', fill='item'), data=Emis_scen)
    + facet_wrap("scenario")
    + geom_point(aes(x='timestep', y='value'), data=NetEmis)

    + labs(y="CO2 eq emissions [tonnes]")
    + theme_minimal()
    + theme(panel_grid_major_x = element_blank(),
            # panel_grid_major_y = element_line(colour="black"),
            )
)



# TODO & Open questions:
#######################

###  There is currently no difference in the mineral fertilizer demand between storage and biogas. Why???
# Answer: Production quantities were to low in comparison to the manure supply
### ArableLand_1 should get the cow manure or the digestate as input

# biogas case:
result["biogas"]["t1"][0]["outputs"].xs("urea ammonium nitrate", level="item").loc[:,["Q", "U"]]
result["biogas"]["t2"][0]["outputs"].xs("urea ammonium nitrate", level="item").loc[:,["Q", "U"]]
# in the second timestep one of the fertilization productions disappears. This is probably because digestate replaces the mineral fertilizer completely

result["biogas"]["t1"][0]["systems"]['production.plant.crop.ArableLand_1'].input().loc[:,["Q", "U"]]  # t1: UAN fertilzier as only input
result["biogas"]["t2"][0]["systems"]['production.plant.crop.ArableLand_1'].input().loc[:,["Q", "U"]]  # t1: only digestate as input

# storage case:
result["storage"]["t1"][0]["outputs"].xs("urea ammonium nitrate", level="item").loc[:,["Q", "U"]]
result["storage"]["t2"][0]["outputs"].xs("urea ammonium nitrate", level="item").loc[:,["Q", "U"]]
# in the second timestep one of the mineral fertilizer production is reduced significantly, but does not disappear completely

result["storage"]["t1"][0]["systems"]['production.plant.crop.ArableLand_1'].input().loc[:,["Q", "U"]]  # t1: UAN fertilzier as only input
result["storage"]["t2"][0]["systems"]['production.plant.crop.ArableLand_1'].input().loc[:,["Q", "U"]]  # t1: only digestate as input

# NOTE: Maybe redict the digestate to another field, where there is no oversupply? Currently it seems as if in both cases there is just no more mineral fertilizer
# Even better would be if it were possible to split the manure between different fields
# Currently the option of sharing the manure between different fields is not available


## Is there a mistake in the ArableLand class. There never seems to be a demand for mineral fertilizer if any manure is supplied

from production.animal.ruminant import DairyFarm
Manure = DairyFarm().output().loc[["cow manure, liquid"],:]
Manure["Q"] = 1
Manure

from production.plant.crop import ArableLand

# at least for winter wheat it seems to work
ArableLand(I=Manure).input()
ArableLand(I=Manure).output()

result["storage"]["t1"][0]["outputs"].xs("cow manure, liquid", level="item").loc[:,"Q"] # 1930 tons of manure supplied
result["storage"]["t2"][0]["outputs"].loc["production.plant.crop.ArableLand_1","Q"] # 10000 tons of silage maize produced

Manure = DairyFarm().output().loc[["cow manure, liquid"],:]
Manure["Q"] = 1930
Manure

ArableLand(I=Manure, O=prod("silage maize, whole", 1500)).input() #  From about 1500 tons of maize onwards, there should be additional demand of mineral fertilizer
ArableLand(I=Manure).output()


## How big is the differences between N supplied via manure and digestate??

ManureOut = result["storage"]["t1"][0]["systems"]['material.storing.ManureStorage_1'].output().loc[:,["Q","N","U"]]
DigestateOut = result["biogas"]["t1"][0]["systems"]['material.biogas.BiogasPlant_1'].output().loc[["biogas digestate"],["Q","N","U"]]

ManureOut["Q"] * ManureOut["N"] # 7568 kg of N
DigestateOut["Q"] * DigestateOut["N"] # 8685 kg of N
# Answer: About 1000 kg more of N preserved



### Why are emissions in second time step of storage lower than in first?

result["storage"]["t1"][0]["emissions"]
result["storage"]["t2"][0]["emissions"]

storage_gwp_t1 = result["storage"]["gwp"].sel(timestep="t1", run=0, gwp="GWP100").to_pandas()
storage_gwp_t2 = result["storage"]["gwp"].sel(timestep="t2", run=0, gwp="GWP100").to_pandas()
storage_gwp_t1 - storage_gwp_t2


# Answer:
# 1. In first time step higher emissions from reduction in humus (no manure supply)
# 2. Emissions from mineral fertilizer production are much lower


### Why are emissions in second time step of biogas so much higher?

biogas_gwp_t1 = result["biogas"]["gwp"].sel(timestep="t1", run=0, gwp="GWP100").to_pandas()
biogas_gwp_t2 = result["biogas"]["gwp"].sel(timestep="t2", run=0, gwp="GWP100").to_pandas()
biogas_gwp_t1 - biogas_gwp_t2

# Answer: Actually emissions (total CO2eq) are lower
# 1. Mineral fertilizer production is significantly lower
# 2. However, indirect N2O emissions from biogas digestate distribution are higher
# 3. Losses in humus are also reduced






############################ Plot 3 #########################
#  Input/ Output plots for some of the main processes #######

from dev_utils import input_output_plot

plots = []
for scenario in result:
    print(f"start creating input/output plots for scenarios {scenario}")
    # plots.append(text_plot(text=("scenario: " + scenario)))

    timestep = "t2" # only for the last timestep
    run = 0 # only for the first run
    # elements = ["nitrogen", "carbon"]
    elements = ["nitrogen"]

    # for sysname in self[scenario][timestep][run]["systems"]:
    # instead of running through all systems only go through the ones which are listed as sinks in the outputs table
    for sysname in result[scenario][timestep][run]["outputs"].index.get_level_values("sink").unique().dropna().to_list():
        sys = result[scenario][timestep][run]["systems"][sysname]

        for el in elements:
            print(f"system: {sysname}, element: {el}")
            # use the produced input as input
            input = result[scenario][timestep][run]["outputs"].xs(sysname, level="sink")                    
            if input is not None:
                plot = input_output_plot(system=sys, sysname=sysname, input=input, element=el)
            else:
                plot = input_output_plot(system=sys, sysname=sysname, element=el)
            plots.append(plot)

plots


## maybe combine the values for different systems first??
from tools import add_system, remove_source
from declarations import Emissions


# systems to assess
OutputPerSys = result[scenario][timestep][run]["outputs"].copy()

SysN = OutputPerSys.dropna(axis="index", subset="N").index.get_level_values("sink").unique().dropna().to_list()
SysC = OutputPerSys.dropna(axis="index", subset="orgC").index.get_level_values("sink").unique().dropna().to_list()

## for nitrogen
timestep = "t2"
run = 0

Ndat = pd.DataFrame()

for sysname in SysN:

    # NOTE: for testing
    # sysname = "production.plant.crop.ArableLand_1"


    print(f"\ncreating data for input/output plot for {sysname} \n")
    system = result[scenario][timestep][run]["systems"][sysname]

    # result[scenario][timestep][run].keys()

    print("Calc output")
    # Output = system.output()
    Output = result[scenario][timestep][run]["outputs"].xs(sysname, level="source").droplevel("sink")
    remove_source(Output)

    print("Calc input")
    # Input = system.input()
    # NOTE: Input is taken from table, because input function returns input without properties
    Input = result[scenario][timestep][run]["outputs"].xs(sysname, level="sink").droplevel("source")            

    print("Calc emissions")
    Emis = system.emissions()

    ProdOut = Output["Q"] * Output["N"]
    ProdIn = Input["Q"] * Input["N"]

    if Emis is not None:
        Emis = Emis.loc[:,[s for s in Emis.columns.tolist() if "-N" in s]].sum()
    else:
        Emis = pd.Series(dtype="float64")

    negEmis = Emis[Emis < 0]
    posEmis = Emis[Emis > 0]

    Out = pd.DataFrame(pd.concat([ProdOut, posEmis]))
    Out = Out.reset_index(level=0)

    Out.columns = ["item", "value"]
    Out["category"] = "Output"

    negEmis = negEmis*-1
    In = pd.DataFrame(pd.concat([ProdIn, negEmis]))

    In = In.reset_index(level=0)
    In.columns = ["item", "value"]
    In["category"] = "Input"

    dat = pd.concat([In, Out])

    dat["value"] = pd.to_numeric(dat["value"], downcast="float")
    dat["system"] = sysname
    dat["element"] = "nitrogen"

    Ndat = pd.concat([Ndat, dat])



    # ProdIn = Input["Q"]*Input["DM"]*Input["oDM"]*Input["orgC"]*1000
    # ProdOut = Output["Q"]*Output["DM"]*Output["oDM"]*Output["orgC"]*1000



    plot = (
    ggplot(Ndat, aes(x='category', y='value', fill='item'))
    + geom_col()
    + facet_wrap("system")
    # + labs(title=sysname, subtitle=infotext, y=ylabtext, x=infotext)
    # + annotate('text', x=0.5, y=0, label=infotext)
    + theme_minimal()
    + theme(panel_grid_major_x = element_blank(),
            panel_grid_major_y = element_blank(),
            )
    )













### Plot 1 (old) ###########################################
### The different gases as facets


scenarios = ["biogas", "storage"]
timesteps = ["t1", "t2"]
Emis_scen = pd.DataFrame()

for scen in scenarios:
    for t in timesteps:
        emis = result[scen][t][0]["emissions"] # unconverted emissions (e.g. CO2-C)
        emis = result[scen][t]["emissions"].sel(run=0).to_pandas() # converted emissions (e.g. CO2)

        emis["item"] = emis.index.get_level_values("item")
        emis_melt = emis.melt(id_vars=["item"], var_name="molecule")
        emis_melt["timestep"] = t
        emis_melt["scenario"] = scen
        emis_melt = emis_melt.dropna(axis="index")
        Emis_scen = pd.concat([Emis_scen, emis_melt])


## subset only for timestep t2
Emis_scen_t2 = Emis_scen.loc[Emis_scen["timestep"] == "t2"]

(ggplot(Emis_scen_t2, aes(x='scenario', y='value', fill='item'))
    + geom_col()
    + facet_wrap("molecule", scales="free_y")
    #+ labs(title=sysname, subtitle=infotext, y=ylabtext, x=infotext)
    # + annotate('text', x=0.5, y=0, label=infotext)
    + theme(panel_grid_major_x = element_blank(),
            panel_grid_major_y = element_blank(),
            subplots_adjust={'wspace':0.8, 'right': 0.6}
            )
)






### NOTE: almost all emissions are lower in the biogas scenario. Something seems to be wrong here.



### A sankey diagram of the nitrogen flows: ###


## a test without real figures 
import plotly.graph_objects as go
import numpy as np

import start
from analysis import read_pickle
result = read_pickle("model_description_paper\\2022-06-09_result.pkl")

Out = result["storage"]["t2"][0]["outputs"]
OutN = Out["Q"] * Out["N"]
OutN = OutN.dropna()
OutN = OutN.reset_index(name="value")



# where sink is NaN replace with item
OutN.loc[OutN["sink"].isna(), "sink"] = OutN.loc[OutN["sink"].isna(), "item"]

# manually add the sink for dairy manure
# NOTE: Needs to be checked individually whether correct
OutN.loc[4, "sink"] = "material.storing.ManureStorage_1"



# first, I need to assign an individual id to all 
SinkSys = OutN["sink"].to_list()
SourceSys = OutN["source"].to_list()

Sys = np.unique(SinkSys + SourceSys)

SysIDs = dict(zip(Sys, range(len(Sys))))

OutN["source_id"] = [SysIDs[s] for s in OutN["source"].to_list()]
OutN["sink_id"] = [SysIDs[s] for s in OutN["sink"].to_list()]

OutN = OutN.sort_values(by=['source_id'])





## second version where I try to include real values:

import plotly.graph_objects as go

fig = go.Figure(go.Sankey(
    arrangement = "snap",
    node = {
        "label": list(SysIDs.keys()),
        # "x":     [0.1, 0.1, 0.1, 0.4, 0.6, 0.4, 0.5, 0.5, 0.8, 0.8, 0.5],
        # "y":     [0.1, 0.3, 0.7, 0.3, 0.5, 0.75, 0.01, 0.01, 0.45, 0.55, 0.85],
        'pad':10},  # 10 Pixels
    link = {
        "label": OutN["item"].to_list(),
        "source": OutN["source_id"].to_list(),
        "target": OutN["sink_id"].to_list(),
        "value":  OutN["value"].to_list()}))


# fig.show()
plot(fig)

fig.write_image("C:/Users/ukreidenweis/Desktop/fig1.svg")

fig.write_image("C:/Users/ukreidenweis/Desktop/carbon_flows_sankey.jpeg", width=800, height=500, scale=3)






## playground version:


fig = go.Figure(go.Sankey(
    arrangement = "snap",
    node = {
        "label": ["ArableLand 1", "Meadow", "ArableLand 2", "Dairy Farm", "Biogas Plant", "Silo", "CO2", "CH4", "CO2", "CH4", "CO2"],
        "x":     [0.1, 0.1, 0.1, 0.4, 0.6, 0.4, 0.5, 0.5, 0.8, 0.8, 0.5],
        "y":     [0.1, 0.3, 0.7, 0.3, 0.5, 0.75, 0.01, 0.01, 0.45, 0.55, 0.85],
        'pad':10},  # 10 Pixels
    link = {
        "source": [0, 1, 2, 5, 3, 4, 4, 3, 3, 4, 4, 5],
        "target": [3, 3, 5, 4, 4, 0, 1, 6, 7, 8, 9, 10],
        "value":  [0.6, 2, 0.8, 0.7, 1.5, 0.5, 0.7, 0.2, 0.1, 0.9, 0.1, 0.1]}))

plot(fig)
fig.show()

fig.write_image("C:/Users/ukreidenweis/Desktop/fig1.svg")

fig.write_image("C:/Users/ukreidenweis/Desktop/carbon_flows_sankey.jpeg", width=800, height=500, scale=3)





### plot example

import pandas as pd
import numpy as np
import plotly.graph_objs as go
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
# init_notebook_mode(connected=True)

# Original data
data = [['Source','Target','Value','Color','Node, Label','Link Color'],
    [0,5,20,'#F27420','Remain+No – 28','rgba(253, 227, 212, 0.5)'],
    [0,6,3,'#4994CE','Leave+No – 16','rgba(242, 116, 32, 1)'],
    [0,7,5,'#FABC13','Remain+Yes – 21','rgba(253, 227, 212, 0.5)'],
    [1,5,14,'#7FC241','Leave+Yes – 14','rgba(219, 233, 246, 0.5)'],
    [1,6,1,'#D3D3D3','Didn’t vote in at least one referendum – 21','rgba(73, 148, 206, 1)'],
    [1,7,1,'#8A5988','46 – No','rgba(219, 233, 246,0.5)'],
    [2,5,3,'#449E9E','39 – Yes','rgba(250, 188, 19, 1)'],
    [2,6,17,'#D3D3D3','14 – Don’t know / would not vote','rgba(250, 188, 19, 0.5)'],
    [2,7,2,'','','rgba(250, 188, 19, 0.5)'],
    [3,5,3,'','','rgba(127, 194, 65, 1)'],
    [3,6,9,'','','rgba(127, 194, 65, 0.5)'],
    [3,7,2,'','','rgba(127, 194, 65, 0.5)'],
    [4,5,5,'','','rgba(211, 211, 211, 0.5)'],
    [4,6,9,'','','rgba(211, 211, 211, 0.5)'],
    [4,7,8,'','','rgba(211, 211, 211, 0.5)']
    ]



headers = data.pop(0)
df = pd.DataFrame(data, columns = headers)
scottish_df = df

data_trace = dict(
    type='sankey',
    domain = dict(
      x =  [0,1],
      y =  [0,1]
    ),
    orientation = "h",
    valueformat = ".0f",
    node = dict(
      pad = 10,
      thickness = 30,
      line = dict(
        color = "black",
        width = 0
      ),
      label =  scottish_df['Node, Label'].dropna(axis=0, how='any'),
      color = scottish_df['Color']
    ),
    link = dict(
      source = scottish_df['Source'].dropna(axis=0, how='any'),
      target = scottish_df['Target'].dropna(axis=0, how='any'),
      value = scottish_df['Value'].dropna(axis=0, how='any'),
      color = scottish_df['Link Color'].dropna(axis=0, how='any'),
  )
)

layout =  dict(
    title = "Scottish Referendum Voters who now want Independence",
    height = 772,
    font = dict(
      size = 10
    ),    
)

fig = dict(data=[data_trace], layout=layout)
plot(fig, validate=False)







## CO2 eq emissions for the two scenarios and the time steps to see differences (if any)


### plot emissions
from plotnine import *
import pandas as pd

scenarios = ["biogas", "storage"]
timesteps = ["t1", "t2"]
gwp100emis = pd.DataFrame()

for scen in scenarios:
    for t in timesteps:
        r = result[scen][t]["gwp"].sel(run=0, gwp="GWP100").to_pandas()
        r["item"] = r.index.get_level_values("item")
        r_melt = r.melt(id_vars=["item"])
        r_melt["timestep"] = t
        r_melt["scenario"] = scen
        r_melt["molecule"] = r_melt['molecule'].astype(str)
        r_melt = r_melt.dropna(axis="index")
        gwp100emis = pd.concat([gwp100emis, r_melt])


gwp100emis


plot = (
ggplot(gwp100emis, aes(x='timestep', y='value', fill='item'))
+ geom_col()
+ facet_grid(facets="~scenario")
# + facet_wrap("variable", scales="free_y")
#+ labs(title=sysname, subtitle=infotext, y=ylabtext, x=infotext)
# + annotate('text', x=0.5, y=0, label=infotext)
+ theme_minimal()
+ theme(panel_grid_major_x = element_blank(),
        panel_grid_major_y = element_blank(),
        )
)
plot
### 




"""
## List of things that look really fishy
- CH4 emissions are much higher in the case of biogas. It should probably be the other way round
- There are very high negative CO2 emissions, probably from crop production. Either too high, or there should be equivalent positive emissions as well.

"""

## Does carbon accumulation in crops generally look ok?
import start
from tools import prod
from production.plant.crop import ArableLand

ArableLand(O=prod("winter wheat, grain", 1, "ton")).output()

ArableLand(O=prod("winter wheat, grain", 1, "ton")).emissions() # for one ton of wheat grains 440 kg of Corg fixation assumed. Seems ok.








## Alternative: Just the two scenarios for the moment:

# Milk quantity
milkQb = result["biogas"]["t1"][0]["systems"]['production.animal.ruminant.DairyFarm_1'].output().loc[("production.animal.ruminant.DairyFarm_1", "cow milk"),"Q"]

t1b = result["biogas"]["t1"]["gwp"].sel(simulation=0, gwp="GWP100").to_pandas()

# Adjust emissions to kg of milk
t1b = t1b/(milkQb * 1000) 
t1b["item"] = t1b.index.get_level_values("item")
t1b_melt = t1b.melt(id_vars=["item"])
t1b_melt["timestep"] = "t1"
t1b_melt["scenario"] = "biogas"
t1b_melt

milkQb = result["storage"]["t1"][0]["systems"]['production.animal.ruminant.DairyFarm_1'].output().loc[("production.animal.ruminant.DairyFarm_1", "cow milk"),"Q"]
t1s = result["storage"]["t1"]["gwp"].sel(simulation=0, gwp="GWP100").to_pandas()
t1s = t1s/(milkQb * 1000) # result per kg of milk
t1s["item"] = t1s.index.get_level_values("item")
t1s_melt = t1s.melt(id_vars=["item"])
t1s_melt["timestep"] = "t1"
t1s_melt["scenario"] = "storage"
t1s_melt

dat = pd.concat([t1b_melt, t1s_melt])


dat['molecule'] = dat['molecule'].astype(str)



plot = (
ggplot(dat, aes(x='scenario', y='value', fill='item'))
+ geom_col()
# + facet_wrap("molecule", scales="free_y")
+ facet_wrap("molecule")
#+ labs(title=sysname, subtitle=infotext, y=ylabtext, x=infotext)
# + annotate('text', x=0.5, y=0, label=infotext)
+ theme_minimal()
+ theme(panel_grid_major_x = element_blank(),
        panel_grid_major_y = element_blank(),
        )
)
plot


plot = (
ggplot(dat, aes(x='scenario', y='value', fill='item'))
+ geom_col()
# + facet_wrap("molecule", scales="free_y")
#+ labs(title=sysname, subtitle=infotext, y=ylabtext, x=infotext)
# + annotate('text', x=0.5, y=0, label=infotext)
+ theme_minimal()
+ theme(panel_grid_major_x = element_blank(),
        panel_grid_major_y = element_blank(),
        )
)
plot






biogasresult = start_simulation(biogasconfig)

for t in biogasresult["biogas"]["config"]["simulation"]["timesteps"]:
    print(f"timestep {t}")
    for i in biogasresult["biogas"][t][0]["systems"]:
        print(i)

biogasresult.to_pickle(filename="biogasresult.pkl")



result = start_simulation(storage_config)

for t in result["storage"]["config"]["simulation"]["timesteps"]:
    print(f"timestep {t}")
    for i in result["storage"][t][0]["systems"]:
        print(i)










#### Plots ####

### Plot of emissions per kg of product ###
## for the two time steps to see differences (if any)

### plot emissions
biogasresult["biogas"]["t1"][0]["emissions"]
biogasresult["biogas"]["t1"][0]["systems"]['material.biogas.BiogasPlant_1'].output()

from plotnine import *
import pandas as pd

t1 = result["biogas"]["t1"][0]["emissions"]
t1["item"] = t1.index.get_level_values("item")
t1_melt = t1.melt(id_vars=["item"])
t1_melt["timestep"] = "t1"
t1_melt

t2 = result["biogas"]["t2"][0]["emissions"]
t2["item"] = t2.index.get_level_values("item")
t2_melt = t2.melt(id_vars=["item"])
t2_melt["timestep"] = "t2"
t2_melt

dat = pd.concat([t1_melt, t2_melt])

dat['variable'] = dat['variable'].astype(str)


plot = (
ggplot(dat, aes(x='timestep', y='value', fill='item'))
+ geom_col()
+ facet_wrap("variable", scales="free_y")
#+ labs(title=sysname, subtitle=infotext, y=ylabtext, x=infotext)
# + annotate('text', x=0.5, y=0, label=infotext)
+ theme_minimal()
+ theme(panel_grid_major_x = element_blank(),
        panel_grid_major_y = element_blank(),
        )
)
plot






### 



### plot for CO2-eq emissions ###


t1 = biogasresult["biogas"]["t1"]["gwp"].loc[:,:,:,"GWP100"].to_pandas()

biogasresult["biogas"]["t1"]["gwp"].loc[:,:,0,"GWP100"]

biogasresult["biogas"]["t1"]["gwp"].sel(simulation=0, gwp="GWP100").to_pandas()




t1["item"] = t1.index.get_level_values("item")
t1_melt = t1.melt(id_vars=["item"])
t1_melt["timestep"] = "t1"
t1_melt

t2 = biogasresult["biogas"]["t2"][0]["emissions"]
t2["item"] = t2.index.get_level_values("item")
t2_melt = t2.melt(id_vars=["item"])
t2_melt["timestep"] = "t2"
t2_melt

dat = pd.concat([t1_melt, t2_melt])

dat['variable'] = dat['variable'].astype(str)



(
ggplot(dat, aes(x='timestep', y='value', fill='item'))
+ geom_col()
+ facet_wrap("variable", scales="free_y")
#+ labs(title=sysname, subtitle=infotext, y=ylabtext, x=infotext)
# + annotate('text', x=0.5, y=0, label=infotext)
+ theme_minimal()
+ theme(panel_grid_major_x = element_blank(),
        panel_grid_major_y = element_blank(),
        )
)


#############################################################################################################

### Check results ###




result["storage"]["t1"][0]["systems"]["production.plant.crop.ArableLand_1"].emissions()
result["storage"]["t1"][0]["systems"]["production.plant.crop.ArableLand_1"].output()
result["storage"]["t2"][0]["systems"]["production.plant.crop.ArableLand_1"].emissions()
result["storage"]["t1"][0]["systems"]["production.plant.crop.ArableLand_1"].input() # mineral fertilizer actually no input, but considered within crop. Bad to show N flows
result["storage"]["t2"][0]["systems"]["production.plant.crop.ArableLand_1"].input()



import start
from tools import prod
from production.plant.crop import ArableLand
from production.plant.fertilization import ManureSpreading, MineralFertilization

ArableLand(I = prod("broiler manure", 1, "ton")).emissions()

ManureSpreading(min_fert_savings=False).emissions()
MineralFertilization().emissions()






"""
## TODO's:
# Dairy Farm

# downstream_chain
- Modelling of time steps: The whole network form the first time step should be copied to the next time step. Only the things that need to be changed should be changed then
- Check whether actually two instances are created when one default module is called to produce different outputs
  Possibly a check is needed, whether the already existing module produces the respective output already (but needs to be for the correct class)
- Add function that adds outputs to simdict again, from calling the individual class instances?

ManureStorage:
-Add a storage term. How much is in the storage. At the beginning it should be 0, then the input should increase the storage


"""

result["dairy"]["t1"][0]["outputs"]



for t in result["biogas"]["config"]["simulation"]["timesteps"]:
    print(f"timestep {t}")
    for i in result["biogas"][t][0]["systems"]:
        print(i)
        # print("output: \n", result["dairy"][t][0]["systems"][i].output())


for t in result["dairy"]["config"]["simulation"]["timesteps"]:
    print(f"timestep {t}")
    for i in result["dairy"][t][0]["systems"]:
        print(i)




### Testing ###

from production.animal.ruminant import DairyFarm
from production.plant.crop import ArableLand


from dev_utils import input_output_plot
from production.plant.fertilization import MineralFertilizerProduction 


### There should be a difference between mineral fertilizer emissions between t1 and t2

result["biogas"]["t1"][0]["systems"]["production.plant.crop.ArableLand_1"].emissions()

result["biogas"]["t1"][0]["systems"]["production.plant.crop.ArableLand_1"].output()


result["biogas"]["t2"][0]["systems"]["production.plant.crop.ArableLand_1"].emissions()

result["biogas"]["t1"][0]["systems"]["production.plant.crop.ArableLand_1"].input() # mineral fertilizer actually no input, but considered within crop. Bad to show N flows
result["biogas"]["t2"][0]["systems"]["production.plant.crop.ArableLand_1"].input()

result["biogas"]["t1"][0]["systems"]["production.plant.fertilization.MineralFertilizerProduction_1000"].output()
result["biogas"]["t1"][0]["systems"]["production.plant.fertilization.MineralFertilizerProduction_1000"].emissions()

result["biogas"]["t1"][0]["systems"]["production.plant.fertilization.MineralFertilizerProduction_1001"].output()
result["biogas"]["t1"][0]["systems"]["production.plant.fertilization.MineralFertilizerProduction_1001"].emissions()



# 9 ton = 90 dt => 1 ha
ArableLand(O=prod("winter wheat, grain", 9, "ton"), avg_yield_dt_ha=90).input()

# calculated demand is 190 kg N / ha
# input is 0.6 ton of urea
# 
MineralFertilizerProduction().output()


ArableLand(O=prod("silage maize, whole", 9, "ton")).avg_yield_dt_ha
# NOTE: the average yield for silage maize is the same as for wheat!! Bad

ArableLand(O=prod("silage maize, whole", 9, "ton")).input()
# Therefore the input per hectar


ArableLand().avail_crops
ArableLand(O=prod("oat, grain", 10, "ton"), side_products=True).output()
ArableLand(O=prod("oat, grain", 10, "ton"), side_products=False).output()
ArableLand(O=prod("oat, straw", 10, "ton"), side_products=True).output() # quantity of the main product not calculated


# test humus balance
from tools import get_content
BroilerManure = get_content(prod("broiler manure", 10, "ton"), required="DM")

ArableLand(O=prod("silage maize, whole", 450, "ton")).humus_balance()

# test
ArableLand(O=prod("oat, grain", 55, "ton"), side_products=False).output()
ArableLand(O=prod("oat, straw", 55, "ton"), side_products=True).output()

ArableLand(O=prod("oat, grain", 55, "ton"), side_products=False).area_ha

ArableLand(O=prod("oat, grain", 55, "ton"), side_products=False)._products()

ArableLand(O=prod("oat, grain", 55, "ton"), side_products=False).humus_balance_crop_HEQ_per_ha()

ArableLand(I= BroilerManure, O=prod("winter wheat, grain", 10, "ton")).humus_balance_crop_HEQ_per_ha()

ArableLand().avail_crops
ArableLand(I= BroilerManure, O=prod('silage maize, whole', 55, "ton"), side_products=False).humus_balance_crop_HEQ_per_ha().sum()

ArableLand(O=prod("oat, grain", 55, "ton"), side_products=False).emissions()
ArableLand(O=prod("oat, grain", 55, "ton"), side_products=True).emissions()
## Think about this. If the straw is considered to be removed this only causes 4000 emissions in humus degradation, but stores 26000 CO2-C in straw

ArableLand(I= BroilerManure, O=prod("winter wheat, grain", 10, "ton")).emissions()


idx = ArableLand(O=prod("silage maize, whole", 450, "ton"))._idx
idx

ArableLand().avail_crops


ArableLand(O=prod("winter wheat, grain", 450, "ton"), side_products=False)._idx

# ArableLand(O=prod("winter wheat, grain", 450, "ton"), side_products=False)._item_element_indexer

idx = ArableLand(O=prod("winter wheat, grain", 10, "ton"))._idx
item_indexer = ArableLand(O=prod("winter wheat, grain", 10, "ton"), side_products=False)._item_indexer
item_element_indexer = ArableLand(O=prod("winter wheat, grain", 10, "ton"), side_products=False)._item_element_indexer
Products = ArableLand(O=prod("winter wheat, grain", 10, "ton"), side_products=False)._products()



Products = ArableLand(O=prod("winter wheat, grain", 450, "ton"), side_products=False)._products()



ArableLand_1 = ArableLand(O=prod("silage maize, whole", 450, "ton"))
ArableLand_1.area_ha
ArableLand_1.input() # 0.46 ton of fertilizer input, about 0.15 ton of N input
Out = ArableLand_1.output()
Out["Q"]*Out["N"] # 1.93 ton of N output
Fertilizer = MineralFertilizerProduction(O=ArableLand_1.input()).output()

input_output_plot(ArableLand_1, "ArableLand_1", input=Fertilizer, element = "nitrogen")

# example of wheat
ArableLand_2 = ArableLand(O=prod("winter wheat, grain", 80, "ton"), Nmin=50)
ArableLand_2.input()
ArableLand_2.area_ha # 1 ha for 80 tons
Fertilizer_2 = MineralFertilizerProduction(O=ArableLand_2.input()).output()




Crop = ArableLand_2.output()
ArableLand_2.avg_yield_dt_ha
idx = ArableLand_2._idx


ArableLand_2.emissions()

input_output_plot(ArableLand_2, "ArableLand_2", input=Fertilizer_2, element = "nitrogen")


##
from material.storing import ManureStorage

Instance = ManureStorage(timestep_transfer=2)
Instance = ManureStorage()

hasattr(Instance, "timestep_transfer")

Instance.__getattribute__("timestep_transfer")

from material.biogas import BiogasPlant
Instance = BiogasPlant()
hasattr(Instance, "timestep_transfer")

Instance.__getattr__("timestep_transfer")


# calculated demand is 190 kg N / ha
# input is 0.6 ton of urea
# 
from production.plant.fertilization import MineralFertilizerProduction 
MineralFertilizerProduction().output()



result.input_output_plots_pdf(filename="2022-06-07_result_in_out_C.pdf", path="C:/Users/ukreidenweis/Documents/biorim/simulation/model_description_paper", elements="carbon")



# barley, grain has no oDM value
result["biogas"]["t2"][0]["outputs"].loc["production.plant.crop.ArableLand_1000"]


## The problem is that Meadow has no input
result["biogas"]["t2"][0]["outputs"].xs(sysname, level="sink")

# maybe rather only go through the systems listed in the output table?

result["biogas"]["t2"][0]["outputs"].index.get_level_values("sink").unique().dropna().to_list()



result["biogas"]["t2"][0]["systems"]["production.plant.crop.ArableLand_1000"].input()
result["biogas"]["t2"][0]["systems"]["production.plant.crop.ArableLand_1000"].I

## The problem is: This is what is requested, not what is passed.

## MineralFertilizerProduction missing N column
from production.plant.fertilization import MineralFertilizerProduction

Input = MineralFertilizerProduction().output()
Input["Q"] * Input["N"]


## ArableLand is probably missing oDM
from production.plant.crop import ArableLand
from tools import prod
ArableLand(O=prod("winter wheat, grain", 1, "ton")).output()

ArableLand(O=prod("silage maize, whole", 500, "ton")).output()