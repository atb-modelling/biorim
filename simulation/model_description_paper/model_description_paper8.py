##################################################################################
# Configuation files for the paper
# A modular framework to assess biological resource utilization impacts (BIORIM)
##################################################################################

import start
from tools import prod
from analysis.timestep_chains import start_simulation

storageconfig = {
# configs = {
    ### Main settings
    "simulation"   :{
        "name": "storage", 
        "type": "single", #"all combinations"
        "starting_system" : "production.plant.crop.ArableLand_1", 
        "timesteps": ["t1","t2"]
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
    },

    "systems"   :{
        
        # produces the barley for the cows
        "production.plant.crop.ArableLand_1" :{
            'I': {"t1": None,
                  "t2": prod(('material.storing.ManureStorage_1', 'cow manure, liquid'), 100, "%")},
            'O': prod("silage maize, whole", 1500, "ton"), #  check whether with higher production of silage maize there is still mineral fertilization in second timestep,
            },

        # TODO: Currently no emissions from meadow production
        # produces the grass for the cows
        "production.plant.grass.Meadow_1" :{
            'O': prod("grass", 800, "ton")
            },

        # TODO:
        # Output quantities need to be realistic
        # Manure contents not calculated based on inputs
        # Emissions from enteric fermentation
        "production.animal.ruminant.DairyFarm_1" :{
            'I' : prod([("production.plant.crop.ArableLand_1","silage maize, whole"),
                        ("production.plant.grass.Meadow_1","grass")], 
                        [100, 100],
                        ["%","%"]),
            'n': 100, #[10,100] # NOTE: TO check whether 100 cows require 10 times the quantities as they should
            'enteric_ferment_method' : "Piatkowski_2021", ## NOTE: Not yet for storage. Check whether not other can be used.
            'co2_respiration_method': 'difference' #"Kinsman_1995"
            },

        ## TODO: Emission calculations not adapted to liquid cow manure yet
        "material.storing.ManureStorage_1" :{
            'I': {"t1": prod(('production.animal.ruminant.DairyFarm_1', 'cow manure, liquid'), 100, "%"),
                 "t2": prod(('production.animal.ruminant.DairyFarm_1', 'cow manure, liquid'), 100, "%")},
                # "t2": None},
            'method': "EEA2016", 
            'timestep_transfer': 1
            },

        "external.ExternOutput_1" :{
            'I': prod(('production.animal.ruminant.DairyFarm_1', 'cow milk'), 100, "%")
            },
        
        "external.ExternOutput_2" :{
                'I': prod(('production.animal.ruminant.DairyFarm_1', 'dairy cow'), 100, "%")
                },

        "external.ExternOutput_3" :{
                'I': prod(('production.animal.ruminant.DairyFarm_1', 'calf, newborn'), 100, "%")
                }

    }
}

# result = start_simulation(configs)
# result = start_simulation(storageconfig)



biogasconfig = {
    ### Main settings
    "simulation"   :{
        "name": "biogas", 
        "type":  "single", # "all combinations",
        "starting_system" : "production.plant.crop.ArableLand_1",
        "timesteps": ["t1","t2"]
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [20, 100]
        },
    },

    "systems"   :{
        
        # TODO: Check emissions. What about machinery usage? What about soil carbon? VDLUFA: Negative balance of humus?
        # Currently only emissions from manure considered. No negative emissions from humus formation from crop residues.
        # Consider whether it makes sense to exclude the ManureSpreading from the ArableLand class again, and have it as a seperate module.
        "production.plant.crop.ArableLand_1" :{
            'I': {"t1": None,
                  "t2": prod(('material.storing.DigestateStorage_1', 'biogas digestate'), 100, "%")},
            'O': prod("silage maize, whole", 1500, "ton"),
                },

        # TODO: Currently no emissions from meadow production
        # produces the grass for the cows
        "production.plant.grass.Meadow_1" :{
            'O': prod("grass", 800, "ton")
            },

        "production.animal.ruminant.DairyFarm_1" :{
            'I' : prod([("production.plant.crop.ArableLand_1","silage maize, whole"),
                        ("production.plant.grass.Meadow_1","grass")], 
                        [100, 100],
                        ["%","%"]),
            'n': 100, #[10,100]
            'enteric_ferment_method' : ["Piatkowski_2021", "IPCC2019_Tier1", "Kirchgeßner_1994"],
            'co2_respiration_method': 'difference' #"Kinsman_1995"
                },

        "material.biogas.BiogasPlant_1" :{
            'I': prod(('production.animal.ruminant.DairyFarm_1', 'cow manure, liquid'), 100, "%"),
            'residue_storage_type': ["closed", "open"],
            'thermal_energy_use': 0.4,
            'CH4_loss_conv_method': "KTBL"
            },

        "material.storing.DigestateStorage_1" :{
            'I': {"t1": prod(('material.biogas.BiogasPlant_1', 'biogas digestate'), 100, "%"),
                  "t2": prod(('material.biogas.BiogasPlant_1', 'biogas digestate'), 100, "%")},
            'timestep_transfer': 1
            },

        "external.Electricity_1" :{
                'I': prod(('material.biogas.BiogasPlant_1', 'electrical energy'), 100, "%")
                },

        "external.Heating_1" :{
                'I': prod(('material.biogas.BiogasPlant_1', 'thermal energy'), 100, "%")
                },

        "external.ExternOutput_1" :{
                'I': prod(('production.animal.ruminant.DairyFarm_1', 'cow milk'), 100, "%")
                },
        
        "external.ExternOutput_2" :{
                'I': prod(('production.animal.ruminant.DairyFarm_1', 'dairy cow'), 100, "%")
                },

        "external.ExternOutput_3" :{
                'I': prod(('production.animal.ruminant.DairyFarm_1', 'calf, newborn'), 100, "%")
                }
    }
}
# result = start_simulation([biogasconfig])


result = start_simulation([storageconfig, biogasconfig])

result.to_folder(subfolder="model_description_paper", time_as_folder=True)


"""
## Remaining TODO for paper:

+ ArableLand
- Check whether emission calculation is meaningful
  Especially humus balance with input of manure!!!!
- Possibly implement machinery use emissions

+ DairyFarm
 - Maybe include at least some electricity consumption
 - Decide for one enteric fermentation method: three available. Check their differences. Describe correctly in the text.



## obvious issues with results:
- high N input for silage maize production, comparatively low output. Maybe realistic. Leaching missing.
- N output of dairy farm higher than input, but N in cows still missing
- N input by mineral fertilization seems too low
- realistic relation between emissions from biogas and storage?


## Figures
1. Sankey diagrams for C or N flows
2. Barcharts emissions over time
3. Sensitivity x-y plot


## Sensitivities to assess:
 - Manure content calculated after assumed emissions, or emissions calculated after assumed manure content



"""

# high N input for silage maize production, comparatively low output. Maybe realistic. Leaching missing.


import start
from tools import prod
from production.animal.ruminant import DairyFarm
from production.plant.crop import ArableLand

Maize = ArableLand(O=prod("silage maize, whole", 1, "ton")).output()
Input = ArableLand(O=prod("silage maize, whole", 1, "ton")).input()

# At this point not possible to see input
Maize = result["storage"]["t2"][0]["systems"]['production.plant.crop.ArableLand_1'].output()
Input = result["storage"]["t2"][0]["systems"]['production.plant.crop.ArableLand_1'].input()     # Here the N still not specified


Fert = result["storage"]["t2"][0]["outputs"].xs("production.plant.crop.ArableLand_1", level="sink")

Maize["Q"] * Maize["N"] # 6450 kg N output

(Fert["Q"] * Fert["N"]).sum() # 7204 kg of N input. Seems ok


## There seems to be a difference between N input into manure storage and output + emissions
# for storage

ManureIn = result["storage"]["t2"][0]["outputs"].xs('material.storing.ManureStorage_1', level="sink")

ManureIn["Q"] * ManureIn["N"]   # 5784 kg N


ManureOut = result["storage"]["t2"][0]["outputs"].xs('material.storing.ManureStorage_1', level="source")

ManureOut["Q"] * ManureOut["N"]   # 4180 kg N

ManureEmis = result["storage"]["t2"][0]["emissions"].loc[[("material.storing.ManureStorage_1", "cow manure, liquid")],:]

ManureEmis.loc[:,["NH3-N", "N2O-N", "NH3-N & NOx-N", "NO-N", "N2-N"]].sum(axis=1)   # 1604, so matches the input. Just that N2 is not shown



ureIn["Q"]


result["storage"]["t2"][0]["systems"]['material.storing.ManureStorage_1'].input()




Input


DairyFarm().emissions()





result["biogas"]["t2"][0]["emissions"] # unconverted emissions, e.g. CO2-C

result["biogas"]["emissions"].coords["molecule"]

## Does this make sense to convert NOx & NH3 emissions to N2O indirect here already??
# Should this not only happen for the 

result["biogas"]["emissions"]
result["biogas"]["gwp"]


result["biogas"].keys()
result["biogas"]["emissions"]

result["biogas"]["emissions"].loc["t1", 0].to_pandas() # Check whether this is not a double accounting for N2O indirect and NOx + NH3?
result["biogas"]["gwp"]



result["storage"]["t1"]

result["storage"]["t1"].keys()
result["storage"]["t1"]["SourceToSink"]
result["storage"]["t1"][0]["outputs"].loc[:,["DM", "Q", "U"]]
result["storage"]["t2"][0].keys()

# outputs that are already available in "t2" from "t1"
result["storage"]["t2"][0]["outputs"].loc[:,["DM", "Q", "source"]]
result["storage"]["t2"][0]["outputs"].columns
result["storage"]["t2"][0]["outputs"].loc[:,["source"]] # source columns could actually be deleted

result["storage"]["t1"][0]["emissions"] # unconverted emissions, e.g. CO2-C; NOTE: Currently no emissions from the dairy farm

result["storage"]["emissions"].loc["t1", 0].to_pandas() # NOTE: Why is ManureStorage here in the first line?? Is it executed first? In ArableLand "cow manure" emissions listed, but with 0
result["storage"]["emissions"].loc["t2", 0].to_pandas() # 


# Where does the problem actually occur?
result["storage"]["t2"]["SourceToSink"]


## changes to the model:

"""
Missing to TODO
- Rapeseed / Mineral fertilizer not called. Additional input may be missing??

- Upstream modelling for I defined. This should be the case when the BiogasPlant is defined as the starting system and then calls DairyFarm (anyhow not working!)

"""






## Test new implementation of N contents in dairy manure
import start
from production.animal.ruminant import DairyFarm

DairyFarm().output()    # some internal functions are called way to often. They should be cached

DairyFarm().input()    # some internal functions are called way to often. They should be cached

DairyFarm(n=20).input()    # some internal functions are called way to often. They should be cached
DairyFarm(n=100).input()    # some internal functions are called way to often. They should be cached


DairyFarm().N_excretion_kg
DairyFarm().emissions()


## Check new implementation of carbon from arable
import start
from production.plant.crop import ArableLand
from production.animal.ruminant import DairyFarm

Manure = DairyFarm().output().loc[["cow manure, liquid"], :]
Manure["Q"] = 1
ArableLand().emissions()

ArableLand(I=Manure).emissions()

from material.biogas import BiogasPlant
BiogasPlant(I=Manure).emissions()

from material.storing import ManureStorage
ManureStorage(I=Manure).emissions()


### Test of the individual scenarios ###

## Storage scenario ##

storageresult = start_simulation(storageconfig)



storageresult["storage"]["t1"][0]["outputs"].loc[:,["Q", "U"]]
storageresult["storage"]["t2"][0]["outputs"].loc[:,["Q", "U"]]

storageresult["storage"]["t1"]["parameters"] # run 0 with 100  tons of grass, run 1 with 2000

storageresult["storage"]["t1"][0]["outputs"].loc[:,["Q", "U"]] # 430 barley grain; 100 rapeseed 
storageresult["storage"]["t1"][1]["outputs"].loc[:,["Q", "U"]] # 117 barley grain; no rapeseed



## Biogas scenario ##
biogasresult = start_simulation(biogasconfig)

biogasresult["biogas"]["t1"][0]["outputs"].loc[:,["Q", "U"]]
biogasresult["biogas"]["t1"][0]["outputs"].loc[:,["Q", "U"]]


# Check the emissions

# Where have the CO2 emissions from the DairyFarm gone?

result["storage"]["emissions"].loc["t2", 0].to_pandas() # value is NaN. Why??








