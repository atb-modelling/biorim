### simulation that minimizes the costs of production


## rather create a very simple model first
## all completely made up values


from tools import prod
import pandas as pd

class ArableLandTest:

    def __init__(self, O=prod("wheat", 1, "ton"), fertilization_kg_ha = 50):
        self.O = O
        self.fertilization_kg_ha = fertilization_kg_ha

    def output(self):
        # arbitrary linear function
        Output = self.O

        if self.O.index == "wheat":
            N_content = 10 + self.fertilization_kg_ha /10
            Output["N"] = N_content
        elif self.O.index == "barley":
            N_content = 8 + self.fertilization_kg_ha /12
            Output["N"] = N_content

        return Output

    def costs(self):
        
        if self.O.index == "wheat":
            FIXED_COSTS = 50  * self.O["Q"][0]
            FertilizationCosts = self.fertilization_kg_ha * 0.5 * self.O["Q"][0]

            Costs = pd.DataFrame()
            Costs.loc["Fixed", "€"] = FIXED_COSTS
            Costs.loc["Fertilization", "€"] = FertilizationCosts

        elif self.O.index == "barley":
            FIXED_COSTS = 50  * self.O["Q"][0]
            FertilizationCosts = self.fertilization_kg_ha * 0.5 * self.O["Q"][0]

            Costs = pd.DataFrame()
            Costs.loc["Fixed", "€"] = FIXED_COSTS
            Costs.loc["Fertilization", "€"] = FertilizationCosts

        return Costs



ArableLandTest(O=prod("wheat", 1, "ton"), fertilization_kg_ha=50).costs()
ArableLandTest(O=prod("wheat", 1, "ton"), fertilization_kg_ha=100).costs()
ArableLandTest(O=prod("wheat", 1, "ton"), fertilization_kg_ha=150).costs()




ArableLandTest(O=prod("barley", 1, "ton"), fertilization_kg_ha=30).costs()
ArableLandTest(O=prod("barley", 1, "ton"), fertilization_kg_ha=60).costs()
ArableLandTest(O=prod("barley", 1, "ton"), fertilization_kg_ha=100).costs()



# The objective function should actually be something like:

# maximize returns

# returns are defined as: wheat production * price - costs


