## example for presentation
import pandas as pd
import numpy as np
from themodel import convert_unit

# broiler farm
production.animal.poultry.BroilerFarm(Q=40000).input()
production.animal.poultry.BroilerFarm(Q=40000).output()
production.animal.poultry.BroilerFarm(Q=40000, fattening_system="short").output()


# biogas plant
Substrate = pd.DataFrame({'Q': [1000,9000], 'U': ["kg", "kg"]}, index=['broiler manure', 'maize silage'])

material.biogas.biogas_plant(I=Substrate).input()

material.biogas.biogas_plant(I=Substrate).emissions()

# emissions along the production chain
chain_emis(material.biogas.biogas_plant, I=Substrate)



#############


from themodel.read import *
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np


# get data
LAF = read.Landtag_BB.large_animal_farms()
poultry = LAF.loc[LAF['Typ'] == "Mastgeflügel"]
NUTS3 = read.Eurostat.NUTS3()
NUTS3.set_index(["NUTS_ID"], inplace=True)
broilerN = poultry.groupby(["NUTS_ID"]).sum()

# apply the function
broilerN["manure N"] = broilerN.apply(lambda x: production.animal.poultry.BroilerFarm(Q=int(x)).output().loc["broiler manure", "N"], axis=1)*7


broilerN3 = NUTS3.join(broilerN, how="right")

# plot
broilerN3.plot(column="manure N", cmap="YlOrRd", legend=True)
plt.title("N from broiler manure [kg]")
plt.show()


###

