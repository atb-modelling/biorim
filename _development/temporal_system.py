### Example of how a system may look like that considers several time steps

import start
import pandas as pd
from tools import prod

class BiogasPlantExample:

    EMISSION_FACTOR = 0.01
    OUTPUT_FACTOR = 0.8

    def __init__(self, I = prod("maize", 10 ,"ton"), max_input=100):
        self.max_input = max_input
        self.I = I
        # self.input_t = {}

    # def provide_input(self, I = prod("maize", 10 ,"ton")):
    #     self.I = I

    def additional_input(self):
        return prod("lubricant", self.I["Q"][0] * 0.05, "ton")

    def input(self):
        return pd.concat([self.I, self.additional_input()])

    def set_input_t(self, t):
        if not hasattr(self, "input_t"):
            self.input_t = {}
        self.input_t[t] = self.I

    def emissions(self):
        if self.I is not None:
            return self.I["Q"][0] * self.EMISSION_FACTOR
        else:
            print("No input defined")

    def set_emissions_t(self, t):
        if not hasattr(self, "emissions_t"):
            self.emissions_t = {}
        self.emissions_t[t] = self.emissions()

    def output(self):
        if self.I is not None:
            return prod("digestate", self.I["Q"][0] * self.OUTPUT_FACTOR, "ton") 
        else:
            print("No input defined")

    def set_output_t(self, t):
        if not hasattr(self, "output_t"):
            self.output_t = {}
        self.output_t[t] = self.output()


## Is it possible to just write an argument to a class??


B1 = BiogasPlantExample(I = prod("maize", 10 ,"ton"))
B1.set_input_t(t = "t1")

B1.input_t





## way to overwrite agruments

vars(B1)

newargs = {'max_input': 100, 'I': prod(('.', 'wheat'), 100, "ton")}

for key, value in newargs.items():
    print(key, ":", value)
    setattr(B1, key, value)

B1.set_input_t(t = "t2")

B1.input_t

B1.input_t = {"t1": newargs}


## How would a function look like that takes a dict of temporal values and hands it over to a class?
## example for the BioegasPlantExample above

I = {"t1": prod(('cow manure, liquid'), 100, "ton"),
     "t2": prod(('broiler manure'), 1, "ton")}

## currently the temporal dictionary is not for all arguments but only for single ones

system = BiogasPlantExample(max_input=500)

def temp_I(system, I):
    for I_t in I.items():
        system.I = I_t[1]
        system.set_input_t(t=I_t[0])
        system.set_output_t(t=I_t[0])
        system.set_emissions_t(t=I_t[0])
    return system

B1 = temp_I(BiogasPlantExample(max_input=500), I = I)

B1.input()
B1.input_t
B1.output_t
B1.emissions_t




for I_t in I.items():
    print(I_t[0])




