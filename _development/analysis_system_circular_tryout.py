### toy example for a new system to analyse production systems
## only work with functions that are currently not in use for the broiler farm, to not destroy them

# import sys
# sys.path.append("C:/Users/ukreidenweis/Documents/themodel")
# import analysis, external, material, production, read, simulation, tools, transport


import start

import pandas as pd

from declarations import Product
from tools import prod

#from dataclasses import asdict

## the following example

# maize from to areas
# one going into cow production
# one going directly into biogas
# digestate is send back to maize


##################################################################
#### define a simconfig that takes advantage of this new dataclass ##


simconfig = {
    ### Main settings
    "simulation"   :{
        "name": "circular dairy system",
        "type": "all combinations",
        "reference_system" : "production.animal.ruminant.DairyFarm_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [10,20,50,100]
        },
    },

    ### Object parameterization
    "systems"   :{
        "production.plant.crop.ArableLand_1":{
            "I" : prod(('material.biogas.BiogasPlant_1','biogas digestate')),    ## amounts to be defined by biogas_plant_1
            "O" : prod("barley, grain")                                   ## amounts to be defined by dairy farm later on
        },

        "production.plant.crop.ArableLand_2":{
            "I" : None,
            "O" : prod("winter wheat, grain")
        },

        "production.animal.ruminant.DairyFarm_1":{
            "I" : prod([
                   (('production.plant.crop.ArableLand_2',"winter wheat, grain")),
                   (('production.plant.crop.ArableLand_1',"barley, grain"))
                   ]),    ## production of winter wheat should be added automatically
            "O" : prod("cow milk, fresh", 10, "ton")
        },

        "material.biogas.BiogasPlant_1":{
            "I" : prod([(('production.animal.ruminant.DairyFarm_1',"broiler manure")),
                   (('production.plant.crop.ArableLand_1',"barley, grain")), 10, "ton"]),
            "O" : prod("biogas digestate")
            },
    }
}


# First need to define what the system should really look like. What does it have to do?

"""
# Good would be the following:
# I define different farms (fields with production)
# Steps that are executed:
1. First the network is established: All production systems are initialized (potentially with default values)
2. Costs are computed for all production systems
3. According to cost minimization the production system is determined (fulfilling all requirements for protein, energy, etc.)
4. 

# Requirements:
1. Costs for crop production should differ at different locations



# To think about:
- What do I achieve with this? What research questions will I be able to answer after this has been implemented?


"""




#### The stepts to execute the config:
# in try to fill the quantities in reverse order.
# to this end identify the object where the output is quantified. This is the "starting point" for the reverse quantification
## reference_fun is the function whose output is taken as reference point (similar to reference product concept)
# collect all Classes that need to be executed in the simulation
# collect all Inputs that are required

from analysis.downstream_chains import start_simulation


start_simulation(simconfig)














class_instance = "production.animal.ruminant.DairyFarm_1"

## NOTE: This is still not going into full depth, as the input_config_and_class function does evaluate only one level below

def input_config_and_class(class_instance):
    print(f"Calculate required inputs for {class_instance}")

    param = simconfig["objects"][class_instance].copy() # parameters according to config
    del param["I"]

    # import the respective module
    import_module(class_instance.rsplit(".",1)[0])

    # read the from_systems from the config
    Input_System_Config = simconfig["objects"][class_instance]["I"] # the info from the simconfig
    if Input_System_Config is not None:
        Input_System_Config = Input_System_Config.drop(columns=['Q','U'])

    # calculate the required inputs for the function given the arguments
    # this is everything that is actually needed
    Needed_Inputs = eval(class_instance.rsplit("_",1)[0])(**param).input() ## needed inputs of DairyFarm: amounts currently not dependant on contents of input. If they were, take defaults as start

    # now join the specified from_systems from the config to the actually required inputs
    if Needed_Inputs is not None:
        Needed_Inputs = Needed_Inputs.join(Input_System_Config, how="left")
        Needed_Inputs["to_system"] = class_instance
        Needed_Inputs = Needed_Inputs.set_index("to_system", append=True)
    # if now items are needed according to .input, but only due to config return those
    elif Input_System_Config is not None:
        Input_System_Config["to_system"] = class_instance
        Needed_Inputs = Input_System_Config.set_index("to_system", append=True)

    return Needed_Inputs



## Step 1: take the reference_cl (Dairy) and evaluate the inputs
simdata = {}
simdata["required_inputs"] = input_config_and_class(reference_cl)

## Step 2: evaluate the inputs from all class_instances that are now in the list of inputs

second_level_systems = needed_inputs.index.get_level_values(0).tolist()

for instance in second_level_systems:
    simdata["required_inputs"] = simdata["required_inputs"].append(input_config_and_class(instance), sort=False)


## Step 3: now do this for all the remaining class_instances

# not correct yet. biogas already mentioned but not executed
third_level_systems = simdata["required_inputs"].index.get_level_values(0).tolist()
remaining_systems = set(simconfig["objects"].keys()).difference(third_level_systems)

for instance in remaining_systems:
    simdata["required_inputs"] = simdata["required_inputs"].append(input_config_and_class(instance), sort=False)


simdata["required_inputs"]






pd.DataFrame(needed_inputs_config)

simconfig_info = simconfig_info.reset_index(level="system")
simconfig_info = simconfig_info.drop(columns=['Q','U'])





## way to add a multiindex level before:
pd.concat([needed_inputs], keys=['Foo'], names=['Firstlevel'])

needed_inputs

needed_inputs.index = simconfig["objects"][reference_cl]["I"].index




        if simconfig["objects"][reference_cl]["I"][system].index[0] == product:
            needed_inputs.loc[product,"from_system"] = system


simconfig["objects"][reference_cl]["I"].index

## for all others this would have to be filled with the default values

## now these input systems would have to be execueted and checked for their inputs





## write these input quantities to a modified simconfig
from copy import deepcopy
simconfig2 = deepcopy(simconfig)


## relation of product and production system
source_sys = pd.DataFrame()
for key, x in simconfig["objects"].items():
    if isinstance(x["I"], tuple):
        source_sys.loc[x["I"][1],"source"] = x["I"][0]

## actually, this should identify all needed inputs
# then I can create a list of all needed inputs and define which of these are executed first and in which quantity
# this should already add up production from one system that is needed for several




## the combinations of class parameters and class outputs/emissions should be written to a dictionary.
# when several runs are executed the function should first check whether this combination has not been calculated previously.
# If so, it should just take the results from the dict to save computing time.



## create a dataframe with the outputs

## and a dataframe or dict with information on the objects: are they dependant on other than external and crop?



### and so forth: now try to fill the missing values in backward direction.

## in the end execute all classes.




### first step: define all function so that the example is working
# cereal production

from production.plant import crop
crop(O = Out("maize silage")).output()

from production.animal.ruminant import DairyFarm
DairyFarm(O=Out("cow milk, fresh", Q=1, U="ton")).output()
DairyFarm(O=Out("cow milk, fresh", Q=1, U="ton")).input() ## need to define some sample input. e.g. "maize silage"and "winter wheat, grain"

from material.biogas import biogas_plant
manure = DairyFarm(O=Out("cow milk, fresh", Q=1, U="kg")).output().loc[["broiler manure"], :]
maize = crop(O=Out("maize silage"), Q=1, U=1).output()
substrate = pd.concat([manure, maize], axis=0, sort=False)
biogas_plant(I=substrate).output() ## DM caclulation of output doesn't seem to work for more than one substrate


pd.DataFrame()



###
DairyFarm().input()
biogas_plant().input()


### to dos:
# adapt the three functions to work with the new In and Out classes

I_example












## how the procedure would have to work:
# 1. execute crops
# 2. execute all functions that only need crops
# objects should only be initialized when there input is already defined

simdata = {}
simdata["simconfig"] = simconfig

simdata["time1"]

## execute crop functions without input

## execute all functions once to see their input needs
simdata["simconfig"]["objects"]["crop_1"]["O"]
simdata["simconfig"]["objects"]["biogas_plant_1"]["O"]

## the starting point has to be defined somewhere else, if there is some functional unit defined











I = ("broiler manure", 1, "ton", "DairyFarm_1")



I1 = ("broiler manure", 10, "ton")
I2 =  ("biogas digestate",1,"ton")
I3 = [("biogas digestate",1,"ton"), ("manure",1,"m³")]
