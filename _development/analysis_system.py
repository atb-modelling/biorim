## to initiate the model:
import sys
sys.path.append("C:/Users/ukreidenweis/Documents/themodel")
import analysis, external, material, production, read, simulation, tools, transport


import numpy as np
import pandas as pd
import warnings
import itertools
from copy import deepcopy
from importlib import import_module

from tools import prod
from declarations import Product
from analysis import simdict

##################################################################
"""
Important to do's:
- Include emissions accounting in all functions
- Enable to model different parameters
- 




"""
##################################################################
#### define a scenarioconfig that takes advantage of this new dataclass ##


## new scenarioconfig for the broiler manure case. First try this out with storage:

sampleconfig = {
    ### Main settings
    "simulation"   :{
        "name": "storage",
        "type": "all combinations",
        "reference_system" : "production.animal.poultry.BroilerFarm_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [10,20,50,100]
        },
    },

    "systems"   :{
        "production.animal.poultry.BroilerFarm_1" :{
                'O' : prod('broiler manure', 1, "ton"),
                'B0_source' : "KTBL",
                },

        "transport.LandTransport_1" :{
                'I': prod(('production.animal.poultry.BroilerFarm_1', 'broiler manure')),
                'dist_km': 5,
                'type': "tractor, small size"
                },

        "material.storing.ManureStorage_1" :{
                'I': prod(('transport.LandTransport_1', 'broiler manure')),
                'method' : "default",
                'time_period' : 168 # [35,168]
                },

        "transport.LandTransport_2" :{
                'I': prod(('material.storing.ManureStorage_1', 'broiler manure')),
                'dist_km': 10,
                'type': "lorry, medium size"
                },

        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod(('transport.LandTransport_2', 'broiler manure')),
                'method': "default",
                'type' : "solid",
                },
    }
}


###########################################################
## The example for the biogas plant
# This example is needed to assess 

biogasconfig = {
    ### Main settings
    "simulation"   :{
        "name": "biogas",
        "type": "all combinations",
        "reference_system" : "production.animal.poultry.BroilerFarm_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [10,20,50,100]
        },
    },

    "systems"   :{
        "production.animal.poultry.BroilerFarm_1" :{
                'O' : prod('broiler manure', 1, "ton"),
                'B0_source' : "KTBL",
                },

        "transport.LandTransport_1" :{
                'I': prod(('production.animal.poultry.BroilerFarm_1', 'broiler manure')),
                'dist_km': 5,
                'type': "tractor, small size"
                },

        "material.biogas.BiogasPlant_1" :{
                'I': prod(('transport.LandTransport_1', 'broiler manure')),
                'residue_storage_type': "closed",
                'CH4_loss_conv_method': "KTBL",
                },

        "transport.LandTransport_2" :{
                'I': prod(('material.biogas.BiogasPlant_1', 'biogas digestate')),
                'dist_km': 10,
                'type': "lorry, medium size"
                },

        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod(('transport.LandTransport_2', 'biogas digestate')),
                'method': "default",
                'type' : "solid",
                },

        "external.ExternOutput_1" :{
                'I': prod(('material.biogas.BiogasPlant_1', 'electrical energy'))
                },
    }
}

## In this case the reference class is the first in the row, therefore only downstream modelling is required (no inputs for the BroilerFarm defined).
# How to figure this out that there is not inputs required.

##########################################
# the storage example with different values

storageconfig = {
    ### Main settings
    "simulation"   :{
        "name": "storage",
        "type": "all combinations",
        "reference_system" : "production.animal.poultry.BroilerFarm_1"
    },

    ### Conducted analyses
    "analyses": {
        "emissions":{
            "outtype": "molecule"
        },
        "gwp":{
            "timerange": [10,20,50,100]
        },
    },

    "systems"   :{
        "production.animal.poultry.BroilerFarm_1" :{
                'O' : prod('broiler manure', 1, "ton"),
                'B0_source' : ["KTBL", "IPCC"],
                },

        "transport.LandTransport_1" :{
                'I': prod(('production.animal.poultry.BroilerFarm_1', 'broiler manure')),
                'dist_km': 5,
                'type': "tractor, small size"
                },

        "material.storing.ManureStorage_1" :{
                'I': prod(('transport.LandTransport_1', 'broiler manure')),
                'method' : "default",
                'time_period' : 168 # [35,168]
                },

        "transport.LandTransport_2" :{
                'I': prod(('material.storing.ManureStorage_1', 'broiler manure')),
                'dist_km': [10, 500],
                'type': "lorry, medium size"
                },

        "production.plant.fertilization.ManureSpreading_1" :{
                'I': prod(('transport.LandTransport_2', 'broiler manure')),
                'method': "default",
                'type' : "solid",
                },
    }
}



def sim_exec(simconfigs):
    
    def upstream_modelling_undefined(further_inputs):
        # iterate only over the different systems (not the list of items, because all items from one system are requested at the same time)
        for system in set(further_inputs.index.get_level_values("system")):

            O = further_inputs.xs(system, level="system", drop_level = False)

            print("upstream modelling for: ", system, O.index.get_level_values("item").to_list())

            Module = system.rsplit(".",1)[0]
            Class = system.rsplit("_",1)[0]

            import_module(Module)

            Instance = eval(Class)(O=O)
            Input = Instance.input()
            Output = Instance.output()
            Emis = Instance.emissions()

            simdata[scenario][run]["objects"][system] = Instance
            simdata[scenario][run]["outputs"] = simdata[scenario][run]["outputs"].append(Output)
            simdata[scenario][run]["emissions"] = simdata[scenario][run]["emissions"].append(Emis)

            if Input is not None:
                upstream_modelling_undefined(Input)


    def system_output(config, system, passed_input = None):
        Module = system.rsplit(".",1)[0]
        Class = system.rsplit("_",1)[0]
        ID = system.rsplit("_",1)[1]

        print(f"calculating outputs for {system}")

        import_module(Module)

        # parameters as defined in the config
        param = config["systems"][system].copy()

        # this is the case for the first system in the row:
        if passed_input is None:
            ## evaluate the system with the parameters from the config
            Instance = eval(Class)(id=ID, **param)

        # for all later systems that get passed some input
        else:            
            # remove I from the config parameters as it is defined here as the sum of I specified in the config and the passed input
            if "I" in config["systems"][system].keys():
                input_from_config = config["systems"][system]["I"]
                del param["I"]
            
            # first add all inputs from config which are not passed. NOTE: This means that all inputs from different chains have to be passed at the same time! Not guaranteed at the moment!
            I = Product()
            I = I.append(input_from_config.loc[input_from_config.index.difference(passed_input.index)])

            # then, add the passed input. NOTE: Later this could for instance add up numbers
            if np.isnan(input_from_config.loc[passed_input.index, "Q"])[0]:
                I = I.append(passed_input)
            else:
                warnings.warn("Cases where quantities of passed inputs are also defined currently not possible")

            # now evaluate the system with this I, and the other parameters from the config
            Instance = eval(Class)(I=I, id=ID, **param)

            # All inputs required by the instance
            AllInputs = Instance.input()

            ## Futher Inputs needed by the class, and not alread provided by the chain!
            FurtherInputs = AllInputs.loc[AllInputs.index.difference(passed_input.index)]

            # for all inputs that are not the ones that are already passed, go upstream
            if FurtherInputs is not None:
                upstream_modelling_undefined(further_inputs = FurtherInputs)

        Output = Instance.output()
        Emis = Instance.emissions()

        # write to simdata
        simdata[scenario][run]["outputs"] = simdata[scenario][run]["outputs"].append(Output)
        simdata[scenario][run]["emissions"] = simdata[scenario][run]["emissions"].append(Emis)
        
        simdata[scenario][run]["objects"][system] = Instance

        # data["objects"][system] = Instance
        
        return Output



    def downstream_modelling(config, passed_input):
        
        # function to select only outputs for which a pass on system is defined
        def output_to_pass(output):
            product_selector = list(set(FromSystemToSystem.keys()).intersection(output.index.to_list()))
            return output.loc[product_selector]

        # now select only the outputs that have to be passed further:
        outputs = output_to_pass(passed_input)

        # for all outputs now selected determine to which system they have to go:
        for out in outputs.itertuples():
            system = FromSystemToSystem[out.Index]

            print(f"downstream modelling for: {system} and {out.Index}")

            # this works as a lookup, because 'outputs' changes constantly
            # ProducedOutputs = data["all outputs"]
            ProducedOutputs = simdata[scenario][run]["outputs"]

            outputs = system_output(config = config, system=system, passed_input = ProducedOutputs.loc[[out.Index],:])
            
            if outputs is not None:
                downstream_modelling(config = config, passed_input=outputs)


    ## function to create a Pandas table of unique parameter combinations
    # if only_frist = True, take only the first of several parameters 

    def parameter_table(config, only_first = False): 

        systems = list(config["systems"].keys())

        # parameter lists for all systems
        prep_list = []
        for sys in systems:
            parameters = list(config["systems"][sys].values())
            if only_first:
                parameters = [[param] if not isinstance(param, list) else [param[0]] for param in parameters] # assure all single parameters are lists
            else:
                parameters = [[param] if not isinstance(param, list) else param for param in parameters] # assure all single parameters are lists
            prep_list.extend(parameters)

        # create unique combinations
        # iter_list = list(itertools.product(*prep_list)).copy()
        iter_list = list(itertools.product(*prep_list))

        # empty parameter table with correct dimensions
        ParamTable = pd.DataFrame()
        for sys in systems:
            ParamTable = ParamTable.append(pd.DataFrame(columns=list(range(len(iter_list))), index = pd.MultiIndex.from_product([[sys], list(config["systems"][sys].keys())], names=["system", "param"])), sort=False)

        # pass the values to the table
        for i, run_param in enumerate(iter_list):
            ParamTable.iloc[:,i] = run_param

        return ParamTable


    ##################################
    # where the modelling actually starts

    # define the output: simdata is the overall file comprising all scenarios
    simdata = simdict()

    if not isinstance(simconfigs, list):
        simconfigs = [simconfigs]

    # scenarios = [simconfig["simulation"]["name"] for simconfig in simconfigs] # list of scenario names, to iterate over (currently iteration over configs)

    for scenarioconfig in simconfigs:

        scenario = scenarioconfig["simulation"]["name"]
        
        # print(f"started modelling for scenario {scenario}")

        print(f"""
        ###########################################################################################################
        #                                                                                                         #
        #  Started modelling for scenario {scenario}
        #                                                                                                         #
        ###########################################################################################################
        """)

        # scendata is the data for one scenario
        # scenariodata = {}
        simdata[scenario] = {}
        simdata[scenario]["config"] = scenarioconfig

        if scenarioconfig["simulation"]["type"] == "single":
            simdata[scenario]["parameters"] = parameter_table(scenarioconfig, only_first = True)

        elif scenarioconfig["simulation"]["type"] == "all combinations":
            simdata[scenario]["parameters"] = parameter_table(scenarioconfig, only_first = False)

        else:
            raise ValueError(f'No option defined for {scenarioconfig["simulation"]["type"]}')

        ### several runs ###

        # for run in scenariodata["parameters"].columns.to_list():
        for run in simdata[scenario]["parameters"].columns.to_list():

            print(f"\nStarting calculations for run {run}\n")

            # rundata = {}
            simdata[scenario][run] = {}

            # rundata["all outputs"] = Product()
            # rundata["objects"] = {}
            simdata[scenario][run]["outputs"] = Product()
            simdata[scenario][run]["emissions"] = Product()
            simdata[scenario][run]["objects"] = {}

            # for each run create a config file again based on the parameters table created earlier
            # creating a list of configs in the first place would have been another option but this was chosen for legacy code reasons, and to get the overview table
            runconfig = deepcopy(scenarioconfig)

            for sys in scenarioconfig["systems"].keys():
                for param in scenarioconfig["systems"][sys].keys():
                    runconfig["systems"][sys][param] = simdata[scenario]["parameters"].loc[(sys,param),run]

            simdata[scenario][run]["config"] = deepcopy(runconfig)

            # dictionary defining where outputs have to be passed to
            # TODO: make this a function, and pass the result to the functions which need it?!
            FromSystemToSystem = {}
            for sys in runconfig["systems"]:
                if "I" in runconfig["systems"][sys].keys():
                    FromSystemToSystem[runconfig["systems"][sys]["I"].index[0]] = sys

            ## here the actual modelling starts
            # NOTE: for more complex cases this has to be modified. Currently it only allows for downstream modelling starting at the reference system

            # in the case of only downstream modelling start with the reference system
            reference_system = runconfig["simulation"]["reference_system"]
            outputs = system_output(config=runconfig, system=reference_system, passed_input = None)

            # pass the outputs from the reference modelling to the downstream modelling
            downstream_modelling(config=runconfig, passed_input=outputs)

            # scenariodata[run] = rundata.copy()

        # simdata[scenario] = scenariodata.copy()
        # simdata[scenario]["config"] = scenarioconfig.copy()

    return simdata


## try out:

simdata = sim_exec(sampleconfig)



simdata["storage"].keys()
# something went wrong here. There should only be the run here? Reconsider the structure

simdata["storage"][0]["outputs"] # seems ok

simdata["storage"][0]["objects"] # seems ok

simdata["storage"]["parameters"] # seems ok



configs = [storageconfig, biogasconfig]

simdata = sim_exec(configs)
# 
simdata["storage"]["parameters"]
simdata["biogas"]["parameters"]

simdata["storage"][0]["emissions"]
simdata["storage"][1]["emissions"]

simdata["storage"][0]["outputs"]
simdata["storage"][1]["outputs"]

simdata["biogas"][0]["emissions"] # wrong indexing

# when executed several times, it only computes the first run!!
storageres = sim_exec(storageconfig)

biogasres = sim_exec(biogasconfig)


storageres["storage"]["parameters"] # has only one dimension and calculations are only done for one of the runs
# iter_list seems already too short!!

simdata.keys()


###########################################

"""
# solve some of the problems:
1. write directly to simdata
2. different runs not considered (seems bug in parameter_table)
3. consider emissions. Emissions also need the add_system tag !





"""