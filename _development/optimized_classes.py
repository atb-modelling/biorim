### Examples of how I would define classes with the knowledge as of today


from functools import cached_property
import start
import pandas as pd
from tools import prod


# Exemplary storage class


# stk = pd.DataFrame({'Q': 100, 'U': "ton"} ,index=["silage maize"])
# I = pd.DataFrame({'Q': 50, 'U': "ton"} ,index=["wheat"])
# O = pd.DataFrame({'Q': 10, 'U': "ton"} ,index=["silage maize"])


class Storage:

    def __init__(self, initial_stk = pd.DataFrame({'Q': 100, 'U': "ton"} ,index=["silage maize"]), max_capacity=500):
        self.max_capacity = max_capacity
        self.stk = initial_stk

    def inp(self, I):
        """
        |  The process of putting something into the storage
        """
        NewStock = pd.concat([self.stk, I])
        NewStock = NewStock.groupby(NewStock.index).sum() # summarize entries with the same index. Later: form weighted average of properties

        self.stk = NewStock


    def out(self, O):
        """
        |  The processes of extracting something from the storage
        """
        # first check whether the required item is in stock

        # stk = pd.concat([stk, I])

        if len(O.index) > 1:
            raise ValueError("O can currently only have one entry")

        if O.index.values in self.stk.index.values:
            if self.stk.loc[O.index, "Q"][0] > O["Q"][0]:
                self.stk.loc[O.index, "Q"] -= O["Q"][0]
        
                # return the output, but use the properties from the stock
                Output = self.stk.loc[O.index,:]
                Output["Q"] = O["Q"][0]
                return Output
            
            else:
                print("item not in stock in requested quantities")
        else:
            print("item not in stock")


    
    def add_inp(self):
        """
        |  Additional inputs needed
        """

    def emis(self):
        """
        |  Emissions resulting from the input
        |  Other option would be to report the emissions as a multiindex or with an additional column that says whether they were based on the input or additional input
        """

    def add_emis(self):
        """
        |  Additional emissions resulting from the additional input
        """



Storage1 = Storage()

## check the status:
Storage1.stk # initial stock is 100 tons of silage maize

# let's add some wheat
Storage1.inp(I=pd.DataFrame({'Q': 50, 'U': "ton"} ,index=["wheat"]))

## check the status:
Storage1.stk # now has two commodities in stock

# let's add some more wheat
Storage1.inp(I=pd.DataFrame({'Q': 200, 'U': "ton"} ,index=["wheat"]))

## check the status:
Storage1.stk # now has two commodities in stock

# try to remove 500 ton of wheat
Storage1.out(O=pd.DataFrame({'Q': 500, 'U': "ton"} ,index=["wheat"]))

# try to remove 50 ton of wheat
Storage1.out(O=pd.DataFrame({'Q': 60, 'U': "ton"} ,index=["wheat"]))
Storage1.stk


### Another example 

class MyClass(object):
    def __init__(self, a):
        self.a = a

    @property
    def b(self):
        print("running method b")
        return self.a + 5

    @property
    def c(self):
        print("running method c")
        return self.a * self.b

x = MyClass(42)

x.a # self.a is read
x.b # method b is executed
x.c # method c and b are executed



### A function and how it could change over time


class StorageExample:
    """
    Example of how a storage class may look like
    """
    def __init__(self, initial=100, capacity=500):
        self.stk = initial
        self.capacity = capacity

    def fill(self, add):
        self.stk += add
    
    def empty(self, remove):
        if  self.stk > remove:
            self.stk -= remove
        else:
            print("Not enough in stock")


A = StorageExample(initial=10, capacity=500)

A.stk
A.fill(add=100)
A.stk
A.empty(remove=200)
A.empty(remove=50)
A.stk


### Example of a more classical thing. The biogas plant
import start
import pandas as pd
from tools import prod

class BiogasPlantExampleNovel:

    EMISSION_FACTOR = 0.01
    OUTPUT_FACTOR = 0.8

    def __init__(self, max_input=100):
        self.max_input = max_input
        self.I = None

    # @cached_property
    def provide_input(self, I = prod("maize", 10 ,"ton")):
        self.I = I

    @property
    def additional_input(self):
        return prod("lubricant", self.I["Q"][0] * 0.05, "ton")

    @property
    def input(self):
        return pd.concat([self.I, self.additional_input])

    @property
    def emissions(self):
        if self.I is not None:
            return self.I["Q"][0] * self.EMISSION_FACTOR
        else:
            print("No input defined")

    @property
    def output(self):
        if self.I is not None:
            return prod("digestate", self.I["Q"][0] * self.OUTPUT_FACTOR, "ton") 
        else:
            print("No input defined")

## What is the advantage of this approach?
# Arguments can only be provided once, but I could be replaced later on



## Classical approach
class BiogasPlantExampleClassical:

    EMISSION_FACTOR = 0.01
    OUTPUT_FACTOR = 0.8

    def __init__(self, I = prod("maize", 10 ,"ton"), max_input=100):
        self.I = I
        self.max_input = max_input

    def additional_input(self):
        return prod("lubricant", self.I["Q"][0] * 0.05, "ton")

    def input(self):
        return pd.concat([self.I, self.additional_input])

    def emissions(self):
        if self.I is not None:
            return self.I["Q"][0] * self.EMISSION_FACTOR
        else:
            print("No input defined")

    def output(self):
        if self.I is not None:
            return prod("digestate", self.I["Q"][0] * self.OUTPUT_FACTOR, "ton") 
        else:
            print("No input defined")


## Classical approach, but amended by the new_I method, which could be inherited to all classes
from functools import cached_property
from tools import remove_system

class BiogasPlantExampleAmended:

    EMISSION_FACTOR = 0.01
    OUTPUT_FACTOR = 0.8

    def __init__(self, I = prod(("production.plant.ArableLand", "maize"), 10 ,"ton"), O = None, max_input=100):
        # self.I = remove_system(I) # NOTE: here I remove the system. Bad!
        self.I = I
        self.O = O
        self.max_input = max_input

    def change_I(self, I):
        # clear the cache
        if "emissions" in self.__dict__:
            del self.emissions
        if "output" in self.__dict__:
            del self.output
        if "input" in self.__dict__:
            del self.input
        if self.O is not None:      # NOTE: I need to distinguish between IProcess, OProcess and IOProcess if I want to inherit this
            self.O = None
            print("O set to None")
        self.I = I                  # NOTE: here I would not remove the system. Bad!

    def additional_input(self):
        return prod("lubricant", self.I["Q"][0] * 0.05, "ton")

    @cached_property
    def input(self):
        return pd.concat([self.I, self.additional_input])

    @cached_property
    def emissions(self):
        if self.I is not None:
            return self.I["Q"][0] * self.EMISSION_FACTOR
        else:
            print("No input defined")

    @cached_property
    def output(self):
        if self.I is not None:
            return prod("digestate", self.I["Q"][0] * self.OUTPUT_FACTOR, "ton") 
        elif self.O is not None:
            return self.O
        else:
            print("No input defined")

## How do I get rid of the remove_system add_system annoiance?



### Differences between the approaches ###
Plant1 = BiogasPlantExampleNovel(max_input = 500)

## in the novel approach, input is provided with a method
Plant1.provide_input(I=prod("maize", 100, "ton"))
Plant1.additional_input
Plant1.emissions

## this way new input can be provided with the same method
Plant1.provide_input(I=prod("maize", 500, "ton"))
Plant1.additional_input
Plant1.emissions


## in the classical approach input is provided as an argument
Plant2 = BiogasPlantExampleClassical(I=prod("maize", 100, "ton"))
Plant2.emissions()
Plant2.additional_input()

Plant2.I = prod("maize", 500, "ton")
Plant2.emissions()
Plant2.additional_input()


## possible mixture of approaches: The classical way is used, but an inherited function is used that replaces I.
# also emissions are cached properties, which are deleted when new_I is invoked
# One advantage of this approach is that in the beginning the class is already initialized as a I or O class.


Plant3 = BiogasPlantExampleAmended(I=prod("maize", 100, "ton"))

Plant3.emissions() # 1
Plant3.additional_input() # 5

Plant3.change_I(I= prod("maize", 500, "ton"))
Plant3.emissions # 5
Plant3.additional_input()
dir(Plant3)
Plant3.__dict__


Plant3a = BiogasPlantExampleAmended(I= None, O=prod("digestate", 2000, "ton")) #  This fails because I do the stupid remove_system on I
Plant3a.I # None
Plant3a.O # 2000 tons of digestate
Plant3a.emissions # This should calculate the input first, that is required to achieve this output, and then also the emissions. Use input or I for this??

Plant3a.change_I(I= prod("maize", 500, "ton"))

Plant3a.I # 500 tones of maize
Plant3a.O # has been set to None





Plant3a.__dict__["emissions"]

Plant3a.emissions

dir(Plant3a)

"emissions" in dir(Plant3a)

Plant3a.O



Plant3a.I

### 