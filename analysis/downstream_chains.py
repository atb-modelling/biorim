import sys
import pandas as pd
import xarray as xr
import warnings
import itertools

from copy import deepcopy
from importlib import import_module
from importlib.util import find_spec
from random import randrange

from start import *
from tools import convert_molmasses, convert_to_gwp, indirect_N2ON_emissions, report
from declarations import Product
from analysis import simdict
from collections import defaultdict



def start_simulation(configs):
    """
    | IMPORTANT: This analysis mode is not working anymore with newest modules! systems are no longer part of the index of outputs and emissions.
    | Compute a network analysis where different systems are evaluated and outputs of systems are passed as inputs to other systems.
    | The network is specified via a config file.
    
    :type configs: dict
    :param config: configuration file for the simulation
    :rtype: simdict
    :return: Returns a simdict (nested dict) containing all results (parameters, systems, outputs, ...) of the simulation

    |
    """


    def create_instance(system, config, I = None, O = None):
        """
        |  Creates the instance of the system based on information from the config.
        |  However, I and O arguments can partly overwrite what is specified in the config.
        """

        Module = system.rsplit(".",1)[0]
        Class = system.rsplit("_",1)[0]
        import_module(Module)

        if (O is not None) and (I is None):
            if system[-1].isdigit():
                ID = system.rsplit("_",1)[1]
                if system in config["systems"]:
                            param = config["systems"][system].copy() # parameters as defined in the config
                            # TODO: This overwriting of config definded output parameters should be dependant on conditions
                            # if there is already a definition for O for this system in the config
                            if "O" in param.keys():
                                # if requesting system asks for 100%, quantities are as defined in the config
                                if all(O["U"] == "%") and all(O["Q"] == 100): # currently only 100% definition allowed
                                    pass
                                # if quantities are not defined in the config, replace them by what is requested
                                elif any(param["O"]["Q"].isna()):
                                    if param["O"].index.equals(O.index):
                                        param["O"]["Q"] = O["Q"]
                                        param["O"]["U"] = O["U"]
                                    else:
                                        print(f"Cannot define output quantities for system {system}")
                                else:
                                    print(f"There seems to be a conflicting definition of O of system {system} in the config. O is overwritten!")
                                    param["O"] = O
                            # if O is not defined in the config, then define as requested
                            else:
                                param["O"] = O
                            Instance = eval(Class)(id=ID, **param)
                else:
                    print(f"system {system} not specified in config")
            else:
                Instance = eval(Class)(O=O)

        elif (I is not None) and (O is None):
            if system[-1].isdigit():
                ID = system.rsplit("_",1)[1]

                # create I as a combination of the passed I and the specification in the config
                if system in config["systems"]:
                    param = config["systems"][system].copy()

                    if "I" in config["systems"][system].keys():
                        
                        I_from_config = config["systems"][system]["I"]

                        Input = Product()
                        Input = Input.append(I_from_config.loc[I_from_config.index.difference(I.index)]) # all config specified inputs, except for products passed (as I)
                        Input = Input.append(I) # 
                        param["I"] = Input

                    else:
                        param["I"] = I

                    Instance = eval(Class)(id=ID, **param)

                else:
                    print(f"system {system} not specified in config")
            
            else:
                Instance = eval(Class)(I=I)
        else:
            raise ValueError("Specify either I or O")

        
        # if system does not have an id (is probably not specified in the config) assign one
        if not "_" in system:
            if Instance.id is None:
                # ID = randrange(1000, 9999)
                # simdata[scenario][t][run]["systems"][system].keys()

                # existing default systems
                sys_id = [x.rsplit("_") for x in simdata[scenario][t][run]["systems"]]

                exist_def_ids = defaultdict(list)
                for sys, id in sys_id:
                    exist_def_ids[sys].append(int(id))

                if system in exist_def_ids:
                    max_value = max(exist_def_ids[system])
                    # use values over 1000 for default systems
                    if max_value >= 1000:
                        ID = max_value + 1
                    else: # does exist as non-default already
                        ID = 1000
                else: # doesn't exist at all
                    ID = 1000
                
                Instance.id = ID
                system = system + "_" + str(ID)

        simdata[scenario][t][run]["systems"][system] = Instance

        # add a copy of the instance to the other timestep as well
        # NOTE: This may only work with two timesteps because at the second the I is not None

        if hasattr(Instance, "timestep_transfer") and Instance.I is not None:
            print(f"system {system} has time_transfer attribute")
            timestep_transfer = Instance.timestep_transfer

            timesteps = simdata[scenario]["config"]["simulation"]["timesteps"]
            timestep_no = timesteps.index(t) + timestep_transfer

            if timestep_no <= (len(timesteps)-1):
                # out_t = timesteps[timesteps.index(t) + timestep_transfer]
                out_t = timesteps[timestep_no]
                simdata[scenario][out_t][run]["systems"][system] = deepcopy(Instance)
            else:
                warnings.warn(f"{system} tries to write output to not existing timestep")

        return Instance



    def upstream_modelling(simdata, config, inputs, forsystem):
        """
        |  Upstream modelling.
        |  For all inputs specified this evaluates the respective source system.
        |  This is done consecutively until system is reached where not further inputs are required.
        
        |  **NOTE**:
        |  - Add scenario argument?
        |  - Change to evaluation based on additional_input?

        """

        # iterate only over the different systems (not the list of items, because all items from one system are requested at the same time)
        for system in set(inputs.index.get_level_values("system")):

            print(f"upstream modelling: evaluating system {system} as input for {forsystem}")
            
            # The inputs of the downstream system define the outputs of the upstream system
            RequestedOutputs = inputs.xs(system, level="system", drop_level = False)

            report(f"RequestedOutputs: {RequestedOutputs}")

            ## first check whether it exists already, and whether it already produced the necessary output
            if system in simdata[scenario][t][run]["systems"]:
                print(f"Instance {system} exists already")
                
                # TODO: Should refine the check whether is already produced in the right quantities
                MissingItems = RequestedOutputs.index.difference(simdata[scenario][t][run]["systems"][system].output().index)
                if len(MissingItems) > 0:
                    warnings.warn(f"some outputs seem to be missing from already created Instance of {system}")

            else:
                Instance = create_instance(system, config, I = None, O = RequestedOutputs)
                # Instance_evaluated = eval_instance_methods(Instance, system)

                # Input = Instance_evaluated.input
                # Output = Instance_evaluated.output

                ##TODO: Change to attribute call (without the "()") when methods have been changed to cached_property
                Input = Instance.input()
                Output = Instance.output()

                report(f"Input: \n {Input}")
                report(f"Output: \n {Output}")

                # Add produced outputs to simdict "outputs_for_system". This summarizes which products have already been defined.
                # RequestedOutputProduced = Output.loc[RequestedOutputs.index,:]
                # RequestedOutputProduced.index = pd.MultiIndex.from_tuples([(forsystem, x[0], x[1]) for x in RequestedOutputProduced.index.to_list()], names=['for_system', 'system', 'item'])
                # simdata[scenario][t][run]["outputs_for_system"] = simdata[scenario][t][run]["outputs_for_system"].append(RequestedOutputProduced)

                if Input is not None:
                    upstream_modelling(simdata, config=config, inputs=Input, forsystem=system)


    def downstream_modelling(simdata, scenario, config, passed_input):
        """
        |  Downstream modelling.
        |  All outputs of systems that are listed as inputs of some other system in the config are passed further.
        |  This is continously done until no more more outputs are created that need to be passed further.
        |  For all systems that are executed in this way it is also checked whether additional inputs are required which have not already been passed.
        """

        ## select only the outputs that have to be passed further (because they are listed as input of some other system in the config):
        # NOTE: This is problematic, because for the "material.storing.DigestateStorage_1" there is currently no output listed there!
        # TODO: Adapt to 
        product_selector = list(set(simdata[scenario][t]["FromSystemToSystem"].keys()).intersection(passed_input.index.to_list()))
        OutputsToPass = passed_input.loc[product_selector]

        # for all outputs now selected determine to which system they have to go:
        # for out in outputs.itertuples():
        for out in OutputsToPass.itertuples():
            system = simdata[scenario][t]["FromSystemToSystem"][out.Index]

            print(f"downstream modelling for: {system} and {out.Index}")

            PassedInput = OutputsToPass.loc[[out.Index],:]

            Instance = create_instance(system, config, I=PassedInput)
            # system_attributes = eval_instance_methods(Instance, system)

            # Outputs = system_attributes.output
            # AllInputs = system_attributes.input

            Outputs = Instance.output()
            AllInputs = Instance.input()



            ## When there are inputs specified other than the ones passed then upstream modelling is required for these

            if AllInputs is not None:
                if PassedInput is not None:
                    FurtherInputs = AllInputs.loc[AllInputs.index.difference(PassedInput.index)] # futher inputs needed by the system, and not alread provided by the chain
                else:
                    FurtherInputs = AllInputs

                if FurtherInputs is not None: # for all inputs that are not the ones that are already passed, go upstream
                    upstream_modelling(simdata, config=config, inputs = FurtherInputs, forsystem=system)

            # For all outputs downstream modelling is done
            if Outputs is not None:
                downstream_modelling(simdata, scenario, config = config, passed_input = Outputs)



    def parameter_table(config, only_first = False): 
        """
        |  Function to create a Pandas table of unique parameter combinations.
        |  if only_frist = True, take only the first of several parameters 
        """

        systems = list(config["systems"].keys())

        # parameter lists for all systems
        prep_list = []
        for sys in systems:
            parameters = list(config["systems"][sys].values())
            if only_first:
                parameters = [[param] if not isinstance(param, list) else [param[0]] for param in parameters] # assure all single parameters are lists
            else:
                parameters = [[param] if not isinstance(param, list) else param for param in parameters] # assure all single parameters are lists
            prep_list.extend(parameters)

        # create unique combinations
        iter_list = list(itertools.product(*prep_list))

        # empty parameter table with correct dimensions
        ParamTable = pd.DataFrame()
        for sys in systems:
            ParamTable = ParamTable.append(pd.DataFrame(columns=list(range(len(iter_list))), index = pd.MultiIndex.from_product([[sys], list(config["systems"][sys].keys())], names=["system", "param"])), sort=False)

        # pass the values to the table
        # NOTE: Both version from unsolved merge. Maybe other version is working.
        # for i, run_param in enumerate(iter_list):
        #     ParamTable.iloc[:,i] = list(run_param)
        
        # NOTE: This part also creates a deprecated warning: VisibleDeprecationWarning: Creating an ndarray from ragged nested sequences (which is a list-or-tuple of lists-or-tuples-or ndarrays with different lengths or shapes) is deprecated. If you meant to do this, you must specify 'dtype=object' when creating the ndarray
        # This seems similar to the old error: tuple instead of list.
        for run, run_param in enumerate(iter_list):
            for row, dat in enumerate(run_param):
                ParamTable[run][row] = dat # TODO: Try with list(dat)

        return ParamTable

    def check_config(config):
        """
        Function to check whether there are any obvious mistakes in the config
        e.g. systems defined which do not exist
        """

        # config type
        if not isinstance(config, dict):
            print("config needs to be of type dict")

        # overall categories
        categories = ["simulation", "analyses", "systems"]
        missing_categories = [c for c in categories if c not in config.keys()]
        if len(missing_categories) > 0:
            print(f"category {missing_categories} is missing in the config")

        # systems
        for system in config["systems"].keys():
            module = system.rsplit(".",1)[0]
            if module not in sys.modules:
                if find_spec(module) is None:
                    print(f"module {module} does not exist")



    def create_simdata(configs):
        """
        Create simdata file with seperate configs for all scenarios and parameter combinations
        """
        simdata = simdict()

        if not isinstance(configs, list):
            configs = [configs]

        for config in configs:

            check_config(config)

            scenario = config["simulation"]["name"]

            simdata[scenario] = {}
            simdata[scenario]["config"] = config # the config as defined by the user


            ## create configs for all timesteps
            if "timesteps" in simdata[scenario]["config"]["simulation"]:
                timesteps = config["simulation"]["timesteps"]
            else:
                timesteps = ["t1"]

            for t in timesteps:
                tconfig = deepcopy(config) # tconfig is a version of the config with the timesteps removed. One for each timestep.
                tconfig["simulation"]["timesteps"] = t
                for system in config["systems"]:
                    if "I" in config["systems"][system].keys():
                        if isinstance(config["systems"][system]["I"], dict):
                            if set(timesteps) == set(config["systems"][system]["I"]):
                                tconfig["systems"][system]["I"] = config["systems"][system]["I"][t]
                            else:
                                raise ValueError(f"{system} not specified with the correct timesteps")
                        

                simdata[scenario][t] = {}
                simdata[scenario][t]["config"] = tconfig

                if config["simulation"]["type"] == "single":
                    simdata[scenario][t]["parameters"] = parameter_table(tconfig, only_first = True)

                elif config["simulation"]["type"] == "all combinations":
                    simdata[scenario][t]["parameters"] = parameter_table(tconfig, only_first = False)

                else:
                    raise ValueError(f'No option defined for {tconfig["simulation"]["type"]}')


                # dictionary which lists where products specified in the config have to be passed to
                simdata[scenario][t]["FromSystemToSystem"] = {}

                for system in tconfig["systems"]:
                    if "I" in tconfig["systems"][system]:
                        if tconfig["systems"][system]["I"] is not None:
                            simdata[scenario][t]["FromSystemToSystem"][tconfig["systems"][system]["I"].index[0]] = system

                # iterate over the runs (= the different parameter combinations of each scenario)
                for run in simdata[scenario][t]["parameters"].columns.to_list():

                    simdata[scenario][t][run] = {}
                    simdata[scenario][t][run]["outputs"] = Product()
                    simdata[scenario][t][run]["outputs_for_system"] = Product()
                    simdata[scenario][t][run]["emissions"] = Product()
                    simdata[scenario][t][run]["systems"] = {}

                    # for each run create a config file again based on the parameters table
                    runconfig = deepcopy(tconfig)

                    for system in tconfig["systems"].keys():
                        for param in tconfig["systems"][system]:
                            runconfig["systems"][system][param] = simdata[scenario][t]["parameters"].loc[(system,param),run]

                    simdata[scenario][t][run]["config"] = runconfig

        return simdata


    def emis_xr_runs(simdata, scenario, indirect_N2O=True, outtype="molecule"): 
        """
        Create a xarray of emissions data
        """

        runs = simdata[scenario][t]["parameters"].columns.to_list()

        Emisxr = xr.concat([xr.DataArray(simdata[scenario][t][run]["emissions"]) for run in runs], dim="simulation")

        Emisxr = Emisxr.assign_coords(simulation=range(len(runs)))
        Emisxr = Emisxr.rename({'dim_0':"source", 'dim_1': "element"})

        if indirect_N2O:
            Emisxr = indirect_N2ON_emissions(Emisxr, climate="wet climate")

        if outtype=="molecule":
            Emisxr = convert_molmasses(Emisxr, to="molecule", keep_unconverted=False)

        return Emisxr



    #####################################
    # where the modelling actually starts

    simdata = create_simdata(configs)

    for scenario in simdata.keys():

        print(f"""
        ###########################################################################################################
        #                                                                                                         #
        #  Started modelling for scenario {scenario}
        #                                                                                                         #
        ###########################################################################################################
        """)

        if "timesteps" in simdata[scenario]["config"]["simulation"]:
            timesteps = simdata[scenario]["config"]["simulation"]["timesteps"]
        else:
            timesteps = ["t1"]


        # #############################
        # # NOTE: For development only
        # timesteps = ["t1"]
        # scenario = "dairy"
        # run = 0
        # t = "t1"

        # t = "t2"
        # #############################

        for t in timesteps:
            print(f"\nStarting calculations for timestep {t}\n")

            for run in simdata[scenario][t]["parameters"]:

                print(f"\nStarting calculations for run {run}\n")

                runconfig = simdata[scenario][t][run]["config"]
                reference_system = runconfig["simulation"]["reference_system"]

                # TODO: Need to enable that I and O are available at the same time

                # if (runconfig["systems"][reference_system].get("I") is not None) and (runconfig["systems"][reference_system].get("O") is not None):
                #     raise Exception("Specifying I and O at the same time for the reference_system currently not supported")

                
                if runconfig["systems"][reference_system].get("I") is not None:
                    
                    print(f"reference system {reference_system} is specified with I. Starting with upstream modelling, to provide inputs.")

                    ## from the reference system model upstream
                    RefConfigInputs = runconfig["systems"][reference_system]["I"]

                    ## STRONGLY SIMPLIFIED:
                    # Need to move the differentiation between I, input, additional_input etc. to the upstream_modelling function

                    upstream_modelling(simdata, config=runconfig, inputs=RefConfigInputs, forsystem=reference_system) # these models all specified systems before the reference
                    ## PROBLEM: This continues upstream although the DigestateStorage already exists
                    # TODO: Needs to stop there

                    InputToRef = Product()
                    # now collect the now produced inputs
                    for sys_item in RefConfigInputs.index:
                        InputToRef = InputToRef.append(simdata[scenario][t][run]["systems"][sys_item[0]].output().loc[[sys_item],:])

                    RefSystemInstance = create_instance(system=reference_system, config=runconfig, I=InputToRef)

                    AllInputs = RefSystemInstance.input()

                    RefOutputs = RefSystemInstance.output()

                    # all further inputs (for upstream modelling below)
                    # NOTE: Potentially replace with a call to additional_inputs later on
                    if len(AllInputs.index.difference(InputToRef.index)) > 0:
                        RefInputs = AllInputs.loc[AllInputs.index.difference(InputToRef.index)]
                    else:
                        RefInputs = None


                elif runconfig["systems"][reference_system].get("O") is not None:

                    print(f"reference system {reference_system} is specified with O. Starting with downstream modelling.")

                    RefConfigOutputs = runconfig["systems"][reference_system]["O"]

                    RefSystemInstance = create_instance(system=reference_system, config=runconfig, O=RefConfigOutputs)
                    RefInputs = RefSystemInstance.input()
                    RefOutputs = RefSystemInstance.output()

                else:
                    raise Exception("Either I or O have to be defined in config for the reference_system")

                # calculate all systems before the reference system for all the other inputs required but not specified in the config
                if RefInputs is not None:
                    upstream_modelling(simdata, config=runconfig, inputs=RefInputs, forsystem=reference_system)


                # now run downstream modelling
                if RefOutputs is not None:
                    downstream_modelling(simdata=simdata, scenario=scenario, config=runconfig, passed_input=RefOutputs)

            # Add here that emissions of several scenarios are added together 
            simdata[scenario][t]["emissions"] = emis_xr_runs(simdata, scenario)
            simdata[scenario][t]["gwp"] = convert_to_gwp(simdata[scenario][t]["emissions"], timerange=[20, 100])

    return simdata