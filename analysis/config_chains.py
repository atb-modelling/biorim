"""
This is the old way of linking different processes. Not maintained anymore.
"""

import os
import numpy as np
import pandas as pd
import xarray as xr
import importlib
import inspect
import itertools
import json
import pickle
from copy import deepcopy

from analysis import EnhancedJSONEncoder
from tools import relpath, convert_molmasses, convert_to_gwp, input_system, indirect_N2ON_emissions
from external import ExternInput, ExternOutput
from declarations import Product
from modelconfig import output_path



## Create the simdict class: child of the dict class, with specific methods for instance for writing to disk

class simdict_old(dict):
    """
    The old version of simdict compatible with config_chains
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __repr__(self):
        return f"{type(self).__name__}({super().__repr__()})"

    ## mulitindices that are in "parameters" or the analyses results "emissions" or "gwp" are reduced in dimension
    def merge_multiindex(self, inplace=False):
        if inplace:
            y = self
        elif inplace==False:
            y = deepcopy(self)

        for sim in y.keys():
            y[sim]["param"].index = y[sim]["param"].index.map('|'.join)

            # iterate over existing analyses
            for analys in set(["emissions", "gwp"]).intersection(set(y[sim].keys())):
                if isinstance(y[sim][analys], xr.DataArray):
                    # multiindex to simple. Currently only considers the "source" dimension of xarrays.
                    new_index = []
                    for w in y[sim][analys].coords["source"].data:
                        new_index.append('|'.join(w))
                    y[sim][analys].coords["source"] = xr.DataArray(new_index, dims="source")

        if inplace==False:
            return y


    # currently this only saves config and parameters
    ## saving everything as a json is still a valid option
    def to_json(self, filename):
#        x = self
        y = self.merge_multiindex(inplace=False)
        ## save the config and parameters as json
        for sim in y.keys():
            # convert parameter pandas to dict
            y[sim]["param"] = y[sim]["param"].to_dict(orient="index")
            # convert simulations results xarrays (e.g. emissions, gwp) to dict
            for analys in y[sim]["simconfig"]["analyses"]:
                y[sim][analys] = y[sim][analys].to_dict()

#                if isinstance(x[sim][analys], xr.DataArray):
#                    # multiindex to simple
#                    new_index = []
#                    for w in x[sim][analys].coords["source"].data:
#                        new_index.append('|'.join(w))
#                    x[sim][analys].coords["source"] = xr.DataArray(new_index, dims="source")

            
            del y[sim]["objects"]
#            path = os.path.abspath(os.path.join(os.path.abspath(output_path),os.path.relpath(filename)))
#            if not os.path.exists(path):
#                os.mkdir(path)

        with open(os.path.abspath(os.path.join(os.path.abspath(output_path),os.path.relpath(filename))), 'w') as f:
            json.dump(y,f, sort_keys=False, indent=4, separators=(',', ': '), cls=EnhancedJSONEncoder)
        
    # save config as json, the parameter pandas as csv, emissions and gwp xarrays as hdf5 (possibly the easiest way to read in R)
    def to_folder(self, subfolder):
        path = os.path.abspath(os.path.join(os.path.abspath(output_path),os.path.relpath(subfolder)))
        if not os.path.exists(path):
            os.mkdir(path)
        x = self
        for key in x.keys():
            # save parameter pandas as csv:
            x[key]["param"].index = x[key]["param"].index.map('|'.join).str.strip()
            x[key]["param"].to_csv(os.path.abspath(os.path.join(path, os.path.relpath(str(key +"_parameters.csv")))), encoding="latin-1", quotechar='"', sep=";")
            # emissions as netcdf
            for analys in ["emissions", "gwp"]:
                if isinstance(x[key][analys], xr.DataArray):
                    # multiindex to simple
                    new_index = []
                    for w in x[key][analys].coords["source"].data:
                        new_index.append('|'.join(w))
                    x[key][analys].coords["source"] = xr.DataArray(new_index, dims="source")
                    x[key][analys].to_netcdf(os.path.abspath(os.path.join(path, os.path.relpath(str(key + "_" + analys +".nc")))))

    def to_pickle(self, filename):
        with open(os.path.abspath(os.path.join(os.path.abspath(output_path),os.path.relpath(filename))), "wb") as f:
            pickle.dump(self, f)



def module_system(x):
    """     
    *To obtain the corresponding module call for the required system module_system opens a csv-file and reads the corresponding string.*
         
    :type x: string
    :param x: 'wheat' OR 'broiler' OR ...
    :return: class, including the module
    :rtype: string 

    |
    """        
    Module_call = pd.read_csv(relpath("analysis\\module_system.csv"), sep=";", encoding="latin-1", index_col=0)
    if(isinstance(x, tuple)):
        x = x[0]
    return Module_call.loc[x,"module"]


def network_info(x):
    """ 
    | *The function* ``network_info`` *requires the information provided in the simconfig* (``simconfig["chains"]``) *to return the information about the simulation network needed for the initilization & simulation*  
    | *Besides the nodes, where chains are connected, the function returns the so called levels, the position of chains in relation to the node(s).*

    :type x: dict
    :param x:  simconfig["chains"]
    :return: (*list*) of nodes and list(s) of the indices of prescribing of which chains (order in simconfig) should be initialized first (first level 1, then level 2, and so forth).

    |
    """
    # how many chains are prescribed
    llength = len(x)
    # initializing lists to be appended
    nodes = []
    checklevel = []
    levels = []
    # iterate over amount of chains
    for pos in range(llength):
        # check for nodes between current chain and next/last chain
        # this has to be changed if more complex networks with more nodes are to be simulated !!!
        if pos == llength-1:
            try:
                node = list(set(x["chain_"+ str(pos)]).intersection(x["chain_"+ str(pos + 1)]))[0]
            except IndexError:
                    raise IndexError("Node or more specifically one of two identical chain members missing in one of the chains. Please check and adjust!")
        else:
            try:
                node = list(set(x["chain_"+ str(pos + 1)]).intersection(x["chain_"+ str(pos + 2)]))[0]
            except IndexError:
                raise IndexError("Node or more specifically one of two identical chain members missing in one of the chains. Please check and adjust!")

        # check if node already exists in nodes list - if not append node
        if node not in nodes:
            nodes.append(node)
        # check position of node in chain - if at ending of chain - level 1 is assigned, else if at beginning level 2.
        # again - this has to be changed if more complex networks with more nodes are to be simulated !!!
        if node in x["chain_"+ str(pos + 1)][len(x["chain_"+ str(pos + 1)])-1]:
            checklevel.append(1)
        elif node in x["chain_"+ str(pos + 1)][0]:
            checklevel.append(2)
    # depending on how many levels (again a question of complexity) for each level a list is appended
    for cl in range(len(checklevel)):
        levels.append(list(np.where(np.array(checklevel) == cl+1)[0] + 1))
    # final nested list is returned
    y = [nodes,levels]
    return(y)

 
def init_objects(simconfig, iterator=None):
    """ 
    | *Function to initialize objects prescribed by the provided* ``simconfig`` *and its simulation chains as well as its object ID settings.*
    | *The initalization needs to run through the chains since it requires the output of each output as input for the next object to be initialized.*
    | *The complexity is increased through nodes since multiple outputs are required as inputs. Furthermore the "input" argument within the simconfig defines the subset of the output to be used.*
    | *the initalization returns a list of the defined (``simconfig``) objects in the order as it is initialized.* 
    
    :type x: dict
    :param x:  simconfig
    :return: (*dict*) of object names (*string*) with objects (each class) prescribed by the simulation chains (*simconfig*)

    **Note**
    - For further information the simconfig file is assigned to the global namespace using its file name.

    |
    """
    ## utility function(s)
# NOTE: This function should possibly return a Product
    def prelevel_outputs(x,system_object, simconfig):
        # index initialization
        ii = 0
        # iterate over objects that should provide their output
        for sys in x:
            # use provided input information (simconfig["objects"][system_object]["input"]) to subset the output of x (if provided/required)
            if len(simconfig["objects"][system_object]["input"]) > 0:
                try:
                    # pre_y = sys.output().loc[simconfig["objects"][system_object]["input"]].dropna(how="all")
# NOTE: THIS CURRENTLY THROWS AN ERROR!
                    print("sys: ", sys)
                    print("sys.output()", sys.output())
# NOTE: This doesn't work because without an input LandTransport can not supply an output
                    pre_y = sys.output().xs(simconfig["objects"][system_object]["input"], level="item", drop_level=False).dropna(how="all")
                except KeyError:
                    raise KeyError("Check and adjust the 'param' or/and the 'input' of your object: " + str(system_object))        
            else:
                pre_y = sys.output()
            # if first iteration then use pd.DataFrame as dataframe - else append
            if ii==0:
                y=pre_y
            else:
                y= y.append(pre_y)
            # increase index
            ii=ii+1
        return y

    ## main
    # read and assign simconfig
#    if isinstance(simconfig, str):
#        simconfig = importlib.import_module("simulation." + simconfig.split(".py")[0]).simconfig
    if not isinstance(simconfig, dict):
        raise Exception("'simconfig' should be a string in terms of simconfig filename or the dict of the simconfig itself.")
    # setting a simulation path throughout the chains and its nodes
    if "simconfig" in simconfig:
        simdata = simconfig
        simconfig = simconfig["simconfig"]
    siminfo = network_info(x=simconfig["chains"]) 
    objects={}
    # information from above is provided with siminfo[1]
    lvls = siminfo[1]
    level = 0
    for lvl in lvls:
        level=level + 1
        # level describes the location around and inbetween node(s). Level 1 is the level before the first node, level 2 after the first node, (level 3 after the second node) 
        check_chain = 0
        for chain_number in lvl:
            # the levels were assigned in network_info and the matching chains where returned as indices (chain_number)
            chain = simconfig["chains"]["chain_"+str(chain_number)]
            for sys in range(len(chain)):
                if(sys == len(chain)-1) and (chain[sys] in siminfo[0]):
                    continue # avoid double accounting of nodes
                # finally a loop over each chain members (systems) to initialize them with each settings provided in simconfig
                system_object = simconfig["chains"]["chain_"+str(chain_number)][sys]
                system = system_object[::-1].split("_",1)[1][::-1]
                #if simconfig["simulation"]["type"] in ["uncertainty, all combinations", "uncertainty, Monte-Carlo"]:
                if "simdata" in locals():
                    if system_object in list(simdata["param"].index.get_level_values('object')):
                        settings = simdata["param"].loc[system_object,iterator].to_dict()
                    else:
                        settings = {}
                else:
                    settings = simconfig["objects"][system_object]["param"]
                print("settings: ", settings)
                # module settings
                print("Initializing object " + system_object + " of class " + system + "...")
                ## import required packages automatically
                # get the location (within module structure) for each class
                modules = module_system(system)
                # get "supermodule" as for example "production" or "material" that somehow needs to be imported concurrently (check if already imported)
                supermodule = modules.split(".")[0]
                if supermodule not in globals():     
                    globals()[supermodule] = importlib.import_module(supermodule)
                # get the corresponding "module chain" for class that needs to be imported (check if already imported)
                submodule = modules[::-1].split(".",1)[1][::-1]
                if submodule not in globals():
                    globals()[submodule] = importlib.import_module(submodule)
                # eval/initialize first object (of all) object within chain network
                if (level==1) and (sys==0):
                    objects[system_object] = eval(modules)(**settings)
                    if check_chain == 0:
                        level_objects = []
                elif (level>=2) and (sys==0):
                    # if first iteration in level 2 (>= 2) then set object prelevel_objects with collected objects from level_objects - which is then "emptied" to be reused.
                    if check_chain ==0:
                        prelevel_objects = level_objects
                        level_objects = []
                    # eval/initialize first object (of second or higher level) object within chain network
                    # function "prelevel_outputs" executes the output methods for multiple objects and supplies its output as a pd.DataFrame
                    print("prelevel_objects: ", prelevel_objects)
                    print("system_object: ", system_object)
                    print("simconfig: \n:", simconfig)
                    objects[system_object] = eval(modules)(I=prelevel_outputs(prelevel_objects,system_object,simconfig),**settings)
                else:
                    # use provided input information (simconfig["objects"][system_obj]["input"]) to subset the output of x (if provided/required)
                    if len(simconfig["objects"][system_object]["input"]) > 0:
                        try:
                            objects[system_object] = eval(modules)(I=presystem_object.output().loc[simconfig["objects"][system_object]["input"]].dropna(how="all"),**settings)
                        except KeyError:
                            raise KeyError("Check and adjust the 'param' or/and the 'input' of your object: " + str(system_object))
                    else:
                        objects[system_object] = eval(modules)(I=presystem_object.output(),**settings)
                # assign object to presystem_object for next iteration usage as input (presystem_object.output)
                presystem_object = objects[system_object]
                # if end of chain (before node) is reached and the last chainobject is a node then append system to a list (to be used in prelevel_outputs)
                if sys == len(chain)-2: 
                    if chain[sys+1] in siminfo[0]:
                        level_objects.append(objects[system_object])
            # increase index
            check_chain = check_chain + 1
            print("Done.")
    return(objects)


def chain_emis(fun, consider_I=False, stopsystem=None, **kwargs):
    """     
    *The function calculates emissions resulting from upstream processes going back from the (main) **fun** function. Further parameters can/have to be specified, as for some an Input **I**  *
         
    :type fun: class or class instance
    :param fun: e.g. material.composting.CompostPlant
    :type stopsystem: class
    :param stopsystem: e.g. BroilerFarm
    :type **kwargs:
    :param **kwargs: additional arguments for **fun**, e.g. I 
    :return: (*pandas.dataframe*) dataframe with listed emissions and multi-index: *system* and *item*
    |
    """        

    ## differentiate between classes being passed to the function (which need the additional arguments), and class instances which already contain the arguments
    if inspect.isclass(fun):
        fun = fun(**kwargs)

    Emissions = pd.DataFrame()
    # try to get the emissions from the main function. If none available, then return pandas with NaN entries
    try:
        fun_emissions = fun.emissions()
    except:
        fun_emissions = pd.DataFrame({'CH4-C':np.nan, 'CO2-C':np.nan, 'N2O-N':np.nan}, index=[fun.__class__.__name__])
    
    # multilevel indexing showing where emissions where caused in the chain
    # maybe even more levels are needed: It can be the case that e.g. wheat production is caused by broiler production and by chicklet production: should be possible to differentiate
    # "element" dimension currently not used because "element" is already the dimension name for gasses like CO2-C (elemenary form), which causes problems in convert_to_gwp
    fun_emissions["system"] = fun.__class__.__name__
    fun_emissions["item"] = fun_emissions.index
    fun_emissions.set_index(["system", "item"], inplace=True)
    emis = Emissions.append(fun_emissions)

    # get the emissions from the lower level processes (via the list of inputs)
    def get_sub_emis(Input, stopsystem):
        for item, param in Input.iterrows():
            # TODO: future application: if item is a tuple the first element of the tuple specifies the input system!
            
            system = input_system(item)

            # if item is a tuple (that actually defines the source system and the item) replace:
            if isinstance(item, tuple):
                item = item[1]

            # import required packages dynamically
            subsystems = system.split(".")[:len(system.split("."))-1]
            supersystem = system.split(".")[0]
            globals()[supersystem] = importlib.import_module(supersystem)
            if(list.__len__(subsystems) >= 2 ):
                supersystem = ".".join(system.split(".")[:2])
                globals()[supersystem] = importlib.import_module(supersystem)

            print("item: ", item)
            print("param: ", param)
            print("system: ", system)

            # more a dirty fix for external inputs to make it usable for the following eval(system)
            if "external.ExternInput" in param.name[0]:
                param.name = (param.name[0].replace("external.ExternInput.", ""), param.name[1])

            subfun = eval(system)(O=Product(param.to_frame().T))
            subfun_emissions = subfun.emissions()

            subfun_emissions["system"] = system
            subfun_emissions["item"] = item
            subfun_emissions.set_index(["system","item"], inplace=True)
            nonlocal emis # necessary so that the function modifies the emis variable defined in the main emis_chain function when run as a subfunction
            emis = emis.append(subfun_emissions)
            # even one more level below:
            if (subfun.input() is not None) and (eval(system) != stopsystem):
                print("subfun: ", subfun)
                get_sub_emis(subfun.input(), stopsystem)
        return emis

    Input_main = fun.input()
    # Only if the main function has some input proceed
    if Input_main is not None:
        ## remove I from the input list if consider_I == False
        if consider_I == False:
            idx = Input_main.index.difference(fun.I.index)
            Input_main = Input_main.loc[idx]

        Emissions = get_sub_emis(Input_main, stopsystem)
    
    return Emissions

## Possible changes:
# With the current approach of using the chain_emis function, the name of the object (e.g. land_transport_1) is not passed, but the name (e.g. land_transport) is used

def calc_emissions(simdata, indirect_N2O=True, outtype="molecule"):
    """ 
    | *Function that calculates emissions for a simdata file. To this end it executes the chain_emis function for each object.* 

    :type init_objects: dict 
    :param init_objects: object names (*string*) with objects (each class) prescribed by the simulation chains (*simconfig*)
    :type upstream: boolean
    :param upstream: whether to include upstream activities (``additional_input``) in terms of emission method of ``externInput`` class
    :return: (*pd.Dataframe*) of listed emissions 

    **Note**
    - in the returned dataframe emissions are listed after their order in the ``init_objects``.
    - if ``upstream == True`` the available input emissions are listed using the object ID + "_input"

    |
    """

    for scenario, x in simdata.items():

        print("Calculating emissions for scenario " + scenario + "...")

        ## Whether upstream emissions should be considered
        upstream = x["simconfig"]["simulation"]["include_upstream_activities"]

        print('x["objects"]: \n', x["objects"])

        # alternative version without this enumerate construction:

        return_emis = xr.DataArray()
        return_emis = return_emis.expand_dims("simulation")

        for name, object in x["objects"].items():
            print("name: ", name)
            print("object: ", object)
            Emis = pd.DataFrame()

            if upstream:
                Emis = Emis.append(chain_emis(object, consider_I=False))
            else:
                pre_Emis = object.emissions()
                pre_Emis["system"] = name
                pre_Emis["item"] = pre_Emis.index
                pre_Emis.set_index(["system", "item"], inplace=True)
                Emis = Emis.append(pre_Emis)

            Emisxr = xr.DataArray(Emis).expand_dims("simulation")
            return_emis = xr.concat([return_emis, Emisxr], dim="simulation")

        # name for the simulation dimension
        return_emis = return_emis.assign_coords(simulation=range(len(x["objects"])))
        return_emis = return_emis.rename({'dim_0':"source", 'dim_1': "element"})

        if indirect_N2O:
            return_emis = indirect_N2ON_emissions(return_emis, climate="wet climate")

        if outtype=="molecule":
            return_emis = convert_molmasses(return_emis, to="molecule", keep_unconverted=False)

        simdata[scenario]["emissions"] = return_emis

    return(simdata)


### function to calculate global warming potential [CO2 equivalents] based on the previously calculated emissions

def calc_gwp(simdata, timerange=[10,20,50,100]):
    
    y = simdata.copy()

    for scenario, x in simdata.items():
        print("Calculating gwp for scenario " + scenario + "...")
        y[scenario]["gwp"] = convert_to_gwp(x["emissions"], timerange)
#        except:
#            warnings.warn("calc_emissions has to be executed before using calc_gwp!")

    return y



def sim_exec(simconfig, file_name=None):
    """ 
    | ``sim_exec`` *is the simulation function of the_model which allows to conduct simulations - single and multiple.*
    | *The function returns a nested dictionary as an output which refers to the objects and the simulation index (multiple simulations).*
    | *If a file_name is specified, then the output is written to the simconfig.data_path location with the specified file.name. The ending ('.json'/'.pickle') decides to which format.*
    | *For that it takes the arguments from the provided simconfiguration file* ``simconfig``. See below. 
    
    :type simconfig: dict
    :param simconfig: simconfig dictionary

    | OR

    :type simconfig: string
    :param simconfig: file-name of simconfig-file provided in the folder *themodel/simulation*
    
    | AND

    :type file_name: string
    :param: file name ('.json'/'.pickle') - default is ``None``, so that no file is written. If not ``None`` a thus named output file is written in the output directory (main simconfig).

    :return: (*simdict*) of simultion data (``simdata``) with simulation ``name``, the simconfiguration dictionary ``simconfig``, a ``param`` dataframe, the initialized ``objects`` and the applied analysis method (e.g. ``emissions``)

    **the simconfig dict (file)**

    *simulation*

    - *name* is used as the simulation name in the output. 
    - *type* defines the type of simulation, whether to have a single simulation (``single``), every combination (multiple parameter possibilites provided as list - ``uncertainty, all combinations``) or many combinations using a Monte-Carlo approach (``uncertainty, Monte-Carlo``)
    - *include_upstream_activities* - flag whether to include upstream activities (``True`` - mostly from method additional_input) for analysis.
    
    *analyses*
    
    - passes the method to be used for the simulation. Currently only ``emissions`` is possible, since only a calc_emissions wrapper (for every object) is available.


    *chains*

    - define chains (list) which the simulation follows. Define the dict keys as ``chain_1``, ``chain_2``, ..., ``chain_n``.
    - the naming convention of the chain links (*objects*) follows the names of the used classes, a ``_`` and the number - for example ``BroilerFarm_1``.  
    - make sure to define the chains until and including the node. If there is no node, then only one chain could be defined.
    - for chains starting after the node, also include the node at the beginning of the chain.
    - the properties (*param*) of the objects are defined in *objects*
    
    *objects*

    - each object (as described in chains) becomes its own dict with its arguments - ``"object_id":{"argument_1" : value, "argument_2" : value, ..., argument_n : value}``
    - make sure that every object that is used within the chains becomes its own object id entry
    - not every *param* has to be defined (if it has a default value)

    *Note*

    - The simconfiguration file can either be passed directly as a dict to ``sim_exec`` or as a file-name (string) in the simulation (currently) folder.
    - The more combinations the longer it takes to process and the more of memory it requires. Note that this phenomenon increases exponentially with more combinations ...
    - For many combinations it is therefore recommended to use a Monte-Carlo approach.

    |
    """

    ## inner utility functions
    # get parameters table for all possible combinations initialization/simulations
    def all_combi_type(simconfig):
        # get the involved (objects underlying) classes from chains
        selector = [jj for ii in list(simconfig["chains"].values()) for jj in ii]
        selector = list(np.unique(np.array(selector)))
        # extract values for every defined parameter as list to add it to an outer list
        # the outer list "prep_list" is later passed to the combination iteration tool itertools.product
        for oo, obj in enumerate(selector):
            object = list(simconfig["objects"][obj]["param"].values())
            # if parameter is not provided as list wrap it in a list for compatibility reasons (itertools.product)            
            for ii,item in enumerate(object):
                if not isinstance(item,list):
                    object[ii] = [item] 
            # if first iteration set prep_list by first objects parameters- else extend list by other objects parameter values (order is crucial!)
            if oo == 0:
                prep_list = object
            else:
                prep_list.extend(object)
        # prepare parameters DataFrame - requires information of objects and parameters (iteration)
        for oo,obj in enumerate(selector):
            # same amount of columns as combinations of parameters using itertools.product (each combination defines one initialization/simulation)
            in_table = pd.DataFrame(columns=list(range(len(list(itertools.product(*prep_list))))))
            # Setting the multi-index      
            in_table["object"] = list(itertools.repeat(obj,len(simconfig["objects"][obj]["param"].keys())))
            in_table["param"] = list(simconfig["objects"][obj]["param"].keys())
            in_table.set_index(["object", "param"], inplace=True)
            if oo == 0:
                out_table = in_table
            else: 
                out_table = out_table.append(in_table)
        # since itertools.product does not support indexing create an index
        ii = 0
        # for every combination (itertools.product(*prep_list)) pass values to corresponding column in beforehand created "out_table"
        for values in itertools.product(*prep_list):
            out_table.iloc[:,ii] = values
            # increase index
            ii += 1
        return out_table

    def sim_print(simconfig,status):
        print("""
        ###########################################################################################################
        #                                                                                                         #
        #  Model simulation '""" + simconfig["simulation"]["name"] + """' """ + status +(100 - len("Model simulation" + simconfig["simulation"]["name"] + "finished..."))*" "+"""#                  
        #                                                                                                         #
        ###########################################################################################################
        """)

    # get parameters table for single combination initialization/simulations
    def single_type(simconfig):
        # get the involved (objects underlying) classes from chains
        selector = [jj for ii in list(simconfig["chains"].values()) for jj in ii]
        selector = list(np.unique(np.array(selector)))
        # prepare parameters DataFrame - requires information of objects and parameters (iteration)
        for oo,obj in enumerate(selector):
            # one "0" column for a single (combination) simulation
            in_table = pd.DataFrame(columns=[0])
            # Setting the multi-index      
            in_table["object"] = list(itertools.repeat(obj,len(simconfig["objects"][obj]["param"].keys())))
            in_table["param"] = list(simconfig["objects"][obj]["param"].keys())
            in_table.set_index(["object", "param"], inplace=True)
            # if first iteration set out_table - else append other objects parameters indices below - also sets order for value assigning
            if oo == 0:
                out_table = in_table
            else: 
                out_table = out_table.append(in_table)    
        values = []
        for obj in selector:
            # iterate over each defined parameter to - depending on its type (list vs. int/str/...) - append it to values
            for item in simconfig["objects"][obj]["param"]:
                if isinstance(simconfig["objects"][obj]["param"][item],list):
                    values.append(simconfig["objects"][obj]["param"][item][0])
                    # simconfig["objects"][obj]["param"][item] = simconfig["objects"][obj]["param"][item][0]
                else:
                    values.append(simconfig["objects"][obj]["param"][item])        
        # pass values to out_table - order of values and objects-parameters is crucial!
        out_table.iloc[:,0] = values
        return(out_table)

    ## main 
    # read and assign simconfig
#    if isinstance(simconfig, str) | isinstance(simconfig, dict):
#        if isinstance(simconfig, str):
            # conf = importlib.import_module("simulation." + simconfig.split(".py")[0]).simconfig

    ## to iterate over simconfigs:
    if isinstance(simconfig, dict):
        simconfigs = [simconfig]
    elif isinstance(simconfig, list):
        simconfigs = simconfig

    simdata = {}
    for simconfig in simconfigs:
        scenario = simconfig["simulation"]["name"]

        sim_print(simconfig,"started...")

        # available simulation types - single (combination), use all combinations for uncertainty, or use a Monte-Carlo approach with random parameter choices - TO BE IMPLEMENTED! - recommended for very large amount of combinations
        avail_types = ["single","uncertainty, all combinations", "uncertainty, Monte-Carlo"]
        
        # create simdata skeleton
        simdata[scenario] = {"simconfig": simconfig}

        # check if simulation type is passed correctly
        if not isinstance(simconfig["simulation"]["type"], str):
            raise Exception("simconfig['simulation']['type'] should be a string. Then consider to choose one of these simulation types (sim_type): " + str(avail_types))
        else:
            if simconfig["simulation"]["type"] not in avail_types:
                raise Exception("Please choose one of these simulation types (sim_type): " + str(avail_types))

        # convert object param into readable structure - mostly regarding multi uncertainty simulations
        if simconfig["simulation"]["type"] == "single":
            # see single_type()
#            simdata["param"] = single_type(simconfig)
            simdata[scenario]["param"] = single_type(simconfig)
        elif simconfig["simulation"]["type"] == "uncertainty, all combinations":
            # see all_combi_type()
            simdata[scenario]["param"] = all_combi_type(simconfig)


        # initialize objects
        if simconfig["simulation"]["type"] in avail_types[1:]:
            simdata[scenario]["objects"] = []
            for ii in list(simdata[scenario]["param"]):
## THIS possibly needs to be updated. Don't get this. Should init_objects not be passed a simconfig??
                simdata[scenario]["objects"].append(init_objects(simdata[scenario], iterator=ii))
        else:
            simdata[scenario]["objects"] = init_objects(simdata[scenario], iterator=0)

### Possibly move the analyses further below, because the calc functions should be able to deal with the simdata files that contain a dict of scenarios 
## Possibly add an **kwargs argument to all calc functions, where the 

        # here first execute the emission function
#        if "emissions" in simconfig["analyses"]: # at the moment this is unconditional, since otherwise it is executed again for every simconfig
#    print("Executing calc_emissions")

# NOTE: Uncomment this line again!
    simdata = calc_emissions(simdata, **simdata[scenario]["simconfig"]["analyses"]["emissions"])

        # then the gwp function, since it relies on the precalculated emissions. For this reason, emissions should not vary between molecule and element
#        if "gwp" in simconfig["analyses"]:

#    print("calc_gwp deactived for the moment")
#    print("Executing calc_gwp")

# NOTE: Uncomment this line again!
    simdata = calc_gwp(simdata)

    return simdict_old(simdata)