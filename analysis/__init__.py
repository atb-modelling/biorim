"""
The main analysis script contains analysing functions that are required outside the analysis 
module as well as within the module and within different scripts (e.g. timestep_chains).

"""
__all__ = ["simdict"]

import os
import pandas as pd
import xarray as xr
import pickle
import json
import dataclasses
import warnings
import datetime
from copy import deepcopy
from plotnine import save_as_pdf_pages
from pathlib import Path

from modelconfig import output_path
from declarations import Product
from dev_utils import input_output_plot, text_plot


class EnhancedJSONEncoder(json.JSONEncoder):
    """
    JSON encoder with added functionality to convert dataframes and dataclass objects
    """
    def default(self, o):
        if isinstance(o, tuple): # avoid that tuple combinations are wrongly interpreted in R
            print("using convert of tuple")
            return str('_'.join(o))
        elif isinstance(o, pd.DataFrame):
            if isinstance(o, Product):
                if len(o.index) > 1:
                    warnings.warn("Product object with several lines truncated")
                return ("Product(" + str(o.index[0]) + ", " + str(o.iloc[0,0]) + ", " + o.iloc[0,1] + ")") # intermediate solution to deal with one row Product objects
            else:
                return o.to_dict(orient="index")
        elif dataclasses.is_dataclass(o):
            return dataclasses.asdict(o)
        return super().default(o)


class simdict(dict):
    """
    Simdict objects are used to store the results of the analysis. This is a special kind of dict (from where it is inherited), and contains
    for instance special methods to write to disk.
    |  **NOTE:**
    |  This version is adapted to the downstream_chains analysis method.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.info = {'time': datetime.datetime.today()}

    def __repr__(self):
        return f"{type(self).__name__}({super().__repr__()})"
       

    def to_json(self, filename):
        """
        |  Save main results as json file for further processing in R.
        |  NOTE:
        |  - This approach is no longer supported. Instead refer to the to_folder method.
        """

        warnings.warn("to_json is no longer maintained. Use to_folder instead")

        ## mulitindices that are in "parameters" or the analyses results "emissions" or "gwp" are reduced in dimension
        def merge_multiindex(self, inplace=False):
            if inplace:
                y = self
            elif inplace==False:
                y = deepcopy(self)

            for sim in y.keys():
                y[sim]["parameters"].index = y[sim]["parameters"].index.map('|'.join)

                # iterate over existing analyses
                for analys in set(["emissions", "gwp"]).intersection(set(y[sim].keys())):
                    if isinstance(y[sim][analys], xr.DataArray):
                        # multiindex to simple. Currently only considers the "source" dimension of xarrays.
                        new_index = []
                        for w in y[sim][analys].coords["source"].data:
                            new_index.append('|'.join(w))
                        y[sim][analys].coords["source"] = xr.DataArray(new_index, dims="source")

            if inplace==False:
                return y

        y = self.merge_multiindex(inplace=False)
        ## save the config and parameters as json
        for sim in y.keys():
            # convert parameter pandas to dict
            y[sim]["parameters"] = y[sim]["parameters"].to_dict(orient="index")
            
            # convert simulations results xarrays (e.g. emissions, gwp) to dict
            for analys in y[sim]["config"]["analyses"]:
                y[sim][analys] = y[sim][analys].to_dict()

            # for the moment filter the runs by their integer names, and delete completely (emissions and gwp already at level above)
            for run in [e for e in y[sim].keys() if isinstance(e, int)]:
                del y[sim][run]

            # delete FromSytemToSystem data
            if "FromSystemToSystem" in y[sim].keys():
                del y[sim]["FromSystemToSystem"]

        with open(os.path.abspath(os.path.join(os.path.abspath(output_path),os.path.relpath(filename))), 'w') as f:
            json.dump(y,f, sort_keys=False, indent=4, separators=(',', ': '), cls=EnhancedJSONEncoder)
        


    # save config as json, the parameter pandas as csv, emissions and gwp xarrays as hdf5 (possibly the easiest way to read in R)
    # NOTE: Adapt to timestep chain
    # TODO: Folder name is timestep (is written to info of simdata)

    def to_folder(self, subfolder=None, time_as_folder=True):
        """
        |  Save results simdict to a subfolder of simulation.
        |  
        | TODO:
        | - Possibly add writing of configs as json. They are already saved within the pickled file, but not in a human readable format. 
        """

        ## Define folder
        if time_as_folder and (subfolder is not None):
            path = Path(os.path.abspath(os.path.join(os.path.abspath(output_path), os.path.relpath(subfolder), os.path.relpath(self.info["time"].strftime("%Y-%m-%d_%H-%M-%S")))))
        elif time_as_folder and (subfolder is None):
            path = Path(os.path.abspath(os.path.join(os.path.abspath(output_path), os.path.relpath(self.info["time"].strftime("%Y-%m-%d_%H-%M-%S")))))
        elif subfolder is not None:           
            path = Path(os.path.abspath(os.path.join(os.path.abspath(output_path), os.path.relpath(subfolder))))
        else:
            path = Path(os.path.abspath(output_path))

        ## Create folder if it doesn't exist yet
        if not os.path.exists(path):
            path.mkdir(parents=True)

        print(f"Data is saved to folder: {path}")     

        ## First save all results to pickle
        with open(os.path.abspath(os.path.join(path, os.path.relpath("simdata.pkl"))), "wb") as f:
            pickle.dump(self, f)

        # This should use the copy (x), because it modifies the data (e.g. joins multiindex)
        x = deepcopy(self)

        for scen in x.info["scenarios"]:

            for t in x[scen]["timesteps"]:
                # save parameter pandas as csv:
                x[scen][t]["parameters"].index = x[scen][t]["parameters"].index.map('|'.join).str.strip()
                x[scen][t]["parameters"].to_csv(os.path.abspath(os.path.join(path, os.path.relpath(str(scen +"_parameters.csv")))), encoding="latin-1", quotechar='"', sep=";")
            
            # emissions and gwp as netcdf
            for analys in set(x[scen]["config"]["analyses"]).intersection(["emissions", "gwp"]):
                if isinstance(x[scen][analys], xr.DataArray):
                    # multiindex to simple
                    new_index = []
                    for w in x[scen][analys].coords["source_item"].data:
                        new_index.append('|'.join(w))
                    x[scen][analys].coords["source_item"] = xr.DataArray(new_index, dims="source_item")
                    x[scen][analys].to_netcdf(os.path.abspath(os.path.join(path, os.path.relpath(str(scen + "_" + analys +".nc")))), engine="netcdf4")

    def to_pickle(self, filename):
        """
        |  Pickle simdict to read it later on with the read_pickle function.
        """
        with open(os.path.abspath(os.path.join(os.path.abspath(output_path),os.path.relpath(filename))), "wb") as f:
            pickle.dump(self, f)

    def input_output_plots_pdf(self, filename, path, elements=["nitrogen", "carbon"]):
        """
        |  Create a pdf of input/output plots for all the systems of the simdict.
        |  **NOTE:**
        |  - Currently only for the first run (0)! And the last timestep

        """
        plots = []
        for scenario in self:
            print(f"start creating input/output plots for scenarios {scenario}")
            plots.append(text_plot(text=("scenario: " + scenario)))

            timestep = self[scenario]["config"]["simulation"]["timesteps"][-1] # only for the last timestep
            run = 0 # only for the first run
            # for sysname in self[scenario][timestep][run]["systems"]:
            # instead of running through all systems only go through the ones which are listed as sinks in the outputs table
            for sysname in self[scenario][timestep][run]["outputs"].index.get_level_values("sink").unique().dropna().to_list():
                sys = self[scenario][timestep][run]["systems"][sysname]
                if not isinstance(elements, list):
                    elements = [elements]
                for el in elements:
                    print(f"system: {sysname}, element: {el}")
                    # use the produced input as input
                    input = self[scenario][timestep][run]["outputs"].xs(sysname, level="sink")                    
                    if input is not None:
                        plot = input_output_plot(system=sys, sysname=sysname, input=input, element=el)
                    else:
                        plot = input_output_plot(system=sys, sysname=sysname, element=el)
                    plots.append(plot)
        save_as_pdf_pages(plots, filename=filename, path=path)


def read_pickle(filename):
    """
    |  Read in simdict files that have been saved to disk with the to_pickle method.
    """
    with open(os.path.abspath(os.path.join(output_path,os.path.relpath(filename))), "rb") as f:
        return pickle.load(f)