## Function to create a barplot of the global warming potential
# takes a the simdata file as input

library(reshape2)
library(ggplot2)


## for the moment, assume that on the x-axis there is the scenario, and colors destinguish the GHG gasses.
## at the moment also simply the first run is used

plot_gwp <- function(x, fill){
  
  data = data.frame()
  for (scenario in names(x)) {
    a <- melt(x[[scenario]][["gwp"]][1,,], varnames = c("object", "gas"))
    a$scenario <- scenario
    data = rbind(data, a)
  }
  
  ## How about indirect emissions from NH3 deposition ??
  
  plot <- ggplot(data, aes_string(x="scenario", y="value", fill=fill)) +
    geom_bar(stat="identity") +
#    scale_fill_brewer() +
    scale_y_continuous(name=expression(paste("GWP [", CO[2],"eq]",sep=""))) +
    theme_light()
 
  return (plot)
}