description:
This folder currently includes various plotting scripts to visualize model outputs. 
The scripts are mainly written for validation and evaluation purposes.  

author: Jannes Breier
date: 11.03.2019