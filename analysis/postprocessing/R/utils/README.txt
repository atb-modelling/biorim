description:
This folder contains utility functions that are required to read and process model related data. 
For now the functions or scripts are collected. For the future a package structure is planned.
author: Jannes Breier
date: 13.05.2019