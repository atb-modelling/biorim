import sys
import pandas as pd
import xarray as xr
import warnings
import itertools
import logging
from collections import namedtuple
from copy import deepcopy
from importlib import import_module
from importlib.util import find_spec
from collections import defaultdict
from analysis.economic.cash_flows import total_cashflow, npv_value

import external, material, production, transport
from tools import convert_molmasses, convert_to_gwp, indirect_N2ON_emissions, source_as_index
from declarations import Product, Emissions #Costs
from analysis import simdict


##################################################################


def unlist_index(x):
    idxlist = x.index.to_list()
    if len(idxlist) == 1:
        return "'" + idxlist[0] + "'"
    else:
        return idxlist

def start_simulation(configs):
    """
    | Compute a network analysis where different systems are evaluated and outputs of systems are passed as inputs to other systems.
    | The network is specified via a config file.
    
    :type configs: dict
    :param config: configuration file for the simulation
    :rtype: simdict
    :return: Returns a simdict (nested dict) containing all results (parameters, systems, outputs, ...) of the simulation

    |
    """
    def parameter_table(config, only_first = False): 
        """
        |  Function to create a Pandas table of unique parameter combinations.
        |  if only_frist = True, take only the first of several parameters 
        """

        systems = list(config["systems"].keys())

        # parameter lists for all systems
        prep_list = []
        for sys in systems:
            parameters = list(config["systems"][sys].values())
            if only_first:
                parameters = [[param] if not isinstance(param, list) else [param[0]] for param in parameters] # assure all single parameters are lists
            else:
                parameters = [[param] if not isinstance(param, list) else param for param in parameters] # assure all single parameters are lists
            prep_list.extend(parameters)

        # create unique combinations
        iter_list = list(itertools.product(*prep_list))

        # empty parameter table with correct dimensions
        ParamTable = pd.DataFrame()
        for sys in systems:
            ParamTable = pd.concat([ParamTable, pd.DataFrame(columns=list(range(len(iter_list))), index = pd.MultiIndex.from_product([[sys], list(config["systems"][sys].keys())], names=["system", "param"]))])

        # pass the values to the table
        # NOTE: This part also creates a deprecated warning: VisibleDeprecationWarning: Creating an ndarray from ragged nested sequences (which is a list-or-tuple of lists-or-tuples-or ndarrays with different lengths or shapes) is deprecated. If you meant to do this, you must specify 'dtype=object' when creating the ndarray
        # This seems similar to the old error: tuple instead of list.
        for run, run_param in enumerate(iter_list):
            for row, dat in enumerate(run_param):
                ParamTable[run][row] = dat # TODO: Try with list(dat)

        return ParamTable


    def check_config(config):
        """
        Function to check whether there are any obvious mistakes in the config
        e.g. systems defined which do not exist
        """

        # config type
        if not isinstance(config, dict):
            logger.debug("config needs to be of type dict")
            raise TypeError("config needs to be of type dict")

        # overall categories
        categories = ["simulation", "analyses", "systems"]
        missing_categories = [c for c in categories if c not in config.keys()]
        if len(missing_categories) > 0:
            warnings.warn(f"category {missing_categories} is missing in the config")

        # systems
        for system in config["systems"].keys():
            module = system.rsplit(".",1)[0]
            if module not in sys.modules:
                if find_spec(module) is None:
                    warnings.warn(f"module {module} does not exist")

        # timesteps
        # if not "timesteps" in config["simulation"]:
        #     print("timesteps have to be defined under 'simulation' in the config!")


    def create_simdata(configs):
        """
        Create simdata file with seperate configs for all scenarios and parameter combinations
        """
        logger.debug("Creating the simdata file")

        simdata = simdict()

        if not isinstance(configs, list):
            configs = [configs]

        # different configs provided are considered 'scenarios'
        # simdata["scenarios"] = [config["simulation"]["name"] for config in configs]
        simdata.info["scenarios"] = [config["simulation"]["name"] for config in configs]

        for config in configs:

            check_config(config)

            scenario = config["simulation"]["name"]

            simdata[scenario] = {}
            simdata[scenario]["config"] = config # the config as defined by the user

            logger.debug(f"Creating timesteps for scenario {scenario}")

            ## create configs for all timesteps
            if "timesteps" in simdata[scenario]["config"]["simulation"]:
                timesteps = config["simulation"]["timesteps"]
                if not isinstance(timesteps, list):
                    timesteps = [timesteps]
            else:
                timesteps = ["t1"]

            simdata[scenario]["timesteps"] = timesteps

            for t in timesteps:
                tconfig = deepcopy(config) # tconfig is a version of the config with the timesteps removed. One for each timestep.
                tconfig["simulation"]["timesteps"] = t
                for system in config["systems"]:
                    if "I" in config["systems"][system].keys():
                        if isinstance(config["systems"][system]["I"], dict):
                            if set(timesteps) == set(config["systems"][system]["I"]):
                                tconfig["systems"][system]["I"] = config["systems"][system]["I"][t]
                            else:
                                logger.debug(f"{system} not specified with the correct timesteps")
                                raise ValueError(f"{system} not specified with the correct timesteps")
                        

                simdata[scenario][t] = {}
                simdata[scenario][t]["config"] = tconfig

                if config["simulation"]["type"] == "single":
                    simdata[scenario][t]["parameters"] = parameter_table(tconfig, only_first = True)

                elif config["simulation"]["type"] == "all combinations":
                    simdata[scenario][t]["parameters"] = parameter_table(tconfig, only_first = False)

                else:
                    logger.debug(f'No option defined for {tconfig["simulation"]["type"]}')
                    raise ValueError(f'No option defined for {tconfig["simulation"]["type"]}')


                ## Dictionary which lists where products specified in the config have to be passed to
                # simdata[scenario][t]["FromSystemToSystem"] = {}

                # for system in tconfig["systems"]:
                #     if "I" in tconfig["systems"][system]:
                #         if tconfig["systems"][system]["I"] is not None:
                #             simdata[scenario][t]["FromSystemToSystem"][(tconfig["systems"][system]["I"]["source"][0], tconfig["systems"][system]["I"].index[0])] = system

                simdata[scenario][t]["SourceToSink"] = {}
                for sink in tconfig["systems"]:
                    if "I" in tconfig["systems"][sink]:
                        if tconfig["systems"][sink]["I"] is not None:
                            simdata[scenario][t]["SourceToSink"][(tconfig["systems"][sink]["I"]["source"][0], tconfig["systems"][sink]["I"].index[0])] = sink
                            # simdata[scenario][t]["SourceToSink"].append((tconfig["systems"][sink]["I"]["source"][0], tconfig["systems"][sink]["I"].index[0]), sink)

                simdata[scenario][t]["runs"] = simdata[scenario][t]["parameters"].columns.to_list()

                ## iterate over the runs (= the different parameter combinations of each scenario)
                for run in simdata[scenario][t]["runs"]:

                    simdata[scenario][t][run] = {}
                    simdata[scenario][t][run]["systems_executed_later"] = []
                    # simdata[scenario][t][run]["outputs"] = Product()
                    simdata[scenario][t][run]["outputs"] = Product()
                    simdata[scenario][t][run]["emissions"] = Emissions()
                    simdata[scenario][t][run]["systems"] = {}

                    # for each run create a config file again based on the parameters table
                    runconfig = deepcopy(tconfig)

                    for system in tconfig["systems"].keys():
                        for param in tconfig["systems"][system]:
                            runconfig["systems"][system][param] = simdata[scenario][t]["parameters"].loc[(system,param),run]

                    simdata[scenario][t][run]["config"] = runconfig

        return simdata


    def unique_system_id(system):
        """
        Checks which ids exist already for systems.
        If not default system exists already return id value of 1000.
        If default system with id > 1000 exists already, assign a larger number e.g. 1001
        """
        
        # existing systems and ids
        sys_id = [x.rsplit("_", 1) for x in simdata[scenario][t][run]["systems"]]

        exist_def_ids = defaultdict(list)
        
        for sys, id in sys_id:
            exist_def_ids[sys].append(int(id))

        if system in exist_def_ids:
            max_value = max(exist_def_ids[system])
            # use values over 1000 for default systems
            if max_value >= 1000:
                ID = max_value + 1
            else: # does exist as non-default already
                ID = 1000
        else: # doesn't exist at all
            ID = 1000
    
        return ID


    def system_arguments(system, config, I = None, O = None):
        """
        |  Creates the parameters from what is directly requested (I or O), and what is defined in the config (other parameters and I and O)
        |  :returns: dictionary of parameters that can be used to create instance
        |  :rtype: dict
        """
        # systems not specified with an id, are assigned one. Parameters are just the I or O.
        if not system[-1].isdigit():
            # print(f"{system} has no number. parameters as default")
            id = unique_system_id(system)

            logger.debug(f"Assigned id {id} to system {system}")

            if I is not None:
                param = {'I': I, 'id':id}
            elif O is not None:
                param = {'O': O, 'id':id}
            else:
                raise ValueError("Either I or O have to be specified for undefined system")
            return param

        # systems with an id take parameters which are a combination of config and I or O
        elif system[-1].isdigit():
            # print(f"{system} has digit and should be defined in the config")
            param = config["systems"][system].copy()

            # neither I nor O directly requested: simply take values from config (but add id if there is any)
            if (I is None) and (O is None):
                # check whether it is defined in the config
                if (config["systems"][system].get("I") is None) and (config["systems"][system].get("O") is None):
                    raise ValueError("Specify either I or O")

            # if O requested from system: define O as combination of config and requested
            elif (O is not None) and (I is None):

                # O is defined in the config
                if param.get("O") is not None:

                    # config defined paramters equal what is requested (e.g. for the reference system): leave as in config
                    if param["O"].equals(O):
                        pass

                    # if requesting system asks for 100%, and absolute values are defined in the config: use the values from the config
                    elif all(O["U"] == "%") and all(O["Q"] == 100):
                        if all(param["O"]["U"] != "%"):
                            pass
                        else:
                            raise ValueError("Percentage values requested via O and config.")
                        
                    # # if percentage values are specified in config, but they are not 100%: error
                    elif all(O["U"] == "%") and any(O["Q"] != 100):
                        raise ValueError("Only 100% values currently allowed")

                    # if quantities are not defined in the config, replace them by what is requested
                    elif any(param["O"]["Q"].isna()):
                        if param["O"].index.equals(O.index):
                            param["O"]["Q"] = O["Q"]
                            param["O"]["U"] = O["U"]
                        else:
                            print(f"Cannot define output quantities for system {system}")

                    # In all other cases, what is requested (O) overwrites the config
                    else:
                        warnings.warn(f"There seems to be a conflicting definition of O of system {system} in the config. O is overwritten!")
                        param["O"] = O

                # if O is not defined in the config: just set O to the one requested 
                else:
                    param["O"] = O


            # I put into system: define I as combination of config and passed
            elif (I is not None) and (O is None):

                # I is defined in the config
                if param.get("I") is not None:

                    # config defined paramters equal what is requested (e.g. for the reference system): leave as in config
                    if param["I"] is I:
                        pass

                    # if requesting system asks for 100% according to config: just leave as requested
                    elif all(param["I"]["U"] == "%") and all(param["I"]["Q"] == 100): # currently only 100% definition allowed
                        param["I"] = I

                    # if percentage values are specified in config, but they are not 100%: error
                    elif all(param["I"]["U"] == "%") and any(param["I"]["Q"] != 100):
                        raise ValueError("Only 100% values currently allowed")

                    # if quantities are not defined in the config, replace them by what is requested
                    elif any(param["I"]["Q"].isna()):
                        if param["I"].index.equals(I.index):
                            param["I"]["Q"] = I["Q"]
                            param["I"]["U"] = I["U"]
                        else:
                            print(f"Cannot define input quantities for system {system}")

                    # In all other cases, what is defined as I overwrites the config
                    else:
                        warnings.warn(f"There seems to be a conflicting definition of I of system {system} in the config. I is overwritten!")
                        param["I"] = I

                # if I is not defined in the config: just set I to the one requested 
                else:
                    param["I"] = I

            # both I and O requested at the same time: currently not allowed
            # TODO: enable this case. Should simply set both values according to the above approaches?
            elif (I is not None) and (O is not None):
                raise ValueError("Definition of both I and O currently not supported")

            ## as a last step, include the id
            param["id"] = system.rsplit("_",1)[1]
            return param
            # return {**{'id':ID}, **param}



    def input_produced_for_sink(requested_input, sink):
        """
        Controls the "outputs" table to see whether outputs have been produced already. If so return these?
        **NOTE**
        - Probably has to be adapted for the timestep_transfer case, where no source system is specified already
        - Should include quantity information as well? Some of the output for one system, some for another
        """
        ## create new index to access "outputs"
        requested_index = pd.MultiIndex.from_tuples([(t.source, t.Index, sink) for t in requested_input.itertuples()], names=["source", "item", "sink"])
        already_produced_index = simdata[scenario][t][run]["outputs"].index.intersection(requested_index)
        ProducedItems = None
        if len(already_produced_index) > 0:
            ProducedItems = simdata[scenario][t][run]["outputs"].loc[already_produced_index,:]

        missing_index = requested_index.difference(simdata[scenario][t][run]["outputs"].index)
        MissingItems = None
        if len(missing_index) > 0:
            MissingIndex = [(idx[0],idx[1]) for idx in missing_index]
            MissingItems = source_as_index(requested_input).loc[MissingIndex,:]

        ExistingMissing = namedtuple("Items", ['existingitems', 'missingitems'])
        return ExistingMissing(ProducedItems, MissingItems)


    ## add instance output to the list of outputs
    def add_to_outputs(Instance, system, sink=None):
        """
        |  Add the output of an instance to the list "outputs"
        |  If there is no "sink" specified, get it from the "SourceToSink" list
        |  If the instance has a timestep_transfer argument write the output to the respective timestep
        |  **NOTE**:
        | - Does not support several different systems for different outputs at the moment
        | - Specifying sink only works for systems without timestep_transfer argument
        """

        InstanceOutput = Instance.output()
        
        if InstanceOutput is not None:

            Output = InstanceOutput.copy()

            logger.info(f"output {unlist_index(Output)} will be added to list of outputs")

            # if there is a timestep_transfer argument then add the output to the respective timestep
            timestep_transfer = None
            if hasattr(Instance, "timestep_transfer"):
                timestep_transfer = Instance.timestep_transfer

            if timestep_transfer is not None:
                logger.debug(f"system {system} has time_transfer attribute")
                # print(f"Output is {Output}")

                timesteps = simdata[scenario]["config"]["simulation"]["timesteps"]
                timestep_no = timesteps.index(t) + timestep_transfer

                if timestep_no <= (len(timesteps)-1):
                    logger.debug(f"output is therefore written to timestep {timesteps[timestep_no]}")

                    out_t = timesteps[timestep_no]
                    Output.index = pd.MultiIndex.from_tuples([( system, i, simdata[scenario][out_t]["SourceToSink"].get((system, i)) ) for i in Output.index], names=["source", "item", "sink"])

                    ## NOTE: In the new approach only the output is copied to the outputs table, not the whole system. Reconsider this
                    simdata[scenario][out_t][run]["outputs"] = pd.concat([simdata[scenario][out_t][run]["outputs"], Output])

                else:
                    print(f"{system} tries to write output to not existing timestep")
                    logger.debug(f"{system} tries to write output to not existing timestep")

            else:
                # if sink is not specified: Try to figure it out from the config (SourceToSink) list
                if sink is None:
                    Output.index = pd.MultiIndex.from_tuples([(system, i, simdata[scenario][t]["SourceToSink"].get((system, i))) for i in Output.index], names=["source", "item", "sink"])
                else:
                    Output.index = pd.MultiIndex.from_tuples([(system, i, sink) for i in Output.index], names=["source", "item", "sink"])

                simdata[scenario][t][run]["outputs"] = pd.concat([simdata[scenario][t][run]["outputs"], Output])

        else:
            logger.info(f"There is no output of system {system}")


    def outputs_to_pass(Instance, source):
        """
        |  Creates a subset of the Output that needs to be passed on to other systems according to the "SourceToSink" dictionary.
        |  In case the class has a "timestep_transfer" argument which is not None, then no output is passed, because it is considered to be passed to other timestep.
        |  TODO:
        |  - Check whether this actually changes the Instance.output(). Then copying would be required.
        """

        # if the Instance has a timestep_transfer argument (which it not None), then assume nothing has to be passed this time
        timestep_transfer = None
        if hasattr(Instance, "timestep_transfer"):
            timestep_transfer = Instance.timestep_transfer

        if timestep_transfer is not None:
            print(f"No output from {source} to be passed within timestep because it is passed to other timestep")
            Output = Product()

        else:
            # print("line 441 outputs_to_pass: Instance.output before:\n", Instance.output())
            Output = Instance.output()
            if Output is not None:
                Output["source"] = source
                Output["sink"] = [simdata[scenario][t]["SourceToSink"].get((source, i)) for i in Output.index]
                # if there is no entry according to this list, drop the row
                Output = Output.dropna(axis="index", subset="sink")
                # print("line 449 outputs_to_pass: Instance.output after:\n", Instance.output())
            else:
                Output = Product()

        return Output


    def create_instance(system, param, sink=None):
        """
        |  Creates an instance of the system with the provided parameters, and adds the outputs (if any) to the list of "outputs"
        |  :return: The instance of the system
        """

        Module = system.rsplit(".",1)[0]
        Class = system.rsplit("_",1)[0]
        import_module(Module)

        # param = system_arguments(system, config, I=, O)
        Instance = eval(Class)(**param)
        simdata[scenario][t][run]["systems"][system] = Instance

        # add the output to the list of outputs
        add_to_outputs(Instance, system, sink=sink)

        return Instance


    def execute_system(system, config, I = None, O = None, sink = None):
        """


        ## For development only:
        scenario = "storage"
        t = "t1"
        run = 0
        simdata = create_simdata(configs)
        runconfig = simdata[scenario][t][run]["config"]
        config = runconfig
        starting_system = runconfig["simulation"]["starting_system"]
        system="production.plant.crop.ArableLand_1"
        I = None
        O = None
        execute_system(system="production.plant.crop.ArableLand_1", config=config, I = None, O = None) # test downstream modelling
        """

        print("")
        logger.info(f"started execute_system for {system}")

        # Paramters are defined as a combination of what is requested from the system directly, and the config values
        param = system_arguments(system, config, I = I, O = O)

        ### If neither I nor O directly requested/provided by other systems ###
        # just run as specified in config 
        if (I is None) and (O is None):
            # print(f"no I and O provided or requested directly (usually the reference system)")
            logger.info(f"no I and O provided or requested directly (usually the starting_system)")

            ## If input is defined for the reference system start with collecting these inputs (upstream modelling)
            if config["systems"][system].get("I") is not None:

                # check whether this has been produced already for this system
                In = input_produced_for_sink(requested_input=config["systems"][system]["I"], sink=system)

                if In.missingitems is not None:
                    # NOTE: run each system, not each item: all outputs of one system requested at a time
                    for source in In.missingitems.index.get_level_values("source"):
                        MissingItems = In.missingitems.index.get_level_values("item").to_list()
                        # print(f"further inputs {MissingItems} required for {system}")

                        logger.info(f"further input(s) {unlist_index(MissingItems)} required for {system}")

                        execute_system(system = source, config=config, O = In.missingitems.loc[system,:], sink = system)
                
                ## Use the input that has been created to far to use as input
                param["I"] = Product(In.existingitems.reset_index(level=["source", "sink"], drop=True))
                Instance = create_instance(system, param)

            ## If the reference system is defined by an output (O) then simply create it
            elif config["systems"][system].get("O") is not None:
                # print(f"creating Instance of {system}")

                logger.debug(f"creating Instance of {system}")

                Instance = create_instance(system=system, param=param)

            ## If additional_inputs are required, call the systems that produce this input
            AddInput = Instance.additional_input()
            if AddInput is not None:
                for idx, item in AddInput.iterrows():
                    # print(f"\nadditional input {idx} required for {system}")

                    logger.info(f"additional input {idx} required for {system}")

                    execute_system(system = item["source"], config=config, I = None, O = Product(AddInput.loc[[idx],:]), sink=system)

            ## As a last step, pass on all items that need to be passed on because they are input to another system
            # NOTE: This currently passes each item seperately. Should probably do this per system?
            
            logger.info("No more additional inputs required")
            
            OutputsToPass = outputs_to_pass(Instance=Instance, source=system)

            for idx in OutputsToPass.index:
                # print(f"{system} passes on output {idx}")

                print("")
                logger.info(f"{system} passes on output {idx}")

                execute_system(system = OutputsToPass.loc[idx,"sink"], config=config, I = OutputsToPass.loc[[idx],:], sink = None)


        ### The case of upstream modelling: O is requested from the system ###
        # This means that the system is initiated from a different system that requests an output.
        elif (O is not None) and (I is None):
            print(f"O {unlist_index(O)} requested: upstream modelling")

            # System not specified with an id (default system)
            # rename system to include the id
            if not system[-1].isdigit():
                system = system + "_" + str(param["id"])
                # print(f"creating instance of default system {system}")

                logger.info(f"creating instance of default system {system}")

                Instance = create_instance(system=system, param=param, sink=sink)

            elif config["systems"][system].get("I") is None:
                # print(f"creating instance of {system}")

                logger.info(f"creating instance of {system}")

                Instance = create_instance(system=system, param=param, sink=sink)

            # if there are more inputs required for this system, and these are not already available, the system has to be executed later
            else:
                warnings.warn("The case of additional I defined for upstream modelling is not finalized yet")

                # # TODO: Add check whether this has already been produced
                # param["I"]

                # input_produced_for_sink(requested_input=param["I"], sink=system)

                # SOME_INPUTS_MISSING = True
                # if SOME_INPUTS_MISSING:

                #     # add to the list of systems that need to be executed later on:
                #     simdata[scenario][t][run]["systems_executed_later"] = system

                # else:
                #     # TODO: I has to be all the inputs that are already produced
                #     Instance = create_instance(system=system, param=param, I=I)

            # TODO: If there is additional output besides the one already requested (e.g. waste) this should also be passed on

            ## If additional_inputs are required, call the systems that produce this input
            # NOTE: For the currently undefined case: consider whether this is done for both (the systems that are executed now, and those executed later). Don't execute twice
            AddInput = Instance.additional_input() 
            if AddInput is not None:
                for idx, item in AddInput.iterrows():
                    # print(f"\nadditional input {idx} required for {system}")

                    logger.info(f"additional input {idx} required for {system}")

                    execute_system(system = item["source"], config=config, I = None, O = Product(AddInput.loc[[idx],:]), sink=system)

        ### the case of downstream modelling: I is provided ##
        # This means that the system is initiated from a different system that passes on some input
        elif (I is not None) and (O is None):
            # print("I {I.index.to_list()} provided: downstream modelling")

            logger.info(f"I {unlist_index(I)} provided: downstream modelling")

            In = input_produced_for_sink(requested_input=config["systems"][system]["I"], sink = system)

            ## If there are other inputs required besides the ones passed, execute the providing systems
            if In.missingitems is not None:
                # logger.info("going into collection of missing inputs")

                # NOTE: run each system, not each item: all outputs of one system requested at a time
                for source in In.missingitems.index.get_level_values("source"):
                    MissingItems = In.missingitems.index.get_level_values("item").to_list()
                    # print(f"further inputs {MissingItems} required for {system}")

                    logger.info(f"further inputs {MissingItems} required for {system}")

                    execute_system(system = source, config=config, O = In.missingitems.loc[source,:], I = None, sink = system)

            In = input_produced_for_sink(requested_input = config["systems"][system]["I"], sink=system)

            # use the input that has been created so far to use as input
            param["I"] = Product(In.existingitems.reset_index(level=["source", "sink"], drop=True))
            # print(f"\nInputs have been collected. Creating instance of {system}")

            logger.info(f"Inputs have been collected. Creating instance of {system}")

            Instance = create_instance(system, param)

            ## Check whether additional inputs are required. If so execute respective systems
            # NOTE: This iterates over items not systems. Consider changing. However, all functions need to be able to deal with this (e.g. ArableLand)
            AddInput = Instance.additional_input()
            if AddInput is not None:
                for idx, item in AddInput.iterrows():
                    # print(f"\nadditional input {idx} required for {system}")

                    logger.info(f"additional input {idx} required for {system}")

                    execute_system(system = item["source"], config=config, I = None, O = Product(AddInput.loc[[idx],:]), sink=system)

            logger.info("No more additional inputs required")

            ## As a last step, add if there is some output that has to be passed
            # NOTE: This currently passes each item seperately. Should probably do this per system?
            OutputsToPass = outputs_to_pass(Instance=Instance, source=system)

            # NOTE: The outputs_to_pass function should probably consider whether they have been "passed" to another timestep already

            for idx in OutputsToPass.index:
                # print(f"{system} passes on output {idx}")

                logger.info(f"{system} passes on output {idx}")

                execute_system(system = OutputsToPass.loc[idx,"sink"], config=config, I = OutputsToPass.loc[[idx],:], sink = None)


    def collect_emissions_systems(scenario, t, run):
        """
        |  Write the emissions from all systems into one table
        |  NOTE: Consider to write zero values for systems that return None.
        """
        logger.debug(f"collect_emissions_systems for scenario {scenario}")

        Emis = Emissions()
        for sys in simdata[scenario][t][run]["systems"]:
            SysEmis = simdata[scenario][t][run]["systems"][sys].emissions()
            if SysEmis is not None:
                Emis = pd.concat([Emis, source_as_index(SysEmis, source=sys)])
        simdata[scenario][t][run]["emissions"] = Emis

    def collect_cashflows_systems(scenario, t, run):
        """
        |  Collects cashflows from systems
        """
        logger.debug(f"collect_cashflows_systems for scenario {scenario}")

        Cashflow = pd.DataFrame()
        for sys in simdata[scenario][t][run]["systems"]:
            print(f"Collecting cashflows for system {sys}")

            SysCashflow = simdata[scenario][t][run]["systems"][sys].cashflow()
            print("SysCashflow", SysCashflow)
            if SysCashflow is not None:
                SysCashflow.index = pd.MultiIndex.from_tuples([(sys, idx) for idx in SysCashflow.index])
                Cashflow = pd.concat([Cashflow, SysCashflow])

        simdata[scenario][t][run]["cashflow"] = Cashflow


    def emis_xr_runs(simdata, scenario, indirect_N2O=True, outtype="molecule"): 
        """
        Create a xarray of emissions data

        NOTE:
        This should indepenently collect all the information that is there
        """
        logger.debug(f"emis_xr_runs for scenario {scenario}")

        Emisxr = xr.DataArray()

        Emisxr_t = []

        for t in simdata[scenario]["timesteps"]:
            
            runs = simdata[scenario][t]["runs"]
            Emisxr_t.append(xr.concat([xr.DataArray(simdata[scenario][t][run]["emissions"]) for run in runs], dim="run"))

        Emisxr = xr.concat(Emisxr_t, dim="timestep")
        Emisxr = Emisxr.rename({'dim_0':"source_item", 'dim_1': "element"})
        Emisxr = Emisxr.assign_coords(timestep=simdata[scenario]["timesteps"],
                                      run=simdata[scenario][t]["runs"])

        if indirect_N2O:
            Emisxr = indirect_N2ON_emissions(Emisxr, climate="wet climate")

        if outtype == "element":
            pass
        elif outtype == "molecule":
            Emisxr = convert_molmasses(Emisxr, to="molecule", keep_unconverted=False)
        else:
            print("Invalid outtype selected for gwp analysis")

        return Emisxr


    #####################################
    # where the modelling actually starts

    ## create log file in the root directory of simulation
    # This is overwritten with every model run.
    # However, the result should also be part of the simdict

    # remove the default StreamHandler added by basicConfig
    logging.getLogger().handlers = []

    # create a StreamHandler that creates the output for the console (and replaces print statements)
    stream_formatter = logging.Formatter('%(message)s')
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(stream_formatter)
    stream_handler.setLevel(logging.INFO)

    # create a FileHandler that writes to logfile.log
    file_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    file_handler = logging.FileHandler('simulation/logfile.log')
    file_handler.setFormatter(file_formatter)
    file_handler.setLevel(logging.DEBUG)

    # create a logger and add the handlers
    logger = logging.getLogger()
    logger.addHandler(stream_handler)

    # set the logging level to INFO
    logger.setLevel(logging.INFO)

    logger.addHandler(file_handler)




    simdata = create_simdata(configs)

    for scenario in simdata.info["scenarios"]:


        print(f"""
###########################################################################################################
#                                                                                                         #
#  Started modelling for scenario {scenario}
#                                                                                                         #
###########################################################################################################
        """)


        for t in simdata[scenario]["timesteps"]:

            print(f"\n### timestep {t} ###")

            for run in simdata[scenario][t]["runs"]:

                # NOTE: for testing:
                # run = 0
                # t = "t2"
                # scenario = "storage"

                print(f"\n## run {run} ##\n")

                logger.debug(f"Creating runconfig for timestep {t} and run {run}")
                runconfig = simdata[scenario][t][run]["config"]
                starting_system = runconfig["simulation"]["starting_system"]

                logger.debug(f"execute_system for starting_system {starting_system}")
                execute_system(system = starting_system, config = runconfig)
                
                ## collect the emission information of all systems of one run
                if ("emissions" in simdata[scenario]["config"]["analyses"]) or ("gwp" in simdata[scenario]["config"]["analyses"]):
                    collect_emissions_systems(scenario, t=t, run=run)

                if "cashflows" in simdata[scenario]["config"]["analyses"]:
                    print("\ncollecting expenditures from all systems")
                    collect_cashflows_systems(scenario, t=t, run=run)

                    total_cashflow(simdata=simdata, scenario=scenario, run=run, t=t)
                    npv_value(simdata=simdata, scenario=scenario, run=run, t=t)
                    # NOTE: Here could be a function to actually calculate the cashflows based on the expenditures previously collected in
                    # simdata[scenario][timestep][run]["expenditure"]
                    # The underlying function could also be in a different module "economic" within the "analysis" module, but be called here
                    # e.g. similar to:
                    # calculate_cashflows(expeditures = simdata[scenario][timestep][run]["expenditure"], ) 

        # Add here that emissions of several runs are added together 
        if "emissions" in simdata[scenario]["config"]["analyses"]:
            print("\ncollecting emissions from all systems")
            outtype = simdata[scenario]["config"]["analyses"]["emissions"]["outtype"]
            simdata[scenario]["emissions"] = emis_xr_runs(simdata, scenario, outtype=outtype)

        if "gwp" in simdata[scenario]["config"]["analyses"]:
            print("\ncollecting global warming potential converted emissions from all systems")
            timerange = simdata[scenario]["config"]["analyses"]["gwp"]["timerange"]
            simdata[scenario]["gwp"] = convert_to_gwp(simdata[scenario]["emissions"], timerange=timerange)      

    return simdata