import pandas as pd
import numpy_financial as npf

def numfmt(x, pos): # custom formatter function: divide by 1000.0
    """
    Custom formatter function to dive axis by 1000. To be used together with npv_graphic method.
    """
    th = '{}'.format(x / 1000)
    return th

def total_cashflow(simdata, scenario, t, run):
    """
    Returns the cash flow for the number of years, based on the operation, investment and financial method.
    """        
    cashflow_simdict = simdata[scenario][t][run]["cashflow"]
    range_years=range(1, cashflow_simdict["period_years"].max()+1)

    total_cash_flow=pd.DataFrame(index=[], columns=[])
    for year in range_years:
        # print(f"Calculating cash flow for year {year}")
        total_cash_flow.loc["operation", year]=cashflow_simdict.loc[cashflow_simdict["section"]=="opex", "EUR"].sum()*-1
        if cashflow_simdict.loc[cashflow_simdict["section"]=="capex", "EUR"].sum()>0:
            if year==1:
                total_cash_flow.loc["investment", year]=cashflow_simdict.loc[cashflow_simdict["section"]=="capex", "EUR"].sum()*-1
                total_cash_flow.loc["operation", year]=0
    
        total_cash_flow.loc["total", year]=total_cash_flow[year].sum()
       
    simdata[scenario][t][run]["total_cashflow"] = total_cash_flow

    cashflow_df=simdata[scenario][t][run]["cashflow"]
    total_capex=cashflow_df[cashflow_df["section"]=="capex"]["EUR"].sum()
    total_opex_pos=cashflow_df[(cashflow_df["section"]=="opex") & (cashflow_df["EUR"]>0)]["EUR"].sum()
    total_opex_neg=cashflow_df[(cashflow_df["section"]=="opex") & (cashflow_df["EUR"]<0)]["EUR"].sum()

    for item in cashflow_df.index:
        if cashflow_df.loc[item, "section"]=="capex":
            cashflow_df.loc[item, "%"]=cashflow_df.loc[item, "EUR"]/total_capex*100
        if cashflow_df.loc[item, "section"]=="opex" and cashflow_df.loc[item, "EUR"]>0:
            cashflow_df.loc[item, "%"]=cashflow_df.loc[item, "EUR"]/total_opex_pos*100
        if cashflow_df.loc[item, "section"]=="opex" and cashflow_df.loc[item, "EUR"]<0:
            cashflow_df.loc[item, "%"]=cashflow_df.loc[item, "EUR"]/total_opex_neg*100

def npv_value(simdata, scenario, t, run):
    """
    Returns the net present value (NPV) at the last year of the years given. 
    """
    total_cash_flow= simdata[scenario][t][run]["total_cashflow"] 

    # print("cashflow_simdict", total_cash_flow)
    last_year = int(total_cash_flow.columns[-1])
    cash_flows = list(total_cash_flow.loc["total",:])
    DISCOUNT_RATE = simdata[scenario]["config"]["analyses"]["cashflows"]["discount_rate"]
    # print("cash_flows: \n", cash_flows)
    # print("DISCOUNT_RATE", DISCOUNT_RATE)
    # npv_value = npf.npv(DISCOUNT_RATE, cash_flows)
    # print (f"The NPV value of the project {scenario} at the year {last_year} is {npv_value}")
    # simdata[scenario][t][run]["npv_value"] = npv_value


    # for a list of discount rates
    if type(DISCOUNT_RATE) == list:
            npv_value=[]
            for i in range(0, len(DISCOUNT_RATE)):
                # print("cash_flows: \n", cash_flows)
                # print("DISCOUNT_RATE", DISCOUNT_RATE[i])
                npv_value.append(npf.npv(DISCOUNT_RATE[i], cash_flows))
                # print (f"The NPV value of the project {scenario} at the year {last_year} is {npv_value}")
                simdata[scenario][t][run]["npv_value"] = npv_value

    else:
        npv_value = npf.npv(DISCOUNT_RATE, cash_flows)
        print (f"The NPV value of the project {scenario} at the year {last_year} is {npv_value}")
        simdata[scenario][t][run]["npv_value"] = npv_value



# NOTE: A variant of total_cashflow that could include depreciation and taxes
# def total_cashflow(simdata, scenario, t, run):
#     """
#     Returns the cash flow for the number of years, based on the operation, investment and financial method.
#     """        
#     cashflow_simdict = simdata[scenario][t][run]["cashflow"]
#     method=simdata[scenario]["config"]["analyses"]["cashflows"]["method"]
#     range_years=range(1, cashflow_simdict["period_years"].max()+1)

#     total_cash_flow=pd.DataFrame(index=[], columns=[])
#     for year in range_years:
#         print(f"Calculating cash flow for year {year}")
#         if method=="sum":
#             total_cash_flow.loc["operation", year]=cashflow_simdict.loc[cashflow_simdict["section"]=="opex"].sum()[0]*-1
#             if cashflow_simdict.loc[cashflow_simdict["section"]=="capex"].sum()[0]>0:
#                 if year==1:
#                     total_cash_flow.loc["investment", year]=cashflow_simdict.loc[cashflow_simdict["section"]=="capex"].sum()[0]*-1
#                     total_cash_flow.loc["operation", year]=0
#             total_cash_flow.loc["total", year]=total_cash_flow[year].sum()
        
#         elif method=="OCF":
#             # NOTE: OCF (Operating Cash Flow)
#             # Based on the following formula: (Cᵢₙ − Cₒᵤₜ − D) × (1 − t) + D
#             # where: 
#             #   Cᵢₙ: cash inflow
#             #   Cₒᵤₜ: cash outflow
#             #   D: Depreciation
#             #   t: tax rate
            
#             CASH_IN_AND_OUT=cashflow_simdict.loc[cashflow_simdict["section"]=="opex"].sum()[0]*-1
#             DEPRECIATION=cashflow_simdict.loc[cashflow_simdict["section"]=="depreciation"]["EUR"][0]
#             TAX=cashflow_simdict.loc[cashflow_simdict["section"]=="tax"]["abs"][0]

#             total_cash_flow.loc["operation", year]=(CASH_IN_AND_OUT-DEPRECIATION)*(1-TAX)+DEPRECIATION  

#             if cashflow_simdict.loc[cashflow_simdict["section"]=="capex"].sum()[0]>0:
#                 if year==1:
#                     total_cash_flow.loc["investment", year]=cashflow_simdict.loc[cashflow_simdict["section"]=="capex"].sum()[0]*-1
#                     total_cash_flow.loc["operation", year]=0
#             total_cash_flow.loc["total", year]=total_cash_flow[year].sum()
       
#     simdata[scenario][t][run]["total_cashflow"] = total_cash_flow

#     cashflow_df=simdata[scenario][t][run]["cashflow"]
#     total_capex=cashflow_df[cashflow_df["section"]=="capex"]["EUR"].sum()
#     total_opex_pos=cashflow_df[(cashflow_df["section"]=="opex") & (cashflow_df["EUR"]>0)]["EUR"].sum()
#     total_opex_neg=cashflow_df[(cashflow_df["section"]=="opex") & (cashflow_df["EUR"]<0)]["EUR"].sum()

#     for item in cashflow_df.index:
#         if cashflow_df.loc[item, "section"]=="capex":
#             cashflow_df.loc[item, "%"]=cashflow_df.loc[item, "EUR"]/total_capex*100
#         if cashflow_df.loc[item, "section"]=="opex" and cashflow_df.loc[item, "EUR"]>0:
#             cashflow_df.loc[item, "%"]=cashflow_df.loc[item, "EUR"]/total_opex_pos*100
#         if cashflow_df.loc[item, "section"]=="opex" and cashflow_df.loc[item, "EUR"]<0:
#             cashflow_df.loc[item, "%"]=cashflow_df.loc[item, "EUR"]/total_opex_neg*100