import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from simulation.data_management_and_plotting.scenarios_plotting import LegendWithoutDuplicate
from matplotlib.lines import Line2D


sce_colors={"composting": "#E53935", #1
            "biogas": "#60CC9E",#1 
            "gasification": "#28864A",#2
            #"cofiring": "#1A639E",#3
            "biogas": "#1F2988",#4
            "biogas (with NaOH pretreatment)": "#bf00ff",
            "biogas and composting":"#AB4242", #5
            "gasification (with pelletization)":"#D49C36",#6
            "cofiring":"#6D4C41",#7 
            # "cofiring and pelletization":"#B3B296", #8
            "cofiring - wet":"#B3B296", #8
            "cofiring (with pelletization)":"#B3B296"}

color_1="#7b7b7b"
color_2="#cdcdcd"

sensitivity_markers={"electrical efficiency": "h",
                     "discount rate": "*",
                     "transport distance": "d",
                     "dryness level": "s",
                     "drying level": "+", 
                     "electricity tariff":"v",
                     "fixed_carbon": "o"
                    }


def color_palette(dict_colors):
    fig, ax =plt.subplots()
    y_ticks=[]
    for k,v in dict_colors.items():
        ax.barh(k, 1, color=v)
        y_ticks.append(k)
    ax.set_yticklabels(y_ticks, fontsize=18, rotation=0)
    plt.tight_layout()
    plt.show()

#color_palette(sce_colors) #NOTE: to check the color palette, uncomment this line and execute the function with the dictionary

#functions for gradient color palettes
def hex_to_RGB(hex_str):
    """ #FFFFFF -> [255,255,255]"""
    #Pass 16 to the integer function for change of base
    return [int(hex_str[i:i+2], 16) for i in range(1,6,2)]

def get_color_gradient(c1, c2, n):
    """
    Given two hex colors, returns a color gradient
    with n colors.
    """
    assert n > 1
    c1_rgb = np.array(hex_to_RGB(c1))/255
    c2_rgb = np.array(hex_to_RGB(c2))/255
    mix_pcts = [x/(n-1) for x in range(n)]
    rgb_colors = [((1-mix)*c1_rgb + (mix*c2_rgb)) for mix in mix_pcts]
    return ["#" + "".join([format(int(round(val*255)), "02x") for val in item]) for item in rgb_colors]

def formatter_million(x, pos):
    return str(round(x / 1e6, 1))


class MACC():

    def __init__(self, scenarios_simdict=[], BAU_scenario="compost", lifetime=20, GHG_unit="tonnes", sensitivity=False, sensitivity_disc=False, obs=None):
        
        self.scenarios_simdict=scenarios_simdict
        self.BAU_scenario=BAU_scenario
        self.lifetime=lifetime
        self.GHG_unit=GHG_unit
        self.sensitivity=sensitivity
        self.sensitivity_disc=sensitivity_disc
        self.obs=obs

        name_scenarios=[]
        for simdict in self.scenarios_simdict:
            name=list(simdict.keys())[0]
            name_scenarios.append(name)
        
        if BAU_scenario not in name_scenarios:
            raise ValueError(f"BAU scenario {self.BAU_scenario} must be within the list of scenarios names")
     
    def marginal_abatement_cost(self):
        name_scenarios=[]
        for simdict in self.scenarios_simdict:
            name=list(simdict.keys())[0]
            name_scenarios.append(name)

        print (f"GHG emissions will be calculated at {self.GHG_unit}")
        if self.GHG_unit=="tonnes":
            FACTOR=1000
        elif self.GHG_unit=="kg":
            FACTOR=1
        else:
            raise ValueError (f"Unit not included, please with 'tonnes' or 'kg'")

        GHG_emissions=[]
        for simdict in self.scenarios_simdict:
            name_scenario=list(simdict.keys())[0]
            GHG_emissions.append(simdict[name_scenario]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas()/FACTOR)

        emissions_projects=pd.DataFrame(index=name_scenarios, columns=["GWP_100_emissions", "BAU"])
        emissions_projects["GWP_100_emissions"]=GHG_emissions

        for scenario in emissions_projects.index:
            if scenario==self.BAU_scenario:
                emissions_projects.loc[scenario,"BAU"]=True
            else:
                emissions_projects.loc[scenario,"BAU"]=False
        
        npv=[]
        for simdict in self.scenarios_simdict:
            name_scenario=list(simdict.keys())[0]
            npv.append(simdict[name_scenario]["t1"][0]["npv_value"])
        
        npv_project=pd.DataFrame(index=name_scenarios, columns=[])
        npv_project["NPV"]=npv

        alternative_scenarios=emissions_projects[emissions_projects["BAU"]==False]
        BAU_scenario=emissions_projects[emissions_projects["BAU"]==True]

        abatement_df=pd.DataFrame(index=[], columns=[])
        for alternative in alternative_scenarios.index:
            abatement_df.loc[alternative, "CO2eq_abatement"]=(BAU_scenario.loc[self.BAU_scenario, "GWP_100_emissions"].sum().sum()-
                                                                alternative_scenarios.loc[alternative,"GWP_100_emissions"].sum().sum()) * self.lifetime

        emissions_projects=emissions_projects
        npv_values=npv_project
        emissions_abatement=abatement_df
        marginal_abatement=pd.DataFrame(index=[], columns=["emissions - GWP100", "NPV", "marginal abatement", "MAC"])
        for item in emissions_projects.index:
            # emissions according to the project's lifetime 
            marginal_abatement.loc[item, "emissions - GWP100"]=emissions_projects.loc[item, "GWP_100_emissions"].sum().sum() * self.lifetime 
        marginal_abatement["NPV"]=npv_values["NPV"]
        for item in emissions_abatement.index:
            marginal_abatement.loc[item,"marginal abatement"]=emissions_abatement.loc[item, "CO2eq_abatement"]
        marginal_abatement["MAC"]=-1*marginal_abatement["NPV"]/marginal_abatement["marginal abatement"]
        marginal_abatement=marginal_abatement.sort_values(by=["MAC"], ascending=True)
        marginal_abatement["type"]="default"
        marginal_abatement["obs"]=self.obs

        sensitivity_df=pd.DataFrame(index=[], columns=[])

        if self.sensitivity==True:
            for result in self.scenarios_simdict:
                name_scenario=list(result.keys())[0]
                sens_numb=[key for key in result[name_scenario]["t1"].keys() if type(key) is int]
                for number in sens_numb:
                    if number==0:
                        pass
                    else:
                        sensitivity_df.loc[f"{name_scenario} - {number}", "emissions - GWP100"]=(result[name_scenario]["gwp"].loc["t1",number,:,:,"GWP100"].to_pandas().sum().sum() * self.lifetime) / FACTOR
                        sensitivity_df.loc[f"{name_scenario} - {number}", "NPV"]=result[name_scenario]["t1"][number]["npv_value"]
                        sensitivity_df.loc[f"{name_scenario} - {number}", "type"] = "sensitivity"
                        sensitivity_df.loc[f"{name_scenario} - {number}", "obs"] = self.obs
        else:
            pass

        if self.sensitivity_disc==True:
            scenarios_simdict_mod=[result for result in self.scenarios_simdict if list(result.keys())[0]!=self.BAU_scenario]
            for result in scenarios_simdict_mod:
                name_scenario=list(result.keys())[0]
                sens_numb=[key for key in result[name_scenario]["t1"].keys() if type(key) is int]
                for number in sens_numb:
                    sensitivity_df.loc[f"{name_scenario} - {number}", "emissions - GWP100"]=(result[name_scenario]["gwp"].loc["t1",number,:,:,"GWP100"].to_pandas().sum().sum() * self.lifetime) / FACTOR
                for sim in range(0,len(result[name_scenario]["t1"][0]["npv_value"])):
                    sensitivity_df.loc[f"{name_scenario} - {sim}", "NPV"]=result[name_scenario]["t1"][0]["npv_value"][sim]
                    sensitivity_df.loc[f"{name_scenario} - {sim}", "emissions - GWP100"]=result[name_scenario]["gwp"].loc["t1",0,:,:,"GWP100"].to_pandas().sum().sum() * self.lifetime / FACTOR
                    sensitivity_df.loc[f"{name_scenario} - {sim}", "type"] = "sensitivity"
                    sensitivity_df.loc[f"{name_scenario} - {sim}", "obs"] = self.obs
        else:
            pass
        
        final_df=pd.concat([marginal_abatement, sensitivity_df])
        #correction for NPV display as a list
        for i in final_df.index:
            if type(final_df.loc[i, "NPV"])==list:
                final_df.loc[i, "NPV"]=final_df.loc[i, "NPV"][0]
            if i[-4:]==" - 0":
                final_df.drop(i, inplace=True)

        return final_df
        #TODO: Include the option of including electrical and thermal energy in the dataset


def single_graphic_MACC(database, ax=None, BAU_scenario="composting", outer_line=False, show_legend=True, show_art=True, save_fig_memory=False,
                        GHG_unit="tonnes", sensitivity_analysis=False, **kwargs):

    defaultKwargs={"fontsize":18, 
                    "loc": 9, 
                    "ncol": 4}
    
    kwargs={**defaultKwargs, **kwargs}
    
    MAC_df=database.drop(BAU_scenario)

    #NOTE: Create a fig only it is desired to depict the legend, otherwise no fig should be created due to blank figure plotting in the multiple plotting
    if show_legend==True:
        fig, ax=plt.subplots()
    else:
        ax=ax or plt.gca()

    x_pos=0

    if sensitivity_analysis==True:
        # MAC_df=database[0]
        sens_pallette=get_color_gradient(color_1,color_2,len(list(MAC_df.index)))
        x_ticks=list(MAC_df.index)
        x_pos=0
        upper_xticks=[]

        for scenario in MAC_df.index:
            ax.bar(x_pos+abs(MAC_df.loc[scenario, "marginal abatement"])/2, MAC_df.loc[scenario, "MAC"], color=sens_pallette[MAC_df.index.get_loc(scenario)], width=MAC_df.loc[scenario, "marginal abatement"]) 
            upper_xticks.append((x_pos+abs(MAC_df.loc[scenario, "marginal abatement"])/2))
            x_pos=x_pos+abs(MAC_df.loc[scenario, "marginal abatement"])

        x_pos=0
        ax.bar(x_pos+abs(MAC_df.iloc[0]["marginal abatement"])/2, MAC_df.iloc[0]["MAC"], color=sce_colors[MAC_df.iloc[0].name], width=MAC_df.iloc[0]["marginal abatement"])
        ax.margins(x=0)
        ax1=ax.twiny()
        ax1.set_xlim(ax.get_xlim())
        ax1.set_xticks(upper_xticks)
        ax1.set_xticklabels(x_ticks)

    else:
        if outer_line==True:
            for scenario in MAC_df.index:
                ax.bar(x_pos+abs(MAC_df.loc[scenario, "marginal abatement"])/2, MAC_df.loc[scenario, "MAC"], color="none", edgecolor=sce_colors[scenario],width=MAC_df.loc[scenario, "marginal abatement"]) 
                x_pos=x_pos+abs(MAC_df.loc[scenario, "marginal abatement"])
        else:
            for scenario in MAC_df.index:
                ax.bar(x_pos+abs(MAC_df.loc[scenario, "marginal abatement"])/2, MAC_df.loc[scenario, "MAC"], color=sce_colors[scenario], width=MAC_df.loc[scenario, "marginal abatement"]) 
                x_pos=x_pos+abs(MAC_df.loc[scenario, "marginal abatement"])

    if show_art==True:
        ax.set_xlabel(f"emissions abatement\n({GHG_unit} CO₂eq)", fontsize=kwargs["fontsize"])
        ax.set_ylabel(f"cost per GHG emissions\n(EUR/{GHG_unit} CO₂eq)", fontsize=kwargs["fontsize"])
        ax.tick_params("both",labelsize=(kwargs["fontsize"]-2))
    else:
        ax.set(xlabel=None)
        ax.set(ylabel=None)
        ax.tick_params("both", labelsize=kwargs["fontsize"]-2)
    
    ax.axhline(0, color='black', linestyle="--", label='_nolegend_') # label='_nolegend_' is for ignoring the handle into the label

    if show_legend==True:
        fig.legend(MAC_df.index, fontsize=kwargs["fontsize"], loc=kwargs["loc"], ncol=kwargs["ncol"])
    else:
        pass
    
    if save_fig_memory==True:
        return ax
    else: 
        plt.show()


def multiple_graphic_MACC(left_column=[], right_column=[], type="2x3", GHG_unit="tonnes", sensitivity_anaylisis=False, **kwargs):

    defaultKwargs={"fontsize":18, 
                    "loc": 9, 
                    "ncol": 3,
                    "supxlabel_pos": [0.5, 0.1], 
                    "supylabel_pos": [0.1, 0.5], 
                    "sharey": False, 
                    "BAU_scenario":"composting", 
                    "left_extra_title": ["","",""],
                    "right_extra_title":["","",""], 
                    "side_sharex":"left"}
    
    kwargs={**defaultKwargs, **kwargs}

    #NOTE: More type of axes distribution can be added
    if type=="2x3":
        fig=plt.figure(figsize=(12,12), dpi=80)
        if kwargs["sharey"]==True:
            if kwargs["side_sharex"]=="left":
                #TODO: at the moment this setting is a quick manual fix, but not the logical. It has to be adquated to automatization. 
                ax326=fig.add_subplot(326)
                ax325=fig.add_subplot(325, sharex=ax326, sharey=ax326)
                ax321=fig.add_subplot(321, sharex=ax325, sharey=ax325)
                ax322=fig.add_subplot(322, sharex=ax326, sharey=ax326)
                ax323=fig.add_subplot(323, sharex=ax321, sharey=ax321)
                ax324=fig.add_subplot(324, sharex=ax322, sharey=ax322)

            elif kwargs["side_sharex"]=="right":
                ax322=fig.add_subplot(322)
                ax321=fig.add_subplot(321, sharex=ax322, sharey=ax322)
                ax323=fig.add_subplot(323, sharex=ax321)
                ax324=fig.add_subplot(324, sharex=ax322, sharey=ax323)
                ax325=fig.add_subplot(325, sharex=ax323)
                ax326=fig.add_subplot(326, sharey=ax325)
        else:
            ax321=fig.add_subplot(321)
            ax322=fig.add_subplot(322)
            ax323=fig.add_subplot(323, sharex=ax321)
            ax324=fig.add_subplot(324, sharex=ax322)
            ax325=fig.add_subplot(325)
            ax326=fig.add_subplot(326)

        single_graphic_MACC(left_column[0], ax321, show_art=False, show_legend=False, save_fig_memory=True, sensitivity_analysis=sensitivity_anaylisis)
        extra_title=kwargs["left_extra_title"][0]
        ax321.set_title(f"(a){extra_title}", fontsize=kwargs["fontsize"])
        ax321.tick_params("both", labelsize=kwargs["fontsize"]-2)
        single_graphic_MACC(left_column[1], ax323, show_art=False, show_legend=False, save_fig_memory=True, sensitivity_analysis=sensitivity_anaylisis)
        extra_title=kwargs["left_extra_title"][1]
        ax323.set_title(f"(b){extra_title}", fontsize=kwargs["fontsize"])
        ax323.tick_params("both", labelsize=kwargs["fontsize"]-2)
        single_graphic_MACC(left_column[2], ax325, show_art=False, show_legend=False, save_fig_memory=True, sensitivity_analysis=sensitivity_anaylisis)
        extra_title=kwargs["left_extra_title"][2]
        ax325.set_title(f"(c){extra_title}", fontsize=kwargs["fontsize"])
        ax325.tick_params("both", labelsize=kwargs["fontsize"]-2)

        single_graphic_MACC(right_column[0], ax322, show_art=False, show_legend=False, save_fig_memory=True, sensitivity_analysis=sensitivity_anaylisis)
        extra_title=kwargs["right_extra_title"][0]
        ax322.set_title(f"(d){extra_title}", fontsize=kwargs["fontsize"])
        ax322.tick_params("both", labelsize=kwargs["fontsize"]-2)        
        single_graphic_MACC(right_column[1], ax324, show_art=False, show_legend=False, save_fig_memory=True, sensitivity_analysis=sensitivity_anaylisis)
        extra_title=kwargs["right_extra_title"][1]
        ax324.set_title(f"(e){extra_title}", fontsize=kwargs["fontsize"])
        ax324.tick_params("both", labelsize=kwargs["fontsize"]-2)        
        single_graphic_MACC(right_column[2], ax326, show_art=False, show_legend=False, save_fig_memory=True, sensitivity_analysis=sensitivity_anaylisis)
        extra_title=kwargs["right_extra_title"][2]
        ax326.set_title(f"(f){extra_title}", fontsize=kwargs["fontsize"])
        ax326.tick_params("both", labelsize=kwargs["fontsize"]-2)

        #NOTE: when all subplots are sharing the same X and Y, the ticks must remain
        # ax321.set_xticks([])
        # ax323.set_xticks([])
        # ax322.set_xticks([])
        # ax324.set_xticks([])

        fig.supxlabel(f"emissions abatement\n({GHG_unit} CO₂eq)", x=kwargs["supxlabel_pos"][0], y=kwargs["supxlabel_pos"][1], fontsize=kwargs["fontsize"])
        fig.supylabel(f"cost per GHG emissions\n(EUR/{GHG_unit} CO₂eq)", x=kwargs["supylabel_pos"][0], y=kwargs["supylabel_pos"][1], fontsize=kwargs["fontsize"])
        
        #NOTE: for this particular configuration, the list of labels for the handles has to be the one link to the last ax plotted, in this case for
        #ax326 (right_columns[2]        
        labels=right_column[2].drop(kwargs["BAU_scenario"]).index
        fig.legend(labels, loc=kwargs["loc"], ncol=kwargs["ncol"], fontsize=kwargs["fontsize"]-2, markerscale=3)
        plt.show()
    
    if type=="2x1":
        fig=plt.figure(figsize=(12,12), dpi=80)
        if kwargs["sharey"]==True:
            ax121=fig.add_subplot(121)
            ax122=fig.add_subplot(122, sharey=ax121)
        
        single_graphic_MACC(left_column[0], ax121, show_art=False, show_legend=False, save_fig_memory=True, sensitivity_analysis=sensitivity_anaylisis)
        extra_title=kwargs["left_extra_title"][0]
        ax121.set_title(f"(a){extra_title}", fontsize=kwargs["fontsize"])
        ax121.tick_params("both", labelsize=kwargs["fontsize"]-2)

        single_graphic_MACC(right_column[0], ax122, show_art=False, show_legend=False, save_fig_memory=True, sensitivity_analysis=sensitivity_anaylisis)
        extra_title=kwargs["right_extra_title"][0]
        ax122.set_title(f"(b){extra_title}", fontsize=kwargs["fontsize"])
        ax122.tick_params("both", labelsize=kwargs["fontsize"]-2)    
    
        fig.supxlabel(f"emissions abatement\n({GHG_unit} CO₂eq)", x=kwargs["supxlabel_pos"][0], y=kwargs["supxlabel_pos"][1], fontsize=kwargs["fontsize"])
        fig.supylabel(f"cost per GHG emissions\n(EUR/{GHG_unit} CO₂eq)", x=kwargs["supylabel_pos"][0], y=kwargs["supylabel_pos"][1], fontsize=kwargs["fontsize"])
        
        plt.show()


def circle_plot_MAC(data_input, ax=None, BAU_scenario="composting", show_legend=True, show_art=True, save_fig_memory=False, 
                    include_BAU=True, GHG_unit="tonnes",**kwargs):
                        
    defaultKwargs={"fontsize":18, 
                    "loc": 9, 
                    "ncol": 3, 
                    "ax_margins":[0.2,0.2],
                    "ax_legend": False, 
                    "size_factor":10000, 
                    "legend_handles_size":200,
                    "grid":True, 
                    "alpha": 1}
    
    kwargs={**defaultKwargs, **kwargs}

    if include_BAU==False:
        data_input_mod=data_input.drop(BAU_scenario)
    if include_BAU==True:
        data_input_mod=data_input.copy()
        data_BAU=data_input.loc[BAU_scenario].T
    
    neg=pd.DataFrame()
    for id in data_input_mod.index:
        if data_input_mod.loc[id, "marginal abatement"]<0:
            neg=pd.concat([neg,data_input_mod.loc[id]], axis=1)
    neg=neg.T

    pos=pd.DataFrame()
    for id in data_input_mod.index:
        if data_input_mod.loc[id, "marginal abatement"]>0:
            pos=pd.concat([pos,data_input_mod.loc[id]], axis=1)
    pos=pos.T



    #NOTE: Create a fig only it is desired to depict the legend, otherwise no fig should be created due to blank figure plotting in the multiple plotting
    if show_legend==True:
        fig, ax=plt.subplots()
    else:
        ax=ax or plt.gca()

    ax.grid(kwargs["grid"])
    if not pos.empty:
        list_labels=list(pos.index)
        try:
            for label in list_labels:
                # ax.scatter(x=pos.loc[label,"NPV"], y=pos.loc[label, "emissions - GWP100"], alpha=kwargs["alpha"], s=pos.loc[label, "marginal abatement"]/kwargs["size_factor"], 
                #         c=sce_colors[label], label=label)
                ax.scatter(x=pos.loc[label,"NPV"], y=pos.loc[label, "emissions - GWP100"], alpha=kwargs["alpha"], s=kwargs["size_factor"], 
                        c=sce_colors[label], label=label)
            ax.scatter(x=data_BAU.loc["NPV"], y=data_BAU.loc["emissions - GWP100"], c=sce_colors[BAU_scenario], s=kwargs["size_factor"], label=BAU_scenario, marker="o")
        except ValueError:
            pass

    #TODO: Check the possibility to creat hollow circle if marginal abatement is negative
    if not neg.empty:
        list_labels=list(neg.index)
        try:
            for label in list_labels:
                # ax.scatter(x=neg.loc[label,"NPV"], y=neg.loc[label, "emissions - GWP100"], alpha=0.5, s=neg.loc[label, "marginal abatement"]/kwargs["size_factor"], 
                #         c=sce_colors[label], label=label)
                ax.scatter(x=neg.loc[label,"NPV"], y=neg.loc[label, "emissions - GWP100"], alpha=0.5, s=kwargs["size_factor"], 
                        c=sce_colors[label], label=label)
            ax.scatter(x=data_BAU.loc["NPV"], y=data_BAU.loc["emissions - GWP100"], c=sce_colors[BAU_scenario], s=kwargs["size_factor"], label=BAU_scenario, marker="o")
        except ValueError:
            pass

    if show_art==True:
        ax.set_xlabel("NPV (EUR)", fontsize=kwargs["fontsize"])
        ax.set_ylabel(f"Total GHG emissions ({GHG_unit} CO2eq)", fontsize=kwargs["fontsize"])
        ax.tick_params("both",labelsize=kwargs["fontsize"]-2)
    else: 
        ax.set(xlabel=None)
        ax.set(ylabel=None)
        ax.tick_params("both", labelsize=kwargs["fontsize"]-2)
    
    ax.axvline(x=0, color="black", linestyle="--")
    ax.axhline(y=0, color="black", linestyle="--")
    ax.margins(kwargs["ax_margins"][0],kwargs["ax_margins"][1])

    ax.xaxis.set_major_formatter(formatter_million)

    if show_legend==True:
        handle_list, label_list = LegendWithoutDuplicate(ax, fig).unique_handles_labels()
        fig_legend=fig.legend(handle_list, label_list, loc=kwargs["loc"], ncol=kwargs["ncol"], fontsize=kwargs["fontsize"])
        for i in range(0, len(fig_legend.legendHandles)):
            fig_legend.legendHandles[i]._sizes=[kwargs["legend_handles_size"]]
    else:
        pass

    if save_fig_memory==True:
        return ax
    else: 
        plt.show()


def multiple_circle_plot(left_column=[], right_column=[], type="2x3", GHG_unit="tonnes", **kwargs):

    defaultKwargs={"fontsize":18, 
                    "loc": 9, 
                    "ncol": 4, 
                    "supxlabel_pos": [0.5, 0.1], 
                    "supylabel_pos": [0.1, 0.5],
                    "ax_margins":[0.2, 0.2],
                    "size_factor":10000, 
                    "sharey":False,
                    "legend_handles_size":200,
                    "grid":True,
                    "left_extra_title": ["","",""],
                    "right_extra_title":["","",""] }
    
    kwargs={**defaultKwargs, **kwargs}

    #NOTE: More type of axes distribution can be added
    if type=="2x3":
        fig=plt.figure(figsize=(12,12), dpi=80)
        if kwargs["sharey"]==True:
            ax321=fig.add_subplot(321)
            ax322=fig.add_subplot(322, sharey=ax321, sharex=ax321)
            ax323=fig.add_subplot(323, sharey=ax321, sharex=ax321)
            ax324=fig.add_subplot(324, sharey=ax321, sharex=ax321)
            ax325=fig.add_subplot(325, sharey=ax321, sharex=ax321)
            ax326=fig.add_subplot(326, sharey=ax321, sharex=ax321)
        else:
            if right_column[0]["NPV"].max()>left_column[0]["NPV"].max():
                ax322=fig.add_subplot(322)
                ax321=fig.add_subplot(321, sharex=ax322)
                ax324=fig.add_subplot(324, sharex=ax322)
                ax323=fig.add_subplot(323, sharex=ax322)
                ax326=fig.add_subplot(326, sharex=ax322)
                ax325=fig.add_subplot(325, sharex=ax322)
            else:
                ax321=fig.add_subplot(321)
                ax322=fig.add_subplot(322, sharex=ax321)
                ax323=fig.add_subplot(323, sharex=ax321)
                ax324=fig.add_subplot(324, sharex=ax321)
                ax325=fig.add_subplot(325, sharex=ax321)
                ax326=fig.add_subplot(326, sharex=ax321)
                

        circle_plot_MAC(left_column[0], ax321, show_art=False, show_legend=False, save_fig_memory=True, **kwargs)
        extra_title=kwargs["left_extra_title"][0]
        ax321.set_title(f"(a){extra_title}", fontsize=kwargs["fontsize"])
        ax321.tick_params("both", labelsize=kwargs["fontsize"]-2)
        circle_plot_MAC(left_column[1], ax323, show_art=False, show_legend=False, save_fig_memory=True, **kwargs)
        extra_title=kwargs["left_extra_title"][1]
        ax323.set_title(f"(b){extra_title}", fontsize=kwargs["fontsize"])
        ax323.tick_params("both", labelsize=kwargs["fontsize"]-2)
        circle_plot_MAC(left_column[2], ax325, show_art=False, show_legend=False, save_fig_memory=True, **kwargs)
        extra_title=kwargs["left_extra_title"][2]
        ax325.set_title(f"(c){extra_title}", fontsize=kwargs["fontsize"])
        ax325.tick_params("both", labelsize=kwargs["fontsize"]-2)

        circle_plot_MAC(right_column[0], ax322, show_art=False, show_legend=False, save_fig_memory=True, **kwargs)
        extra_title=kwargs["right_extra_title"][0]
        ax322.set_title(f"(d){extra_title}", fontsize=kwargs["fontsize"])
        ax322.tick_params("both", labelsize=kwargs["fontsize"]-2)        
        circle_plot_MAC(right_column[1], ax324, show_art=False, show_legend=False, save_fig_memory=True, **kwargs)
        extra_title=kwargs["right_extra_title"][1]
        ax324.set_title(f"(e){extra_title}", fontsize=kwargs["fontsize"])
        ax324.tick_params("both", labelsize=kwargs["fontsize"]-2)        
        circle_plot_MAC(right_column[2], ax326, show_art=False, show_legend=False, save_fig_memory=True, **kwargs)
        extra_title=kwargs["right_extra_title"][2]
        ax326.set_title(f"(f){extra_title}", fontsize=kwargs["fontsize"])
        ax326.tick_params("both", labelsize=kwargs["fontsize"]-2)

        #NOTE: when all subplots are sharing the same X and Y, the ticks must remain
        # ax321.set_xticks([])
        # ax323.set_xticks([])
        # ax322.set_xticks([])
        # ax324.set_xticks([])

        fig.supxlabel("NPV (million EUR)", x=kwargs["supxlabel_pos"][0], y=kwargs["supxlabel_pos"][1], fontsize=kwargs["fontsize"])
        fig.supylabel(f"Total GHG emissions ({GHG_unit} CO2eq)", x=kwargs["supylabel_pos"][0], y=kwargs["supylabel_pos"][1], fontsize=kwargs["fontsize"])

        handle_list, label_list = LegendWithoutDuplicate(ax321, fig).unique_handles_labels()
        fig_legend=fig.legend(handle_list, label_list, loc=kwargs["loc"], ncol=kwargs["ncol"], fontsize=kwargs["fontsize"])
        for i in range(0, len(fig_legend.legendHandles)):
            fig_legend.legendHandles[i]._sizes=[kwargs["legend_handles_size"]]
        plt.show()


def circle_plot_NPV_emissions(data_input, ax=None, BAU_scenario="composting", show_legend=True, show_art=True, save_fig_memory=False, 
                            sensitivity=False, sens_disc_rate=False, GHG_unit="tonnes", **kwargs):
                        
    defaultKwargs={"fontsize":18, 
                    "loc": 9, 
                    "ncol": 3, 
                    "ax_margins":[0.2,0.2],
                    "ax_legend": False, 
                    "size_factor":10000, 
                    "legend_handles_size":200,
                    "grid":True, 
                    "alpha": 1}
    
    kwargs={**defaultKwargs, **kwargs}

    data_input_mod=data_input.drop(BAU_scenario)
    data_input_mod=data_input_mod[data_input_mod["type"]=="default"]
    data_BAU=data_input.loc[BAU_scenario].T

    #NOTE: Create a fig only it is desired to depict the legend, otherwise no fig should be created due to blank figure plotting in the multiple plotting
    if show_legend==True:
        fig, ax=plt.subplots()
    else:
        ax=ax or plt.gca()

    ax.grid(kwargs["grid"])

    for scenario in data_input_mod.index:
        ax.scatter(x=data_input_mod.loc[scenario,"NPV"], y=data_input_mod.loc[scenario, "emissions - GWP100"], alpha=kwargs["alpha"], c=sce_colors[scenario], 
                   s=kwargs["size_factor"], label=scenario)
    ax.scatter(x=data_BAU.loc["NPV"], y=data_BAU.loc["emissions - GWP100"], c=sce_colors[BAU_scenario], s=kwargs["size_factor"], label=BAU_scenario, marker="o")

    #sensitivity:
    if sensitivity==True:
        if sens_disc_rate==True:
            pass
        else:
            data_sens=data_input.drop(BAU_scenario)
            data_sens=data_sens[data_sens["type"]=="sensitivity"]
            list_obs=list(set(data_sens["obs"]))


            for obs in list_obs:
                data_sens_obs=data_sens[data_sens["obs"]==obs]

                for sim in data_sens_obs.index:  
                    # ax.scatter(x=data_sens_obs.loc[sim,"NPV"], y=data_sens_obs.loc[sim, "emissions - GWP100"], alpha=kwargs["alpha"],  marker=sensitivity_markers[obs], c=sce_colors[sim[:-4]],
                    # s=kwargs["size_factor"]/3)
                    # ax.plot(data_sens_obs.loc[sim,"NPV"], data_sens_obs.loc[sim, "emissions - GWP100"], marker=sensitivity_markers[obs], c=sce_colors[sim[:-4]])
                    plt.plot(data_sens_obs.loc[sim,"NPV"], data_sens_obs.loc[sim, "emissions - GWP100"], marker=sensitivity_markers[obs], c=sce_colors[sim[:-4]])

    if show_art==True:
        ax.set_xlabel("NPV (million EUR)", fontsize=kwargs["fontsize"])
        ax.set_ylabel(f"Total GHG emissions ({GHG_unit} CO2eq)", fontsize=kwargs["fontsize"])
        ax.tick_params("both",labelsize=kwargs["fontsize"]-2)
    else: 
        ax.set(xlabel=None)
        ax.set(ylabel=None)
        ax.tick_params("both", labelsize=kwargs["fontsize"]-2)
    
    ax.axvline(x=0, color="black", linestyle="--")
    ax.axhline(y=0, color="black", linestyle="--")
    ax.margins(kwargs["ax_margins"][0],kwargs["ax_margins"][1])

    ax.xaxis.set_major_formatter(formatter_million)

    if show_legend==True:
        handle_list, label_list = LegendWithoutDuplicate(ax, fig).unique_handles_labels()
        fig_legend=fig.legend(handle_list, label_list, loc=kwargs["loc"], ncol=kwargs["ncol"], fontsize=kwargs["fontsize"])
        for i in range(0, len(fig_legend.legendHandles)):
            fig_legend.legendHandles[i]._sizes=[kwargs["legend_handles_size"]]
        if sensitivity==True:
            list_obs=list(set(data_sens["obs"]))
            obs_handels=[]
            for obs in list_obs:
                obs_handels.append(Line2D([], [], color="black", marker=sensitivity_markers[obs], linestyle="None", markersize=12, label=obs))
            obs_handels.append(Line2D([], [], color="black", marker="o", linestyle="None", markersize=12, label="default"))
            fig.legend(handles=obs_handels, loc=kwargs["loc"]-1, ncol=kwargs["ncol"], fontsize=kwargs["fontsize"])
    else:
        pass

    if save_fig_memory==True:
        return ax
    else: 
        plt.show()


def circle_plot_NPV_emissions_panel(data_input, type_panel="2x2", BAU_scenario="composting", GHG_unit="tonnes", obs_legend=True, **kwargs):
        
    defaultKwargs={"fontsize":18, 
                    "loc": 9, 
                    "ncol": 3, 
                    "supxlabel_pos": [0.5, 0.1], 
                    "supylabel_pos": [0.1, 0.5],
                    "ax_margins":[0.2,0.2],
                    "ax_legend": False, 
                    "size_factor":10000, 
                    "legend_handles_size":200,
                    "grid":True, 
                    "alpha": 1}
    
    kwargs={**defaultKwargs, **kwargs}
    
    #definitions
    data_sens=data_input.drop(BAU_scenario)
    data_sens=data_sens[data_sens["type"]=="sensitivity"]
    list_obs=[]
    for i in data_input["obs"]:
        if i not in list_obs:
            list_obs.append(i)

    fig=plt.figure()
    if type_panel=="2x2":

        ax221=fig.add_subplot(221)
        circle_plot_NPV_emissions(data_input[data_input["obs"]==list_obs[0]], ax=ax221, save_fig_memory=True, show_art=False, show_legend=False, sensitivity=True, **kwargs)
        ax221.set_title(f"(a): {list_obs[0]}", fontsize=kwargs["fontsize"])
        ax221.tick_params("both", labelsize=kwargs["fontsize"]-2)

        ax222=fig.add_subplot(222, sharey=ax221, sharex=ax221)
        circle_plot_NPV_emissions(data_input[data_input["obs"]==list_obs[1]], ax=ax222, save_fig_memory=True, show_art=False, show_legend=False, sensitivity=True, **kwargs)
        ax222.set_title(f"(b): {list_obs[1]}", fontsize=kwargs["fontsize"])
        ax222.tick_params("both", labelsize=kwargs["fontsize"]-2)

        ax223=fig.add_subplot(223, sharey=ax221, sharex=ax221)
        circle_plot_NPV_emissions(data_input[data_input["obs"]==list_obs[2]], ax=ax223, save_fig_memory=True, show_art=False, show_legend=False, sensitivity=True, **kwargs)
        ax223.set_title(f"(c): {list_obs[2]}", fontsize=kwargs["fontsize"])
        ax223.tick_params("both", labelsize=kwargs["fontsize"]-2)

        ax224=fig.add_subplot(224, sharey=ax221, sharex=ax221)
        circle_plot_NPV_emissions(data_input[data_input["obs"]==list_obs[3]], ax=ax224, save_fig_memory=True, show_art=False, show_legend=False, sensitivity=True, **kwargs)
        ax224.set_title(f"(d): {list_obs[3]}", fontsize=kwargs["fontsize"])
        ax224.tick_params("both", labelsize=kwargs["fontsize"]-2)

        fig.supxlabel("NPV (million EUR)", x=kwargs["supxlabel_pos"][0], y=kwargs["supxlabel_pos"][1], fontsize=kwargs["fontsize"])
        fig.supylabel(f"Total GHG emissions ({GHG_unit} CO2eq)", x=kwargs["supylabel_pos"][0], y=kwargs["supylabel_pos"][1], fontsize=kwargs["fontsize"])

        handle_list, label_list = LegendWithoutDuplicate(ax221, fig).unique_handles_labels()

    elif type_panel=="3x2 column": 

        ax321=fig.add_subplot(321)
        circle_plot_NPV_emissions(data_input[data_input["obs"]==list_obs[0]], ax=ax321, save_fig_memory=True, show_art=False, show_legend=False, sensitivity=True, **kwargs)
        ax321.set_title(f"(a): {list_obs[0]}", fontsize=kwargs["fontsize"])
        ax321.tick_params("both", labelsize=kwargs["fontsize"]-2)

        ax323=fig.add_subplot(323, sharey=ax321, sharex=ax321)
        circle_plot_NPV_emissions(data_input[data_input["obs"]==list_obs[1]], ax=ax323, save_fig_memory=True, show_art=False, show_legend=False, sensitivity=True, **kwargs)
        ax323.set_title(f"(c): {list_obs[1]}", fontsize=kwargs["fontsize"])
        ax323.tick_params("both", labelsize=kwargs["fontsize"]-2)

        ax325=fig.add_subplot(325, sharey=ax321, sharex=ax321)
        circle_plot_NPV_emissions(data_input[data_input["obs"]==list_obs[2]], ax=ax325, save_fig_memory=True, show_art=False, show_legend=False, sensitivity=True, **kwargs)
        ax325.set_title(f"(e): {list_obs[2]}", fontsize=kwargs["fontsize"])
        ax325.tick_params("both", labelsize=kwargs["fontsize"]-2)

        ax322=fig.add_subplot(322, sharey=ax321, sharex=ax321)
        circle_plot_NPV_emissions(data_input[data_input["obs"]==list_obs[3]], ax=ax322, save_fig_memory=True, show_art=False, show_legend=False, sensitivity=True, **kwargs)
        ax322.set_title(f"(b): {list_obs[3]}", fontsize=kwargs["fontsize"])
        ax322.tick_params("both", labelsize=kwargs["fontsize"]-2)

        ax324=fig.add_subplot(324, sharey=ax321, sharex=ax321)
        circle_plot_NPV_emissions(data_input[data_input["obs"]==list_obs[4]], ax=ax324, save_fig_memory=True, show_art=False, show_legend=False, sensitivity=True, **kwargs)
        ax324.set_title(f"(d): {list_obs[4]}", fontsize=kwargs["fontsize"])
        ax324.tick_params("both", labelsize=kwargs["fontsize"]-2)

        fig.supxlabel("NPV (million EUR)", x=kwargs["supxlabel_pos"][0], y=kwargs["supxlabel_pos"][1], fontsize=kwargs["fontsize"])
        fig.supylabel(f"Total GHG emissions ({GHG_unit} CO2eq)", x=kwargs["supylabel_pos"][0], y=kwargs["supylabel_pos"][1], fontsize=kwargs["fontsize"])

        handle_list, label_list = LegendWithoutDuplicate(ax321, fig).unique_handles_labels()

    fig_legend=fig.legend(handle_list, label_list, loc=kwargs["loc"], ncol=kwargs["ncol"], fontsize=kwargs["fontsize"])
    for i in range(0, len(fig_legend.legendHandles)):
        fig_legend.legendHandles[i]._sizes=[kwargs["legend_handles_size"]]
    
    #NOTE: obs_legend is the legend for the observation column, meant for the sensitivity analysis  
    if obs_legend==True:
        list_obs=list(set(data_sens["obs"]))
        obs_handels=[]
        for obs in list_obs:
            obs_handels.append(Line2D([], [], color="black", marker=sensitivity_markers[obs], linestyle="None", markersize=12, label=obs))
        obs_handels.append(Line2D([], [], color="black", marker="o", linestyle="None", markersize=12, label="default"))
        fig.legend(handles=obs_handels, loc=kwargs["loc"]-1, ncol=kwargs["ncol"], fontsize=kwargs["fontsize"])

    plt.show()