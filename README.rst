BIORIM: Biological Resource Utilization Impacts model
=====================================================

The BIORIM model can be used to assess environmental impacts of biological resource utilization.
It is developed within the working group Bioeconomic systems modelling
of the Leibniz Institute for Agricultural Engineering and Bioeconomy (ATB) in Potsdam, Germany.


Authors
-------
Ulrich Kreidenweis, Andres Vargas, Jannes Breier


License
-------

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.