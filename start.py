## to start the whole thing:

import os
import sys
import importlib

from pathlib import Path


sys.path.append(os.path.abspath(Path(__file__).parent)) # import the model location

if not Path("modelconfig.py").is_file():
    print("Make sure that a 'modelconfig.py' file is existing in the root folder of biorim similar to the 'modelconfig_sample.py'")

import analysis, external, material, production, read, simulation, tools, transport


biorim_modules = ["analysis", "external", "material", "production", "read", "simulation", "tools", "transport"]

## reload all biorim modules
def reload_modules():
    loaded_modules = [sys.modules[x] for x in sys.modules if x.split(".")[0] in biorim_modules]
    # print("reloading the following modules:", loaded_modules)
    for module in loaded_modules:
        importlib.reload(module)
