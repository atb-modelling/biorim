from read import datapath, read_mycsv

## Emission factors for GHG inventories, EPA (2014)

def Table2():
    return read_mycsv(datapath("EPA\\Table2.csv"), encoding="latin-1", index_col=0)

def Table3():
    return read_mycsv(datapath("EPA\\Table3.csv"), encoding="latin-1", index_col=0)

def Table4():
    return read_mycsv(datapath("EPA\\Table4.csv"), encoding="latin-1", index_col=0)
