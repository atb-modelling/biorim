import pandas as pd
import numpy as np
import requests
import math
import urllib3

from bs4 import BeautifulSoup
from datetime import datetime
from urllib.request import Request, urlopen
from datetime import date

class InputPrice():
    """
    The class retrieves price of input that are used within the model. 
    It uses updated data from website sources, and therefore it is based on webscrapping. 
    :type notification: boolean
    :param notification: When True, enables the notification of units of the prices, date and currency

    For single methods: 
    :type currency: string 
    :param currency: currency of the price
    :type days_record: int
    :param days_record: number days for the last set of observations of the price/exchange rate
    :type update: boolean
    :param update: When True, updates de price to tha lastest stand from the website and ovewrites the value in the section of csv file within the system
    :type save_system: boolean
    :param save_system: valid for the "udapte_all" method. When True, ovewrites the entire csv files after the data gathering from all methods.
    """
    

    #default URL sources: 
    
    def __init__(self, notification=False):
        self.price_table = pd.read_csv("read/prices_data/input_prices.csv", index_col=0)
        self.notification=notification
    
    def update_csv(self, index, df):
        """
        write the information to the read/prices_data/input_prices.csv
        """
        self.price_table.loc[[index],:] = df
        self.price_table.to_csv("read/prices_data/input_prices.csv")

    def USD_to_EUR(self, days_record=30, update=False):
        """
        Exchange rate of USD to EUR.
        """
        
        if not update:
            return self.price_table.loc[["exchange rate"],:]

        elif update:
            URL_USD_to_EUR="https://www.ecb.europa.eu/stats/policy_and_exchange_rates/euro_reference_exchange_rates/html/usd.xml" #USD to EUR history conversion
            page=requests.get(URL_USD_to_EUR)
            soup=BeautifulSoup(page.content, "xml")
            Obs_data=soup.find_all("Obs")
            Obs_data=list(Obs_data)
            Obs_data_df=pd.DataFrame(columns=range(0,5))
            for i in range(-days_record,0):
                to_append=str(Obs_data[i]).split()
                Obs_data_df_lenght=len(Obs_data_df)
                Obs_data_df.loc[Obs_data_df_lenght]=to_append
            Obs_data_df.drop(columns=[0,1,2], inplace=True)
            Obs_data_df.rename(columns={3:"EUR->USD", 4:"Date"}, inplace=True)
            Obs_data_df.replace(["OBS_VALUE=","TIME_PERIOD=", '"', '/>' ],["","","",""], regex=True, inplace=True)
            Obs_data_df["EUR->USD"]=pd.to_numeric(Obs_data_df["EUR->USD"], downcast="float")
            Obs_data_df=Obs_data_df.iloc[::-1].reset_index().drop(columns="index")
            exchange_rate=Obs_data_df.loc[0, "EUR->USD"]  
            date=Obs_data_df.loc[0, "Date"]
            if self.notification==True:
                print(f"Exchange rate EUR to USD at the date {date}")

            Exchange_rate_df = pd.DataFrame({'Q': exchange_rate, 'U': "EUR/USD", 'date': date}, index=["exchange rate"])

            # update the price_table
            self.update_csv(index="exchange rate", df=Exchange_rate_df)

            return Exchange_rate_df


    def crude_oil(self, currency="EUR", update=False):
        """
        Price of crude oil originally in USD/tonne
        """

        if not update:
            return self.price_table.loc[["crude oil"],:]

        elif update:
            URL_CRUDE_OIL="https://fred.stlouisfed.org/graph/fredgraph.csv?bgcolor=%23e1e9f0&chart_type=line&drp=0&fo=open%20sans&graph_bgcolor=%23ffffff&height=450&mode=fred&recession_bars=off&txtcolor=%23444444&ts=12&tts=12&width=1168&nt=0&thu=0&trc=0&show_legend=yes&show_axis_titles=yes&show_tooltip=yes&id=MCOILBRENTEU&scale=left&cosd=1987-05-01&coed=2022-06-01&line_color=%234572a7&link_values=false&line_style=solid&mark_type=none&mw=3&lw=2&ost=-99999&oet=99999&mma=0&fml=a&fq=Monthly&fam=avg&fgst=lin&fgsnd=2020-02-01&line_index=1&transformation=lin&vintage_date=2022-07-14&revision_date=2022-07-14&nd=1987-05-01" #crude oil price in USD
            oil_price_data=pd.read_csv(URL_CRUDE_OIL)[::-1].reset_index()
            oil_price_data=oil_price_data.drop(columns=["index"])
            oil_price_data.rename(columns={"MCOILBRENTEU":"price"}, inplace=True)
            oil_price=oil_price_data.iloc[0]["price"]
            date=oil_price_data.iloc[0]["DATE"]

            if currency=="USD":
                if self.notification==True:
                    print(f"Crude oil price is given in USD per barrel for the date {date}")
            
            elif currency=="EUR":
                oil_price=oil_price*self.USD_to_EUR()["Q"][0]
                if self.notification==True:
                    print(f"Crude oil price is given in EUR per barrel for the date {date}")

            Oil_price_data_df = pd.DataFrame({'Q': oil_price, 'U': f"{currency}/tonne", 'date': date}, index=["crude oil"])
            self.update_csv(index="crude oil", df=Oil_price_data_df)

            return Oil_price_data_df


    def rapeseed_oil(self, currency="EUR", days_record=30, update=False):
        """
        Price of rapeseed oil originally in USD/tonne
        """

        if not update:
            return self.price_table.loc[["rapeseed oil"],:]

        elif update: 
            URL_RAPESEED_OIL="https://ir-service.appspot.com/share/nesteoil/English/price_monitor3_dg.html?name=Palmoil&r=2201" #The table is shown in the website: https://www.neste.com/investors/market-data/palm-and-rapeseed-oil-prices
            #Data extraction
            page=requests.get(URL_RAPESEED_OIL)
            soup=BeautifulSoup(page.content, "lxml")
            all_scripts=soup.find_all("script")
            all_scripts=list(all_scripts[5])
            raw_data=all_scripts[0].split("c")

            #Data structuring
            list_rapeoil=[]
            for i in raw_data:
                if "v:" in i:
                    list_rapeoil.append(i)
            index_size=range(0, len(list_rapeoil)-1)
            rapeoil_data=pd.DataFrame(index=index_size, columns=["RAW_DATA"])
            for i in index_size:
                rapeoil_data.loc[i, "RAW_DATA"]=list_rapeoil[i]
                rapeoil_data.loc[i, "YEAR"]=str(rapeoil_data.iloc[i][0]).split(",")[0]
                rapeoil_data.loc[i, "MONTH"]=str(rapeoil_data.iloc[i][0]).split(",")[1]
                rapeoil_data.loc[i, "DAY"]=str(rapeoil_data.iloc[i][0]).split(",")[2]
                rapeoil_data.loc[i, "PRICE_RAPESEED_OIL"]=str(rapeoil_data.iloc[i][0]).split(",")[3]
            rapeoil_data.drop(columns=["RAW_DATA"], inplace=True)
            rapeoil_data.replace(["new Date","{","}"," ","\[","\]","v",":","\(","\)"], ["","","","","","","","","",""], regex=True, inplace=True)
            rapeoil_data=rapeoil_data[::-1].reset_index().drop(columns="index")
            rapeoil_data["PRICE_RAPESEED_OIL"]=pd.to_numeric(rapeoil_data["PRICE_RAPESEED_OIL"], downcast="float")
            cols_date=["YEAR","MONTH","DAY"]
            rapeoil_data["DATE"]=rapeoil_data[cols_date].apply(lambda x: '-'.join(x.values.astype(str)), axis="columns")
            rapeoil_data.drop(columns=cols_date, inplace=True)
            rapeoil_data=rapeoil_data.iloc[:days_record]
            last_date=rapeoil_data.loc[0,"DATE"]
            price_rapeoil=rapeoil_data.loc[0,"PRICE_RAPESEED_OIL"]

            if currency=="USD":
                if self.notification==True:
                    print(f"Rape oil price is given in USD per tonne of rapeseed oil, for the date {last_date}")

            if currency=="EUR":
                price_rapeoil=price_rapeoil*self.USD_to_EUR()["Q"][0]
                if self.notification==True:
                    print(f"Rape oil price is given in EUR per tonne of rapeseed oil, for the date {last_date}")

            rapeoil_price_data_df = pd.DataFrame({'Q': price_rapeoil, 'U': f"{currency}/tonne", 'date': last_date}, index=["rapeseed oil"])
            #self.update_csv(index="rapeseed oil", df=rapeoil_price_data_df) #NOTE: This line is the same as in the crude oil, but not working for this method.
        
            return rapeoil_price_data_df

    def diesel(self, currency="EUR", update=False):
        """
        Price of diesel originally in EUR/liter
        """
        #WARNING: the calculation of diesel price may not work on time schedules nearby to midnight, due to price updating in the website
        if not update:
            return self.price_table.loc[["diesel"],:]

        elif update:
            URL_DIESEL="https://de.fuelo.net/fuel/type/diesel?lang=en"
            page=requests.get(URL_DIESEL)
            soup=BeautifulSoup(page.content, "lxml")
            all_scripts=soup.find_all('script', {'type': 'text/javascript'})
            all_scripts=list(all_scripts)
            raw_data=str(list(all_scripts[17])[0]).split("\n")
            raw_data=raw_data[7]
            raw_data=raw_data.split()[1]
            raw_data=str(raw_data).split(",")
            dates=raw_data[::2]
            values=raw_data[1::2]

            diesel_price_df=pd.DataFrame(columns=["DATES", "PRICE"])
            diesel_price_df["DATES"]=dates
            diesel_price_df["PRICE"]=values
            diesel_price_df.replace(("\[","\]"), ("",""), regex=True, inplace=True)
            for i in diesel_price_df.index:
                diesel_price_df.loc[i,"DATES"]=diesel_price_df.loc[i,"DATES"][:10]
                diesel_price_df.loc[i,"DATES"]=datetime.fromtimestamp(float(diesel_price_df.loc[i,"DATES"])).isoformat()
            diesel_price_df.replace("T22:00:00", "", regex=True, inplace=True)
            diesel_price_df["PRICE"]=pd.to_numeric(diesel_price_df["PRICE"], downcast="float")
            diesel_price_df=diesel_price_df[::-1].reset_index().drop(columns="index")

            diesel_price=diesel_price_df.iloc[0][1]
            date=diesel_price_df.iloc[0][0]

            if currency=="USD":
                diesel_price=diesel_price/self.USD_to_EUR()["Q"][0]
                if self.notification==True:
                    print(f"Diesel price is given in USD per liter for the date {date}")

            if currency=="EUR":
                if self.notification==True:
                    print(f"Diesel price is given in EUR per liter for the date {date}")

            #TODO:
            # Discuss the posibility of using avarage prices in a period of time.
            # Posilibity of establish a fixed price that belongs to a fixed date.
            # Check what other input could fit on this class.  

            diesel_price_data_df = pd.DataFrame({'Q': diesel_price, 'U': f"{currency}/liter", 'date': date}, index=["diesel"])
            # self.update_csv(index="diesel", df=diesel_price_data_df)

            return diesel_price_data_df
    
    def lubricating_oil(self, currency="EUR", update=False):
        #NOTE: This function has to be updated with webscrapping
        if currency=="EUR":
            lubricating_price_data={"lubricating oil": [10, f"{currency}/liter", date.today()]}
        elif currency=="USD":
            lubricating_price_data={"lubricating oil": [10/self.USD_to_EUR()["Q"][0], f"{currency}/liter", date.today()]}

        lubricating_price_data_df=pd.DataFrame.from_dict(lubricating_price_data, orient="index", columns=["Q", "U", "date"])

        return lubricating_price_data_df

    def transportation(self, currency="EUR", update=False):
        """
        Price of freight transportation originally in EUR/km for a category of truck.
        """

        if not update:
            price_10t=self.price_table.loc[["transport <10 ton"],:]
            price_20t_10t=self.price_table.loc[["transport 10-20 ton"],:]
            price_20t=self.price_table.loc[["transport >20 ton"],:]
            return pd.concat([price_10t, price_20t_10t, price_20t])

        elif update: 
            URL_TRANSPORTATION="https://della.eu/cost/local/"
            page=requests.get(URL_TRANSPORTATION)
            soup=BeautifulSoup(page.content, "lxml")
            soup.find_all("td")
            data_raw=list(soup.find_all("td"))

            for i in range(0,len(data_raw)):
                data_raw[i]=str(data_raw[i])

            data_extract=[s for s in data_raw if "EUR" in s]
            main_data=data_extract[0].split("td")
            main_data=[s for s in main_data if "price_text nobr" in s]
            arr=np.array_split(main_data,1)
            n_rows=len(arr[0])/4
            data_organized=pd.DataFrame(index=range(0,int(n_rows)), columns=["distance", "ton", "total_price", "rate"])
            
            #organzing data in a dataframe
            distance=[]
            for i in range(0,len(arr[0]),4):
                distance.append(arr[0][i])
            ton=[]
            for i in range(1,len(arr[0]),4):
                ton.append(arr[0][i])
            total_price=[]
            for i in range(2,len(arr[0]),4):
                total_price.append(arr[0][i])
            rate=[]
            for i in range(3,len(arr[0]),4):
                rate.append(arr[0][i])
            data_organized["distance"]=distance
            data_organized["ton"]=ton
            data_organized["total_price"]=total_price
            data_organized["rate"]=rate

            #purge of non-useful information
            data_organized.replace('class="price_text nobr"', "", regex=True, inplace=True)
            data_organized.replace('>\r\n~\r\n<a class="distance_link" href="', "", regex=True, inplace=True)
            data_organized.replace(">", "", regex=True, inplace=True)
            data_organized.replace("</", "", regex=True, inplace=True)
            data_organized.replace('" target="_blank"', "", regex=True, inplace=True)
            data_organized.replace('\/?cities=', "", regex=True, inplace=True)
            data_organized.replace('/distance/', "", regex=True, inplace=True)
            data_organized.replace(',', ".", regex=True, inplace=True)

            for i in data_organized.index:
                if "EUR" in data_organized.loc[i, "rate"]:
                    data_organized.loc[i, "EUR"]=data_organized.loc[i, "rate"][:5]
            data_organized["EUR"].replace(' ',"", regex=True, inplace=True)
            data_organized["EUR"]=pd.to_numeric(data_organized["EUR"], downcast="float")

            for i in data_organized.index: 
                if "USD" in data_organized.loc[i, "rate"]:
                    data_organized.loc[i, "USD"]=data_organized.loc[i, "rate"][:5]
            data_organized["USD"].replace(' ',"", regex=True, inplace=True)
            data_organized["USD"]=pd.to_numeric(data_organized["USD"], downcast="float")

            for i in data_organized.index:  
                if math.isnan(data_organized.loc[i,"EUR"])==True:
                    data_organized.loc[i, "EUR"]=data_organized.loc[i, "USD"]/InputPrice().USD_to_EUR()["Q"][0]

            for i in data_organized.index:
                if "t" in data_organized.loc[i,"ton"]:
                    data_organized.loc[i, "ton(t)"]=data_organized.loc[i, "ton"][:3]
            data_organized["ton(t)"].replace(("t"," ","-"), ("","",""), regex=True, inplace=True)
            data_organized["ton(t)"]=pd.to_numeric(data_organized["ton(t)"], downcast="float")

            price_20t=data_organized[data_organized["ton(t)"]>=20]["EUR"].mean()
            price_10t_20t=data_organized[(data_organized["ton(t)"]<20) & (data_organized["ton(t)"]>=10)]["EUR"].mean()
            price_10t=data_organized[data_organized["ton(t)"]<10]["EUR"].mean()
            
            if currency=="USD":
                price_20t=price_20t/self.USD_to_EUR()["Q"][0]
                price_10t_20t=price_10t_20t/self.USD_to_EUR()["Q"][0]
                price_10t=price_10t/self.USD_to_EUR()["Q"][0]
                if self.notification==True:
                    print(f"Transportation price is given in USD per km for the date {str(date.today())}")
            
            if currency=="EUR":
                if self.notification==True:
                    print(f"Transportation price is given in EUR per km for the date {str(date.today())}")
 

            price_transportation_10df = pd.DataFrame({'Q': price_10t, 'U': f"{currency}/km", 'date': str(date.today())}, index=["transport price <10 ton"])
            price_transportation_10_20df = pd.DataFrame({'Q': price_10t_20t, 'U': f"{currency}/km", 'date': str(date.today())}, index=["transport price 10-20 ton"])
            price_transportation_20df = pd.DataFrame({'Q': price_20t, 'U': f"{currency}/km", 'date': str(date.today())}, index=["transport price >20 ton"])
            # self.update_csv(index="transport price <10 ton", df=price_transportation_10df)
            # self.update_csv(index="transport price 10-20 ton", df=price_transportation_10_20df)
            # self.update_csv(index="transport price >20 ton", df=price_transportation_20df)

            return pd.concat([price_transportation_10df, price_transportation_10_20df, price_transportation_20df])

    def electricity(self, currency="EUR", update=False, use="business"):
        """
        Price of electricity originally in EUR/kWh.
        """
        if not update:
            elec_business= self.price_table.loc[["electricity, business"]]
            elec_business_large= self.price_table.loc[["electricity, large use, business"]]
            elec_industry_large= self.price_table.loc[["electricity, large use, industry"]]
            return pd.concat([elec_business, elec_business_large, elec_industry_large])

        elif update:
            URL_ELECTRICITY="https://www.globalpetrolprices.com/Germany/electricity_prices/"
            page=requests.get(URL_ELECTRICITY)
            soup=BeautifulSoup(page.content, "lxml")
            soup.find_all("td")
            data_raw=list(soup.find_all("td"))

            data_elec=pd.DataFrame()
            data_elec.loc["household", "EUR"]=str(data_raw[2])
            data_elec.loc["business", "EUR"]=str(data_raw[3])
            data_elec["EUR"].replace('<td align="center" height="30" style="border-bottom: 1px solid #f8f8f8">',"" , regex=True, inplace=True)
            data_elec["EUR"].replace('</td>',"" , regex=True, inplace=True)
            data_elec["EUR"]=pd.to_numeric(data_elec["EUR"], downcast="float")
            price_elec=data_elec.loc[use, "EUR"] 

            if currency=="USD":
                price_elec=price_elec/self.USD_to_EUR()["Q"][0]
                if self.notification==True:
                    print(f"Electricity price is given in USD per kWh")

            if currency=="EUR":
                if self.notification==True:
                    print(f"Electricity price is given in EUR per kWh")
  
            elec_price_data_df= pd.DataFrame({'Q': price_elec, 'U': f"{currency}/kWh", 'date': "not especified"}, index=["electricity, business"])
            #self.update_csv(index="electricity", df=elec_price_data_df)

            #NOTE: for large use of electricity, the electricity rates changes (more info in: https://de.statista.com/statistik/daten/studie/154902/umfrage/strompreise-fuer-industrie-und-gewerbe-seit-2006/#:~:text=Ohne%20auferlegte%20Steuern%20betrug%20der,sich%20aus%20verschiedenen%20Posten%20zusammen.)
            elec_price_business_large_df=pd.DataFrame({'Q': 0.2323, 'U': f"{currency}/kWh", 'date': "2021-04-01"}, index=["electricity, large use, business"])
            elec_price_industry_large_df=pd.DataFrame({'Q': 0.1694, 'U': f"{currency}/kWh", 'date': "2021-04-01"}, index=["electricity, large use, industry"])

            return pd.concat([elec_price_data_df, elec_price_business_large_df, elec_price_industry_large_df])

    def urea(self, currency="EUR", update=False): #NOTE: Urea method should  be updated!!, headers use 
        """
        Price of urea originally in USD per tonne. More information in the webpage: https://tradingeconomics.com/commodity/urea"
        """
        
        if not update:
            return self.price_table.loc[["urea"],:]
        
        elif update: 
            headers={"user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36"}

            URL_UREA_MAIN="https://tradingeconomics.com/commodity/urea"
            response=requests.get(URL_UREA_MAIN, headers=headers)
            soup=BeautifulSoup(response.content, "html.parser")
            data_soup=soup.find_all("script")

            for ele in data_soup:
                if "TESecurify" in str(ele):
                    TESecurify=str(ele)

            TESecurify=TESecurify.split(" ")
            TESecurify=TESecurify[12]
            TESecurify=TESecurify.replace(";\r\n","")
            TESecurify=TESecurify.replace("'","")

            URL_UREA="https://markets.tradingeconomics.com/chart?s=ufb:com&interval=1d&span=1y&securify=new&url=/commodity/urea&AUTH="

            page=requests.get(URL_UREA+TESecurify, headers=headers)
            soup=BeautifulSoup(page.content, "lxml")
            data_raw=str(soup)
            data_raw=data_raw.split(",")

            date_values=[i for i in data_raw if '"date":' in i]
            y_values=[i for i in data_raw if '"y":' in i]

            data_price=pd.DataFrame(columns=["date", "price"])
            data_price["date"]=date_values
            data_price["price"]=y_values
            data_price["date"].replace('"date":',"" , regex=True, inplace=True)
            data_price["date"].replace('"',"" , regex=True, inplace=True)
            data_price["date"].replace('T00:00:00',"" , regex=True, inplace=True)
            data_price["price"].replace('"y":',"" , regex=True, inplace=True)
            data_price["price"]=pd.to_numeric(data_price["price"], downcast="float")
            data_price=data_price.iloc[::-1].reset_index().drop(columns="index")
            data_price.iloc[0]["price"]
            price_urea=data_price.iloc[0]["price"]
            date=data_price.iloc[0]["date"]

            if currency=="USD":
                if self.notification==True:
                    print(f"Urea price is given in USD per tonne for the date {date}")
            
            if currency=="EUR":
                price_urea=price_urea*self.USD_to_EUR()["Q"][0]
                if self.notification==True:
                    print(f"Urea price is given in EUR per tonne for the date {date}")

            price_urea_df= pd.DataFrame({'Q': price_urea, 'U': f"{currency}/tonne", 'date': date}, index=["urea"])
            #self.update_csv(index="urea", df=price_urea_df)

            return price_urea_df

    def petrol(self, currency="EUR", update=False):
        """
        Price of petrol originally in USD per liter. 
        """

        if not update:
            return self.price_table.loc[["petrol"],:]

        elif update:
            URL_PETROL="https://www.globalpetrolprices.com/Germany/gasoline_prices/"
            page=requests.get(URL_PETROL)
            soup=BeautifulSoup(page.content, "lxml")
            data_raw=list(soup.find_all("td"))
            data_str=[]
            for i in range(0,len(data_raw)):
                data_str.append(str(data_raw[i]))

            date_str=str(soup.find_all("h1"))
            date_str=date_str.replace("[<h1>Germany Gasoline prices, ","")
            date_str=date_str.replace("</h1>]","")

            price_df=pd.DataFrame(index=["EUR", "USD"], columns=["liter", "gallon"])
            price_df.loc["EUR","liter"]=data_str[2]
            price_df.loc["EUR","gallon"]=data_str[3]
            price_df.loc["USD","liter"]=data_str[4]
            price_df.loc["USD","gallon"]=data_str[5]
            price_df.replace(('<td align="center" height="30">','</td>'),("",""), regex=True, inplace=True)
            price_df["liter"]=pd.to_numeric(price_df["liter"], downcast="float")
            price_df["gallon"]=pd.to_numeric(price_df["gallon"], downcast="float")
            price_petrol=price_df.loc["EUR","liter"]

            if currency=="USD":
                price_petrol=price_petrol/self.USD_to_EUR()["Q"][0]
                if self.notification==True:
                    print(f"Petrol price is given in USD per liter for the date {date_str}")

            if currency=="EUR":
                if self.notification==True:
                    print(f"Petrol price is given in EUR per liter for the date {date_str}")

            price_petrol_df= pd.DataFrame({'Q': price_petrol, 'U': f"{currency}/liter", 'date': date_str}, index=["petrol"])
            #self.update_csv(index="petrol", df=price_petrol_df)

            return price_petrol_df

    def HDPE_film(self, currency="EUR", update=False):
        """
        Price of HDPE film originally in EUR per tonne. 
        """

        if not update:
            return self.price_table.loc[["HDPE film"],:]

        elif update: 
            URL_HDPE_PRICE="https://plasticker.de/preise/preise_myceppi_en.php"
            page=requests.get(URL_HDPE_PRICE).content
            df_list=pd.read_html(page)
            raw_data=df_list[0]
            raw_data[0]
            raw_data.set_index(raw_data[0], inplace=True)
            raw_data.drop(0,axis=1, inplace=True)
            raw_data.rename(columns=raw_data.iloc[0], inplace=True)
            raw_data.drop("Type",axis=0, inplace=True)
            raw_data.replace(("/t"," ",",","\x80"), ("","","",""), regex=True, inplace=True)
            raw_data["Price"]=pd.to_numeric(raw_data["Price"], downcast="float", errors="coerce")
            price_HDPE=raw_data.loc["HDPE film", "Price"]
            price_HDPE #EUR/tonne

            if currency=="USD":
                price_HDPE=price_HDPE/self.USD_to_EUR()["Q"][0]
                if self.notification==True:
                    print(f"HDPE film price is given in USD per tonne for the date {date.today()}")
            
            if currency=="EUR":
                if self.notification==True:
                    print(f"HDPE film price is given in EUR per tonne for the date {date.today()}")

            price_HDPE_df= pd.DataFrame({'Q': price_HDPE, 'U': f"{currency}/tonne", 'date': date.today()}, index=["HDPE film"])

            return price_HDPE_df

    # NOTE: This could be replace with a function that updates all datasets by calling self.EUR_to_USD(update=True) etc.
    def update_all(self, save_system=True):
        """
        Updates in a single run the input prices. Possible to save it to the system
        """
        print(f"webscraping and collection data from all methods")
        storage_data=pd.concat([self.USD_to_EUR(update=True), self.crude_oil(update=True), self.rapeseed_oil(update=True), 
                                self.diesel(update=True), self.lubricating_oil(update=True), self.electricity(update=True), 
                                 self.HDPE_film(update=True), self.transportation(update=True), self.urea(update=True), 
                                self.petrol(update=True)])

        if save_system==True:
            print(f"Saving data in the system")
            storage_data.to_csv("read/prices_data/input_prices.csv")

        return storage_data

class LandPrice():
    """
    Class for prices and rent of land in Germany
    :type use: string
    :param use: Type of land use
    :type source: string
    :param source: Source of information
    :type location: string
    :param location: Location of the analysis
    TODO: If possible, add additional information for other land uses and sources. 
    """

    #TODO: More information that could be included can be retrieved from DESTATIS (https://www.destatis.de/DE/Themen/Branchen-Unternehmen/Landwirtschaft-Forstwirtschaft-Fischerei/Landwirtschaftliche-Betriebe/_inhalt.html#_6a8x77d7y)

    def __init__(self, use="building land", source="Brandenburg Viewer", location="Hennickendorf"):
        self.use=use
        self.source=source
        self.location=location

    def purchase(self):
        if self.use=="agricultural":
            if self.source=="Krischke et al. 2021": #values for 2016 Fig.3.1 (https://link.springer.com/content/pdf/10.1007/978-3-030-50841-8_3.pdf)
                purchase=pd.DataFrame({'Q':25000 , 'U':"EUR/ha", 'source': self.source}, index=["purchase, agricultura land"])
        if self.use=="building land":    
            if self.source=="Brandenburg Viewer": #more info in: https://bb-viewer.geobasis-bb.de/ 
                if self.location in ["Hennickendorf"]:
                    purchase=pd.DataFrame({'Q':27 , 'U':"EUR/m2", 'source': self.source}, index=["purchase, building land"])
        return purchase

    def rent(self):
        if self.use=="agricultural":
            if self.source=="Krischke et al. 2021": #values for 2018, Fig.3.2 (https://link.springer.com/content/pdf/10.1007/978-3-030-50841-8_3.pdf)
                #NOTE: in year basis
                rent=pd.DataFrame({'Q':280 , 'U':"EUR/ha", 'source': self.source}, index=["rent, agricultural land"])
        if self.use=="building land":
            if self.source=="Brandenburg Viewer": #more info in: https://bb-viewer.geobasis-bb.de/ 
                if self.location in ["Hennickendorf"]:
                #NOTE: in year basis
                    rent=pd.DataFrame({'Q':self.purchase()["Q"][0]*0.01, 'U':"EUR/m2", 'source': self.source}, index=["rent, building land"])
                
        return rent