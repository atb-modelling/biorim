from read import datapath, read_mycsv

## Emissions from fuel combustion. Source: https://ghgprotocol.org/sites/default/files/Emission_Factors_from_Cross_Sector_Tools_March_2017.xlsx

def Table1():
    return read_mycsv(datapath("GHG_protocol\\Table1.csv"), encoding="latin-1", index_col=0)

def Table2():
    return read_mycsv(datapath("GHG_protocol\\Table2.csv"), encoding="latin-1", index_col=0)

def Table3():
    return read_mycsv(datapath("GHG_protocol\\Table3.csv"), encoding="latin-1", index_col=0)
