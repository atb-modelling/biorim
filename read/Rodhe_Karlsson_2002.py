from read import datapath, read_mycsv

def all_manure_managements():
    return read_mycsv(datapath("Rodhe_Karlsson_2002\\Table 2.csv"), encoding="latin-1", index_col=0)

def losses_manure_spreading():
    return read_mycsv(datapath("Rodhe_Karlsson_2002\\Table 3.csv"), encoding="latin-1", index_col=0)

