from read import datapath, read_mycsv

def NH3_EF_solid_poultry_manure_spreading():
    return read_mycsv(datapath("Thuenen_Institute\\Thuenen_report_57\\Table_8-3.csv"), encoding="latin-1", index_col=0)

def NH3_EF_liquid_cattle_manure_spreading():
    return read_mycsv(datapath("Thuenen_Institute\\Thuenen_report_57\\Table_4-6.csv"), encoding="latin-1", index_col=0)
