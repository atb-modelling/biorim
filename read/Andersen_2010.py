from read import datapath, read_mycsv

def Table1():
    return read_mycsv(datapath("Andersen_et_al_2010\\Table1.csv"), encoding="latin-1", index_col=0)

def Table5():
    return read_mycsv(datapath("Andersen_et_al_2010\\Table5.csv"), encoding="latin-1", index_col=0)