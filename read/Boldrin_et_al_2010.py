from read import datapath, read_mycsv

def Table7():
    return read_mycsv(datapath("Boldrin_et_al_2010\\Table7.csv"), encoding="latin-1", index_col=0)