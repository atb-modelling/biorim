import pandas as pd
import numpy as np
from read import datapath

def large_animal_farms():
    """
    The location of Large Animal Farms in Brandenburg: "Genehmigungsbedürftige Tierhaltungsanlagen - im Betrieb"
    """
    LargeAnimalFarms = pd.read_csv(datapath("Landtag_BB/Drucksache 6-7293_manual_edit.csv"), encoding="latin-1", sep=";", dtype={'PLZ': np.character})
    LargeAnimalFarms.rename(columns={"Plätze":"Places"}, inplace=True)
    
    # add the NUTS3 region according to the PLZ
    PLZtoNUTS3 = pd.read_csv(datapath("Eurostat/pc2016_de_NUTS-2013_v2.3.csv"), sep=";" , dtype={"CODE": "unicode_"}, quotechar="'")
    PLZtoNUTS3.rename(columns={"CODE":"PLZ", "NUTS_3": "NUTS_ID"}, inplace=True)
    
    return LargeAnimalFarms.merge(PLZtoNUTS3, on="PLZ")
    
    