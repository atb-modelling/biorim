from read import datapath, read_mycsv

def CH4_oxidation():
    return read_mycsv(datapath("Munoz_Schmidt_2016\\CH4_oxidation_GWP_GTP.csv"), encoding="latin-1", index_col=0)