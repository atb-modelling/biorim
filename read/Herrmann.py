from read import datapath, read_mycsv

## Chemical composition and methane production given by Herrmann et al. (unpublished)

def Chemical_composition():
    return read_mycsv(datapath("Herrmann\\Chemical_composition.csv"), encoding="latin-1", index_col=0)

def Methane_production():
    return read_mycsv(datapath("Herrmann\\Methane_production.csv"), encoding="latin-1", index_col=0)
