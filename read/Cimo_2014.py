from read import datapath, read_mycsv

def material_characteristics():
    return read_mycsv(datapath("Cimo_2014\\Table 1.csv"), encoding="latin-1", index_col=0)

