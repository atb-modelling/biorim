from read import datapath, read_mycsv

def broiler_content():
    dat = read_mycsv(datapath("LWK_NS\\composition_broiler.csv"), encoding="latin-1", index_col=0)
    return dat