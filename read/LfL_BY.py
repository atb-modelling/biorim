from read import datapath, read_mycsv
from tools import convert_molmasses

## nutrient content of crops
def nutrient_crops(to_element=True):
    file = datapath("LfL_BY\\basisdaten_2018-03_table1a_nutrient_content_crops3.csv")
    dat = read_mycsv(file, encoding="latin-1", index_col=(0,1))
    source = dat.source
    description = dat.description
    dat.sort_index(inplace=True)
    dat.loc[:,["N", "P2O5", "K2O", "MgO"]] = dat[["N", "P2O5", "K2O", "MgO"]]*10 # convert from kg/dt to kg/ton
    dat["avg yield (ton/ha)"] = dat["avg yield (dt/ha)"]/10
    dat.drop(columns="avg yield (dt/ha)", inplace=True)
    if to_element==True:
        dat = convert_molmasses(dat, to="element",species_to_be_converted=["P2O5", "K2O", "MgO"])
        dat = dat.rename({'P2O5-P':"P", 'K2O-K':"K", 'MgO-Mg':"Mg"}, axis=1)
    dat.unit = "nutrition contents [kg/ton]"
    dat.file = file
    dat.source = source
    dat.description = description
    return dat

## nutrient content of animals
def nutrient_content_animals(to_element=True):
    dat = read_mycsv(datapath("LfL_BY\\basisdaten_2018-03_table6a_nutrient_content_animals.csv"), encoding="latin-1", index_col=0, decimal=",", sep=";")
    dat.drop(columns="Tier", inplace=True)
    dat = dat*10 # convert from kg/dt to kg/ton
    dat.description = "nutrition contents of animal bodies [kg/ton]"
    if to_element==True:
        dat = convert_molmasses(dat, to="element",species_to_be_converted=["P2O5", "K2O", "MgO"])
        dat = dat.rename({'P2O5-P':"P", 'K2O-K':"K", 'MgO-Mg':"Mg"}, axis=1)
    return dat

def nutrition_contents_efficacy():
    return read_mycsv(datapath("LfL_BY\\basisdaten_2018-03_table5.csv"), encoding="latin-1", index_col=0)

### Gruber feeding tables
# energy and crude protein demand calves
def energy_protein_demand_calves():
    return read_mycsv(datapath("LfL_BY\\gruber_tabelle_fuetterung_milchkuehe_zuchtrinder_schafe_ziegen_table2-1_calves_me_protein_edit.csv"), index_col=0, sep=";")

def feed_example_cavles():
    dat = read_mycsv(datapath("LfL_BY\\gruber_tabelle_fuetterung_milchkuehe_zuchtrinder_schafe_ziegen_lfl-information_table2-3_cavles_feed_examples.csv"), encoding="latin-1", index_col=0, sep=";")
    dat.drop(columns="Futtermittel", inplace=True)
    return dat

# nutrient content of milk products
def nutrient_milk_products(per_FM=True):
    dat =  read_mycsv(datapath("LfL_BY\\gruber-tabelle-fuetterung-rindermast_lfl-information_tabelle8-6_molkereiprodukte.csv"), encoding="latin-1", index_col=0, sep=";")
    dat.drop(["ID", "Produkt"], axis=1, inplace=True)
    if per_FM:
        dat2 = dat.loc[:,dat.columns != "DM"].multiply(dat["DM"], axis="index")
        dat2["DM"] = dat["DM"]
        return dat2
    else:
        return dat

def nutrient_fodder_crops(per_FM=True):
    file = datapath("LfL_BY\\gruber_tabelle_fuetterung_milchkuehe_zuchtrinder_schafe_ziegen_lfl-information_table10_nutrients_edit.csv")
    dat = read_mycsv(datapath("LfL_BY\\gruber_tabelle_fuetterung_milchkuehe_zuchtrinder_schafe_ziegen_lfl-information_table10_nutrients_edit.csv"), encoding="latin-1", index_col=1, sep=";")
    dat.drop(["ID", "Futtermittel", "Stadium"], axis=1, inplace=True)
    # values are usually related to dry matter. Option to relate to fresh matter
    if per_FM:
        dat2 = dat.loc[:,dat.columns != "DM"].multiply(dat["DM"], axis="index")
        dat2["DM"] = dat["DM"]
    else:
        dat2 = dat
    dat2.file = file
    return dat2

def N_fertilizer_demand_arable():
    return read_mycsv(datapath("LfL_BY\\duengebedarfsermittlung_ackerland_blw-1-2018_tab2.csv"), encoding="latin-1", index_col=0, sep=";")
