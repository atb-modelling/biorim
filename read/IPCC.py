from read import datapath, read_mycsv

def mcf_manure_managements():
    """
    Methane conversion factors (MCF) from the 2019 IPCC Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories
    """
    return read_mycsv(datapath("IPCC\\2019 IPCC Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories\\19R_V4_Ch10_Livestock_advance_table10-17.csv"), index_col=0, sep=";")

def Bo_manure_managements():
    return read_mycsv(datapath("IPCC\\2019 IPCC Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories\\19R_V4_Ch10_Livestock_advance_table10-16.csv"), index_col=0, sep=";")

def N2O_EF_manure_management():
    return read_mycsv(datapath("IPCC\\2019 IPCC Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories\\19R_V4_Ch10_Livestock_advance_table10-21.csv"), encoding="latin-1", index_col=0)

def NH3_and_NOx_EF_manure_management():
    return read_mycsv(datapath("IPCC\\2019 IPCC Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories\\19R_V4_Ch10_Livestock_advance_table10-22.csv"), encoding="latin-1", index_col=0, header=[0,1])

def direct_N2O_EF_managed_soils():
    return read_mycsv(datapath("IPCC\\2019 IPCC Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories\\19R_V4_Ch11_Soils_N2O&CO2_table11-1.csv"), encoding="latin-1", index_col=[0,1]).sort_index()

def indirect_N2O_EF_soils():
    return read_mycsv(datapath("IPCC\\2019 IPCC Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories\\19R_V4_Ch11_Soils_N2O&CO2_table11-3.csv"), encoding="latin-1", index_col=[0,1])

def GWP_factors_Table8SM17():
    return read_mycsv(datapath("IPCC\\2013 IPCC AR5 The Physical Science Basis\\Table8SM17.csv"), encoding="latin-1", index_col=0)

def GWP_factors():
    return read_mycsv(datapath("IPCC\\2013 IPCC AR5 The Physical Science Basis\\Table8A1.csv"), encoding="latin-1", index_col=0)

def GWP_factors_NOx():
    return read_mycsv(datapath("IPCC\\2013 IPCC AR5 The Physical Science Basis\\Table8A3.csv"), encoding="latin-1", index_col=0)

def fraction_of_biocharC_remaining_in_soil():
    return read_mycsv(datapath("IPCC\\2019 IPCC Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories\\19R_V4_Ch02_Ap4_Table 4A2.csv"), encoding="latin-1", index_col=0)
