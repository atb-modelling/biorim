from read import datapath, read_mycsv

def Table6():
    return read_mycsv(datapath("Jurgutis_et_al_2021\\Table6.csv"), encoding="latin-1", index_col=0)