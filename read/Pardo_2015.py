from read import read_mycsv, datapath

def gas_emissions_solid_waste():
    return read_mycsv(datapath("Pardo_2015\\Pardo_et_al_2015_table2.csv"), encoding="latin-1", index_col=0, header=[0,1])
