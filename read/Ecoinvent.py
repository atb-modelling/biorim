__all__ = ["Ecoinvent34"]

import os
import numpy as np
import pandas as pd
import warnings
import shutil
import xml.etree.ElementTree as ET
import modelconfig
from declarations import Emissions
from tools import convert_molmasses, convert_unit, prod
from read import read_mycsv, datapath, ecoinventpath


class Ecoinvent34:

    def __init__(self, activity="machine operation, diesel, >= 18.64 kW and < 74.57 kW, high load factor", product=None, unit="h", location = "GLO"):
        self.activity = activity
        # if not product is specified try with the activity as product
        if product is None:
            self.product = activity
        else:
            self.product = product
        self.location = location
        self.unit = unit

        # lookup table with name of XML file (converted SPOLD - changed ending to XML) and items ('AcitivityName', 'ReferenceProduct') and location 
        FilenameToActivity = pd.read_csv(datapath("ecoinvent\\ecoinvent_34_cutoff_ecoSpold02\\FilenameToActivtiyLookup.csv"), delimiter=";") 
        FilenameToActivity.set_index(["ActivityName","ReferenceProduct","Location"], inplace=True)

        self.filename = FilenameToActivity.loc[(self.activity, self.product, self.location),"Filename"].replace(".spold", "")

        # check if file exists locally, if notcopy from network location
        if not os.path.isfile(datapath("ecoinvent/ecoinvent_34_cutoff_lci_ecoSpold02/datasets/" + self.filename + ".xml")):
            try:
                shutil.copy(ecoinventpath("ecoinvent_34_cutoff_lci_ecoSpold02/datasets/" + self.filename + ".spold"),datapath("ecoinvent/ecoinvent_34_cutoff_lci_ecoSpold02/datasets/" + self.filename + ".xml"))
            except:
                warnings.warn("Please check your modelconfig to set your local_ecoinvent_path for uploading the corresponding ecoinvent xml file.")

        ## read data from the .xml file
        tree = ET.parse(datapath("ecoinvent/ecoinvent_34_cutoff_lci_ecoSpold02/datasets/" + self.filename + ".xml")) #
        root = tree.getroot()

        ns = {'EcoSpold02': 'http://www.EcoInvent.org/EcoSpold02'}

        FlowDataXml = root[0][1]

        Q = []
        U = []
        Name = []
        Compartment = []
        Subcompartment = []

        for Exchange in FlowDataXml:
            if Exchange.tag == "{http://www.EcoInvent.org/EcoSpold02}intermediateExchange":
                MainUnit = Exchange.find('EcoSpold02:unitName',ns).text
            elif Exchange.tag == "{http://www.EcoInvent.org/EcoSpold02}elementaryExchange":
                Name.append(Exchange.find('EcoSpold02:name',ns).text)
                Q.append(float(Exchange.attrib["amount"]))
                U.append(Exchange.find('EcoSpold02:unitName',ns).text)
                Compartment.append(Exchange.findall('EcoSpold02:compartment',ns)[0].find('EcoSpold02:compartment',ns).text)
                Subcompartment.append(Exchange.findall('EcoSpold02:compartment',ns)[0].find('EcoSpold02:subcompartment',ns).text)

        self.MainUnit = MainUnit

        self.flowdata = pd.DataFrame(list(zip(Q, U, Compartment, Subcompartment)),
                        columns=["Q", "U", "Compartment", "Subcompartment"],
                        index=Name)

        ## check whether the specified unit agrees with the unit from the database
        EcoInventUnit = {'hour': "h",
                        'kg' : "kg",
                        'kWh': "kWh",
                        'm3': "m³",
                        'unit': "n",
                        'metric ton*km': "tkm",
                        'l': 'l'}

        if not self.unit in EcoInventUnit.values():
            warnings.warn(f"Specified unit '{self.unit}' not defined yet.")
        
        elif (self.unit != EcoInventUnit[MainUnit]):
            warnings.warn(f"Unit '{self.unit}' of {self.activity} does not agree with the Ecoinvent database unit '{MainUnit}'")


    def emissions(self, substance = ["CO2", "CH4", "N2O", "NH3"], compartment="air", search=True, sum_transpose=True, convert_to_element=True):

        if substance is not None:
            # convert substance specified as chemical formula to full name
            FormulaToName = {'CO2': "Carbon dioxide",
                             'CH4': "Methane",
                             'N2O': "Dinitrogen monoxide",
                             'NH3': "Ammonia"}

            if len(set(substance).intersection(FormulaToName.keys())) > 0:
                substance_in_dict = list(set(substance).intersection(FormulaToName.keys()))
                if len(substance_in_dict) != len(substance):
                    warnings.warn("Specified substance not known. Try writing out all names, e.g. 'Carbon dioxide'.")
                substance = [FormulaToName.get(key) for key in substance_in_dict]
            
            # if search argument is True, then use filter to look for the substance
            if search:
                # Emis = pd.DataFrame()
                Emis = Emissions()

                for s in substance:
                    EmisElement = self.flowdata.filter(like=s, axis=0).copy()
                    EmisElement.loc[:,"item"] = s
                    Emis = pd.concat([Emis, EmisElement], sort=False)
            else:
                Emis = self.flowdata.loc[substance,:]
                Emis.loc[:,"item"] = Emis.index

        else:
            Emis = self.flowdata

        ## select according to compartment
        if compartment is not None:
            Emis = Emis.loc[Emis["Compartment"] == compartment]

        ## now everything should be in kg, since this information is now lost
        if any(Emis["U"] != "kg"):
            warnings.warn("Some emissions reported in unit other than 'kg'!")

        ## summarize and transpose and convert to molmasses
        if sum_transpose:
            Emis = Emis.groupby(["item"], sort=True).sum(numeric_only=True)

            NameToFormula = {v: k for k, v in FormulaToName.items()}
            Emis.rename(NameToFormula, inplace=True)
            Emis = Emis.T

            Emis.index = pd.Index([self.activity])

        if convert_to_element:
            Emis = convert_molmasses(Emis, to="element")

        return Emis


""" test case:
Ecoinvent34("ammonium nitrate phosphate production", "nitrogen fertiliser, as N", unit= "kg", location = "RER").emissions(sum_transpose_convert=False)
Ecoinvent34("ammonium nitrate phosphate production", "nitrogen fertiliser, as N", unit= "kg", location = "RER").emissions()

Ecoinvent34("composting facility construction, open", "composting facility, open", unit="h", location="RoW")
"""