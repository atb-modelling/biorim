from read import datapath, read_mycsv

## import the methane conversion factors (MCF) from 2006 IPCC Guidelines for National Greenhouse Gas Inventories

def N2O_EF_manure_management():
    return read_mycsv(datapath("EEA\\Air pollutant emission inventory guidebook 2016\\Table 3.8.csv"), encoding="latin-1", index_col=0)

def NH3_N_EF_manure_management():
    return read_mycsv(datapath("EEA\\Air pollutant emission inventory guidebook 2016\\Table 3.9.csv"), encoding="latin-1", index_col=[0,1])

def N_EF_spreading():
    return read_mycsv(datapath("EEA\\Air pollutant emission inventory guidebook 2016\\Chapter 3D Table 3.1.csv"), encoding="latin-1", index_col=0)