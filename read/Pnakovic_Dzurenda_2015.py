from read import datapath, read_mycsv

def Fig3():
    return read_mycsv(datapath("Pnakovic_Dzurenda_2015\\Fig3.csv"), encoding="latin-1", index_col=0)

def Table2():
    return read_mycsv(datapath("Pnakovic_Dzurenda_2015\\Table2.csv"), encoding="latin-1", index_col=0)