### read data from the Regionaldatenbank Deutschland (https://www.regionalstatistik.de/genesis/online/)

import pandas as pd
import numpy as np
from read import datapath
from read.Eurostat import NUTS_LAU

english_translation = {
    'Ackerland insgesamt': "arable land",
    'Gerste' : "barley",
    'Getreide' : "cereals",
    'Hafer' : 'oat',
    'Hülsenfrüchte' : "pulses",
    'Kartoffeln' : "potatoes",
    'Körnermais/Corn-Cob-Mix' : "grain maize/corn-cobb-mix",
    'Pflanzen zur Grünernte': "green-harvested crops",
    'Roggen und Wintermenggetreide': "rye and maslin",
    'Silomais': "silage maize",
    'Silomais/Grünmais': "silage maize",
    'Sommergerste': "summer barley",
    'Sonstiges Getreide': "other cereals",
    'Triticale': "triticale",
    'Weizen' : "wheat",
    'Wintergerste': "winter barley",
    'Winterraps' : "winter oilseed rape",
    'Winterweizen (einschließlich Dinkel und Einkorn)' : "winter wheat (incl. dinkel and einkorn wheat)",
    'Winterweizen' : "winter wheat",
    'Zuckerrüben' : "sugar beet",
    'Ölfrüchte' : "oil seeds"
}

def crop_area(NUTS=True, english=True):
    d = pd.read_csv(datapath("Regionaldatenbank/Feldfruechte/41141-02-02-4.csv"), encoding="latin-1", header=0, skiprows=[0,1,2,3,4,5,6,8,9], skipfooter = 4, engine="python", sep=";")
    d.columns.values[0] = "LAU_5digit"
    d.columns.values[1] = "region"
    d[d=="."] = np.nan
    d[d=="-"] = 0
    d["region"].replace("^ *", "", regex=True, inplace=True) # remove leading space characters
    # convert data columns to float
    columns = list(d.columns)[2:]
    t = dict(zip(columns,["float"]*len(columns)))
    d = d.astype(dtype=t)
    if english == True:
        d.rename(columns=english_translation, inplace=True)
    if NUTS == True:
        NUTSindex = NUTS_LAU().loc[:,["NUTS0", "NUTS1", "NUTS3", "LAU_5digit"]].drop_duplicates()
        d = d.merge(right=NUTSindex, how="right", on="LAU_5digit")
        d.set_index(["NUTS0", "NUTS1", "NUTS3"], inplace=True)
        d.drop(columns="LAU_5digit", inplace=True)
    else:
        d.set_index("LAU_5digit", inplace=True)
    d.unit = "ha"
    return d

def yields(NUTS=True, english=True):
    d = pd.read_csv(datapath("Regionaldatenbank/Feldfruechte/41241-01-03-4.csv"), encoding="latin-1", header=0, skiprows=5, skipfooter = 4, engine="python", sep=";", decimal=",")
    d.columns.values[0] = "year"
    d.columns.values[1] = "LAU_5digit"
    d.columns.values[2] = "region"
    d.year = d["year"][0]
    d.drop(columns="year", inplace=True)
    d[d=="."] = np.nan
    d[d=="-"] = np.nan
    d["region"].replace("^ *", "", regex=True, inplace=True) # remove space characters
    d.loc[:, d.columns != "region"] = d.drop(["region"],axis=1).replace(",", ".", regex=True) # replace decimal comma with dot
    columns = list(d.columns)[2:]
    t = dict(zip(columns,["float"]*len(columns)))
    d = d.astype(dtype=t)
    if english == True:
        d.rename(columns=english_translation, inplace=True)
    if NUTS == True:
        NUTSindex = NUTS_LAU().loc[:,["NUTS0", "NUTS1", "NUTS3", "LAU_5digit"]].drop_duplicates()
        d = d.merge(right=NUTSindex, how="right", on="LAU_5digit")
        d.set_index(["NUTS0", "NUTS1", "NUTS3"], inplace=True)
        d.drop(columns="LAU_5digit", inplace=True)
    else:
        d.set_index("LAU_5digit", inplace=True)
    d.unit = "dt/ha"
    return d


### try to include the functionality to read the data from the genesis api
# some functionality copied from 'genesisclient' from Marian Steinbach

