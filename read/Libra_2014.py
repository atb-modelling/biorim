from read import datapath, read_mycsv

def HTC_material_characteristics():
    return read_mycsv(datapath("Libra_2014\\Table 5.csv"), encoding="latin-1", index_col=0)

def pyrolysis_material_characteristics():
    return read_mycsv(datapath("Libra_2014\\Table 6.csv"), encoding="latin-1", index_col=0)

