from read import datapath, read_mycsv

def N_E_ts_manure_managements():
    return read_mycsv(datapath("Tiquia_Tam_2000_2002\\Table 2 (2000).csv"), encoding="latin-1", index_col=0)

def all_E_manure_managements():
    return read_mycsv(datapath("Tiquia_Tam_2000_2002\\Table 3_4 (2002).csv"), encoding="latin-1", index_col=0)

