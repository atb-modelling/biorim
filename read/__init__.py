import pandas as pd
import csv
import re
import json
import os

import modelconfig

__all__ = ['Amon_2001', 'Andersen_2010', 'Bacenetti_2015', 'Boldrin_and_Christensen', 'Boldrin_et_al_2010', 'Chen_2018',
           'Cimo_2014', 'Cobb', 'DESTATIS', 'DLG', 'Ecoinvent', 'EEA', 'EMeRGE', 'EPA', 'Eurostat', 'FAOSTAT', 'FernandezLopez_2015',
           'GHG_protocol', 'Herrmann', 'Input_Prices', 'IPCC', 'Jurgutis_et_al_2021', 'KTBL', 'Landtag_BB', 'Lauf_2019',
           'LAZBW', 'LfL_BY', 'Libra_2014', 'LWK_NRW', 'LWK_NS', 'Miah2016', 'MontielBohorquez_Perez', 'Moro2018',
           'Munoz_Schmidt_2016', 'Nicholson1996', 'Pardo_2015', 'Peters_2015', 'Pnakovic_Dzurenda_2015', 'Prices',
           'Regionaldatenbank', 'Rodhe_Karlsson_2002', 'Ro_2010', 'Song_Guo_2011', 'Street_leaves_collection', 'Terboven',
           'Thannimalay_2013', 'Thuenen', 'Tiquia_Tam', 'VDLUFA', 'Vergara_2019']


## function that sets the relative data path
def datapath(x):
    return os.path.abspath(os.path.join(os.path.abspath(modelconfig.data_path), os.path.relpath(x)))

## path to the ecoinvent database
def ecoinventpath(x):
    return os.path.abspath(os.path.join(os.path.abspath(modelconfig.ecoinvent_path), os.path.relpath(x)))


## function that reads csv documents with comments (description and source) as pandas documents
# can currently not handle empty entries in the first line after "# Source"
def read_mycsv(x, sep=";", encoding="utf-8", **kwargs):
    if "delimiter" in kwargs:
        raise ValueError("Please specify 'sep' instead of 'delimiter'")
    with open(x, encoding=encoding) as csvfile:
        reader = csv.reader(csvfile, delimiter=sep)
        l = []
        for row in reader:
            l.append(row[0])
            if not row[0]:
                break
            elif (row[0][0]!="#"):
                break
    l = l[:-1]
    tab = pd.read_csv(x, sep=sep, skiprows=len(l), encoding=encoding, **kwargs)
    tab.description = re.sub(r'# Description: ',"",str([s for s in l if "# Description" in s][0]))
    tab.source = re.sub(r'# Source: ',"",str([s for s in l if "# Source" in s][0]))
    tab.file = re.sub(re.escape("\\"), "/", x)
    return tab

class myjson(dict):
    def __init__(self,x, description, source):
        super(myjson,self).__init__(x)
        self.description = description
        self.source = source
   
def read_myjson(x):
    with open(x, 'r') as f:
        y = json.load(f)
    z = myjson(y["Data"], description=y["Description"],source=y["Source"])
    z.file = x
    return z