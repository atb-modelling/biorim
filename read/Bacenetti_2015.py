from read import datapath, read_mycsv

def Table2():
    return read_mycsv(datapath("Bacenetti_2015\\Table2.csv"), encoding="latin-1", index_col=0)

def Table3():
    return read_mycsv(datapath("Bacenetti_2015\\Table3.csv"), encoding="latin-1", index_col=0)

def Table4():
    return read_mycsv(datapath("Bacenetti_2015\\Table4.csv"), encoding="latin-1", index_col=0)