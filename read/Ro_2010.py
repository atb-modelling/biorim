from read import datapath, read_mycsv

def noncondensable_gases():
    return read_mycsv(datapath("Ro_2010\\Table2.csv"), encoding="latin-1", index_col=0, header=[0,1])