from read import datapath, read_mycsv

def emission_factor_storage():
    return read_mycsv(datapath("Amon_2001\\Table_9.csv"), encoding="latin-1", index_col=0)

