from read import datapath, read_mycsv

def humus_reproduction():
    return read_mycsv(datapath("VDLUFA\\page 20.csv"), encoding="latin-1", index_col=0)

def humus_consumption_crops():
    return read_mycsv(datapath("VDLUFA\\table2a.csv"), encoding="latin-1", index_col=0)
