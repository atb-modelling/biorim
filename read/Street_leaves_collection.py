from read import datapath, read_mycsv

## input data parameters found in the literature for tree leaves collection

def blower_production():
    return read_mycsv(datapath("Street_leaves_collection\\blower_production.csv"), encoding="latin-1", index_col=0)

def blower_operation():
    return read_mycsv(datapath("Street_leaves_collection\\blower_operation.csv"), encoding="latin-1", index_col=0)

def small_sweeper_production():
    return read_mycsv(datapath("Street_leaves_collection\\small_sweeper_production.csv"), encoding="latin-1", index_col=0)

def small_sweeper_operation():
    return read_mycsv(datapath("Street_leaves_collection\\small_sweeper_operation.csv"), encoding="latin-1", index_col=0)

def large_sweeper_production():
    return read_mycsv(datapath("Street_leaves_collection\\large_sweeper_production.csv"), encoding="latin-1", index_col=0)

def large_sweeper_operation():
    return read_mycsv(datapath("Street_leaves_collection\\large_sweeper_operation.csv"), encoding="latin-1", index_col=0)