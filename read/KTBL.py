from read import datapath, read_mycsv

## import the methane conversion factors (MCF) from 2006 IPCC Guidelines for National Greenhouse Gas Inventories

def biogas_climate_balances_BEK():
    return read_mycsv(datapath("KTBL\\BEK_2016.csv"), encoding="latin-1", index_col=0)

def biogas_fist_numbers():
    return read_mycsv(datapath("KTBL\\Faustzahlen_Biogas.csv"), encoding="latin-1", index_col=0)

def biogas_biogas_yields():
    return read_mycsv(datapath("KTBL\\Gasausbeuten_Faustzahlen_Biogas.csv"), encoding="latin-1", index_col=0)

def cow_manure_amounts():
    return read_mycsv(datapath("KTBL\\Faustzahlen_Landwirtschaft_Mengenanfall_Guelle.csv"), encoding="latin-1", index_col=[0,1,2], header=0)



# from SPARQLWrapper import SPARQLWrapper, JSON

# # Query all machines and their labels:
# sparql = SPARQLWrapper("http://srv.ktbl.de/query")
# sparql.setQuery("""
#     PREFIX ktblvoc: <http://srv.ktbl.de/vocabulary#>
#     PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

#     SELECT ?uri ?label WHERE {
#     ?uri a ktblvoc:MachineClass .
#     ?uri rdfs:label ?label .
#     }
# """)

# sparql.setReturnFormat(JSON)
# results = sparql.query().convert()

# # just the first machine in the list
# results["results"]["bindings"][0]


# # to get the data for the machine
# from rdflib import Graph
# g = Graph()
# g.parse('http://srv.ktbl.de/data/MachineClass/1622.ttl', format='ttl')
