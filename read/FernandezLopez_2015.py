from read import datapath, read_mycsv

def gaseous_emissions():
    return read_mycsv(datapath("Fernandez-Lopez_2015\\Fig 5.csv"), encoding="latin-1", index_col=0)

