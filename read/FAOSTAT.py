"""
Data from the FAOSTAT database
"""

import pandas as pd
from read import datapath

# common read in function
def read_fao(file, description):
    f = "FAOSTAT/" + file + ".csv"
    d = pd.read_csv(datapath(f), encoding="latin-1", sep=",")
    d.set_index(["Area", "Item", "Element", "Unit"], inplace=True)
    d.sort_index(inplace=True)
    d.description = description
    return d


def fertilizers():
    return read_fao("Inputs_FertilizersNutrient_E_All_Data", "Nutrients in chemical and mineral fertilizers")

def livestock_primary():
    return read_fao("Production_LivestockPrimary_E_All_Data", "Amount of livestock (primary)")

def manure():
    return read_fao("Environment_Manure_E_All_Data", "Amount of manure and associated N amounts")