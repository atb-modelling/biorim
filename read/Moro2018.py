from read import datapath, read_mycsv

def carbon_intensity_electricity():
    return read_mycsv(datapath("Moro_2018\\Electricity carbon intensity in European Member States.csv"), encoding="latin-1", index_col=0)
