### read the shapefile of the NUTS3 regions

import geopandas as gpd
import pandas as pd
from read import datapath

def NUTS3():
    return gpd.read_file(datapath("Eurostat\\ref-nuts-2013-01m.shp\\NUTS_RG_01M_2013_4326_LEVL_3.shp"), encoding="utf-8")

def NUTS1_Bundeslaender():
    return pd.read_csv(datapath("Eurostat\\NUTS1_Bundeslaender.csv"), sep=";", encoding="latin-1")

def NUTS_LAU(region="DE"):
    # individually converted to csv, because reading Excel file is super slow
    if region=="DE":
        data = pd.read_csv(datapath("Eurostat\\EU-28-LAU-2019-NUTS-2016_de.csv"), sep=";", encoding="latin-1", dtype="str", usecols=[0,1,2])
        data.rename(columns={'NUTS 3 CODE':"NUTS3",
                     'LAU CODE':"LAU",
                     'LAU NAME NATIONAL': "NAME"}, inplace=True)
        data["LAU_5digit"] = data["LAU"].str[:5]
        data["NUTS1"] = data["NUTS3"].str[:3]
        NUTS1 = NUTS1_Bundeslaender().loc[:,["ID", "NUTS1"]]
        data = data.merge(NUTS1, on="NUTS1")
        data["NUTS0"] = "DE"
    return data


