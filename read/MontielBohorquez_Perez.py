from read import datapath, read_mycsv

def Table1():
    return read_mycsv(datapath("MontielBohorquez_Perez\\Table1.csv"), encoding="latin-1", index_col=0)

def Fig5():
    return read_mycsv(datapath("MontielBohorquez_Perez\\Fig5.csv"), encoding="latin-1", index_col=0)