from read import datapath, read_mycsv

def emission_factor_winter_IBC():
    return read_mycsv(datapath("EMeRGE\\emission_factors_winter_storage_IBC.csv"), encoding="latin-1", index_col=0, sep=";", decimal=",")