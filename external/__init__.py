__all__ = ["ExternInput", "ExternOutput", "Electricity", "Heating"]


import pandas as pd
import numpy as np

from math import isnan

from tools import convert_molmasses, prod, remove_system
from read import Moro2018
from read.Ecoinvent import Ecoinvent34
from read.Prices import InputPrice
from parameters import ConversionFactor
from declarations import Emissions, Process



class ExternInput(Process):
    """
    |  Represents systems not explicitly represented in detail in the model. Currently this is mainly a link to the Ecoinvent database.
    | 
    """
    def __init__(self, O = prod("nitrogen fertiliser, as N", 10, "kg", location = "RER", activity="ammonium nitrate phosphate production"), id = None):
        self.O = O
        self.id = id

    def input(self):
        return None

    def emissions(self, substance = ["CO2", "CH4", "N2O", "NH3"], compartment="air", search=True):
        """
        | *The emissions method is exclusively written to obtain ecoinvent emissions entries.*
        | *It returns a pd.Series with the values for the gases "Carbon dioxide", "Dinitrogen monoxide" and "Methane"*

        |
        """
        Emis = Emissions()

        for index, row in self.O.iterrows():
            
            location = row["location"]
            QUANTITY = row["Q"]
            unit = row["U"]
            product = index

            activity = row.get("activity", product)
            if not isinstance(activity, str):
                activity = product

            # the manually defined cases. Currently water is defined as causing no emissions
            if product in ["water"]:
                Emis = pd.concat([Emis, pd.DataFrame({'CO2-C':0}, index=product)])

            # if no method defined manually try to use Ecoinvent
            else:
                try:
                    # multiply the Ecoinvent derived emissions with the quantity
                    ProdEmis = Ecoinvent34(activity, product, unit, location).emissions(substance=substance, compartment=compartment, search=search, sum_transpose=True, convert_to_element=True) * QUANTITY
                    Emis = pd.concat([Emis, ProdEmis])

                except:
                    raise ValueError (f"Currently no emission source available for product: {product}, location: {location}")

        return Emis

    def output(self):
        return self.O
    
    def cashflow(self):
        """
        |  First implementation of cashflows.
        |  **NOTE:**
        |  - All items listed here should probably be moved to individual producition functions, as the purpose of this ExternalInput class is to be a very generic link to ecoinvent.
        """
        Cashflow = pd.DataFrame(index=[], columns=[])

        # TODO: Source of this data remains unclear. Better use read functions.
        # TODO: Avoid ifelse construction

        PRICE_RAPESEED_OIL = InputPrice().rapeseed_oil()["Q"].item() #EUR/tonne
        PRICE_LUBRICATING_OIL= InputPrice().lubricating_oil()["Q"].item() #EUR/liter
        PRICE_GLYCEROL=0.25 #EUR/kg https://www.selinawamucii.com/insights/prices/germany/glycerol/ #NOTE: the prices of glycerol tends to vary, must be included in the sensitivity analysis (previous was 0.74)
        DENSITY_LUBRICATING_OIL=0.85 #kg/liter from a range between 0.7 to 0.95
        

        if self.O.index == "rape oil, crude": #standard unit: kg
            Cashflow.loc["rape oil, crude", "EUR"] = PRICE_RAPESEED_OIL/1000 * self.O.loc["rape oil, crude", "Q"]

        elif self.O.index == "lubricating oil": #standard unit: kg
            Cashflow.loc["lubricating oil", "EUR"] = (PRICE_LUBRICATING_OIL/DENSITY_LUBRICATING_OIL) * self.O.loc["lubricating oil", "Q"]

        elif self.O.index == "trichloropropane": #standard unit: kg
            Cashflow.loc["trichloropropane (glycerol)", "EUR"] = PRICE_GLYCEROL * self.O.loc["trichloropropane", "Q"]

        else:
            Cashflow = None
    	
        Cashflow["period_years"]=1
        Cashflow["section"]="opex"

        return Cashflow



## TODO: ExternOutput should be adapted to the same approach as externInput as above. Maybe it should just invert results.
# This approach is problematic, since output that is waste does not generate negetive emissions

class ExternOutput2(ExternInput):
    def emissions(self):
        return super().emissions() * -1


## Try out:
# ExternOutput2(O=prod(("market for electricity, high voltage","electricity, high voltage"),2,"kWh", location="DE")).emissions()


class ExternOutput(Process):
    """
    | *The class* ``externOutput`` *represents all external classes that are not explicitly modelled.*
    | *The function is to account for positive external effects obtain specific ecoinvent entries for the emissions of the external quantity.*
    | *Thereby it focuses on the main (important) gases "Carbon dioxide", "Dinitrogen monoxide" and "Methane" (this list can be complemented!)*

    | 
    """
    def __init__(self, I = prod(('electrical energy'), 1, "kWh"), id = None):
        self.I = I
        self.id = id

    # TODO: This needs to be generalized. Should not only work with this very specific index "electrical energy"
    def emissions(self):
        I = remove_system(self.I)
        
        if "electrical energy" in I.index:
            if I.loc["electrical energy", "U"] == "kWh":
                ENERGY_kWh = -1 * I.loc["electrical energy", "Q"]
                Emis = ENERGY_kWh * Ecoinvent34('market for electricity, high voltage', 'electricity, high voltage', unit="kWh", location="DE").emissions()
            else:
                raise Exception ("Other units than 'kWh' currently not supported")

            return Emis
        
        else:
            return None

    def input(self):
        """
        | *Currently no  inputs defined.*
        |
        """        
        return self.I

    def output(self):
        """
        | *Currently no specific output defined.*
        |
        """        
        return None


class Electricity(Process):

    # how I and O are defined: prod(('electrical energy'), 1, "kWh")

    def __init__(self, I = None, O = None, id = None):
        self.I = I
        self.O = O
        self.id = id

    def input(self):
        return self.I

    def output(self):
        if self.O is not None:
            return self.O
        else:
            return None
    
    def cashflow(self):
        """
        |  All cashflows for electricity flow to the grid
        |  **NOTE:**
        |  - These costs are not yet related to the production quantities.
        |  - Revenues for electricity fed to grid not yet available (?)
        """
        Cashflow = pd.DataFrame(index=[], columns=[])

        #Costs of electriciy flow from the grid
        if self.O is not None:
            if self.O.loc["electrical energy", "Q"]<50000:
                Cashflow.loc["electrical energy from grid", "EUR"] = InputPrice().electricity().loc["electricity, business","Q"] * self.O.loc["electrical energy", "Q"]
            elif self.O.loc["electrical energy", "Q"]>=50000 and self.O.loc["electrical energy", "Q"]<24000000:
                Cashflow.loc["electrical energy from grid", "EUR"] = InputPrice().electricity().loc["electricity, large use, business","Q"] * self.O.loc["electrical energy", "Q"]
            elif self.O.loc["electrical energy", "Q"]>=24000000:
                Cashflow.loc["electrical energy from grid", "EUR"] = InputPrice().electricity().loc["electricity, large use, industry","Q"] * self.O.loc["electrical energy", "Q"]

            Cashflow.loc["electrical energy from grid", "period_years"] = 1
            Cashflow.loc["electrical energy from grid", "section"] = "opex"
        
        if self.I is not None:
            Cashflow.loc["electrical energy to grid", "EUR"] = 0
            Cashflow.loc["electrical energy to grid", "period_years"] = 1
            Cashflow.loc["electrical energy to grid", "section"] = "opex"
        
        return Cashflow

    ## emissions generated or compensated by electrical energy production. Calculated based on Moro, A., Lonza, L., 2018. Electricity carbon intensity in European Member States: Impacts on GHG emissions of electric vehicles. Transp. Res. Part D Transp. Environ. 64, 5–14. https://doi.org/10.1016/j.trd.2017.07.012
    def emissions(self):
        Emis_CO2eq = pd.DataFrame()
        carbon_intensity_gCO2 = Moro2018.carbon_intensity_electricity()

        if self.O is not None:
            Emis_CO2eq.loc["electrical energy from grid","CO2"] = self.O["Q"].item() * carbon_intensity_gCO2.loc[self.O.loc[:,"location"].item(),"electricity consumed at LV (with upstream)"]/1000
            
        elif self.I is not None:
            Emis_CO2eq.loc["electrical energy to grid","CO2"] = -1 * self.I["Q"].item() * carbon_intensity_gCO2.loc["DEU","electricity supplied (with upstream)"]/1000

        Emis = convert_molmasses(Emis_CO2eq, to="element")

        return Emis


class Heating(Process):
    def __init__(self, I = None, O = None, id = None):
        self.I = I
        self.O = O
        self.id = id

    def input(self):
        return self.I

    def output(self):
        if self.O is not None:
            return self.O
        else:
            return None

    def emissions(self):
        
        ## assumption is currently a replacement of natural gas:
        HEATING_VALUE_NATGAS_H = 10.42 # kWh/m³ see: References/Erdgastechnik_Zahlen-Daten-Fakten_BDEW.pdf
        DENSITY_NATGAS_H = 0.79 # kg/m³
        LOSS_OF_CH4 = 0.023 # The share of natural gas that is lost during production, transmission and burning as CH4 (emitted additionally to the one burned) adapted from Alvarez, R.A., Zavala-Araiza, D., Lyon, D.R., Allen, D.T., Barkley, Z.R., Brandt, A.R., Davis, K.J., Herndon, S.C., Jacob, D.J., Karion, A., Kort, E.A., Lamb, B.K., Lauvaux, T., Maasakkers, J.D., Marchese, A.J., Omara, M., Pacala, S.W., Peischl, J., Robinson, A.L., Shepson, P.B., Sweeney, C., Townsend-Small, A., Wofsy, S.C., Hamburg, S.P., 2018. Assessment of methane emissions from the U.S. oil and gas supply chain. Science (80-. ). 361, 186–188. https://doi.org/10.1126/science.aar7204

        def emissions_prod_burn(kWh):
            Emis = pd.DataFrame()
            gas_burned_m3 = kWh / HEATING_VALUE_NATGAS_H
            gas_burned_kg = gas_burned_m3 * DENSITY_NATGAS_H

            Emis.loc["leakage production + transmission", "CH4-C"] = gas_burned_kg * LOSS_OF_CH4 * ConversionFactor.loc[("CH4", "CH4-C"), "factor"]
            Emis.loc["natural gas burning", "CO2-C"] = gas_burned_kg * ConversionFactor.loc[("CH4", "CH4-C"), "factor"] # assumption of complete burning as CO2: all CH4-C is then CO2-C
            return Emis

        if (self.O is not None) and (self.I is None):
            if all(self.O["U"] == "kWh"):
                kWh = self.O["Q"].item()
        elif (self.I is not None) and (self.O is None):
            if all(self.I["U"] == "kWh"):
                kWh = -1 * self.I["Q"].item()
        else:
            raise ValueError("defining I and O at the same time not possible")

        return emissions_prod_burn(kWh)

    def cashflow(self):
        """
        |  All cashflows for electricity flow to the grid
        |  **NOTE:**
        |  - These costs are not yet related to the production quantities.
        """
        Cashflow = pd.DataFrame(index=[], columns=[])

        # Costs of electriciy flow from the grid
        if self.O is not None:
            Cashflow.loc["thermal energy from grid", "EUR"] = 0
            Cashflow.loc["thermal energy from grid", "period_years"] = 1
            Cashflow.loc["thermal energy from grid", "section"] = "opex"
        
        if self.I is not None:
            Cashflow.loc["thermal energy to grid", "EUR"] = 0
            Cashflow.loc["thermal energy to grid", "period_years"] = 1
            Cashflow.loc["thermal energy to grid", "section"] = "opex"