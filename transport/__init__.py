"""
Transport of goods.
"""

__all__ = ["LandTransport"]

import pandas as pd
import numpy as np
import math

from tools import prod, convert_molmasses
from read.Ecoinvent import Ecoinvent34
from read import EPA
from read.Prices import InputPrice
from declarations import Process


class LandTransport(Process):
    """     
    |  All forms of land transport (by lorry or tractor)

    :type I: Product
    :param I: The substrate to be transported
    :type dist_km: numeric  
    :param dist_km: Distance in kilometers for which the substrate is transported
    :type type: string  
    :param type: Transportation type. ``lorry, large size``, ``lorry, medium size`` or ``tractor, small size``
    :type method: string
    :param method: method por GHG calculations
    :type load_basis: string
    :param method: criteria for loading, under basis of 'mass' or 'volumen'
    :type CO2_EF: string
    :param method: CO2 emission factors. Currently from 'IPCC' and 'EPA'
 
    **Notes:**  
    |  Updated to partial loading when "LBE" method is defined. P

    **Examples:**
    LandTransport(I=prod("maize silage", 1, "ton"), dist_km=100).emissions()
    LandTransport(I=prod("maize silage", 10, "ton"), dist_km=100).emissions()
    LandTransport(I=prod("maize silage", 10, "ton"), dist_km=1000).emissions()

    |
    """        
    def __init__(self, I = prod(("testsys","testitem"), 10, "ton"), O = None, dist_km=1, type="lorry, medium size", 
                                method="LBE", load_basis="mass", CO2_EF="EPA", transport_cost="sommer_elso",  id=None):
        # When O is defined and I set to the default, the I is not used
        self.O = O
        self.I = I
        self.dist_km = dist_km
        self.method=method
        method_list=["ecoinvent", "LBE"]
        if self.method not in method_list:
            raise ValueError (f"{self.method} not included. Method must be defined according to this list: {method_list}")
        self.load_basis=load_basis
        if self.load_basis not in ["mass", "volume"]:
            raise ValueError (f"{self.load_basis} not included. Method must be defined between 'mass' or 'volumen'")
        self.CO2_EF=CO2_EF
        self.transport_cost=transport_cost
        self.id = id

        # distinguish between type of transportation
        self.type = type # "lorry, large size", "lorry, medium size", "tractor, small size"

        if self.type == "lorry, large size": # ~40 ton truck - ecoinvent item: "transport, freight, lorry >32 metric ton, EURO6"
            self.LOAD_MASS = 24 # t, potential range = 23-24 t, reference: https://publikationen.sachsen.de/bdb/artikel/13806/documents/16053 -->EcoTransIT: 26 tons
            self.LOAD_VOLUME = 75 # m3, potential range = 75-80 m3, referene: https://www.bruns-umwelttechnik.de/produkte/abrollcontainer/leichte_abrollcontainer/
            self.EMPTY_WEIGHT= 14 # ton, based on EcoTransIT database: https://www.ecotransit.org/download/EcoTransIT_World_Methodology_ShortVersion_2019.pdf 
            self.GVW= self.LOAD_MASS+self.EMPTY_WEIGHT # GVW = Gross Vehicule Weight, variable linked to fuel consumption (EcoTransIT)
            self.EMPTY_CONSUMP=0.245 #l/km when the truck is emtpy 
            self.FULL_CONSUMP=0.335 #l/km when the truck is full
            self.activity = "transport, freight, lorry >32 metric ton, EURO6"
            self.region="RER"

        elif self.type == "lorry, medium size": # ~26 ton truck - ecoinvent activity: "transport, freight, lorry 16-32 metric ton, EURO6"
            self.LOAD_MASS = 17 # t, potential range = 12-13 t, (reference: s.o), changed to the EcoTransIT data set, previous was 12
            self.LOAD_VOLUME = 38 # m3, potential range = 38-40 m3, (reference: s.o)
            self.EMPTY_WEIGHT=9 # ton, based on EcoTransIT database: https://www.ecotransit.org/download/EcoTransIT_World_Methodology_ShortVersion_2019.pdf 
            self.GVW= self.LOAD_MASS+self.EMPTY_WEIGHT
            self.EMPTY_CONSUMP=0.275 #l/km when the truck is emtpy
            self.FULL_CONSUMP=0.35 #l/km when the truck is full
            self.activity = "transport, freight, lorry 16-32 metric ton, EURO6"
            self.region="RER"

        elif self.type == "tractor, small size": # tractor and trailer - ecoinvent activity: "transport, tractor and trailer, agricultural_GLO_2002"
            self.LOAD_MASS = 8 # t, ecoinvent activity: "transport, tractor and trailer, agricultural_GLO_2002"
            self.LOAD_VOLUME = 12.5 # m3, potential range = 10-15 m3, (reference: various internet comparisons)
            self.EMPTY_WEIGHT= 6 # ton, based on EcoTransIT database: https://www.ecotransit.org/download/EcoTransIT_World_Methodology_ShortVersion_2019.pdf 
            self.GVW= self.LOAD_MASS+self.EMPTY_WEIGHT
            self.EMPTY_CONSUMP=0.225 #l/km when the truck is emtpy
            self.FULL_CONSUMP=0.27 #l/km when the truck is full
            self.activity = "transport, tractor and trailer, agricultural"
            self.region = "CH" # somehow RoW data does not exist (anymore ?) even though it should be after lookup table (dataset incomplete?)
        else:
            print(str(self.type) +" is not included for class transport.")
    
    def _fuel_consumption_slope(self):
        #determining the formula of fuel consumption as a linear regression (assumption), using np.polyfit()
        x=np.array([self.EMPTY_WEIGHT, self.GVW])
        y=np.array([self.EMPTY_CONSUMP,self.FULL_CONSUMP])
        m,b=np.polyfit(x,y,1)
        return (m,b)
    
    def _total_distance(self):

        if self.method=="LBE":
            EMPTY_HAULING=0.2 # 20% of the road from A to B is driven empty, from the parking spot to A. Range in Germany between 20-25%.
            if self.load_basis=="mass": 
                NUMBER_TRIPS=math.ceil(self.input()["Q"].item()/self.LOAD_MASS) # need to be a condition whether is mass basis or volumen basis
            else:
                VOLUMEN_CHECK=self.LOAD_MASS/self.LOAD_VOLUME
                if self.input()["density"].item()<=VOLUMEN_CHECK:
                    TOTAL_VOLUME=self.input()["Q"].item()/self.input()["density"].item()
                    NUMBER_TRIPS=math.ceil(TOTAL_VOLUME/self.LOAD_VOLUME)
                else:
                    raise ValueError(f"The density of input is higher than the density of the loading capacity of the truck: {VOLUMEN_CHECK} t/m3. Use instead 'load_basis' as 'mass'")

            TOTAL_DISTANCE=(NUMBER_TRIPS*2+EMPTY_HAULING)*self.dist_km
            return TOTAL_DISTANCE

    def input(self):
        if self.O is None:
            return self.I

        # if O is specified the default of I is just neglected
        elif (self.O is not None) and self.I.equals(prod(("testsys","testitem"), 10, "ton")):
            return self.O

        # if I and O share the same item
        # This is for the case that the config defines where the I should come from, but O describes the quantities
        elif self.O.index.equals(self.I.index):
            O = self.O.copy()
            O.index = self.I.index
            return O

        else:
            raise Exception("Case not defined")

    def emissions(self):
        # NOTE: Even when calculations are done for 1 kg, the previous would mean that a whole truck drive is considered. Therefore not used currently.

        # # check wether substrate mass fits into the truck
        # mass_check = self.I["Q"].item()/self.LOAD_MASS
        # # round up since for a mass greater than LOAD_MASS another truck is required
        # check_rep = math.ceil(mass_check)
        # # if density and therefore a volume is provided then also consider it
        # if "density" in self.I:
        #     # same as above now for volume
        #     volume = self.I["Q"].item()/self.I["density"].item()
        #     volume_check = volume/self.LOAD_VOLUME
        #     check_vol_rep = math.ceil(volume_check)
        #     # if the volume is the "limiting factor" then set it as "check_rep", the repetition of truck drives
        #     if check_vol_rep > check_rep:
        #         check_rep = check_vol_rep
        # # if multiple truck drives are required then split up the masses into equal portions that are driven consecutively/parallel
        # driven_mass = self.I["Q"].item()/check_rep
        # # again the "specific ecoinvent factor" is calculated, this time for tonne-kilometres
        # specific_factor = driven_mass * self.dist
        # Emis = ecoinvent_emissions(item=self.item,factor=specific_factor,location=self.region)*check_rep
        # return Emis

        if self.method =="ecoinvent":
            AMOUNT_TKM = (self.input()["Q"] * self.dist_km).sum(axis=0)
            Emis = AMOUNT_TKM * Ecoinvent34(activity=self.activity, unit="tkm", location=self.region).emissions()
        
        if self.method=="LBE":
            EMPTY_HAULING=0.2 # 20% of the road from A to B is driven empty, from the parking spot to A. Range in Germany between 20-25%.
            if self.load_basis=="mass": 
                NUMBER_TRIPS=math.ceil(self.input()["Q"].item()/self.LOAD_MASS) # need to be a condition whether is mass basis or volumen basis
                #determing last-trip payload
                PAYLOAD_LAST_TRIP=self.input()["Q"].item()%NUMBER_TRIPS
            else:
                VOLUMEN_CHECK=self.LOAD_MASS/self.LOAD_VOLUME
                if self.input()["density"].item()<=VOLUMEN_CHECK:
                    TOTAL_VOLUME=self.input()["Q"].item()/self.input()["density"].item()
                    NUMBER_TRIPS=math.ceil(TOTAL_VOLUME/self.LOAD_VOLUME)
                    #determing last-trip payload
                    VOLUME_LAST_TRIP=TOTAL_VOLUME%NUMBER_TRIPS
                    PAYLOAD_LAST_TRIP=VOLUME_LAST_TRIP*self.input()["density"].item()
                else:
                    raise ValueError(f"The density of input is higher than the density of the loading capacity of the truck: {VOLUMEN_CHECK} t/m3. Use instead 'load_basis' as 'mass'")
            
            #determining  predicted fuel consumption
            m,b =self._fuel_consumption_slope()
            PAYLOAD_LAST_TRIP_FUEL_CONSUMP=m*PAYLOAD_LAST_TRIP+b #following a linear function --> y=m*x+b

            #determining total empty distance to travel and total distance

            #### logic behind the trips 
            
            EMPTY_DIST=self.dist_km*EMPTY_HAULING + NUMBER_TRIPS*self.dist_km #it is asummed that the number of trips with payload are the same as the trips empty (roundtrips)
            TOTAL_DISTANCE=(NUMBER_TRIPS*2+EMPTY_HAULING)*self.dist_km
            
            #lifetime of trucks
            LIFETIME_TRUCK= 180000 #km, assumption based in a old document (1979) from EPA, need to be updated with more recent information, asumming the same value for all type of vehicles
            
            #defining the function of total fuel consumption
            if self.load_basis=="mass": 
                TOTAL_FUEL_CONSUMPTION=self.EMPTY_CONSUMP*EMPTY_DIST+self.FULL_CONSUMP*(NUMBER_TRIPS-1)*self.dist_km +PAYLOAD_LAST_TRIP_FUEL_CONSUMP*self.dist_km #expressed in total liters(l)
            else:
                PAYLOAD_EACH_TRIP=self.LOAD_VOLUME*self.input()["density"].item()
                FUEL_EACH_TRIP=m*PAYLOAD_EACH_TRIP+b
                TOTAL_FUEL_CONSUMPTION=self.EMPTY_CONSUMP*EMPTY_DIST+FUEL_EACH_TRIP*(NUMBER_TRIPS-1)*self.dist_km +PAYLOAD_LAST_TRIP_FUEL_CONSUMP*self.dist_km

            #Emissions diesel production
            DIESEL_DENSITY=0.85 #kg/l
            Emis_diesel_production=TOTAL_FUEL_CONSUMPTION*DIESEL_DENSITY*Ecoinvent34(activity="diesel production, low-sulfur",product="diesel, low-sulfur", unit="kg",location="Europe without Switzerland").emissions()
            Emis_diesel_production.rename({'diesel production, low-sulfur':"diesel production"}, inplace=True)

            #Emissions truck production
            if self.type=="lorry, large size": #not sure if ecoinvent data is refering to the GWV or the empty weight, because in 'tranport' all lorries have the same weight (16 tons), need to be checked
                Emis_truck_production=TOTAL_DISTANCE/LIFETIME_TRUCK*Ecoinvent34(activity="lorry production, 40 metric ton",product="lorry, 40 metric ton",unit="n", location="RER").emissions()
                Emis_truck_production.rename({'lorry production, 40 metric ton':"truck/lorry production"}, inplace=True)

            elif self.type=="lorry, medium size":
                Emis_truck_production=TOTAL_DISTANCE/LIFETIME_TRUCK*Ecoinvent34(activity="lorry production, 28 metric ton",product="lorry, 28 metric ton",unit="n", location="RER").emissions()
                Emis_truck_production.rename({'lorry production, 28 metric ton':"truck/lorry production"}, inplace=True)

            elif self.type=="tractor, small size":
                Emis_truck_production=TOTAL_DISTANCE/LIFETIME_TRUCK*Ecoinvent34(activity="lorry production, 16 metric ton",product="lorry, 16 metric ton",unit="n", location="RER").emissions()
                Emis_truck_production.rename({'lorry production, 16 metric ton':"truck/lorry production"}, inplace=True)

            #Emis diesel combustion

            N2O_EF={"lorry, large size":0.096,
                    "lorry, medium size":0.096,
                    "tractor, small size":0.192} #emissions factors (EF) are expresed in g of N2O/kg fuel(diesel),IPCC (2000) https://www.ipcc-nggip.iges.or.jp/public/gp/bgp/2_3_Road_Transport.pdf

            Emis_combustion=pd.DataFrame()
            if self.CO2_EF=="EPA":
                AMERICAN_GALLON_TO_LITER = 3.78541
                Emis_combustion.loc[f"{self.activity}","CO2"]=EPA.Table2().loc["Diesel fuel","CO2"]/AMERICAN_GALLON_TO_LITER* TOTAL_FUEL_CONSUMPTION

            elif self.CO2_EF=="IPCC":
                Emis_combustion.loc[f"{self.activity}","CO2"]=3172.31/1000*DIESEL_DENSITY*TOTAL_FUEL_CONSUMPTION #the value has units g/kg fuel, IPCC (2000) https://www.ipcc-nggip.iges.or.jp/public/gp/bgp/2_3_Road_Transport.pdf
            
            Emis_combustion.loc[f"{self.activity}","CH4"]=0.86/1000*DIESEL_DENSITY* TOTAL_FUEL_CONSUMPTION #the value has units g/kg fuel, IPCC (2000) https://www.ipcc-nggip.iges.or.jp/public/gp/bgp/2_3_Road_Transport.pdf
            Emis_combustion.loc[f"{self.activity}","N2O"]=N2O_EF[f"{self.type}"]/1000 *DIESEL_DENSITY*TOTAL_FUEL_CONSUMPTION #the value has units g/kg fuel, IPCC (2000) https://www.ipcc-nggip.iges.or.jp/public/gp/bgp/2_3_Road_Transport.pdf
            Emis_combustion.loc[f"{self.activity}","NOx"]=9.76/1000*DIESEL_DENSITY* TOTAL_FUEL_CONSUMPTION #the value has units g/kg fuel, IPCC (2000) https://www.ipcc-nggip.iges.or.jp/public/gp/bgp/2_3_Road_Transport.pdf
            Emis_combustion = convert_molmasses(Emis_combustion, to="element")
        
            Emis = pd.concat([Emis_diesel_production,Emis_truck_production,Emis_combustion])

        return Emis

    def output(self):
        return self.input().copy()

    def cashflow(self):
        """
        |  All cashflows for the investment and operation of transportation
        |  **NOTE:**
        |  - These costs are not yet related to the production quantities.
        """
        Cashflow_capex = pd.DataFrame(index=[], columns=[])
        #cost of installation
        Cashflow_capex.loc["fleet investment", "EUR"]=float(np.NaN)
        Cashflow_capex["period_years"]=float(np.NaN)
        Cashflow_capex["section"]="capex"

        #costs of operation
        Cashflow_opex = pd.DataFrame(index=[], columns=[])
        #NOTE: At the moment, the concept of "costs" in this function is based on the price that could be obtained from the market.
        # The approach, however should include the costs as a part of the operation execution rather than market prices
        
        if self.transport_cost=="sommer_elso":
        #NOTE: Sommer & Elso is a german company for transportation, and this prices are valid from 01.10.22. Set as default method
        # Prices are for Gruppe 4, 5 and 7. More information: https://www.sommer-elso.com/preisliste.php 
            if self.type=="lorry, large size":
                Cashflow_opex.loc["transport price - lorry, large size", "EUR"]=1.71*self._total_distance()

            if self.type=="lorry, medium size":
                Cashflow_opex.loc["transport price - lorry, medium size", "EUR"]=1.10*self._total_distance()

            if self.type=="tractor, small size":
                Cashflow_opex.loc["transport price - tractor, small size", "EUR"]=0.95*self._total_distance()
        
        elif self.transport_cost=="self transportation":

            if self.type=="lorry, large size":
                Cashflow_opex.loc["transport price - lorry, large size", "EUR"]=0

            if self.type=="lorry, medium size":
                Cashflow_opex.loc["transport price - lorry, medium size", "EUR"]=0

            if self.type=="tractor, small size":
                Cashflow_opex.loc["transport price - tractor, small size", "EUR"]=0

        else: 
        #NOTE: The prices for transportation from webscrapping are for internacional destinations (e.g. Germany - Ukraine), therefore is set as an alternative prices
            if self.type=="lorry, large size":
                Cashflow_opex.loc["transport price - lorry, large size", "EUR"]=InputPrice().transportation().loc["transport price >20 ton", "Q"]*self._total_distance()

            if self.type=="lorry, medium size":
                Cashflow_opex.loc["transport price - lorry, medium size", "EUR"]=InputPrice().transportation().loc["transport price 10-20 ton", "Q"]*self._total_distance()

            if self.type=="tractor, small size":
                Cashflow_opex.loc["transport price - tractor, small size", "EUR"]=InputPrice().transportation().loc["transport price <10 ton", "Q"]*self._total_distance()

        Cashflow_opex["period_years"]=1
        Cashflow_opex["section"]="opex"

        return pd.concat([Cashflow_capex, Cashflow_opex])

class LandTransportMultiple(Process):
    """
    |  Transport module based on LandTransport, but oriented in the calculation pf multiple routes of different distances and freight loads

    :type I: List of Product
    :param I: The substrate to be transported
    :type mass allocation: list of floats or integers
    :param mass allocation: ratios (absolute) of mass allocated in different routes
    :type dist_km: list of floats or integers
    :param dist_km: Distance in kilometers for which the substrate is transported
    :type type: list of strings
    :param type: Transportation type. ``lorry, large size``, ``lorry, medium size`` or ``tractor, small size``
    :type same_transp: boolean
    :param same_transp: 'True' if same transportation  

    """
    def __init__(self, I=prod("tree leaves", 100, "ton"), O=None, mass_allocation=[], dist_km=[], type_transp=None, method="LBE", 
                 load_basis="mass", CO2_EF="EPA", transport_cost="sommer_elso",id=None):
        self.I=I
        self.O=O
        self.mass_allocation=mass_allocation
        self.dist_km=dist_km
        self.type_transp=type_transp
        self.method=method
        self.load_basis=load_basis
        self.CO2_EF=CO2_EF
        self.transport_cost=transport_cost
        self.id=id

    def route_plan(self):
        route_df=pd.DataFrame(index=[], columns=[])
        for i in range(0,len(self.mass_allocation)):
            route_df.loc[i,"Q"]=self.I["Q"].item() * self.mass_allocation[i]
            route_df.loc[i, "dist_km"]=self.dist_km[i]

            if isinstance(self.type_transp, list):
                route_df.loc[i, "type"]=self.type_transp[i]

            if isinstance(self.type_transp, str):
                route_df["type"]=self.type_transp

        return route_df

    def input(self):
        if self.O is None:
            return self.I

        # if O is specified the default of I is just neglected
        elif (self.O is not None) and self.I.equals(prod(("testsys","testitem"), 10, "ton")):
            return self.O

        # if I and O share the same item
        # This is for the case that the config defines where the I should come from, but O describes the quantities
        elif self.O.index.equals(self.I.index):
            O = self.O.copy()
            O.index = self.I.index
            return O

        else:
            raise Exception("Case not defined")

    def emissions(self):
        route_df=self.route_plan()
        emissions=[]
        for i in list(route_df.index):
            allocated_mass=self.I.copy()
            allocated_mass["Q"]=allocated_mass["Q"]* self.mass_allocation[i]
            emissions.append(LandTransport(allocated_mass, dist_km=self.dist_km[i], type=route_df.loc[i, "type"], method=self.method, 
                                        transport_cost=self.transport_cost, load_basis=self.load_basis, CO2_EF=self.CO2_EF).emissions())
        concat_emissions=pd.concat(emissions)

        total_emissions=pd.DataFrame(index=list(set(concat_emissions.index)), columns=concat_emissions.columns)
        for item in total_emissions.index:
            for gas in list(total_emissions.columns):
                total_emissions.loc[item, gas]=concat_emissions.loc[item][gas].sum()

        return total_emissions

    def output(self):

        return  self.input().copy()

    def cashflow(self):
        route_df=self.route_plan()
        cashflows=[]
        for i in list(route_df.index):
            allocated_mass=self.I.copy()
            allocated_mass["Q"]=allocated_mass["Q"] * self.mass_allocation[i]
            cashflows.append(LandTransport(allocated_mass, dist_km=self.dist_km[i], type=route_df.loc[i, "type"], method=self.method, 
                                        transport_cost=self.transport_cost, load_basis=self.load_basis, CO2_EF=self.CO2_EF).cashflow())
        concat_cashflows=pd.concat(cashflows)

        total_cashflow=pd.DataFrame(index=[], columns=[])
        total_cashflow.loc["transport price - multiple routes", "EUR"]=concat_cashflows["EUR"].sum()
        total_cashflow["section"]="opex"
        total_cashflow["period_years"]=1
        
        return total_cashflow
