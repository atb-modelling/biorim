
import pandas as pd
import folium
import leafmap
import branca
import ee
import geemap
import json
import geopandas as geopd
import plotly.express as px
import plotly.graph_objects as go
import matplotlib.pyplot as plt
import geodatasets
import contextily as cx

from branca.element import Template, MacroElement
from folium import plugins, features
from html2image import Html2Image
from PIL import Image
from selenium import webdriver
from IPython.display import display, HTML

#TODO: This module is working at the moment with geojson data, but need to be developed in terms of legend display, which has to be done manually
class MapCreation():
    """     
    | Creates a map (html format) based on geographical information 

    :type method: str
    :param method: mapping package to be used during the mapping
    :type data_format: str
    :param data_format: type of data used for the map production
    :type center_location: list
    :param center_location: latitude and longitude for the center of the map
    :type zoom: int
    :param zoom: map zoom value (for a state of Germany 9 is recomended, a lower numer means a greater area of covarage)
    :type layers: dict
    :param layers: dictionary of input files and the respective names to be displayed. The input files MUST have the ending of the format (i.e. file.geojson)
    :type input_path: str
    :param input_path: directory in which the input files are located
    :type color_preference: list
    :param color_preference: color preference for the geographical information (at the moment not supporting htmml colors)
    :type icon_preference: list
    :param icon_preference: icon preference for the geographical information, based on 'glyphicon' and 'fa' icons
    :type output_path: str
    :param output_path: output for the resulting map
    :type output_name: str
    :param output_name: file name for the resulting map, IMPORTANT: must contain the file format at the end of the name (i.e. output.html)
    
    """

    def __init__(self, method="folium", data_format="geojson", center_location=[52.5035979, 13.4447221], zoom=9, layers={}, input_path="",
                 color_preference=[], icon_preference=[], output_path=None, output_name="output.html"):
        self.method=method
        self.data_format=data_format
        self.center_location=center_location
        self.zoom=zoom
        self.layers=layers
        self.input_path=input_path
        self.color_preference=color_preference
        self.icon_preference=icon_preference
        self.output_path=output_path
        self.output_name=output_name

    def map_creation(self):

        if self.method=="folium":
            if self.data_format=="geojson":
                map=folium.Map(location=self.center_location, zoom_start=self.zoom)
                display_info=list(zip(self.layers.keys(), self.layers.values(), self.color_preference, self.icon_preference))
                for config in display_info:
                    folium.GeoJson( f"{input_path}"+f"\{config[0]}",
                                    marker=folium.Marker(icon=folium.Icon(icon=config[3][0], prefix=config[3][1], color=config[2])),
                                    name=config[1]).add_to(map)

                    # data_geojson.add_to(map)

                #add layer control
                folium.LayerControl(collapsed=False).add_to(map)

                #add a minimap
                minimap=plugins.MiniMap(toggle_display=True)
                map.add_child(minimap)

                if self.output_path==None:
                    self.output_path=self.input_path
                
                map.save(f"{self.output_path}"+f"\{self.output_name}")


#Test begins
berlin_layers={"Zwischenanlager.geojson": "Temporal storage", 
                "Kompost_Anlage_BSR_declaration.geojson": "Composting facilities", 
                "Biogas_plant.geojson": "Biogas plant", 
                "Moabit_CHP.geojson": "Cofiring plant"}

input_path="C:\\Users\\AVargas\\Documents\\PhD\\Research_papers\\2.Paper_costs_treeleaves\\Economic\\Mapping\\Zwischenanlager__Kompostalange"
output_path="C:\\Users\\AVargas\\Documents\\PhD\\Research_papers\\2.Paper_costs_treeleaves\\Economic\\Mapping"
color_preference=["blue", "green", "red", "orange"]
icon_preference=[("glyphicon-transfer", "glyphicon"), 
                 ("fa-recycle", "fa"),
                 ("circle", "fa"),
                 ("fa-industry", "fa")]
output_name="output.html"


MapCreation(layers=berlin_layers,input_path=input_path,output_path=output_path,
            color_preference=color_preference, icon_preference=icon_preference).map_creation()
#Test ends















#-->BEGIN OF MANUAL MAPPING

map=folium.Map(location=[52.5035979, 13.4447221], zoom_start=9)

data_geojson=folium.GeoJson(r"C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\Mapping\Zwischenanlager__Kompostalange\Zwischenanlager.geojson", 
               marker=folium.Marker(icon=folium.Icon(icon="glyphicon-transfer", prefix="glyphicon", color="blue")),
               popup=["Name"],
               name="Temporal storage")
# folium.GeoJsonPopup(fields=["Name"], labels=True).add_to(data_geojson)
data_geojson.add_to(map)

folium.GeoJson(r"C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\Mapping\Zwischenanlager__Kompostalange\Kompost_Anlage_BSR_declaration.geojson", 
               marker=folium.Marker(icon=folium.Icon(icon="fa-recycle", prefix="fa", color="green")),
               name="Composting falicities").add_to(map)

folium.GeoJson(r"C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\Mapping\Zwischenanlager__Kompostalange\Biogas_plant.geojson", 
               marker=folium.Marker(icon=folium.Icon(icon="circle", prefix="fa", color="red")),
               name="Biogas plant").add_to(map)

folium.GeoJson(r"C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\Mapping\Zwischenanlager__Kompostalange\Moabit_CHP.geojson", 
               marker=folium.Marker(icon=folium.Icon(icon="fa-industry", prefix="fa", color="orange")),
               name="Cofiring plant").add_to(map)

# feature_temporal=folium.FeatureGroup()
# for i in info_temporal.index:
#     coords=(info_temporal.loc[i, "Y"], info_temporal.loc[i, "X"])
#     feature_temporal.add_child(folium.Marker(coords, icon=folium.Icon(icon="glyphicon-transfer", prefix="glyphicon", color="blue")).add_to(map))

# # map.add_child(feature_temporal)

# for i in info_compost.index:
#     coords= (info_compost.loc[i, "Y"], info_compost.loc[i, "X"])
#     folium.Marker(coords, icon=folium.Icon(icon="fa-recycle", prefix="fa", color="green")).add_to(map)

# for i in info_biogas_plant.index:
#     coords= (info_biogas_plant.loc[i, "Y"], info_biogas_plant.loc[i, "X"])
#     folium.Marker(coords, icon=folium.Icon(icon="circle", prefix="fa", color="red")).add_to(map)

# for i in info_cofiring.index:
#     coords= (info_cofiring.loc[i, "Y"], info_cofiring.loc[i, "X"])
#     folium.Marker(coords, icon=folium.Icon(icon="fa-industry", prefix="fa", color="orange")).add_to(map)

folium.LayerControl(collapsed=False).add_to(map)

#add a minimap

minimap=plugins.MiniMap(toggle_display=True)
map.add_child(minimap)

# # add legend
# map=add_categorical_legend(map, "Legend", 
#                            colors=["blue", "green", "red", "orange"], 
#                            labels=["Temporal storage", "Composting facilities", "Biogas plant", "Cofiring plant"], 
#                            )

#add legend with branca

# {% macro html(this, kwargs) %}
# {% endmacro %}

berlin_layers={"Zwischenanlager.geojson": "Temporal storage", 
                "Kompost_Anlage_BSR_declaration.geojson": "Composting facilities", 
                "Biogas_plant.geojson": "Biogas plant", 
                "Moabit_CHP.geojson": "Cofiring plant"}

color_preference=["blue", "green", "red", "orange"]

label_color=list(zip(berlin_layers.values(), color_preference))

# legend_html_2 = '''
# {% macro html(this, kwargs) %}
# <div style="
#     position: fixed; 
#     bottom: 500px;
#     left: 1700px;
#     width: 250px;
#     height: 80px;
#     z-index:9999;
#     font-size:14px;
#     ">
#     {% for config in label_color %}
#         <p><i class="fa fa-map-marker" style="font-size:20px;color:{{config[1]}};"></i>&emsp;{{config[0]}}</p>
#     {% endfor %}
# </div>
# <div style="
#     position: fixed; 
#     bottom: 450px;
#     left: 1670px;
#     width: 250px;
#     height: 140px; 
#     z-index:9998;
#     font-size:14px;
#     background-color: #ffffff;

#     opacity: 0.7;
#     ">
# </div>
# {% endmacro %}
# '''

legend_html = '''
{% macro html(this, kwargs) %}
<div style="
    position: fixed; 
    bottom: 500px;
    left: 1700px;
    width: 250px;
    height: 80px;
    z-index:9999;
    font-size:14px;
    ">
    <p><i class="fa fa-map-marker" style="font-size:20px;color:#3498DB;"></i>&emsp;Temporal storage</p>
    <p><i class="fa fa-map-marker" style="font-size:20px;color:#28B463;"></i>&emsp;Composting facilities</p>
    <p><i class="fa fa-map-marker" style="font-size:20px;color:#E74C3C;"></i>&emsp;Biogas plant</p>
    <p><i class="fa fa-map-marker" style="font-size:20px;color:#F39C12;"></i>&emsp;Cofiring plant</p>
</div>
<div style="
    position: fixed; 
    bottom: 450px;
    left: 1670px;
    width: 250px;
    height: 140px; 
    z-index:9998;
    font-size:14px;
    background-color: #ffffff;

    opacity: 0.7;
    ">
</div>
{% endmacro %}
'''

legend = MacroElement()
legend._template = Template(legend_html)

map.get_root().add_child(legend)



#saving map as html
map.save(r'C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\Mapping\map_facilities.html')
map

#-->END OF MANUAL MAPPING



#OTHER TEMPLATE FOR GENERATING A DRAGGABLE LEGEND IN FOLIUM 
template = """
{% macro html(this, kwargs) %}

<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>jQuery UI Draggable - Default functionality</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
  <script>
  $( function() {
    $( "#maplegend" ).draggable({
                    start: function (event, ui) {
                        $(this).css({
                            right: "auto",
                            top: "auto",
                            bottom: "auto"
                        });
                    }
                });
});

  </script>
</head>
<body>

 
<div id='maplegend' class='maplegend' 
    style='position: absolute; z-index:9999; border:2px solid grey; background-color:rgba(255, 255, 255, 0.8);
     border-radius:6px; padding: 10px; font-size:14px; right: 20px; bottom: 20px;'>
     
<div class='legend-title'>Legend (draggable!)</div>
<div class='legend-scale'>
  <ul class='legend-labels'>
    <li><span style='background:white;opacity:0.7;'></span>States that have Null values.</li>
    

  </ul>
</div>
</div>
 
</body>
</html>

<style type='text/css'>
  .maplegend .legend-title {
    text-align: left;
    margin-bottom: 5px;
    font-weight: bold;
    font-size: 90%;
    }
  .maplegend .legend-scale ul {
    margin: 0;
    margin-bottom: 5px;
    padding: 0;
    float: left;
    list-style: none;
    }
  .maplegend .legend-scale ul li {
    font-size: 80%;
    list-style: none;
    margin-left: 0;
    line-height: 18px;
    margin-bottom: 2px;
    }
  .maplegend ul.legend-labels li span {
    display: block;
    float: left;
    height: 16px;
    width: 30px;
    margin-right: 5px;
    margin-left: 0;
    border: 1px solid #999;
    }
  .maplegend .legend-source {
    font-size: 80%;
    color: #777;
    clear: both;
    }
  .maplegend a {
    color: #777;
    }
</style>
{% endmacro %}"""

macro = MacroElement()
macro._template = Template(template)

map.get_root().add_child(macro)
#END OF TEMPLATE

##NOTE: using leafmap

m=leafmap.Map(center=(52.5035979, 13.44472219), zoom=9)
m.add_geojson(r"C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\Mapping\Zwischenanlager__Kompostalange\Zwischenanlager.geojson", 
               layer_name="Temporal storage")
m.add_geojson(r"C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\Mapping\Zwischenanlager__Kompostalange\Kompost_Anlage_BSR_declaration.geojson", 
               layer_name="Composting facilities", fill_colors="green")
m.add_legend(title="Legend", labels=["Temporal storage", "Composting facilities"], colors=["#8DD3C7", "#FFFFB3"])
m.save(r'C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\Mapping\map_leafmap.html')










# imgkit.from_file(r'C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\Mapping\map_facilities.html', 
#                  r'C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\Mapping\map_facilities.png')

#NOTE: for exporting into a image
# hti=Html2Image(output_path=r'C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\Mapping')

# hti.screenshot(url=r'C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\Mapping\map_facilities.html', 
#                 save_as="map_facilities.png")

#Info: https://github.com/YuChunTsao/csv2geojson 
#https://www.convertcsv.com/csv-to-geojson.htm
#https://pypi.org/project/csv2geojson/ 



#NOTE: using plotly
info_temporal=pd.read_csv(r"C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\Mapping\Zwischenanlager__Kompostalange\Zwischenanlager.csv")
info_temporal

fig=px.Scattermapbox(    data=info_temporal, 
                   lat="Y", 
                   lon="X", 
                   hover_name=info_temporal["Name"], 
                   marker={"size":20, "symbol":["bus"]}
                   )
fig.update_layout(mapbox_style="open-street-map", showlegend=True)
# go.Scattermapbox(marker={"size":20, "symbol":"diamond"})
#fig.update_traces(marker={"size":20, "symbol":["diamond"]})

fig.show()


px.set



#NOTE: using geopandas
info_temporal_geopd=geopd.read_file(r"C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\Mapping\Zwischenanlager__Kompostalange\Zwischenanlager.geojson")
info_temporal_geopd["Obs"]="Temporal"
info_temporal_geopd

ax=info_temporal_geopd.plot(figsize=(10,12), legend=True, column="Obs") 
#cx.add_basemap(ax, crs=info_temporal_geopd.crs)
plt.show()





#NOTE: csv to json
# csv2geojson(r"C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\Mapping\Zwischenanlager__Kompostalange\Zwischenanlager.csv", 
#             r"C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\Mapping\Zwischenanlager__Kompostalange\Zwischenanlager.geojson")

# info_temporal=pd.read_csv(r"C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\Mapping\Zwischenanlager__Kompostalange\Zwischenanlager.csv")
# info_temporal

# info_compost=pd.read_csv(r"C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\Mapping\Zwischenanlager__Kompostalange\Kompost_Anlage_BSR_declaration.csv")
# info_compost

# info_biogas_plant=pd.read_csv(r"C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\Mapping\Zwischenanlager__Kompostalange\Biogas_plant.csv")

# info_cofiring=pd.read_csv(r"C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\Mapping\Zwischenanlager__Kompostalange\Moabit_CHP.csv")

#NOTE: visualize json file as pandas
# with open(r"C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\Mapping\Zwischenanlager__Kompostalange\Zwischenanlager.geojson") as f:
#     data=json.load(f)
# data

# df_geopd=geopd.read_file(r"C:\Users\AVargas\Documents\PhD\Research_papers\2.Paper_costs_treeleaves\Economic\Mapping\Zwischenanlager__Kompostalange\Zwischenanlager.geojson")


#NOTE: catergorial legend template

# def add_categorical_legend(folium_map, title, colors, labels):
#     if len(colors) != len(labels):
#         raise ValueError("colors and labels must have the same length.")

#     color_by_label = dict(zip(labels, colors))
    
#     legend_categories = ""     
#     for label, color in color_by_label.items():
#         legend_categories += f"<li><span style='background:{color}'></span>{label}</li>"
        
#     legend_html = f"""
#     <div id='maplegend' class='maplegend'>
#       <div class='legend-title'>{title}</div>
#       <div class='legend-scale'>
#         <ul class='legend-labels'>
#         {legend_categories}
#         </ul>
#       </div>
#     </div>
#     """
#     script = f"""
#         <script type="text/javascript">
#         var oneTimeExecution = (function() {{
#                     var executed = false;
#                     return function() {{
#                         if (!executed) {{
#                              var checkExist = setInterval(function() {{
#                                        if ((document.getElementsByClassName('leaflet-top leaflet-right').length) || (!executed)) {{
#                                           document.getElementsByClassName('leaflet-top leaflet-right')[0].style.display = "flex"
#                                           document.getElementsByClassName('leaflet-top leaflet-right')[0].style.flexDirection = "column"
#                                           document.getElementsByClassName('leaflet-top leaflet-right')[0].innerHTML += `{legend_html}`;
#                                           clearInterval(checkExist);
#                                           executed = true;
#                                        }}
#                                     }}, 100);
#                         }}
#                     }};
#                 }})();
#         oneTimeExecution()
#         </script>
#       """
   

#     css = """

#     <style type='text/css'>
#       .maplegend {
#         z-index:9999;
#         float:right;
#         background-color: rgba(255, 255, 255, 1);
#         border-radius: 5px;
#         border: 2px solid #bbb;
#         padding: 10px;
#         font-size:12px;
#         positon: relative;
#       }
#       .maplegend .legend-title {
#         text-align: left;
#         margin-bottom: 5px;
#         font-weight: bold;
#         font-size: 90%;
#         }
#       .maplegend .legend-scale ul {
#         margin: 0;
#         margin-bottom: 5px;
#         padding: 0;
#         float: left;
#         list-style: none;
#         }
#       .maplegend .legend-scale ul li {
#         font-size: 80%;
#         list-style: none;
#         margin-left: 0;
#         line-height: 18px;
#         margin-bottom: 2px;
#         }
#       .maplegend ul.legend-labels li span {
#         display: block;
#         float: left;
#         height: 16px;
#         width: 30px;
#         margin-right: 5px;
#         margin-left: 0;
#         border: 0px solid #ccc;
#         }
#       .maplegend .legend-source {
#         font-size: 80%;
#         color: #777;
#         clear: both;
#         }
#       .maplegend a {
#         color: #777;
#         }
#     </style>
#     """

#     folium_map.get_root().header.add_child(folium.Element(script + css))

#     return folium_map