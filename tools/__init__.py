__all__ = ["add_system", "convert_unit", "convert_molmasses", "convert_to_gwp", "get_content", "prod", "relpath", "remove_system"]

## function that sets the relative data path
def relpath(x):
    return os.path.abspath(os.path.join(os.path.dirname( __file__ ),"..", os.path.relpath(x)))

import os
import importlib
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
import warnings
import copy
import inspect
import random

from math import isnan

from parameters import ConversionFactor
from tools import relpath
from read import IPCC, Munoz_Schmidt_2016, Herrmann
from declarations import Product, Emissions
from modelconfig import report_internal

from simulation.data_management_and_plotting.scenarios_plotting import LegendWithoutDuplicate

## the look-up table for defining the system where commodities come from:
def input_system(x):
    """     
    Returns the default production system for items. Not used for future model development, since systems are now specified together with the items
    
    :type x: string
    :param x: 'wheat' OR 'broiler' OR ...
    :return: class, including the module
    
   |  
    """        

    # it input system is already specified then simply return this, otherwise read from file

    if isinstance(x, tuple):
        # in the case of external this needs to be slightly different, because the Ecoinvent activities are no existing modules
        if "external.ExternInput" in x[0]:
            System = "external.ExternInput"
        else:
            System = x[0]
    else:
        try:
            System = pd.read_csv(relpath("analysis\\input_system.csv"), sep=";", encoding="latin-1", index_col=0).loc[x,"system"]
        except:
            System = "undefined"

    return System


#### Convert functions ####


def remove_source(O):
    """
    |  Removes the "system" index column from Products if it does exist

    :type O: Product
    :param O: Product for which to remove the system information
    
    |  
    """
    if not (isinstance(O, Product) or isinstance(O, Emissions)):
        warnings.warn("O should by of type Product")
    if isinstance(O.index, pd.MultiIndex):
        if "source" in O.index.names:
            O = O.droplevel("source")
    return O


def remove_system(O):
    """
    |  Removes the "system" index column from Products if it does exist

    :type O: Product
    :param O: Product for which to remove the system information
    
    |  
    """
    if not (isinstance(O, Product) or isinstance(O, Emissions)):
        warnings.warn("O should by of type Product")
    if isinstance(O.index, pd.MultiIndex):
        if "system" in O.index.names:
            O = O.droplevel("system")
    return O


def add_system(O, module = None, class_name = None, id = None, system = None):
    """
    Adds the system information to products. Usually it is used at the end of the output method.

    :type O: Product
    :param O: Product to which to add the system information
    :type module: string
    :param module: the name of the module
    :type class_name: string
    :param class_name: the name of the system
    :type id: numeric
    :param id: the id of the class used for creating the instance
    :type system: string
    :param system: alternative to define module, class_name and id, directly name system
    :return: Product with the system (e.g. production.animal.poultry.BroilerFarm_1) added


    |  The usual way to use this is:
    |  return add_system(Output, module = self.__module__, class_name = self.__class__.__name__, id = self.id)
    |  
    """

    stack = inspect.stack()
    O = O.copy()

    # try to automatically load the class_name and the module from the history of the calling function
    if class_name is None and system is None:
        try:
            class_name = stack[1][0].f_locals["self"].__class__.__name__
        except:
            print("Define class_name")

    if module is None and system is None:
        try:
            module = stack[1][0].f_locals["self"].__class__.__module__
        except:
            print("Define module")

    if id is None and system is None:
        system = (module + "." + class_name)
    elif system is None:
        system = (module + "." + class_name + "_" + str(id))
    
    if isinstance(O, pd.DataFrame):
        O = Product(O)
    elif not isinstance(O, Product):
        TypeError("O should by of type Product")
    
    if isinstance(O.index, pd.MultiIndex) and ("system" in O.index.names):
        new_index = pd.MultiIndex.from_tuples([(system, i[1]) for i in O.index], names=["source", "item"])
        O.set_index(new_index, inplace=True)
        return O
    else:
        new_index = pd.MultiIndex.from_tuples([(system, i) for i in O.index], names=["source", "item"])
        O.set_index(new_index, inplace=True)
        return O


def source_as_index(I, source=None):
    """
    |  Adds the source system as multiindex.
    |  Source is either taken from a column named "source" or from what is passed as argument.
    |  **Examples**:
    |  source_as_index(I = prod(["maize","digestate"], [100, 200], ["ton", "ton"], source=["production.plant.crop.ArableLand_1", "material.biogas.BiogasPlant_1"]))
    """

    if I.index.nlevels > 1:
        warnings.warn("Index is already multilevel")

    Out = I.copy()

    if isinstance(source, str):
        Out.index = pd.MultiIndex.from_tuples([(source, idx) for idx in I.index], names=['source', 'item'])

    # source defined by column
    elif "source" in I.columns:
        idx = []
        for item, row in I.iterrows():
            idx.append((row["source"], item))

        Out.index = pd.MultiIndex.from_tuples(idx, names=['source', 'item'])
        Out.drop(columns="source", inplace=True)

    else:
        raise ValueError("source neither defined by parameter nor by column")

    return Out



def sourceindex_as_column(I):
    """
    reverse of the above.
    """
    Out = I.copy()
    Out["source"] = Out.index.get_level_values("source")
    return Out.reset_index("source", drop=True)



### Simplified Product creator
def prod(item = None, Q = np.float64(np.nan), U = "ton", **kwargs):
    """
    Creates objects of type Product from strings, list of strings or tuples

    :type item: string, tuple or list of strings or tuples
    :param item: the items to convert to a Product
    :type Q: float or list of floats
    :param Q: quantities
    :type U: string
    :param U: units
    :return: Product

    |  **Examples:**
    |  prod(item="broiler", Q=1, U="kg")
    |  prod(["maize","winter wheat"], Q=[1,2], U=["kg","kg"])
    |  prod(["maize","winter wheat"], [1,2], ["kg","kg"], N=[10,20])

    |  prod(item=("production.plant.crop.ArableLand","winter wheat"), Q=1, U="kg")
    |  prod(item=[("crop1","maize"),("crop2","wheat")], Q=[1,2], U=["kg","ton"], N=[2,5])
    |  
    """

    returndict = {**{'Q': Q}, **{'U': U}, **kwargs}

    if isinstance(item, str):
        #system = "." # dot used if system is not specified
        # return Product(returndict, index=pd.MultiIndex.from_tuples([(system, item)], names=['system','item']))

        # NOTE: Change to the definition. System not defined in any case.
        return Product(returndict, index=pd.Index([item], name="item"))

    if (isinstance(item, list) and all(isinstance(i, str) for i in item)):
        return Product(returndict, index=pd.Index(item, name="item"))

    # NOTE: For the transitional period transform tuples to source 
    elif isinstance(item, tuple):
        if 'source' not in returndict:
            returndict = {**{'Q': Q}, **{'U': U}, **kwargs, **{'source': item[0]}}
            return Product(returndict, index=pd.Index([item[1]], name='item'))
        else:
            raise ValueError("source column and definition of source system as tuple not allowed")

    elif (isinstance(item, list) and all(isinstance(i, tuple) for i in item)):
        if 'source' not in returndict:
            returndict = {**{'Q': Q}, **{'U': U}, **kwargs, **{'source': [i[0] for i in item]}}
            return Product(returndict, index=pd.Index([i[1] for i in item], name='item'))
        else:
            raise ValueError("source column and definition of source system as tuple not allowed")


def valid_item(I, valid_items):
    if not isinstance(valid_items, list):
        valid_items = [valid_items]
    if not set(I.index.get_level_values("item")).issubset(set(valid_items)):
        not_valid = list(set(I.index.get_level_values("item")).difference(set(valid_items)))
        print(f"{not_valid} are no valid items. Only {valid_items} allowed.")
        return False
    else:
        return True



def get_content(I, required):
    """
    This function helps to assure that all properties of inputs that are required are specified. It is usually used in the __init__ of production classes.
    If this is not the case this function trys to run the default production system (as specified in tools/input_system.csv) to get these properties

    |  *Examples*
    |  self.I = get_content(I, required=["N", "NH4-N", "DM", "oDM", "orgC", "B0", "density"])
    |
    """

    if not isinstance(I, Product):
        raise ValueError("Input for get_content needs to be of type Product")

    # when all columns are already there do nothing
    if all(elem in I.columns.to_list() for elem in required):
        return I

    else:
        print("Using the get_content function, to get the contents of:")

        if any(I["Q"].isna()):
            report("replacing NA values with 1")
            I["Q"].fillna(1, inplace=True)

        # function to execute the output of the source system
        def output_from_prodclass(index, row, file):
            # if isinstance(index, tuple):
            # Redefined. Source system not defined differently
            if "source" in row:
                if isinstance(row["source"], str):
                    system = row["source"]
                else:
                    raise ValueError("source not defined correctly")
                    # system = file.loc[index, "system"]
                row = row.drop("source")
            elif index in file.index:
                system = file.loc[index, "system"]
            else:
                raise ValueError(f"source for {index} not defined in input_system.csv")
            
            Module = system.rsplit(".", 1)[0]
            Class = system.rsplit(".", 1)[1]
            print(index, "from", system)
            return getattr(importlib.import_module(Module), Class)(O=Product(row.to_dict(), index=pd.Index([index], name="item"))).output().loc[[index],:]

        file = pd.read_csv(relpath("tools\\input_system.csv"), sep=";", encoding="latin-1", index_col=0)
        return pd.concat([output_from_prodclass(index, row , file) for index, row in I.iterrows()], sort=False)


## convert units of inputs
def convert_unit(x, to="kg"):
    if all(pd.unique(x["U"]) == to):
        return x
    elif (to == "kg") and any(pd.unique(x["U"]) == "m³"):
        x.loc[x.U == "ton", "Q"] = x.loc[x.U == "ton", "Q"]*1000
        x.loc[x.U == "ton", "U"] = "kg"
        x.loc[x.U == "m³", "Q"] = x.loc[x.U == "m³", "Q"]*x.loc[x.U == "m³", "density"]*1000
        x.loc[x.U == "m³", "U"] = "kg"
    elif (to == "ton") and any(pd.unique(x["U"]) == "m³"):
        x.loc[x.U == "kg", "Q"] = x.loc[x.U == "kg", "Q"]/1000
        x.loc[x.U == "kg", "U"] = "ton"
        x.loc[x.U == "m³", "Q"] = x.loc[x.U == "m³", "Q"]*x.loc[x.U == "m³", "density"]
        x.loc[x.U == "m³", "U"] = "ton"
    elif (to == "kg"):
        x.loc[x.U == "ton", "Q"] = x.loc[x.U == "ton", "Q"]*1000
        x.loc[x.U == "ton", "U"] = "kg"
    elif (to == "ton"):
        x.loc[x.U == "kg", "Q"] = x.loc[x.U == "kg", "Q"]/1000
        x.loc[x.U == "kg", "U"] = "ton"
    if not all(pd.unique(x["U"]) == to):
        warnings.warn(("not all converted to " + to))
    return x


## like this it only returns the selected "species to be converted. Should also return the others unchanged

def convert_molmasses(x, to="element", species_to_be_converted=None, keep_unconverted=True):
    """
    Converts from element to molecular form or the other way round. E.g. from CO2-C to CO2
    """
    x = copy.deepcopy(x)
    Factor = ConversionFactor.copy()
    Factor.reset_index(inplace=True)
    Factor.set_index(keys=["convert_to", "from"], inplace=True)

    if isinstance(x, float):
        if species_to_be_converted is None:
            warnings.warn("species_to_be_converted needs to be defined")
        else:
            converted = x * Factor.loc[(to,species_to_be_converted),"factor"]

    elif(isinstance(x, pd.DataFrame)):
        Factor = Factor.loc[to,:]
        # if none are specified all that are in the conversion list are converted
        if species_to_be_converted is None:
            species_to_be_converted = list(set(x.columns).intersection(set(Factor.index)))

        Factor = Factor.loc[species_to_be_converted,:]

        converted = x.multiply(Factor["factor"], axis=1).loc[:,species_to_be_converted]
        converted.rename(columns=Factor.loc[species_to_be_converted, "to"], inplace=True)

        if keep_unconverted:
            unconverted = x.loc[:,x.columns.difference(species_to_be_converted)]
            converted = converted.join(unconverted)

    elif(isinstance(x, xr.DataArray)):
        ## the species_to_be_converted and keep_unconverted arguments are not implemented yet
        ## should implement a check whether the the xarray is not already in the correct dimension

        if to=="element":
            fr = "molecule"
            to = "element"
        elif to=="molecule":
            fr = "element"
            to = "molecule"

        Factor = Factor.loc[to,:]
        factors = xr.DataArray(Factor.loc[:,"factor"])
        factors = factors.rename({"from":fr})

        ## the calulation (this drops entries without conversion factors)
        converted = x * factors

        # change coordinates
        intersec = converted.coords[fr].to_index().intersection(Factor.index)
        lost_elements = x.coords[fr].to_index().difference(converted.coords[fr].to_index())
        converted = converted.assign_coords(element=Factor.loc[intersec,"to"].to_list())

        if keep_unconverted==True:
            unconverted = x.loc[{fr:lost_elements.to_list()}]
            converted = xr.concat([converted, unconverted], dim=fr)

        # then change the dimension name
        converted = converted.rename({fr: to})

    else:

        raise ValueError("Please provide a Pandas dataframe or list in combination with provided species_to_be_converted list.")

    return converted

# convert emissions containing nitrogen converted to indirect N2O-N
def indirect_N2ON_emissions(x, climate="wet climate"):
    x = x.copy()
    # alternative would be to use wet climate?
    EF_INDIRECT_N2O = IPCC.indirect_N2O_EF_soils().loc[("EF_4", climate),"value"]
    
    if isinstance(x, pd.DataFrame):
        col_select = x.columns.intersection(pd.Index(["NO2-N", "NO-N", "NOx-N", "NH3-N", "NH3-N & NOx-N"]))
        x["N2O-N indirect"] = (x[col_select]*EF_INDIRECT_N2O).sum(axis=1)

    elif isinstance(x, xr.DataArray):
        selector = list(set(x.coords["element"].values).intersection(["NO2-N", "NO-N", "NOx-N", "NH3-N", "NH3-N & NOx-N"])) 
        x_indirect = (x.sel(element=selector)*EF_INDIRECT_N2O).sum(dim="element")
        x_indirect = x_indirect.assign_coords(element="N2O-N indirect")
        x = xr.concat([x, x_indirect], dim="element")

    return x



def convert_to_gwp(x, timerange=100, indirect_N2O=True):
    """
    Converts emissions according to global warming potential into CO2 equivalents.

    :type x: pd.DataFrame or xr.DataArray
    :param x: Emissions table to be converted according to global warming potentials
    :type timerange: numeric
    :param timerange: 20 or 100 years global warming potentials
    :type indirect_N2O: boolean
    :param indirect_N2O: consider "N2O indirect" emissions
    """
    y = x.copy()
    GWP_FACTOR = IPCC.GWP_factors().astype("float64")
    GWP_FACTOR_NOx = IPCC.GWP_factors_NOx()

    # correct for the fact that CH4 is converted to CO2 in the atmoshpere after a while and this CO2 also contributes to global warming
    CH4_oxidation = Munoz_Schmidt_2016.CH4_oxidation()
    GWP_FACTOR.loc["CH4",:] += CH4_oxidation.loc["CH4",:]

    # NOx value for Europe:
    GWP_FACTOR.loc["NOx",:] = GWP_FACTOR_NOx.loc["NOx EU + North Africa"]

    if indirect_N2O:
        GWP_FACTOR.loc["N2O indirect",:] = GWP_FACTOR.loc["N2O",:]

    if isinstance(y, pd.DataFrame):

        if timerange in [20, 100]:
            GWP_FACTOR =  GWP_FACTOR.loc[:,"GWP" + str(timerange)]
        else:
            raise Exception("Choose between timeranges of 20 or 100 years for GWP!")

        ## identify common elements, and subset to these
        gwp_gases = list(set(GWP_FACTOR.index).intersection(y.columns))
        y = y.loc[:, gwp_gases]

        # TODO: CHECK whether this still makes sense. If so, then implement also for xarray
        for gas in gwp_gases:
            if gas == "NO":
                y.loc[:,"NOx"] = y.loc[:,["NOx","NO"]].sum(axis=1) * GWP_FACTOR["NOx"]
            else:
                y.loc[:,gas] *= GWP_FACTOR[gas]

    elif isinstance(y, xr.DataArray):
        GWP_FACTOR =  xr.DataArray(GWP_FACTOR)
        GWP_FACTOR = GWP_FACTOR.rename({'Species':"molecule", 'dim_1':"gwp"})

        if set(timerange) <= set([20,100]):
            GWP_FACTOR = GWP_FACTOR.sel(gwp=["GWP" + str(x) for x in timerange])
        else:
             raise Exception("Choose between timeranges of 20, 100 years or 'all' for GWP!")

        y = y*GWP_FACTOR

    else:
        raise KeyError("x  is not a pandas DataFrame. Please provide a 'pd.DataFrame'.")

    return y

### 

## a report method: print function internals if is activated
def report(*args):
    if report_internal:
        print(*args)
    else:
        pass

# scale_by_mass is a list of the contents which don't change but need to be adapted to new output mass (e.g. K and P)
# assumes that output is aggregated

def O_from_massloss(I, MASSLOSS_TON=0, H2O_LOSS_TON=0, DM_LOSS_TON=0, oDM_LOSS_TON=0, orgC_LOSS_TON=0, N_LOSS_TON=0, NH4N_LOSS_TON=0, scale_by_mass=None, index="test", dropna=True):
    
    if (MASSLOSS_TON != 0 and (H2O_LOSS_TON != 0 and DM_LOSS_TON != 0)):
        if (MASSLOSS_TON != (H2O_LOSS_TON + DM_LOSS_TON)):
            warnings.warn("MASSLOSS and both H2O_LOSS and DM_LOSS defined, but disagreeing")

    ## all inputs in absolute terms
    if all(I["U"] =="ton"):
        I_MASS_TON = I["Q"].sum()
        I_DM_TON = (I["Q"]*I["DM"]).sum() # input DM in ton
        I_oDM_TON = (I["Q"]*I["DM"]*I["oDM"]).sum()
        I_orgC_TON = (I["Q"]*I["DM"]*I["oDM"]*I["orgC"]).sum()
        I_N_KG = (I["Q"]*I["N"]).sum()
        if "NH4-N" in I.columns:
            I_NH4N_KG = (I["Q"]*I["NH4-N"]).sum()
        report(f"absolute inputs are:\n I_MASS_TON: {round(I_MASS_TON,3)} \n I_DM_TON: {round(I_DM_TON,3)} \n I_oDM_TON: {round(I_oDM_TON,3)} \n I_orgC_TON: {round(I_orgC_TON,3)} \n I_N_KG: {round(I_N_KG,3)}")
    else:
        raise ValueError("I has to be in 'ton'")

    if isinstance(index, str):
        index = pd.Index([index])

    O = pd.DataFrame(columns=I.columns, index=index)

    ## define DM loss as oDM loss, or sum of orgC and N loss, if not already defined
    if DM_LOSS_TON == 0 and oDM_LOSS_TON != 0:
        report("DM loss assumed as oDM loss")
        DM_LOSS_TON = oDM_LOSS_TON
    elif ((orgC_LOSS_TON != 0 or N_LOSS_TON != 0) and DM_LOSS_TON == 0):
        DM_LOSS_TON = orgC_LOSS_TON + N_LOSS_TON
        report("DM loss assumed as sum of orgC and N loss")
        if oDM_LOSS_TON == 0:
            oDM_LOSS_TON = orgC_LOSS_TON + N_LOSS_TON
            report("oDM loss assumed as sum of orgC and N loss")

    # define MASSLOSS as H2O_LOSS and DM_LOSS if not already defiend
    if MASSLOSS_TON == 0:
        MASSLOSS_TON = H2O_LOSS_TON + DM_LOSS_TON
        report("massloss assumed as sum of H2O and DM loss")

    report(f"absolute losses are:\n MASSLOSS_TON: {round(MASSLOSS_TON,3)} \n DM_LOSS_TON: {round(DM_LOSS_TON,3)} \n oDM_LOSS_TON: {round(oDM_LOSS_TON,3)} \n orgC_LOSS_TON: {round(orgC_LOSS_TON,3)} \n N_LOSS_TON: {round(N_LOSS_TON,4)} \n NH4N_LOSS_TON: {round(NH4N_LOSS_TON,4)}")

    ## some checks: are sum of individuals not more than 1% bigger than summarized categories (neglect rounding errors)?
    if ((DM_LOSS_TON + H2O_LOSS_TON)/MASSLOSS_TON > 1.01):
        warnings.warn(f"DM_LOSS_TON seems too high: DM + H2O loss: {DM_LOSS_TON + H2O_LOSS_TON}, MASS loss: {MASSLOSS_TON}")
    if (oDM_LOSS_TON/DM_LOSS_TON > 1.01):
        warnings.warn(f"oDM_LOSS_TON seems too high: oDM loss: {oDM_LOSS_TON}, DM loss: {DM_LOSS_TON}")
    if ((orgC_LOSS_TON + N_LOSS_TON)/oDM_LOSS_TON > 1.01):
        warnings.warn(f"sum of orgC_LOSS_TON and N_LOSS_TON seems too high: oDM loss:  {oDM_LOSS_TON}, orgC + N loss: {orgC_LOSS_TON + N_LOSS_TON}")

    ## total mass
    O_MASS_TON = I_MASS_TON - MASSLOSS_TON
    O["Q"] = O_MASS_TON

    # DM
    O_DM_TON = I_DM_TON - DM_LOSS_TON
    O["DM"] = O_DM_TON/O["Q"]

    # oDM
    O_oDM_TON = I_oDM_TON - oDM_LOSS_TON
    O["oDM"] = O_oDM_TON/O_DM_TON
    
    # orgC
    O_orgC_TON = I_orgC_TON - orgC_LOSS_TON
    O["orgC"] = O_orgC_TON/ O_oDM_TON

    # N
    O_N_KG = I_N_KG - N_LOSS_TON*1000
    O["N"] = O_N_KG/O["Q"]

    # NH4-N (only calculated if NH4N_LOSS specified)
    if NH4N_LOSS_TON != 0:
        O_NH4N_KG = I_NH4N_KG - NH4N_LOSS_TON*1000
        O["NH4-N"] = O_NH4N_KG/O["Q"]

        if any(O["NH4-N"] > O["N"]):
            warnings.warn(f"Initially calculated NH4-N content {O['NH4-N'][0]} higher than total N content {O['N'][0]}. Value of NH4-N content set to N content")
            O["NH4-N"] = O["N"]

    report(f"absolute outputs are: \n O_MASS_TON:{round(O_MASS_TON,3)} \n O_DM_TON: {round(O_DM_TON,3)} \n O_oDM_TON: {round(O_oDM_TON,3)} \n O_orgC_TON: {round(O_orgC_TON,3)} \n O_N_KG: {round(O_N_KG,4)}")

#    if any(O.loc[:,O.columns != "U"].lt(0)):
#        smaller = O.loc[:,O.columns != 'U'].lt(0)
#        warnings.warn(f"Calculated value smaller than 0! O: \n {O} \n {smaller}")

    # species that are just adjusted to new mass
    if scale_by_mass is not None:
        I_other_abs = I.loc[:,scale_by_mass].mul(I.loc[:,"Q"], axis=0).sum(axis=0)
        O_other_abs = I_other_abs/(O.loc[:,"Q"].sum())
        O_other_abs = O_other_abs.to_frame().transpose()
        O_other_abs.index = index
        O.update(O_other_abs)
    if dropna:
        O.dropna(axis=1, how="all", inplace=True)

    O["U"] = "ton"

    return O

def determine_c_n(I = prod(["cow manure, liquid", "tree leaves"], [1,1], ["ton", "ton"])):
    #NOTE: Showing error with get_content function
    """
    Tool for determining the final C/N ratio between two substrates
    Based on the publication by Cornell University (more: http://compost.css.cornell.edu/calc/cn_ratio.html)
    The formula: 
    (Q1*C1*(100-M1)+Q2*C2*(100-M2))/(Q1*N1*(100-M1)+Q2*N2*(100-M2))
    Where: 
    Q1 = Amount of substrate 1 in tons
    C1 = Amount of carbon (%) in total solid (TS) basis of substrate 1
    N1 = Amount of nitrogen (%) in total solid (TS) basis of substrate 1
    M1 = Moisture (%) of substrate 1
    Q2 = Amount of substrate 2 in tons
    C2 = Amount of carbon (%) in total solid (TS) basis of substrate 2
    N2 = Amount of nitrogen (%) in total solid (TS) basis of substrate 2
    M2 = Moisture (%) of substrate 2
    """

    I = get_content(I, required=["DM","oDM","orgC","N"]) 

    if len(I.index) > 2 or len(I.index) < 2:
        raise Exception("Input (I) with two entries required")
    
    Q1 = I["Q"][0]
    C1 = I["oDM"][0]*I["orgC"][0]*100
    N1 = I["N"][0]/I["DM"][0]/10
    M1 = 100-I["DM"][0]*100
    Q2 = I["Q"][1]
    C2 = I["oDM"][1]*I["orgC"][1]*100
    N2 = I["N"][1]/I["DM"][1]/10
    M2 = 100-I["DM"][1]*100

    CN_mixture=(Q1*C1*(100-M1)+Q2*C2*(100-M2))/(Q1*N1*(100-M1)+Q2*N2*(100-M2))
    return round (CN_mixture)

def determine_c_n_v2(I1 = prod("cow manure, liquid", 1, "ton"), I2 = prod("tree leaves", 1, "ton")):
    """
    Tool for determining the final C/N ratio between two substrates
    Based on the publication by Cornell University (more: http://compost.css.cornell.edu/calc/cn_ratio.html)
    The formula: 
    (Q1*C1*(100-M1)+Q2*C2*(100-M2))/(Q1*N1*(100-M1)+Q2*N2*(100-M2))
    Where: 
    Q1 = Amount of substrate 1 in tons
    C1 = Amount of carbon (%) in total solid (TS) basis of substrate 1
    N1 = Amount of nitrogen (%) in total solid (TS) basis of substrate 1
    M1 = Moisture (%) of substrate 1
    Q2 = Amount of substrate 2 in tons
    C2 = Amount of carbon (%) in total solid (TS) basis of substrate 2
    N2 = Amount of nitrogen (%) in total solid (TS) basis of substrate 2
    M2 = Moisture (%) of substrate 2
    """

    # I = get_content(I, required=["DM","oDM","orgC","N"]) 

    # if len(I.index) > 2 or len(I.index) < 2:
    #     raise Exception("Input (I) with two entries required")
    
    Q1 = I1["Q"][0]
    C1 = I1["oDM"][0]*I1["orgC"][0]*100
    N1 = I1["N"][0]/I1["DM"][0]/10
    M1 = 100-I1["DM"][0]*100
    Q2 = I2["Q"][0]
    C2 = I2["oDM"][0]*I2["orgC"][0]*100
    N2 = I2["N"][0]/I2["DM"][0]/10
    M2 = 100-I2["DM"][0]*100

    CN_mixture=(Q1*C1*(100-M1)+Q2*C2*(100-M2))/(Q1*N1*(100-M1)+Q2*N2*(100-M2))
    return round (CN_mixture)

def oDM_mix(I1 = prod("cow manure, liquid", 1, "ton"), I2 = prod("tree leaves", 1, "ton"), units="per fresh mass"):
    if units not in ["per fresh mass", "per oDM"]:
        raise ValueError("'Units' should be 'per fresh mass' or 'per oDM'")
    
    total_oDM=I1["Q"][0]*I1["DM"][0]*I1["oDM"][0]+I2["Q"][0]*I2["DM"][0]*I2["oDM"][0]
    
    if units=="per fresh mass":
        oDM_mix=total_oDM/(I1["Q"][0]+I2["Q"][0])
        print(f"The organic dry matter per tonne of fresh mass in the mix is {oDM_mix}")
        return oDM_mix
    
    elif units=="per oDM":
        oDM_mix=total_oDM/(I1["Q"][0]*I1["DM"][0]*I1["oDM"][0]+I2["Q"][0]*I2["DM"][0]*I2["oDM"][0])
        print(f"The organic dry matter per oDM mass in the mix is {oDM_mix}")
        return oDM_mix
    

def pie_chart(y=[], labels=[], title="", loc=9):

    fig, ax= plt.subplots()
    ax.pie(y, autopct='%1.1f%%', textprops={'fontsize': 14})
    #ax.set_title(title, fontsize=18)
    fig.legend(labels, loc=loc, fontsize=14)
    plt.title(title, pad=5, fontsize=18)
    #plt.title(title, bbox={"pad":5}, fontsize=18)
    plt.show()

def pie_chart_cashflow(y_capex=[], y_opex_pos=[], y_opex_neg=[], labels_capex=[], labels_opex_pos=[], labels_opex_neg=[], title="", loc=9):
    #NOTE: Check whether to keep this function
    colors = ["#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)]) for i in range(20)]
    colors

    #fig, (ax1, ax2, ax3, ax4)=plt.subplots(ncols=2, nrows=2)
    fig=plt.figure()
    ax1=fig.add_subplot(221)
    ax1.pie(y_capex, autopct='%1.1f%%', colors=colors, textprops={'fontsize': 14}, pctdistance=1.5)
    ax2=fig.add_subplot(222)
    ax2.pie(y_opex_pos, autopct='%1.1f%%', colors=colors, textprops={'fontsize': 14}, pctdistance=1.5)
    ax3=fig.add_subplot(223)
    ax3.pie(y_opex_neg, autopct='%1.1f%%', colors=colors, textprops={'fontsize': 14}, pctdistance=1.5)
    ax4=fig.add_subplot(224, frameon=False)
    ax4.set_xticks([])
    ax4.set_yticks([])
    total_labels=labels_capex + labels_opex_pos + labels_opex_neg
    #handles, labels= LegendWithoutDuplicate(ax2, fig, font=12, n_cols=1).for_list_axes([ax1,ax2,ax3], handles_and_labels=True)
    ax4.legend(labels=total_labels, loc=loc)
    plt.title(title, pad=5, fontsize=18)
    plt.tight_layout()
    plt.show()

def LHV_correction(I = prod("tree leaves", 1, "ton"), H=5, DM=0.9):
    
    #According to USEPA LHV = HHV - 10.55*(W + 9H), source: https://www3.epa.gov/airtoxics/utility/fnl_biomass_cogen_TSD_04_19_07.pdf
    I=I.copy()
    if "tree leaves" in str(I.index):
        Elemental_comp=Herrmann.Chemical_composition()[["DM", "C", "N", "H"]]
        Elemental_comp["O"]=100-Elemental_comp[["C", "N", "H"]].sum(axis=1)
        #According to Sheng & Azevedo (2005): HHV = -1.3675 + 0.3137C + 0.7009H + 0.0318O
        HHV=-1.3675 + 0.3137*Elemental_comp["C"] + 0.7009*Elemental_comp["H"]+ 0.0318*Elemental_comp["O"]
        if DM != None:
            I["DM"]=DM
        I["LHV"]=HHV["Mean"] - 10.55*((1-DM)*100 + 9*H)/100
    
    return I



