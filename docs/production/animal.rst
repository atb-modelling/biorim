Animal husbandry
================

main
----

.. automodule:: production.animal
    :members:
    :undoc-members:
    :show-inheritance:

poultry
-------

.. automodule:: production.animal.poultry
    :members:
    :undoc-members:
    :show-inheritance: