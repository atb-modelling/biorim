Modules
=======

The model is structured into several modules that collect either certain functionality (e.g. modules analysis and dev_utils)
or that represent different sectors of the bioeconomy (e.g. production, transport).



.. toctree::
    :maxdepth: 2

    analysis
    declarations
    dev_utils
    external
    material
    production
    read
    regions
    simulation
    tools
    transport