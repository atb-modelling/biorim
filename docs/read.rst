####
read
####

main
----

.. automodule:: read
    :members:
    :undoc-members:
    :show-inheritance:

Amon_2001
---------
.. automodule:: read.Amon_2001
    :members:
    :undoc-members:
    :show-inheritance:

Chen_2018
---------
.. automodule:: read.Chen_2018
    :members:
    :undoc-members:
    :show-inheritance:

Cobb
----

.. automodule:: read.Cobb
    :members:
    :undoc-members:
    :show-inheritance:

Cimo_2014
---------

.. automodule:: read.Cimo_2014
    :members:
    :undoc-members:
    :show-inheritance:

DLG
---

.. automodule:: read.DLG
    :members:
    :undoc-members:
    :show-inheritance:

EEA
---

.. automodule:: read.EEA
    :members:
    :undoc-members:
    :show-inheritance:

FAOSTAT
-------

.. automodule:: read.FAOSTAT
    :members:
    :undoc-members:
    :show-inheritance:

FernandezLopez_2015
-------------------

.. automodule:: read.FernandezLopez_2015
    :members:
    :undoc-members:
    :show-inheritance:

IPCC
----

.. automodule:: read.IPCC
    :members:
    :undoc-members:
    :show-inheritance:

KTBL
----

.. automodule:: read.KTBL
    :members:
    :undoc-members:
    :show-inheritance:

Libra_2014
----------

.. automodule:: read.Libra_2014
    :members:
    :undoc-members:
    :show-inheritance:

LWK\_NRW
--------

.. automodule:: read.LWK_NRW
    :members:
    :undoc-members:
    :show-inheritance:

LWK\_NS
-------

.. automodule:: read.LWK_NS
    :members:
    :undoc-members:
    :show-inheritance:

Landtag\_BB
-----------

.. automodule:: read.Landtag_BB
    :members:
    :undoc-members:
    :show-inheritance:

LfL\_BY
-------

.. automodule:: read.LfL_BY
    :members:
    :undoc-members:
    :show-inheritance:

Miah2016
--------

.. automodule:: read.Miah2016
    :members:
    :undoc-members:
    :show-inheritance:

Nicholson1996
-------------

.. automodule:: read.Nicholson1996
    :members:
    :undoc-members:
    :show-inheritance:

Peters_2015
-----------

.. automodule:: read.Peters_2015
    :members:
    :undoc-members:
    :show-inheritance:

Prices
-----------

.. automodule:: read.Prices
    :members:
    :undoc-members:
    :show-inheritance:

Ro_2010
-------

.. automodule:: read.Ro_2010
    :members:
    :undoc-members:
    :show-inheritance:

Rodhe_Karlsson_2002
-------------------

.. automodule:: read.Rodhe_Karlsson_2002
    :members:
    :undoc-members:
    :show-inheritance:

Song_Guo_2011
-------------

.. automodule:: read.Song_Guo_2011
    :members:
    :undoc-members:
    :show-inheritance:

Tiquia\_Tam
-----------

.. automodule:: read.Tiquia_Tam
    :members:
    :undoc-members:
    :show-inheritance:

Thuenen
-------

.. automodule:: read.Thuenen
    :members:
    :undoc-members:
    :show-inheritance:

Eurostat
--------

.. automodule:: read.Eurostat
    :members:
    :undoc-members:
    :show-inheritance:

VDLUFA
------

.. automodule:: read.VDLUFA
    :members:
    :undoc-members:
    :show-inheritance:

