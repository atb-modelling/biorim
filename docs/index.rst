BIORIM model documentation
==========================

|  The Biological Resource Utilization Impacts (BIORIM) is a model to assess impacts and material flows of bioeconomic production systems.
|  
|  This documentation provides a basic tutorial on how to run the model, a description of the individual modules, and provides hints for model developers.

.. toctree::
   :maxdepth: 2

   tutorial
   model_development
   modules


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`