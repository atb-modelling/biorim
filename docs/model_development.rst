#################
Model development
#################

The BIORIM model basically consists of three parts:

|  :ref:`Systems` that represent the processes of the bioeconomy.
|  :ref:`Products & Emissions` which are the exchanged between systems.
|  :ref:`Analysis` used to assess impacts of complex production networks.


.. _Systems:

*******
Systems
*******


Systems represent the processes of the bioeconomy (e.g. a storage of some substrate). In the model they are represented by classes.

* They share a standard set of methods (e.g. input, output, emissions)
* They are the only place where products can be created and/or modified.


Parameters & Initialization
===========================

Class arguments are used for three basic purposes:

#. Define requested inputs (I) or outputs (O)
#. Define process parameters (e.g. process temperature)
#. Select emission computation method

Minimal requirement
-------------------

* Classes should interit from the :meth:`declarations.Process` class to assure basic functionality
* They can either be defined via their input or output. Therefore all classes require at least one of the two arguments.
  **I** or **O** should be defined with the object type :meth:`declarations.Product`, and a default setting should be included.
* An **id** argument is required to be able to distinguish different class instances in the analysis

A simple example of a class __init__ can therefore look like this:

.. code-block:: python

    from delarations import Process
    from tools import prod

    class CalfRearing(Process):
        def __init__(self, O = prod("calves", 1, "n"), livespan_days = 150, id = None):
            self.O = O
            self.livespan_days = livespan_days
            self.id = id

Class parameters
----------------

Class parameters are usefull to define parameters (usually floating point numbers) that are currently not varied (and therefore the same for all class instances), 
but could potentially for future applications. They are defined directly below the class definition. One advantage is that they are automatically listed in the documentation.
As all parameters they should be defined in ALL_CAPS.

.. code-block:: python

    class BiogasPlant(Process):
        CHP_EFFICIENCY_ELECTRICAL = KTBL.biogas_fist_numbers().loc["CHP efficiency, electrical","value"]
        CHP_EFFICIENCY_THERMAL = KTBL.biogas_fist_numbers().loc["CHP efficiency, thermal","value"]

        def __init__(self, ...

Methods
=======

There are two types of methods:

#. Methods shared across all system classes (e.g. input() and output())
#. Methods that are mainly used internally

Shared methods
--------------

The following methods are shared across different classes. They need to be defined as they are used when conducting the network analysis. 
Additional arguments to these methods are **not** allowed.

**input** (*Product*): This is the sum of all inputs. If not defined specifically it is automatically set as the sum of *additional_input()* and *I* when inheriting from :meth:`declarations.Process`

**additional_input** (*Product*): This is all input required besides the one already provided via the I argument.

**output** (*Product*): All output of the class except for emissions.

In order for including the name of the class and the id in the output use the following option::

    return add_system(Output, module = self.__module__, class_name = self.__class__.__name__, id = self.id)

**emissions** (*Emissions*): Emissions caused by the system. Emissions reported in in kg. 
Within the model they are usually reported in relation to the main element, e.g. CO2-C (amount of carbon in CO2) or NH4-N

**costs**: Not used yet, but will potentially be included later on.

Internally used methods
-----------------------

To define the system appropriately it is often helpful to use additional methods.
There are no requirements for these additional methods, but they should be named in a way that makes it easy to grasp their meaning.
It is therefore also good practice to also include the unit in the name, e.g. **digestion_time_h**


.. _Products & Emissions:

********************
Products & Emissions
********************

|  *Products* which are produced by :ref:`Systems` can be the input to other Systems.
|  *Emissions* are losses to the environment, and therefore not passed to other Systems.

Products
========

Products are the result of an output method.

* :meth:`declarations.Product` is a special pandas DataFrame used for commodities exchanged in the model
* :meth:`tools.prod` is a helper function to create them, e.g. ``prod("maize", 1, "ton")``

Required properties
-------------------

All products should comprise at least the following four columns:

========== ==============================================================================
Column     Description
========== ==============================================================================
**system** Index column that describes where the item originated from
**item**   Name of the item, e.g. "broiler manure"
**Q**      Represents the quantiy. For masses this is the fresh matter (FM) in tons.
**U**      The unit in which Q is represented. Default units are: *"ton"*, *"kWh"*, *"h"*
========== ==============================================================================

A typical Product therefore looks like this::

                                                           Q    U
    system                                item
    production.animal.poultry.BroilerFarm broiler manure  10  ton

Products that consist only of one item however are displayed in the short form::

    Product('('production.animal.poultry.BroilerFarm', 'broiler manure')', 10, ton)


Other common properties
-----------------------

=========== ================================== =============== ==============================
Column      Description                        Unit            Note
=========== ================================== =============== ==============================
**mass**    *not used for future development!*
**DM**      Dry matter content                 kg DM/kg FM
**oDM**     Organic dry matter content         kg oDM/kg DM    ≙ volatile solids (VS)
**orgC**    Organic carbon content             kg Corg/kg oDM
**N**       Nitrogen content                   kg N/ton FM
**NH4-N**   Ammonium content                   kg NH4-N/ton FM ≙ TAN (total ammonia nitrogen)
**P**       Phosphorus content
**K**       Potassium content
**density** Density                            ton FM/m³
=========== ================================== =============== ==============================

|  Since **oDM** and **orgC** is defined relative to **DM** it is necessary to multiply them with each other to derive absolute masses. 
   To get the absolute amount of organic carbon in kg it is necessary to compute:
|  Q * DM * oDM * orgC * 1000 

|  Since the **NH4-N** content is defined in kg NH4-N/ton FM to get the absolute amount in kg is more straightforward:
|  Q * NH4-N

Since **P** and **K** contents are often reported as *P2O5* and *K2O* they need to be converted to the elementary contents.
This can be done with the use of conversion factors::

    from tools import prod
    from parameters import ConversionFactor

    A = prod("example item", Q = 1, U = "ton", P2O5 = 5, K2O = 10)
    
    A["P"] = A["P2O5"] * ConversionFactor.loc[("P2O5","P2O5-P"), "factor"]
    A["K"] = A["K2O"] * ConversionFactor.loc[("K2O","K2O-K"), "factor"]


Special properties
------------------

These are for instance used to define feedstock for biogas plants.

=============== ================================================== ================ ==============================
Column          Description                                        Unit             Note
=============== ================================================== ================ ==============================
**N_fert_eff**  Nitrogen fertilization efficiency                  kg N/kg N
**HEQ**         Humus equivalents (see VDLUFA method)
**CH4 content** Methane content in biogas                          m³ CH4/m³ biogas
**B0**          Methane production potential                       m3/kg oDM
**location**    Currently only used to specify Ecoinvent datasets.                  Examples are *GLO*, *DEU*, *CH*
=============== ================================================== ================ ==============================


Emissions
=========

Similar to Products, :meth:`declarations.Emissions` are a special DataFrame type to report emissions from processes.

* Emissions are always reported in *kg*.
* Internally, for emissions of nitrogen and carbon containing gases (e.g. CO2 or N2O) the quantity of the elements nitrogen or carbon is specified. 
  This is done because it simplifies the computation of nitrogen and carbon losses.

A simple option to convert these gases from molecule to element masses is to use the function :meth:`tools.convert_molmasses` ::

    from tools import convert_molmasses
    from declarations import Emissions

    A = Emissions({'CO2': 10, 'N2O': 0.5}, index=["example emissions"])
    B = convert_molmasses(A, to="element")



.. _Analysis:

********
Analysis
********


*************
Documentation
*************

.. toctree::
   :maxdepth: 2

   model_development/documentation

*****************
Inclusion of data
*****************

The general approach for including data in the BIORIM model differs depending on the complexity of the data.

*  Single values can be included directly in the code. In this case a reference to the origin needs to be mentioned as a comment
*  More complex datasets (e.g. tables) should be included via the :doc:`read` module

The read module provides the basic functionality to load datasets from the datafolder as specified in the ``data_path`` in the *modelconfig.py*.
To include new datasets includes the following steps:

Storage of data
===============

For each data source (e.g. publication, archieve, etc.) a subfolder is created in the datafolder

*  The folder is named according to the source (e.g. IPCC or Liebetrau_2010 for a publication by Liebetrau et al. 2010)
*  This folder always needs to contain the original dataset (e.g. pdf file of publication) as well as the data in a format that can be read be the computer (e.g. csv)

Function in read module
=======================

Within read a module is defined by the same name as the data folder, e.g. *Liebetrau_2010.py*

In the simplest case this looks like this::

    from read import datapath, read_mycsv

    def mcf_manure_managements():
        return read_mycsv(datapath("IPCC\\2019 IPCC Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories\\19R_V4_Ch10_Livestock_advance_table10-17.csv"), index_col=0, sep=";")

|  ``datapath`` sets the absolute path to the data folder
|  ``read_mycsv`` is an adapted function to read in csv files that start like this:

.. code-block:: none

    # Description: Methane conversion factors for manure management systems
    # Source: Table 10.17 of 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories
    System;Cool Temperate Moist;Cool Temperate Dry;Boreal Moist;Boreal Dry;Warm Temperate Moist;Warm Temperate Dry;Tropical Montane;Tropical Wet;Tropical Moist;Tropical Dry
    Uncovered anaerobic lagoon;60;67;50;49;73;76;76;80;80;80
    Liquid/Slurry, and Pit storage below animal confinements, 1 month;6;8;4;4;13;15;25;38;36;42
    Liquid/Slurry, and Pit storage below animal confinements, 3 month;12;16;8;8;24;28;43;61;57;62
    ...

Whenever datasets are stored as csv files the *first two rows* should contain **# Description:** and **# Source:** and provide a brief description of the dataset.
This has the advantage that this information can later on easily be accessed directly during model development, e.g.::

    from read import IPCC
    MCF_Manure_Management = IPCC.mcf_manure_managements()
    MCF_Manure_Management.file
    MCF_Manure_Management.source

Usage in classes
================

Using the data in the module where it is required is then straightforward::

    from read import IPCC
    MCF_POULTRY = IPCC.mcf_manure_managements().loc["Poultry manure with and without litter", "Cool Temperate Dry"]


******************
Naming conventions
******************

The coding conventions are based on some of the general python recommendations found under:
https://www.python.org/dev/peps/pep-0008/
However, they specify also some additional requirements

* Classes are named in CapWords, e.g. BroilerFarm
* Functions and methods (functions with classes) are usually in lower_case_with_underscores, e.g. convert_molmasses
* Constants (usually integer or float type) are in UPPERCASE, e.g. N_LOSS_TON
* Pandas DataFrames and Product objects should use use CapWords with underscores, e.g. Emis or Total_Emis

****************************************
General hints for new Python programmers
****************************************

If you are more familiar with programming in R, it should be realtively easy to feel at home in Python after a short while due to the similar logic.
There are however some pitfalls where the two programming languages are behaving very differently. The following provides a loose collection of notable differences that frequently cause issues:

.. toctree::
    :maxdepth: 2
 
    model_development/differences_to_R

