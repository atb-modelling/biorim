########
Tutorial
########

.. role:: python(code)
   :language: python
   :class: highlight


General requirements
====================


Python distribution
-------------------

|  To run the model a version of Python 3.8 is required. It is highly recommended to use the python distribution `Anaconda <https://www.anaconda.com/products/individual>`_.
|  Several Python packages are required in addition to run the model. These dependencies are defined in the *environment.yml* file.
|  In order to create a new Anaconda environment with all required packages installed navigate to the biorim folder and run the following command:

.. code-block:: python

    conda env create

Code editor
-----------

|  The recommended code editor for working with the model is `Visual Studio Code <https://code.visualstudio.com>`_.
|  When using different environments in Anaconda it is important to also select the correct Python interpreter in Visual Studio Code as described in the `documentation <https://code.visualstudio.com/docs/python/environments>`_.

Getting started
===============

Define model config
-------------------

|  After Anaconda is installed and a version of the model derived from Gitlab via

.. code-block:: python

    git clone https://gitlab.com/atb-modelling/biorim.git

|  it is necessary to customize a few pathes that link to the underlying data.
|  In the main directory of the model look for a file called *modelconfig_sample.py*. It should read similar to this:

.. code-block:: python

    data_path = "C:/Users/ukreidenweis/Nextcloud/biorimdata"
    ecoinvent_path = "L:/Abt2/Systemmodellierung"
    output_path = "C:/Users/ukreidenweis/Documents/biorim/simulation/output"
    report_internal = False

Adapt this file according to your local conditions and save it as *modelconfig.py*

| ``data_path`` should specify the folder where the necessary model data is stored
| ``ecoinvent_path`` defines where the whole ecoinvent database is located
| ``output_path`` is the folder where model results will be written to
| ``report_internal`` is a switch to help in debugging some functions, as it enables more reporting.

Add model to Python search path
-------------------------------

In order for Python to be able to find the modules of the BIORIM model it is necessary to add them to the search path.
This is easiest achieved by navigating to the biorim model folder (or opening it in Visual Studio Code), and executing

.. code-block:: python

    import start

in a Python terminal.
This way the core model folder and the main modules (e.g. analysis, production, read) are added to the search path.


General model description
=========================

The basic structure of the model is similar to other life cycle assessment models.
Different processes or systems, such as a biogas plant, are represented as a class (:meth:`material.biogas.BiogasPlant`)
that converts a set of inputs (biogas feedstocks in this case) into a set of outputs (e.g. biogas digestate, and electricity) and emissions.
Inputs, outputs and emissions are represented by methods of the process class.

These systems can be used *standalone*.

In order to compute the theoretical output for a biogas plant that is fed with 10 tonnes of broiler manure this looks like this::

    from material.biogas import BiogasPlant
    from tools import prod

    BiogasPlant(I=prod("broiler manure", Q=10, U="ton")).output()

The argument **I** in this case stands for the input of feedstock and is defined by the user.
**output()** is the method that shows the products that are produced in this case::

                                                         DM     HEQ        K          N     NH4-N  N_fert_eff        P            Q    U       oDM      orgC
    system                      item
    material.biogas.BiogasPlant biogas digestate   0.480555  0.2833  24.7825  44.185668  4.433345         0.6  14.1879     6.766900  ton  0.684489  0.347832
                                electrical energy       NaN     NaN      NaN        NaN       NaN         NaN      NaN  4831.269699  kWh       NaN       NaN
                                thermal energy          NaN     NaN      NaN        NaN       NaN         NaN      NaN  2237.640703  kWh       NaN       NaN

Returned is a table, that shows that 6.77 tons of biogas digestate are produced in this case, as well as 4831 kWh of electricity and 2238 kWh of heat.
The **Q** column represents the quantity here, **U** stands for the unit of the output. Default units in the model are "ton" for masses (in fresh matter) 
and "kWh" for electricity. The other columns specify further output properties, such as the dry matter content (DM) or the nitrogen content (N).

Calling :python:`BiogasPlant(I=prod("broiler manure", 10, "ton")).emissions()` produces the respective emissions caused by this process::

                                                                CH4-C       CO2-C          H2O  N2O-N  NH3-N      NOx-N
    system                      item
    material.biogas.BiogasPlant digester                     6.639632    5.851471          NaN    NaN    NaN        NaN
                                poststorage                  0.349454    0.307972          NaN    0.0    0.0        NaN
                                CHP methane leakage          6.919196         NaN          NaN    NaN    NaN        NaN
                                CHP methane combustion            NaN  685.000395  2055.001185    NaN    NaN  11.240604
                                CHP CO2 from biogas               NaN  609.784904          NaN    NaN    NaN        NaN
                                CHP ignition oil combustion       NaN   32.398359    48.597539    NaN    NaN        NaN
                                construction                      NaN   19.784049          NaN    NaN    NaN        NaN


Setting up a network analysis
=============================

While using and linking individual systems is feasible and may be reasonable in some cases, setting up the model to do an analysis of a more complex network 
is probably the more meaningful way in most cases.

General concept
---------------

The general approach of network analysis is to link different systems in a way that the output of one system is passed to a subsequent downstream system.
This way the impacts of more complex production chains can be calculated.
The BIORIM model is especially targeted to consider interdependencies between different processes in doing the analysis. This achieved by passing products
with defined characteristics (e.g. dry matter content) between the processes.

To do the analysis involves three basic steps:

1. Setting up a config file that represents the network to be assessed
2. Executing the ``start_simulation`` function from ``analysis.timestep_chains`` to start the calculations
3. Interpreting the results created by this function

1. Setting up the config
------------------------


| Please consider the examples in *../simulation* which are named *config_example_timesteps.py* and *config_example_simple_sensitivity.py*:

.. literalinclude:: ../simulation/config_example_simple_sensitivity.py
    :language: python
    :emphasize-lines: 12,21,30

.. code-block:: python
    :emphasize-lines: 1
    
    simulation

- *name* – is used to identify the scenario in the results
- *type* – defines the type of simulation. Either ("single") using only the first parameter, or a combination of all parameters ("all combinations")
- *starting_system* – system for which the quantities are defined and from which modelling happens upstream and downstream
    
.. code-block:: python
    :emphasize-lines: 1
        
    analyses

|  Dictionary of the analyses that are conducted, and spefified settings for these. Currently there are three options:
|  *emissions* – reporting of emissions. outtype can either be set to ``molecule`` or ``element``.
|  *gwp* – calculation of CO2 equivalent emissions. "timerange" can be set to 20 or 100 years, or both (``[20, 100]``).
|  *cashflows* – calculation of financial cashflows.

    
.. code-block:: python
    :emphasize-lines: 1
    
    systems

|  Dictionary of all systems that are involved in the analysis for which parameters shall be defined in the following form: ``"object_id":{"argument_1" : value, "argument_2" : value, ..., argument_n : value}``
|  To differentiate between class instances of the same type they are numbered consecutively, e.g. ``transport.LandTransport_1``, ``transport.LandTransport_2``, etc.
|  To link different systems the can take the output of another system as input. To achieve this just name the source function in the input, e.g. ``'I': prod(('transport.LandTransport_1', 'broiler manure'))``
|  Parameters can either be specified with one value or lists of values. In case a list is specified, and when the simulation type is set accordingly all combinanations are assessed.


2. Run the simulation and save the results
------------------------------------------

The next step is to run the simulation by executing the :meth:`analysis.timestep_chains.start_simulation` function::

    from analysis.timestep_chains import start_simulation
    result = start_simulation(storageconfig)

Several scenarios can be run at once by providing them as a list::

    result = start_simulation([storageconfig, compostconfig])

The returned object from the ``start_simulation`` function is of the type :meth:`analysis.simdict`.
The results can easily be saved to a folder. 
The :meth:`analysis.to_folder` function creates a folder that contains the scenario parameters as .csv files, emissions as .nc (for processing in R) and the simdict as a .pkl file.::

    result.to_folder(subfolder="model_description_paper", time_as_folder=True)

|  The specified paths are relative to the *output_path* as specified in the main *modelconfig* file.

The results can later easily be load back into memory::

    from analysis import read_pickle
    result = read_pickle("model_description_paper\\2024-01-11_16-04-05\simdata.pkl")

3. Accessing the simdata file and interpreting the data
-------------------------------------------------------

|  The simdict object is a special dictionary. The main category are the names as specied under *name* in the config.

All entries can easily be seen by::

    result["storage"].keys()

Examples on how to access the results:

.. code-block:: python

    result["storage"]["t1"]["parameters"] # table of the assessed parameter combinations (runs)

    result["storage"]["t1"][0]["emissions"] # emissions from the first run (as used internally in the model)
    result["storage"]["t1"][3]["emissions"] # emissions from the third run

    result["storage"]["emissions"].loc["t1", 0].to_pandas() # emissions from the first run (converted as specified under analyses in the config)

    result["storage"]["gwp"].sel(timestep="t1", run=0, gwp="GWP100").to_pandas() # 100 years global warming potenial emissions