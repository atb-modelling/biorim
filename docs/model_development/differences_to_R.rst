Differences to R
================

While R and Python are both object-oriented programming languages and share some common features, there are also some common pitfalls.
This is a loose collection of problems that have frequently occured:

Logical operators
-----------------

|  ``|`` and ``&`` are binery operators, meaning they compare on binary level.
|  E.g. 156 | 52 = 188 because it is computed as 10011100 or 110100, which gives 10111100 (= 188).

|  The respective logical operators are ``or`` and ``and``.
|  For more details see for instance `realpython <https://realpython.com/python-bitwise-operators/>`_

Mutable and immutable objects
-----------------------------

Most python objects (booleans, integers, floats, strings, and tuples) are immutable, which means they cannot be changed.
Entries in lists and pandas dataframes, however, can be modified.

Consider the following example::

    >>> X = 10
    >>> Y = X
    >>> Y = 20
    >>> X
    10

As expected, X remains 10.

A change of an entry of DataFrame B changes DataFrame A::

    >>> import pandas as pd
    >>> A = pd.DataFrame({'Q':10, 'U':"ton"}, index=["maize"])
    >>> A
            Q    U
    maize  10  ton
    >>> B = A
    >>> B["Q"] = 20
    >>> A
            Q    U
    maize  20  ton
    >>>

One way to avoid this is to make a copy::

    >>> A = pd.DataFrame({'Q':10, 'U':"ton"}, index=["maize"])
    >>> A
            Q    U
    maize  10  ton
    >>> B = A.copy()
    >>> B["Q"] = 20
    >>> A
            Q    U
    maize  10  ton

Note that this also applies to functions and classes, where it may cause errors that are hard to debug::

    from tools import prod

    class Test:
        def __init__(self, O = prod("maize",10,"ton")):
            self.O = O
        
        def a(self):
            O = self.O 
            O["Q"] = 20

        def b(self):
            O = self.O.copy()
            O["Q"] = 30

Method *a* changes the attribute *O*, while method *b* does not::

    >>> Test().O
    Product('('.', 'maize')', 10, ton)
    >>> Test().a()
    >>> Test().O
    Product('('.', 'maize')', 20, ton)
    >>> Test().b()
    >>> Test().O
    Product('('.', 'maize')', 20, ton)