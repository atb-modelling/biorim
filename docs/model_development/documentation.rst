Documentation
=============

`Sphinx <sphinx-doc.org>`_ is used for documenting the model. When new modules are developed they therefore need to be documented properly.

Documentation of classes
------------------------

Classes need a documentation block right below the first line. Consider the following example:

.. code-block:: python

    class PyrolysisPlant(Process):
        
        """     
        | The PyrolysisPlant converts biogenic material into biochar.
        | The assumption is that biochar is the main product, therefore no additional syngas, tar or heat are produced
        
        :type I: Product
        :param I: Input to the Pyrolysis Plant
        :type heating_temp: numeric
        :param heating_temp: heating temperature of substrate within the plant (in °C)
        :type ton_DM_per_year: numeric  
        :param ton_DM_per_year: mass (in t) which is generally processed per year
        :type CH4_content_exhaust: numeric
        :param CH4_content_exhaust: share of CH4 in exhaust (e.g. 0.0001)

        |
        """

The documentation should briefly describe which system it represents. All parameters should also be described.

Create the docs
---------------

Assure that the necessary .rst files exist in the docs folder, and add the modules to the toctree if necessary.


Update the html files
---------------------

Creating the documentation is straightforward. In the console assue that the right Anaconda environment is activated::

    conda activate biorim

Then navigate to the documentation folder::

    cd docs

And lastly remove the old html files and create the new ones::

    make clean
    make html

The ``make clean`` command is not required if only one single .rst file is updated.