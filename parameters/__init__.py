__all__ : ["GasDensity", "ConversionFactor"]

import pandas as pd
from tools import relpath

GasDensity = {
    "CH4": 0.67,
    "CO2": 1.98,
    "N2O": 1.98,
    }

ConversionFactor = \
    pd.read_csv(relpath("parameters\\conversion_factors.csv"), sep=";", encoding="latin-1", index_col=(0,1), skiprows=3)